<?php
/**
 *
 * @copyright 2013 Spirit of Co.,Ltd. All Rights Reserved.
 * @link http://sbps.ec-cube.pro/
 *
 */


// {{{ requires
require_once('../require.php');
require_once(MODULE_REALDIR . "mdl_sps/inc/include.php");
require_once(MDL_SPS_CLASSEX_PATH . 'page_extends/LC_Page_Admin_Order_SbpsStatus_Ex.php');

// }}}
// {{{ generate page

$objPage = new LC_Page_Admin_Order_SbpsStatus_Ex();
$objPage->init();
$objPage->process();
