"use strict";

/*!
 * @copyright Copyright (c) 2017 IcoMoon.io
 * @license   Licensed under MIT license
 *            See https://github.com/Keyamoon/svgxuse
 * @version   1.2.6
 */
/*jslint browser: true */
/*global XDomainRequest, MutationObserver, window */
(function () {
    "use strict";

    if (typeof window !== "undefined" && window.addEventListener) {
        var cache = Object.create(null); // holds xhr objects to prevent multiple requests
        var checkUseElems;
        var tid; // timeout id
        var debouncedCheck = function debouncedCheck() {
            clearTimeout(tid);
            tid = setTimeout(checkUseElems, 100);
        };
        var unobserveChanges = function unobserveChanges() {
            return;
        };
        var observeChanges = function observeChanges() {
            var observer;
            window.addEventListener("resize", debouncedCheck, false);
            window.addEventListener("orientationchange", debouncedCheck, false);
            if (window.MutationObserver) {
                observer = new MutationObserver(debouncedCheck);
                observer.observe(document.documentElement, {
                    childList: true,
                    subtree: true,
                    attributes: true
                });
                unobserveChanges = function unobserveChanges() {
                    try {
                        observer.disconnect();
                        window.removeEventListener("resize", debouncedCheck, false);
                        window.removeEventListener("orientationchange", debouncedCheck, false);
                    } catch (ignore) {}
                };
            } else {
                document.documentElement.addEventListener("DOMSubtreeModified", debouncedCheck, false);
                unobserveChanges = function unobserveChanges() {
                    document.documentElement.removeEventListener("DOMSubtreeModified", debouncedCheck, false);
                    window.removeEventListener("resize", debouncedCheck, false);
                    window.removeEventListener("orientationchange", debouncedCheck, false);
                };
            }
        };
        var createRequest = function createRequest(url) {
            // In IE 9, cross origin requests can only be sent using XDomainRequest.
            // XDomainRequest would fail if CORS headers are not set.
            // Therefore, XDomainRequest should only be used with cross origin requests.
            function getOrigin(loc) {
                var a;
                if (loc.protocol !== undefined) {
                    a = loc;
                } else {
                    a = document.createElement("a");
                    a.href = loc;
                }
                return a.protocol.replace(/:/g, "") + a.host;
            }
            var Request;
            var origin;
            var origin2;
            if (window.XMLHttpRequest) {
                Request = new XMLHttpRequest();
                origin = getOrigin(location);
                origin2 = getOrigin(url);
                if (Request.withCredentials === undefined && origin2 !== "" && origin2 !== origin) {
                    Request = XDomainRequest || undefined;
                } else {
                    Request = XMLHttpRequest;
                }
            }
            return Request;
        };
        var xlinkNS = "http://www.w3.org/1999/xlink";
        checkUseElems = function checkUseElems() {
            var base;
            var bcr;
            var fallback = ""; // optional fallback URL in case no base path to SVG file was given and no symbol definition was found.
            var hash;
            var href;
            var i;
            var inProgressCount = 0;
            var isHidden;
            var Request;
            var url;
            var uses;
            var xhr;
            function observeIfDone() {
                // If done with making changes, start watching for chagnes in DOM again
                inProgressCount -= 1;
                if (inProgressCount === 0) {
                    // if all xhrs were resolved
                    unobserveChanges(); // make sure to remove old handlers
                    observeChanges(); // watch for changes to DOM
                }
            }
            function attrUpdateFunc(spec) {
                return function () {
                    if (cache[spec.base] !== true) {
                        spec.useEl.setAttributeNS(xlinkNS, "xlink:href", "#" + spec.hash);
                        if (spec.useEl.hasAttribute("href")) {
                            spec.useEl.setAttribute("href", "#" + spec.hash);
                        }
                    }
                };
            }
            function onloadFunc(xhr) {
                return function () {
                    var body = document.body;
                    var x = document.createElement("x");
                    var svg;
                    xhr.onload = null;
                    x.innerHTML = xhr.responseText;
                    svg = x.getElementsByTagName("svg")[0];
                    if (svg) {
                        svg.setAttribute("aria-hidden", "true");
                        svg.style.position = "absolute";
                        svg.style.width = 0;
                        svg.style.height = 0;
                        svg.style.overflow = "hidden";
                        body.insertBefore(svg, body.firstChild);
                    }
                    observeIfDone();
                };
            }
            function onErrorTimeout(xhr) {
                return function () {
                    xhr.onerror = null;
                    xhr.ontimeout = null;
                    observeIfDone();
                };
            }
            unobserveChanges(); // stop watching for changes to DOM
            // find all use elements
            uses = document.getElementsByTagName("use");
            for (i = 0; i < uses.length; i += 1) {
                try {
                    bcr = uses[i].getBoundingClientRect();
                } catch (ignore) {
                    // failed to get bounding rectangle of the use element
                    bcr = false;
                }
                href = uses[i].getAttribute("href") || uses[i].getAttributeNS(xlinkNS, "href") || uses[i].getAttribute("xlink:href");
                if (href && href.split) {
                    url = href.split("#");
                } else {
                    url = ["", ""];
                }
                base = url[0];
                hash = url[1];
                isHidden = bcr && bcr.left === 0 && bcr.right === 0 && bcr.top === 0 && bcr.bottom === 0;
                if (bcr && bcr.width === 0 && bcr.height === 0 && !isHidden) {
                    // the use element is empty
                    // if there is a reference to an external SVG, try to fetch it
                    // use the optional fallback URL if there is no reference to an external SVG
                    if (fallback && !base.length && hash && !document.getElementById(hash)) {
                        base = fallback;
                    }
                    if (uses[i].hasAttribute("href")) {
                        uses[i].setAttributeNS(xlinkNS, "xlink:href", href);
                    }
                    if (base.length) {
                        // schedule updating xlink:href
                        xhr = cache[base];
                        if (xhr !== true) {
                            // true signifies that prepending the SVG was not required
                            setTimeout(attrUpdateFunc({
                                useEl: uses[i],
                                base: base,
                                hash: hash
                            }), 0);
                        }
                        if (xhr === undefined) {
                            Request = createRequest(base);
                            if (Request !== undefined) {
                                xhr = new Request();
                                cache[base] = xhr;
                                xhr.onload = onloadFunc(xhr);
                                xhr.onerror = onErrorTimeout(xhr);
                                xhr.ontimeout = onErrorTimeout(xhr);
                                xhr.open("GET", base);
                                xhr.send();
                                inProgressCount += 1;
                            }
                        }
                    }
                } else {
                    if (!isHidden) {
                        if (cache[base] === undefined) {
                            // remember this URL if the use element was not empty and no request was sent
                            cache[base] = true;
                        } else if (cache[base].onload) {
                            // if it turns out that prepending the SVG is not necessary,
                            // abort the in-progress xhr.
                            cache[base].abort();
                            delete cache[base].onload;
                            cache[base] = true;
                        }
                    } else if (base.length && cache[base]) {
                        setTimeout(attrUpdateFunc({
                            useEl: uses[i],
                            base: base,
                            hash: hash
                        }), 0);
                    }
                }
            }
            uses = "";
            inProgressCount += 1;
            observeIfDone();
        };
        var _winLoad;
        _winLoad = function winLoad() {
            window.removeEventListener("load", _winLoad, false); // to prevent memory leaks
            tid = setTimeout(checkUseElems, 0);
        };
        if (document.readyState !== "complete") {
            // The load event fires when all resources have finished loading, which allows detecting whether SVG use elements are empty.
            window.addEventListener("load", _winLoad, false);
        } else {
            // No need to add a listener if the document is already loaded, initialize immediately.
            _winLoad();
        }
    }
})();
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInZlbmRvci9zdmd4dXNlLmpzIl0sIm5hbWVzIjpbIndpbmRvdyIsImFkZEV2ZW50TGlzdGVuZXIiLCJjYWNoZSIsIk9iamVjdCIsImNyZWF0ZSIsImNoZWNrVXNlRWxlbXMiLCJ0aWQiLCJkZWJvdW5jZWRDaGVjayIsImNsZWFyVGltZW91dCIsInNldFRpbWVvdXQiLCJ1bm9ic2VydmVDaGFuZ2VzIiwib2JzZXJ2ZUNoYW5nZXMiLCJvYnNlcnZlciIsIk11dGF0aW9uT2JzZXJ2ZXIiLCJvYnNlcnZlIiwiZG9jdW1lbnQiLCJkb2N1bWVudEVsZW1lbnQiLCJjaGlsZExpc3QiLCJzdWJ0cmVlIiwiYXR0cmlidXRlcyIsImRpc2Nvbm5lY3QiLCJyZW1vdmVFdmVudExpc3RlbmVyIiwiaWdub3JlIiwiY3JlYXRlUmVxdWVzdCIsInVybCIsImdldE9yaWdpbiIsImxvYyIsImEiLCJwcm90b2NvbCIsInVuZGVmaW5lZCIsImNyZWF0ZUVsZW1lbnQiLCJocmVmIiwicmVwbGFjZSIsImhvc3QiLCJSZXF1ZXN0Iiwib3JpZ2luIiwib3JpZ2luMiIsIlhNTEh0dHBSZXF1ZXN0IiwibG9jYXRpb24iLCJ3aXRoQ3JlZGVudGlhbHMiLCJYRG9tYWluUmVxdWVzdCIsInhsaW5rTlMiLCJiYXNlIiwiYmNyIiwiZmFsbGJhY2siLCJoYXNoIiwiaSIsImluUHJvZ3Jlc3NDb3VudCIsImlzSGlkZGVuIiwidXNlcyIsInhociIsIm9ic2VydmVJZkRvbmUiLCJhdHRyVXBkYXRlRnVuYyIsInNwZWMiLCJ1c2VFbCIsInNldEF0dHJpYnV0ZU5TIiwiaGFzQXR0cmlidXRlIiwic2V0QXR0cmlidXRlIiwib25sb2FkRnVuYyIsImJvZHkiLCJ4Iiwic3ZnIiwib25sb2FkIiwiaW5uZXJIVE1MIiwicmVzcG9uc2VUZXh0IiwiZ2V0RWxlbWVudHNCeVRhZ05hbWUiLCJzdHlsZSIsInBvc2l0aW9uIiwid2lkdGgiLCJoZWlnaHQiLCJvdmVyZmxvdyIsImluc2VydEJlZm9yZSIsImZpcnN0Q2hpbGQiLCJvbkVycm9yVGltZW91dCIsIm9uZXJyb3IiLCJvbnRpbWVvdXQiLCJsZW5ndGgiLCJnZXRCb3VuZGluZ0NsaWVudFJlY3QiLCJnZXRBdHRyaWJ1dGUiLCJnZXRBdHRyaWJ1dGVOUyIsInNwbGl0IiwibGVmdCIsInJpZ2h0IiwidG9wIiwiYm90dG9tIiwiZ2V0RWxlbWVudEJ5SWQiLCJvcGVuIiwic2VuZCIsImFib3J0Iiwid2luTG9hZCIsInJlYWR5U3RhdGUiXSwibWFwcGluZ3MiOiI7O0FBQUE7Ozs7OztBQU1BO0FBQ0E7QUFDQyxhQUFZO0FBQ1Q7O0FBQ0EsUUFBSSxPQUFPQSxNQUFQLEtBQWtCLFdBQWxCLElBQWlDQSxPQUFPQyxnQkFBNUMsRUFBOEQ7QUFDMUQsWUFBSUMsUUFBUUMsT0FBT0MsTUFBUCxDQUFjLElBQWQsQ0FBWixDQUQwRCxDQUN6QjtBQUNqQyxZQUFJQyxhQUFKO0FBQ0EsWUFBSUMsR0FBSixDQUgwRCxDQUdqRDtBQUNULFlBQUlDLGlCQUFpQixTQUFqQkEsY0FBaUIsR0FBWTtBQUM3QkMseUJBQWFGLEdBQWI7QUFDQUEsa0JBQU1HLFdBQVdKLGFBQVgsRUFBMEIsR0FBMUIsQ0FBTjtBQUNILFNBSEQ7QUFJQSxZQUFJSyxtQkFBbUIsNEJBQVk7QUFDL0I7QUFDSCxTQUZEO0FBR0EsWUFBSUMsaUJBQWlCLFNBQWpCQSxjQUFpQixHQUFZO0FBQzdCLGdCQUFJQyxRQUFKO0FBQ0FaLG1CQUFPQyxnQkFBUCxDQUF3QixRQUF4QixFQUFrQ00sY0FBbEMsRUFBa0QsS0FBbEQ7QUFDQVAsbUJBQU9DLGdCQUFQLENBQXdCLG1CQUF4QixFQUE2Q00sY0FBN0MsRUFBNkQsS0FBN0Q7QUFDQSxnQkFBSVAsT0FBT2EsZ0JBQVgsRUFBNkI7QUFDekJELDJCQUFXLElBQUlDLGdCQUFKLENBQXFCTixjQUFyQixDQUFYO0FBQ0FLLHlCQUFTRSxPQUFULENBQWlCQyxTQUFTQyxlQUExQixFQUEyQztBQUN2Q0MsK0JBQVcsSUFENEI7QUFFdkNDLDZCQUFTLElBRjhCO0FBR3ZDQyxnQ0FBWTtBQUgyQixpQkFBM0M7QUFLQVQsbUNBQW1CLDRCQUFZO0FBQzNCLHdCQUFJO0FBQ0FFLGlDQUFTUSxVQUFUO0FBQ0FwQiwrQkFBT3FCLG1CQUFQLENBQTJCLFFBQTNCLEVBQXFDZCxjQUFyQyxFQUFxRCxLQUFyRDtBQUNBUCwrQkFBT3FCLG1CQUFQLENBQTJCLG1CQUEzQixFQUFnRGQsY0FBaEQsRUFBZ0UsS0FBaEU7QUFDSCxxQkFKRCxDQUlFLE9BQU9lLE1BQVAsRUFBZSxDQUFFO0FBQ3RCLGlCQU5EO0FBT0gsYUFkRCxNQWNPO0FBQ0hQLHlCQUFTQyxlQUFULENBQXlCZixnQkFBekIsQ0FBMEMsb0JBQTFDLEVBQWdFTSxjQUFoRSxFQUFnRixLQUFoRjtBQUNBRyxtQ0FBbUIsNEJBQVk7QUFDM0JLLDZCQUFTQyxlQUFULENBQXlCSyxtQkFBekIsQ0FBNkMsb0JBQTdDLEVBQW1FZCxjQUFuRSxFQUFtRixLQUFuRjtBQUNBUCwyQkFBT3FCLG1CQUFQLENBQTJCLFFBQTNCLEVBQXFDZCxjQUFyQyxFQUFxRCxLQUFyRDtBQUNBUCwyQkFBT3FCLG1CQUFQLENBQTJCLG1CQUEzQixFQUFnRGQsY0FBaEQsRUFBZ0UsS0FBaEU7QUFDSCxpQkFKRDtBQUtIO0FBQ0osU0ExQkQ7QUEyQkEsWUFBSWdCLGdCQUFnQixTQUFoQkEsYUFBZ0IsQ0FBVUMsR0FBVixFQUFlO0FBQy9CO0FBQ0E7QUFDQTtBQUNBLHFCQUFTQyxTQUFULENBQW1CQyxHQUFuQixFQUF3QjtBQUNwQixvQkFBSUMsQ0FBSjtBQUNBLG9CQUFJRCxJQUFJRSxRQUFKLEtBQWlCQyxTQUFyQixFQUFnQztBQUM1QkYsd0JBQUlELEdBQUo7QUFDSCxpQkFGRCxNQUVPO0FBQ0hDLHdCQUFJWixTQUFTZSxhQUFULENBQXVCLEdBQXZCLENBQUo7QUFDQUgsc0JBQUVJLElBQUYsR0FBU0wsR0FBVDtBQUNIO0FBQ0QsdUJBQU9DLEVBQUVDLFFBQUYsQ0FBV0ksT0FBWCxDQUFtQixJQUFuQixFQUF5QixFQUF6QixJQUErQkwsRUFBRU0sSUFBeEM7QUFDSDtBQUNELGdCQUFJQyxPQUFKO0FBQ0EsZ0JBQUlDLE1BQUo7QUFDQSxnQkFBSUMsT0FBSjtBQUNBLGdCQUFJcEMsT0FBT3FDLGNBQVgsRUFBMkI7QUFDdkJILDBCQUFVLElBQUlHLGNBQUosRUFBVjtBQUNBRix5QkFBU1YsVUFBVWEsUUFBVixDQUFUO0FBQ0FGLDBCQUFVWCxVQUFVRCxHQUFWLENBQVY7QUFDQSxvQkFBSVUsUUFBUUssZUFBUixLQUE0QlYsU0FBNUIsSUFBeUNPLFlBQVksRUFBckQsSUFBMkRBLFlBQVlELE1BQTNFLEVBQW1GO0FBQy9FRCw4QkFBVU0sa0JBQWtCWCxTQUE1QjtBQUNILGlCQUZELE1BRU87QUFDSEssOEJBQVVHLGNBQVY7QUFDSDtBQUNKO0FBQ0QsbUJBQU9ILE9BQVA7QUFDSCxTQTVCRDtBQTZCQSxZQUFJTyxVQUFVLDhCQUFkO0FBQ0FwQyx3QkFBZ0IseUJBQVk7QUFDeEIsZ0JBQUlxQyxJQUFKO0FBQ0EsZ0JBQUlDLEdBQUo7QUFDQSxnQkFBSUMsV0FBVyxFQUFmLENBSHdCLENBR0w7QUFDbkIsZ0JBQUlDLElBQUo7QUFDQSxnQkFBSWQsSUFBSjtBQUNBLGdCQUFJZSxDQUFKO0FBQ0EsZ0JBQUlDLGtCQUFrQixDQUF0QjtBQUNBLGdCQUFJQyxRQUFKO0FBQ0EsZ0JBQUlkLE9BQUo7QUFDQSxnQkFBSVYsR0FBSjtBQUNBLGdCQUFJeUIsSUFBSjtBQUNBLGdCQUFJQyxHQUFKO0FBQ0EscUJBQVNDLGFBQVQsR0FBeUI7QUFDckI7QUFDQUosbUNBQW1CLENBQW5CO0FBQ0Esb0JBQUlBLG9CQUFvQixDQUF4QixFQUEyQjtBQUFFO0FBQ3pCckMsdUNBRHVCLENBQ0g7QUFDcEJDLHFDQUZ1QixDQUVMO0FBQ3JCO0FBQ0o7QUFDRCxxQkFBU3lDLGNBQVQsQ0FBd0JDLElBQXhCLEVBQThCO0FBQzFCLHVCQUFPLFlBQVk7QUFDZix3QkFBSW5ELE1BQU1tRCxLQUFLWCxJQUFYLE1BQXFCLElBQXpCLEVBQStCO0FBQzNCVyw2QkFBS0MsS0FBTCxDQUFXQyxjQUFYLENBQTBCZCxPQUExQixFQUFtQyxZQUFuQyxFQUFpRCxNQUFNWSxLQUFLUixJQUE1RDtBQUNBLDRCQUFJUSxLQUFLQyxLQUFMLENBQVdFLFlBQVgsQ0FBd0IsTUFBeEIsQ0FBSixFQUFxQztBQUNqQ0gsaUNBQUtDLEtBQUwsQ0FBV0csWUFBWCxDQUF3QixNQUF4QixFQUFnQyxNQUFNSixLQUFLUixJQUEzQztBQUNIO0FBQ0o7QUFDSixpQkFQRDtBQVFIO0FBQ0QscUJBQVNhLFVBQVQsQ0FBb0JSLEdBQXBCLEVBQXlCO0FBQ3JCLHVCQUFPLFlBQVk7QUFDZix3QkFBSVMsT0FBTzVDLFNBQVM0QyxJQUFwQjtBQUNBLHdCQUFJQyxJQUFJN0MsU0FBU2UsYUFBVCxDQUF1QixHQUF2QixDQUFSO0FBQ0Esd0JBQUkrQixHQUFKO0FBQ0FYLHdCQUFJWSxNQUFKLEdBQWEsSUFBYjtBQUNBRixzQkFBRUcsU0FBRixHQUFjYixJQUFJYyxZQUFsQjtBQUNBSCwwQkFBTUQsRUFBRUssb0JBQUYsQ0FBdUIsS0FBdkIsRUFBOEIsQ0FBOUIsQ0FBTjtBQUNBLHdCQUFJSixHQUFKLEVBQVM7QUFDTEEsNEJBQUlKLFlBQUosQ0FBaUIsYUFBakIsRUFBZ0MsTUFBaEM7QUFDQUksNEJBQUlLLEtBQUosQ0FBVUMsUUFBVixHQUFxQixVQUFyQjtBQUNBTiw0QkFBSUssS0FBSixDQUFVRSxLQUFWLEdBQWtCLENBQWxCO0FBQ0FQLDRCQUFJSyxLQUFKLENBQVVHLE1BQVYsR0FBbUIsQ0FBbkI7QUFDQVIsNEJBQUlLLEtBQUosQ0FBVUksUUFBVixHQUFxQixRQUFyQjtBQUNBWCw2QkFBS1ksWUFBTCxDQUFrQlYsR0FBbEIsRUFBdUJGLEtBQUthLFVBQTVCO0FBQ0g7QUFDRHJCO0FBQ0gsaUJBaEJEO0FBaUJIO0FBQ0QscUJBQVNzQixjQUFULENBQXdCdkIsR0FBeEIsRUFBNkI7QUFDekIsdUJBQU8sWUFBWTtBQUNmQSx3QkFBSXdCLE9BQUosR0FBYyxJQUFkO0FBQ0F4Qix3QkFBSXlCLFNBQUosR0FBZ0IsSUFBaEI7QUFDQXhCO0FBQ0gsaUJBSkQ7QUFLSDtBQUNEekMsK0JBekR3QixDQXlESjtBQUNwQjtBQUNBdUMsbUJBQU9sQyxTQUFTa0Qsb0JBQVQsQ0FBOEIsS0FBOUIsQ0FBUDtBQUNBLGlCQUFLbkIsSUFBSSxDQUFULEVBQVlBLElBQUlHLEtBQUsyQixNQUFyQixFQUE2QjlCLEtBQUssQ0FBbEMsRUFBcUM7QUFDakMsb0JBQUk7QUFDQUgsMEJBQU1NLEtBQUtILENBQUwsRUFBUStCLHFCQUFSLEVBQU47QUFDSCxpQkFGRCxDQUVFLE9BQU92RCxNQUFQLEVBQWU7QUFDYjtBQUNBcUIsMEJBQU0sS0FBTjtBQUNIO0FBQ0RaLHVCQUFPa0IsS0FBS0gsQ0FBTCxFQUFRZ0MsWUFBUixDQUFxQixNQUFyQixLQUNJN0IsS0FBS0gsQ0FBTCxFQUFRaUMsY0FBUixDQUF1QnRDLE9BQXZCLEVBQWdDLE1BQWhDLENBREosSUFFSVEsS0FBS0gsQ0FBTCxFQUFRZ0MsWUFBUixDQUFxQixZQUFyQixDQUZYO0FBR0Esb0JBQUkvQyxRQUFRQSxLQUFLaUQsS0FBakIsRUFBd0I7QUFDcEJ4RCwwQkFBTU8sS0FBS2lELEtBQUwsQ0FBVyxHQUFYLENBQU47QUFDSCxpQkFGRCxNQUVPO0FBQ0h4RCwwQkFBTSxDQUFDLEVBQUQsRUFBSyxFQUFMLENBQU47QUFDSDtBQUNEa0IsdUJBQU9sQixJQUFJLENBQUosQ0FBUDtBQUNBcUIsdUJBQU9yQixJQUFJLENBQUosQ0FBUDtBQUNBd0IsMkJBQVdMLE9BQU9BLElBQUlzQyxJQUFKLEtBQWEsQ0FBcEIsSUFBeUJ0QyxJQUFJdUMsS0FBSixLQUFjLENBQXZDLElBQTRDdkMsSUFBSXdDLEdBQUosS0FBWSxDQUF4RCxJQUE2RHhDLElBQUl5QyxNQUFKLEtBQWUsQ0FBdkY7QUFDQSxvQkFBSXpDLE9BQU9BLElBQUl5QixLQUFKLEtBQWMsQ0FBckIsSUFBMEJ6QixJQUFJMEIsTUFBSixLQUFlLENBQXpDLElBQThDLENBQUNyQixRQUFuRCxFQUE2RDtBQUN6RDtBQUNBO0FBQ0E7QUFDQSx3QkFBSUosWUFBWSxDQUFDRixLQUFLa0MsTUFBbEIsSUFBNEIvQixJQUE1QixJQUFvQyxDQUFDOUIsU0FBU3NFLGNBQVQsQ0FBd0J4QyxJQUF4QixDQUF6QyxFQUF3RTtBQUNwRUgsK0JBQU9FLFFBQVA7QUFDSDtBQUNELHdCQUFJSyxLQUFLSCxDQUFMLEVBQVFVLFlBQVIsQ0FBcUIsTUFBckIsQ0FBSixFQUFrQztBQUM5QlAsNkJBQUtILENBQUwsRUFBUVMsY0FBUixDQUF1QmQsT0FBdkIsRUFBZ0MsWUFBaEMsRUFBOENWLElBQTlDO0FBQ0g7QUFDRCx3QkFBSVcsS0FBS2tDLE1BQVQsRUFBaUI7QUFDYjtBQUNBMUIsOEJBQU1oRCxNQUFNd0MsSUFBTixDQUFOO0FBQ0EsNEJBQUlRLFFBQVEsSUFBWixFQUFrQjtBQUNkO0FBQ0F6Qyx1Q0FBVzJDLGVBQWU7QUFDdEJFLHVDQUFPTCxLQUFLSCxDQUFMLENBRGU7QUFFdEJKLHNDQUFNQSxJQUZnQjtBQUd0Qkcsc0NBQU1BO0FBSGdCLDZCQUFmLENBQVgsRUFJSSxDQUpKO0FBS0g7QUFDRCw0QkFBSUssUUFBUXJCLFNBQVosRUFBdUI7QUFDbkJLLHNDQUFVWCxjQUFjbUIsSUFBZCxDQUFWO0FBQ0EsZ0NBQUlSLFlBQVlMLFNBQWhCLEVBQTJCO0FBQ3ZCcUIsc0NBQU0sSUFBSWhCLE9BQUosRUFBTjtBQUNBaEMsc0NBQU13QyxJQUFOLElBQWNRLEdBQWQ7QUFDQUEsb0NBQUlZLE1BQUosR0FBYUosV0FBV1IsR0FBWCxDQUFiO0FBQ0FBLG9DQUFJd0IsT0FBSixHQUFjRCxlQUFldkIsR0FBZixDQUFkO0FBQ0FBLG9DQUFJeUIsU0FBSixHQUFnQkYsZUFBZXZCLEdBQWYsQ0FBaEI7QUFDQUEsb0NBQUlvQyxJQUFKLENBQVMsS0FBVCxFQUFnQjVDLElBQWhCO0FBQ0FRLG9DQUFJcUMsSUFBSjtBQUNBeEMsbURBQW1CLENBQW5CO0FBQ0g7QUFDSjtBQUNKO0FBQ0osaUJBbkNELE1BbUNPO0FBQ0gsd0JBQUksQ0FBQ0MsUUFBTCxFQUFlO0FBQ1gsNEJBQUk5QyxNQUFNd0MsSUFBTixNQUFnQmIsU0FBcEIsRUFBK0I7QUFDM0I7QUFDQTNCLGtDQUFNd0MsSUFBTixJQUFjLElBQWQ7QUFDSCx5QkFIRCxNQUdPLElBQUl4QyxNQUFNd0MsSUFBTixFQUFZb0IsTUFBaEIsRUFBd0I7QUFDM0I7QUFDQTtBQUNBNUQsa0NBQU13QyxJQUFOLEVBQVk4QyxLQUFaO0FBQ0EsbUNBQU90RixNQUFNd0MsSUFBTixFQUFZb0IsTUFBbkI7QUFDQTVELGtDQUFNd0MsSUFBTixJQUFjLElBQWQ7QUFDSDtBQUNKLHFCQVhELE1BV08sSUFBSUEsS0FBS2tDLE1BQUwsSUFBZTFFLE1BQU13QyxJQUFOLENBQW5CLEVBQWdDO0FBQ25DakMsbUNBQVcyQyxlQUFlO0FBQ3RCRSxtQ0FBT0wsS0FBS0gsQ0FBTCxDQURlO0FBRXRCSixrQ0FBTUEsSUFGZ0I7QUFHdEJHLGtDQUFNQTtBQUhnQix5QkFBZixDQUFYLEVBSUksQ0FKSjtBQUtIO0FBQ0o7QUFDSjtBQUNESSxtQkFBTyxFQUFQO0FBQ0FGLCtCQUFtQixDQUFuQjtBQUNBSTtBQUNILFNBeklEO0FBMElBLFlBQUlzQyxRQUFKO0FBQ0FBLG1CQUFVLG1CQUFZO0FBQ2xCekYsbUJBQU9xQixtQkFBUCxDQUEyQixNQUEzQixFQUFtQ29FLFFBQW5DLEVBQTRDLEtBQTVDLEVBRGtCLENBQ2tDO0FBQ3BEbkYsa0JBQU1HLFdBQVdKLGFBQVgsRUFBMEIsQ0FBMUIsQ0FBTjtBQUNILFNBSEQ7QUFJQSxZQUFJVSxTQUFTMkUsVUFBVCxLQUF3QixVQUE1QixFQUF3QztBQUNwQztBQUNBMUYsbUJBQU9DLGdCQUFQLENBQXdCLE1BQXhCLEVBQWdDd0YsUUFBaEMsRUFBeUMsS0FBekM7QUFDSCxTQUhELE1BR087QUFDSDtBQUNBQTtBQUNIO0FBQ0o7QUFDSixDQTdOQSxHQUFEIiwiZmlsZSI6InZlbmRvci9zdmd4dXNlLmpzIiwic291cmNlc0NvbnRlbnQiOlsiLyohXG4gKiBAY29weXJpZ2h0IENvcHlyaWdodCAoYykgMjAxNyBJY29Nb29uLmlvXG4gKiBAbGljZW5zZSAgIExpY2Vuc2VkIHVuZGVyIE1JVCBsaWNlbnNlXG4gKiAgICAgICAgICAgIFNlZSBodHRwczovL2dpdGh1Yi5jb20vS2V5YW1vb24vc3ZneHVzZVxuICogQHZlcnNpb24gICAxLjIuNlxuICovXG4vKmpzbGludCBicm93c2VyOiB0cnVlICovXG4vKmdsb2JhbCBYRG9tYWluUmVxdWVzdCwgTXV0YXRpb25PYnNlcnZlciwgd2luZG93ICovXG4oZnVuY3Rpb24gKCkge1xuICAgIFwidXNlIHN0cmljdFwiO1xuICAgIGlmICh0eXBlb2Ygd2luZG93ICE9PSBcInVuZGVmaW5lZFwiICYmIHdpbmRvdy5hZGRFdmVudExpc3RlbmVyKSB7XG4gICAgICAgIHZhciBjYWNoZSA9IE9iamVjdC5jcmVhdGUobnVsbCk7IC8vIGhvbGRzIHhociBvYmplY3RzIHRvIHByZXZlbnQgbXVsdGlwbGUgcmVxdWVzdHNcbiAgICAgICAgdmFyIGNoZWNrVXNlRWxlbXM7XG4gICAgICAgIHZhciB0aWQ7IC8vIHRpbWVvdXQgaWRcbiAgICAgICAgdmFyIGRlYm91bmNlZENoZWNrID0gZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgY2xlYXJUaW1lb3V0KHRpZCk7XG4gICAgICAgICAgICB0aWQgPSBzZXRUaW1lb3V0KGNoZWNrVXNlRWxlbXMsIDEwMCk7XG4gICAgICAgIH07XG4gICAgICAgIHZhciB1bm9ic2VydmVDaGFuZ2VzID0gZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgcmV0dXJuO1xuICAgICAgICB9O1xuICAgICAgICB2YXIgb2JzZXJ2ZUNoYW5nZXMgPSBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICB2YXIgb2JzZXJ2ZXI7XG4gICAgICAgICAgICB3aW5kb3cuYWRkRXZlbnRMaXN0ZW5lcihcInJlc2l6ZVwiLCBkZWJvdW5jZWRDaGVjaywgZmFsc2UpO1xuICAgICAgICAgICAgd2luZG93LmFkZEV2ZW50TGlzdGVuZXIoXCJvcmllbnRhdGlvbmNoYW5nZVwiLCBkZWJvdW5jZWRDaGVjaywgZmFsc2UpO1xuICAgICAgICAgICAgaWYgKHdpbmRvdy5NdXRhdGlvbk9ic2VydmVyKSB7XG4gICAgICAgICAgICAgICAgb2JzZXJ2ZXIgPSBuZXcgTXV0YXRpb25PYnNlcnZlcihkZWJvdW5jZWRDaGVjayk7XG4gICAgICAgICAgICAgICAgb2JzZXJ2ZXIub2JzZXJ2ZShkb2N1bWVudC5kb2N1bWVudEVsZW1lbnQsIHtcbiAgICAgICAgICAgICAgICAgICAgY2hpbGRMaXN0OiB0cnVlLFxuICAgICAgICAgICAgICAgICAgICBzdWJ0cmVlOiB0cnVlLFxuICAgICAgICAgICAgICAgICAgICBhdHRyaWJ1dGVzOiB0cnVlXG4gICAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICAgICAgdW5vYnNlcnZlQ2hhbmdlcyA9IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgICAgICAgICAgdHJ5IHtcbiAgICAgICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLmRpc2Nvbm5lY3QoKTtcbiAgICAgICAgICAgICAgICAgICAgICAgIHdpbmRvdy5yZW1vdmVFdmVudExpc3RlbmVyKFwicmVzaXplXCIsIGRlYm91bmNlZENoZWNrLCBmYWxzZSk7XG4gICAgICAgICAgICAgICAgICAgICAgICB3aW5kb3cucmVtb3ZlRXZlbnRMaXN0ZW5lcihcIm9yaWVudGF0aW9uY2hhbmdlXCIsIGRlYm91bmNlZENoZWNrLCBmYWxzZSk7XG4gICAgICAgICAgICAgICAgICAgIH0gY2F0Y2ggKGlnbm9yZSkge31cbiAgICAgICAgICAgICAgICB9O1xuICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICBkb2N1bWVudC5kb2N1bWVudEVsZW1lbnQuYWRkRXZlbnRMaXN0ZW5lcihcIkRPTVN1YnRyZWVNb2RpZmllZFwiLCBkZWJvdW5jZWRDaGVjaywgZmFsc2UpO1xuICAgICAgICAgICAgICAgIHVub2JzZXJ2ZUNoYW5nZXMgPSBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICAgICAgICAgIGRvY3VtZW50LmRvY3VtZW50RWxlbWVudC5yZW1vdmVFdmVudExpc3RlbmVyKFwiRE9NU3VidHJlZU1vZGlmaWVkXCIsIGRlYm91bmNlZENoZWNrLCBmYWxzZSk7XG4gICAgICAgICAgICAgICAgICAgIHdpbmRvdy5yZW1vdmVFdmVudExpc3RlbmVyKFwicmVzaXplXCIsIGRlYm91bmNlZENoZWNrLCBmYWxzZSk7XG4gICAgICAgICAgICAgICAgICAgIHdpbmRvdy5yZW1vdmVFdmVudExpc3RlbmVyKFwib3JpZW50YXRpb25jaGFuZ2VcIiwgZGVib3VuY2VkQ2hlY2ssIGZhbHNlKTtcbiAgICAgICAgICAgICAgICB9O1xuICAgICAgICAgICAgfVxuICAgICAgICB9O1xuICAgICAgICB2YXIgY3JlYXRlUmVxdWVzdCA9IGZ1bmN0aW9uICh1cmwpIHtcbiAgICAgICAgICAgIC8vIEluIElFIDksIGNyb3NzIG9yaWdpbiByZXF1ZXN0cyBjYW4gb25seSBiZSBzZW50IHVzaW5nIFhEb21haW5SZXF1ZXN0LlxuICAgICAgICAgICAgLy8gWERvbWFpblJlcXVlc3Qgd291bGQgZmFpbCBpZiBDT1JTIGhlYWRlcnMgYXJlIG5vdCBzZXQuXG4gICAgICAgICAgICAvLyBUaGVyZWZvcmUsIFhEb21haW5SZXF1ZXN0IHNob3VsZCBvbmx5IGJlIHVzZWQgd2l0aCBjcm9zcyBvcmlnaW4gcmVxdWVzdHMuXG4gICAgICAgICAgICBmdW5jdGlvbiBnZXRPcmlnaW4obG9jKSB7XG4gICAgICAgICAgICAgICAgdmFyIGE7XG4gICAgICAgICAgICAgICAgaWYgKGxvYy5wcm90b2NvbCAhPT0gdW5kZWZpbmVkKSB7XG4gICAgICAgICAgICAgICAgICAgIGEgPSBsb2M7XG4gICAgICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICAgICAgYSA9IGRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoXCJhXCIpO1xuICAgICAgICAgICAgICAgICAgICBhLmhyZWYgPSBsb2M7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIHJldHVybiBhLnByb3RvY29sLnJlcGxhY2UoLzovZywgXCJcIikgKyBhLmhvc3Q7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICB2YXIgUmVxdWVzdDtcbiAgICAgICAgICAgIHZhciBvcmlnaW47XG4gICAgICAgICAgICB2YXIgb3JpZ2luMjtcbiAgICAgICAgICAgIGlmICh3aW5kb3cuWE1MSHR0cFJlcXVlc3QpIHtcbiAgICAgICAgICAgICAgICBSZXF1ZXN0ID0gbmV3IFhNTEh0dHBSZXF1ZXN0KCk7XG4gICAgICAgICAgICAgICAgb3JpZ2luID0gZ2V0T3JpZ2luKGxvY2F0aW9uKTtcbiAgICAgICAgICAgICAgICBvcmlnaW4yID0gZ2V0T3JpZ2luKHVybCk7XG4gICAgICAgICAgICAgICAgaWYgKFJlcXVlc3Qud2l0aENyZWRlbnRpYWxzID09PSB1bmRlZmluZWQgJiYgb3JpZ2luMiAhPT0gXCJcIiAmJiBvcmlnaW4yICE9PSBvcmlnaW4pIHtcbiAgICAgICAgICAgICAgICAgICAgUmVxdWVzdCA9IFhEb21haW5SZXF1ZXN0IHx8IHVuZGVmaW5lZDtcbiAgICAgICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgICAgICBSZXF1ZXN0ID0gWE1MSHR0cFJlcXVlc3Q7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfVxuICAgICAgICAgICAgcmV0dXJuIFJlcXVlc3Q7XG4gICAgICAgIH07XG4gICAgICAgIHZhciB4bGlua05TID0gXCJodHRwOi8vd3d3LnczLm9yZy8xOTk5L3hsaW5rXCI7XG4gICAgICAgIGNoZWNrVXNlRWxlbXMgPSBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICB2YXIgYmFzZTtcbiAgICAgICAgICAgIHZhciBiY3I7XG4gICAgICAgICAgICB2YXIgZmFsbGJhY2sgPSBcIlwiOyAvLyBvcHRpb25hbCBmYWxsYmFjayBVUkwgaW4gY2FzZSBubyBiYXNlIHBhdGggdG8gU1ZHIGZpbGUgd2FzIGdpdmVuIGFuZCBubyBzeW1ib2wgZGVmaW5pdGlvbiB3YXMgZm91bmQuXG4gICAgICAgICAgICB2YXIgaGFzaDtcbiAgICAgICAgICAgIHZhciBocmVmO1xuICAgICAgICAgICAgdmFyIGk7XG4gICAgICAgICAgICB2YXIgaW5Qcm9ncmVzc0NvdW50ID0gMDtcbiAgICAgICAgICAgIHZhciBpc0hpZGRlbjtcbiAgICAgICAgICAgIHZhciBSZXF1ZXN0O1xuICAgICAgICAgICAgdmFyIHVybDtcbiAgICAgICAgICAgIHZhciB1c2VzO1xuICAgICAgICAgICAgdmFyIHhocjtcbiAgICAgICAgICAgIGZ1bmN0aW9uIG9ic2VydmVJZkRvbmUoKSB7XG4gICAgICAgICAgICAgICAgLy8gSWYgZG9uZSB3aXRoIG1ha2luZyBjaGFuZ2VzLCBzdGFydCB3YXRjaGluZyBmb3IgY2hhZ25lcyBpbiBET00gYWdhaW5cbiAgICAgICAgICAgICAgICBpblByb2dyZXNzQ291bnQgLT0gMTtcbiAgICAgICAgICAgICAgICBpZiAoaW5Qcm9ncmVzc0NvdW50ID09PSAwKSB7IC8vIGlmIGFsbCB4aHJzIHdlcmUgcmVzb2x2ZWRcbiAgICAgICAgICAgICAgICAgICAgdW5vYnNlcnZlQ2hhbmdlcygpOyAvLyBtYWtlIHN1cmUgdG8gcmVtb3ZlIG9sZCBoYW5kbGVyc1xuICAgICAgICAgICAgICAgICAgICBvYnNlcnZlQ2hhbmdlcygpOyAvLyB3YXRjaCBmb3IgY2hhbmdlcyB0byBET01cbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBmdW5jdGlvbiBhdHRyVXBkYXRlRnVuYyhzcGVjKSB7XG4gICAgICAgICAgICAgICAgcmV0dXJuIGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgICAgICAgICAgaWYgKGNhY2hlW3NwZWMuYmFzZV0gIT09IHRydWUpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIHNwZWMudXNlRWwuc2V0QXR0cmlidXRlTlMoeGxpbmtOUywgXCJ4bGluazpocmVmXCIsIFwiI1wiICsgc3BlYy5oYXNoKTtcbiAgICAgICAgICAgICAgICAgICAgICAgIGlmIChzcGVjLnVzZUVsLmhhc0F0dHJpYnV0ZShcImhyZWZcIikpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBzcGVjLnVzZUVsLnNldEF0dHJpYnV0ZShcImhyZWZcIiwgXCIjXCIgKyBzcGVjLmhhc2gpO1xuICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgfTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGZ1bmN0aW9uIG9ubG9hZEZ1bmMoeGhyKSB7XG4gICAgICAgICAgICAgICAgcmV0dXJuIGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgICAgICAgICAgdmFyIGJvZHkgPSBkb2N1bWVudC5ib2R5O1xuICAgICAgICAgICAgICAgICAgICB2YXIgeCA9IGRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoXCJ4XCIpO1xuICAgICAgICAgICAgICAgICAgICB2YXIgc3ZnO1xuICAgICAgICAgICAgICAgICAgICB4aHIub25sb2FkID0gbnVsbDtcbiAgICAgICAgICAgICAgICAgICAgeC5pbm5lckhUTUwgPSB4aHIucmVzcG9uc2VUZXh0O1xuICAgICAgICAgICAgICAgICAgICBzdmcgPSB4LmdldEVsZW1lbnRzQnlUYWdOYW1lKFwic3ZnXCIpWzBdO1xuICAgICAgICAgICAgICAgICAgICBpZiAoc3ZnKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICBzdmcuc2V0QXR0cmlidXRlKFwiYXJpYS1oaWRkZW5cIiwgXCJ0cnVlXCIpO1xuICAgICAgICAgICAgICAgICAgICAgICAgc3ZnLnN0eWxlLnBvc2l0aW9uID0gXCJhYnNvbHV0ZVwiO1xuICAgICAgICAgICAgICAgICAgICAgICAgc3ZnLnN0eWxlLndpZHRoID0gMDtcbiAgICAgICAgICAgICAgICAgICAgICAgIHN2Zy5zdHlsZS5oZWlnaHQgPSAwO1xuICAgICAgICAgICAgICAgICAgICAgICAgc3ZnLnN0eWxlLm92ZXJmbG93ID0gXCJoaWRkZW5cIjtcbiAgICAgICAgICAgICAgICAgICAgICAgIGJvZHkuaW5zZXJ0QmVmb3JlKHN2ZywgYm9keS5maXJzdENoaWxkKTtcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICBvYnNlcnZlSWZEb25lKCk7XG4gICAgICAgICAgICAgICAgfTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGZ1bmN0aW9uIG9uRXJyb3JUaW1lb3V0KHhocikge1xuICAgICAgICAgICAgICAgIHJldHVybiBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICAgICAgICAgIHhoci5vbmVycm9yID0gbnVsbDtcbiAgICAgICAgICAgICAgICAgICAgeGhyLm9udGltZW91dCA9IG51bGw7XG4gICAgICAgICAgICAgICAgICAgIG9ic2VydmVJZkRvbmUoKTtcbiAgICAgICAgICAgICAgICB9O1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgdW5vYnNlcnZlQ2hhbmdlcygpOyAvLyBzdG9wIHdhdGNoaW5nIGZvciBjaGFuZ2VzIHRvIERPTVxuICAgICAgICAgICAgLy8gZmluZCBhbGwgdXNlIGVsZW1lbnRzXG4gICAgICAgICAgICB1c2VzID0gZG9jdW1lbnQuZ2V0RWxlbWVudHNCeVRhZ05hbWUoXCJ1c2VcIik7XG4gICAgICAgICAgICBmb3IgKGkgPSAwOyBpIDwgdXNlcy5sZW5ndGg7IGkgKz0gMSkge1xuICAgICAgICAgICAgICAgIHRyeSB7XG4gICAgICAgICAgICAgICAgICAgIGJjciA9IHVzZXNbaV0uZ2V0Qm91bmRpbmdDbGllbnRSZWN0KCk7XG4gICAgICAgICAgICAgICAgfSBjYXRjaCAoaWdub3JlKSB7XG4gICAgICAgICAgICAgICAgICAgIC8vIGZhaWxlZCB0byBnZXQgYm91bmRpbmcgcmVjdGFuZ2xlIG9mIHRoZSB1c2UgZWxlbWVudFxuICAgICAgICAgICAgICAgICAgICBiY3IgPSBmYWxzZTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgaHJlZiA9IHVzZXNbaV0uZ2V0QXR0cmlidXRlKFwiaHJlZlwiKVxuICAgICAgICAgICAgICAgICAgICAgICAgfHwgdXNlc1tpXS5nZXRBdHRyaWJ1dGVOUyh4bGlua05TLCBcImhyZWZcIilcbiAgICAgICAgICAgICAgICAgICAgICAgIHx8IHVzZXNbaV0uZ2V0QXR0cmlidXRlKFwieGxpbms6aHJlZlwiKTtcbiAgICAgICAgICAgICAgICBpZiAoaHJlZiAmJiBocmVmLnNwbGl0KSB7XG4gICAgICAgICAgICAgICAgICAgIHVybCA9IGhyZWYuc3BsaXQoXCIjXCIpO1xuICAgICAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgICAgIHVybCA9IFtcIlwiLCBcIlwiXTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgYmFzZSA9IHVybFswXTtcbiAgICAgICAgICAgICAgICBoYXNoID0gdXJsWzFdO1xuICAgICAgICAgICAgICAgIGlzSGlkZGVuID0gYmNyICYmIGJjci5sZWZ0ID09PSAwICYmIGJjci5yaWdodCA9PT0gMCAmJiBiY3IudG9wID09PSAwICYmIGJjci5ib3R0b20gPT09IDA7XG4gICAgICAgICAgICAgICAgaWYgKGJjciAmJiBiY3Iud2lkdGggPT09IDAgJiYgYmNyLmhlaWdodCA9PT0gMCAmJiAhaXNIaWRkZW4pIHtcbiAgICAgICAgICAgICAgICAgICAgLy8gdGhlIHVzZSBlbGVtZW50IGlzIGVtcHR5XG4gICAgICAgICAgICAgICAgICAgIC8vIGlmIHRoZXJlIGlzIGEgcmVmZXJlbmNlIHRvIGFuIGV4dGVybmFsIFNWRywgdHJ5IHRvIGZldGNoIGl0XG4gICAgICAgICAgICAgICAgICAgIC8vIHVzZSB0aGUgb3B0aW9uYWwgZmFsbGJhY2sgVVJMIGlmIHRoZXJlIGlzIG5vIHJlZmVyZW5jZSB0byBhbiBleHRlcm5hbCBTVkdcbiAgICAgICAgICAgICAgICAgICAgaWYgKGZhbGxiYWNrICYmICFiYXNlLmxlbmd0aCAmJiBoYXNoICYmICFkb2N1bWVudC5nZXRFbGVtZW50QnlJZChoYXNoKSkge1xuICAgICAgICAgICAgICAgICAgICAgICAgYmFzZSA9IGZhbGxiYWNrO1xuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgIGlmICh1c2VzW2ldLmhhc0F0dHJpYnV0ZShcImhyZWZcIikpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIHVzZXNbaV0uc2V0QXR0cmlidXRlTlMoeGxpbmtOUywgXCJ4bGluazpocmVmXCIsIGhyZWYpO1xuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgIGlmIChiYXNlLmxlbmd0aCkge1xuICAgICAgICAgICAgICAgICAgICAgICAgLy8gc2NoZWR1bGUgdXBkYXRpbmcgeGxpbms6aHJlZlxuICAgICAgICAgICAgICAgICAgICAgICAgeGhyID0gY2FjaGVbYmFzZV07XG4gICAgICAgICAgICAgICAgICAgICAgICBpZiAoeGhyICE9PSB0cnVlKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgLy8gdHJ1ZSBzaWduaWZpZXMgdGhhdCBwcmVwZW5kaW5nIHRoZSBTVkcgd2FzIG5vdCByZXF1aXJlZFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHNldFRpbWVvdXQoYXR0clVwZGF0ZUZ1bmMoe1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB1c2VFbDogdXNlc1tpXSxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgYmFzZTogYmFzZSxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgaGFzaDogaGFzaFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0pLCAwKTtcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgICAgIGlmICh4aHIgPT09IHVuZGVmaW5lZCkge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIFJlcXVlc3QgPSBjcmVhdGVSZXF1ZXN0KGJhc2UpO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlmIChSZXF1ZXN0ICE9PSB1bmRlZmluZWQpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgeGhyID0gbmV3IFJlcXVlc3QoKTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgY2FjaGVbYmFzZV0gPSB4aHI7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHhoci5vbmxvYWQgPSBvbmxvYWRGdW5jKHhocik7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHhoci5vbmVycm9yID0gb25FcnJvclRpbWVvdXQoeGhyKTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgeGhyLm9udGltZW91dCA9IG9uRXJyb3JUaW1lb3V0KHhocik7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHhoci5vcGVuKFwiR0VUXCIsIGJhc2UpO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB4aHIuc2VuZCgpO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBpblByb2dyZXNzQ291bnQgKz0gMTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgICAgICBpZiAoIWlzSGlkZGVuKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICBpZiAoY2FjaGVbYmFzZV0gPT09IHVuZGVmaW5lZCkge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIC8vIHJlbWVtYmVyIHRoaXMgVVJMIGlmIHRoZSB1c2UgZWxlbWVudCB3YXMgbm90IGVtcHR5IGFuZCBubyByZXF1ZXN0IHdhcyBzZW50XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgY2FjaGVbYmFzZV0gPSB0cnVlO1xuICAgICAgICAgICAgICAgICAgICAgICAgfSBlbHNlIGlmIChjYWNoZVtiYXNlXS5vbmxvYWQpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAvLyBpZiBpdCB0dXJucyBvdXQgdGhhdCBwcmVwZW5kaW5nIHRoZSBTVkcgaXMgbm90IG5lY2Vzc2FyeSxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAvLyBhYm9ydCB0aGUgaW4tcHJvZ3Jlc3MgeGhyLlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNhY2hlW2Jhc2VdLmFib3J0KCk7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgZGVsZXRlIGNhY2hlW2Jhc2VdLm9ubG9hZDtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBjYWNoZVtiYXNlXSA9IHRydWU7XG4gICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgIH0gZWxzZSBpZiAoYmFzZS5sZW5ndGggJiYgY2FjaGVbYmFzZV0pIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIHNldFRpbWVvdXQoYXR0clVwZGF0ZUZ1bmMoe1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHVzZUVsOiB1c2VzW2ldLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGJhc2U6IGJhc2UsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgaGFzaDogaGFzaFxuICAgICAgICAgICAgICAgICAgICAgICAgfSksIDApO1xuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfVxuICAgICAgICAgICAgdXNlcyA9IFwiXCI7XG4gICAgICAgICAgICBpblByb2dyZXNzQ291bnQgKz0gMTtcbiAgICAgICAgICAgIG9ic2VydmVJZkRvbmUoKTtcbiAgICAgICAgfTtcbiAgICAgICAgdmFyIHdpbkxvYWQ7XG4gICAgICAgIHdpbkxvYWQgPSBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICB3aW5kb3cucmVtb3ZlRXZlbnRMaXN0ZW5lcihcImxvYWRcIiwgd2luTG9hZCwgZmFsc2UpOyAvLyB0byBwcmV2ZW50IG1lbW9yeSBsZWFrc1xuICAgICAgICAgICAgdGlkID0gc2V0VGltZW91dChjaGVja1VzZUVsZW1zLCAwKTtcbiAgICAgICAgfTtcbiAgICAgICAgaWYgKGRvY3VtZW50LnJlYWR5U3RhdGUgIT09IFwiY29tcGxldGVcIikge1xuICAgICAgICAgICAgLy8gVGhlIGxvYWQgZXZlbnQgZmlyZXMgd2hlbiBhbGwgcmVzb3VyY2VzIGhhdmUgZmluaXNoZWQgbG9hZGluZywgd2hpY2ggYWxsb3dzIGRldGVjdGluZyB3aGV0aGVyIFNWRyB1c2UgZWxlbWVudHMgYXJlIGVtcHR5LlxuICAgICAgICAgICAgd2luZG93LmFkZEV2ZW50TGlzdGVuZXIoXCJsb2FkXCIsIHdpbkxvYWQsIGZhbHNlKTtcbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgIC8vIE5vIG5lZWQgdG8gYWRkIGEgbGlzdGVuZXIgaWYgdGhlIGRvY3VtZW50IGlzIGFscmVhZHkgbG9hZGVkLCBpbml0aWFsaXplIGltbWVkaWF0ZWx5LlxuICAgICAgICAgICAgd2luTG9hZCgpO1xuICAgICAgICB9XG4gICAgfVxufSgpKTtcbiJdfQ==
