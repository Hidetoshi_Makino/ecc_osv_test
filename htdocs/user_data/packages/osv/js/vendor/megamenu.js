'use strict';

/*global $ */
$(document).ready(function () {

	"use strict";

	$('.nav > ul > li:has( > ul)').addClass('nav-dropdown-icon');
	//Checks if li has sub (ul) and adds class for toggle icon - just an UI


	$('.nav > ul > li > ul:not(:has(ul))').addClass('normal-sub');
	//Checks if drodown menu's li elements have anothere level (ul), if not the dropdown is shown as regular dropdown, not a mega menu (thanks Luka Kladaric)

	// $(".nav > ul").before("<a href=\"#\" class=\"nav-mobile\">Navigation</a>");

	//Adds menu-mobile class (for mobile toggle menu) before the normal menu
	//Mobile menu is hidden if width is more then 959px, but normal menu is displayed
	//Normal menu is hidden if width is below 959px, and jquery adds mobile menu
	//Done this way so it can be used with wordpress without any trouble

	$(".nav > ul > li").hover(function (e) {
		if ($(window).width() > BREAK_POINT) {
			$(this).children("ul").fadeIn(150);
			e.preventDefault();
		}
	}, function (e) {
		if ($(window).width() > BREAK_POINT) {
			$(this).children("ul").fadeOut(150);
			e.preventDefault();
		}
	});
	//If width is more than 943px dropdowns are displayed on hover

	$(".nav > ul > li").click(function () {
		if ($(window).width() < BREAK_POINT) {
			$(this).children("ul").fadeToggle(150);
			$(this).children('.anchor').toggleClass('-opened');
		}
	});
	//If width is less or equal to 943px dropdowns are displayed on click (thanks Aman Jain from stackoverflow)

	// $(".nav-mobile").click(function (e) {
	// 	$(".nav > ul").toggleClass('show-on-mobile');
	// 	e.preventDefault();
	// });
	//when clicked on mobile-menu, normal menu is shown as a list, classic rwd menu story (thanks mwl from stackoverflow)
});
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInZlbmRvci9tZWdhbWVudS5qcyJdLCJuYW1lcyI6WyIkIiwiZG9jdW1lbnQiLCJyZWFkeSIsImFkZENsYXNzIiwiaG92ZXIiLCJlIiwid2luZG93Iiwid2lkdGgiLCJCUkVBS19QT0lOVCIsImNoaWxkcmVuIiwiZmFkZUluIiwicHJldmVudERlZmF1bHQiLCJmYWRlT3V0IiwiY2xpY2siLCJmYWRlVG9nZ2xlIiwidG9nZ2xlQ2xhc3MiXSwibWFwcGluZ3MiOiI7O0FBQUE7QUFDQUEsRUFBRUMsUUFBRixFQUFZQyxLQUFaLENBQWtCLFlBQVk7O0FBRTdCOztBQUVBRixHQUFFLDJCQUFGLEVBQStCRyxRQUEvQixDQUF3QyxtQkFBeEM7QUFDQTs7O0FBR0FILEdBQUUsbUNBQUYsRUFBdUNHLFFBQXZDLENBQWdELFlBQWhEO0FBQ0E7O0FBRUE7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUFILEdBQUUsZ0JBQUYsRUFBb0JJLEtBQXBCLENBQ0MsVUFBVUMsQ0FBVixFQUFhO0FBQ1osTUFBSUwsRUFBRU0sTUFBRixFQUFVQyxLQUFWLEtBQW9CQyxXQUF4QixFQUFxQztBQUNwQ1IsS0FBRSxJQUFGLEVBQVFTLFFBQVIsQ0FBaUIsSUFBakIsRUFBdUJDLE1BQXZCLENBQThCLEdBQTlCO0FBQ0FMLEtBQUVNLGNBQUY7QUFDQTtBQUNELEVBTkYsRUFNSSxVQUFVTixDQUFWLEVBQWE7QUFDZixNQUFJTCxFQUFFTSxNQUFGLEVBQVVDLEtBQVYsS0FBb0JDLFdBQXhCLEVBQXFDO0FBQ3BDUixLQUFFLElBQUYsRUFBUVMsUUFBUixDQUFpQixJQUFqQixFQUF1QkcsT0FBdkIsQ0FBK0IsR0FBL0I7QUFDQVAsS0FBRU0sY0FBRjtBQUNBO0FBQ0QsRUFYRjtBQWFBOztBQUVBWCxHQUFFLGdCQUFGLEVBQW9CYSxLQUFwQixDQUEwQixZQUFXO0FBQ3BDLE1BQUliLEVBQUVNLE1BQUYsRUFBVUMsS0FBVixLQUFvQkMsV0FBeEIsRUFBcUM7QUFDbkNSLEtBQUUsSUFBRixFQUFRUyxRQUFSLENBQWlCLElBQWpCLEVBQXVCSyxVQUF2QixDQUFrQyxHQUFsQztBQUNBZCxLQUFFLElBQUYsRUFBUVMsUUFBUixDQUFpQixTQUFqQixFQUE0Qk0sV0FBNUIsQ0FBd0MsU0FBeEM7QUFDRDtBQUNELEVBTEQ7QUFNQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUEsQ0EvQ0QiLCJmaWxlIjoidmVuZG9yL21lZ2FtZW51LmpzIiwic291cmNlc0NvbnRlbnQiOlsiLypnbG9iYWwgJCAqL1xuJChkb2N1bWVudCkucmVhZHkoZnVuY3Rpb24gKCkge1xuXG5cdFwidXNlIHN0cmljdFwiO1xuXG5cdCQoJy5uYXYgPiB1bCA+IGxpOmhhcyggPiB1bCknKS5hZGRDbGFzcygnbmF2LWRyb3Bkb3duLWljb24nKTtcblx0Ly9DaGVja3MgaWYgbGkgaGFzIHN1YiAodWwpIGFuZCBhZGRzIGNsYXNzIGZvciB0b2dnbGUgaWNvbiAtIGp1c3QgYW4gVUlcblxuXG5cdCQoJy5uYXYgPiB1bCA+IGxpID4gdWw6bm90KDpoYXModWwpKScpLmFkZENsYXNzKCdub3JtYWwtc3ViJyk7XG5cdC8vQ2hlY2tzIGlmIGRyb2Rvd24gbWVudSdzIGxpIGVsZW1lbnRzIGhhdmUgYW5vdGhlcmUgbGV2ZWwgKHVsKSwgaWYgbm90IHRoZSBkcm9wZG93biBpcyBzaG93biBhcyByZWd1bGFyIGRyb3Bkb3duLCBub3QgYSBtZWdhIG1lbnUgKHRoYW5rcyBMdWthIEtsYWRhcmljKVxuXG5cdC8vICQoXCIubmF2ID4gdWxcIikuYmVmb3JlKFwiPGEgaHJlZj1cXFwiI1xcXCIgY2xhc3M9XFxcIm5hdi1tb2JpbGVcXFwiPk5hdmlnYXRpb248L2E+XCIpO1xuXG5cdC8vQWRkcyBtZW51LW1vYmlsZSBjbGFzcyAoZm9yIG1vYmlsZSB0b2dnbGUgbWVudSkgYmVmb3JlIHRoZSBub3JtYWwgbWVudVxuXHQvL01vYmlsZSBtZW51IGlzIGhpZGRlbiBpZiB3aWR0aCBpcyBtb3JlIHRoZW4gOTU5cHgsIGJ1dCBub3JtYWwgbWVudSBpcyBkaXNwbGF5ZWRcblx0Ly9Ob3JtYWwgbWVudSBpcyBoaWRkZW4gaWYgd2lkdGggaXMgYmVsb3cgOTU5cHgsIGFuZCBqcXVlcnkgYWRkcyBtb2JpbGUgbWVudVxuXHQvL0RvbmUgdGhpcyB3YXkgc28gaXQgY2FuIGJlIHVzZWQgd2l0aCB3b3JkcHJlc3Mgd2l0aG91dCBhbnkgdHJvdWJsZVxuXG5cdCQoXCIubmF2ID4gdWwgPiBsaVwiKS5ob3Zlcihcblx0XHRmdW5jdGlvbiAoZSkge1xuXHRcdFx0aWYgKCQod2luZG93KS53aWR0aCgpID4gQlJFQUtfUE9JTlQpIHtcblx0XHRcdFx0JCh0aGlzKS5jaGlsZHJlbihcInVsXCIpLmZhZGVJbigxNTApO1xuXHRcdFx0XHRlLnByZXZlbnREZWZhdWx0KCk7XG5cdFx0XHR9XG5cdFx0fSwgZnVuY3Rpb24gKGUpIHtcblx0XHRcdGlmICgkKHdpbmRvdykud2lkdGgoKSA+IEJSRUFLX1BPSU5UKSB7XG5cdFx0XHRcdCQodGhpcykuY2hpbGRyZW4oXCJ1bFwiKS5mYWRlT3V0KDE1MCk7XG5cdFx0XHRcdGUucHJldmVudERlZmF1bHQoKTtcblx0XHRcdH1cblx0XHR9XG5cdCk7XG5cdC8vSWYgd2lkdGggaXMgbW9yZSB0aGFuIDk0M3B4IGRyb3Bkb3ducyBhcmUgZGlzcGxheWVkIG9uIGhvdmVyXG5cblx0JChcIi5uYXYgPiB1bCA+IGxpXCIpLmNsaWNrKGZ1bmN0aW9uKCkge1xuXHRcdGlmICgkKHdpbmRvdykud2lkdGgoKSA8IEJSRUFLX1BPSU5UKSB7XG5cdFx0ICAkKHRoaXMpLmNoaWxkcmVuKFwidWxcIikuZmFkZVRvZ2dsZSgxNTApO1xuXHRcdCAgJCh0aGlzKS5jaGlsZHJlbignLmFuY2hvcicpLnRvZ2dsZUNsYXNzKCctb3BlbmVkJyk7XG5cdFx0fVxuXHR9KTtcblx0Ly9JZiB3aWR0aCBpcyBsZXNzIG9yIGVxdWFsIHRvIDk0M3B4IGRyb3Bkb3ducyBhcmUgZGlzcGxheWVkIG9uIGNsaWNrICh0aGFua3MgQW1hbiBKYWluIGZyb20gc3RhY2tvdmVyZmxvdylcblxuXHQvLyAkKFwiLm5hdi1tb2JpbGVcIikuY2xpY2soZnVuY3Rpb24gKGUpIHtcblx0Ly8gXHQkKFwiLm5hdiA+IHVsXCIpLnRvZ2dsZUNsYXNzKCdzaG93LW9uLW1vYmlsZScpO1xuXHQvLyBcdGUucHJldmVudERlZmF1bHQoKTtcblx0Ly8gfSk7XG5cdC8vd2hlbiBjbGlja2VkIG9uIG1vYmlsZS1tZW51LCBub3JtYWwgbWVudSBpcyBzaG93biBhcyBhIGxpc3QsIGNsYXNzaWMgcndkIG1lbnUgc3RvcnkgKHRoYW5rcyBtd2wgZnJvbSBzdGFja292ZXJmbG93KVxuXG59KTsiXX0=
