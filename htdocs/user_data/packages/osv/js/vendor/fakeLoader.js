'use strict';

/*--------------------------------------------------------------------
 *JAVASCRIPT "FakeLoader.js"
 *Version:    1.1.0 - 2014
 *author:     João Pereira
 *website:    http://www.joaopereira.pt
 *Licensed MIT 
-----------------------------------------------------------------------*/
(function ($) {

    $.fn.fakeLoader = function (options) {

        //Defaults
        var settings = $.extend({
            timeToHide: 1200, // Default Time to hide fakeLoader
            pos: 'fixed', // Default Position
            top: '0px', // Default Top value
            left: '0px', // Default Left value
            width: '100%', // Default width 
            height: '100%', // Default Height
            zIndex: '999', // Default zIndex 
            bgColor: '#efefef', // Default background color
            spinner: 'spinner1', // Default Spinner
            imagePath: '' // Default Path custom image
        }, options);

        //Customized Spinners
        var spinner01 = '<div class="fl spinner1"><div class="double-bounce1"></div><div class="double-bounce2"></div></div>';
        // var spinner02 = '<div class="fl spinner2"><div class="spinner-container container1"><div class="circle1"></div><div class="circle2"></div><div class="circle3"></div><div class="circle4"></div></div><div class="spinner-container container2"><div class="circle1"></div><div class="circle2"></div><div class="circle3"></div><div class="circle4"></div></div><div class="spinner-container container3"><div class="circle1"></div><div class="circle2"></div><div class="circle3"></div><div class="circle4"></div></div></div>';
        // var spinner03 = '<div class="fl spinner3"><div class="dot1"></div><div class="dot2"></div></div>';
        // var spinner04 = '<div class="fl spinner4"></div>'; 
        // var spinner05 = '<div class="fl spinner5"><div class="cube1"></div><div class="cube2"></div></div>'; 
        // var spinner06 = '<div class="fl spinner6"><div class="rect1"></div><div class="rect2"></div><div class="rect3"></div><div class="rect4"></div><div class="rect5"></div></div>'; 
        // var spinner07 = '<div class="fl spinner7"><div class="circ1"></div><div class="circ2"></div><div class="circ3"></div><div class="circ4"></div></div>'; 

        //The target
        var el = $(this);

        //Init styles
        var initStyles = {
            'position': settings.pos,
            'width': settings.width,
            'height': settings.height,
            'top': settings.top,
            'left': settings.left
        };

        //Apply styles
        el.css(initStyles);

        //Each 
        el.each(function () {
            el.html(spinner01);
            // var a = settings.spinner;
            // switch (a) {
            //     case 'spinner1':
            //             el.html(spinner01);
            //         break;
            // case 'spinner2':
            //         el.html(spinner02);
            //     break;
            // case 'spinner3':
            //         el.html(spinner03);
            //     break;
            // case 'spinner4':
            //         el.html(spinner04);
            //     break;
            // case 'spinner5':
            //         el.html(spinner05);
            //     break;
            // case 'spinner6':
            //         el.html(spinner06);
            //     break;
            // case 'spinner7':
            //         el.html(spinner07);
            //     break;
            // default:
            //     el.html(spinner01);
            // }

            //Add customized loader image

            if (settings.imagePath != '') {
                el.html('<div class="fl"><img src="' + settings.imagePath + '"></div>');
                centerLoader();
            }
        });

        //Time to hide fakeLoader
        setTimeout(function () {
            $(el).fadeOut();
        }, settings.timeToHide);

        //Return Styles 
        return this.css({
            'backgroundColor': settings.bgColor,
            'zIndex': settings.zIndex
        });
    }; // End Fake Loader


    //Center Spinner
    function centerLoader() {

        var winW = $(window).width();
        var winH = $(window).height();

        var spinnerW = $('.fl').outerWidth();
        var spinnerH = $('.fl').outerHeight();

        $('.fl').css({
            'position': 'absolute',
            'left': winW / 2 - spinnerW / 2,
            'top': winH / 2 - spinnerH / 2
        });
    }

    $(window).load(function () {
        centerLoader();
        $(window).resize(function () {
            centerLoader();
        });
    });
})(jQuery);
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInZlbmRvci9mYWtlTG9hZGVyLmpzIl0sIm5hbWVzIjpbIiQiLCJmbiIsImZha2VMb2FkZXIiLCJvcHRpb25zIiwic2V0dGluZ3MiLCJleHRlbmQiLCJ0aW1lVG9IaWRlIiwicG9zIiwidG9wIiwibGVmdCIsIndpZHRoIiwiaGVpZ2h0IiwiekluZGV4IiwiYmdDb2xvciIsInNwaW5uZXIiLCJpbWFnZVBhdGgiLCJzcGlubmVyMDEiLCJlbCIsImluaXRTdHlsZXMiLCJjc3MiLCJlYWNoIiwiaHRtbCIsImNlbnRlckxvYWRlciIsInNldFRpbWVvdXQiLCJmYWRlT3V0Iiwid2luVyIsIndpbmRvdyIsIndpbkgiLCJzcGlubmVyVyIsIm91dGVyV2lkdGgiLCJzcGlubmVySCIsIm91dGVySGVpZ2h0IiwibG9hZCIsInJlc2l6ZSIsImpRdWVyeSJdLCJtYXBwaW5ncyI6Ijs7QUFBQTs7Ozs7OztBQU9DLFdBQVVBLENBQVYsRUFBYTs7QUFFVkEsTUFBRUMsRUFBRixDQUFLQyxVQUFMLEdBQWtCLFVBQVNDLE9BQVQsRUFBa0I7O0FBRWhDO0FBQ0EsWUFBSUMsV0FBV0osRUFBRUssTUFBRixDQUFTO0FBQ3BCQyx3QkFBVyxJQURTLEVBQ0g7QUFDakJDLGlCQUFJLE9BRmdCLEVBRVI7QUFDWkMsaUJBQUksS0FIZ0IsRUFHUjtBQUNaQyxrQkFBSyxLQUplLEVBSVI7QUFDWkMsbUJBQU0sTUFMYyxFQUtOO0FBQ2RDLG9CQUFPLE1BTmEsRUFNTDtBQUNmQyxvQkFBUSxLQVBZLEVBT0o7QUFDaEJDLHFCQUFTLFNBUlcsRUFRQTtBQUNwQkMscUJBQVEsVUFUWSxFQVNBO0FBQ3BCQyx1QkFBVSxFQVZVLENBVVA7QUFWTyxTQUFULEVBV1paLE9BWFksQ0FBZjs7QUFhQTtBQUNBLFlBQUlhLFlBQVkscUdBQWhCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0EsWUFBSUMsS0FBS2pCLEVBQUUsSUFBRixDQUFUOztBQUVBO0FBQ0EsWUFBSWtCLGFBQWE7QUFDYix3QkFBV2QsU0FBU0csR0FEUDtBQUViLHFCQUFRSCxTQUFTTSxLQUZKO0FBR2Isc0JBQVNOLFNBQVNPLE1BSEw7QUFJYixtQkFBTVAsU0FBU0ksR0FKRjtBQUtiLG9CQUFPSixTQUFTSztBQUxILFNBQWpCOztBQVFBO0FBQ0FRLFdBQUdFLEdBQUgsQ0FBT0QsVUFBUDs7QUFFQTtBQUNBRCxXQUFHRyxJQUFILENBQVEsWUFBVztBQUNkSCxlQUFHSSxJQUFILENBQVFMLFNBQVI7QUFDRDtBQUNJO0FBQ0E7QUFDQTtBQUNBO0FBQ0k7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVKOztBQUVBLGdCQUFJWixTQUFTVyxTQUFULElBQXFCLEVBQXpCLEVBQTZCO0FBQ3pCRSxtQkFBR0ksSUFBSCxDQUFRLCtCQUE2QmpCLFNBQVNXLFNBQXRDLEdBQWdELFVBQXhEO0FBQ0FPO0FBQ0g7QUFDUixTQW5DRDs7QUFxQ0E7QUFDQUMsbUJBQVcsWUFBVTtBQUNqQnZCLGNBQUVpQixFQUFGLEVBQU1PLE9BQU47QUFDSCxTQUZELEVBRUdwQixTQUFTRSxVQUZaOztBQUlBO0FBQ0EsZUFBTyxLQUFLYSxHQUFMLENBQVM7QUFDWiwrQkFBa0JmLFNBQVNTLE9BRGY7QUFFWixzQkFBU1QsU0FBU1E7QUFGTixTQUFULENBQVA7QUFNSCxLQTFGRCxDQUZVLENBNEZQOzs7QUFHQztBQUNBLGFBQVNVLFlBQVQsR0FBd0I7O0FBRXBCLFlBQUlHLE9BQU96QixFQUFFMEIsTUFBRixFQUFVaEIsS0FBVixFQUFYO0FBQ0EsWUFBSWlCLE9BQU8zQixFQUFFMEIsTUFBRixFQUFVZixNQUFWLEVBQVg7O0FBRUEsWUFBSWlCLFdBQVc1QixFQUFFLEtBQUYsRUFBUzZCLFVBQVQsRUFBZjtBQUNBLFlBQUlDLFdBQVc5QixFQUFFLEtBQUYsRUFBUytCLFdBQVQsRUFBZjs7QUFFQS9CLFVBQUUsS0FBRixFQUFTbUIsR0FBVCxDQUFhO0FBQ1Qsd0JBQVcsVUFERjtBQUVULG9CQUFRTSxPQUFLLENBQU4sR0FBVUcsV0FBUyxDQUZqQjtBQUdULG1CQUFPRCxPQUFLLENBQU4sR0FBVUcsV0FBUztBQUhoQixTQUFiO0FBTUg7O0FBRUQ5QixNQUFFMEIsTUFBRixFQUFVTSxJQUFWLENBQWUsWUFBVTtBQUNyQlY7QUFDQXRCLFVBQUUwQixNQUFGLEVBQVVPLE1BQVYsQ0FBaUIsWUFBVTtBQUN2Qlg7QUFDSCxTQUZEO0FBR0gsS0FMRDtBQVFQLENBeEhBLEVBd0hDWSxNQXhIRCxDQUFEIiwiZmlsZSI6InZlbmRvci9mYWtlTG9hZGVyLmpzIiwic291cmNlc0NvbnRlbnQiOlsiLyotLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuICpKQVZBU0NSSVBUIFwiRmFrZUxvYWRlci5qc1wiXG4gKlZlcnNpb246ICAgIDEuMS4wIC0gMjAxNFxuICphdXRob3I6ICAgICBKb8OjbyBQZXJlaXJhXG4gKndlYnNpdGU6ICAgIGh0dHA6Ly93d3cuam9hb3BlcmVpcmEucHRcbiAqTGljZW5zZWQgTUlUIFxuLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0qL1xuKGZ1bmN0aW9uICgkKSB7XG4gXG4gICAgJC5mbi5mYWtlTG9hZGVyID0gZnVuY3Rpb24ob3B0aW9ucykge1xuXG4gICAgICAgIC8vRGVmYXVsdHNcbiAgICAgICAgdmFyIHNldHRpbmdzID0gJC5leHRlbmQoe1xuICAgICAgICAgICAgdGltZVRvSGlkZToxMjAwLCAvLyBEZWZhdWx0IFRpbWUgdG8gaGlkZSBmYWtlTG9hZGVyXG4gICAgICAgICAgICBwb3M6J2ZpeGVkJywvLyBEZWZhdWx0IFBvc2l0aW9uXG4gICAgICAgICAgICB0b3A6JzBweCcsICAvLyBEZWZhdWx0IFRvcCB2YWx1ZVxuICAgICAgICAgICAgbGVmdDonMHB4JywgLy8gRGVmYXVsdCBMZWZ0IHZhbHVlXG4gICAgICAgICAgICB3aWR0aDonMTAwJScsIC8vIERlZmF1bHQgd2lkdGggXG4gICAgICAgICAgICBoZWlnaHQ6JzEwMCUnLCAvLyBEZWZhdWx0IEhlaWdodFxuICAgICAgICAgICAgekluZGV4OiAnOTk5JywgIC8vIERlZmF1bHQgekluZGV4IFxuICAgICAgICAgICAgYmdDb2xvcjogJyNlZmVmZWYnLCAvLyBEZWZhdWx0IGJhY2tncm91bmQgY29sb3JcbiAgICAgICAgICAgIHNwaW5uZXI6J3NwaW5uZXIxJywgLy8gRGVmYXVsdCBTcGlubmVyXG4gICAgICAgICAgICBpbWFnZVBhdGg6JycgLy8gRGVmYXVsdCBQYXRoIGN1c3RvbSBpbWFnZVxuICAgICAgICB9LCBvcHRpb25zKTtcblxuICAgICAgICAvL0N1c3RvbWl6ZWQgU3Bpbm5lcnNcbiAgICAgICAgdmFyIHNwaW5uZXIwMSA9ICc8ZGl2IGNsYXNzPVwiZmwgc3Bpbm5lcjFcIj48ZGl2IGNsYXNzPVwiZG91YmxlLWJvdW5jZTFcIj48L2Rpdj48ZGl2IGNsYXNzPVwiZG91YmxlLWJvdW5jZTJcIj48L2Rpdj48L2Rpdj4nO1xuICAgICAgICAvLyB2YXIgc3Bpbm5lcjAyID0gJzxkaXYgY2xhc3M9XCJmbCBzcGlubmVyMlwiPjxkaXYgY2xhc3M9XCJzcGlubmVyLWNvbnRhaW5lciBjb250YWluZXIxXCI+PGRpdiBjbGFzcz1cImNpcmNsZTFcIj48L2Rpdj48ZGl2IGNsYXNzPVwiY2lyY2xlMlwiPjwvZGl2PjxkaXYgY2xhc3M9XCJjaXJjbGUzXCI+PC9kaXY+PGRpdiBjbGFzcz1cImNpcmNsZTRcIj48L2Rpdj48L2Rpdj48ZGl2IGNsYXNzPVwic3Bpbm5lci1jb250YWluZXIgY29udGFpbmVyMlwiPjxkaXYgY2xhc3M9XCJjaXJjbGUxXCI+PC9kaXY+PGRpdiBjbGFzcz1cImNpcmNsZTJcIj48L2Rpdj48ZGl2IGNsYXNzPVwiY2lyY2xlM1wiPjwvZGl2PjxkaXYgY2xhc3M9XCJjaXJjbGU0XCI+PC9kaXY+PC9kaXY+PGRpdiBjbGFzcz1cInNwaW5uZXItY29udGFpbmVyIGNvbnRhaW5lcjNcIj48ZGl2IGNsYXNzPVwiY2lyY2xlMVwiPjwvZGl2PjxkaXYgY2xhc3M9XCJjaXJjbGUyXCI+PC9kaXY+PGRpdiBjbGFzcz1cImNpcmNsZTNcIj48L2Rpdj48ZGl2IGNsYXNzPVwiY2lyY2xlNFwiPjwvZGl2PjwvZGl2PjwvZGl2Pic7XG4gICAgICAgIC8vIHZhciBzcGlubmVyMDMgPSAnPGRpdiBjbGFzcz1cImZsIHNwaW5uZXIzXCI+PGRpdiBjbGFzcz1cImRvdDFcIj48L2Rpdj48ZGl2IGNsYXNzPVwiZG90MlwiPjwvZGl2PjwvZGl2Pic7XG4gICAgICAgIC8vIHZhciBzcGlubmVyMDQgPSAnPGRpdiBjbGFzcz1cImZsIHNwaW5uZXI0XCI+PC9kaXY+JzsgXG4gICAgICAgIC8vIHZhciBzcGlubmVyMDUgPSAnPGRpdiBjbGFzcz1cImZsIHNwaW5uZXI1XCI+PGRpdiBjbGFzcz1cImN1YmUxXCI+PC9kaXY+PGRpdiBjbGFzcz1cImN1YmUyXCI+PC9kaXY+PC9kaXY+JzsgXG4gICAgICAgIC8vIHZhciBzcGlubmVyMDYgPSAnPGRpdiBjbGFzcz1cImZsIHNwaW5uZXI2XCI+PGRpdiBjbGFzcz1cInJlY3QxXCI+PC9kaXY+PGRpdiBjbGFzcz1cInJlY3QyXCI+PC9kaXY+PGRpdiBjbGFzcz1cInJlY3QzXCI+PC9kaXY+PGRpdiBjbGFzcz1cInJlY3Q0XCI+PC9kaXY+PGRpdiBjbGFzcz1cInJlY3Q1XCI+PC9kaXY+PC9kaXY+JzsgXG4gICAgICAgIC8vIHZhciBzcGlubmVyMDcgPSAnPGRpdiBjbGFzcz1cImZsIHNwaW5uZXI3XCI+PGRpdiBjbGFzcz1cImNpcmMxXCI+PC9kaXY+PGRpdiBjbGFzcz1cImNpcmMyXCI+PC9kaXY+PGRpdiBjbGFzcz1cImNpcmMzXCI+PC9kaXY+PGRpdiBjbGFzcz1cImNpcmM0XCI+PC9kaXY+PC9kaXY+JzsgXG5cbiAgICAgICAgLy9UaGUgdGFyZ2V0XG4gICAgICAgIHZhciBlbCA9ICQodGhpcyk7XG5cbiAgICAgICAgLy9Jbml0IHN0eWxlc1xuICAgICAgICB2YXIgaW5pdFN0eWxlcyA9IHtcbiAgICAgICAgICAgICdwb3NpdGlvbic6c2V0dGluZ3MucG9zLFxuICAgICAgICAgICAgJ3dpZHRoJzpzZXR0aW5ncy53aWR0aCxcbiAgICAgICAgICAgICdoZWlnaHQnOnNldHRpbmdzLmhlaWdodCxcbiAgICAgICAgICAgICd0b3AnOnNldHRpbmdzLnRvcCxcbiAgICAgICAgICAgICdsZWZ0JzpzZXR0aW5ncy5sZWZ0XG4gICAgICAgIH07XG5cbiAgICAgICAgLy9BcHBseSBzdHlsZXNcbiAgICAgICAgZWwuY3NzKGluaXRTdHlsZXMpO1xuXG4gICAgICAgIC8vRWFjaCBcbiAgICAgICAgZWwuZWFjaChmdW5jdGlvbigpIHtcbiAgICAgICAgICAgICBlbC5odG1sKHNwaW5uZXIwMSk7XG4gICAgICAgICAgICAvLyB2YXIgYSA9IHNldHRpbmdzLnNwaW5uZXI7XG4gICAgICAgICAgICAgICAgLy8gc3dpdGNoIChhKSB7XG4gICAgICAgICAgICAgICAgLy8gICAgIGNhc2UgJ3NwaW5uZXIxJzpcbiAgICAgICAgICAgICAgICAvLyAgICAgICAgICAgICBlbC5odG1sKHNwaW5uZXIwMSk7XG4gICAgICAgICAgICAgICAgLy8gICAgICAgICBicmVhaztcbiAgICAgICAgICAgICAgICAgICAgLy8gY2FzZSAnc3Bpbm5lcjInOlxuICAgICAgICAgICAgICAgICAgICAvLyAgICAgICAgIGVsLmh0bWwoc3Bpbm5lcjAyKTtcbiAgICAgICAgICAgICAgICAgICAgLy8gICAgIGJyZWFrO1xuICAgICAgICAgICAgICAgICAgICAvLyBjYXNlICdzcGlubmVyMyc6XG4gICAgICAgICAgICAgICAgICAgIC8vICAgICAgICAgZWwuaHRtbChzcGlubmVyMDMpO1xuICAgICAgICAgICAgICAgICAgICAvLyAgICAgYnJlYWs7XG4gICAgICAgICAgICAgICAgICAgIC8vIGNhc2UgJ3NwaW5uZXI0JzpcbiAgICAgICAgICAgICAgICAgICAgLy8gICAgICAgICBlbC5odG1sKHNwaW5uZXIwNCk7XG4gICAgICAgICAgICAgICAgICAgIC8vICAgICBicmVhaztcbiAgICAgICAgICAgICAgICAgICAgLy8gY2FzZSAnc3Bpbm5lcjUnOlxuICAgICAgICAgICAgICAgICAgICAvLyAgICAgICAgIGVsLmh0bWwoc3Bpbm5lcjA1KTtcbiAgICAgICAgICAgICAgICAgICAgLy8gICAgIGJyZWFrO1xuICAgICAgICAgICAgICAgICAgICAvLyBjYXNlICdzcGlubmVyNic6XG4gICAgICAgICAgICAgICAgICAgIC8vICAgICAgICAgZWwuaHRtbChzcGlubmVyMDYpO1xuICAgICAgICAgICAgICAgICAgICAvLyAgICAgYnJlYWs7XG4gICAgICAgICAgICAgICAgICAgIC8vIGNhc2UgJ3NwaW5uZXI3JzpcbiAgICAgICAgICAgICAgICAgICAgLy8gICAgICAgICBlbC5odG1sKHNwaW5uZXIwNyk7XG4gICAgICAgICAgICAgICAgICAgIC8vICAgICBicmVhaztcbiAgICAgICAgICAgICAgICAgICAgLy8gZGVmYXVsdDpcbiAgICAgICAgICAgICAgICAgICAgLy8gICAgIGVsLmh0bWwoc3Bpbm5lcjAxKTtcbiAgICAgICAgICAgICAgICAgICAgLy8gfVxuXG4gICAgICAgICAgICAgICAgLy9BZGQgY3VzdG9taXplZCBsb2FkZXIgaW1hZ2VcblxuICAgICAgICAgICAgICAgIGlmIChzZXR0aW5ncy5pbWFnZVBhdGggIT0nJykge1xuICAgICAgICAgICAgICAgICAgICBlbC5odG1sKCc8ZGl2IGNsYXNzPVwiZmxcIj48aW1nIHNyYz1cIicrc2V0dGluZ3MuaW1hZ2VQYXRoKydcIj48L2Rpdj4nKTtcbiAgICAgICAgICAgICAgICAgICAgY2VudGVyTG9hZGVyKCk7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICB9KTtcblxuICAgICAgICAvL1RpbWUgdG8gaGlkZSBmYWtlTG9hZGVyXG4gICAgICAgIHNldFRpbWVvdXQoZnVuY3Rpb24oKXtcbiAgICAgICAgICAgICQoZWwpLmZhZGVPdXQoKTtcbiAgICAgICAgfSwgc2V0dGluZ3MudGltZVRvSGlkZSk7XG5cbiAgICAgICAgLy9SZXR1cm4gU3R5bGVzIFxuICAgICAgICByZXR1cm4gdGhpcy5jc3Moe1xuICAgICAgICAgICAgJ2JhY2tncm91bmRDb2xvcic6c2V0dGluZ3MuYmdDb2xvcixcbiAgICAgICAgICAgICd6SW5kZXgnOnNldHRpbmdzLnpJbmRleFxuICAgICAgICB9KTtcblxuIFxuICAgIH07IC8vIEVuZCBGYWtlIExvYWRlclxuIFxuXG4gICAgICAgIC8vQ2VudGVyIFNwaW5uZXJcbiAgICAgICAgZnVuY3Rpb24gY2VudGVyTG9hZGVyKCkge1xuXG4gICAgICAgICAgICB2YXIgd2luVyA9ICQod2luZG93KS53aWR0aCgpO1xuICAgICAgICAgICAgdmFyIHdpbkggPSAkKHdpbmRvdykuaGVpZ2h0KCk7XG5cbiAgICAgICAgICAgIHZhciBzcGlubmVyVyA9ICQoJy5mbCcpLm91dGVyV2lkdGgoKTtcbiAgICAgICAgICAgIHZhciBzcGlubmVySCA9ICQoJy5mbCcpLm91dGVySGVpZ2h0KCk7XG5cbiAgICAgICAgICAgICQoJy5mbCcpLmNzcyh7XG4gICAgICAgICAgICAgICAgJ3Bvc2l0aW9uJzonYWJzb2x1dGUnLFxuICAgICAgICAgICAgICAgICdsZWZ0Jzood2luVy8yKS0oc3Bpbm5lclcvMiksXG4gICAgICAgICAgICAgICAgJ3RvcCc6KHdpbkgvMiktKHNwaW5uZXJILzIpXG4gICAgICAgICAgICB9KTtcblxuICAgICAgICB9XG5cbiAgICAgICAgJCh3aW5kb3cpLmxvYWQoZnVuY3Rpb24oKXtcbiAgICAgICAgICAgIGNlbnRlckxvYWRlcigpO1xuICAgICAgICAgICAgJCh3aW5kb3cpLnJlc2l6ZShmdW5jdGlvbigpe1xuICAgICAgICAgICAgICAgIGNlbnRlckxvYWRlcigpO1xuICAgICAgICAgICAgfSk7XG4gICAgICAgIH0pO1xuXG5cbn0oalF1ZXJ5KSk7XG5cblxuXG5cbiJdfQ==
