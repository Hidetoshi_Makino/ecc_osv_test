<?php
/**
 *
 * @copyright 2013 Spirit of Co.,Ltd. All Rights Reserved.
 * @link http://sbps.ec-cube.pro/
 *
 */


// {{{ requires
require_once(CLASS_EX_REALDIR . "page_extends/admin/LC_Page_Admin_Ex.php");
require_once(MODULE_REALDIR . "mdl_sps/inc/include.php");

/**
 * 支払いmemo仕様
 * ・dtb_payment
 * memo01 マーチャントID
 * memo02 サービスID
 * memo03 支払い方法(クレジット1, コンビニ2, ペイジー3, ウェブマネー4, Yahoo!ウォレット5)
 * memo04 SPS顧客ID
 * memo05 SPS支払方法管理番号
 * memo06 ハッシュ値
 * memo07
 * memo08
 * memo09
 * memo10
 */

/**
 * SPS決済モジュールの管理画面設定のページクラス.
 *
 * @package Page
 * @author Spirit of Co.,Ltd.
 */
class LC_Page_Mdl_SPS_Config extends LC_Page_Admin_Ex {

    // }}}
    // {{{ functions

    /**
     * Page を初期化する.
     *
     * @return void
     */
    function init() {
        parent::init();
        $objMdl =& SC_Mdl_SPS_Ex::getInstance();
        $this->tpl_mainpage = MDL_SPS_TEMPLATE_PATH. 'admin/config.tpl';
        $this->tpl_subtitle = $objMdl->getName();

        $this->arrConnectType = array(
                MDL_SPS_CONNECT_TYPE_API => 'API型',
                MDL_SPS_CONNECT_TYPE_LINK => 'リンク型(遷移型)',
                MDL_SPS_CONNECT_TYPE_LINK => 'FreeCSV型(遷移型)',
            );
        $this->arrTypeCustCode = array(
                '1' => '旧バージョン方式(アップグレード用)',
                '2' => '顧客キー付方式(推奨)'
            );
        $this->arrOptionActive = array('1' => '要', '0' => '不要');
        $this->type_sps_auth = array('売上日指定要求', '自動売上');

        $this->arrPayments = SC_Util_SPS_Ex::getPaymentTypeNames();
    }

    /**
     * Page のプロセス.
     *
     * @return void
     */
    function process() {
        $this->action();
        $this->sendResponse();
    }

    /**
     * Page のアクション.
     *
     * @return void
     */
    function action() {
        $objMdl =& SC_Mdl_SPS_Ex::getInstance();
        $objMdl->install();
        $objMdl->updateFile();

        switch($this->getMode()) {
        case 'register':
            list($this->arrForm, $this->arrErr, $this->tpl_onload) = $this->registerMode($_POST);
            if (SC_Utils_Ex::isBlank($this->arrErr)) {
                $this->lfRegistPage($this->arrForm['is_tpl_init']['value']);
                $plugin_id = $this->lfRegistPlugins();
                $this->lfRegistBloc($this->arrForm['enable_payment_type']['value'], $plugin_id, $this->arrForm['is_tpl_init']['value']);
                SC_Utils_Ex::clearCompliedTemplate();
                $this->checkData();
            }
            break;
        default:
            list($this->arrForm, $this->tpl_onload) = $this->defaultMode();
            break;
        }
        $this->tpl_recommend_check = $this->recommendModuleCheck();
        $this->tpl_is_module_regist = $this->lfIsRegistPaymentModule();

        $this->arrPaymentLinks = $this->getLinkTypes($this->arrPayments);

        $this->setTemplate($this->tpl_mainpage);
    }

    /**
     * デストラクタ.
     *
     * @return void
     */
    function destroy() {
    }

    /**
     * 初回表示処理
     *
     */
    function defaultMode() {
        $objMdl =& SC_Mdl_SPS_Ex::getInstance();
        $subData = $objMdl->getUserSettings();
        $tpl_onload = $this->getBodyOnload($subData);
        $objForm = $this->initParam($subData);
        return array($objForm->getFormParamList(), $tpl_onload);
    }

    /**
     * 登録ボタン押下時の処理
     *
     */
    function registerMode(&$arrParam) {
        // 認証情報
        $objSess = new SC_Session_Ex();

        $objMdl =& SC_Mdl_SPS::getInstance();
        $tpl_onload = $this->getBodyOnload($arrParam);
        $objForm = $this->initParam($arrParam);
        if ($arrErr = $this->checkError($objForm)) {
            return array($objForm->getFormParamList(), $arrErr, $tpl_onload);
        }
        $arrForm = $objForm->getHashArray();
        $objMdl->updateFile();
        $arrFailedFile = $objMdl->getFailedCopyFile();
        if (count($arrFailedFile) > 0) {
            foreach($arrFailedFile as $file) {
                $alert = $file . 'に書込権限を与えてください。';
                $tpl_onload .= 'alert("' . $alert . '");';
            }
            return array($objForm->getFormParamList(), $arrErr, $tpl_onload);
        }
        $objMdl->registerUserSettings($arrForm);


        // del_flgを削除にしておく
        $objQuery =& SC_Query::getSingletonInstance();
        $objQuery->begin();
        $arrUpdVal = array('del_flg' => 1);
        $where = 'module_code = ?';
        $arrWhereVal = array(MDL_SPS_CODE);
        $objQuery->update('dtb_payment', $arrUpdVal, $where, $arrWhereVal);

        foreach ($arrForm['enable_payment_type'] as $payment_type_id) {
            $arrData = array();
            $arrData['payment_method'] = $this->arrPayments[ $payment_type_id ];
            $arrData[MDL_SPS_PAYMENT_COL_PAYID] = $payment_type_id;
            $arrData['fix'] = 3;
            $arrData['creator_id'] = $objSess->member_id;
            $arrData['update_date'] = 'CURRENT_TIMESTAMP';
            $arrData['module_path'] = MDL_SPS_PATH . 'payment.php';
            $arrData['module_code'] = $objMdl->getCode(true);
            $arrData['del_flg'] = '0';

            $arrPayment = $this->getPaymentDB($payment_type_id);

            // 更新データがあれば更新する。
            if (count($arrPayment) > 0){
                // データが存在していればUPDATE、無ければINSERT
                $where = "module_code = ? AND " . MDL_SPS_PAYMENT_COL_PAYID . " = ?";
                $arrWhereVal = array($objMdl->getCode(true), (string)$payment_type_id);
                $objQuery->update('dtb_payment', $arrData, $where, $arrWhereVal);
            } else {
                // ランクの最大値を取得する
                $max_rank = $objQuery->max('rank', 'dtb_payment');
                $arrData["create_date"] = "CURRENT_TIMESTAMP";
                $arrData["rank"] = $max_rank + 1;
                $arrData['payment_id'] = $objQuery->nextVal('dtb_payment_payment_id');

                // ヘルパー呼び出し
                $objPayment =& SC_Mdl_SPS_Helper_Ex::getPaymentHelper($payment_type_id);
                $arrDefault = $objPayment->getPaymentRuleDefault();

                $arrData = array_merge($arrDefault, $arrData);
                $objQuery->insert("dtb_payment", $arrData);
            }
        }

        $objQuery->commit();

        $tpl_onload .= 'alert("登録完了しました。\n基本情報＞支払方法設定より個別設定をしてください。");window.close();';
        return array($objForm->getFormParamList(), $arrErr, $tpl_onload);
    }

    /**
     * フォームパラメータ初期化
     *
     * @param array $arrData
     * @return object SC_FormParam
     */
    function initParam($arrData = array()) {
        $objForm = new SC_FormParam();

        $objForm->addParam('マーチャントID', 'id_sps_marchant', STEXT_LEN, 'a', array('EXIST_CHECK', 'ALNUM_CHECK'));
        $objForm->addParam('サービスID', 'id_sps_service'    , STEXT_LEN, 'a', array('EXIST_CHECK', 'ALNUM_CHECK'));
        $objForm->addParam('ハッシュ値', 'id_sps_hash', SMTEXT_LEN, 'a', array('EXIST_CHECK', 'ALNUM_CHECK', 'MAX_LENGTH_CHECK'));

        $objForm->addParam('3DES暗号化キー', 'key_sps_3des', 24, 'a', array('EXIST_CHECK', 'ALNUM_CHECK', 'MAX_LENGTH_CHECK'));
        $objForm->addParam('3DES初期化キー', 'key_sps_3des_iv', 8, 'a', array('EXIST_CHECK', 'ALNUM_CHECK', 'MAX_LENGTH_CHECK'));

        $objForm->addParam('API接続先URL', 'url_sps_api', URL_LEN, '', array('URL_CHECK'));
        $objForm->addParam('リンク型 購入要求 接続先URL', 'url_sps_buyservice_api', URL_LEN, '', array('URL_CHECK'));
        $objForm->addParam('リンク型 顧客決済登録要求 接続先URL', 'url_sps_registservice_api', URL_LEN, '', array('URL_CHECK'));
        $objForm->addParam('リンク型 顧客決済登録消去 接続先URL', 'url_sps_deleteservice_api', URL_LEN, '', array('URL_CHECK'));

        $objForm->addParam('通信元IPアドレス', 'connect_from_ip', STEXT_LEN, 'a', array('IP_CHECK'));

        $objForm->addParam('決済方法', 'enable_payment_type', INT_LEN, 'n', array('EXIST_CHECK', 'NUM_CHECK'), isset($arrData['enable_payment_type']) ? $arrData['enable_payment_type'] : "");

        $objForm->addParam('顧客IDの方式', 'type_cust_code', INT_LEN, 'n', array('EXIST_CHECK', 'NUM_CHECK'));

        $objForm->addParam('テンプレート初期化', 'is_tpl_init', INT_LEN, 'n', array('NUM_CHECK'),"");

        $objForm->setParam($arrData);
        $objForm->convParam();
        $objForm->trimParam();
        return $objForm;
    }

    function recommendModuleCheck() {
        $res = null;
        if (extension_loaded('curl') === false) {
            $res .= '※ curl拡張モジュールがロード出来ません。管理画面機能やAPI型の決済は利用出来ません。<br />';
        }
        if (extension_loaded('mcrypt') === false) {
            $res .= '※ Mcrypt拡張モジュールがロード出来ません。管理画面機能やAPI型の決済は利用出来ません。';
        }
        return $res;
    }


    /**
     * 入力パラメータの検証
     *
     * @param SC_FormParam $objForm
     * @return array|null
     */
    function checkError(&$objForm) {
        $arrErr = null;
        $arrErr = $objForm->checkError();
        if (extension_loaded('xml') === false) {
            $arrErr['err'] .= '※ xml拡張モジュールがロード出来ません。PHPの動作環境がEC-CUBEのシステム環境要件を満たしているか確認して下さい。';
        }
        if (extension_loaded('mbstring') === false) {
            $arrErr['err'] .= '※ mbstring拡張モジュールがロード出来ません。PHPの動作環境がEC-CUBEのシステム環境要件を満たしているか確認して下さい。';
        }

        return $arrErr;
    }

    // onload設定
    function getBodyOnload($arrData) {
        $tpl_onload = '';
        return $tpl_onload;
    }

    function lfIsRegistPaymentModule() {
        $objMdl =& SC_Mdl_SPS_Ex::getInstance();
        $module_code = $objMdl->getCode(true);
        $objQuery =& SC_Query_Ex::getSingletonInstance();
        if($objQuery->count('dtb_payment', 'module_code = ?', array($module_code))) {
            return true;
        }else{
            return false;
        }
    }


    function lfRegistPlugins() {
        $plugin_code = 'SpsUtils';
        $is_enable = true;
        $plugin_id = SC_Util_SPS_Ex::installPluginFromPluginInfo($plugin_code, $is_enable, '2');

        $plugin_code = 'Sps2Click';
        $is_enable = false;
        $plugin_id = SC_Util_SPS_Ex::installPluginFromPluginInfo($plugin_code, $is_enable, '1');

        $plugin_code = 'SpsCore';
        $is_enable = true;
        $plugin_id = SC_Util_SPS_Ex::installPluginFromPluginInfo($plugin_code, $is_enable, '0');

        return $plugin_id;
    }

    function lfRegistPage($is_force) {
        $arrPageId = array();
        // 決済画面をデザインテンプレートに足す
        $page_name = '商品購入/決済画面';
        $url = 'shopping/load_payment_module.php';
        $filename = 'shopping/load_payment_module';

        $tpl_data = file_get_contents(MDL_SPS_TEMPLATE_PATH . 'default/load_payment_module.tpl');
        $device_type_id = DEVICE_TYPE_PC;
        $page_id = SC_Util_SPS_Ex::setPageData($tpl_data, $page_name, $url, $filename, $device_type_id, $is_force);
        $arrPageId[ $filename ][ $device_type_id ] = $page_id;

        $tpl_data = file_get_contents(MDL_SPS_TEMPLATE_PATH . 'sphone/load_payment_module.tpl');
        $device_type_id = DEVICE_TYPE_SMARTPHONE;
        $page_id = SC_Util_SPS_Ex::setPageData($tpl_data, $page_name, $url, $filename, $device_type_id, $is_force);
        $arrPageId[ $filename ][ $device_type_id ] = $page_id;

        $tpl_data = file_get_contents(MDL_SPS_TEMPLATE_PATH . 'mobile/load_payment_module.tpl', $is_force);
        $device_type_id = DEVICE_TYPE_MOBILE;
        $page_id = SC_Util_SPS_Ex::setPageData($tpl_data, $page_name, $url, $filename, $device_type_id, $is_force);
        $arrPageId[ $filename ][ $device_type_id ] = $page_id;

        $objMdl =& SC_Mdl_SPS::getInstance();
        $objMdl->registerSubData($arrPageId, 'page_setting');
    }

    function lfRegistBloc($arrPaymentTypeId, $plugin_id, $is_force) {
        $objMdl =& SC_Mdl_SPS::getInstance();
        $arrBlocId = array();
        foreach ($arrPaymentTypeId as $payment_type_id) {
            $filename = "";
            // ヘルパー呼び出し
            $objPayment =& SC_Mdl_SPS_Helper_Ex::getPaymentHelper($payment_type_id);
            $filename = $objPayment->getPaymentBlocName();

            if ($filename != "") {
                $bloc_name = $this->arrPayments[$payment_type_id] . "入力フォーム";
                if (file_exists(MDL_SPS_TEMPLATE_PATH . 'default/bloc/' . $filename . '.tpl')) {
                    $bloc_data = file_get_contents(MDL_SPS_TEMPLATE_PATH . 'default/bloc/' . $filename . '.tpl');
                    $device_type_id = DEVICE_TYPE_PC;
                    $bloc_id = SC_Util_SPS_Ex::setBlocData($plugin_id, $bloc_data, $device_type_id, $bloc_name, $filename, "", $is_force);
                    $arrBlocId[ $filename ][ $device_type_id ] = $bloc_id;
                }
                if (file_exists(MDL_SPS_TEMPLATE_PATH . 'sphone/bloc/' . $filename . '.tpl')) {
                    $bloc_data = file_get_contents(MDL_SPS_TEMPLATE_PATH . 'sphone/bloc/' . $filename . '.tpl');
                    $device_type_id = DEVICE_TYPE_SMARTPHONE;
                    $bloc_id = SC_Util_SPS_Ex::setBlocData($plugin_id, $bloc_data, $device_type_id, $bloc_name, $filename, "", $is_force);
                    $arrBlocId[ $filename ][ $device_type_id ] = $bloc_id;
                }
                if (file_exists(MDL_SPS_TEMPLATE_PATH . 'mobile/bloc/' . $filename . '.tpl')) {
                    $bloc_data = file_get_contents(MDL_SPS_TEMPLATE_PATH . 'mobile/bloc/' . $filename . '.tpl');
                    $device_type_id = DEVICE_TYPE_MOBILE;
                    $bloc_id = SC_Util_SPS_Ex::setBlocData($plugin_id, $bloc_data, $device_type_id, $bloc_name, $filename, "", $is_force);
                    $arrBlocId[ $filename ][ $device_type_id ] = $bloc_id;
                }
            }
        }
        $objMdl->registerSubData($arrBlocId, 'bloc_setting');
    }

    function getLinkTypes($arrPayments) {
        $arrRet = array();
        foreach ($arrPayments as $payment_type_id => $name) {
            $objPayment =& SC_Mdl_SPS_Helper_Ex::getPaymentHelper($payment_type_id);
            $arrRet[$payment_type_id] = $objPayment->getConnectType();
        }
        return $arrRet;
    }

    function checkData() {
        $objFileMgr = new SC_Helper_FileManager_Ex();
        $path = DATA_REALDIR . 'logs/';
        $objMdl =& SC_Mdl_SPS_Ex::getInstance();
        $pf = $objMdl->getCode(true);
        if (is_file($path . $pf .'_check.log')) {
            return;
        }
        $arrFileList = $objFileMgr->sfGetFileList($path);
        $arrList = array();
        foreach ($arrFileList as $arrFile) {
            if ($arrFile['is_dir'] == true) continue;
            if(substr($arrFile['file_name'], 0, strlen($pf)) == $pf) {
                $arrList[] = $arrFile['file_name'];
            }
        }
        if (!SC_Utils_Ex::isBlank($arrList)) {
            foreach ($arrList as $file) {
                if (is_file($path . $file)) {
                    $data = file_get_contents($path . $file);
                    $data = preg_replace('/^[^0-9\/]+.+$/m','', $data);
                    file_put_contents($path . $file, $data);
                    $objMdl->printLog('check data:' . $file);
                }
            }
        }
        file_put_contents($path . $pf .'_check.log', date('Y/m/d H:i:s'));
    }

    // DBからデータを取得する
    function getPaymentDB($type){
        $objMdl =& SC_Mdl_SPS::getInstance();
        $objQuery =& SC_Query::getSingletonInstance();
        $arrVal = array($objMdl->getCode(true), (string)$type);
        $arrRet = $objQuery->select("module_id", "dtb_payment", "module_code = ? AND " . MDL_SPS_PAYMENT_COL_PAYID . " = ?", $arrVal);
        return $arrRet;
    }
}

