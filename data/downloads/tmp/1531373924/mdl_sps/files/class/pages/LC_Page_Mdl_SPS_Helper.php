<?php
/**
 *
 * @copyright 2013 Spirit of Co.,Ltd. All Rights Reserved.
 * @link http://sbps.ec-cube.pro/
 *
 */

// {{{ requires
require_once(MODULE_REALDIR . 'mdl_sps/inc/include.php');
require_once CLASS_EX_REALDIR . 'page_extends/LC_Page_Ex.php';

/**
 * 決済モジュール 決済画面クラス
 */
class LC_Page_Mdl_SPS_Helper extends LC_Page_Ex {
    var $type;
    var $objMdl;
    var $arrSetting;

    /**
     * コンストラクタ
     *
     * @return void
     */
    function LC_Page_Mdl_SPS_Helper() {
        $this->objMdl =& SC_Mdl_SPS_Ex::getInstance();
        $this->arrSetting = $this->objMdl->getUserSettings();
    }

    /**
     * Page を初期化する.
     *
     * @return void
     */
    function init() {
        parent::init();
        $this->httpCacheControl('nocache');
    }

    /**
     * Page のプロセス.
     *
     * @return void
     */
    function process() {
        $this->action();
        $this->sendResponse();
    }

    /**
     * Page のアクション.
     *
     * @return void
     */
    function action() {
        $objSiteSess = new SC_SiteSession_Ex();
        $objPurchase = new SC_Helper_Purchase_Ex();
        $objCartSess = new SC_CartSession_Ex();

        if (!SC_Utils_Ex::isBlank($_SESSION['order_id'])) {
            $order_id = $_SESSION['order_id'];
        } else if (!SC_Utils_Ex::isBlank($_REQUEST['order_id'])
                     && SC_Utils_Ex::sfIsInt($_REQUEST['order_id'])
                     && $this->lfIsValidToken($_REQUEST['order_id'], $_REQUEST[TRANSACTION_ID_NAME])) {
            $order_id = $_REQUEST['order_id'];
            $_SESSION['order_id'] = $order_id;
        } else {
            $msg =  "例外エラー<br />注文情報の取得が出来ませんでした。<br />この手続きは無効となりました。";
            $this->objMdl->printLog($msg);
            SC_Utils_Ex::sfDispSiteError(FREE_ERROR_MSG, "", true, $msg);
        }

        $arrOrder = $objPurchase->getOrder($order_id);
        $this->tpl_title = $arrOrder['payment_method'];
        $objFormParam = new SC_FormParam_Ex();

        if ($arrOrder['status'] != ORDER_PENDING) {
            switch ($arrOrder['status']) {
            case ORDER_NEW:
            case ORDER_PRE_END:
                SC_Response_Ex::sendRedirect(SHOPPING_COMPLETE_URLPATH);
                SC_Response_Ex::actionExit();
                break;
            case ORDER_PAY_WAIT:
                // リンク型遷移での戻りは各ヘルパーに処理させる場合があるため、リダイレクトしない。
                if ($this->getMode() != 'spsreturn') {
                    SC_Response_Ex::sendRedirect(SHOPPING_COMPLETE_URLPATH);
                    SC_Response_Ex::actionExit();
                }
                break;
            default:
                if ($this->getMode() != 'spsreturn' && !SC_Utils_Ex::isBlank($arrOrder['status'])) {
                    $msg = "例外エラー<br />注文情報が無効です。<br />この手続きは無効となりました。";
                    $this->objMdl->printLog($msg);
                    SC_Utils_Ex::sfDispSiteError(FREE_ERROR_MSG, "", true, $msg);
                    SC_Response_Ex::actionExit();
                }
                break;
            }
        }

        // 決済手段毎のヘルパークラスを読み込み
        $arrPayment = SC_Util_SPS_Ex::getPaymentTypeConfig($arrOrder['payment_id']);
        $this->arrPayment = $arrPayment;
        $payment_type_id = $arrPayment[MDL_SPS_PAYMENT_COL_PAYID];

        if (SC_Utils_Ex::isBlank($payment_type_id)) {
            $msg = "例外エラー<br />注文情報の決済方法と決済モジュールの設定が一致していません。<br />この手続きは無効となりました。<br />管理者に連絡をして下さい。";
            $this->objMdl->printLog($msg);
            SC_Utils_Ex::sfDispSiteError(FREE_ERROR_MSG, "", true, $msg);
            SC_Response_Ex::actionExit();
        }

        // ヘルパー呼び出し
        $objPayment =& SC_Mdl_SPS_Helper_Ex::getPaymentHelper($payment_type_id);

        $this->lfSetToken($arrOrder, $arrPayment[MDL_SPS_PAYMENT_COL_PAYID]);

        $objPayment->initParam($objFormParam, $arrPayment, $arrOrder);

        $objFormParam->setParam($arrPayment);
        $objFormParam->setParam($_REQUEST);
        $objFormParam->convParam();

        $this->tpl_url = "?";
        $mode = $this->getMode();
        if(SC_Display_Ex::detectDevice() === DEVICE_TYPE_MOBILE && $this->getMode() == '') {
            $mode = 'next';
        }else if(SC_Display_Ex::detectDevice() === DEVICE_TYPE_SMARTPHONE) {
            $this->tpl_url = SHOPPING_MODULE_URLPATH;
        }

        $objPayment->modeAction($mode, $objFormParam, $arrOrder, $this);

        $this->tpl_form_bloc_path = $objPayment->getFormBloc();
        if ($this->tpl_form_bloc_path == "") {
            $msg = "例外エラー<br />決済用ブロックテンプレートが見つかりません。<br />この手続きは無効となりました。<br />管理者に連絡をして下さい。";
            $this->objMdl->printLog($msg);
            SC_Utils_Ex::sfDispSiteError(FREE_ERROR_MSG, "", true, $msg);
            SC_Response_Ex::actionExit();
        }

        $this->arrForm = $objFormParam->getFormParamList();

        $this->tpl_payment_onload = $this->tpl_payment_onload;

        if (defined('MDL_SPS_DEBUG') && MDL_SPS_DEBUG) {
            $this->objMdl->printLog(print_r($this,true));
        }

    }

    /**
     * デストラクタ.
     *
     * @return void
     */
    function destroy() {
    }

    /**
     * トークンチェックしない.
     */
    function doValidToken() {
        // nothing.
    }

    function lfSetToken(&$arrOrder, $pay_id) {
        $objPurchase = new SC_Helper_Purchase_Ex();
        $sqlval[MDL_SPS_ORDER_COL_TOKEN] = SC_Helper_Session_Ex::getToken();
        $sqlval[MDL_SPS_ORDER_COL_PAYID] = $pay_id;
        $sqlval[MDL_SPS_ORDER_COL_REQDATE] = date('YmdHis');
        $objQuery =& SC_Query_Ex::getSingletonInstance();
        $objQuery->begin();
        $objPurchase->sfUpdateOrderStatus($arrOrder['order_id'], null, null, null, $sqlval);
        $objQuery->commit();
    }

    /**
     * 外部ページからの遷移の際に受注情報内のTRANSACTION IDとのCSFRチェックを行う。
     *
     * @param integer $order_id 受注ID
     * @param text $transactionid TRANSACTION ID
     * @return void
     */
    function lfIsValidToken($order_id, $transactionid) {
        $objQuery =& SC_Query_Ex::getSingletonInstance();
        if($objQuery->get(MDL_SPS_ORDER_COL_TOKEN, 'dtb_order', 'order_id = ?', array($order_id)) == $transactionid) {
            return true;
        }
        return false;
    }

}
