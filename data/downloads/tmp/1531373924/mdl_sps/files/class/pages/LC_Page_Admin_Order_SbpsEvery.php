<?php
/*
 * Copyright(c) 2011-2013 SOFTBANK PAYMENT SERVICE Corp. All rights reserved.
 * http://www.sbpayment.jp/
 * Copyright(c) 2013 Spirit of Co.,Ltd. All rights reserved.
 */

// {{{ requires
require_once CLASS_REALDIR . 'pages/admin/order/LC_Page_Admin_Order_Status.php';

require_once(MODULE_REALDIR . 'mdl_sps/inc/include.php');
require_once(MDL_SPS_CLASS_PATH. 'SC_Mdl_SPS.php');
require_once(MDL_SPS_CLASS_PATH. "util/SC_Util_SPS.php");

if (!defined('MODULE_PATH')) {
    define('MODULE_PATH', MODULE_REALDIR);
}

/**
 * SBPS 継続課金対応状況管理 のページクラス(拡張).
 *
 * LC_Page_Admin_Order_SbpsEvery をカスタマイズする場合はこのクラスを編集する.
 *
 * @package Page
 * @author Spirit of Co.,Ltd.
 */
class LC_Page_Admin_Order_SbpsEvery extends LC_Page_Admin_Order_Status {

    // }}}
    // {{{ functions

    /**
     * Page を初期化する.
     *
     * @return void
     */
    function init() {
        parent::init();
        $this->tpl_mainpage = MDL_SPS_TEMPLATE_PATH . 'admin/plg_sbps_every.tpl';
        $this->tpl_mainno = 'order';
        $this->tpl_subno = 'plg_sbps_every';
        $this->tpl_maintitle = '受注管理';
        $this->tpl_subtitle = 'SBPS簡易継続課金管理';

        $masterData = new SC_DB_MasterData_Ex();
        $this->arrORDERSTATUS = $masterData->getMasterData('mtb_order_status');
        unset($this->arrORDERSTATUS[ORDER_PENDING]);
        $this->arrORDERSTATUS_COLOR = $masterData->getMasterData('mtb_order_status_color');
        $objDate = new SC_Date_Ex();
        $objDate->setStartYear(RELEASE_YEAR);
        $this->arrYear = $objDate->getYear();
        $this->arrMonth = $objDate->getMonth();
        $this->arrDay = $objDate->getDay();

        $this->arrSbpsStatus = array(
            SPS_EVERY_OK => '継続課金中',
            SPS_EVERY_CR => '課金解約/売上失敗',
            SPS_EVERY_CC => 'キャリア解約',
            SPS_EVERY_CANCEL => 'キャンセル',
            );
    }

    /**
     * Page のプロセス.
     *
     * @return void
     */
    function process() {
        parent::process();
    }

    /**
     * デストラクタ.
     *
     * @return void
     */
    function destroy() {
    }

    function action() {
        // パラメーター管理クラス
        $objFormParam = new SC_FormParam_Ex();
        // パラメーター情報の初期化
        $this->lfInitParam($objFormParam);
        $objFormParam->setParam($_POST);
        // 入力値の変換
        $objFormParam->convParam();

        $this->arrForm = $objFormParam->getHashArray();

        $arrEveryConfig = $this->loadData();
        $every_product_type = $arrEveryConfig['every_product_type'];

        $objDb = new SC_Helper_DB_Ex();
        $this->arrPayment = $objDb->sfGetIDValueList('dtb_payment', 'payment_id', 'payment_method', 'module_code = ? AND payment_id IN (SELECT payment_id FROM dtb_payment_options WHERE deliv_id = (SELECT deliv_id FROM dtb_deliv WHERE product_type_id = ?))', array(MDL_SPS_CODE, $every_product_type));


        $this->arrErr = $this->lfCheckError($objFormParam);


        if (SC_Utils_Ex::isBlank($this->arrErr)) {
            $mode = $this->getMode();
        } else {
            $mode = 'search';
        }

        switch ($mode) {
            case 'update':
                switch ($objFormParam->getValue('change_status')) {
                    case '':
                        break;
                        // 削除
                    case 'cancel':
                        $this->tpl_exec_result = $this->lfDoCancel($objFormParam->getValue('move'), $objFormParam);
                        break;
                        // 再開
                    case 'update':
                        $this->tpl_exec_result = $this->lfDoUpdate($objFormParam->getValue('move'), $objFormParam);
                        break;
                }

                // 対応状況
                $status = !is_null($objFormParam->getValue('status')) ? $objFormParam->getValue('status') : ORDER_NEW;
                $sbps_status = !is_null($_POST['sbps_status']) ? $objFormParam->getValue('sbps_status') : '';
                $payment_id = !is_null($_POST['payment_id']) ? $objFormParam->getValue('payment_id') : '';
                break;

            case 'search':
                // 対応状況
                $status = !is_null($_POST['status']) ? $objFormParam->getValue('status') : ORDER_NEW;
                $sbps_status = !is_null($_POST['sbps_status']) ? $objFormParam->getValue('sbps_status') : '';
                $payment_id = !is_null($_POST['payment_id']) ? $objFormParam->getValue('payment_id') : '';
                break;

            default:
                // 対応状況
                //デフォルトで新規受付一覧表示
                $status = ORDER_NEW;
                $sbps_status = '';
                $payment_id = '';
                break;
        }
        // 対応状況
        $this->SelectedStatus = $status;
        $this->SelectedSbpsStatus = $sbps_status;
        $this->SelectedPayment = $payment_id;
        //検索結果の表示
        $this->arrStatus = $this->lfStatusDisp($status, $sbps_status, $payment_id, $objFormParam->getValue('search_pageno'));
    }

    /**
     *  パラメーター情報の初期化
     *  @param SC_FormParam
     */
    function lfInitParam(&$objFormParam) {
        $objFormParam->addParam('注文番号', 'order_id', INT_LEN, 'n', array('MAX_LENGTH_CHECK', 'NUM_CHECK'));
        $objFormParam->addParam('注文状況', 'status', INT_LEN, 'n', array('MAX_LENGTH_CHECK', 'NUM_CHECK'));
        $objFormParam->addParam('継続課金状態', 'sbps_status', INT_LEN, 'n', array('MAX_LENGTH_CHECK', 'NUM_CHECK'));
        $objFormParam->addParam('支払い方法', 'payment_id', INT_LEN, 'n', array('MAX_LENGTH_CHECK', 'NUM_CHECK'));

        $objFormParam->addParam('指定年月日 年', 'sps_year', INT_LEN, 'n', array('MAX_LENGTH_CHECK', 'NUM_CHECK'),date('Y'));
        $objFormParam->addParam('指定年月日 月', 'sps_month', INT_LEN, 'n', array('MAX_LENGTH_CHECK', 'NUM_CHECK'), date('m'));
        $objFormParam->addParam('指定年月日 日', 'sps_day', INT_LEN, 'n', array('MAX_LENGTH_CHECK', 'NUM_CHECK'), date('d'));

        $objFormParam->addParam('変更課金状態', 'change_status', STEXT_LEN, 'KVa', array('MAX_LENGTH_CHECK', 'ALNUM_CHECK'));
        $objFormParam->addParam('ページ番号', 'search_pageno', INT_LEN, 'n', array('MAX_LENGTH_CHECK', 'NUM_CHECK'));
        $objFormParam->addParam('対象注文番号', 'move', INT_LEN, 'n', array('MAX_LENGTH_CHECK', 'NUM_CHECK'));
    }

    /**
     *  入力内容のチェック
     *  @param SC_FormParam
     */
    function lfCheckError(&$objFormParam) {
        // 入力データを渡す。
        $arrRet = $objFormParam->getHashArray();
        $arrErr = $objFormParam->checkError();
        if (is_null($objFormParam->getValue('search_pageno'))) {
            $objFormParam->setValue('search_pageno', 1);
        }

        if ($this->getMode() == 'update') {
            if (is_null($objFormParam->getValue('change_status'))) {
                $objFormParam->setValue('change_status','');
            }
        }
        return $arrErr;
    }

    // 対応状況一覧の表示
    function lfStatusDisp($status, $sbps_status, $payment_id, $pageno) {
        $objQuery =& SC_Query_Ex::getSingletonInstance();

        $select ='dtb_order.*';
        $from = 'dtb_order left join dtb_payment on dtb_order.payment_id = dtb_payment.payment_id';
        $where = 'dtb_order.del_flg = 0 AND dtb_order.status = ?';
        $where .= ' AND dtb_order.' . MDL_SPS_ORDER_COL_EVERY . ' IS NOT NULL';
        $where .= ' AND dtb_payment.module_code = ?';
        $arrWhereVal = array($status, MDL_SPS_CODE);

        if (!SC_Utils_Ex::isBlank($sbps_status)) {
            $where .= " AND dtb_order." . MDL_SPS_ORDER_COL_EVERY . " = ?";
            $arrWhereVal[] = $sbps_status;
        }
        if (!SC_Utils_Ex::isBlank($payment_id)) {
            $where .= " AND dtb_order.payment_id = ?";
            $arrWhereVal[] = $payment_id;
        }

        $order = 'order_id DESC';

        $linemax = $objQuery->count($from, $where, $arrWhereVal);
        $this->tpl_linemax = $linemax;

        // ページ送りの処理
        $page_max = ORDER_STATUS_MAX;

        // ページ送りの取得
        $objNavi = new SC_PageNavi_Ex($pageno, $linemax, $page_max, 'fnNaviSearchOnlyPage', NAVI_PMAX);
        $this->tpl_strnavi = $objNavi->strnavi;      // 表示文字列
        $startno = $objNavi->start_row;

        $this->tpl_pageno = $pageno;

        // 取得範囲の指定(開始行番号、行数のセット)
        $objQuery->setLimitOffset($page_max, $startno);

        //表示順序
        $objQuery->setOrder($order);

        //検索結果の取得
        $arrData = $objQuery->select($select, $from, $where, $arrWhereVal);
        if (!SC_Utils_Ex::isBlank($arrData)) {
            foreach ($arrData as $key => $data) {
                $payment_type_id = $data[MDL_SPS_ORDER_COL_PAYID];
                $objPayment =& SC_Mdl_SPS_Helper_Ex::getPaymentHelper($payment_type_id);
                $is_commit = $objPayment->isSales();
                $is_cancel = $objPayment->isCancel();
                if ($is_commit && $data[MDL_SPS_ORDER_COL_REQTYPE] == SPS_COMMIT_OK) {
                    $is_commit = false;
                }
                if ($is_commit && $data[MDL_SPS_ORDER_COL_RESDATA] == SPS_CANCEL_OK) {
                    $is_commit = false;
                }
                if ($is_commit && $data[MDL_SPS_ORDER_COL_RESDATA] == SPS_DATECOMMIT_OK) {
                    $is_commit = false;
                }
                if ($is_cancel && $data[MDL_SPS_ORDER_COL_RESDATA] == SPS_CANCEL_OK) {
                    $is_cancel = false;
                }
                $arrData[$key]['is_commit'] = $is_commit;
                $arrData[$key]['is_cancel'] = $is_cancel;
            }
        }

        return $arrData;
    }

    function lfDoUpdate($arrOrderId, &$objFormParam) {
        $arrErr = array();
        $ymd = sprintf('%04d%02d%02d', $objFormParam->getValue('sps_year'),
                                        $objFormParam->getValue('sps_month'),
                                        $objFormParam->getValue('sps_day'));
        $objSPS =& SC_Mdl_SPS_Ex::getInstance();
        $objSPS->printLog("lfDoUpdate(Every): pdate=$ymd order_ids=" . implode(',', $arrOrderId));

        foreach ($arrOrderId as $order_id) {
            if (SC_Utils_Ex::isBlank($order_id)) {
                continue;
            }

            $objQuery =& SC_Query_Ex::getSingletonInstance();
            $objQuery->begin();

            $res = SC_Mdl_SPS_Helper_Ex::doPaymentReAuth($order_id, $ymd);
            if ($res === false) {
                $arrErr[] = '処理出来ない注文です。 受注ID:' . $order_id;
            } else if($res === true) {
                $arrErr[] = '正常に登録をしました。 受注ID:' . $order_id;
            } else {
                $arrErr[] = $res .  ' 受注ID:' . $order_id;
            }
            $objQuery->commit();
        }

        return implode("\n", $arrErr);
    }

    function lfDoCancel($arrOrderId, &$objFormParam) {
        $arrErr = array();
        $ymd = sprintf('%04d%02d%02d', $objFormParam->getValue('sps_year'),
                                        $objFormParam->getValue('sps_month'),
                                        $objFormParam->getValue('sps_day'));
        $ymd = date('Ymd');
        $objSPS =& SC_Mdl_SPS_Ex::getInstance();
        $objSPS->printLog("lfDoCancel(Every): pdate=$ymd order_ids=" . implode(',', $arrOrderId));

        foreach ($arrOrderId as $order_id) {
            if (SC_Utils_Ex::isBlank($order_id)) {
                continue;
            }

            $objQuery =& SC_Query_Ex::getSingletonInstance();
            $objQuery->begin();

            $res = SC_Mdl_SPS_Helper_Ex::doPaymentEveryStop($order_id, $ymd);
            if ($res === false) {
                $arrErr[] = '処理出来ない注文です。 受注ID:' . $order_id;
            } else if($res === true) {
                $arrErr[] = '正常に取消をしました。 受注ID:' . $order_id;
            } else {
                $arrErr[] = $res .  ' 受注ID:' . $order_id;
            }
            $objQuery->commit();
        }

        return implode("\n", $arrErr);
    }

    function loadData() {
        $arrRet = array();
        $objQuery =& SC_Query_Ex::getSingletonInstance();
        $where = "plugin_code = 'SpsEvery'";
        $arrData = $objQuery->getRow('*', 'dtb_plugin', $where);
        if (!SC_Utils_Ex::isBlank($arrData['free_field1'])) {
            $arrRet = unserialize($arrData['free_field1']);
        }
        return $arrRet;
    }
}
