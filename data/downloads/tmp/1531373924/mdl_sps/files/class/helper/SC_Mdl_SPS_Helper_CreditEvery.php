<?php
/**
 *
 * @copyright 2013 Spirit of Co.,Ltd. All Rights Reserved.
 * @link http://sbps.ec-cube.pro/
 *
 */
require_once(MDL_SPS_HELPEREX_PATH . 'SC_Mdl_SPS_Helper_Base_Ex.php');

/**
 * 決済個別用ヘルパー関数 クレジットクラス
 */
class SC_Mdl_SPS_Helper_CreditEvery extends SC_Mdl_SPS_Helper_Base_Ex {

    function init() {
        $this->payment_type_id = SPS_CREDIT_EVERY_LINK_PAYID;
        $this->payment_name = 'Credit3d';
    }

    function getPayMethod() {
        return 'credit3d';
    }

    function getConnectType() {
        return MDL_SPS_CONNECT_TYPE_LINK;
    }

    function getSpsPayType() {
        // 購入タイプ(固定　0:都度購入、1:継続購入)
        return '1';
    }

    function isGetStatus() {
//        return true;
        return false;
    }

    function getPaymentEveryStopApiId() {
        return 'ST02-00302-101';
    }

    function isEveryStop() {
        return true;
    }

    function getPaymentRuleDefault() {
        $arrData = parent::getPaymentRuleDefault();
        $arrData['rule_max'] = '2';
        $arrData['upper_rule'] = '9999999';
        $arrData['upper_rule_max'] = $arrData['upper_rule'];
        return $arrData;
    }

    function getPaymentBlocName() {
        return 'sps_creditevery';
    }

    function initParamPaymentInput(&$objPage, &$objFormParam, &$arrConfig) {
        parent::initParamPaymentInput($objPage, $objFormParam, $arrConfig);

        $key = 'commit_order_status';
        $objFormParam->addParam('決済後の受注状況', $key, INT_LEN, 'n',
                array('EXIST_CHECK','MAX_LENGTH_CHECK', 'NUM_CHECK'),
                SC_Utils_Ex::isBlank($arrConfig[$key]) ? ORDER_NEW : $arrConfig[$key]);

        $key = 'cancel_order_status';
        $objFormParam->addParam('課金解約受信時の受注状況', $key, INT_LEN, 'n',
                array('MAX_LENGTH_CHECK', 'NUM_CHECK'),
                SC_Utils_Ex::isBlank($arrConfig[$key]) ? ORDER_CANCEL : $arrConfig[$key]);

    }

    function checkErrorPaymentInput(&$objFormParam) {
        return parent::checkErrorPaymentInput($objFormParam);
    }

    function initParam(&$objFormParam, &$arrPayment, &$arrOrder) {
        parent::initParam($objFormParam, $arrPayment, $arrOrder);
    }

    function modeAction($mode, &$objFormParam, &$arrOrder, $objPage) {
        parent::modeAction($mode, $objFormParam, $arrOrder, $objPage);
    }

    function doPaymentRecvOK(&$arrParam, &$arrOrder, &$arrPayment) {
        $objPurchase = new SC_Helper_Purchase_Ex();

        $sqlval = array();
        $sqlval[MDL_SPS_ORDER_COL_TRANSID] = $arrParam['res_tracking_id'];
        $sqlval[MDL_SPS_ORDER_COL_EVERY] = SPS_EVERY_OK;

        $new_status =  $arrPayment['commit_order_status'];

        $sqlval[MDL_SPS_ORDER_COL_PAYVIEW] = $this->makePaymentMemo($arrParam, $arrPayment);

        $objPurchase->sfUpdateOrderStatus($arrOrder['order_id'], $new_status, null, null, $sqlval);
        if (SC_Helper_Mobile_Ex::gfIsMobileMailAddress($arrOrder['order_email'])) {
            $template_id = '2';
        } else {
            $template_id = '1';
        }

        $objMailHelper = new SC_Helper_Mail_Ex();
        $objMailHelper->sfSendOrderMail($arrOrder['order_id'], $template_id);
        return true;
    }

    function doPaymentRecvCR(&$arrParam, &$arrOrder, &$arrPayment) {
        $new_status = null;
        if (!SC_Utils_Ex::isBlank($arrPayment['cancel_order_status']) && $arrPayment['cancel_order_status'] != '0') {
            $new_status = $arrPayment['cancel_order_status'];
        }
        $objPurchase = new SC_Helper_Purchase_Ex();
        $sqlval[MDL_SPS_ORDER_COL_EVERY] = SPS_EVERY_CR;
        $objPurchase->sfUpdateOrderStatus($arrOrder['order_id'], $new_status, null, null, $sqlval);
        return true;
    }

    function createLinkData(&$arrOrder, &$arrPaymentInfo, &$objFormParam) {
        $arrPluginConfig = $this->loadPluginData();
        $arrPluginConfig['auto_charge_type'] = '1';
        $arrPluginConfig['service_type'] = '0';
        $arrPluginConfig['div_settele'] = '0';
        $arrPluginConfig['camp_type'] = $arrPluginConfig['camp_type'] ? '1' : '0';
        $arrPaymentInfo = array_merge($arrPaymentInfo, $arrPluginConfig);
        return parent::createLinkData($arrOrder, $arrPaymentInfo, $objFormParam);
    }

    function loadPluginData() {
        $arrRet = array();
        $objQuery =& SC_Query_Ex::getSingletonInstance();
        $where = "plugin_code = 'SpsEvery'";
        $arrData = $objQuery->getRow('*', 'dtb_plugin', $where);
        if (!SC_Utils_Ex::isBlank($arrData['free_field1'])) {
            $arrRet = unserialize($arrData['free_field1']);
        }
        return $arrRet;
    }

}
