<?php
/**
 *
 * @copyright 2013 Spirit of Co.,Ltd. All Rights Reserved.
 * @link http://sbps.ec-cube.pro/
 *
 */
require_once(MDL_SPS_HELPEREX_PATH . 'SC_Mdl_SPS_Helper_Base_Ex.php');

/**
 * 決済個別用ヘルパー関数 ConveniApiクラス
 */
class SC_Mdl_SPS_Helper_ConveniApi extends SC_Mdl_SPS_Helper_Base_Ex {

    function init() {
        $this->payment_type_id = SPS_CREDIT_API_PAYID;
        $this->payment_name = 'ConveniApi';
    }

    function getPayMethod() {
        return 'conveniapi';
    }

    function getPaymentBlocName() {
        return 'sps_cvsapi';
    }

    function getConnectType() {
        return MDL_SPS_CONNECT_TYPE_API;
    }

    function getSpsPayType() {
        // 購入タイプ(固定　0:都度購入、1:継続購入)
        return '0';
    }

    function getPaymentRequestApiId() {
        return 'ST01-00101-701';
    }

    function getPaymentCommitApiId() {
        return '';
    }

    function getPaymentGetStatusApiId() {
        return 'MG01-00101-701';
    }

    function isGetStatus() {
        return true;
    }

    function isThreePhase() {
        return false;
    }

    function isDoPayment() {
        return true;
    }

    function getPaymentRuleDefault() {
        $arrData = parent::getPaymentRuleDefault();
        $arrData['rule_max'] = '1';
        $arrData['upper_rule'] = '300000';
        $arrData['upper_rule_max'] = $arrData['upper_rule'];
        return $arrData;
    }

    function initParamPaymentInput(&$objPage, &$objFormParam, &$arrConfig) {
        parent::initParamPaymentInput($objPage, $objFormParam, $arrConfig);

        $objPage->arrCvs = $this->getConvenis();
        $objPage->arrAgency = $this->getAgency();

        $key = 'exec_order_status';
        $objFormParam->addParam('決済実行 受注状況', $key, INT_LEN, 'n',
                array('MAX_LENGTH_CHECK', 'NUM_CHECK'),
                SC_Utils_Ex::isBlank($arrConfig[$key]) ? '' : $arrConfig[$key]);

        $key = 'auth_order_status';
        $objFormParam->addParam('決済後の受注状況', $key, INT_LEN, 'n',
                array('EXIST_CHECK','MAX_LENGTH_CHECK', 'NUM_CHECK'),
                SC_Utils_Ex::isBlank($arrConfig[$key]) ? ORDER_PAY_WAIT : $arrConfig[$key]);

        $key = 'commit_order_status';
        $objFormParam->addParam('入金後の受注状況', $key, INT_LEN, 'n',
                array('MAX_LENGTH_CHECK', 'NUM_CHECK'),
                SC_Utils_Ex::isBlank($arrConfig[$key]) ? ORDER_PRE_END : $arrConfig[$key]);

        $key = 'cancel_order_status';
        $objFormParam->addParam('期限切れ後の受注状況', $key, INT_LEN, 'n',
                array('MAX_LENGTH_CHECK', 'NUM_CHECK'),
                SC_Utils_Ex::isBlank($arrConfig[$key]) ? ORDER_CANCEL : $arrConfig[$key]);

        $key = 'agency_type';
        $objFormParam->addParam('取次契約先', $key, INT_LEN, 'n',
                array('MAX_LENGTH_CHECK', 'NUM_CHECK', 'EXIST_CHECK'),
                SC_Utils_Ex::isBlank($arrConfig[$key]) ? '' : $arrConfig[$key]);

        $key = 'enable_webcvstype';
        $objFormParam->addParam('利用可能なコンビニ', $key, INT_LEN, 'n',
                array('MAX_LENGTH_CHECK', 'NUM_CHECK', 'EXIST_CHECK'),
                SC_Utils_Ex::isBlank($arrConfig[$key]) ? '' : $arrConfig[$key]);

        $key = 'payment_limit_day';
        $objFormParam->addParam('支払い期限日数', $key, INT_LEN, 'n',
                array('MAX_LENGTH_CHECK', 'NUM_CHECK', 'EXIST_CHECK'),
                SC_Utils_Ex::isBlank($arrConfig[$key]) ? '' : $arrConfig[$key]);

        foreach ($objPage->arrAgency as $agency_id => $agency_name) {
            foreach ($objPage->arrCvs as $cvstype => $cvs_name) {
                $key = 'mail_title_' . $agency_id . '_' . $cvstype;
                $title = $cvs_name . 'でのお支払い方法について';
                $objFormParam->addParam($cvs_name . '支払い説明タイトル', $key, STEXT_LEN, '',
                        array('MAX_LENGTH_CHECK', 'SPTAB_CHECK'),
                        SC_Utils_Ex::isBlank($arrConfig[$key]) ? $title : $arrConfig[$key]);

                $key = 'mail_body_' . $agency_id . '_' . $cvstype;
                $body = $this->getCvsPaymentMailDefault($agency_id, $cvstype);
                $objFormParam->addParam($cvs_name . '支払い説明本文', $key, MLTEXT_LEN, '',
                        array('MAX_LENGTH_CHECK', 'SPTAB_CHECK'),
                        SC_Utils_Ex::isBlank($arrConfig[$key]) ? $body : $arrConfig[$key]);
            }
        }

    }

    function getCvsPaymentMailDefault($agency_type, $cvstype) {
        $template_dir = MDL_SPS_TEMPLATE_PATH . 'admin/mail/';
        $file_name = 'cvs_' . $agency_type . '_' . $cvstype . '.txt';
        if (is_file($template_dir . $file_name)) {
            return file_get_contents($template_dir . $file_name);
        }
        return '';
    }

    function getPaymentMailDefault() {
        return '';
    }

    function checkErrorPaymentInput(&$objFormParam) {
        return parent::checkErrorPaymentInput($objFormParam);
    }

    function initParam(&$objFormParam, &$arrPayment, &$arrOrder) {
        parent::initParam($objFormParam, $arrPayment, $arrOrder);

        $objFormParam->addParam('支払いをするコンビニ', 'webcvstype', 3, 'n', array('EXIST_CHECK', 'MAX_LENGTH_CHECK', 'NUM_CHECK'));

    }

    /**
     * 入力内容のチェックを行なう.
     *
     * @param SC_FormParam $objFormParam SC_FormParam インスタンス
     * @return array 入力チェック結果の配列
     */
    function checkError(&$objFormParam) {
        $arrParam = $objFormParam->getHashArray();
        $objErr = new SC_CheckError_Ex($arrParam);
        $objErr->arrErr = $objFormParam->checkError();

        return $objErr->arrErr;
    }

    function makeCvsMemo(&$arrRes, &$arrPayment, $cvstype, &$arrOrder) {
        $arrMemo02 = array();

        $arrMemo02['title'] = array(
                'name'  => '',
                'value' => true
            );

        $arrMemo02['paylimit'] = array(
                'name' => '払込期限',
                'value' => substr($arrRes['bill_date'],0,4) . '年' . substr($arrRes['bill_date'],4,2) . '月' . substr($arrRes['bill_date'],6,2) . '日'
            );

        if (substr($arrRes['cvs_pay_data1'],0,4) == 'http'
            && substr($arrRes['cvs_pay_data2'],0,4) == 'http') {
            // 両方リンクの場合、　ウェルネットセブンイレブン
            if (SC_Helper_Mobile_Ex::gfIsMobileMailAddress($arrOrder['order_email'])) {
                $link = $arrRes['cvs_pay_data2'];
            } else {
                $link = $arrRes['cvs_pay_data1'];
            }
            $arrMemo02['paylink'] = array(
                'name' => '支払選択画面URL',
                'value' => $link
            );
        } else if (substr($arrRes['cvs_pay_data2'],0,4) == 'http' &&
                    strlen($arrRes['cvs_pay_data1']) == 18) {
            // 情報２はリンクで、情報１が18桁の場合　電算システムLoppi
            list($no1, $no2) = explode('-', $arrRes['cvs_pay_data1']);

            $arrMemo02['payno1'] = array(
                'name' => '受付番号',
                'value' => $no1
            );
            $arrMemo02['payno2'] = array(
                'name' => '確認番号',
                'value' => $no2
            );
            $arrMemo02['paylink'] = array(
                'name' => '払込票URL',
                'value' => $arrRes['cvs_pay_data2']
            );
        } else if (substr($arrRes['cvs_pay_data2'],0,4) == 'http') {
            // 情報２はリンクの場合
            $arrMemo02['payno'] = array(
                'name' => '払込票番号(決済番号)',
                'value' => $arrRes['cvs_pay_data1']
            );
            $arrMemo02['paylink'] = array(
                'name' => '払込票URL',
                'value' => $arrRes['cvs_pay_data2']
            );
        } else if (SC_Utils_Ex::isBlank($arrRes['cvs_pay_data2'])) {
            // 情報２が空の場合
            $arrMemo02['payno'] = array(
                'name' => '受付番号(決済番号)',
                'value' => $arrRes['cvs_pay_data1']
            );
        } else {
            if (strlen($arrRes['cvs_pay_data1']) > strlen($arrRes['cvs_pay_data2'])) {
                // 1のほうが長い場合
                $no1 = $arrRes['cvs_pay_data1']; // お客さま番号
                $no2 = $arrRes['cvs_pay_data2']; // 確認番号
            } else {
                // 2のほうが長い場合
                $no1 = $arrRes['cvs_pay_data2']; // お客さま番号
                $no2 = $arrRes['cvs_pay_data1']; // 確認番号
            }
            // それ以外
            $arrMemo02['payno1'] = array(
                'name' => 'お客様番号(注文番号)',
                'value' => $no1
            );
            $arrMemo02['payno2'] = array(
                'name' => '確認番号(企業コード)',
                'value' => $no2
            );
        }

        $agency_id = $arrPayment['agency_type'];

        $title_key = 'mail_title_' . $agency_id . '_' . $cvstype;
        $body_key = 'mail_body_' . $agency_id . '_' . $cvstype;

        if (SC_Utils_Ex::isBlank($arrPayment[$title_key]) || SC_Utils_Ex::isBlank($arrPayment[$body_key])) {
            return '';
        }

        $arrMemo02['payinfo'] = array(
                'name'  => $arrPayment[$title_key],
                'value' => $arrPayment[$body_key]
            );

        return serialize($arrMemo02);

    }

    function doPaymentRecvOK(&$arrParam, &$arrOrder, &$arrPayment, $arrInput) {
        $objPurchase = new SC_Helper_Purchase_Ex();

        $sqlval = array();
        $sqlval[MDL_SPS_ORDER_COL_TRANSID] = $arrParam['res_tracking_id'];

        $new_status = $arrPayment['auth_order_status'];

        $sqlval[MDL_SPS_ORDER_COL_PAYVIEW] = $this->makeCvsMemo($arrParam, $arrPayment, $arrInput['webcvstype'], $arrOrder);

        $objPurchase->sfUpdateOrderStatus($arrOrder['order_id'], $new_status, null, null, $sqlval);

        if (SC_Helper_Mobile_Ex::gfIsMobileMailAddress($arrOrder['order_email'])) {
            $template_id = '2';
        } else {
            $template_id = '1';
        }

        $objMailHelper = new SC_Helper_Mail_Ex();
        $objMailHelper->sfSendOrderMail($arrOrder['order_id'], $template_id);
        return true;
    }

    function doApiPaymentFix($arrRes, &$objPage, &$objFormParam, &$arrOrder, &$arrPayment) {
        $res = $this->doPaymentRecvOK($arrRes, $arrOrder, $arrPayment, $objFormParam->getHashArray());

        if ($res) {
            $this->actionCompletePage($objFormParam, $arrOrder, $arrPayment);
        }
    }

    function getApiPayMethodInfoData(&$arrInput, &$arrPayment, &$arrOrder) {
        $masterData = new SC_DB_MasterData_Ex();
        $arrPref = $masterData->getMasterData('mtb_pref');

        $arrData = array();

        $arrData['issue_type'] = '0';
        $arrData['last_name'] = mb_convert_kana($arrOrder['order_name01'], 'ASKV');
        $arrData['first_name'] =mb_convert_kana($arrOrder['order_name02'], 'ASKV');
        $arrData['last_name_kana'] = mb_convert_kana($arrOrder['order_kana01'],'ASKV');
        $arrData['first_name_kana'] = mb_convert_kana($arrOrder['order_kana02'], 'ASKV');
        $arrData['first_zip'] = $arrOrder['order_zip01'];
        $arrData['second_zip'] = $arrOrder['order_zip02'];
        $arrData['add1'] = $arrPref[$arrOrder['order_pref']];
        $arrData['add2'] = mb_convert_kana($arrOrder['order_addr01'], 'ASKV');
        $arrData['add3'] = mb_convert_kana($arrOrder['order_addr02'], 'ASKV');
        $arrData['tel'] = $arrOrder['order_tel01'] . $arrOrder['order_tel02'] . $arrOrder['order_tel03'];
        $arrData['mail'] = $arrOrder['order_email'];
        $arrData['seiyakudate'] = date('Ymd', strtotime($arrOrder['create_date']));

        $arrData['webcvstype'] = $arrInput['webcvstype'];

        $arrData['bill_date'] = date('Ymd', mktime(23,59,59, date('m'), date('d') + $arrPayment['payment_limit_day'], date('Y')));

        return $arrData;
    }

    function doApiPaymentGetStatus(&$arrOrder, $arrPayment, $target_datetime) {
        $objMdl =& SC_Mdl_SPS_Ex::getInstance();
        $arrModuleConfig = $objMdl->getUserSettings();

        $arrData = $this->createGetStatusXmlData($arrOrder, $arrPayment, $target_datetime);
        $request_xml = SC_Util_SPS_Ex::changeArray2Xml($arrData, $this->getPaymentGetStatusApiId());
        list($r_code, $response) = SC_Util_SPS_Ex::sendRequestSps($request_xml, $arrModuleConfig);
        $arrRes = $this->checkResponse($r_code, $response, $this);

        if ($arrRes !== false) {
            $sqlval = array();
            switch ($arrRes['payment_status']) {
            case '1'://未入金
            case '2'://一部入金済
                $new_status = $arrPayment['auth_order_status'];
                break;
            case '3'://満額入金
            case '4'://過入金
                $new_status = $arrPayment['commit_order_status'];
                break;
            case '9'://キャンセル
                $new_status = $arrPayment['cancel_order_status'];
                $sqlval[MDL_SPS_ORDER_COL_RESDATA] = SPS_CANCEL_OK;
                break;
            }
            SC_Util_SPS_Ex::setOrderPayData($arrOrder, $arrRes, false, $new_status, $sqlval);
        } else {
            $sqlva[MDL_SPS_ORDER_COL_RESDATA] = SPS_ERROR;
            SC_Util_SPS_Ex::setOrderPayData($arrOrder, $arrRes, true, $new_status, $sqlval);
            return $this->arrErr['tpl_error'];
        }
        return true;
    }

    function createGetStatusXmlData(&$arrOrder, &$arrPayment, $target_datetime) {
        $arrData = array();
        $objMdl =& SC_Mdl_SPS_Ex::getInstance();
        $arrModuleConfig = $objMdl->getUserSettings();
        $now_time = date("YmdHis");

        $arrData['merchant_id'] = $arrModuleConfig['id_sps_marchant'];
        $arrData['service_id'] = $arrModuleConfig['id_sps_service'];
        $arrData['tracking_id'] = $arrOrder[MDL_SPS_ORDER_COL_TRANSID];
        $arrData['encrypted_flg'] = '1';
        $arrData['request_date'] = $now_time;
//        $arrData['limit_second'] = '600';

        $arrData = $this->normalizeData($arrData, false);
        $arrData['sps_hashcode'] = $this->createSpsHash($arrData);
        return $arrData;

    }

    function modeAction($mode, &$objFormParam, &$arrOrder, $objPage) {

        $objPage->arrCvs = $this->getEnableCvs($objPage->arrPayment);
        if ($mode == 'next' && $objPage->getMode() == '') {
            $mode = '';
        }

        parent::modeAction($mode, $objFormParam, $arrOrder, $objPage);
    }

    function getConvenis() {
        $arrData = array(
            '001' => 'セブンイレブン',
            '002' => 'ローソン',
            '016' => 'ファミリーマート',
            '017' => 'サークルＫサンクス',
            '018' => 'セイコーマート',
            '005' => 'ミニストップ',
            '010' => 'デイリーヤマザキ',
        );
        return $arrData;
    }

    function getAgency() {
        $arrData = array(
            '3' => '電算システム',
            '2' => 'i-コレクト',
            '1' => 'ウェルネット',
        );
        return $arrData;
    }

    function getEnableCvs($arrPayment) {
        $arrDefault = $this->getConvenis();
        $arrData = array();
        foreach ($arrPayment['enable_webcvstype'] as $id) {
            $arrData[ $id ] = $arrDefault[$id];
        }
        return $arrData;
    }


    function doApiPayment(&$objPage, &$objFormParam, &$arrOrder, &$arrPaymentInfo, $no_fix = false)
    {
        SC_Util_SPS_Ex::setOrderPayData($arrOrder, $objFormParam->getHashArray());
        if ($arrPaymentInfo['exec_order_status'] == ''
                || $no_fix === true) {
            // 決済実行
            return parent::doApiPayment($objPage, $objFormParam, $arrOrder, $arrPaymentInfo, $no_fix);
        }

        // 決済実行保留
        $objPurchase = new SC_Helper_Purchase_Ex();

        $sqlval = array();
        $new_status = $arrPaymentInfo['exec_order_status'];

        $objPurchase->sfUpdateOrderStatus($arrOrder['order_id'], $new_status, null, null, $sqlval);

        if (SC_Helper_Mobile_Ex::gfIsMobileMailAddress($arrOrder['order_email'])) {
            $template_id = '2';
        } else {
            $template_id = '1';
        }

        $objMailHelper = new SC_Helper_Mail_Ex();
        $objMailHelper->sfSendOrderMail($arrOrder['order_id'], $template_id);

        $this->actionCompletePage($objFormParam, $arrOrder, $arrPaymentInfo);
    }
}
