<?php
/**
 *
 * @copyright 2015 Spirit of Co.,Ltd. All Rights Reserved.
 * @link http://sbps.ec-cube.pro/
 *
 */
require_once(MDL_SPS_HELPEREX_PATH . 'SC_Mdl_SPS_Helper_Base_Ex.php');

/**
 * 決済個別用ヘルパー関数 Rakuten2(非ギャザ)クラス
 */
class SC_Mdl_SPS_Helper_Rakuten2 extends SC_Mdl_SPS_Helper_Base_Ex {

    function init() {
        $this->payment_type_id = SPS_RAKUTEN2_LINK_PAYID;
        $this->payment_name = 'Rakuten2';
    }

    function getPayMethod() {
        return 'rakuten';
    }

    function getConnectType() {
        return MDL_SPS_CONNECT_TYPE_LINK;
    }

    function getSpsPayType() {
        // 購入タイプ(固定　0:都度購入、1:継続購入)
        return '0';
    }

    function getPaymentSalesApiId() {
        return 'ST02-00201-305';
    }

    function getPaymentCancelApiId() {
        return 'ST02-00306-305';
    }

    function getPaymentReAuthApiId() {
        return 'ST02-00502-305';
    }

    function getPaymentGetStatusApiId() {
        return '';
    }

    function isSales() {
        return true;
    }

    function isCancel() {
        return true;
    }

    function isReAuth() {
        return true;
    }

    function isGetStatus() {
        return false;
    }

    function getPaymentRuleDefault() {
        $arrData = parent::getPaymentRuleDefault();
        $arrData['rule_max'] = '100';
        $arrData['upper_rule'] = '100000';
        $arrData['upper_rule_max'] = $arrData['upper_rule'];
        return $arrData;
    }

    function getPaymentBlocName() {
        return 'sps_rakuten2';
    }

    function initParamPaymentInput(&$objPage, &$objFormParam, &$arrConfig) {
        parent::initParamPaymentInput($objPage, $objFormParam, $arrConfig);

        $key = 'auth_order_status';
        $objFormParam->addParam('与信後の受注状況', $key, INT_LEN, 'n',
                array('EXIST_CHECK','MAX_LENGTH_CHECK', 'NUM_CHECK'),
                SC_Utils_Ex::isBlank($arrConfig[$key]) ? ORDER_NEW : $arrConfig[$key]);

        $key = 'reauth_order_status';
        $objFormParam->addParam('金額変更確定後の受注状況', $key, INT_LEN, 'n',
                array('EXIST_CHECK','MAX_LENGTH_CHECK', 'NUM_CHECK'),
                SC_Utils_Ex::isBlank($arrConfig[$key]) ? ORDER_NEW : $arrConfig[$key]);

        $key = 'commit_order_status';
        $objFormParam->addParam('売上確定後の受注状況', $key, INT_LEN, 'n',
                array('MAX_LENGTH_CHECK', 'NUM_CHECK'),
                SC_Utils_Ex::isBlank($arrConfig[$key]) ? ORDER_PRE_END : $arrConfig[$key]);

        $key = 'cancel_order_status';
        $objFormParam->addParam('取消後の受注状況', $key, INT_LEN, 'n',
                array('MAX_LENGTH_CHECK', 'NUM_CHECK'),
                SC_Utils_Ex::isBlank($arrConfig[$key]) ? ORDER_CANCEL : $arrConfig[$key]);

        $key = 'wait_order_status';
        $objFormParam->addParam('処理待ちの受注状況', $key, INT_LEN, 'n',
                array('MAX_LENGTH_CHECK', 'NUM_CHECK'),
                SC_Utils_Ex::isBlank($arrConfig[$key]) ? ORDER_PAY_WAIT : $arrConfig[$key]);
    }

    function checkErrorPaymentInput(&$objFormParam) {
        return parent::checkErrorPaymentInput($objFormParam);
    }

    function initParam(&$objFormParam, &$arrPayment, &$arrOrder) {
        parent::initParam($objFormParam, $arrPayment, $arrOrder);
    }

    function modeAction($mode, &$objFormParam, &$arrOrder, $objPage) {
        parent::modeAction($mode, $objFormParam, $arrOrder, $objPage);
    }

    function doPaymentRequest (&$objPage, &$objFormParam, &$arrOrder, &$arrPaymentInfo) {
        $this->setSJISConv();   // SJIS出力

        $objMdl =& SC_Mdl_SPS_Ex::getInstance();
        $connect_type = $this->getConnectType();

        switch ($connect_type) {
        case MDL_SPS_CONNECT_TYPE_LINK:
            $arrData = $this->createLinkData($arrOrder, $arrPaymentInfo, $objFormParam);
            $objMdl->printLog($arrData);
            $objPage->arrSpsData = $arrData;
            $objPage->tpl_next_form = 'form_sps';
            $objPage->sps_url = $this->getRequestUrl();
            // 楽天は遷移時のロゴ表示が必須なので自動遷移無し
//            $objPage->tpl_payment_onload = 'fnAutoLoadSubmitSPS();';
            break;
        case MDL_SPS_CONNECT_TYPE_API:
            break;
        case MDL_SPS_CONNECT_TYPE_FREECSV:
            break;
        }

        return true;
    }


    function doPaymentRecvOK(&$arrParam, &$arrOrder, &$arrPayment) {
        $sqlval = array();
        $is_send_mail = false;

        switch($arrParam['res_payinfo_key']) {
            case 'R01':
                $new_status = $arrPayment['auth_order_status'];                
                $is_send_mail = true;
                break;
            case 'R02':
                $new_status = $arrPayment['reauth_order_status'];
                break;
            case 'R03':
                $new_status = $arrPayment['commit_order_status'];
                break;
            default:
                if ($arrParam['res_payinfo_key'] != '') {
                    $new_status = $arrPayment['auth_order_status'];
                    $is_send_mail = true;
                } else {
                    $new_status = $arrPayment['wait_order_status'];
                }
                break;
        }

        $objPurchase = new SC_Helper_Purchase_Ex();

        $sqlval = array();
        $sqlval[MDL_SPS_ORDER_COL_TRANSID] = $arrParam['res_tracking_id'];

        $objPurchase->sfUpdateOrderStatus($arrOrder['order_id'], $new_status, null, null, $sqlval);

        if ($is_send_mail) {
            if (SC_Helper_Mobile_Ex::gfIsMobileMailAddress($arrOrder['order_email'])) {
                $template_id = '2';
            } else {
                $template_id = '1';
            }

            $objMailHelper = new SC_Helper_Mail_Ex();
            $objMailHelper->sfSendOrderMail($arrOrder['order_id'], $template_id);
        }
        return true;
    }

    function doApiPaymentReAuth(&$arrOrder, $arrPayment, $target_datetime) {
        $objMdl =& SC_Mdl_SPS_Ex::getInstance();
        $arrModuleConfig = $objMdl->getUserSettings();

        $arrData = $this->createReAuthXmlData($arrOrder, $arrPayment, $target_datetime);
        $request_xml = SC_Util_SPS_Ex::changeArray2Xml($arrData, $this->getPaymentReAuthApiId());
        list($r_code, $response) = SC_Util_SPS_Ex::sendRequestSps($request_xml, $arrModuleConfig);
        $arrRes = $this->checkResponse($r_code, $response, $this);
        if ($arrRes !== false) {
            if ($arrRes['result'] == 'OK') {
                $sqlval[MDL_SPS_ORDER_COL_RESDATA] = SPS_WAIT;
                $sqlval[MDL_SPS_ORDER_COL_REQDATE] = $arrData['request_date'];
                $sqlval[MDL_SPS_ORDER_COL_TRANSID] = $arrRes['res_tracking_id'];
                if ($arrPayment['wait_order_status']) {
                    $new_status = $arrPayment['wait_order_status'];
                }
                SC_Util_SPS_Ex::setOrderPayData($arrOrder, $arrRes, false, $new_status, $sqlval);
            }
        } else {
            $sqlval[MDL_SPS_ORDER_COL_RESDATA] = SPS_ERROR;
            SC_Util_SPS_Ex::setOrderPayData($arrOrder, $arrRes, true, $new_status, $sqlval);
            return $this->arrErr['tpl_error'];
        }
        return true;
    }

    function doApiPaymentCancel(&$arrOrder, $arrPayment, $target_datetime, $is_update = true) {
        $objMdl =& SC_Mdl_SPS_Ex::getInstance();
        $arrModuleConfig = $objMdl->getUserSettings();
        $arrData = $this->createCancelXmlData($arrOrder, $arrPayment, $target_datetime);
        $request_xml = SC_Util_SPS_Ex::changeArray2Xml($arrData, $this->getPaymentCancelApiId());
        list($r_code, $response) = SC_Util_SPS_Ex::sendRequestSps($request_xml, $arrModuleConfig);
        $arrRes = $this->checkResponse($r_code, $response, $this);
        if ($arrRes !== false) {
            $sqlval = array();
            $sqlval[MDL_SPS_ORDER_COL_RESDATA] = SPS_CANCEL_OK;
            if (!SC_Utils_Ex::isBlank($arrPayment['cancel_order_status'])) {
                $new_status = $arrPayment['cancel_order_status'];
            } else {
                $new_status = null;
            }
            if ($is_update) {
                SC_Util_SPS_Ex::setOrderPayData($arrOrder, $arrRes, false, $new_status, $sqlval);
            } else {
                SC_Util_SPS_Ex::setOrderPayData($arrOrder, $arrRes, true);
            }
        } else {
            $sqlval[MDL_SPS_ORDER_COL_RESDATA] = SPS_ERROR;
            SC_Util_SPS_Ex::setOrderPayData($arrOrder, $arrRes, true, $new_status, $sqlval);
            return $this->arrErr['tpl_error'];
        }
        return true;                
    }

    function createSalesXmlData(&$arrOrder, &$arrPayment, $target_datetime) {
        $arrData = array();
        $objMdl =& SC_Mdl_SPS_Ex::getInstance();
        $arrModuleConfig = $objMdl->getUserSettings();
        $now_time = date("YmdHis");

        $arrData['merchant_id'] = $arrModuleConfig['id_sps_marchant'];
        $arrData['service_id'] = $arrModuleConfig['id_sps_service'];
        $arrData['tracking_id'] = $arrOrder[MDL_SPS_ORDER_COL_TRANSID];
        if (SC_Utils_Ex::isBlank($arrData['tracking_id'])) {
            $arrData['tracking_id'] = $arrOrder['res_tracking_id'];
        }
        $arrData['request_date'] = $now_time;
        $arrData['limit_second'] = '600';

        $arrData = $this->normalizeData($arrData, false);
        $arrData['sps_hashcode'] = $this->createSpsHash($arrData);
        return $arrData;
    }

    function createCancelXmlData(&$arrOrder, &$arrPayment, $target_datetime) {
        $arrData = array();
        $objMdl =& SC_Mdl_SPS_Ex::getInstance();
        $arrModuleConfig = $objMdl->getUserSettings();
        $now_time = date("YmdHis");

        $arrData['merchant_id'] = $arrModuleConfig['id_sps_marchant'];
        $arrData['service_id'] = $arrModuleConfig['id_sps_service'];
        $arrData['tracking_id'] = $arrOrder[MDL_SPS_ORDER_COL_TRANSID];
        $arrData['request_date'] = $now_time;
        $arrData['limit_second'] = '600';

        $arrData = $this->normalizeData($arrData, false);
        $arrData['sps_hashcode'] = $this->createSpsHash($arrData);
        return $arrData;
    }

    function createReAuthXmlData(&$arrOrder, &$arrPayment, $target_datetime) {
        $arrData = array();
        $objMdl =& SC_Mdl_SPS_Ex::getInstance();
        $arrModuleConfig = $objMdl->getUserSettings();
        $now_time = date("YmdHis");

        $objDb = new SC_Helper_DB_Ex();
        $arrInfo = $objDb->sfGetBasisData();

        $arrData['merchant_id'] = $arrModuleConfig['id_sps_marchant'];
        $arrData['service_id'] = $arrModuleConfig['id_sps_service'];
        $arrData['tracking_id'] = $arrOrder[MDL_SPS_ORDER_COL_TRANSID];
        if (SC_Utils_Ex::isBlank($arrData['tracking_id'])) {
            $arrData['tracking_id'] = $arrOrder['res_tracking_id'];
        }
        $arrData['amount'] = $arrOrder['payment_total'];
        $arrData['request_date'] = $now_time;
        $arrData['limit_second'] = '600';

        $arrData = $this->normalizeData($arrData, false);
        $arrData['sps_hashcode'] = $this->createSpsHash($arrData);
        return $arrData;
    }
}
