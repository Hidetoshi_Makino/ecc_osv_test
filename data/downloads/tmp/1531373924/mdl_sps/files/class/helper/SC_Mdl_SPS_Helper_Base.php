<?php
/**
 *
 * @copyright 2013 Spirit of Co.,Ltd. All Rights Reserved.
 * @link http://sbps.ec-cube.pro/
 *
 */

/**
 * 決済個別用ヘルパー基底関数クラス
 */
class SC_Mdl_SPS_Helper_Base {

    var $payment_type_id;
    var $payment_name;
    var $arrErr;

    function init() {
        $this->payment_type_id = '0';
        $this->payment_name = 'base';
        $this->arrErr = array();
    }

    function getConnectType() {
//        return MDL_SPS_CONNECT_TYPE_API;
        return MDL_SPS_CONNECT_TYPE_LINK;
//        return MDL_SPS_CONNECT_TYPE_FREECSV;
    }

    function isThreePhase() {
        return false;
    }

    function getPaymentRequestApiId() {
        return '';
    }

    function getPaymentCommitApiId() {
        return 'ST02-00101-101';
    }

    function getPaymentSalesApiId() {
        return 'ST02-00201-101';
    }

    function getPaymentCancelApiId() {
        return 'ST02-00303-101';
    }

    function getPaymentRefoundApiId() {
        return 'ST02-00303-101';
    }

    function getPaymentReAuthApiId() {
        return 'ST01-00113-101';
    }

    function getPaymentGetStatusApiId() {
        return 'MG01-00101-101';
    }

    function getPaymentEveryStopApiId() {
        return 'ST02-00302-101';
    }

    function isSales() {
        return false;
    }

    function isCancel() {
        return false;
    }

    function isReAuth() {
        return false;
    }

    function isGetStatus() {
        return false;
    }

    function isEveryStop() {
        return false;
    }

    function isDoPayment() {
        return false;
    }

    function getPayMethod() {
        return '';
    }

    function getPaymentRuleDefault() {
        $arrData = array();
        $arrData['charge'] = '0';
        $arrData['rule_max'] = '1';
        $arrData['upper_rule'] = '999999';
        $arrData['upper_rule_max'] = $arrData['upper_rule'];
        return $arrData;
    }

    /**
     * 画面に設定するフォーム用ブロック名を返す
     *
     * @return text テンプレートブロック名
     */
    function getPaymentBlocName() {
        return 'dummy';
    }

    function getSpsPayType() {
        // 購入タイプ(固定　0:都度購入、1:継続購入)
        return '0';
    }


    /**
     * 画面に設定するフォーム用ブロックパスを返す
     *
     * @return text テンプレートブロックパス
     */
    function getFormBloc() {
        $bloc_name = $this->getPaymentBlocName();
        if ($bloc_name == '') {
            return '';
        }
        $objMdl =& SC_Mdl_SPS_Ex::getInstance();
        $arrBlocId = $objMdl->getSubData('bloc_setting');
        $device_type_id = SC_Display_Ex::detectDevice();
        $bloc_id =  $arrBlocId[$bloc_name][ $device_type_id ];
        if ($bloc_id) {
            $objLayout = new SC_Helper_PageLayout_Ex();
            $arrBloc = $objLayout->getBlocs($device_type_id, 'bloc_id = ?', array($bloc_id), true);
            if (is_file($arrBloc[0]['tpl_path'])) {
                return $arrBloc[0]['tpl_path'];
            }
        }
        switch ($device_type_id) {
        case DEVICE_TYPE_MOBILE:
            $tpl = MDL_SPS_TEMPLATE_PATH . "mobile/bloc/" . $bloc_name . '.tpl';
            break;
        case DEVICE_TYPE_SMARTPHONE:
            $tpl = MDL_SPS_TEMPLATE_PATH . "sphone/bloc/" . $bloc_name . '.tpl';
            break;
        default:
            $tpl = MDL_SPS_TEMPLATE_PATH . "default/bloc/" . $bloc_name . '.tpl';
        }
        if (is_file($tpl)) {
            return $tpl;
        }
        return null;
    }

    function initParam(&$objFormParam, &$arrPayment, &$arrOrder) {
        if ($arrPayment['is_enable_config'] != '1') {
            $objMdl =& SC_Mdl_SPS_Ex::getInstance();
            $msg = "例外エラー<br />決済の詳細設定がありません。支払い方法の設定をして下さい。<br />この手続きは無効となりました。<br />管理者に連絡をして下さい。";
            $objMdl->printLog($msg);
            SC_Utils_Ex::sfDispSiteError(FREE_ERROR_MSG, "", true, $msg);
            SC_Response_Ex::actionExit();
        }
        $objFormParam->addParam('処理結果ステータス', 'res_result', 2, 'a', array('MAX_LENGTH_CHECK', 'ALPHA_CHECK'));
        $objFormParam->addParam('処理結果', 'ecr', STEXT_LEN, 'a', array('MAX_LENGTH_CHECK', 'ALPHA_CHECK'));
        $objFormParam->addParam('エラーコード', 'res_err_code', STEXT_LEN, 'a', array('MAX_LENGTH_CHECK'));

    }

    function actionReturn() {
        $objMdl =& SC_Mdl_SPS_Ex::getInstance();
        $objMdl->printLog("actoin return. rollback order order_id=" . $_SESSION['order_id']);
        $objPurchase = new SC_Helper_Purchase_Ex();
        $objPurchase->rollbackOrder($_SESSION['order_id'], ORDER_CANCEL, true);
        SC_Response_Ex::sendRedirect(SHOPPING_CONFIRM_URLPATH);
        SC_Response_Ex::actionExit();
    }

    function actionNext(&$objPage, &$objFormParam, &$arrOrder, &$arrPayment) {
        $objPage->arrErr = $this->checkError($objFormParam);
        if (SC_Utils_Ex::isBlank($objPage->arrErr)) {

            $res = $this->doPaymentRequest($objPage, $objFormParam, $arrOrder, $objPage->arrPayment);
            if (!$res) {
                $objPage->tpl_payment_onload = '';
            }
        }
    }

    function actionCompletePage(&$objFormParam, &$arrOrder, &$arrPayment) {
        $objMdl =& SC_Mdl_SPS_Ex::getInstance();
        $objMdl->printLog("actoin complete. order_id=" . $_SESSION['order_id']);
        SC_Response_Ex::sendRedirect(SHOPPING_COMPLETE_URLPATH);
        SC_Response_Ex::actionExit();
    }

    function actionSpsReturn(&$objPage, &$objFormParam, &$arrOrder, &$arrPayment) {

        $objPage->arrErr = $this->checkError($objFormParam);
        if (SC_Utils_Ex::isBlank($objPage->arrErr)) {

            switch ($objFormParam->getValue('ecr')) {
            case 'success':
                if ($objFormParam->getValue('res_result') == 'OK'
                        && ($arrOrder['status'] == $arrPayment['auth_order_status']
                            || $arrOrder['status'] == $arrPayment['commit_order_status'])
                        ) {
                    $this->actionCompletePage($objFormParam, $arrOrder, $arrPayment);
                }
            case 'cancel':
                $objPage->arrErr['message'] = '操作がキャンセルされました。';
                $objPage->arrErr['res_err_code'] = 'コード:' . $objFormParam->getValue('res_err_code');
                break;
            case 'error':
            default:
                $objPage->arrErr['res_err_code'] = 'エラーコード:' . $objFormParam->getValue('res_err_code');
                break;
            }
        }
    }

    function modeAction($mode, &$objFormParam, &$arrOrder, $objPage) {
        $objMdl =& SC_Mdl_SPS_Ex::getInstance();
        $objMdl->printLog("modeAction mode=$mode order={$arrOrder['order_id']} customer_id={$arrOrder['customer_id']}");

        switch($mode) {
        case 'return':
            $this->actionReturn();
            break;
        case 'next':
            $this->actionNext($objPage, $objFormParam, $arrOrder, $objPage->arrPayment);
            break;
        case 'spsreturn':
            $this->actionSpsReturn($objPage, $objFormParam, $arrOrder, $objPage->arrPayment);
            break;
        default:
            if ($this->getConnectType() == MDL_SPS_CONNECT_TYPE_LINK) {
                $objPage->tpl_payment_onload = 'fnAutoLoadSubmit();';
            }
            break;
        }
    }

    /**
     * 入力内容のチェックを行なう.
     *
     * @param SC_FormParam $objFormParam SC_FormParam インスタンス
     * @return array 入力チェック結果の配列
     */
    function checkError(&$objFormParam) {
        $arrParam = $objFormParam->getHashArray();
        $objErr = new SC_CheckError_Ex($arrParam);
        $objErr->arrErr = $objFormParam->checkError();
        return $objErr->arrErr;
    }


    function initParamPaymentInput(&$objPage, &$objFormParam, &$arrConfig) {
        $masterData = new SC_DB_MasterData_Ex();
        $objPage->arrORDERSTATUS = $masterData->getMasterData('mtb_order_status');
        // 決済処理中は遷移制御に使うので選択不可
        unset($objPage->arrORDERSTATUS[ORDER_PENDING]);

        $key = 'is_enable_config';
        $objFormParam->addParam('個別設定済みフラグ', $key, INT_LEN, 'n',
                array('EXIST_CHECK','MAX_LENGTH_CHECK'),
                SC_Utils_Ex::isBlank($arrConfig[$key]) ? '' : $arrConfig[$key]);
        $key = 'free3';
        $objFormParam->addParam('自由欄', $key, 20, 'n',
                array('MAX_LENGTH_CHECK'),
                SC_Utils_Ex::isBlank($arrConfig[$key]) ? '' : $arrConfig[$key]);

        $key = 'mail_title';
        $title = $objPage->arrForm['payment_method']['value'] . 'でのお支払い方法について';
        $objFormParam->addParam('支払い説明タイトル', $key, STEXT_LEN, '',
                array('MAX_LENGTH_CHECK', 'SPTAB_CHECK'),
                SC_Utils_Ex::isBlank($arrConfig[$key]) ? $title : $arrConfig[$key]);

        $key = 'mail_body';
        $body = $this->getPaymentMailDefault();
        $objFormParam->addParam('支払い説明本文', $key, MLTEXT_LEN, '',
                array('MAX_LENGTH_CHECK', 'SPTAB_CHECK'),
                SC_Utils_Ex::isBlank($arrConfig[$key]) ? $body : $arrConfig[$key]);

    }

    function getPaymentMailDefault() {
        $template_dir = MDL_SPS_TEMPLATE_PATH . 'admin/mail/';
        $file_name = $this->getPaymentBlocName() .'.txt';
        if (is_file($template_dir . $file_name)) {
            return file_get_contents($template_dir . $file_name);
        }
        return '';
    }

    function makePaymentMemo(&$arrRes, &$arrPayment) {
        if (SC_Utils_Ex::isBlank($arrPayment['mail_title']) || SC_Utils_Ex::isBlank($arrPayment['mail_body'])) {
            return '';
        }
        $arrMemo02 = array();

        $arrMemo02['title'] = array(
                'name'  => '',
                'value' => true
            );

        $arrMemo02['payinfo'] = array(
                'name'  => $arrPayment['mail_title'],
                'value' => $arrPayment['mail_body']
            );

        return serialize($arrMemo02);
    }

    function checkErrorPaymentInput(&$objFormParam) {
        $arrRet =  $objFormParam->getHashArray();
        $objErr = new SC_CheckError_Ex($arrRet);
        $objErr->arrErr = $objFormParam->checkError();
        return $objErr->arrErr;
    }

    function doPaymentRecvOK(&$arrParam, &$arrOrder, &$arrPayment, $arrInput = array()) {
        $objPurchase = new SC_Helper_Purchase_Ex();

        $sqlval = array();
        $sqlval[MDL_SPS_ORDER_COL_TRANSID] = $arrParam['res_tracking_id'];

        if ($arrPayment['sps_auth_type'] == SPS_AUTH_AUTO) {
            $new_status =  $arrPayment['commit_order_status'];
            if (SC_Utils_Ex::isBlank($new_status) || $new_status == '0') {
                $new_status = $arrPayment['auth_order_status'];
            }
        } else {
            $new_status = $arrPayment['auth_order_status'];
        }

        $sqlval[MDL_SPS_ORDER_COL_PAYVIEW] = $this->makePaymentMemo($arrParam, $arrPayment);

        $objPurchase->sfUpdateOrderStatus($arrOrder['order_id'], $new_status, null, null, $sqlval);
        if (SC_Helper_Mobile_Ex::gfIsMobileMailAddress($arrOrder['order_email'])) {
            $template_id = '2';
        } else {
            $template_id = '1';
        }

        $objMailHelper = new SC_Helper_Mail_Ex();
        $objMailHelper->sfSendOrderMail($arrOrder['order_id'], $template_id);
        return true;
    }

    function doPaymentRecvNG(&$arrParam, &$arrOrder, &$arrPayment) {
        $sqlval = array();
        if (!SC_Utils_Ex::isBlank($arrPayment['cancel_order_status']) && $arrPayment['cancel_order_status'] != '0') {
            $objPurchase = new SC_Helper_Purchase_Ex();
            $new_status = $arrPayment['cancel_order_status'];
            $objPurchase->sfUpdateOrderStatus($arrOrder['order_id'], $new_status, null, null, $sqlval);
        }
        return true;
    }

    function doPaymentRecvCR(&$arrParam, &$arrOrder, &$arrPayment) {
        $sqlval = array();
        if (!SC_Utils_Ex::isBlank($arrPayment['cancel_order_status']) && $arrPayment['cancel_order_status'] != '0') {
            $objPurchase = new SC_Helper_Purchase_Ex();
            $sqlval[MDL_SPS_ORDER_COL_EVERY] = SPS_EVERY_CR;
            $new_status = $arrPayment['cancel_order_status'];
            $objPurchase->sfUpdateOrderStatus($arrOrder['order_id'], $new_status, null, null, $sqlval);
        }
        return true;
    }

    function doPaymentRecvCC(&$arrParam, &$arrOrder, &$arrPayment) {
        $sqlval = array();
        if (!SC_Utils_Ex::isBlank($arrPayment['cancel_order_status']) && $arrPayment['cancel_order_status'] != '0') {
            $objPurchase = new SC_Helper_Purchase_Ex();
            $new_status = $arrPayment['cancel_order_status'];
            $sqlval[MDL_SPS_ORDER_COL_EVERY] = SPS_EVERY_CC;
            $objPurchase->sfUpdateOrderStatus($arrOrder['order_id'], $new_status, null, null, $sqlval);
        }
        return true;
    }

    function doPaymentRecvPY(&$arrParam, &$arrOrder, &$arrPayment) {
        $sqlval = array();
        $sqlval[MDL_SPS_ORDER_COL_TRANSID] = $arrParam['res_tracking_id'];

        list($recv_type, $payment, $total, $email) = split(',', $arrParam['res_payinfo_key']);

        $arrMemo02 = array(
            'title' => array(
                'name'  => '',
                'value' => true
            ),
            'pay' => array(
                'name'  => '入金額',
                'value' => $payment
            ),
            'total' => array(
                'name'  => '入金累計額',
                'value' => $total
            ),
            'email' => array(
                'name'  => 'メールアドレス',
                'value' => $email
            ),
        );
        $sqlval[MDL_SPS_ORDER_COL_PAYVIEW] = serialize($arrMemo02);

        switch($recv_type) {
        case 'P':
            if ($total == $arrOrder['payment_total']) {
                $new_status =  $arrPayment['commit_order_status'];
            } else {
                $new_status =  $arrPayment['auth_order_status'];
            }
            break;
        case 'D':
            if ($total == $arrOrder['payment_total'] && $arrOrder['status'] != $arrPayment['auth_order_status']) {
                $new_status =  $arrPayment['commit_order_status'];
            }
            break;
        case 'C':
            $new_status =  $arrPayment['auth_order_status'];
            break;
        case 'G':
            $new_status =  $arrPayment['auth_order_status'];
            break;
        default:
            return false;
            break;
        }
        if ($new_status === '0') {
            $new_status = null;
        }
        $objPurchase = new SC_Helper_Purchase_Ex();
        $objPurchase->sfUpdateOrderStatus($arrOrder['order_id'], $new_status, null, null, $sqlval);

        return true;
    }

    function doPaymentRecvCN(&$arrParam, &$arrOrder, &$arrPayment) {
        $sqlval = array();
        if (!SC_Utils_Ex::isBlank($arrPayment['cancel_order_status']) && $arrPayment['cancel_order_status'] != '0') {
            $objPurchase = new SC_Helper_Purchase_Ex();
            $new_status = $arrPayment['cancel_order_status'];
            $objPurchase->sfUpdateOrderStatus($arrOrder['order_id'], $new_status, null, null, $sqlval);
        }
        return true;
    }

    function doPaymentRecv(&$arrParam, &$arrOrder, &$arrPayment) {
        switch ($arrParam['res_result']) {
        case 'OK':
            $res =  $this->doPaymentRecvOK($arrParam, $arrOrder, $arrPayment);
            break;
        case 'NG':
            $res =  $this->doPaymentRecvNG($arrParam, $arrOrder, $arrPayment);
            break;
        case 'CR':
            $res =  $this->doPaymentRecvCR($arrParam, $arrOrder, $arrPayment);
            break;
        case 'CC':
            $res =  $this->doPaymentRecvCC($arrParam, $arrOrder, $arrPayment);
            break;
        case 'PY':
            $res =  $this->doPaymentRecvPY($arrParam, $arrOrder, $arrPayment);
            break;
        case 'CN':
            $res =  $this->doPaymentRecvCN($arrParam, $arrOrder, $arrPayment);
            break;
        default:
            $res = false;
            break;
        }
        return $res;
    }

    function doPaymentRequest (&$objPage, &$objFormParam, &$arrOrder, &$arrPaymentInfo) {
        $objMdl =& SC_Mdl_SPS_Ex::getInstance();
        $connect_type = $this->getConnectType();

        switch ($connect_type) {
        case MDL_SPS_CONNECT_TYPE_LINK:
        case MDL_SPS_CONNECT_TYPE_FREECSV:
            $this->setSJISConv();   // SJIS出力
            $arrData = $this->createLinkData($arrOrder, $arrPaymentInfo, $objFormParam);
            $objPage->arrSpsData = $arrData;
            $objPage->tpl_next_form = 'form_sps';
            $objPage->sps_url = $this->getRequestUrl();
            $objPage->tpl_payment_onload = 'fnAutoLoadSubmitSPS();';
            $objMdl->printLog($objPage->tpl_next_form . ' url=' . $objPage->sps_url);
            break;
        case MDL_SPS_CONNECT_TYPE_API:
            $this->doApiPayment($objPage, $objFormParam, $arrOrder, $arrPaymentInfo);
            break;
            break;
        }

        return true;
    }

    function doApiPaymentReAuth(&$arrOrder, $arrPayment, $target_datetime) {
        $objMdl =& SC_Mdl_SPS_Ex::getInstance();
        $arrModuleConfig = $objMdl->getUserSettings();

        $arrOldOrder = $arrOrder; // 返金用

        $arrData = $this->createReAuthXmlData($arrOrder, $arrPayment, $target_datetime);
        $request_xml = SC_Util_SPS_Ex::changeArray2Xml($arrData, $this->getPaymentReAuthApiId());
        list($r_code, $response) = SC_Util_SPS_Ex::sendRequestSps($request_xml, $arrModuleConfig);
        $arrRes = $this->checkResponse($r_code, $response, $this);
        $sqlval = array();

        if ($arrRes !== false) {

//            if ($this->isThreePhase()) {
                SC_Util_SPS_Ex::setOrderPayData($arrOrder, $arrRes);

                $arrData = $this->createCommitXmlData($arrOrder, $arrRes, $arrPayment);
                $request_xml = SC_Util_SPS_Ex::changeArray2Xml($arrData, $this->getPaymentCommitApiId());
                list($r_code, $response) = SC_Util_SPS_Ex::sendRequestSps($request_xml, $arrModuleConfig);
                $arrRes2 = $this->checkResponse($r_code, $response, $objPage);

                if ($arrRes2 !== false) {
                    if ($arrPayment['sps_auth_type'] == SPS_AUTH_AUTO) {
                        $sqlval[MDL_SPS_ORDER_COL_RESDATA] = SPS_COMMIT_OK;
                    } else {
                        $sqlval[MDL_SPS_ORDER_COL_RESDATA] = SPS_AUTH_DATE_COMMIT;
                    }

                    $sqlval[MDL_SPS_ORDER_COL_REQDATE] = $arrData['request_date'];

                    $sqlval[MDL_SPS_ORDER_COL_TRANSID] = $arrRes2['res_tracking_id'];
                    if ($sqlval[MDL_SPS_ORDER_COL_TRANSID] == "") {
                        $sqlval[MDL_SPS_ORDER_COL_TRANSID] = $arrRes['res_tracking_id'];
                    }
                    if ($arrPayment['reauth_order_status']) {
                        $new_status = $arrPayment['reauth_order_status'];
                    }

                    SC_Util_SPS_Ex::setOrderPayData($arrOrder, $arrRes2, false, $new_status, $sqlval);
                } else {
                    $sqlval[MDL_SPS_ORDER_COL_RESDATA] = SPS_ERROR;
                    SC_Util_SPS_Ex::setOrderPayData($arrOrder, $arrRes, true, $new_status, $sqlval);
                    return $this->arrErr['tpl_error'];
                }
/*
            } else {
                $sqlval[MDL_SPS_ORDER_COL_TRANSID] = $arrParam['res_tracking_id'];
                if ($arrPayment['reauth_order_status']) {
                    $new_status = $arrPayment['reauth_order_status'];
                }
                SC_Util_SPS_Ex::setOrderPayData($arrOrder, $arrRes, false, $new_status, $sqlval);
            }
*/            
            // 返金処理を移動

                $res = $this->doApiPaymentCancel($arrOldOrder, $arrPayment, $target_datetime, false);// 更新無し指定
            if ($res !== true) {
                return $this->arrErr['tpl_error'];
            }
            // 最新状態を確認
            $arrOrder = SC_Util_SPS_Ex::getOrderPayData($arrOrder['order_id']);
            $this->doApiPaymentGetStatus($arrOrder, $arrPayment);
        } else {
            $sqlval[MDL_SPS_ORDER_COL_RESDATA] = SPS_ERROR;
            SC_Util_SPS_Ex::setOrderPayData($arrOrder, $arrRes, true, $new_status, $sqlval);
            return $this->arrErr['tpl_error'];
        }
        return true;
    }

    function doApiPaymentSales(&$arrOrder, $arrPayment, $target_datetime) {
        $objMdl =& SC_Mdl_SPS_Ex::getInstance();
        $arrModuleConfig = $objMdl->getUserSettings();

        $arrData = $this->createSalesXmlData($arrOrder, $arrPayment, $target_datetime);
        $request_xml = SC_Util_SPS_Ex::changeArray2Xml($arrData, $this->getPaymentSalesApiId());
        list($r_code, $response) = SC_Util_SPS_Ex::sendRequestSps($request_xml, $arrModuleConfig);
        $arrRes = $this->checkResponse($r_code, $response, $this);

        if ($arrRes !== false) {
            $sqlval = array();
            $sqlval[MDL_SPS_ORDER_COL_RESDATA] = SPS_DATECOMMIT_OK;
            if (!SC_Utils_Ex::isBlank($arrPayment['commit_order_status'])) {
                $new_status = $arrPayment['commit_order_status'];
            } else {
                $new_status = null;
            }
            SC_Util_SPS_Ex::setOrderPayData($arrOrder, $arrRes, false, $new_status, $sqlval);
        } else {
            $sqlval[MDL_SPS_ORDER_COL_RESDATA] = SPS_ERROR;
            SC_Util_SPS_Ex::setOrderPayData($arrOrder, $arrRes, true, $new_status, $sqlval);
            return $this->arrErr['tpl_error'];
        }
        return true;
    }

    function doApiPaymentCancel(&$arrOrder, $arrPayment, $target_datetime, $is_update = true) {
        $objMdl =& SC_Mdl_SPS_Ex::getInstance();
        $arrModuleConfig = $objMdl->getUserSettings();

        // 現在の決済側ステータスを取得
        $arrRes = $this->getApiPaymentGetStatus($arrOrder, $arrPayment);
        switch ($arrRes['payment_status']) {
            case '1': // 与信済
                $target_api_id = $this->getPaymentCancelApiId();
                break;
            case '2': // 売上済
                $target_api_id = $this->getPaymentRefoundApiId();
                break;
            case '3':
            case '4': // 取消済み
            case '0': // for rakuten
                // 状態のズレがあるので、更新する。
                $sqlval[MDL_SPS_ORDER_COL_RESDATA] = SPS_CANCEL_OK;
                if (!SC_Utils_Ex::isBlank($arrPayment['cancel_order_status'])) {
                    $new_status = $arrPayment['cancel_order_status'];
                } else {
                    $new_status = null;
                }
                if ($is_update) {
                    SC_Util_SPS_Ex::setOrderPayData($arrOrder, $arrRes, false, $new_status, $sqlval);
                } else {
                    SC_Util_SPS_Ex::setOrderPayData($arrOrder, $arrRes, true);
                }
                return true;
                break;
            default: // 返答が正常ではない場合はエラー
                return '決済状態が確認出来ませんでした。';
                break;
        }

        $arrData = $this->createCancelXmlData($arrOrder, $arrPayment, $target_datetime);
        $request_xml = SC_Util_SPS_Ex::changeArray2Xml($arrData, $target_api_id);
        list($r_code, $response) = SC_Util_SPS_Ex::sendRequestSps($request_xml, $arrModuleConfig);
        $arrRes = $this->checkResponse($r_code, $response, $this);

        if ($arrRes !== false) {
            $sqlval = array();
            $sqlval[MDL_SPS_ORDER_COL_RESDATA] = SPS_CANCEL_OK;
            if (!SC_Utils_Ex::isBlank($arrPayment['cancel_order_status'])) {
                $new_status = $arrPayment['cancel_order_status'];
            } else {
                $new_status = null;
            }
            if ($is_update) {
                SC_Util_SPS_Ex::setOrderPayData($arrOrder, $arrRes, false, $new_status, $sqlval);
            } else {
                SC_Util_SPS_Ex::setOrderPayData($arrOrder, $arrRes, true);
            }
        } else {
            $sqlval[MDL_SPS_ORDER_COL_RESDATA] = SPS_ERROR;
            SC_Util_SPS_Ex::setOrderPayData($arrOrder, $arrRes, true, $new_status, $sqlval);
            return $this->arrErr['tpl_error'];
        }
        return true;
    }

    function doApiPaymentEveryStop(&$arrOrder, $arrPayment, $target_datetime) {
        $objMdl =& SC_Mdl_SPS_Ex::getInstance();
        $arrModuleConfig = $objMdl->getUserSettings();

        $arrData = $this->createEveryStopXmlData($arrOrder, $arrPayment, $target_datetime);
        $request_xml = SC_Util_SPS_Ex::changeArray2Xml($arrData, $this->getPaymentEveryStopApiId());
        list($r_code, $response) = SC_Util_SPS_Ex::sendRequestSps($request_xml, $arrModuleConfig);
        $arrRes = $this->checkResponse($r_code, $response, $this);

        if ($arrRes !== false) {
            $sqlval = array();
            $sqlval[MDL_SPS_ORDER_COL_RESDATA] = SPS_CANCEL_OK;
            $sqlval[MDL_SPS_ORDER_COL_EVERY] = SPS_EVERY_CANCEL;
            if (!SC_Utils_Ex::isBlank($arrPayment['cancel_order_status'])) {
                $new_status = $arrPayment['cancel_order_status'];
            } else {
                $new_status = null;
            }
            SC_Util_SPS_Ex::setOrderPayData($arrOrder, $arrRes, false, $new_status, $sqlval);
        } else {
            $sqlval[MDL_SPS_ORDER_COL_RESDATA] = SPS_ERROR;
            SC_Util_SPS_Ex::setOrderPayData($arrOrder, $arrRes, true, $new_status, $sqlval);
            return $this->arrErr['tpl_error'];
        }
        return true;
    }

    function getApiPaymentGetStatus(&$arrOrder, $arrPayment, $is_log = true) {
        $objMdl =& SC_Mdl_SPS_Ex::getInstance();
        $arrModuleConfig = $objMdl->getUserSettings();

        $arrData = $this->createGetStatusXmlData($arrOrder, $arrPayment);
        $request_xml = SC_Util_SPS_Ex::changeArray2Xml($arrData, $this->getPaymentGetStatusApiId());
        list($r_code, $response) = SC_Util_SPS_Ex::sendRequestSps($request_xml, $arrModuleConfig);
        $arrRes = $this->checkResponse($r_code, $response, $this);
        if ($arrRes !== false && $is_log) {
            SC_Util_SPS_Ex::setOrderPayData($arrOrder, $arrRes, true);
        }
        return $arrRes;
    }

    function doApiPaymentGetStatus(&$arrOrder, $arrPayment, $target_datetime = null) {
        $objMdl =& SC_Mdl_SPS_Ex::getInstance();
        $arrModuleConfig = $objMdl->getUserSettings();

        $arrRes = $this->getApiPaymentGetStatus($arrOrder, $arrPayment, false);

        if ($arrRes !== false) {
            $sqlval = array();
            switch ($arrRes['payment_status']) {
            case '1':
                if ($arrPayment['sps_auth_type'] == SPS_AUTH_AUTO) {
                    $sqlval[MDL_SPS_ORDER_COL_RESDATA] = SPS_COMMIT_OK;
                } else {
                    $sqlval[MDL_SPS_ORDER_COL_RESDATA] = SPS_AUTH_DATE_COMMIT;
                }
                break;
            case '2':
                if ($arrPayment['sps_auth_type'] == SPS_AUTH_AUTO) {
                    $sqlval[MDL_SPS_ORDER_COL_RESDATA] = SPS_COMMIT_OK;
                } else {
                    $sqlval[MDL_SPS_ORDER_COL_RESDATA] = SPS_DATECOMMIT_OK;
                }
                break;
            case '0':   //for rakuten
            case '3':
            case '4':
                $sqlval[MDL_SPS_ORDER_COL_RESDATA] = SPS_CANCEL_OK;
                break;
            }
            SC_Util_SPS_Ex::setOrderPayData($arrOrder, $arrRes, false, $new_status, $sqlval);
        } else {
            $sqlval[MDL_SPS_ORDER_COL_RESDATA] = SPS_ERROR;
            SC_Util_SPS_Ex::setOrderPayData($arrOrder, $arrRes, true, $new_status, $sqlval);
            return $this->arrErr['tpl_error'];
        }
        return true;
    }

    function doApiPayment(&$objPage, &$objFormParam, &$arrOrder, &$arrPaymentInfo, $no_fix = false) {
        $objMdl =& SC_Mdl_SPS_Ex::getInstance();
        $arrModuleConfig = $objMdl->getUserSettings();

        $objQuery =& SC_Query_Ex::getSingletonInstance();
        $objQuery->begin();

        $arrData = $this->createXmlData($arrOrder, $arrPaymentInfo, $objFormParam);

        $request_xml = SC_Util_SPS_Ex::changeArray2Xml($arrData, $this->getPaymentRequestApiId());

        list($r_code, $response) = SC_Util_SPS_Ex::sendRequestSps($request_xml, $arrModuleConfig);

        $arrRes = $this->checkResponse($r_code, $response, $objPage);

        if ($arrRes !== false) {
            SC_Util_SPS_Ex::setOrderPayData($arrOrder, $arrRes);

            if ($this->isThreePhase()) {
                $arrData = $this->createCommitXmlData($arrOrder, $arrRes, $arrPaymentInfo);
                $request_xml = SC_Util_SPS_Ex::changeArray2Xml($arrData, $this->getPaymentCommitApiId());
                list($r_code, $response) = SC_Util_SPS_Ex::sendRequestSps($request_xml, $arrModuleConfig);
                $arrRes2 = $this->checkResponse($r_code, $response, $objPage);

                if ($arrRes2 !== false) {
                    if ($no_fix) {
                        $res = $this->doPaymentRecvOK($arrRes, $arrOrder, $arrPaymentInfo, $objFormParam->getHashArray());
                    } else {
                        $this->doApiPaymentFix($arrRes, $objPage, $objFormParam, $arrOrder, $arrPaymentInfo);
                    }
                } else {
                    SC_Util_SPS_Ex::setOrderPayData($arrOrder, $arrRes, true);
                }
            } else {
                if ($no_fix) {
                    $res = $this->doPaymentRecvOK($arrRes, $arrOrder, $arrPaymentInfo, $objFormParam->getHashArray());
                } else {
                    $this->doApiPaymentFix($arrRes, $objPage, $objFormParam, $arrOrder, $arrPaymentInfo);
                }
            }
        } else {
            SC_Util_SPS_Ex::setOrderPayData($arrOrder, $arrRes, true);
        }

        $objQuery->commit();
    }

    function doApiPaymentFix($arrRes, &$objPage, &$objFormParam, &$arrOrder, &$arrPayment) {
        $res = $this->doPaymentRecvOK($arrRes, $arrOrder, $arrPayment);

        if ($res) {
            $this->actionCompletePage($objFormParam, $arrOrder, $arrPayment);
        }
    }

    function checkResponse($r_code, $response, &$objPage) {
        if ($r_code != 200) {
            $objPage->arrErr['tpl_error'] = "SBPSとの通信に失敗しました。(RCODE:$r_code)";
            return false;
        } else {
            // XMLを配列化
            $arrRes = SC_Util_SPS_Ex::changeXml2Array($response);
            // 成功
            if($arrRes['res_result'] == "OK") {
                if (!SC_Utils_Ex::isBlank($arrRes['res_pay_method_info'])) {
                    $objMdl =& SC_Mdl_SPS_Ex::getInstance();
                    $arrModuleConfig = $objMdl->getUserSettings();
                    $arrDec = SC_Util_SPS_Ex::decodeData($arrRes['res_pay_method_info'], $arrModuleConfig);
                    // 階層化は必要無いので並列化
                    foreach ($arrDec as $key => $val) {
                        $arrRes[$key] = $val;
                    }
                    unset ($arrRes['res_pay_method_info']);
                }
                foreach ($arrRes as $key => $val) {
                    if (is_array($val)) {
                        foreach ($val as $skey => $sval) {
                            if (!SC_Utils_Ex::isBlank($sval)) {
                                $arrRes[$key][$skey] = SC_Utils_Ex::sfTrim($sval);
                            }
                        }
                        continue;
                    }
                    if (!SC_Utils_Ex::isBlank($val)) {
                        $arrRes[$key] = SC_Utils_Ex::sfTrim($val);
                    }
                }
                return $arrRes;
            // 失敗
            } else {
                $objPage->arrErr['tpl_error'] = "決済に失敗しました。";
                $objPage->arrErr['tpl_error'] .= (strlen($arrRes['res_err_code']) > 0) ? "（エラーコード：${arrRes['res_err_code']}）" : "";
                return false;
            }
        }
    }

    function createGetStatusXmlData(&$arrOrder, &$arrPayment, $target_datetime = null) {
        $arrData = array();
        $objMdl =& SC_Mdl_SPS_Ex::getInstance();
        $arrModuleConfig = $objMdl->getUserSettings();
        $now_time = date("YmdHis");

        $arrData['merchant_id'] = $arrModuleConfig['id_sps_marchant'];
        $arrData['service_id'] = $arrModuleConfig['id_sps_service'];
        $arrData['tracking_id'] = $arrOrder[MDL_SPS_ORDER_COL_TRANSID];
        if (SC_Utils_Ex::isBlank($arrData['tracking_id'])) {
            $arrData['tracking_id'] = $arrOrder['res_tracking_id'];
        }
        $arrData['response_info_type'] = '2';
        $arrData['pay_option_manage']['cardbrand_return_flg'] = '1';
        $arrData['encrypted_flg'] = '1';
        $arrData['request_date'] = $now_time;
        $arrData['limit_second'] = '600';

        $arrData = $this->normalizeData($arrData, false);
        $arrData['sps_hashcode'] = $this->createSpsHash($arrData);
        return $arrData;

    }

    function createSalesXmlData(&$arrOrder, &$arrPayment, $target_datetime) {
        $arrData = array();
        $objMdl =& SC_Mdl_SPS_Ex::getInstance();
        $arrModuleConfig = $objMdl->getUserSettings();
        $now_time = date("YmdHis");

        $arrData['merchant_id'] = $arrModuleConfig['id_sps_marchant'];
        $arrData['service_id'] = $arrModuleConfig['id_sps_service'];
        $arrData['tracking_id'] = $arrOrder[MDL_SPS_ORDER_COL_TRANSID];
        if (SC_Utils_Ex::isBlank($arrData['tracking_id'])) {
            $arrData['tracking_id'] = $arrOrder['res_tracking_id'];
        }
        $arrData['processing_datetime'] = $target_datetime;
        $arrData['pay_option_manage']['amount'] = $arrOrder['payment_total'];
        $arrData['request_date'] = $now_time;
        $arrData['limit_second'] = '600';

        $arrData = $this->normalizeData($arrData, false);
        $arrData['sps_hashcode'] = $this->createSpsHash($arrData);
        return $arrData;
    }

    function createCancelXmlData(&$arrOrder, &$arrPayment, $target_datetime) {
        $arrData = array();
        $objMdl =& SC_Mdl_SPS_Ex::getInstance();
        $arrModuleConfig = $objMdl->getUserSettings();
        $now_time = date("YmdHis");

        $arrData['merchant_id'] = $arrModuleConfig['id_sps_marchant'];
        $arrData['service_id'] = $arrModuleConfig['id_sps_service'];
        $arrData['tracking_id'] = $arrOrder[MDL_SPS_ORDER_COL_TRANSID];
        if (SC_Utils_Ex::isBlank($arrData['tracking_id'])) {
            $arrData['tracking_id'] = $arrOrder['res_tracking_id'];
        }
        $arrData['processing_datetime'] = $target_datetime;
        $arrData['request_date'] = $now_time;
        $arrData['limit_second'] = '600';

        $arrData = $this->normalizeData($arrData, false);
        $arrData['sps_hashcode'] = $this->createSpsHash($arrData);
        return $arrData;

    }

    function createEveryStopXmlData(&$arrOrder, &$arrPayment, $target_datetime) {
        $arrData = array();
        $objMdl =& SC_Mdl_SPS_Ex::getInstance();
        $arrModuleConfig = $objMdl->getUserSettings();
        $now_time = date("YmdHis");

        $arrData['merchant_id'] = $arrModuleConfig['id_sps_marchant'];
        $arrData['service_id'] = $arrModuleConfig['id_sps_service'];
        $arrData['tracking_id'] = $arrOrder[MDL_SPS_ORDER_COL_TRANSID];
        $arrData['processing_datetime'] = $target_datetime;
        $arrData['request_date'] = $now_time;
        $arrData['limit_second'] = '600';

        $arrData = $this->normalizeData($arrData, false);
        $arrData['sps_hashcode'] = $this->createSpsHash($arrData);
        return $arrData;

    }

    function createCommitXmlData(&$arrOrder, &$arrRes, &$arrPayment) {
        $arrData = array();
        $objMdl =& SC_Mdl_SPS_Ex::getInstance();
        $arrModuleConfig = $objMdl->getUserSettings();
        $now_time = date("YmdHis");

        $arrData['merchant_id'] = $arrModuleConfig['id_sps_marchant'];
        $arrData['service_id'] = $arrModuleConfig['id_sps_service'];
        $arrData['tracking_id'] = $arrRes['res_tracking_id'];
        $arrData['processing_datetime'] = $arrRes['res_process_date'];
        $arrData['request_date'] = $now_time;
        $arrData['limit_second'] = '600';

        $arrData = $this->normalizeData($arrData, false);
        $arrData['sps_hashcode'] = $this->createSpsHash($arrData);
        return $arrData;

    }

    function createReAuthXmlData(&$arrOrder, &$arrPayment, $target_datetime) {
        $arrData = array();
        $objMdl =& SC_Mdl_SPS_Ex::getInstance();
        $arrModuleConfig = $objMdl->getUserSettings();
        $now_time = date("YmdHis");

        $objDb = new SC_Helper_DB_Ex();
        $arrInfo = $objDb->sfGetBasisData();

        $arrData['merchant_id'] = $arrModuleConfig['id_sps_marchant'];
        $arrData['service_id'] = $arrModuleConfig['id_sps_service'];
        $arrData['tracking_id'] = $arrOrder[MDL_SPS_ORDER_COL_TRANSID];
        if (SC_Utils_Ex::isBlank($arrData['tracking_id'])) {
            $arrData['tracking_id'] = $arrOrder['res_tracking_id'];
        }
        $arrData['cust_code'] =SC_Util_SPS::getCustomerCode($arrOrder['customer_id']);
        $arrData['order_id'] = $arrOrder['order_id']. '_' . $now_time; // マーチャント購入ID + リクエスト日時(YYYYMMDDHHMMSS)
        $arrData['item_id'] = 'ec-cube';
        $arrData['item_name'] = '';
        $arrData['tax'] = '';
        $arrData['amount'] = $arrOrder['payment_total'];
        $arrData['free1'] = $arrOrder['order_id'];
        $arrData['free2'] = MDL_SPS_VERSION . '_' .ECCUBE_VERSION;
        $arrData['free3'] = SC_Util_SPS_Ex::subString($arrPayment['free3'], 20);
        $arrData['sps_cust_info_return_flg'] = '1';

        $arrData['pay_option_manage']['cust_manage_flg'] ='0';
        $arrData['pay_option_manage']['pay_info_control_type'] = 'B';
        $arrData['pay_option_manage']['pay_info_detail_control_type'] = 'B';

        $arrData['encrypted_flg'] = '1';
        $arrData['request_date'] = $now_time;
        $arrData['limit_second'] = '600';

        $arrData = $this->normalizeData($arrData, false);
        $arrData['sps_hashcode'] = $this->createSpsHash($arrData);

        $arrData = SC_Util_SPS_Ex::encodeMbstring($arrData, array('free1','free2','free3'));
        return $arrData;
    }

    function getApiPayMethodInfoData(&$arrInput, &$arrPayment, &$arrOrder) {
        $arrData = array();
        $arrData['cc_number'] = $arrInput['card_no01']. $arrInput['card_no02']. $arrInput['card_no03']. $arrInput['card_no04'];
        $arrData['cc_expiration'] = $arrInput['card_year']. $arrInput['card_month'];
        $arrData['security_code'] = $arrInput['security_code'];
        $arrData['dealings_type'] =  $arrInput['dealings_type'];
        $arrData['divide_times'] = ($arrInput['divide_times'] > 0) ? sprintf("%03d", $arrInput['divide_times']) : "";
        return $arrData;
    }

    function createXmlData(&$arrOrder, &$arrPayment, &$objFormParam) {
        $arrData = array();
        $objMdl =& SC_Mdl_SPS_Ex::getInstance();
        $arrModuleConfig = $objMdl->getUserSettings();
        $now_time = date("YmdHis");

        $arrInput = $objFormParam->getHashArray();

        $objDb = new SC_Helper_DB_Ex();
        $arrInfo = $objDb->sfGetBasisData();

        $arrData['merchant_id'] = $arrModuleConfig['id_sps_marchant'];
        $arrData['service_id'] = $arrModuleConfig['id_sps_service'];
        $arrData['cust_code'] =SC_Util_SPS_Ex::getCustomerCode($arrOrder['customer_id']);
        $arrData['order_id'] = $arrOrder['order_id']. '_' . $now_time; // マーチャント購入ID + リクエスト日時(YYYYMMDDHHMMSS)
        $arrData['item_id'] = 'ec-cube';
        $arrData['item_name'] = '';
        if (class_exists('SC_Helper_TaxRule_Ex')) {
            $arrTax = SC_Helper_TaxRule_Ex::getTaxRule();
            $tax_rate = $arrTax['tax_rate'];
        } else {
            $tax_rate = $arrInfo['tax'];
        }
        if ($tax_rate < 1) {
            $tax_rate = 8;
        }
        $arrData['tax'] = floor($arrOrder['payment_total'] * $tax_rate / (100 + $tax_rate));
        $arrData['amount'] = $arrOrder['payment_total'];
        $arrData['free1'] = $arrOrder['order_id'];
        $arrData['free2'] = MDL_SPS_VERSION . '_' .ECCUBE_VERSION;
        $arrData['free3'] = SC_Util_SPS_Ex::subString($arrPaymentInfo['free3'], 20);
        $arrData['sps_cust_info_return_flg'] = '1';

        $arrData['pay_method_info'] = $this->getApiPayMethodInfoData($arrInput, $arrPayment, $arrOrder);
        if ($arrInput['cust_manage_flg']) {
            $arrData['pay_option_manage']['cust_manage_flg'] = $arrInput['cust_manage_flg'] ? $arrInput['cust_manage_flg'] : '0';
        }
//        $arrData['pay_option_manage']['cardbrand_return_flg'] ='1';

        $arrData['encrypted_flg'] = '1';
        $arrData['request_date'] = $now_time;
        $arrData['limit_second'] = '600';

        $arrData = $this->normalizeData($arrData, false, true);
        $arrData['sps_hashcode'] = $this->createSpsHash($arrData);
        $arrData = SC_Util_SPS_Ex::encryptData($arrData, 'pay_method_info', $arrModuleConfig, false);
        $arrData = SC_Util_SPS_Ex::encodeMbstring($arrData, array('free1','free2','free3'));
        return $arrData;
    }

    function createLinkData(&$arrOrder, &$arrPaymentInfo, &$objFormParam) {
        $arrData = array();
        $objMdl =& SC_Mdl_SPS_Ex::getInstance();
        $arrModuleConfig = $objMdl->getUserSettings();
        $now_time = date('YmdHis');
        $objPurchase = new SC_Helper_Purchase_Ex();

        $objDb = new SC_Helper_DB_Ex();
        $arrInfo = $objDb->sfGetBasisData();

        $arrDetails = $objPurchase->getOrderDetail($arrOrder['order_id'], false);

        $arrData['pay_method'] = $this->getPayMethod();
        $arrData['merchant_id'] = $arrModuleConfig['id_sps_marchant'];
        $arrData['service_id'] = $arrModuleConfig['id_sps_service'];
        $arrData['cust_code'] =SC_Util_SPS::getCustomerCode($arrOrder['customer_id']);
        $arrData['sps_cust_no'] = '';
        $arrData['spspayment_no'] = '';
        $arrData['order_id'] = $arrOrder['order_id']. '_' . $now_time; // マーチャント購入ID + リクエスト日時(YYYYMMDDHHMMSS)
        $arrData['item_id'] = $arrDetails[0]['product_id'];
        $arrData['pay_item_id'] = '';
        $arrData['item_name'] = SC_Util_SPS_Ex::subString($arrDetails[0]['product_name'], 40);
        $arrData['tax'] = '';
        $arrData['amount'] = $arrOrder['payment_total'];
        $arrData['pay_type'] = $this->getSpsPayType();
        $arrData['auto_charge_type'] = SC_Utils_Ex::isBlank($arrPaymentInfo['auto_charge_type']) ? '' : $arrPaymentInfo['auto_charge_type'];
        $arrData['service_type'] = SC_Utils_Ex::isBlank($arrPaymentInfo['service_type']) ? '0' : $arrPaymentInfo['service_type'];
        $arrData['div_settele'] = SC_Utils_Ex::isBlank($arrPaymentInfo['div_settele']) ? '' : $arrPaymentInfo['div_settele'];
        $arrData['last_charge_month'] = '';
        $arrData['camp_type'] = SC_Utils_Ex::isBlank($arrPaymentInfo['camp_type']) ? '' : $arrPaymentInfo['camp_type'];
        $arrData['tracking_id'] = '';
        $arrData['terminal_type'] = SC_Display_Ex::detectDevice() === DEVICE_TYPE_MOBILE ? '1' : '0';
        $arrData['success_url'] = $this->getReturnUrl(MDL_SPS_COMPLETE_URL, $arrOrder);
        $arrData['cancel_url'] = $this->getReturnUrl(MDL_SPS_RETURN_URL, $arrOrder);
        $arrData['error_url'] = $this->getReturnUrl(MDL_SPS_ERROR_URL, $arrOrder);
        $arrData['pagecon_url'] = MDL_SPS_SETTLEMENT_URL . '?ec_order_id=' . $arrOrder['order_id'] . '&tid=' . SC_Helper_Session_Ex::getToken();
        $arrData['free1'] = $arrOrder['order_id'];
        $arrData['free2'] = MDL_SPS_VERSION . '_' .ECCUBE_VERSION;
        $arrData['free3'] = SC_Util_SPS_Ex::subString($arrPaymentInfo['free3'], 20);
        $arrData['free_csv'] = $this->getFreeCsv($arrOrder, $arrPaymentInfo, $objFormParam);

        $row_no = 1;
        foreach ($arrDetails as $arrDetail) {
            $tax_in = SC_Utils_Ex::sfCalcIncTax($arrDetail['price'], $arrDetail['tax_rate'], $arrDetail['tax_rule'] );

            $arrDtl = array();
            $arrDtl['dtl_rowno'] = $row_no;
            $arrDtl['dtl_item_id'] = $arrDetail['product_id'];
            $arrDtl['dtl_item_name'] = SC_Util_SPS_Ex::subString($arrDetail['product_name'], 40);
            $arrDtl['dtl_item_count'] = $arrDetail['quantity'];
//            $arrDtl['dtl_tax'] = '';
            $arrDtl['dtl_amount'] = $tax_in * $arrDetail['quantity'];
            $arrData['dtl'][] = $arrDtl;

            $row_no++;
        }

        // 送料
        if ($arrOrder['deliv_fee'] > 0) {
            $arrDtl = array();
            $arrDtl['dtl_rowno'] = $row_no;
            $arrDtl['dtl_item_id'] = 'deliv_fee';
            $arrDtl['dtl_item_name'] = '送料';
            $arrDtl['dtl_item_count'] = '1';
//            $arrDtl['dtl_tax'] = '';
            $arrDtl['dtl_amount'] = $arrOrder['deliv_fee'];
            $arrData['dtl'][] = $arrDtl;
            $row_no++;
        }

        // 手数料
        if ($arrOrder['charge'] > 0) {
            $arrDtl = array();
            $arrDtl['dtl_rowno'] = $row_no;
            $arrDtl['dtl_item_id'] = 'charge';
            $arrDtl['dtl_item_name'] = '手数料';
            $arrDtl['dtl_item_count'] = '1';
//            $arrDtl['dtl_tax'] = '';
            $arrDtl['dtl_amount'] = $arrOrder['charge'];
            $arrData['dtl'][] = $arrDtl;
            $row_no++;
        }

        $arrData['request_date'] = $now_time;
        $arrData['limit_second'] = '600';

        $isMobile = SC_Display_Ex::detectDevice() === DEVICE_TYPE_MOBILE;
        $arrData = $this->normalizeData($arrData, $isMobile);

        $arrData['sps_hashcode'] = $this->createSpsHash($arrData);
        return $arrData;
    }

    function createSpsHash($arrData) {
        $objMdl =& SC_Mdl_SPS_Ex::getInstance();
        $key_for_checksum = $objMdl->getUserSettings('id_sps_hash');

        $join_param = '';
        foreach ($arrData as $val) {
            if (is_array($val) === false) {
                $join_param .= $val;
            } else {
                foreach ($val as $val2) {
                    if (is_array($val2) === false) {
                        $join_param .= $val2;
                    } else {
                        foreach ($val2 as $val3) {
                            $join_param .= $val3;
                        }
                    }
                }
            }
        }
        $hashcode = sha1($join_param . trim($key_for_checksum));
        return $hashcode;
    }

   function normalizeData($arrData, $isMobile = false, $isConv = false) {

        foreach ($arrData as $key => $data) {
            if (is_array($data)) {
                $data = $this->normalizeData($data, $isMobile, $isConv);
            } else {
                if ($isMobile) {
                    $arrData[$key] = mb_convert_kana($data, 'k');
                }
                if (substr($data, 0, 4) == 'http') {
                    $data = trim($data);
                } else {
                    $data = SC_Util_SPS_Ex::convNgChar($data);
                    $data = SC_Util_SPS_Ex::convertProhibitedChar($data);
                    $data = SC_Util_SPS_Ex::convertProhibitedKigo($data);
                    $data = trim($data);
                }
            }
            if ($isConv && !is_array($data)) {
                $arrData[$key] = @mb_convert_encoding($data, 'SJIS-win', CHAR_CODE);
            } else {
                $arrData[$key] = $data;
            }
        }
        return $arrData;
    }

    function getRequestUrl() {
        $objMdl =& SC_Mdl_SPS_Ex::getInstance();
        $url = $objMdl->getUserSettings( 'url_sps_buyservice_api' );
        $objMdl->printLog("goto: $url");
        return $url;
    }

    function getReturnUrl($url, &$arrOrder) {
        $url .= '&order_id=' . $arrOrder['order_id'];
        if (SC_Display_Ex::detectDevice() === DEVICE_TYPE_MOBILE) {
            $url .= '&' . session_name() . '=' . session_id();
        }
        $url .= '&' . TRANSACTION_ID_NAME . '=' . SC_Helper_Session_Ex::getToken();
        SC_Utils_Ex::sfRmDupSlash($url);
        return $url;
    }

    function setSJISConv() {
        $objMdl =& SC_Mdl_SPS_Ex::getInstance();
        $objMdl->printLog("setSJISConverter");
        if (SC_Display_Ex::detectDevice() !== DEVICE_TYPE_MOBILE) {
            ob_start(array($this, 'lfConvSJIShandler'));
            mb_http_output('SJIS-win');
            ob_start('mb_output_handler');
        }
    }

    function lfConvSJIShandler($buf) {
        return preg_replace('/' . CHAR_CODE . '/', 'Shift_JIS', $buf);
    }

    function getFreeCsv(&$arrOrder, &$arrPaymentInfo, &$objFormParam) {
        return '';
    }

    function makeFreeCsvField($arrData) {
        $csv = "";

        $delimiter = ',';
        $connecter = '=';

        foreach ($arrData as $key => $val) {
            $ptn = '';
            switch ($key) {
                case 'FIRST_ZIP':
                case 'SECOND_ZIP':
                case 'TEL':
                case 'MAIL':
                    $ptn = 'KVa';
                    break;
                default:
                    $ptn = 'ASKV';
                    break;
            }
            $val = @mb_convert_kana($val, $ptn);
            $arrProhiditedKigo = array(',', '\'','"','%','|','&','<','>','=');
            foreach ($arrProhiditedKigo as $prohidited_kigo) {
                if(strstr($val, $prohidited_kigo)) {
                    $val = str_replace($prohidited_kigo, " ", $val);
                }
            }
            $val = SC_Util_SPS_Ex::convNgChar($val);
            $val = @mb_convert_encoding($val, 'sjis-win', CHAR_CODE);
            $csv .= $key . $connecter . $val . $delimiter;
        }
        $csv = substr($csv, 0, -1);
        if (MDL_SPS_DEBUG) {
            $objMdl =& SC_Mdl_SPS_Ex::getInstance();
            $objMdl->printLog($csv);
        }
        $csv = base64_encode($csv);
        return $csv;
    }

}
