<?php
/**
 *
 * @copyright 2013 Spirit of Co.,Ltd. All Rights Reserved.
 * @link http://sbps.ec-cube.pro/
 *
 */
require_once(MDL_SPS_HELPEREX_PATH . 'SC_Mdl_SPS_Helper_Base_Ex.php');

/**
 * 決済個別用ヘルパー関数 BankTransApiクラス
 */
class SC_Mdl_SPS_Helper_BankTransApi extends SC_Mdl_SPS_Helper_Base_Ex {

    function init() {
        $this->payment_type_id = SPS_CREDIT_API_PAYID;
        $this->payment_name = 'BankTransApi';
    }

    function getPayMethod() {
        return 'banktransapi';
    }

    function getPaymentBlocName() {
        return 'sps_banktransapi';
    }

    function getConnectType() {
        return MDL_SPS_CONNECT_TYPE_API;
    }

    function getSpsPayType() {
        // 購入タイプ(固定　0:都度購入、1:継続購入)
        return '0';
    }

    function getPaymentRequestApiId() {
        return 'ST01-00101-702';
    }

    function getPaymentCommitApiId() {
        return '';
    }

    function isThreePhase() {
        return false;
    }

    function getPaymentRuleDefault() {
        $arrData = parent::getPaymentRuleDefault();
        $arrData['rule_max'] = '1';
        $arrData['upper_rule'] = '9999999';
        $arrData['upper_rule_max'] = $arrData['upper_rule'];
        return $arrData;
    }

    function getIssueType() {
        $arrData = array();
        $arrData['0'] = '発行済み';
        $arrData['1'] = '要発行';
        $arrData['2'] = '発行不要';
        return $arrData;
    }

    function getAccountDiv() {
        $arrData = array();
        $arrData['D'] = '代表口座';
        $arrData['V'] = 'バーチャル口座';
        return $arrData;
    }

    function initParamPaymentInput(&$objPage, &$objFormParam, &$arrConfig) {
        parent::initParamPaymentInput($objPage, $objFormParam, $arrConfig);

        $objPage->arrIssueType = $this->getIssueType();
        $objPage->arrAccountDiv = $this->getAccountDiv();

        $key = 'auth_order_status';
        $objFormParam->addParam('決済後の受注状況', $key, INT_LEN, 'n',
                array('EXIST_CHECK','MAX_LENGTH_CHECK', 'NUM_CHECK'),
                SC_Utils_Ex::isBlank($arrConfig[$key]) ? ORDER_PAY_WAIT : $arrConfig[$key]);

        $key = 'commit_order_status';
        $objFormParam->addParam('入金後の受注状況', $key, INT_LEN, 'n',
                array('MAX_LENGTH_CHECK', 'NUM_CHECK'),
                SC_Utils_Ex::isBlank($arrConfig[$key]) ? ORDER_PRE_END : $arrConfig[$key]);

        $key = 'issue_type';
        $objFormParam->addParam('請求書発行区分', $key, INT_LEN, 'n',
                array('MAX_LENGTH_CHECK', 'NUM_CHECK', 'EXIST_CHECK'),
                SC_Utils_Ex::isBlank($arrConfig[$key]) ? '' : $arrConfig[$key]);

        $key = 'account_div';
        $objFormParam->addParam('口座区分', $key, 1, 'a',
                array('MAX_LENGTH_CHECK', 'EXIST_CHECK'),
                SC_Utils_Ex::isBlank($arrConfig[$key]) ? '' : $arrConfig[$key]);

    }

    function checkErrorPaymentInput(&$objFormParam) {
        return parent::checkErrorPaymentInput($objFormParam);
    }

    function initParam(&$objFormParam, &$arrPayment, &$arrOrder) {
        parent::initParam($objFormParam, $arrPayment, $arrOrder);
    }

    /**
     * 入力内容のチェックを行なう.
     *
     * @param SC_FormParam $objFormParam SC_FormParam インスタンス
     * @return array 入力チェック結果の配列
     */
    function checkError(&$objFormParam) {
        $arrParam = $objFormParam->getHashArray();
        $objErr = new SC_CheckError_Ex($arrParam);
        $objErr->arrErr = $objFormParam->checkError();

        return $objErr->arrErr;
    }

    function makePaymentMemo(&$arrRes, &$arrPayment) {
        if (SC_Utils_Ex::isBlank($arrPayment['mail_title']) || SC_Utils_Ex::isBlank($arrPayment['mail_body'])) {
            return '';
        }
        $arrMemo02 = array();

        $arrMemo02['title'] = array(
                'name'  => '',
                'value' => true
            );

        switch($arrRes['account_type']) {
        case '1':
            $account = '普通預金';
            break;
        case '2':
            $account = '当座預金';
            break;
        case '4':
            $account = '貯蓄預金';
            break;
        }

        $arrMemo02['account_type'] = array(
                'name' => '振込先 口座種別',
                'value' => $account
            );

        $arrMemo02['bank_code'] = array(
                'name' => '振込先 銀行コード',
                'value' => $arrRes['bank_code']
            );
        $arrMemo02['bank_name'] = array(
                'name' => '振込先 銀行名',
                'value' => $arrRes['bank_name']
            );
        $arrMemo02['bank_kana'] = array(
                'name' => '振込先 銀行名カナ',
                'value' => $arrRes['bank_kana']
            );
        $arrMemo02['branch_code'] = array(
                'name' => '振込先 支店コード',
                'value' => $arrRes['branch_code']
            );
        $arrMemo02['branch_name'] = array(
                'name' => '振込先 支店名',
                'value' => $arrRes['branch_name']
            );
        $arrMemo02['branch_kana'] = array(
                'name' => '振込先 支店名カナ',
                'value' => $arrRes['branch_kana']
            );
        $arrMemo02['account_no'] = array(
                'name' => '振込先 口座番号',
                'value' => $arrRes['account_no']
            );
        $arrMemo02['account_reg_name'] = array(
                'name' => '振込先 口座名',
                'value' => $arrRes['account_reg_name']
            );
        $arrMemo02['account_reg_kana'] = array(
                'name' => '振込先 口座名カナ',
                'value' => $arrRes['account_reg_kana']
            );

        return serialize($arrMemo02);
    }

    function doPaymentRecvOK(&$arrParam, &$arrOrder, &$arrPayment, $arrInput) {
        $objPurchase = new SC_Helper_Purchase_Ex();

        $sqlval = array();
        $sqlval[MDL_SPS_ORDER_COL_TRANSID] = $arrParam['res_tracking_id'];

        $new_status = $arrPayment['auth_order_status'];

        $sqlval[MDL_SPS_ORDER_COL_PAYVIEW] = $this->makePaymentMemo($arrParam, $arrPayment);

        $objPurchase->sfUpdateOrderStatus($arrOrder['order_id'], $new_status, null, null, $sqlval);
        if (SC_Helper_Mobile_Ex::gfIsMobileMailAddress($arrOrder['order_email'])) {
            $template_id = '2';
        } else {
            $template_id = '1';
        }

        $objMailHelper = new SC_Helper_Mail_Ex();
        $objMailHelper->sfSendOrderMail($arrOrder['order_id'], $template_id);
        return true;
    }

    function doApiPaymentFix($arrRes, &$objPage, &$objFormParam, &$arrOrder, &$arrPayment) {
        $res = $this->doPaymentRecvOK($arrRes, $arrOrder, $arrPayment, $objFormParam->getHashArray());

        if ($res) {
            $this->actionCompletePage($objFormParam, $arrOrder, $arrPayment);
        }
    }

    function getApiPayMethodInfoData(&$arrInput, &$arrPayment, &$arrOrder) {
        $masterData = new SC_DB_MasterData_Ex();
        $arrPref = $masterData->getMasterData('mtb_pref');

        $arrData = array();
        $arrData['issue_type'] = $arrPayment['issue_type'];
        $arrData['last_name'] = SC_Util_SPS_Ex::subString(mb_convert_kana($arrOrder['order_name01'], 'ASKV'),20);
        $arrData['first_name'] =SC_Util_SPS_Ex::subString(mb_convert_kana($arrOrder['order_name02'], 'ASKV'),20);
        $arrData['last_name_kana'] = SC_Util_SPS_Ex::subString(mb_convert_kana($arrOrder['order_kana01'],'ASKV'), 20);
        $arrData['first_name_kana'] = SC_Util_SPS_Ex::subString(mb_convert_kana($arrOrder['order_kana02'], 'ASKV'),20);
        $arrData['first_zip'] = $arrOrder['order_zip01'];
        $arrData['second_zip'] = $arrOrder['order_zip02'];
        $arrData['add1'] = $arrPref[$arrOrder['order_pref']];
        $arrData['add2'] = SC_Util_SPS_Ex::subString(mb_convert_kana($arrOrder['order_addr01'], 'ASKV'),50);
        $arrData['add3'] = SC_Util_SPS_Ex::subString(mb_convert_kana($arrOrder['order_addr02'], 'ASKV'),50);
        $arrData['tel'] = $arrOrder['order_tel01'] . $arrOrder['order_tel02'] . $arrOrder['order_tel03'];
        $arrData['mail'] = $arrOrder['order_email'];
        $arrData['seiyakudate'] = date('Ymd', strtotime($arrOrder['create_date']));

        $arrData['account_div'] = $arrPayment['account_div'];

        return $arrData;
    }


    function modeAction($mode, &$objFormParam, &$arrOrder, $objPage) {

        switch($mode) {
        case 'return':
            break;
        case 'next':
            break;
        case 'spsreturn':
            break;
        default:
            $objPage->tpl_payment_onload = 'fnAutoLoadSubmit();';
            break;
        }
        parent::modeAction($mode, $objFormParam, $arrOrder, $objPage);
    }
}
