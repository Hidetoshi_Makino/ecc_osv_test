<?php
/**
 *
 * @copyright 2013 Spirit of Co.,Ltd. All Rights Reserved.
 * @link http://sbps.ec-cube.pro/
 *
 */
require_once(MDL_SPS_HELPEREX_PATH . 'SC_Mdl_SPS_Helper_Base_Ex.php');

/**
 * 決済個別用ヘルパー関数 クレジット登録クラス
 */
class SC_Mdl_SPS_Helper_RegistCredit extends SC_Mdl_SPS_Helper_Base_Ex {

    function init() {
        $this->payment_type_id = SPS_CREDIT_LINK_PAYID;
        $this->payment_name = 'RegistCredit';
    }

    function getPayMethod() {
        return 'credit';
    }

    function getConnectType() {
        return MDL_SPS_CONNECT_TYPE_LINK;
    }

    function getSpsPayType() {
        // 購入タイプ(固定　0:都度購入、1:継続購入)
        return '0';
    }

    function getPaymentRuleDefault() {
        $arrData = parent::getPaymentRuleDefault();
        $arrData['rule_max'] = '2';
        $arrData['upper_rule'] = '9999999';
        $arrData['upper_rule_max'] = $arrData['upper_rule'];
        return $arrData;
    }

    function getPaymentBlocName() {
        return 'sps_credit';
    }

    function initParamPaymentInput(&$objPage, &$objFormParam, &$arrConfig) {
        parent::initParamPaymentInput($objPage, $objFormParam, $arrConfig);

        $key = 'auth_order_status';
        $objFormParam->addParam('登録成功後の受注状況', $key, INT_LEN, 'n',
                array('EXIST_CHECK','MAX_LENGTH_CHECK', 'NUM_CHECK'),
                SC_Utils_Ex::isBlank($arrConfig[$key]) ? ORDER_NEW : $arrConfig[$key]);
    }

    function checkErrorPaymentInput(&$objFormParam) {
        return parent::checkErrorPaymentInput($objFormParam);
    }

    function initParam(&$objFormParam, &$arrPayment, &$arrOrder) {
        parent::initParam($objFormParam, $arrPayment, $arrOrder);
    }

    function modeAction($mode, &$objFormParam, &$arrOrder, $objPage) {
        parent::modeAction($mode, $objFormParam, $arrOrder, $objPage);
    }

    function doPaymentRecvOK(&$arrParam, &$arrOrder, &$arrPayment) {
        $objPurchase = new SC_Helper_Purchase_Ex();

        $sqlval = array();

        $new_status = $arrPayment['auth_order_status'];

        $objPurchase->sfUpdateOrderStatus($arrOrder['order_id'], $new_status, null, null, $sqlval);
        if (SC_Helper_Mobile_Ex::gfIsMobileMailAddress($arrOrder['order_email'])) {
            $template_id = '2';
        } else {
            $template_id = '1';
        }

        $objMailHelper = new SC_Helper_Mail_Ex();
        $objMailHelper->sfSendOrderMail($arrOrder['order_id'], $template_id);
        return true;
    }

    function doPaymentRecvNG(&$arrParam, &$arrOrder, &$arrPayment) {
        return true;
    }

    function doPaymentRecvCN(&$arrParam, &$arrOrder, &$arrPayment) {
        return true;
    }

    function createLinkData(&$arrOrder, &$arrPaymentInfo, &$objFormParam) {
        $arrData = array();
        $objMdl =& SC_Mdl_SPS_Ex::getInstance();
        $arrModuleConfig = $objMdl->getUserSettings();
        $now_time = date("YmdHis");
        $objPurchase = new SC_Helper_Purchase_Ex();

        $objDb = new SC_Helper_DB_Ex();
        $arrInfo = $objDb->sfGetBasisData();

        $arrData['pay_method'] = $this->getPayMethod();
        $arrData['merchant_id'] = $arrModuleConfig['id_sps_marchant'];
        $arrData['service_id'] = $arrModuleConfig['id_sps_service'];
        $arrData['cust_code'] =SC_Util_SPS::getCustomerCode($arrOrder['customer_id']);
        $arrData['sps_cust_no'] = '';
        $arrData['spspayment_no'] = '';
        $arrData['terminal_type'] = SC_Display_Ex::detectDevice() === DEVICE_TYPE_MOBILE ? '1' : '0';
        $arrData['success_url'] = $this->getReturnUrl(MDL_SPS_COMPLETE_URL, $arrOrder);
        $arrData['cancel_url'] = $this->getReturnUrl(MDL_SPS_RETURN_URL, $arrOrder);
        $arrData['error_url'] = $this->getReturnUrl(MDL_SPS_ERROR_URL, $arrOrder);
        $arrData['pagecon_url'] = MDL_SPS_SETTLEMENT_URL . '?ec_order_id=' . $arrOrder['order_id'] . '&tid=' . SC_Helper_Session_Ex::getToken();
        $arrData['free1'] = $arrOrder['order_id'];
        $arrData['free2'] = MDL_SPS_VERSION . '_' .ECCUBE_VERSION;
        $arrData['free3'] = SC_Util_SPS_Ex::subString($arrPaymentInfo['free3'], 20);

        $arrData['request_date'] = $now_time;
        $arrData['limit_second'] = '600';

        $isMobile = SC_Display_Ex::detectDevice() === DEVICE_TYPE_MOBILE;
        $arrData = $this->normalizeData($arrData, $isMobile);

        $arrData['sps_hashcode'] = $this->createSpsHash($arrData);
        return $arrData;
    }

    function getRequestUrl() {
        $objMdl =& SC_Mdl_SPS_Ex::getInstance();
        $url = $objMdl->getUserSettings( 'url_sps_registservice_api' );
        $objMdl->printLog("goto: $url");
        return $url;
    }

}
