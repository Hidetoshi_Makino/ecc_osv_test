<?php
/**
 *
 * @copyright 2013 Spirit of Co.,Ltd. All Rights Reserved.
 * @link http://sbps.ec-cube.pro/
 *
 */
require_once(MDL_SPS_HELPEREX_PATH . 'SC_Mdl_SPS_Helper_Base_Ex.php');

/**
 * 決済個別用ヘルパー関数 Conveniクラス
 */
class SC_Mdl_SPS_Helper_Conveni extends SC_Mdl_SPS_Helper_Base_Ex {

    function init() {
        $this->payment_type_id = SPS_CREDIT_LINK_PAYID;
        $this->payment_name = 'Conveni';
    }

    function getPayMethod() {
        return 'webcvs';
    }

    function getConnectType() {
        return MDL_SPS_CONNECT_TYPE_LINK;
    }

    function getSpsPayType() {
        // 購入タイプ(固定　0:都度購入、1:継続購入)
        return '0';
    }

    function getPaymentGetStatusApiId() {
        return 'MG01-00101-701';
    }

    function isGetStatus() {
        return false;
//        return true;
    }

    function getPaymentRuleDefault() {
        $arrData = parent::getPaymentRuleDefault();
        $arrData['rule_max'] = '1';
        $arrData['upper_rule'] = '300000';
        $arrData['upper_rule_max'] = $arrData['upper_rule'];
        return $arrData;
    }

    function getPaymentBlocName() {
        return 'sps_cvs';
    }

    function initParamPaymentInput(&$objPage, &$objFormParam, &$arrConfig) {
        parent::initParamPaymentInput($objPage, $objFormParam, $arrConfig);

        $key = 'auth_order_status';
        $objFormParam->addParam('決済後の受注状況', $key, INT_LEN, 'n',
                array('EXIST_CHECK','MAX_LENGTH_CHECK', 'NUM_CHECK'),
                SC_Utils_Ex::isBlank($arrConfig[$key]) ? ORDER_PAY_WAIT : $arrConfig[$key]);

        $key = 'commit_order_status';
        $objFormParam->addParam('入金後の受注状況', $key, INT_LEN, 'n',
                array('MAX_LENGTH_CHECK', 'NUM_CHECK'),
                SC_Utils_Ex::isBlank($arrConfig[$key]) ? ORDER_PRE_END : $arrConfig[$key]);

        $key = 'cancel_order_status';
        $objFormParam->addParam('期限切れ後の受注状況', $key, INT_LEN, 'n',
                array('MAX_LENGTH_CHECK', 'NUM_CHECK'),
                SC_Utils_Ex::isBlank($arrConfig[$key]) ? ORDER_CANCEL : $arrConfig[$key]);

    }

    function checkErrorPaymentInput(&$objFormParam) {
        return parent::checkErrorPaymentInput($objFormParam);
    }

    function initParam(&$objFormParam, &$arrPayment, &$arrOrder) {
        parent::initParam($objFormParam, $arrPayment, $arrOrder);
    }

    function modeAction($mode, &$objFormParam, &$arrOrder, $objPage) {
        parent::modeAction($mode, $objFormParam, $arrOrder, $objPage);
    }

    function doPaymentRecvOK(&$arrParam, &$arrOrder, &$arrPayment) {
        $objPurchase = new SC_Helper_Purchase_Ex();

        $sqlval = array();
        $sqlval[MDL_SPS_ORDER_COL_TRANSID] = $arrParam['res_tracking_id'];

        $new_status = $arrPayment['auth_order_status'];

        $objPurchase->sfUpdateOrderStatus($arrOrder['order_id'], $new_status, null, null, $sqlval);
        if (SC_Helper_Mobile_Ex::gfIsMobileMailAddress($arrOrder['order_email'])) {
            $template_id = '2';
        } else {
            $template_id = '1';
        }

        $objMailHelper = new SC_Helper_Mail_Ex();
        $objMailHelper->sfSendOrderMail($arrOrder['order_id'], $template_id);
        return true;
    }

    function doPaymentRecvNG(&$arrParam, &$arrOrder, &$arrPayment) {
        return true;
    }

    function doPaymentRecvCR(&$arrParam, &$arrOrder, &$arrPayment) {
        return true;
    }

    function doPaymentRecvCC(&$arrParam, &$arrOrder, &$arrPayment) {
        return true;
    }

    function getFreeCsv(&$arrOrder, &$arrPaymentInfo, &$objFormParam) {
        $masterData = new SC_DB_MasterData_Ex();
        $arrPref = $masterData->getMasterData('mtb_pref');

        $arrData = array();
        $arrData['LAST_NAME'] = $arrOrder['order_name01'];
        $arrData['FIRST_NAME'] = $arrOrder['order_name02'];
        $arrData['LAST_NAME_KANA'] = $arrOrder['order_kana01'];
        $arrData['FIRST_NAME_KANA'] = $arrOrder['order_kana02'];
        $arrData['FIRST_ZIP'] = $arrOrder['order_zip01'];
        $arrData['SECOND_ZIP'] = $arrOrder['order_zip02'];

        $arrData['ADD1'] = $arrPref[$arrOrder['order_pref']];
        $arrData['ADD2'] = $arrOrder['order_addr01'];
        $arrData['ADD3'] = $arrOrder['order_addr02'];

        $arrData['TEL'] = $arrOrder['order_tel01'] . $arrOrder['order_tel02'] . $arrOrder['order_tel03'];
        $arrData['MAIL'] = $arrOrder['order_email'];
        return $this->makeFreeCsvField($arrData);
    }

    function doApiPaymentGetStatus(&$arrOrder, $arrPayment, $target_datetime) {
        $objMdl =& SC_Mdl_SPS_Ex::getInstance();
        $arrModuleConfig = $objMdl->getUserSettings();

        $arrData = $this->createGetStatusXmlData($arrOrder, $arrPayment, $target_datetime);
        $request_xml = SC_Util_SPS_Ex::changeArray2Xml($arrData, $this->getPaymentCancelApiId());
        list($r_code, $response) = SC_Util_SPS_Ex::sendRequestSps($request_xml, $arrModuleConfig);
        $arrRes = $this->checkResponse($r_code, $response, $this);

        if ($arrRes !== false) {
            $sqlval = array();
            switch ($arrRes['payment_status']) {
            case '1'://未入金
            case '2'://一部入金済
                $new_status = $arrPayment['auth_order_status'];
                break;
            case '3'://満額入金
            case '4'://過入金
                $new_status = $arrPayment['commit_order_status'];
                break;
            case '9'://キャンセル
                $new_status = $arrPayment['cancel_order_status'];
                $sqlval[MDL_SPS_ORDER_COL_RESDATA] = SPS_CANCEL_OK;
                break;
            }
            SC_Util_SPS_Ex::setOrderPayData($arrOrder, $arrRes, false, $new_status, $sqlval);
        } else {
            $sqlva[MDL_SPS_ORDER_COL_RESDATA] = SPS_ERROR;
            SC_Util_SPS_Ex::setOrderPayData($arrOrder, $arrRes, true, $new_status, $sqlval);
            return $this->arrErr['tpl_error'];
        }
        return true;
    }

    function createGetStatusXmlData(&$arrOrder, &$arrPayment, $target_datetime) {
        $arrData = array();
        $objMdl =& SC_Mdl_SPS_Ex::getInstance();
        $arrModuleConfig = $objMdl->getUserSettings();
        $now_time = date("YmdHis");

        $arrData['merchant_id'] = $arrModuleConfig['id_sps_marchant'];
        $arrData['service_id'] = $arrModuleConfig['id_sps_service'];
        $arrData['sps_transaction_id'] = $arrOrder['res_sps_transaction_id'];
        $arrData['tracking_id'] = $arrOrder[MDL_SPS_ORDER_COL_TRANSID];
        $arrData['encrypted_flg'] = '1';
        $arrData['request_date'] = $now_time;
        $arrData['limit_second'] = '600';

        $arrData = $this->normalizeData($arrData, false);
        $arrData['sps_hashcode'] = $this->createSpsHash($arrData);
        return $arrData;

    }

}
