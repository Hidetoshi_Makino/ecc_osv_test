<?php
/**
 *
 * @copyright 2013 Spirit of Co.,Ltd. All Rights Reserved.
 * @link http://sbps.ec-cube.pro/
 *
 */
require_once(MDL_SPS_HELPEREX_PATH . 'SC_Mdl_SPS_Helper_Base_Ex.php');

/**
 * 決済個別用ヘルパー関数 RegistCreditApiクラス
 */
class SC_Mdl_SPS_Helper_RegistCreditApi extends SC_Mdl_SPS_Helper_Base_Ex {

    function init() {
        $this->payment_type_id = SPS_REGIST_CREDIT_API_PAYID;
        $this->payment_name = 'RegistCreditApi';
    }

    function getPayMethod() {
        return 'registcreditapi';
    }

    function getPaymentBlocName() {
        return 'sps_registcreditapi';
    }

    function getConnectType() {
        return MDL_SPS_CONNECT_TYPE_API;
    }

    function getSpsPayType() {
        // 購入タイプ(固定　0:都度購入、1:継続購入)
        return '0';
    }

    function getPaymentRequestApiId() {
        if (SC_Utils_Ex::isBlank($this->cust_code) || $this->cust_code == '-') {
            $objCustomer = new SC_Customer_Ex();
            if($objCustomer->isLoginSuccess(true)) {
                $this->cust_code = SC_Util_SPS_Ex::getCustomerCode($objCustomer->getValue('customer_id'));
            }
        }
        if (SC_Utils_Ex::isBlank($this->cust_code) || $this->cust_code == '-') {
            return null;
        }
        $res = $this->getRegistStatusFromCustCode($this->cust_code);
        if ($res) {
            return 'MG02-00102-101';
        } else {
            return 'MG02-00101-101';
        }
    }

    function getRegistStatusFromCustCode($cust_code) {
        $objMdl =& SC_Mdl_SPS_Ex::getInstance();
        $arrModuleConfig = $objMdl->getUserSettings();

        $arrData = $this->createRegistStatusXmlData($cust_code);
        $request_xml = SC_Util_SPS_Ex::changeArray2Xml($arrData, 'MG02-00104-101');
        list($r_code, $response) = SC_Util_SPS_Ex::sendRequestSps($request_xml, $arrModuleConfig);
        $arrRes = $this->checkResponse($r_code, $response, $this);
        if ($arrRes !== false && $arrRes['cc_number'] != "") {
            return true;
        }
        return false;
    } 

    function getPaymentCommitApiId() {
        return '';
    }

    function isSales() {
        return false;
    }

    function isCancel() {
        return false;
    }

    function isReAuth() {
        return false;
    }

    function isGetStatus() {
        return false;
    }

    function isThreePhase() {
        return false;
    }

    function getPaymentRuleDefault() {
        $arrData = parent::getPaymentRuleDefault();
        $arrData['rule_max'] = '1';
        return $arrData;
    }

    function initParamPaymentInput(&$objPage, &$objFormParam, &$arrConfig) {
        parent::initParamPaymentInput($objPage, $objFormParam, $arrConfig);

        $objPage->arrOptionActive = array('1' => '要', '0' => '不要');

        $key = 'auth_order_status';
        $objFormParam->addParam('与信後の受注状況', $key, INT_LEN, 'n',
                array('EXIST_CHECK','MAX_LENGTH_CHECK', 'NUM_CHECK'),
                SC_Utils_Ex::isBlank($arrConfig[$key]) ? ORDER_NEW : $arrConfig[$key]);

        $key = 'is_security_code';
        $objFormParam->addParam('セキュリティコード', $key, INT_LEN, 'n',
                array('MAX_LENGTH_CHECK', 'NUM_CHECK'),
                SC_Utils_Ex::isBlank($arrConfig[$key]) ? '0' : $arrConfig[$key]);

    }

    function checkErrorPaymentInput(&$objFormParam) {
        return parent::checkErrorPaymentInput($objFormParam);
    }

    function initParam(&$objFormParam, &$arrPayment, &$arrOrder) {
        parent::initParam($objFormParam, $arrPayment, $arrOrder);
        $objFormParam->addParam('カード番号1', 'card_no01', 4, 'n', array('MAX_LENGTH_CHECK', 'NUM_CHECK', 'EXIST_CHECK'));
        $objFormParam->addParam('カード番号2', 'card_no02', 4, 'n', array('MAX_LENGTH_CHECK', 'NUM_CHECK', 'EXIST_CHECK'));
        $objFormParam->addParam('カード番号3', 'card_no03', 4, 'n', array('MAX_LENGTH_CHECK', 'NUM_CHECK', 'EXIST_CHECK'));
        $objFormParam->addParam('カード番号4', 'card_no04', 4, 'n', array('MAX_LENGTH_CHECK', 'NUM_CHECK', 'EXIST_CHECK'));
        $objFormParam->addParam('有効期限:年', 'card_year', 4, 'n', array('MAX_LENGTH_CHECK', 'NUM_CHECK', 'EXIST_CHECK'));
        $objFormParam->addParam('有効期限:月', 'card_month', 4, 'n', array('MAX_LENGTH_CHECK', 'NUM_CHECK', 'EXIST_CHECK'));
        if ($arrPayment['is_security_code']) {
            $objFormParam->addParam('セキュリティコード', 'security_code', 4, 'n', array('MAX_LENGTH_CHECK', 'NUM_CHECK', 'EXIST_CHECK'));
        }
        $objFormParam->addParam("カード情報保持", "cust_manage_flg", 1, "n", array("NUM_CHECK"), '1');
    }

    /**
     * 入力内容のチェックを行なう.
     *
     * @param SC_FormParam $objFormParam SC_FormParam インスタンス
     * @return array 入力チェック結果の配列
     */
    function checkError(&$objFormParam) {
        $arrParam = $objFormParam->getHashArray();
        $objErr = new SC_CheckError_Ex($arrParam);
        $objErr->arrErr = $objFormParam->checkError();
        return $objErr->arrErr;
    }



    function modeAction($mode, &$objFormParam, &$arrOrder, $objPage) {
        // 月日配列
        $objDate = new SC_Date_Ex();
        $objDate->setStartYear(date('Y'));
        $objDate->setEndYear(date('Y') + 15);
        $objPage->arrYear = $objDate->getYear();
        $objPage->arrMonth = $objDate->getZeroMonth();
        if ($mode == 'next' && $objPage->getMode() == '') {
            $mode = '';
        }
        parent::modeAction($mode, $objFormParam, $arrOrder, $objPage);
    }

    function createXmlData(&$arrOrder, &$arrPayment, &$objFormParam) {
        $arrData = array();
        $objMdl =& SC_Mdl_SPS_Ex::getInstance();
        $arrModuleConfig = $objMdl->getUserSettings();
        $now_time = date("YmdHis");

        $arrInput = $objFormParam->getHashArray();

        $objDb = new SC_Helper_DB_Ex();
        $arrInfo = $objDb->sfGetBasisData();

        $arrData['merchant_id'] = $arrModuleConfig['id_sps_marchant'];
        $arrData['service_id'] = $arrModuleConfig['id_sps_service'];
        $arrData['cust_code'] = SC_Util_SPS_Ex::getCustomerCode($arrOrder['customer_id']);

        $this->cust_code = $arrData['cust_code'];

//        $arrData['free1'] = $arrOrder['order_id'];
//        $arrData['free2'] = MDL_SPS_VERSION . '_' .ECCUBE_VERSION;
//        $arrData['free3'] = SC_Util_SPS_Ex::subString($arrPaymentInfo['free3'], 20);
        $arrData['sps_cust_info_return_flg'] = '1';

        $arrData['pay_method_info'] = $this->getApiPayMethodInfoData($arrInput, $arrPayment, $arrOrder);

//        $arrData['pay_option_manage']['cardbrand_return_flg'] ='1';

        $arrData['encrypted_flg'] = '1';
        $arrData['request_date'] = $now_time;
        $arrData['limit_second'] = '600';

        $arrData = $this->normalizeData($arrData, false, true);
        $arrData['sps_hashcode'] = $this->createSpsHash($arrData);
        $objMdl->printLog($arrData);
        $arrData = SC_Util_SPS_Ex::encryptData($arrData, 'pay_method_info', $arrModuleConfig, false);
        $arrData = SC_Util_SPS_Ex::encodeMbstring($arrData, array('free1','free2','free3'));
        return $arrData;
    }

    function createRegistStatusXmlData($cust_code) {
        $arrData = array();
        $objMdl =& SC_Mdl_SPS_Ex::getInstance();
        $arrModuleConfig = $objMdl->getUserSettings();
        $now_time = date("YmdHis");

        $objDb = new SC_Helper_DB_Ex();
        $arrInfo = $objDb->sfGetBasisData();

        $arrData['merchant_id'] = $arrModuleConfig['id_sps_marchant'];
        $arrData['service_id'] = $arrModuleConfig['id_sps_service'];
        $arrData['cust_code'] = $cust_code;

        $arrData['sps_cust_info_return_flg'] = '1';
        $arrData['response_info_type'] = '2';

        $arrData['pay_option_manage']['cardbrand_return_flg'] ='1';

        $arrData['encrypted_flg'] = '1';
        $arrData['request_date'] = $now_time;
        $arrData['limit_second'] = '600';

        $arrData = $this->normalizeData($arrData, false, true);
        $arrData['sps_hashcode'] = $this->createSpsHash($arrData);
        return $arrData;
    }

}
