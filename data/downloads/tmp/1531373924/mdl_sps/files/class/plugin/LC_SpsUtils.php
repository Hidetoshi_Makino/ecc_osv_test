<?php
/*
 * Copyright(c) 2013 Spirit of Co.,Ltd. All rights reserved.
 * http://sbps.ec-cube.pro/
 */

/**
 * プラグインの処理クラス
 */
class LC_SpsUtils {

    function actionPrefilterTransform($class_name, &$source, &$objPage, $filename, $objPlugin) {
        switch($objPage->arrPageLayout['device_type_id']){
            case DEVICE_TYPE_MOBILE:
            case DEVICE_TYPE_SMARTPHONE:
            case DEVICE_TYPE_PC:
                if (preg_match('/^LC_Page_Mypage.*_Ex$/', $class_name) && strpos($filename, 'navi.tpl') === FALSE) {
                    $arrPlgConfig = $this->loadData();
                    if ($arrPlgConfig['mypage_enable'] != '1') {
                        return;
                    }
                    $objTransform = new SC_Helper_Transform($source);
                    $template_file = 'mypage_navi_add.tpl';
                    switch($objPage->arrPageLayout['device_type_id']){
                        case DEVICE_TYPE_MOBILE:
                            if (strpos($filename, 'index.tpl')) {
                                $template_dir = MDL_SPS_TEMPLATE_PATH . 'mobile/';
                                $snip = file_get_contents($template_dir . $template_file);
                                $source = str_replace("退会</a><br>", "退会</a><br>" . $snip, $source);
                                return;
                            }
                            break;
                        case DEVICE_TYPE_SMARTPHONE:
                            $template_dir = MDL_SPS_TEMPLATE_PATH . 'sphone/';
                            $objTransform->select('h2.title',NULL, false)->insertAfter(file_get_contents($template_dir . $template_file));
                            break;
                        case DEVICE_TYPE_PC:
                        default:
                            $template_dir = MDL_SPS_TEMPLATE_PATH . 'default/';
                            $objTransform->select('h2',NULL, false)->appendChild(file_get_contents($template_dir . $template_file));
                            break;
                    }
                    $source = $objTransform->getHTML();
                }
                break;
            case DEVICE_TYPE_ADMIN:
            default:
                if (strpos($filename, 'mail_templates') !== FALSE) {
                    return;
                } else if (strpos($filename, 'popup') !== FALSE) {
                    return;
                } else if (strpos($filename, 'pdf') !== FALSE) {
                    return;
                } else if (strpos($filename, 'csv') !== FALSE) {
                    return;
                }

                if (preg_match('/^LC_Page_Admin.*_Ex$/', $class_name)) {
                    $template_dir = MDL_SPS_TEMPLATE_PATH . 'admin/';
                    $objTransform = new SC_Helper_Transform($source);
                    $template_file = 'admin_common_utils_add.tpl';
                    $objTransform->select('head')->appendChild(file_get_contents($template_dir . $template_file));
                    $source = $objTransform->getHTML();
                }

                switch($filename){
                    case 'order/edit.tpl':
                        // 受注登録・編集画面
                        $template_dir = MDL_SPS_TEMPLATE_PATH . 'admin/';
                        $template_file = 'order_edit_status_add.tpl';
                        $objTransform = new SC_Helper_Transform($source);
                        $objTransform->select('div#order')->appendFirst(file_get_contents($template_dir . $template_file));

                        $source = $objTransform->getHTML();
                        break;
                    default:
                        break;
                }
                break;
        }

    }

    function actionHook($class_name, $hook_point, &$objPage, $objPlugin) {
        switch ($class_name) {
        case 'LC_Page_Admin_Order_Edit_Ex':
            if ($hook_point == 'after') {
                $this->lfDoAdminOrderEditAfter($objPage);
            } else if($hook_point == 'before') {
                $this->lfDoAdminOrderEditBefore($objPage);
            }
            break;
        case 'LC_Page_Admin_Customer_Ex':
//            $this->lfDoAdminCustomer($objPage);
            break;
        }
    }

    function lfDoAdminOrderAfter(&$objPage) {
    }

    function lfDoAdminOrderEditAfter(&$objPage) {
        // データ読み込み
        if (!SC_Utils_Ex::isBlank($objPage->arrForm['order_id']['value'])) {

            $objPage->arrSbpsStatus = array(
            SPS_AUTH_DATE_COMMIT => '決済後/与信済み',
            SPS_COMMIT_OK => '売上確定済み',
            SPS_DATECOMMIT_OK => '指定日売上確定済み',
            SPS_CANCEL_OK => '返金・取消済み',
            SPS_ERROR => '通信失敗など',
            SPS_WAIT => '処理待ち',
            );

            $order_id = $objPage->arrForm['order_id']['value'];
            $objPage->arrPaymentData = SC_Util_SPS_Ex::getOrderPayData($order_id);
            if ($objPage->arrPaymentData[MDL_SPS_ORDER_COL_PAYID]) {
                $payment_type_id = $objPage->arrPaymentData[MDL_SPS_ORDER_COL_PAYID];
                $objPage->plg_sps_payid = $payment_type_id;
                $objPayment =& SC_Mdl_SPS_Helper_Ex::getPaymentHelper($payment_type_id);
                $objPage->plg_sps_is_commit = $objPayment->isSales();
                $objPage->plg_sps_is_cancel = $objPayment->isCancel();
                $objPage->plg_sps_is_getstatus = $objPayment->isGetStatus();
                $objPage->plg_sps_is_reauth = $objPayment->isReAuth();
                $objPage->plg_sps_is_every = $objPayment->getSpsPayType() == '1' ? true : false;
                $objPage->plg_sps_is_everystop = $objPayment->isEveryStop();
                $objPage->plg_sps_is_dopayment = $objPayment->isDoPayment();
                $objPage->plg_sps_status = $objPage->arrPaymentData[MDL_SPS_ORDER_COL_RESDATA];
                if ($objPage->plg_sps_status == "" && $arrPaymentData['res_result'] == 'OK') {
                    $objPage->plg_sps_status = '0';
                }

                $arrPayNames = SC_Util_SPS_Ex::getPaymentTypeNames();
                $objPage->plg_sps_pay_name = $arrPayNames[$objPage->plg_sps_payid];

                if ($payment_type_id == SPS_CONVENI_API_PAYID) {
                    require_once(MDL_SPS_HELPEREX_PATH . 'SC_Mdl_SPS_Helper_ConveniApi_Ex.php');
                    $objPage->arrConveni = SC_Mdl_SPS_Helper_ConveniApi_Ex::getConvenis();
                }
            }
        }

        if (!SC_Utils_Ex::isBlank($objPage->arrForm['payment_id']['value'])) {
            $payment_id = $objPage->arrForm['payment_id']['value'];
            $arrPaymentInfo = SC_Util_SPS_Ex::getPaymentTypeConfig($payment_id);
            $objPage->arrPaymentInfo = $arrPaymentInfo;
        }
    }

    function lfDoAdminOrderEditBefore(&$objPage) {
        $mode = $objPage->getMode();
        $objFormParam = new SC_FormParam_Ex();
        $objFormParam->addParam('指定年月日 年', 'sps_year', INT_LEN, 'n', array('MAX_LENGTH_CHECK', 'NUM_CHECK'),date('Y'));
        $objFormParam->addParam('指定年月日 月', 'sps_month', INT_LEN, 'n', array('MAX_LENGTH_CHECK', 'NUM_CHECK'), date('m'));
        $objFormParam->addParam('指定年月日 日', 'sps_day', INT_LEN, 'n', array('MAX_LENGTH_CHECK', 'NUM_CHECK'), date('d'));

        $objFormParam->addParam('コンビニ', 'webcvstype', INT_LEN, 'n', array('MAX_LENGTH_CHECK', 'NUM_CHECK'));

        switch ($mode) {
        case 'plg_sps_commit':
        case 'plg_sps_cancel':
        case 'plg_sps_reauth':
        case 'plg_sps_get_status':
        case 'plg_sps_everystop':
        case 'plg_sps_dopayment':
            if (!SC_Utils_Ex::isBlank($_POST['order_id']) && SC_Utils_Ex::sfIsInt($_POST['order_id'])) {
                $objFormParam->setParam($_POST);
                $objFormParam->convParam();
                $objPage->arrSpsForm = $objFormParam->getHashArray();
                $arrErr = $objFormParam->checkError();
                $objErr = new SC_CheckError_Ex($objPage->arrSpsForm);
                $objErr->doFunc(array("売上・返金日", "sps_year", "sps_month", "sps_day"), array("CHECK_DATE"));
                $arrErr = array_merge($arrErr, $objErr->arrErr);
                if (SC_Utils_Ex::isBlank($arrErr)) {
                    $ymd = sprintf('%04d%02d%02d', $objFormParam->getValue('sps_year'),
                                        $objFormParam->getValue('sps_month'),
                                        $objFormParam->getValue('sps_day'));
                    $order_id = $_POST['order_id'];
                    $objSPS =& SC_Mdl_SPS_Ex::getInstance();
                    $objSPS->printLog("mode=$mode pdate=$ymd order_id=$order_id");
                    $objQuery =& SC_Query_Ex::getSingletonInstance();
                    $objQuery->begin();

                    switch ($mode) {
                    case 'plg_sps_commit':
                        $ret = SC_Mdl_SPS_Helper_Ex::doPaymentSales($order_id, $ymd);
                        break;
                    case 'plg_sps_cancel':
                        $ret = SC_Mdl_SPS_Helper_Ex::doPaymentCancel($order_id, $ymd);
                        break;
                    case 'plg_sps_reauth':
                        $ret = SC_Mdl_SPS_Helper_Ex::doPaymentReAuth($order_id, $ymd);
                        break;
                    case 'plg_sps_get_status':
                        $ret = SC_Mdl_SPS_Helper_Ex::doPaymentGetStatus($order_id, $ymd);
                        break;
                    case 'plg_sps_everystop':
                        $ret = SC_Mdl_SPS_Helper_Ex::doPaymentEveryStop($order_id, $ymd);
                        break;
                    case 'plg_sps_dopayment':
                        $ret = SC_Mdl_SPS_Helper_Ex::doPaymentDoPayment($order_id, $objFormParam);
                    }
                    $objQuery->commit();

                    if ($ret === false) {
                        $arrErr[] = '処理出来ない注文です。 受注ID:' . $order_id;
                    } else if($ret === true) {
                        $arrErr[] = '正常に処理をしました。 受注ID:' . $order_id;
                    } else {
                        $arrErr[] = $ret .  ' 受注ID:' . $order_id;
                    }
                }
                $objPage->plg_sps_error = implode('<br />', $arrErr);
            }
            $_GET['mode'] = 'recalculate';
            break;
        }
        $objPage->arrSpsForm = $objFormParam->getFormParamList();
    }

    function loadData() {
        $arrRet = array();
        $objQuery =& SC_Query_Ex::getSingletonInstance();
        $where = "plugin_code = 'SpsUtils'";
        $arrData = $objQuery->getRow('*', 'dtb_plugin', $where);
        if (!SC_Utils_Ex::isBlank($arrData['free_field1'])) {
            $arrRet = unserialize($arrData['free_field1']);
        }
        return $arrRet;
    }

}
