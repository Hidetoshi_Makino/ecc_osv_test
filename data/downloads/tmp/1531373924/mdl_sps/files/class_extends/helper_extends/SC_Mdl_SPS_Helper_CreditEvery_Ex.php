<?php
/**
 *
 * @copyright 2013 Spirit of Co.,Ltd. All Rights Reserved.
 * @link http://sbps.ec-cube.pro/
 *
 */
require_once(MDL_SPS_HELPER_PATH . 'SC_Mdl_SPS_Helper_CreditEvery.php');

/**
 * 決済個別用ヘルパー関数 クレジットクラス
 */
class SC_Mdl_SPS_Helper_CreditEvery_Ex extends SC_Mdl_SPS_Helper_CreditEvery {

    function createLinkData(&$arrOrder, &$arrPaymentInfo, &$objFormParam) {
        $arrData = parent::createLinkData($arrOrder, $arrPaymentInfo, $objFormParam);

        //$arrData['service_id'] = '003'; // サービスID 書き換え
        // hash 再生成
        unset ($arrData['sps_hashcode']);
        $isMobile = SC_Display_Ex::detectDevice() === DEVICE_TYPE_MOBILE;        
        $arrData = $this->normalizeData($arrData, $isMobile);

        $arrData['sps_hashcode'] = $this->createSpsHash($arrData);
        return $arrData;
    }

}
