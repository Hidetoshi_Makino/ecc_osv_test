<?php
/*
 * Copyright(c) 2013 Spirit of Co.,Ltd. All rights reserved.
 * http://sbps.ec-cube.pro/
 */

// {{{ requires
require_once(MDL_SPS_CLASS_PATH . "pages/LC_Page_Admin_Order_SbpsStatus.php");

/**
 * SPS決済モジュールの管理画面設定のページクラス.
 *
 * @package Page
 * @author SOFTBANK PAYMENT SERVICE Corp.
 */
class LC_Page_Admin_Order_SbpsStatus_Ex extends LC_Page_Admin_Order_SbpsStatus {

}
