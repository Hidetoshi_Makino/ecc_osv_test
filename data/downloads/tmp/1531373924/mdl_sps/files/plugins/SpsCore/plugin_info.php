<?php
/**
 * プラグイン の情報クラス.
 *
 * @package SpsCore
 * @author Spirit of Co.,Ltd.
 */
class plugin_info{
    /** プラグインコード(必須)：プラグインを識別する為キーで、他のプラグインと重複しない一意な値である必要がありま. */
    static $PLUGIN_CODE       = "SpsCore";
    /** プラグイン名(必須)：EC-CUBE上で表示されるプラグイン名. */
    static $PLUGIN_NAME       = "ソフトバンク・ペイメント・サービス決済コアプラグイン";
    /** クラス名(必須)：プラグインのクラス（拡張子は含まない） */
    static $CLASS_NAME        = "SpsCore";
    /** プラグインバージョン(必須)：プラグインのバージョン. */
    static $PLUGIN_VERSION    = "1.1";
    /** 対応バージョン(必須)：対応するEC-CUBEバージョン. */
    static $COMPLIANT_VERSION = "2.13.0";
    /** 作者(必須)：プラグイン作者. */
    static $AUTHOR            = "SBPS決済EC-CUBE用モジュールサポート事務局";
    /** 説明(必須)：プラグインの説明. */
    static $DESCRIPTION       = "SBPS決済モジュールの動作に必須なプラグインです。";
    /** プラグインURL：プラグイン毎に設定出来るURL（説明ページなど） */
    static $PLUGIN_SITE_URL   = "http://sbps.ec-cube.pro/";
    /** プラグイン作者URL：プラグイン毎に設定出来るURL（説明ページなど） */
    static $AUTHOR_SITE_URL   = "http://sbps.ec-cube.pro/";
    /** ライセンス **/
    // ライセンスについては同梱のLICENSE.txtを参照して下さい。
    static $LICENSE = "ORG";
    /** フックポイント：フックポイントとコールバック関数を定義します */
    static $HOOK_POINTS       = array(
        array("LC_Page_Admin_Basis_PaymentInput_action_after", 'hookActionAfter'),
        array("LC_Page_Shopping_Payment_action_after", 'hookActionAfter'),
        array("LC_Page_Shopping_Payment_action_before", 'hookActionBefore'),
        array("LC_Page_Shopping_Complete_action_before", 'hookActionBefore'),
        array("prefilterTransform", 'prefilterTransform')
        );
}
