<?php
/*
 *
 * Copyright(c) 2012 Spirit of Co.,Ltd. All Rights Reserved.
 *
 * http://www.spiritof.jp/
 *
 * License: see a LICENCE.txt file.
 */
require_once(MODULE_REALDIR . "mdl_sps/inc/include.php");
require_once(MDL_SPS_CLASSEX_PATH . 'plugin_extends/LC_SpsCore_Ex.php');

/**
 * プラグインのメインクラス
 *
 * @package SpsCore
 * @author Spirit of Co.,Ltd.
 */
class SpsCore extends SC_Plugin_Base {

    static $plugin_name = 'SpsCore';
    static $plugin_data = null;

    /**
     * コンストラクタ
     */
    public function __construct(array $arrSelfInfo) {
        parent::__construct($arrSelfInfo);
        if (!$this->plugin_name) {
            $this->plugin_name = $arrSelfInfo['class_name'];
        }
    }

    /**
     * インストール
     * installはプラグインのインストール時に実行されます.
     * 引数にはdtb_pluginのプラグイン情報が渡されます.
     *
     * @param array $arrPlugin plugin_infoを元にDBに登録されたプラグイン情報(dtb_plugin)
     * @return void
     */
    function install($arrPlugin) {
        if (!is_object($arrPlugin)) {
            $my_obj = new $arrPlugin['class_name']($arrPlugin);
        } else {
            $my_obj = $this;
        }
        $my_obj->plugin_data = $arrPlugin;
        $class_name = 'LC_' . $my_obj->plugin_name;
        $obj = new $class_name();
        $arrFiles = $obj->getInstallFiles($my_obj);
        $arrErr = $my_obj->installFiles($arrFiles);

        if (!SC_Utils_Ex::isBlank($arrErr)) {
            $msg = 'プラグインインストールエラー: ' . implode('<br />', $arrErr);
            GC_Utils_Ex::gfPrintLog($msg);
        }
        $obj->actionInstall($my_obj);
    }

    /**
     * アンインストール
     * uninstallはアンインストール時に実行されます.
     * 引数にはdtb_pluginのプラグイン情報が渡されます.
     *
     * @param array $arrPlugin プラグイン情報の連想配列(dtb_plugin)
     * @return void
     */
    function uninstall($arrPlugin) {
        if (!is_object($arrPlugin)) {
            $my_obj = new $arrPlugin['class_name']($arrPlugin);
        } else {
            $my_obj = $this;
        }

        $my_obj->plugin_data = $arrPlugin;
        $class_name = 'LC_' . $my_obj->plugin_name;
        $obj = new $class_name();
        $arrFiles = $obj->getInstallFiles($my_obj);
        if (!SC_Utils_Ex::isBlank($arrFiles)) {
            foreach ($arrFiles as $src => $dst) {
                SC_Helper_FileManager_Ex::deleteFile($dst);
            }
        }
        $obj->actionUninstall($my_obj);
    }

    /**
     * 稼働
     * enableはプラグインを有効にした際に実行されます.
     * 引数にはdtb_pluginのプラグイン情報が渡されます.
     *
     * @param array $arrPlugin プラグイン情報の連想配列(dtb_plugin)
     * @return void
     */
    function enable($arrPlugin) {
        if (!is_object($arrPlugin)) {
            $my_obj = new $arrPlugin['class_name']($arrPlugin);
        } else {
            $my_obj = $this;
        }
        $my_obj->plugin_data = $arrPlugin;
        $class_name = 'LC_' . $my_obj->plugin_name;
        $obj = new $class_name();
        $arrFiles = $obj->getEnableFiles($my_obj);
        $arrErr = $my_obj->installFiles($arrFiles);

        if (!SC_Utils_Ex::isBlank($arrErr)) {
            $msg = 'プラグイン enableエラー: ' . implode('<br />', $arrErr);
            GC_Utils_Ex::gfPrintLog($msg);
        }
        $obj->actionEnable($my_obj);
    }

    /**
     * 停止
     * disableはプラグインを無効にした際に実行されます.
     * 引数にはdtb_pluginのプラグイン情報が渡されます.
     *
     * @param array $arrPlugin プラグイン情報の連想配列(dtb_plugin)
     * @return void
     */
    function disable($arrPlugin) {

        if (!is_object($arrPlugin)) {
            $my_obj = new $arrPlugin['class_name']($arrPlugin);
        } else {
            $my_obj = $this;
        }

        $my_obj->plugin_data = $arrPlugin;
        $class_name = 'LC_' . $my_obj->plugin_name;
        $obj = new $class_name();
        $arrFiles = $obj->getEnableFiles($my_obj);
        if (!SC_Utils_Ex::isBlank($arrFiles)) {
            foreach ($arrFiles as $src => $dst) {
                SC_Helper_FileManager_Ex::deleteFile($dst);
            }
        }
        $obj->actionDisable($my_obj);
    }

    /**
     * 処理の介入箇所とコールバック関数を設定
     * registerはプラグインインスタンス生成時に実行されます
     *
     * @param SC_Helper_Plugin $objHelperPlugin
     */
    function register(SC_Helper_Plugin $objHelperPlugin) {
        return parent::register($objHelperPlugin, $priority);
    }

    // プラグインの設定値を読み込む
    function loadData($force_read = false) {
        $arrRet = array();
        if ($this->plugin_data && $force_read) {
            return $this->plugin_data;
        }
        $arrData = SC_Plugin_Util_Ex::getPluginByPluginCode($this->plugin_name);
        if (!SC_Utils_Ex::isBlank($arrData['free_field1'])) {
            $arrRet = unserialize($arrData['free_field1']);
        }
        $this->plugin_data = $arrRet;
        return $arrRet;
    }

    // プラグイン独自の設定データを保存
    function saveData($arrData) {
        $this->plugin_data = $arrData;

        $objQuery = SC_Query_Ex::getSingletonInstance();
        $sqlval = array();
        $sqlval['free_field1'] = serialize($arrData);
        $sqlval['update_date'] = 'CURRENT_TIMESTAMP';
        $where = "plugin_code = ?";
        $objQuery->update('dtb_plugin', $sqlval, $where, array($this->plugin_info));
    }

    // プラグイン独自の設定データ個別を更新
    function setValue($key, $value) {
        $objQuery = SC_Query_Ex::getSingletonInstance();
        $objQuery->begin();
        $arrData = $this->loadData(true);
        $arrData[$key] = $value;
        $this->saveData($arrData);
        $objQuery->commit();
    }

    // プラグイン独自の設定データ個別を読み込み
    function getValue($key) {
        $arrData = $this->loadData(true);
        return $arrData[$key];
    }

    /**
     * プレフィルタコールバック関数
     *
     * @param string &$source テンプレートのHTMLソース
     * @param LC_Page_Ex $objPage ページオブジェクト
     * @param string $filename テンプレートのファイル名
     * @return void
     */
    function prefilterTransform(&$source, LC_Page_Ex $objPage, $filename) {
        $class_name = get_class($objPage);
        $lc_class_name = 'LC_' . $this->plugin_name;
        $obj = new $lc_class_name();
        $obj->actionPrefilterTransform($class_name, $source, $objPage, $filename, $this);
    }

    /**
        以下、各フック関数。処理はLC_XXXクラスに.
    **/
    function hookActionBefore(LC_Page_Ex $objPage) {
        $this->callHookAction('before', $objPage);
    }

    function hookActionAfter(LC_Page_Ex $objPage) {
        $this->callHookAction('after', $objPage);
    }

    function hookActionMode(LC_Page_Ex $objPage) {
        $this->callHookAction('mode', $objPage);
    }

    function callHookAction($hook_point, &$objPage) {
        $class_name = get_class($objPage);
        $lc_class_name = 'LC_' . $this->plugin_name;
        $obj = new $lc_class_name();
        $obj->actionHook($class_name, $hook_point, $objPage, $this);
    }

    function installFiles($arrFiles) {
        $arrErr = array();
        if (!SC_Utils_Ex::isBlank($arrFiles)) {
            foreach ($arrFiles as $file) {
                GC_Utils_Ex::gfPrintLog($file['src'] . 'を' .  $file['dst'] . 'に複製します。');
                if (is_dir($file['src'])) {
                    SC_Utils_Ex::sfCopyDir($file['src'], $file['dst']);
                    if (!is_dir($file['dst'])) {
                        $arrErr[] = $file['src'] . 'を' . $file['dst'] . 'に複製出来ませんでした。';
                    }
                } else if (is_file($file['src'])) {
                    $temp = copy($file['src'], $file['dst']);
                    if (!is_file($file['dst'])) {
                        $arrErr[] = $file['src'] . 'を' . $file['dst'] . 'に複製出来ませんでした。';
                    }
                } else {
                    $arrErr[] = '複製元ファイル' . $file['src'] . 'が存在しませんでした。';
                }
                GC_Utils_Ex::gfPrintLog($file['src'] . 'を' .  $file['dst'] . 'に複製しました。');
            }
        }
        return $arrErr;
    }
}
