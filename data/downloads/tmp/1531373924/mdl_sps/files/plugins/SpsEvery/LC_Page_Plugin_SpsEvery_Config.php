<?php
/**
 *
 * @copyright 2013 Spirit of Co.,Ltd. All Rights Reserved.
 * @link http://sbps.ec-cube.pro/
 *
 */

// {{{ requires
require_once CLASS_EX_REALDIR . 'page_extends/admin/LC_Page_Admin_Ex.php';

/**
 * プラグインの設定画面クラス
 *
 * @package SpsUtils
 */
class LC_Page_Plugin_SpsEvery_Config extends LC_Page_Admin_Ex {

    var $arrForm = array();

    /**
     * 初期化する.
     *
     * @return void
     */
    function init() {
        parent::init();
        $this->tpl_mainpage = PLUGIN_UPLOAD_REALDIR ."SpsUtils/config.tpl";
        $this->tpl_subtitle = "ソフトバンク・ペイメント・サービス簡易継続課金プラグイン";
        $this->arrConnectType = array('1' => 'API', '2' => 'LINK');
        $this->arrEnableType = array('1' => '有効', '0' => '無効');

        $masterData = new SC_DB_MasterData_Ex();
        $this->arrProductType = $masterData->getMasterData('mtb_product_type');

    }

    /**
     * プロセス.
     *
     * @return void
     */
    function process() {
        $this->action();
        $this->sendResponse();
    }

    /**
     * Page のアクション.
     *
     * @return void
     */
    function action() {
        $objFormParam = new SC_FormParam_Ex();
        $this->lfInitParam($objFormParam);
        $objFormParam->setParam($_POST);
        $objFormParam->convParam();

        $arrForm = array();
        switch ($this->getMode()) {
        case 'add_product_type':
                $this->lfAddProductType($_SESSION['member_id']);
                $masterData = new SC_DB_MasterData_Ex();
                $masterData->clearCache('mtb_product_type');
                $this->arrProductType = $masterData->getMasterData('mtb_product_type');
            break;
        case 'register':
            $arrForm = $objFormParam->getHashArray();
            $this->arrErr = $objFormParam->checkError();
            // エラーなしの場合にはデータを送信
            if (count($this->arrErr) == 0) {
                $this->arrErr = $this->registData($arrForm);
                if (count($this->arrErr) == 0) {
                    SC_Utils_Ex::clearCompliedTemplate();
                    $this->tpl_onload = "alert('設定が完了しました。');";
                }
            }
            break;
        default:
            $arrForm = $this->loadData();
            $this->tpl_is_init = true;
            break;
        }
        $this->arrForm = $arrForm;
        $this->setTemplate($this->tpl_mainpage);
    }

    /**
     * デストラクタ.
     *
     * @return void
     */
    function destroy() {
    }

    /**
     * パラメーター情報の初期化
     *
     * @param object $objFormParam SC_FormParamインスタンス
     * @return void
     */
    function lfInitParam(&$objFormParam) {
        $objFormParam->addParam("継続課金用の商品種別", "every_product_type", INT_LEN, 'n', array("EXIST_CHECK", "MAX_LENGTH_CHECK"));
        $objFormParam->addParam("ユーザー解約機能", 'evenry_user_cancel_is_enable', INT_LEN, 'n', array("MAX_LENGTH_CHECK",'NUM_CHECK'));
        $objFormParam->addParam("キャンペーン設定", 'camp_type', INT_LEN, 'n', array('MAX_LENGTH_CHECK','NUM_CHECK'));
    }


    function loadData() {
        $arrRet = array();
        $objQuery =& SC_Query_Ex::getSingletonInstance();
        $where = "plugin_code = 'SpsEvery'";
        $arrData = $objQuery->getRow('*', 'dtb_plugin', $where);
        if (!SC_Utils_Ex::isBlank($arrData['free_field1'])) {
            $arrRet = unserialize($arrData['free_field1']);
        }
        return $arrRet;
    }

    function registData($arrData) {
        $objQuery =& SC_Query_Ex::getSingletonInstance();
        $objQuery->begin();
        // UPDATEする値を作成する。
        $sqlval = array();
        $sqlval['free_field1'] = serialize($arrData);
        $sqlval['free_field2'] = '';
        $sqlval['update_date'] = 'CURRENT_TIMESTAMP';
        $where = "plugin_code = 'SpsEvery'";
        // UPDATEの実行
        $objQuery->update('dtb_plugin', $sqlval, $where);
        $objQuery->commit();
    }

    /**
     * プロダクトタイプ追加
     *
     * @param $member_id 管理者ID
     * @return array()
     */
    function lfAddProductType($member_id) {
        $objMdl =& SC_Mdl_SPS_Ex::getInstance();
        $objQuery =& SC_Query_Ex::getSingletonInstance();
        $objQuery->begin();
        $id = $objQuery->max('id', 'mtb_product_type');
        $rank = $objQuery->max('rank', 'mtb_product_type');
        $arrData = array( 'id' => $id + 1, 'name' => '継続課金商品' , 'rank' => $rank + 1);
        $objQuery->insert('mtb_product_type', $arrData);

        // 配送情報登録
        $sqlval = array();
        $sqlval['name'] = '継続課金配送業者';
        $sqlval['service_name'] = '継続課金配送';
        $sqlval['remark'] = '継続課金の配送用業者です。';
        $sqlval['product_type_id'] = $id + 1;
        $sqlval['creator_id'] = $member_id;
        $sqlval['update_date'] = 'now()';

        $deliv_id = $objQuery->nextVal('dtb_deliv_deliv_id');
        $sqlval['deliv_id'] = $deliv_id;
        $sqlval['rank'] = $objQuery->max('rank', "dtb_deliv") + 1;
        $sqlval['create_date'] = 'now()';
        $objQuery->insert("dtb_deliv", $sqlval);

        foreach ($this->arrPref as $pref_id => $pref_name) {
            $sqlval = array();
            $sqlval['deliv_id'] = $deliv_id;
            $sqlval['fee_id'] = $pref_id;
            $sqlval['fee'] = '0';
            $sqlval['pref'] = $pref_id;
            $objQuery->insert("dtb_delivfee", $sqlval);
        }

        // 支払い方法
        $sqlval = array();
        $sqlval['deliv_id'] = $deliv_id;
        $arrPaymentIds = $objQuery->getCol('payment_id', 'dtb_payment', "module_code = ? and (memo03 = ? OR memo03 = ? OR memo03 = ? OR memo03 = ?) and del_flg = 0", array($objMdl->getCode(true),
         SPS_CREDIT_EVERY_LINK_PAYID, SPS_CREDIT_EVERY_API_PAYID, SPS_AU_EVERY_LINK_PAYID, SPS_DOCOMO_EVERY_LINK_PAYID));

        $counter = 1;
        foreach($arrPaymentIds as $payment_id) {
            $sqlval['payment_id'] = $payment_id;
            $sqlval['rank'] = $counter;
            $objQuery->insert('dtb_payment_options', $sqlval);
            $counter ++;
        }

        $objQuery->commit();
    }

}

