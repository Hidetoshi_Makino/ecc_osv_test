<?php
/*
 * Copyright(c) SOFTBANK PAYMENT SERVICE Corp. All rights reserved.
 * http://www.sbpayment.jp/
 */

if (is_file(MODULE_REALDIR . 'mdl_sps/inc/include.php')) {
    require_once(MODULE_REALDIR . 'mdl_sps/inc/include.php');
}

require_once(PLUGIN_UPLOAD_REALDIR . 'Sps2Click/LC_Sps2Click_Ex.php');


/**
 * プラグインのメインクラス
 *
 * @package Sps2Click
 * @author SOFTBANK PAYMENT SERVICE Corp.
 */
class Sps2Click extends SC_Plugin_Base {

    /**
     * コンストラクタ
     */
    public function __construct(array $arrSelfInfo) {
        parent::__construct($arrSelfInfo);
    }

    /**
     * インストール
     * installはプラグインのインストール時に実行されます.
     * 引数にはdtb_pluginのプラグイン情報が渡されます.
     *
     * @param array $arrPlugin plugin_infoを元にDBに登録されたプラグイン情報(dtb_plugin)
     * @return void
     */
    function install($arrPlugin) {
        // 必要なファイルをコピーします.
        if (!is_dir(PLUGIN_HTML_REALDIR . "Sps2Click")) {
            mkdir(PLUGIN_HTML_REALDIR . "Sps2Click");
        }
        copy(PLUGIN_UPLOAD_REALDIR . "Sps2Click/logo.png", PLUGIN_HTML_REALDIR . "Sps2Click/logo.png");
        copy(PLUGIN_UPLOAD_REALDIR . "Sps2Click/btn_2click.gif", PLUGIN_HTML_REALDIR . "Sps2Click/btn_2click.gif");
        copy(PLUGIN_UPLOAD_REALDIR . "Sps2Click/btn_2click_on.gif", PLUGIN_HTML_REALDIR . "Sps2Click/btn_2click_on.gif");
    }

    /**
     * アンインストール
     * uninstallはアンインストール時に実行されます.
     * 引数にはdtb_pluginのプラグイン情報が渡されます.
     *
     * @param array $arrPlugin プラグイン情報の連想配列(dtb_plugin)
     * @return void
     */
    function uninstall($arrPlugin) {
    }

    /**
     * 稼働
     * enableはプラグインを有効にした際に実行されます.
     * 引数にはdtb_pluginのプラグイン情報が渡されます.
     *
     * @param array $arrPlugin プラグイン情報の連想配列(dtb_plugin)
     * @return void
     */
    function enable($arrPlugin) {
        // nop
    }

    /**
     * 停止
     * disableはプラグインを無効にした際に実行されます.
     * 引数にはdtb_pluginのプラグイン情報が渡されます.
     *
     * @param array $arrPlugin プラグイン情報の連想配列(dtb_plugin)
     * @return void
     */
    function disable($arrPlugin) {
        // nop
    }

    /**
     * 処理の介入箇所とコールバック関数を設定
     * registerはプラグインインスタンス生成時に実行されます
     *
     * @param SC_Helper_Plugin $objHelperPlugin
     */
    function register(SC_Helper_Plugin $objHelperPlugin) {
        return parent::register($objHelperPlugin, $priority);
    }

    // プラグイン独自の設定データを追加
    function insertFreeField() {
    }

    function insertBloc($arrPlugin) {
    }

    /**
     * プレフィルタコールバック関数
     *
     * @param string &$source テンプレートのHTMLソース
     * @param LC_Page_Ex $objPage ページオブジェクト
     * @param string $filename テンプレートのファイル名
     * @return void
     */
    function prefilterTransform(&$source, LC_Page_Ex $objPage, $filename) {
        $class_name = get_class($objPage);
        $obj = new LC_Sps2Click_Ex();
        $obj->actionPrefilterTransform($class_name, $source, $objPage, $filename, $this);
    }

    function hookActionBefore(LC_Page_Ex $objPage) {
        $this->callHookAction('before', $objPage);
    }

    function hookActionAfter(LC_Page_Ex $objPage) {
        $this->callHookAction('after', $objPage);
    }

    function hookActionMode(LC_Page_Ex $objPage) {
        $this->callHookAction('mode', $objPage);
    }

    function callHookAction($hook_point, &$objPage) {
        $class_name = get_class($objPage);
        $obj = new LC_Sps2Click_Ex();
        $obj->actionHook($class_name, $hook_point, $objPage, $this);
    }

    function loadData() {
        $arrRet = array();
        $arrData = SC_Plugin_Util_Ex::getPluginByPluginCode("Sps2Click");
        if (!SC_Utils_Ex::isBlank($arrData['free_field1'])) {
            $arrRet = unserialize($arrData['free_field1']);
        }
        return $arrRet;
    }

}
