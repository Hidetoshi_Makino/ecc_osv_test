<!--{*
 * Copyright(c) 2013 Spirit of Co.,Ltd. All rights reserved.
 * http://sbps.ec-cube.pro/
 *}-->
<!--{include file="`$smarty.const.TEMPLATE_ADMIN_REALDIR`admin_popup_header.tpl"}-->
<!--{include file='css/contents.tpl'}-->
<script type="text/javascript">
<!--
self.moveTo(20,20);self.focus();

function win_open(URL){
    var WIN;
    WIN = window.open(URL);
    WIN.focus();
}

//-->
</script>
<style type="text/css">
.info { font-size: 90%; };
</style>



<h1><span class="title"><!--{$tpl_subtitle}--></span></h1>

<h2>マイページ設定</h2>
<form name="form1" id="form1" method="post" action="<!--{$smarty.server.REQUEST_URI|escape}-->">
  <input type="hidden" name="mode" value="" />
  <input type="hidden" name="<!--{$smarty.const.TRANSACTION_ID_NAME}-->" value="<!--{$transactionid}-->" />
  <table class="form">
    <colgroup width="20%">
    <colgroup width="80%">
    <!--{if $arrErr.err != ""}-->
    <tr>
      <td colspan="2"><!--{$arrErr.err}--></td>
    </tr>
    <!--{/if}-->

    <!--{assign var=key value="mypage_enable"}-->
    <tr id="<!--{$key}-->">
      <th>ＭＹページ クレジット登録変更画面<span class="attention"> *</span></th>
      <td>
        <!--{if $arrErr[$key]}--><span class="attention"><!--{$arrErr[$key]}--></span><!--{/if}-->
        <!--{html_radios name=$key options=$arrEnableType selected=$arrForm[$key] separator="&nbsp;"}-->
      </td>
    </tr>

    <!--{assign var=key value="mypage_type"}-->
    <tr id="<!--{$key}-->">
      <th>ＭＹページ接続方法<span class="attention"> *</span></th>
      <td>
        <!--{if $arrErr[$key]}--><span class="attention"><!--{$arrErr[$key]}--></span><!--{/if}-->
        <!--{html_radios name=$key options=$arrConnectType selected=$arrForm[$key] separator="&nbsp;"}-->
      </td>
    </tr>

  </table>
  <div class="btn-area">
    <ul>
      <li>
        <a class="btn-action" href="javascript:;" onclick="document.form1.mode.value='register';document.body.style.cursor = 'wait';document.form1.submit();return false;"><span class="btn-next">この内容で登録する</span></a>
      </li>
    </ul>
  </div>

</form>

<!--{include file="`$smarty.const.TEMPLATE_ADMIN_REALDIR`admin_popup_footer.tpl"}-->
