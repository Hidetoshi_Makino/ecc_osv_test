<!--{*
 * Copyright(c) 2013 Spirit of Co.,Ltd. All rights reserved.
 * http://sbps.ec-cube.pro/
 * Updated: 2013/08/01
 * sps_crediteveryapi.tpl
 *}-->
<script type="text/javascript">//<![CDATA[
var send = true;

function fnCheckSubmit(mode) {
    $('#payment_form_body').slideToggle();
    $('#payment_form_loading').slideToggle();

    if(send) {
        send = false;
        fnModeSubmit(mode,'','');
        return false;
    } else {
        alert("只今、処理中です。しばらくお待ち下さい。");
        return false;
    }
}

function fnAutoLoadSubmit() {

    var mode = 'next';
    send = false;
    setTimeout("fnModeSubmit('next','','')", 500);
}

function fnAutoLoadSubmitSPS() {
    setTimeout("document.form_sps.submit()", 500);
}

function next(now, next) {
    if (now.value.length >= now.getAttribute('maxlength')) {
    next.focus();
    }
}


$(function() {

    <!--{$tpl_payment_onload}-->
});
//]]>
</script>

        <div id="payment_form_loading" style="display:none;">
            <div class="information">
                <p>決済処理中です。そのまま、お待ち下さい。</p>
                <span class="attention">※画面が切り替るまで少々時間がかかる場合がございますが、そのままお待ちください。</span>
            </div>
            <table summary="">
                <tr>
                <td class="alignC">
                    <img src="<!--{$smarty.const.MDL_SPS_MEDIAFILE_URL}-->loading.gif" />
                </td>
                </tr>
            </table>
        </div>
        <div id="payment_form_body">
            <div class="information">
                <p>下記項目にご入力ください。「<span class="attention">※</span>」印は入力必須項目です。<br />
                入力後、一番下の「決済する」ボタンをクリックしてください。</p>

                <!--{if count($arrErr) > 0}-->
                <p>決済エラー<br />
                大変お手数ですがエラーの内容を確認して、再度お手続きを進めて下さい。</p>
                <!--{foreach from=$arrErr item=err}-->
                <p class="attention"><!--{$err}--></p>
                <!--{/foreach}-->
                <!--{/if}-->
            </div>
            <table summary="クレジットカード番号入力">
                <colgroup width="20%"></colgroup>
                <colgroup width="80%"></colgroup>
                <tr>
                    <th colspan="2" class="alignC"><!--{$tpl_title|h}-->番号入力</th>
                </tr>
                <tr>
                    <th class="alignR">
                        カード番号<span class="attention">※</span>
                    </th>
                    <td>
                  <!--{assign var=key1 value="card_no01"}-->
                  <!--{assign var=key2 value="card_no02"}-->
                  <!--{assign var=key3 value="card_no03"}-->
                  <!--{assign var=key4 value="card_no04"}-->
                  <span class="attention"><!--{$arrErr[$key1]}--></span>
                  <span class="attention"><!--{$arrErr[$key2]}--></span>
                  <span class="attention"><!--{$arrErr[$key3]}--></span>
                  <span class="attention"><!--{$arrErr[$key4]}--></span>
                  <input type="text" name="<!--{$key1}-->" value="<!--{$arrForm[$key1].value|escape}-->" maxlength="4" style="ime-mode: disabled; <!--{$arrErr[$key1]|sfGetErrorColor}-->"  size="6" class="box60" onkeyup="next(this, this.form.<!--{$key2}-->)" >&nbsp;-&nbsp;
                  <input type="text" name="<!--{$key2}-->" value="<!--{$arrForm[$key2].value|escape}-->" maxlength="4" style="ime-mode: disabled; <!--{$arrErr[$key2]|sfGetErrorColor}-->"  size="6" class="box60" onkeyup="next(this, this.form.<!--{$key3}-->)" >&nbsp;-&nbsp;
                  <input type="text" name="<!--{$key3}-->" value="<!--{$arrForm[$key3].value|escape}-->" maxlength="4" style="ime-mode: disabled; <!--{$arrErr[$key3]|sfGetErrorColor}-->"  size="6" class="box60" onkeyup="next(this, this.form.<!--{$key4}-->)" >&nbsp;-&nbsp;
                  <input type="text" name="<!--{$key4}-->" value="<!--{$arrForm[$key4].value|escape}-->" maxlength="4" style="ime-mode: disabled; <!--{$arrErr[$key4]|sfGetErrorColor}-->"  size="6" class="box60">
                  <br /><span class="attention">※ご本人名義のカードをご使用ください。</span><br /><p class="mini">半角入力（例：1234-5678-9012-3456）</p>

                    </td>
                </tr>
                <tr>
                    <th class="alignR">
                        カード有効期限<span class="attention">※</span>
                    </th>
                    <td>
                    <!--{assign var=key1 value="card_month"}-->
                    <!--{assign var=key2 value="card_year"}-->
                    <span class="attention"><!--{$arrErr[$key1]}--></span>
                    <span class="attention"><!--{$arrErr[$key2]}--></span>
                    <select name="<!--{$key1}-->" style="<!--{$arrErr[$key1]|sfGetErrorColor}-->">
                    <option value="">&minus;&minus;</option>
                    <!--{html_options options=$arrMonth selected=$arrForm[$key1].value}-->
                    </select>月
                    &nbsp;/&nbsp;
                    <select name="<!--{$key2}-->" style="<!--{$arrErr[$key2]|sfGetErrorColor}-->">
                    <option value="">&minus;&minus;</option>
                    <!--{html_options options=$arrYear selected=$arrForm[$key2].value}-->
                    </select>年
                    </td>
                </tr>
                <!--{if $arrPayment.is_security_code == '1'}-->
                <tr>
                    <th class="alignR">
                        セキュリティコード <span class="attention">※</span>
                    </th>
                    <td>
                        <!--{assign var=key value="security_code"}-->
                        <span class="attention"><!--{$arrErr[$key]}--></span>
                        <input type="text" name="<!--{$key}-->" value="<!--{$arrForm[$key].value|h}-->" maxlength="<!--{$arrForm[$key].length}-->" style="ime-mode: disabled; <!--{$arrErr[$key]|sfGetErrorColor}-->"  size="4" class="box60" />
                        <p class="mini"><span class="attention">※カード裏面の3桁または4桁の番号を記入して下さい。</span>半角入力 (例: 123)</p>
                    </td>
                </tr>
                <!--{/if}-->
            </table>

            <table>
                <tr>
                    <td>
                        以上の内容で間違いなければ、下記「次へ」ボタンをクリックしてください。<br />
                        <span class="attention">※画面が切り替るまで少々時間がかかる場合がございますが、そのままお待ちください。</span>
                    </td>
                </tr>
            </table>

            <div class="btn_area">
                <ul>
                    <li>
                        <input type="image" onclick="return fnCheckSubmit('return');" onmouseover="chgImg('<!--{$TPL_URLPATH}-->img/button/btn_back_on.jpg',this)" onmouseout="chgImg('<!--{$TPL_URLPATH}-->img/button/btn_back.jpg',this)" src="<!--{$TPL_URLPATH}-->img/button/btn_back.jpg" alt="戻る" border="0" name="back" id="back"/>
                    </li>

                    <li>
                        <input type="image" onclick="return fnCheckSubmit('next');" onmouseover="chgImg('<!--{$TPL_URLPATH}-->img/button/btn_next_on.jpg',this)" onmouseout="chgImg('<!--{$TPL_URLPATH}-->img/button/btn_next.jpg',this)" src="<!--{$TPL_URLPATH}-->img/button/btn_next.jpg" alt="次へ" border="0" name="next" id="next" />
                    </li>
                </ul>
            </div>

       </div><!--{* /payment_form_body *}-->



