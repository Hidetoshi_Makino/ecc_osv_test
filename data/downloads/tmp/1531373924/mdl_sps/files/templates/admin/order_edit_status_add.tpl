<!--{*
 * Copyright(c) 2013 Spirit of Co.,Ltd. All rights reserved.
 * http://sbps.ec-cube.pro/
 * Updated: 2013/08/01
 *}-->
<!--{if $plg_sps_payid}-->
<script type="text/javascript">
<!--
    function fnPlgSpsConfirm(mode, anchor, anchor_name) {
        if(window.confirm('決済操作を行います。\n受注データを編集していない場合は先に保存して下さい。\nよろしいですか？')) {
            fnModeSubmit(mode, anchor, anchor_name);
        }
    }

//-->
</script>

    <h2><!--{$smarty.const.MDL_SPS_SERVICE_NAME}-->決済情報</h2>
    <table class="form" id="plg_sps_form">
        <tr>
            <th>決済種別</th>
            <td><!--{$plg_sps_pay_name|h}-->(<!--{$plg_sps_payid}-->)</td>
        </tr>
        <!--{if $plg_sps_is_every}-->
        <tr>
            <th>簡易継続状態</th>
            <td>
            <!--{if $arrPaymentData.memo08 == $smarty.const.SPS_EVERY_OK}-->
            課金中
            <!--{elseif $arrPaymentData.memo08 == $smarty.const.SPS_EVERY_CR}-->
            課金失敗
            <!--{elseif $arrPaymentData.memo08 == $smarty.const.SPS_EVERY_CC}-->
            キャリア解約
            <!--{elseif $arrPaymentData.memo08 == $smarty.const.SPS_EVERY_CANEL}-->
            解約済み
            <!--{/if}-->
            </td>
        </tr>
        <!--{/if}-->
        <!--{if $plg_sps_error}-->
        <tr>
            <th>決済通信結果メッセージ</th>
            <td class="attention"><!--{$plg_sps_error|h}--></td>
        </tr>
        <!--{/if}-->
        <!--{if $arrPaymentData.res_tracking_id != ""}-->
        <tr>
            <th>決済トラッキングID</th>
            <td><!--{$arrPaymentData.res_tracking_id|h}--></td>
        </tr>
        <!--{/if}-->
        <!--{if $arrPaymentData.res_err_code != ""}-->
        <tr>
            <th>最終エラーコード</th>
            <td><!--{$arrPaymentData.res_err_code|h}--></td>
        </tr>
        <!--{/if}-->
        <!--{if $plg_sps_status|strlen > 0}-->
        <tr>
            <th>決済操作状態</th>
            <td>
            <!--{$arrSbpsStatus[$plg_sps_status]|default:"(未決済/未確認)"|h}-->
            </td>
        </tr>
        <!--{/if}-->
    <!--{if $arrPaymentData.amount != ""}-->
        <tr>
            <th>決済金額</th>
            <td>
            <!--{$arrPaymentData.amount|number_format|h}-->円
            <!--{if $arrPaymentData.amount != $arrForm.payment_total.value}-->
                &nbsp;<span class="attention">
                ※決済金額とお支払い合計に差異があります。
                </span>
            <!--{/if}-->
            </td>
        </tr>
    <!--{/if}-->

    <!--{if $plg_sps_payid == $smarty.const.SPS_CONVENI_API_PAYID}-->
        <tr>
            <th>コンビニ選択</th>
            <td>
                <!--{if $arrPaymentData.webcvstype != ""}-->
                <!--{$arrConveni[$arrPaymentData.webcvstype]|h}-->
                <input type="hidden" name="webcvstype" value="<!--{$arrPaymentData.webcvstype|h}-->" />
                <!--{else}-->
                <select name="webcvstype">
                <option value="">--選択して下さい--</option>
                <!--{html_options options=$arrConveni selected=$arrPaymentData.webcvstype}-->
                </select>
                <!--{/if}-->
            </td>
        </tr>
    <!--{/if}-->

    <!--{if $arrPaymentData.res_tracking_id == "" && $plg_sps_is_dopayment}-->
        <tr>
            <th>新規決済実行</th>
            <td>
                <!--{if $plg_sps_is_dopayment}-->
                <a class="btn-normal" href="javascript:;" onclick="fnPlgSpsConfirm('plg_sps_dopayment','','');">決済実行</a>&nbsp;
                <!--{/if}-->
            </td>
        </tr>
    <!--{else}-->
        <tr>
            <th>決済操作</th>
            <td>
            
            <!--{if $plg_sps_is_commit || $plg_sps_is_cancel || $plg_sps_is_reauth || $plg_sps_is_getstatus}-->
                <select name="sps_year" style="<!--{$arrErr.sps_year|sfGetErrorColor}-->">
                <option value="">----</option>
                <!--{html_options options=$arrYearShippingDate selected=$arrSpsForm.sps_year.value}-->
                </select>年
                <select name="sps_month" style="<!--{$arrErr.sps_month|sfGetErrorColor}-->">
                <option value="">--</option>
                <!--{html_options options=$arrMonth selected=$arrSpsForm.sps_month.value}-->
                </select>月
                <select name="sps_day" style="<!--{$arrErr.sps_day|sfGetErrorColor}-->">
                <option value="">--</option>
                <!--{html_options options=$arrDay selected=$arrSpsForm.sps_day.value}-->
                </select>日

                <br /><br />
                <!--{if $plg_sps_is_everystop}-->
                <a class="btn-normal" href="javascript:;" onclick="fnPlgSpsConfirm('plg_sps_everystop','','');">簡易継続解約</a>&nbsp;
                <!--{/if}-->
                <!--{if $plg_sps_is_commit}-->
                <a class="btn-normal" href="javascript:;" onclick="fnPlgSpsConfirm('plg_sps_commit','','');">売上確定実行</a>&nbsp;
                <!--{/if}-->
                <!--{if $plg_sps_is_cancel}-->
                <a class="btn-normal" href="javascript:;" onclick="fnPlgSpsConfirm('plg_sps_cancel','','');">取消(返品)実行</a>&nbsp;
                <!--{/if}-->
                <!--{if $plg_sps_is_reauth}-->
                <a class="btn-normal" href="javascript:;" onclick="fnPlgSpsConfirm('plg_sps_reauth','','');">再オーソリ実行</a>&nbsp;
                <!--{/if}-->
                <!--{if $plg_sps_is_getstatus}-->
                <a class="btn-normal" href="javascript:;" onclick="fnPlgSpsConfirm('plg_sps_get_status','','');">決済状態確認・反映</a>&nbsp;
                <!--{/if}-->
            <!--{else}-->
            この決済では決済操作はできません。
            <!--{/if}-->
            </td>
        </tr>
        <!--{/if}-->

    <!--{if $arrPaymentData.payment_log != "" && is_array($arrPaymentData.payment_log) && count($arrPaymentData.payment_log) > 0}-->
        <tr>
            <th>決済ログ</th>
            <td>
            <a href="javascript:void();" onclick="$('#plg_sps_log').slideToggle();">決済ログ表示・非表示</a>
            <br />
            <table id="plg_sps_log" style="display:none;" class="list">
                <tr>
                    <th>時間</th>
                    <th>内容</th>
                </tr>
            <!--{foreach from=$arrPaymentData.payment_log item=data key=key}-->
                <!--{foreach from=$data item=sdata key=skey}-->
                <tr>
                    <td>
                    <!--{$skey|h}-->
                    </td>
                    <td>
                    <!--{foreach from=$sdata item=val key=vkey}-->
                       <!--{if $val != ""}-->
                       <!--{$vkey|h}-->=
                         <!--{if is_array($val)}-->
                            <!--{$val|var_dump|h}-->
                         <!--{else}-->
                            <!--{$val|h}-->
                         <!--{/if}-->,
                       <!--{/if}-->
                    <!--{/foreach}-->
                    </td>
                </tr>
                <!--{/foreach}-->
            <!--{/foreach}-->
            </table>
            </td>
        </tr>
    <!--{/if}-->

    </table>


    <h2>受注詳細</h2>
<!--{/if}-->

