<!--{*
 * Copyright(c) 2015 Spirit of Co.,Ltd. All rights reserved.
 * http://sbps.ec-cube.pro/
 * Updated: 2015/09/10
 *}-->

<!--{if $plg_sps_payment_type_id == $smarty.const.SPS_CONVENI_API_PAYID}-->
            <tr>
                <th>新規受付時の受注状況<span class="attention"> </span></th>
                <td>
                    <!--{assign var=key value="exec_order_status"}-->
                    <span class="attention"><!--{$arrErr[$key]}--></span>
                    <select name="<!--{$key}-->" style="<!--{$arrErr[$key]|sfGetErrorColor}-->" id="<!--{$key}-->">
                    <option value="">--(フロント受注時自動)--</option>
                    <!--{html_options options=$arrORDERSTATUS selected=$arrForm[$key].value}-->
                    </select>
                    <span style="font-size:80%"><br />
                    ※フロント画面受注時に決済(番号発行)処理を行う場合は「受注時自動」選択して下さい。<br />
                    ※受注状況を選択した場合は、新規受付時は指定の受注状況に設定され、管理画面にて後から決済（番号発行）を行う必要があります。
                    </span>
                </td>
            </tr>

            <tr>
                <th>決済後の受注状況<span class="attention"> *</span></th>
                <td>
                    <!--{assign var=key value="auth_order_status"}-->
                    <span class="attention"><!--{$arrErr[$key]}--></span>
                    <select name="<!--{$key}-->" style="<!--{$arrErr[$key]|sfGetErrorColor}-->" id="<!--{$key}-->">
                    <option value="">--選択して下さい--</option>
                    <!--{html_options options=$arrORDERSTATUS selected=$arrForm[$key].value}-->
                    </select>
                    <span style="font-size:80%"><br />
                    ※決済処理が完了し、番号発行された時に注文データの受注状況を設定した状態に変更します。
                    </span>
                </td>
            </tr>

            <tr>
                <th>入金後の受注状況</th>
                <td>
                    <!--{assign var=key value="commit_order_status"}-->
                    <span class="attention"><!--{$arrErr[$key]}--></span>
                    <select name="<!--{$key}-->" style="<!--{$arrErr[$key]|sfGetErrorColor}-->" id="<!--{$key}-->">
                    <option value="0">変更しない</option>
                    <!--{html_options options=$arrORDERSTATUS selected=$arrForm[$key].value}-->
                    </select>
                    <span style="font-size:80%"><br />
                    ※入金通知受信時に注文データの受注状況を設定した状態に変更します。
                    </span>
                </td>
            </tr>

            <tr>
                <th>期限切れ後の受注状況</th>
                <td>
                    <!--{assign var=key value="cancel_order_status"}-->
                    <span class="attention"><!--{$arrErr[$key]}--></span>
                    <select name="<!--{$key}-->" style="<!--{$arrErr[$key]|sfGetErrorColor}-->" id="<!--{$key}-->">
                    <option value="0">変更しない</option>
                    <!--{html_options options=$arrORDERSTATUS selected=$arrForm[$key].value}-->
                    </select>
                    <span style="font-size:80%"><br />
                    ※期限切れ通知時に注文データの受注状況を設定した状態に変更します。
                    </span>
                </td>
            </tr>

            <tr>
                <th>取次契約先<span class="attention"> *</span></th>
                <td>
                    <!--{assign var=key value="agency_type"}-->
                    <span class="attention"><!--{$arrErr[$key]}--></span>
                    <!--{html_radios name=$arrForm[$key].keyname options=$arrAgency selected=$arrForm[$key].value separator="&nbsp;"}-->
                    <span style="font-size:80%"><br />
                    </span>
                </td>
            </tr>

            <tr>
                <th>利用可能なコンビニ<span class="attention"> *</span></th>
                <td>
                    <!--{assign var=key value="enable_webcvstype"}-->
                    <span class="attention"><!--{$arrErr[$key]}--></span>
                    <!--{html_checkboxes name="$key" options=$arrCvs selected=$arrForm[$key].value separator="&nbsp;"}-->
                    <span style="font-size:80%"><br />
                    ※契約に合わせて有効にするものを選択して下さい。
                    </span>
                </td>
            </tr>

            <tr>
                <th>支払い期限日数</th>
                <td>
                    <!--{assign var=key value="payment_limit_day"}-->
                    <span class="attention"><!--{$arrErr[$key]}--></span>
                    <input type="text" name="<!--{$arrForm[$key].keyname}-->" value="<!--{$arrForm[$key].value|h}-->" size="6" class="box6" maxlength="<!--{$arrForm[$key].length}-->" style="<!--{$arrErr[$key]|sfGetErrorColor}-->" />                    <span class="attention">（上限<!--{$arrForm[$key].length}-->文字）</span>
                </td>
            </tr>

            <!--{foreach from=$arrAgency item=agency_name key=agency_type}-->
            <!--{foreach from=$arrCvs item=cvs_name key=cvs_id}-->
            <tr class="cvs_agency_group_<!--{$agency_type|h}-->" style="display:none;">
                <th><!--{$cvs_name|h}-->支払い説明タイトル</th>
                <td>
                    <!--{assign var=key value="mail_title_`$agency_type`_`$cvs_id`"}-->
                    <span class="attention"><!--{$arrErr[$key]}--></span>
                    <input type="text" name="<!--{$arrForm[$key].keyname}-->" value="<!--{$arrForm[$key].value|h}-->" size="60" class="box200" maxlength="<!--{$arrForm[$key].length}-->" style="<!--{$arrErr[$key]|sfGetErrorColor}-->" />                    <span class="attention">（上限<!--{$arrForm[$key].length}-->文字）</span>
                </td>
            </tr>
            <tr class="cvs_agency_group_<!--{$agency_type|h}-->" style="display:none;">
                <th><!--{$cvs_name|h}-->支払い説明本文<br />(<!--{$agency_name|h}-->)</th>
                <td>
                    <!--{assign var=key value="mail_body_`$agency_type`_`$cvs_id`"}-->
                    <span class="attention"><!--{$arrErr[$key]}--></span>                                                                        <textarea name="<!--{$arrForm[$key].keyname}-->" maxlength="<!--{$arrForm[$key].length}-->" cols="60" rows="4" class="area60" style="<!--{$arrErr[$key]|sfGetErrorColor}-->"><!--{"\n"}--><!--{$arrForm[$key].value|h}--></textarea>
                    <span class="attention">（上限<!--{$arrForm[$key].length}-->文字）</span>
                    <span style="font-size:80%"><br />
                    ※有効にするにはタイトルと本文の両方を指定する必要があります。
                    </span>
                </td>
            </tr>
            <!--{/foreach}-->
            <!--{/foreach}-->


<script type="text/javascript">//<![CDATA[

    $(function(){
        lfSpsCvsAgencyChange();
        $('input[name="agency_type"]').change(function(event){lfSpsCvsAgencyChange();});
    });

function lfSpsCvsAgencyChange() {
    var agency_type = $('input[name="agency_type"]:checked').val();
    <!--{foreach from=$arrAgency item=agency_name key=id}-->
    $('.cvs_agency_group_<!--{$id}-->').hide();
    <!--{/foreach}-->
    if (agency_type != undefined) {
        $('.cvs_agency_group_' + agency_type).show();
    }
}
//]]></script>

<!--{/if}-->
