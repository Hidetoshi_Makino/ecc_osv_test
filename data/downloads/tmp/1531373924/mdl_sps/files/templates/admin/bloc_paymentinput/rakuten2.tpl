<!--{*
 * Copyright(c) 2015 Spirit of Co.,Ltd. All rights reserved.
 * http://sbps.ec-cube.pro/
 * Updated: 2015/07/20
 *}-->

<!--{if $plg_sps_payment_type_id == $smarty.const.SPS_RAKUTEN2_LINK_PAYID}-->
            <tr>
                <th>与信後の受注状況</th>
                <td>
                    <!--{assign var=key value="auth_order_status"}-->
                    <span class="attention"><!--{$arrErr[$key]}--></span>
                    <select name="<!--{$key}-->" style="<!--{$arrErr[$key]|sfGetErrorColor}-->" id="<!--{$key}-->">
                    <!--{html_options options=$arrORDERSTATUS selected=$arrForm[$key].value}-->
                    </select>
                </td>
            </tr>
            <tr>
                <th>処理待ちの受注状況</th>
                <td>
                    <!--{assign var=key value="wait_order_status"}-->
                    <span class="attention"><!--{$arrErr[$key]}--></span>
                    <select name="<!--{$key}-->" style="<!--{$arrErr[$key]|sfGetErrorColor}-->" id="<!--{$key}-->">
                    <option value="0">変更しない</option>
                    <!--{html_options options=$arrORDERSTATUS selected=$arrForm[$key].value}-->
                    </select>
                    <span style="font-size:80%"><br />
                    ※金額変更、売上確定処理時に結果通知が来るまで注文データの受注状況を設定した状態に変更します。
                    </span>
                </td>
            </tr>

            <tr>
                <th>売上確定後の受注状況</th>
                <td>
                    <!--{assign var=key value="commit_order_status"}-->
                    <span class="attention"><!--{$arrErr[$key]}--></span>
                    <select name="<!--{$key}-->" style="<!--{$arrErr[$key]|sfGetErrorColor}-->" id="<!--{$key}-->">
                    <option value="0">変更しない</option>
                    <!--{html_options options=$arrORDERSTATUS selected=$arrForm[$key].value}-->
                    </select>
                    <span style="font-size:80%"><br />
                    ※売上確定時に注文データの受注状況を設定した状態に変更します。
                    </span>
                </td>
            </tr>
            <tr>
                <th>金額変更確定後の受注状況</th>
                <td>
                    <!--{assign var=key value="reauth_order_status"}-->
                    <span class="attention"><!--{$arrErr[$key]}--></span>
                    <select name="<!--{$key}-->" style="<!--{$arrErr[$key]|sfGetErrorColor}-->" id="<!--{$key}-->">
                    <option value="0">変更しない</option>
                    <!--{html_options options=$arrORDERSTATUS selected=$arrForm[$key].value}-->
                    </select>
                    <span style="font-size:80%"><br />
                    ※金額変更の確定時に注文データの受注状況を設定した状態に変更します。
                    </span>
                </td>
            </tr>
            <tr>
                <th>取消後の受注状況</th>
                <td>
                    <!--{assign var=key value="cancel_order_status"}-->
                    <span class="attention"><!--{$arrErr[$key]}--></span>
                    <select name="<!--{$key}-->" style="<!--{$arrErr[$key]|sfGetErrorColor}-->" id="<!--{$key}-->">
                    <option value="0">変更しない</option>
                    <!--{html_options options=$arrORDERSTATUS selected=$arrForm[$key].value}-->
                    </select>
                    <span style="font-size:80%"><br />
                    ※与信取消時に注文データの受注状況を設定した状態に変更します。
                    </span>
                </td>
            </tr>

<!--{/if}-->
