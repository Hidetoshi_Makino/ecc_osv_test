<!--{*
/*
 * This file is part of EC-CUBE
 *
 * Copyright(c) 2000-2012 LOCKON CO.,LTD. All Rights Reserved.
 *
 * http://www.lockon.co.jp/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
*}-->

<form name="form1" id="form1" method="POST" action="?" >
<input type="hidden" name="<!--{$smarty.const.TRANSACTION_ID_NAME}-->" value="<!--{$transactionid}-->" />
<input type="hidden" name="mode" value="" />
<input type="hidden" name="status" value="<!--{if $arrForm.status == ""}-->1<!--{else}--><!--{$arrForm.status|h}--><!--{/if}-->" />
<input type="hidden" name="sbps_status" value="<!--{$arrForm.sbps_status|h}-->" />
<input type="hidden" name="payment_id" value="<!--{$arrForm.payment_id|h}-->" />
<input type="hidden" name="search_pageno" value="<!--{$tpl_pageno|h}-->" >
<input type="hidden" name="order_id" value="" />
<div id="order" class="contents-main">
    <h2>抽出条件： 対応状況 (選択中:<!--{$arrORDERSTATUS[$SelectedStatus]|h}-->)</h2>
        <div class="btn">
        <!--{foreach key=key item=item from=$arrORDERSTATUS}-->
            <a
                class="btn-normal"
                style="padding-right: 1em;"
                    href="javascript:;"
                    onclick="document.form1.search_pageno.value='1'; fnModeSubmit('search','status','<!--{$key}-->' ); return false;"
            ><!--{$item}--></a>
        <!--{/foreach}-->
        </div>
    <h2>抽出条件： 支払い方法
    <!--{if $SelectedPayment}--> (選択中:<!--{$arrPayment[$SelectedPayment]|h}-->)<!--{/if}--></h2>
        <div class="btn">
        <!--{foreach key=key item=item from=$arrPayment}-->
            <a
                class="btn-normal"
                style="padding-right: 1em;"
                    href="javascript:;"
                    onclick="document.form1.search_pageno.value='1'; fnModeSubmit('search','payment_id','<!--{$key}-->' ); return false;"
            ><!--{$item}--></a>
        <!--{/foreach}-->
        </div>
    <h2>抽出条件： SBPS決済状態
    <!--{if $arrSbpsStatus[$SelectedSbpsStatus] != ""}--> (選択中:<!--{$arrSbpsStatus[$SelectedSbpsStatus]|h}-->)<!--{/if}--></h2>
        <div class="btn">
        <!--{foreach key=key item=item from=$arrSbpsStatus}-->
            <a
                class="btn-normal"
                style="padding-right: 1em;"
                    href="javascript:;"
                    onclick="document.form1.search_pageno.value='1'; fnModeSubmit('search','sbps_status','<!--{$key}-->' ); return false;"
            ><!--{$item}--></a>
        <!--{/foreach}-->
        </div>

    <h2>決済状況変更</h2>
    <span class="attention"><!--{$tpl_exec_result|h|nl2br}--></span><br />
    <!--{* 登録テーブルここから *}-->
    <!--{if $tpl_linemax > 0}-->
        <div class="btn">
            <select name="change_status">
                <option value="" selected="selected" style="<!--{$Errormes|sfGetErrorColor}-->" >選択してください</option>
                <option value="commit">売上日指定要求</option>
                <option value="cancel">返金要求</option>
            </select>
          <span class="attention"><!--{$arrErr.sps_year}--><!--{$arrErr.sps_month}--><!--{$arrErr.sps_date}--></span>
          <select name="sps_year" style="<!--{$arrErr.sps_year|sfGetErrorColor}-->">
            <option value="">----</option>
            <!--{html_options options=$arrYear selected=$arrForm.sps_year}-->
          </select>年
          <select name="sps_month" style="<!--{$arrErr.sps_month|sfGetErrorColor}-->">
            <option value="">--</option>
            <!--{html_options options=$arrMonth selected=$arrForm.sps_month}-->
          </select>月
          <select name="sps_day" style="<!--{$arrErr.sps_month|sfGetErrorColor}-->">
            <option value="">--</option>
            <!--{html_options options=$arrDay selected=$arrForm.sps_day}-->
          </select>日
            <a class="btn-normal" href="javascript:;" onclick="fnSelectCheckSubmit(); return false;"><span>実行</span></a>
        </div>
        <span class="attention">※ 実行してから時間が掛かる場合がありますが画面を閉じないで下さい。</span><br />

        <p class="remark">
            <!--{$tpl_linemax}-->件が該当しました。
            <!--{$tpl_strnavi}-->
        </p>

        <table class="list center">
            <col width="5%" />
            <col width="9%" />
            <col width="9%" />
            <col width="5%" />
            <col width="5%" />
            <col width="7%" />
            <col width="10%" />
            <col width="20%" />
            <col width="10%" />
            <col width="10%" />
            <col width="12%" />
            <tr>
                <th><label for="move_check">選択</label> <input type="checkbox" name="move_check" id="move_check" onclick="fnAllCheck(this, 'input[name=move[]]')" /></th>
                <th>対応状況</th>
                <th>決済状況</th>
                <th>売上</th>
                <th>取消</th>
                <th>注文番号</th>
                <th>受注日</th>
                <th>お名前</th>
                <th>支払方法</th>
                <th>購入金額（円）</th>
                <th>発送日</th>
            </tr>
            <!--{section name=cnt loop=$arrStatus}-->
            <!--{assign var=status value="`$arrStatus[cnt].status`"}-->
            <tr style="background:<!--{$arrORDERSTATUS_COLOR[$status]}-->;">
                <td><input type="checkbox" name="move[]" value="<!--{$arrStatus[cnt].order_id}-->" ></td>
                <td><!--{$arrORDERSTATUS[$status]}--></td>
                <td>
                <!--{assign var=sbps_order_status value="`$arrStatus[cnt].memo05`"}-->
                <!--{if $arrSbpsStatus[$sbps_order_status] != ""}-->
                    <!--{$arrSbpsStatus[$sbps_order_status]|h}-->
                <!--{else}-->
                    決済後
                <!--{/if}-->
                </td>
                <td>
                <!--{if $arrStatus[cnt].is_commit}-->○<!--{else}-->×<!--{/if}-->
                </td>
                <td>
                <!--{if $arrStatus[cnt].is_cancel}-->○<!--{else}-->×<!--{/if}-->
                </td>
                <td>
                <a href="?" onclick="fnChangeAction('<!--{$smarty.const.ADMIN_ORDER_EDIT_URLPATH}-->'); fnModeSubmit('pre_edit', 'order_id', '<!--{$arrStatus[cnt].order_id}-->'); return false;"><!--{$arrStatus[cnt].order_id}--></a></td>
                <td><!--{$arrStatus[cnt].create_date|sfDispDBDate:false}--></td>
                <td><!--{$arrStatus[cnt].order_name01|h}--><!--{$arrStatus[cnt].order_name02|h}--></td>
                <!--{assign var=payment_id value=`$arrStatus[cnt].payment_id`}-->
                <td><!--{$arrPayment[$payment_id]|h}--></td>
                <td class="right"><!--{$arrStatus[cnt].total|number_format}--></td>
                <td><!--{if $arrStatus[cnt].status eq 5}--><!--{$arrStatus[cnt].commit_date|sfDispDBDate:false}--><!--{else}-->未発送<!--{/if}--></td>
            </tr>
            <!--{/section}-->
        </table>
        <input type="hidden" name="move[]" value="" />

        <p><!--{$tpl_strnavi}--></p>

    <!--{elseif $arrStatus != "" & $tpl_linemax == 0}-->
        <div class="message">
            該当するデータはありません。
        </div>
    <!--{/if}-->

    <!--{* 登録テーブルここまで *}-->
</div>
</form>


<script type="text/javascript">
<!--
function fnSelectCheckSubmit(){

    var selectflag = 0;
    var fm = document.form1;

    if(fm.change_status.options[document.form1.change_status.selectedIndex].value == ""){
    selectflag = 1;
    }

    if(selectflag == 1){
        alert('セレクトボックスが選択されていません');
        return false;
    }
    var i;
    var checkflag = 0;
    var max = fm["move[]"].length;

    if(max) {
        for (i=0;i<max;i++){
            if(fm["move[]"][i].checked == true){
                checkflag = 1;
            }
        }
    } else {
        if(fm["move[]"].checked == true) {
            checkflag = 1;
        }
    }

    if(checkflag == 0){
        alert('チェックボックスが選択されていません');
        return false;
    }

    if(selectflag == 0 && checkflag == 1){
    document.form1.mode.value = 'update';
    document.form1.submit();
    }
}
//-->
</script>
