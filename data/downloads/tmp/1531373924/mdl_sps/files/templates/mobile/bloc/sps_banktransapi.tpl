<!--{*
 * Copyright(c) 2013 Spirit of Co.,Ltd. All rights reserved.
 * http://sbps.ec-cube.pro/
 * Updated: 2013/08/01
 * sps_banktransapi.tpl
 *}-->

        <!--{if count($arrErr) > 0}-->
    <b>決済エラー</b>
    大変お手数ですがエラーの内容を確認して、再度お手続きを進めて下さい。<br>
    <!--{foreach from=$arrErr item=err}-->
    <font color="#ff0000"><!--{$err|h}--></font><br>
    <!--{/foreach}-->
    <br>

        <!--{else}-->
    <b>決済処理を進めます</b>
    <hr>
    決済処理を進めるため「次へ」ボタンを押して下さい。<br/>
    <font color="#ff0000">※画面が切り替るまで少々時間がかかる場合がございますが、そのままお待ちください。</font>
    <br>
    <br>
    <center><input type="submit" value="次へ"></center>
       <!--{/if}-->

    <hr>
    </form>
    <form action="?mode=return" method="get">
        <input type="hidden" name="mode" value="return">
        <center><input type="submit" value="戻る"></center>
    </form>
    <form name="dummy">
