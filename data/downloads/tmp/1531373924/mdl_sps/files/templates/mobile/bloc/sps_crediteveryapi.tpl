<!--{*
 * Copyright(c) 2013 Spirit of Co.,Ltd. All rights reserved.
 * http://sbps.ec-cube.pro/
 * Updated: 2013/08/01
 * sps_crediteveryapi.tpl
 *}-->
<!--{strip}-->
<b>下記項目にご入力ください。</b><br>
入力後、一番下の「決済する」ボタンをクリックしてください。<br>
<hr>
<!--{if count($arrErr) > 0}-->
    <b>決済エラー</b>
    大変お手数ですがエラーの内容を確認して、再度お手続きを進めて下さい。<br>
    <br>
<!--{/if}-->
 カード番号<br>
  <!--{assign var=key1 value="card_no01"}-->
  <!--{assign var=key2 value="card_no02"}-->
  <!--{assign var=key3 value="card_no03"}-->
  <!--{assign var=key4 value="card_no04"}-->
  <font color="#ff0000"><!--{$arrErr[$key1]}--></font>
  <font color="#ff0000"><!--{$arrErr[$key2]}--></font>
  <font color="#ff0000"><!--{$arrErr[$key3]}--></font>
  <font color="#ff0000"><!--{$arrErr[$key4]}--></font>
  <input type="text" name="<!--{$key1}-->" value="<!--{$arrForm[$key1].value|escape}-->" maxlength="4" istyle="3"  style="<!--{$arrErr[$key1]|sfGetErrorColor}-->"  size="6">-<input type="text" name="<!--{$key2}-->" value="<!--{$arrForm[$key2].value|escape}-->" maxlength="4" istyle="3"  style="<!--{$arrErr[$key2]|sfGetErrorColor}-->"  size="6">-<input type="text" name="<!--{$key3}-->" value="<!--{$arrForm[$key3].value|escape}-->" maxlength="4" istyle="3"  style="<!--{$arrErr[$key3]|sfGetErrorColor}-->"  size="6">-<input type="text" name="<!--{$key4}-->" value="<!--{$arrForm[$key4].value|escape}-->" maxlength="4" istyle="3"  style="<!--{$arrErr[$key4]|sfGetErrorColor}-->"  size="6">
  <br>
  <font color="#ff0000">※ご本人名義のカードをご使用ください。</font>
  <br>
  <br>
  カード有効期限<br>
    <!--{assign var=key1 value="card_month"}-->
    <!--{assign var=key2 value="card_year"}-->
    <font color="#ff0000"><!--{$arrErr[$key1]}--></font>
    <font color="#ff0000"><!--{$arrErr[$key2]}--></font>
    <select name="<!--{$key1}-->" style="<!--{$arrErr[$key1]|sfGetErrorColor}-->">
    <option value="">&minus;&minus;</option>
    <!--{html_options options=$arrMonth selected=$arrForm[$key1].value}-->
    </select>月
    /
    <select name="<!--{$key2}-->" style="<!--{$arrErr[$key2]|sfGetErrorColor}-->">
    <option value="">&minus;&minus;</option>
    <!--{html_options options=$arrYear selected=$arrForm[$key2].value}-->
    </select>年
    <br>
    <br>
  <!--{if $arrPayment.is_security_code == '1'}-->
  セキュリティコード<br>
  <!--{assign var=key value="security_code"}-->
  <font color="#ff0000"><!--{$arrErr[$key]}--></font>
  <input type="text" name="<!--{$key}-->" value="<!--{$arrForm[$key].value|h}-->" maxlength="<!--{$arrForm[$key].length}-->" istyle="3" style="<!--{$arrErr[$key]|sfGetErrorColor}-->"  size="4" />
  <br>
  <font color="#ff0000">※カード裏面の3桁または4桁の番号を記入して下さい。</font>
  <br>
  <br>
  <!--{/if}-->

<p>以上の内容で間違いなければ、下記「次へ」ボタンをクリックしてください。<br>
<font color="#ff0000">※画面が切り替るまで少々時間がかかる場合がございますが、そのままお待ちください。</font>
</p>
        <center><input type="submit" value="次へ"></center>

    </form>

    <form action="?mode=return" method="get">
        <input type="hidden" name="mode" value="return">
        <center><input type="submit" value="戻る"></center>
    </form>
<!--{/strip}-->
