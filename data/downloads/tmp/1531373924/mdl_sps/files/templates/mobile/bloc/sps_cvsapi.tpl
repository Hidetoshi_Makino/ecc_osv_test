<!--{*
 * Copyright(c) 2013 Spirit of Co.,Ltd. All rights reserved.
 * http://sbps.ec-cube.pro/
 * Updated: 2013/08/01
 * sps_cvsapi.tpl
 *}-->
<!--{strip}-->
<b>下記項目をご選択ください。</b><br>
入力後、一番下の「次へ」ボタンをクリックしてください。<br>
<font color="#ff0000">※画面が切り替るまで少々時間がかかる場合がございますが、そのままお待ちください。</font>
<hr>
<!--{if count($arrErr) > 0}-->
    <b>決済エラー</b>
    大変お手数ですがエラーの内容を確認して、再度お手続きを進めて下さい。<br>
    <br>
<!--{/if}-->
コンビニ選択<br>

<!--{foreach from=$arrCvs item=cvsname key=cvstype}-->
<input type="radio" name="webcvstype" value="<!--{$cvstype|h}-->" <!--{if $arrForm[$key].value == $cvstype}-->checked="checked"<!--{/if}--> />
<!--{$cvsname|h}-->
<br>
<!--{/foreach}-->
<br>


<p>以上の内容で間違いなければ、下記「次へ」ボタンをクリックしてください。<br>
<font color="#ff0000">※画面が切り替るまで少々時間がかかる場合がございますが、そのままお待ちください。</font>
</p>
        <center><input type="submit" value="次へ"></center>

    </form>

    <form action="?mode=return" method="get">
        <input type="hidden" name="mode" value="return">
        <center><input type="submit" value="戻る"></center>
    </form>
<!--{/strip}-->
