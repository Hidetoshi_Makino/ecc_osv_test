EC-CUBE ソフトバンク・ペイメント・サービスモジュール

Version 4.3.1

======================
EC-CUBE 対応バージョン
======================
  2.13.5
  2.13.4
  2.13.3
  2.13.2
  2.13.1
  2.13.0

  ※ 本モジュールは、上記バージョンのEC-CUBEにのみ対応しております。

  ※ 2.13.0未満のEC-CUBEではご利用できません。そちらに対応した決済
     モジュールをご利用下さい。

========
変更履歴
========
2015/12/28
 - Version 4.3.1
 新規
 ・PayEasy API決済において取次契約先選択・動作切替機能を追加。
 ・EC-CUBE 2.13.5に対応
 変更
 ・PayEasy API決済の入金通知（確報）の動作を変更。

2015/09/10
 - Version 4.3.0
 新規
 ・コンビニ決済APIにおいて、受注後金額確定に対応。

2015/08/26
 - Version 4.2.3
 修正
 ・楽天ID決済で受注時メールの自動送信が働かない場合がある問題の改修。

2015/08/10
 - Version 4.2.2
 修正
 ・一部環境で楽天ID決済時の結果がエラーとなる問題の改修。
 変更
 ・総合振込における挙動の見直し。

2015/07/31
 - Version 4.2.1
 変更
 ・コンビニ支払い、総合振込の仕様変更に合わせた修正。

2015/07/23
 - Version 4.2.0
 新規
 ・楽天ID決済（非ギャザ版）に対応。
 変更
 ・楽天ID決済への名称変更に伴う文言修正
 ・その他一部処理の見直し。

2015/05/01
 - Version 4.1.1
 修正:
 ・一部の環境下で利用時に、フューチャーフォンの一部機種において、
　一部の決済が正しく決済出来ない場合がある問題を修正。


2015/03/17
 - Version 4.1.0
 新規:
  クレジット分割時のルール追加
 修正:
  ・決済において正しく遷移出来ず決済が出来ない場合がある問題を修正。
  一部処理の見直し。
  ログの出力仕様の変更。

2015/02/14
 - Version 4.0.9
 新規:
  EC-CUBE 2.13.3に対応
 修正:
  一部の処理を見直して実装を修正。

2014/08/21
 - Version 4.0.8
 修正:
  一部環境で暗号化の復号処理が正常に働かない場合がある問題の修正。

2014/08/11
 - Version 4.0.7
 修正:
  一部環境でAPI型コンビニ決済とAPI型PayEasy決済の通知が受け取れない問題の修正。
 変更:
  一部ログ処理の見直し。

2014/07/25
 - Versoin 4.0.6
 新規:
  EC-CUBE 2.13.2 に対応
 変更:
  楽天あんしん決済のAPI機能について修正

2014/06/01
 - Version 4.0.5
 修正:
   2クリック決済が正常に動作しない場合がある問題を改修しました。

2014/02/14
 - Version 4.0.4
 修正:
   カード情報編集機能が正常に動作しない問題を改修しました。

2013/01/30
 - Version 4.0.3
 修正:
   一部環境で決済が正常に動作しない問題を改修しました。

2013/11/18
 - Version 4.0.2
 新規：
   EC-CUBE 2.13.1に対応。
 修正：
   API決済時にエラーが生じた後、再度決済要求をする際にデータの状況によって、再要求がエラーになる場合がある
　 問題の修正。

2013/10/01
 - Version 4.0.1
  EC-CUBE 2.13.0用にリリース


