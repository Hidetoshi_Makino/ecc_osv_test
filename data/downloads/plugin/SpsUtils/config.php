<?php
/**
 *
 * @copyright 2013 Spirit of Co.,Ltd. All Rights Reserved.
 * @link http://sbps.ec-cube.pro/
 *
 */

// {{{ requires
require_once PLUGIN_UPLOAD_REALDIR .  'SpsUtils/LC_Page_Plugin_SpsUtils_Config.php';

// }}}
// {{{ generate page
$objPage = new LC_Page_Plugin_SpsUtils_Config();
$objPage->init();
$objPage->process();

