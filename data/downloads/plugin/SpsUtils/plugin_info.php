<?php
/**
 * プラグイン の情報クラス.
 *
 * @package SpsUtils
 * @author Spirit of Co.,Ltd.
 */
class plugin_info{
    /** プラグインコード(必須)：プラグインを識別する為キーで、他のプラグインと重複しない一意な値である必要がありま. */
    static $PLUGIN_CODE       = "SpsUtils";
    /** プラグイン名(必須)：EC-CUBE上で表示されるプラグイン名. */
    static $PLUGIN_NAME       = "ソフトバンク・ペイメント・サービス 決済補助機能プラグイン";
    /** クラス名(必須)：プラグインのクラス（拡張子は含まない） */
    static $CLASS_NAME        = "SpsUtils";
    /** プラグインバージョン(必須)：プラグインのバージョン. */
    static $PLUGIN_VERSION    = "1.0";
    /** 対応バージョン(必須)：対応するEC-CUBEバージョン. */
    static $COMPLIANT_VERSION = "2.13.0";
    /** 作者(必須)：プラグイン作者. */
    static $AUTHOR            = "ソフトバンク・ペイメント・サービス EC-CUBEサポート事務局";
    /** 説明(必須)：プラグインの説明. */
    static $DESCRIPTION       = "ソフトバンク・ペイメント・サービス用の決済に関する補助機能を提供するプラグインです。";
    /** プラグインURL：プラグイン毎に設定出来るURL（説明ページなど） */
    static $PLUGIN_SITE_URL   = "http://sbps.ec-cube.pro/";
    /** プラグイン作者URL：プラグイン毎に設定出来るURL（説明ページなど） */
    static $AUTHOR_SITE_URL   = "http://sbps.ec-cube.pro/";
    /** フックポイント：フックポイントとコールバック関数を定義します */
    static $HOOK_POINTS       = array(
        array("LC_Page_Admin_Order_Status_action_before", 'hookActionBefore'),
        array("LC_Page_Admin_Order_Edit_action_before", 'hookActionBefore'),
        array("LC_Page_Admin_Order_Edit_action_after", 'hookActionAfter'),
        array("LC_Page_Admin_Order_action_before", 'hookActionBefore'),
        array("LC_Page_Admin_Order_action_after", 'hookActionAfter'),
        array("LC_Page_Admin_Customer_action_after", 'hookActionAfter'),
        array("prefilterTransform", 'prefilterTransform'));
}
