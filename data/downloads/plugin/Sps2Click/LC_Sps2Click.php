<?php
/*
 * Copyright(c) SOFTBANK PAYMENT SERVICE Corp. All rights reserved.
 * http://www.sbpayment.jp/
 */

/**
 * プラグインの処理クラス
 */
class LC_Sps2Click {
    function LC_Sps2Click () {
        $this->tpl_path = PLUGIN_UPLOAD_REALDIR . 'Sps2Click/templates/';
    }

    function actionPrefilterTransform($class_name, &$source, &$objPage, $filename, $objPlugin) {
        $plugin = SC_Plugin_Util_Ex::getPluginByPluginCode("Sps2Click");

        if(strpos($filename, 'cart/index.tpl') !== false) {
                $template_file = 'cart_index_2click_add.tpl';
                $objTransform = new SC_Helper_Transform($source);
                switch($objPage->arrPageLayout['device_type_id']){
                    case DEVICE_TYPE_MOBILE:
                        $template_dir = $this->tpl_path . 'mobile/';
                        $objTransform->select('form')->insertAfter(file_get_contents($template_dir . $template_file));
                        break;
                    case DEVICE_TYPE_SMARTPHONE:
                        $template_dir = $this->tpl_path . 'sphone/';
                        $objTransform->select('div.btn_area_btm')->appendChild(file_get_contents($template_dir . $template_file));
                        break;
                    case DEVICE_TYPE_PC:
                        $template_dir = $this->tpl_path . 'default/';
                        $objTransform->select('div.btn_area > ul')->appendChild(file_get_contents($template_dir . $template_file));
                        break;
                    default:
                        break;
                }

                $source = $objTransform->getHTML();
        } else if(strpos($filename, 'products/detail.tpl') !== false) {
                $template_file = 'products_detail_2click_add.tpl';
                $objTransform = new SC_Helper_Transform($source);
                switch ($objPage->arrPageLayout['device_type_id']){
                    case DEVICE_TYPE_MOBILE:
                        break;
                    case DEVICE_TYPE_SMARTPHONE:
                        $template_dir = $this->tpl_path . 'sphone/';
                        $objTransform->select('div#cartbtn_default')->insertAfter(file_get_contents($template_dir . $template_file));
                        break;
                    case DEVICE_TYPE_PC:
                        $template_dir = $this->tpl_path . 'default/';
                        $objTransform->select('div.cartin')->insertAfter(file_get_contents($template_dir . $template_file));
                        break;
                    default:
                        break;
                }

                $source = $objTransform->getHTML();
        } else if(strpos($filename, 'products/select_item.tpl') !== false) {
                $template_file = 'products_detail_2click_add.tpl';
                $objTransform = new SC_Helper_Transform($source);
                switch ($objPage->arrPageLayout['device_type_id']){
                    case DEVICE_TYPE_MOBILE:
                        $template_dir = $this->tpl_path . 'mobile/';
                        $objTransform->select('form')->insertAfter(file_get_contents($template_dir . $template_file));
                        break;
                    case DEVICE_TYPE_SMARTPHONE:
                        break;
                    case DEVICE_TYPE_PC:
                        break;
                    default:
                        break;
                }

                $source = $objTransform->getHTML();
        } else if(strpos($filename, 'products/list.tpl') !== false) {
                $template_file = 'products_list_2click_add.tpl';
                $objTransform = new SC_Helper_Transform($source);
                switch ($objPage->arrPageLayout['device_type_id']){
                    case DEVICE_TYPE_MOBILE:
                        break;
                    case DEVICE_TYPE_SMARTPHONE:
                        break;
                    case DEVICE_TYPE_PC:
                        $template_dir = $this->tpl_path . 'default/';
                        $objTransform->select('div.cartin_btn')->insertAfter(file_get_contents($template_dir . $template_file));
                        break;
                    default:
                        break;
                }

                $source = $objTransform->getHTML();
        }
    }

    function actionHook($class_name, $hook_point, &$objPage, $objPlugin) {
        switch ($class_name) {
        case 'LC_Page_Products_Detail_Ex':
            if ($objPage->getMode() == 'plg_sps2click') {
                $_POST['mode'] = 'cart';
                $_REQUEST['mode'] = 'cart';
                $objSess = new SC_Session_Ex();
                $objSess->SetSession('plg_sps2click_payment_item', true);
            } else {
                $objSess = new SC_Session_Ex();
                $objSess->SetSession('plg_sps2click_payment_item', false);
            }
            break;
        case 'LC_Page_Products_List_Ex':
            if ($objPage->getMode() == 'plg_sps2click') {
                $_POST['mode'] = 'cart';
                $_REQUEST['mode'] = 'cart';
                $objSess = new SC_Session_Ex();
                $objSess->SetSession('plg_sps2click_payment_item', true);
            } else {
                $objSess = new SC_Session_Ex();
                $objSess->SetSession('plg_sps2click_payment_item', false);
            }
            break;
        case 'LC_Page_Cart_Ex':
            if ($objPage->getMode() == 'up'
                    || $objPage->getMode() == 'down'
                    || $objPage->getMode() == 'setQuantity'
                    || $objPage->getMode() == 'delete') {
                $objSess = new SC_Session_Ex();
                $objSess->SetSession('plg_sps2click_payment', false);
            } else if ($objPage->getMode() == 'plg_sps2click') {
                $_POST['mode'] = 'confirm';
                $_REQUEST['mode'] = 'confirm';
                $objSess = new SC_Session_Ex();
                $objSess->SetSession('plg_sps2click_payment', true);
            } else {
                $objSess = new SC_Session_Ex();
                // 商品からの遷移の場合
                if ($objSess->GetSession('plg_sps2click_payment_item')) {
                    $objSess->SetSession('plg_sps2click_payment_item', false);

                    $objCartSess = new SC_CartSession_Ex();
                    $cartKeys = $objCartSess->getKeys();
                    if (count($cartKeys) == 1) {
                        $_POST['mode'] = 'confirm';
                        $_REQUEST['mode'] = 'confirm';
                        $_POST['cartKey'] = $cartKeys[0];
                        $_REQUEST['cartKey'] = $cartKeys[0];
                        $objSess->SetSession('plg_sps2click_payment', true);
                    } else {
                        $objSess->SetSession('plg_sps2click_payment', false);
                    }
                } else {
                    $objSess->SetSession('plg_sps2click_payment', false);
                }
            }
            break;
        case 'LC_Page_Shopping_Ex':
            if ($objPage->getMode() == 'nonmember_confirm'
                    || $objPage->getMode() == 'return'
                    || $objPage->getMode() == 'multiple') {

                $objSess = new SC_Session_Ex();
                if($objSess->GetSession('plg_sps2click_payment')) {
                    $objSess->SetSession('plg_sps2click_payment', false);
                }
            }
            break;
        case 'LC_Page_Shopping_Deliv_Ex':
            if ($objPage->getMode() == 'delete'
                    || $objPage->getMode() == 'return'
                    || $objPage->getMode() == 'multiple') {

                $objSess = new SC_Session_Ex();
                if($objSess->GetSession('plg_sps2click_payment')) {
                    $objSess->SetSession('plg_sps2click_payment', false);
                }
            } else {
                $objSess = new SC_Session_Ex();
                if($objSess->GetSession('plg_sps2click_payment')) {
                    $_POST['mode'] = 'customer_addr';
                    $_REQUEST['mode'] = 'customer_addr';
                    $_POST['deliv_check'] = '-1';
                }
            }
            break;
        case 'LC_Page_Shopping_Payment_Ex':
            if ($objPage->getMode() == 'select_deliv'
                    || $objPage->getMode() == 'return') {

                $objSess = new SC_Session_Ex();
                if($objSess->GetSession('plg_sps2click_payment')) {
                    $objSess->SetSession('plg_sps2click_payment', false);
                }
            } else {
                $objSess = new SC_Session_Ex();
                if($objSess->GetSession('plg_sps2click_payment')) {

                    $objSiteSess = new SC_SiteSession_Ex();
                    $objCartSess = new SC_CartSession_Ex();
                    $objPurchase = new SC_Helper_Purchase_Ex();
                    $objCustomer = new SC_Customer_Ex();
                    $objDelivery = new SC_Helper_Delivery_Ex();

                    $is_multiple = $objPurchase->isMultiple();
                    $arrShipping = $objPurchase->getShippingTemp($is_multiple);
                    $tpl_uniqid = $objSiteSess->getUniqId();
                    $cart_key = $objCartSess->getKey();

                    $arrDeliv = $objDelivery->getList($cart_key);
                    $is_single_deliv = $objPage->isSingleDeliv($arrDeliv);

                    $arrOrderTemp = $objPurchase->getOrderTemp($tpl_uniqid);

                    $arrPrices = $objCartSess->calculate($cart_key, $objCustomer);

                    $deliv_id = $arrDeliv[0]['deliv_id'];
                    $arrSelectedDeliv = $objPage->getSelectedDeliv($objCartSess, $deliv_id);
                    $arrPayment = $arrSelectedDeliv['arrPayment'];

                    if(SC_Utils_Ex::isBlank($arrPayment)) {
                        $objSess = new SC_Session_Ex();
                        if($objSess->GetSession('plg_sps2click_payment')) {
                            $objSess->SetSession('plg_sps2click_payment', false);
                        }
                        return;
                    }

                    $target_payment_id = '';
                    if ($objCustomer->isLoginSuccess(true)) {
                        $objQuery =& SC_Query_Ex::getSingletonInstance();
                        $objQuery->setOrder('create_date desc');
                        $objQuery->setLimit('1');
                        $arrOldOrder = $objQuery->getRow('*', 'dtb_order', 'customer_id = ? and del_flg = 0 and status <> ?', array($objCustomer->getValue('customer_id'), ORDER_PENDING));
                        $target_payment_id = $arrOldOrder['payment_id'];
                    }
                    if ($target_payment_id == '') {
                        foreach ($arrPayment as $arrPaymentData) {
                            $arrPaymentInfo = SC_Util_SPS_Ex::getPaymentInfo($arrPaymentData['payment_id']);
                            if ($arrPaymentInfo[MDL_SPS_PAYMENT_COL_PAYID] == SPS_CREDIT_LINK_PAYID
                                    || $arrPaymentInfo[MDL_SPS_PAYMENT_COL_PAYID] == SPS_CREDIT3D_LINK_PAYID
                                    || $arrPaymentInfo[MDL_SPS_PAYMENT_COL_PAYID] == SPS_CREDIT_REGIST_LINK_PAYID
                                    || $arrPaymentInfo[MDL_SPS_PAYMENT_COL_PAYID] == SPS_CREDIT3D_REGIST_LINK_PAYID
                                    || $arrPaymentInfo[MDL_SPS_PAYMENT_COL_PAYID] == SPS_CREDIT_API_PAYID
                                    || $arrPaymentInfo[MDL_SPS_PAYMENT_COL_PAYID] == SPS_CREDIT_REGIST_API_PAYID
                                    || $arrPaymentInfo[MDL_SPS_PAYMENT_COL_PAYID] == SPS_CREDIT3D_FCSV_PAYID) {
                                $target_payment_id = $arrPaymentData['payment_id'];
                                break;
                            }
                        }
                    }

                    if (!SC_Utils_Ex::isBlank($target_payment_id)) {
                        $_POST['mode'] = 'confirm';
                        $_REQUEST['mode'] = 'confirm';
                        $_POST['deliv_id'] = $deliv_id;
                        $_POST['payment_id'] = $target_payment_id;
                    }
                }
            }
            break;
        case 'LC_Page_Shopping_Confirm_Ex':
            if ($hook_point == 'before') {
                if ($objPage->getMode() == 'return') {
                    $objSess = new SC_Session_Ex();
                    if($objSess->GetSession('plg_sps2click_payment')) {
                        $objSess->SetSession('plg_sps2click_payment', false);
                    }
                } else {
                    $objSess = new SC_Session_Ex();
                    if($objSess->GetSession('plg_sps2click_payment')) {
                        $objSess->SetSession('plg_sps2click_payment', false);
                        $objSess->SetSession('plg_sps2click_payment_confirm', true);
                    }
                }
            } else if ($hook_point == 'after') {
            }
            break;
        }
    }
}
