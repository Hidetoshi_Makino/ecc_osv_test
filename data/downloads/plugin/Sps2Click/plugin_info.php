<?php
/*
 * Copyright(c) SOFTBANK PAYMENT SERVICE Corp. All rights reserved.
 * http://www.sbpayment.jp/
 */

class plugin_info {
    /** プラグインコード(必須)：プラグインを識別する為キーで、他のプラグインと重複しない一意な値である必要がありま. */
    static $PLUGIN_CODE       = "Sps2Click";
    /** プラグイン名(必須)：EC-CUBE上で表示されるプラグイン名. */
    static $PLUGIN_NAME       = "ソフトバンク・ペイメント・サービス 2クリック注文プラグイン";
    /** クラス名(必須)：プラグインのクラス（拡張子は含まない） */
    static $CLASS_NAME        = "Sps2Click";
    /** プラグインバージョン(必須)：プラグインのバージョン. */
    static $PLUGIN_VERSION    = "1.0";
    /** 対応バージョン(必須)：対応するEC-CUBEバージョン. */
    static $COMPLIANT_VERSION = "2.12.0";
    /** 作者(必須)：プラグイン作者. */
    static $AUTHOR            = "ソフトバンク・ペイメント・サービス EC-CUBEサポート事務局";
    /** 説明(必須)：プラグインの説明. */
    static $DESCRIPTION       = "ソフトバンク・ペイメント・サービス用の２クリック注文プラグインです。";
    /** プラグインURL：プラグイン毎に設定出来るURL（説明ページなど） */
    static $PLUGIN_SITE_URL   = "http://sbpayment.ec-cube.pro/";
    /** プラグイン作者URL：プラグイン毎に設定出来るURL（説明ページなど） */
    static $AUTHOR_SITE_URL   = "http://sbpayment.ec-cube.pro/";
    /** フックポイント：フックポイントとコールバック関数を定義します */
    static $HOOK_POINTS       = array(
        array("LC_Page_Products_Detail_action_before", 'hookActionBefore'),
        array("LC_Page_Products_List_action_before", 'hookActionBefore'),
        array("LC_Page_Cart_action_before", 'hookActionBefore'),
        array("LC_Page_Shopping_action_before", 'hookActionBefore'),
        array("LC_Page_Shopping_Payment_action_before", 'hookActionBefore'),
        array("LC_Page_Shopping_Deliv_action_before", 'hookActionBefore'),
        array("LC_Page_Shopping_Confirm_action_before", 'hookActionBefore'),
        array("LC_Page_Shopping_Confirm_action_after", 'hookActionAfter'),
        array("prefilterTransform", 'prefilterTransform'));
}
