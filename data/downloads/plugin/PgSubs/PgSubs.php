<?php
/*
 *
 * Copyright(c) 2012 GMO Payment Gateway, Inc. All Rights Reserved.
 *
 * http://www.gmo-pg.com/
 *
 * License: see a LICENCE.txt file.
 */

require_once (PLUGIN_UPLOAD_REALDIR . 'PgSubs/LC_PgSubs_Ex.php');

/**
 * プラグインのメインクラス
 *
 * @package PgSubs
 * @author GMO Payment Gateway, Inc.
 * @version $Id$
 *
 */
class PgSubs extends SC_Plugin_Base {

    static $plugin_name = 'PgSubs';
    static $plugin_data = null;

    /**
     * コンストラクタ
     */
    public function __construct(array $arrSelfInfo) {
        parent::__construct($arrSelfInfo);
        if (!$this->plugin_name) {
            $this->plugin_name = $arrSelfInfo['class_name'];
        }
    }

    /**
     * インストール
     * installはプラグインのインストール時に実行されます.
     * 引数にはdtb_pluginのプラグイン情報が渡されます.
     *
     * @param array $arrPlugin plugin_infoを元にDBに登録されたプラグイン情報(dtb_plugin)
     * @return void
     */
    function install($arrPlugin) {
        if (!is_object($arrPlugin)) {
            $my_obj = new $arrPlugin['class_name']($arrPlugin);
        } else {
            $my_obj = $this;
        }
        $my_obj->plugin_data = $arrPlugin;
        $class_name = 'LC_' . $my_obj->plugin_name . '_Ex';
        $obj = new $class_name();
        $arrFiles = $obj->getInstallFiles($my_obj);
        $arrErr = $my_obj->installFiles($arrFiles);

        if (!SC_Utils_Ex::isBlank($arrErr)) {
            $msg = 'プラグインインストールエラー: ' . implode('<br />', $arrErr);
            GC_Utils_Ex::gfPrintLog($msg);
        }
        $obj->actionInstall($my_obj);
    }

    /**
     * アンインストール
     * uninstallはアンインストール時に実行されます.
     * 引数にはdtb_pluginのプラグイン情報が渡されます.
     *
     * @param array $arrPlugin プラグイン情報の連想配列(dtb_plugin)
     * @return void
     */
    function uninstall($arrPlugin) {
        /*
        if (!is_object($arrPlugin)) {
            $my_obj = new $arrPlugin['class_name']($arrPlugin);
        } else {
            $my_obj = $this;
        }

        $my_obj->plugin_data = $arrPlugin;
        $class_name = 'LC_' . $my_obj->plugin_name . '_Ex';
        $obj = new $class_name();
        $arrFiles = $obj->getInstallFiles($my_obj);
        if (!SC_Utils_Ex::isBlank($arrFiles)) {
            foreach ($arrFiles as $src => $dst) {
                SC_Helper_FileManager_Ex::deleteFile($dst);
            }
        }
        $obj->actionUninstall($my_obj);
        */
    }

    /**
     * 稼働
     * enableはプラグインを有効にした際に実行されます.
     * 引数にはdtb_pluginのプラグイン情報が渡されます.
     *
     * @param array $arrPlugin プラグイン情報の連想配列(dtb_plugin)
     * @return void
     */
    function enable($arrPlugin) {
        if (!is_file(MODULE_REALDIR . 'mdl_pg_mulpay/inc/include.php')){
            $msg = 'プラグイン enableエラー: EC-CUBEペイメント(GMO-PG)決済モジュールが見つかりません。有効に出来ません。';
            GC_Utils_Ex::gfPrintLog($msg);
            return false;
        }
        if (!is_object($arrPlugin)) {
            $my_obj = new $arrPlugin['class_name']($arrPlugin);
        } else {
            $my_obj = $this;
        }
        $my_obj->plugin_data = $arrPlugin;
        $class_name = 'LC_' . $my_obj->plugin_name . '_Ex';
        $obj = new $class_name();
        $arrFiles = $obj->getEnableFiles($my_obj);
        $arrErr = $my_obj->installFiles($arrFiles);

        if (!SC_Utils_Ex::isBlank($arrErr)) {
            $msg = 'プラグイン enableエラー: ' . implode('<br />', $arrErr);
            GC_Utils_Ex::gfPrintLog($msg);
        }
        $obj->actionEnable($my_obj);
    }

    /**
     * 停止
     * disableはプラグインを無効にした際に実行されます.
     * 引数にはdtb_pluginのプラグイン情報が渡されます.
     *
     * @param array $arrPlugin プラグイン情報の連想配列(dtb_plugin)
     * @return void
     */
    function disable($arrPlugin) {
        if (!is_object($arrPlugin)) {
            $my_obj = new $arrPlugin['class_name']($arrPlugin);
        } else {
            $my_obj = $this;
        }

        $my_obj->plugin_data = $arrPlugin;
        $class_name = 'LC_' . $my_obj->plugin_name . '_Ex';
        $obj = new $class_name();
        $arrFiles = $obj->getEnableFiles($my_obj);
        if (!SC_Utils_Ex::isBlank($arrFiles)) {
            foreach ($arrFiles as $src => $dst) {
                SC_Helper_FileManager_Ex::deleteFile($dst);
            }
        }
        $obj->actionDisable($my_obj);
    }

    /**
     * 処理の介入箇所とコールバック関数を設定
     * registerはプラグインインスタンス生成時に実行されます
     *
     * @param SC_Helper_Plugin $objHelperPlugin
     */
    function register(SC_Helper_Plugin $objHelperPlugin) {
        if (!is_file(MODULE_REALDIR . 'mdl_pg_mulpay/inc/include.php')){ return false; }
        return parent::register($objHelperPlugin, $priority);
    }

    // プラグインの設定値を読み込む
    function loadData($force_read = false) {
        $arrRet = array();
        if ($this->plugin_data && $force_read) {
            return $this->plugin_data;
        }
        $arrData = SC_Plugin_Util_Ex::getPluginByPluginCode($this->plugin_name);
        if (!SC_Utils_Ex::isBlank($arrData['free_field1'])) {
            $arrRet = unserialize($arrData['free_field1']);
        }
        $this->plugin_data = $arrRet;
        return $arrRet;
    }

    // プラグイン独自の設定データを保存
    function saveData($arrData) {
        $this->plugin_data = $arrData;

        $objQuery = SC_Query_Ex::getSingletonInstance();
        $sqlval = array();
        $sqlval['free_field1'] = serialize($arrData);
        $sqlval['update_date'] = 'CURRENT_TIMESTAMP';
        $where = "plugin_code = ?";
        $objQuery->update('dtb_plugin', $sqlval, $where, array($this->plugin_info));
    }

    // プラグイン独自の設定データ個別を更新
    function setValue($key, $value) {
        $objQuery = SC_Query_Ex::getSingletonInstance();
        $objQuery->begin();
        $arrData = $this->loadData(true);
        $arrData[$key] = $value;
        $this->saveData($arrData);
        $objQuery->commit();
    }

    // プラグイン独自の設定データ個別を読み込み
    function getValue($key) {
        $arrData = $this->loadData(true);
        return $arrData[$key];
    }

    /**
     * プレフィルタコールバック関数
     *
     * @param string &$source テンプレートのHTMLソース
     * @param LC_Page_Ex $objPage ページオブジェクト
     * @param string $filename テンプレートのファイル名
     * @return void
     */
    function prefilterTransform(&$source, LC_Page_Ex $objPage, $filename) {
        if (!is_file(MODULE_REALDIR . 'mdl_pg_mulpay/inc/include.php')){ return false; }
        $class_name = get_class($objPage);
        $lc_class_name = 'LC_' . $this->plugin_name . '_Ex';
        $obj = new $lc_class_name();
        $obj->actionPrefilterTransform($class_name, $source, $objPage, $filename, $this);
    }

    /**
        以下、各フック関数。処理はLC_XXXクラスに.
    **/
    function hookActionBefore(LC_Page_Ex $objPage) {
        $this->callHookAction('before', $objPage);
    }

    function hookActionAfter(LC_Page_Ex $objPage) {
        $this->callHookAction('after', $objPage);
    }

    function hookActionMode(LC_Page_Ex $objPage) {
        $this->callHookAction('mode', $objPage);
    }

    function callHookAction($hook_point, &$objPage) {
        if (!is_file(MODULE_REALDIR . 'mdl_pg_mulpay/inc/include.php')){ return false; }
        $class_name = get_class($objPage);
        $lc_class_name = 'LC_' . $this->plugin_name . '_Ex';
        $obj = new $lc_class_name();
        $obj->actionHook($class_name, $hook_point, $objPage, $this);
    }

    // その他ユーティリティ関数

    // ブロック追加
    function setBlocData($bloc_data, $bloc_name, $filename, $device_type_id = DEVICE_TYPE_PC, $php_path = "", $is_force = false) {
        $arrPluginInfo = $this->getPluginInfo();
        $plugin_id = $arrPluginInfo['plguin_id'];

        $objLayout = new SC_Helper_PageLayout_Ex();
        $objQuery = SC_Query_Ex::getSingletonInstance();
        $objQuery->begin();

        $bloc_dir = $objLayout->getTemplatePath($device_type_id) . BLOC_DIR;
        $tpl_path = $filename . '.tpl';

        if (!SC_Utils_Ex::isBlank($plugin_id)) {
            $where = 'filename = ?';
            $arrval = array($filename);
        } else {
            $where = 'filename = ?';
            $arrval = array($filename);
        }

        $arrExists = $objLayout->getBlocs($device_type_id, $where, $arrval);
        $exists_file = $bloc_dir . $arrExists[0]['filename'] . '.tpl';
        if (file_exists($exists_file)) {
            if ($is_force) {
                @copy($exists_file, $exists_file . '.bak.' . date('YmdHis'));
                unlink($exists_file);
            } else {
                $objQuery->commit();
                return $arrExists[0]['bloc_id'];
            }
        }

        $sqlval_bloc = array();
        $sqlval_bloc['device_type_id'] = $device_type_id;
        $sqlval_bloc['bloc_name'] = $bloc_name;
        $sqlval_bloc['tpl_path'] = $filename . '.tpl';
        $sqlval_bloc['filename'] = $filename;
        $sqlval_bloc['update_date'] = "CURRENT_TIMESTAMP";
        if (!SC_Utils_Ex::isBlank($php_path)) {
            $sqlval_bloc['php_path'] = $php_path;
        }
        $sqlval_bloc['deletable_flg'] = 0;
        $sqlval_bloc['plugin_id'] = $plugin_id;
        $objQuery->setOrder('');

        if (SC_Utils_Ex::isBlank($arrExists[0]['bloc_id'])) {
            $sqlval_bloc['bloc_id'] = $objQuery->max('bloc_id', "dtb_bloc", "device_type_id = ?", array($device_type_id)) + 1;
            $sqlval_bloc['create_date'] = "CURRENT_TIMESTAMP";
            $objQuery->insert("dtb_bloc", $sqlval_bloc);
            $bloc_id = $sqlval_bloc['bloc_id'];
        } else {
            $objQuery->update("dtb_bloc", $sqlval_bloc, "device_type_id = ? AND bloc_id = ?", array($device_type_id, $arrExists[0]['bloc_id']));
            $bloc_id = $arrExists[0]['bloc_id'];
        }

        $bloc_path = $bloc_dir . $tpl_path;
        if (!SC_Helper_FileManager_Ex::sfWriteFile($bloc_path, $bloc_data)) {
            $objQuery->rollback();
            return false;
        }

        $objQuery->commit();
        return $bloc_id;
    }

    // ページ追加
    function setPageData($tpl_data, $page_name, $url, $filename, $device_type_id = DEVICE_TYPE_PC, $is_force = false) {
        $objLayout = new SC_Helper_PageLayout_Ex();
        $objQuery =& SC_Query_Ex::getSingletonInstance();
        $objQuery->begin();

        $tpl_dir = $objLayout->getTemplatePath($device_type_id);
        $tpl_path = $filename . '.tpl';

        $arrExists = $objLayout->getPageProperties($device_type_id, null, 'device_type_id = ? and filename = ?', array($device_type_id, $filename));

        $exists_file = $tpl_dir . $arrExists[0]['filename'] . '.tpl';
        if (file_exists($exists_file)) {
            if ($is_force) {
                @copy($exists_file, $exists_file . '.bak.' . date('YmdHis'));
                unlink($exists_file);
            } else {
                $objQuery->commit();
                return $arrExists[0]['page_id'];
            }
        }
        $table = 'dtb_pagelayout';
        $arrValues = array();
        $arrValues['device_type_id'] = $device_type_id;
        $arrValues['header_chk'] = 1;
        $arrValues['footer_chk'] = 1;
        $arrValues['page_name'] = $page_name;
        $arrValues['url'] = $url;
        $arrValues['filename'] = $filename;
        $arrValues['edit_flg'] = '2';
        $arrValues['update_url'] = $_SERVER['HTTP_REFERER'];
        $arrValues['update_date'] = 'CURRENT_TIMESTAMP';
        $objQuery->setOrder('');
        if (SC_Utils_Ex::isBlank($arrExists[0]['page_id'])) {
            $arrValues['page_id'] = 1 + $objQuery->max('page_id', $table, 'device_type_id = ?',
                                                       array($arrValues['device_type_id']));
            $arrValues['create_date'] = 'CURRENT_TIMESTAMP';
            $objQuery->insert($table, $arrValues);
            $page_id = $arrValues['page_id'];
        } else {
            $objQuery->update($table, $arrValues, 'page_id = ? AND device_type_id = ?',
                                                       array($arrExists[0]['page_id'], $arrValues['device_type_id']));
            $page_id = $arrExists[0]['page_id'];
        }

        $tpl_path = $tpl_dir . $filename . '.tpl';

        if (!SC_Helper_FileManager_Ex::sfWriteFile($tpl_path, $tpl_data)) {
            $objQuery->rollback();
            return false;
        }
        $objQuery->commit();
        return $page_id;
    }

    function installFiles($arrFiles) {
        $arrErr = array();
        if (!SC_Utils_Ex::isBlank($arrFiles)) {
            foreach ($arrFiles as $file) {
                GC_Utils_Ex::gfPrintLog($file['src'] . 'を' .  $file['dst'] . 'に複製します。');
                if (is_dir($file['src'])) {
                    SC_Utils_Ex::sfCopyDir($file['src'], $file['dst']);
                    if (!is_dir($file['dst'])) {
                        $arrErr[] = $file['src'] . 'を' . $file['dst'] . 'に複製出来ませんでした。';
                    }
                } else if (is_file($file['src'])) {
                    $temp = copy($file['src'], $file['dst']);
                    if (!is_file($file['dst'])) {
                        $arrErr[] = $file['src'] . 'を' . $file['dst'] . 'に複製出来ませんでした。';
                    }
                } else {
                    $arrErr[] = '複製元ファイル' . $file['src'] . 'が存在しませんでした。';
                }
                GC_Utils_Ex::gfPrintLog($file['src'] . 'を' .  $file['dst'] . 'に複製しました。');
            }
        }
        return $arrErr;
    }

}
