<?php
/*
 * Copyright(c) 2012 GMO Payment Gateway, Inc. All rights reserved.
 * http://www.gmo-pg.com/
 */
require_once('../require.php');
require_once(MODULE_REALDIR . 'mdl_pg_mulpay/inc/include.php');
require_once(PLUGIN_UPLOAD_REALDIR . 'PgSubs/class_extends/LC_Page_Admin_Order_PgSubs_Payment_Ex.php');

// }}}
// {{{ generate page

$objPage = new LC_Page_Admin_Order_PgSubs_Payment_Ex();
$objPage->init();
$objPage->process();

