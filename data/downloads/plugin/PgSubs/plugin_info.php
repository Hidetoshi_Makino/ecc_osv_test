<?php
/**
 * プラグイン の情報クラス.
 *
 * @package PgSubs
 * @author GMO Payment Gateway, Inc.
 * @version $Id$
 */
class plugin_info{
    /** プラグインコード(必須)：プラグインを識別する為キーで、他のプラグインと重複しない一意な値である必要がありま. */
    static $PLUGIN_CODE       = "PgSubs";
    /** プラグイン名(必須)：EC-CUBE上で表示されるプラグイン名. */
    static $PLUGIN_NAME       = "ＰＧマルチペイメントサービス 定期購入プラグイン";
    /** クラス名(必須)：プラグインのクラス（拡張子は含まない） */
    static $CLASS_NAME        = "PgSubs";
    /** プラグインバージョン(必須)：プラグインのバージョン. */
    static $PLUGIN_VERSION    = "2.12.1";
    /** 対応バージョン(必須)：対応するEC-CUBEバージョン. */
    static $COMPLIANT_VERSION = "2.12.1";
    /** 作者(必須)：プラグイン作者. */
    static $AUTHOR            = "GMOペイメントゲートウェイ株式会社";
    /** 説明(必須)：プラグインの説明. */
    static $DESCRIPTION       = "PGマルチペイメントサービス決済モジュール用の定期購入に関する機能を提供するプラグインです。";
    /** プラグインURL：プラグイン毎に設定出来るURL（説明ページなど） */
    static $PLUGIN_SITE_URL   = "http://www.gmo-pg.com/";
    /** プラグイン作者URL：プラグイン毎に設定出来るURL（説明ページなど） */
    static $AUTHOR_SITE_URL   = "http://www.gmo-pg.com/";
    /** ライセンス **/
    // ライセンスについては同梱のLICENSE.txtを参照して下さい。
    static $LICENSE = "ORG";
    /** フックポイント：フックポイントとコールバック関数を定義します */
    static $HOOK_POINTS       = array(
        array("LC_Page_Admin_Order_Edit_action_before", 'hookActionBefore'),
        array("LC_Page_Admin_Order_Edit_action_after", 'hookActionAfter'),
        array("LC_Page_Admin_Order_action_before", 'hookActionBefore'),
        array("LC_Page_Admin_Order_action_after", 'hookActionAfter'),
        array("LC_Page_Admin_Customer_action_after", 'hookActionAfter'),
        array("LC_Page_Admin_Customer_Edit_action_before", "hookActionBefore"),
        array("LC_Page_Admin_Customer_Edit_action_after", "hookActionAfter"),
        array("LC_Page_Admin_Basis_DeliveryInput_action_before", 'hookActionBefore'),
        array("LC_Page_Admin_Basis_DeliveryInput_action_after", 'hookActionAfter'),
        array("LC_Page_Shopping_Payment_action_before", 'hookActionBefore'),
        array("LC_Page_Shopping_Payment_action_after", 'hookActionAfter'),
        array("LC_Page_Shopping_action_before", 'hookActionBefore'),
        array("LC_Page_Shopping_Deliv_action_before", 'hookActionBefore'),
        array("LC_Page_Shopping_Confirm_action_before", 'hookActionBefore'),
        array("LC_Page_Mypage_ChangeCard_action_after", 'hookActionAfter'),
        array("LC_Page_Mdl_PG_MULPAY_Helper_Ex_action_before", 'hookActionBefore'),
        array("LC_Page_Mypage_Refusal_Ex_action_before", 'hookActionBefore'),
        array("LC_Page_Mdl_PG_MULPAY_Recv_action_", 'hookActionMode'),
        array("prefilterTransform", 'prefilterTransform'),
        );
}
