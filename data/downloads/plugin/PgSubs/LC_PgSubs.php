<?php
/*
 * Copyright(c) 2012 GMO Payment Gateway, Inc. All rights reserved.
 * http://www.gmo-pg.com/
 */

if (is_file(MODULE_REALDIR . 'mdl_pg_mulpay/inc/include.php')) {
    require_once(MODULE_REALDIR . 'mdl_pg_mulpay/inc/include.php');
}

if (!defined('MDL_PG_MULPAY_PAYID_BANKTRANS')) {
    define('MDL_PG_MULPAY_PAYID_BANKTRANS', '100');
}

if (!defined('MDL_PG_MULPAY_SUBS_STATUS_END')) {
    define('MDL_PG_MULPAY_SUBS_STATUS_END', '9');      // 終了
}

/**
 * プラグインの処理クラス
 */
class LC_PgSubs {

    // インストール時に複製するファイル・ディレクトリ設定（uninstall時には削除)
    function getInstallFiles(&$objPlugin) {
        $arrFiles = array(
            array(
                'src' => PLUGIN_UPLOAD_REALDIR . $objPlugin->plugin_name . "/logo.png",
                'dst' => PLUGIN_HTML_REALDIR . $objPlugin->plugin_name ."/logo.png",
                ),
        );
        return $arrFiles;
    }

    // enable時に複製するファイル・ディレクトリ設定（disable時には削除)
    function getEnableFiles(&$objPlugin) {
        $arrFiles = array(
            array(
                'src' => PLUGIN_UPLOAD_REALDIR . $objPlugin->plugin_name . "/admin/plg_pgsubs_config.php",
                'dst' => HTML_REALDIR . ADMIN_DIR . "basis/plg_pgsubs_config.php",
                ),
            array(
                'src' => PLUGIN_UPLOAD_REALDIR . $objPlugin->plugin_name . "/admin/plg_pgsubs_order.php" ,
                'dst' => HTML_REALDIR . ADMIN_DIR .  "order/plg_pgsubs_order.php",
                ),
            array(
                'src' => PLUGIN_UPLOAD_REALDIR . $objPlugin->plugin_name . "/admin/plg_pgsubs_payment.php" ,
                'dst' => HTML_REALDIR . ADMIN_DIR . "order/plg_pgsubs_payment.php",
                ),
            array(
                'src' => PLUGIN_UPLOAD_REALDIR . $objPlugin->plugin_name . "/copy/mypage_subs_order.php",
                'dst' => HTML_REALDIR . "mypage/mypage_subs_order.php",
                ),
        );
        return $arrFiles;
    }

    function actionInstall(&$objPlugin) {
        // インストール時の追加処理
        $this->createPluginTable();
        // テーブルの修正
        $objDb = new SC_Helper_DB_Ex();

        $objDb->sfColumnExists('dtb_customer', 'plg_pgsubs_status', 'smallint', '', true);
        $objDb->sfColumnExists('dtb_customer', 'plg_pgsubs_data', 'text', '', true);
    }

    function actionUninstall(&$objPlugin) {
        // アンインストール時の追加処理
    }

    function actionEnable(&$objPlugin) {
        // 有効時の追加処理
        $template_dir = PLUGIN_UPLOAD_REALDIR . $objPlugin->plugin_name . '/templates/';
        $tpl_data = file_get_contents($template_dir . 'default/mypage_subs_order.tpl');
        $page_name = 'MYページ/定期購入一覧';
        $url = 'mypage/mypage_subs_order.php';
        $filename = 'mypage/mypage_subs_order';
        $device_type_id = DEVICE_TYPE_PC;
        $is_force = false;
        $page_id = $objPlugin->setPageData($tpl_data, $page_name, $url, $filename, $device_type_id, $is_force);
        $device_type_id = DEVICE_TYPE_SMARTPHONE;
        $is_force = false;
        $page_id = $objPlugin->setPageData($tpl_data, $page_name, $url, $filename, $device_type_id, $is_force);
        $device_type_id = DEVICE_TYPE_MOBILE;
        $is_force = false;
        $tpl_data = file_get_contents($template_dir . 'mobile/mypage_subs_order.tpl');
        $page_id = $objPlugin->setPageData($tpl_data, $page_name, $url, $filename, $device_type_id, $is_force);
    }

    function actionDisable(&$objPlugin) {
        // disable時の追加処理
    }

    function actionPrefilterTransform($class_name, &$source, &$objPage, $filename, &$objPlugin) {
        $template_dir = PLUGIN_UPLOAD_REALDIR . $objPlugin->plugin_name . '/templates/';
        $template_file = '';
        $selecter = '';
        $selecter_method = '';
        $index = NULL;
        $is_require = true;
        $find_selecter = '';
        $find_index = NULL;
        $find_is_require = false;
        switch($objPage->arrPageLayout['device_type_id']){
            case DEVICE_TYPE_PC:
            case DEVICE_TYPE_SMARTPHONE:
            case DEVICE_TYPE_MOBILE:
                // ディレクトリ割当
                switch($objPage->arrPageLayout['device_type_id']) {
                    case DEVICE_TYPE_MOBILE:
                        $template_dir .= 'mobile/';
                        break;
                    case DEVICE_TYPE_SMARTPHONE:
                        $template_dir .= 'sphone/';
                        break;
                    case DEVICE_TYPE_PC:
                        $template_dir .= 'default/';
                        break;
                }
                if(strpos($filename, 'shopping/payment.tpl') !== false) {
                    $template_file = 'shopping_payment_subs_add.tpl';
                    $selecter = '';
                    $selecter_method = '';
                    if ($objPage->arrPageLayout['device_type_id'] == DEVICE_TYPE_MOBILE) {
                        $str = file_get_contents($template_dir . $template_file);
                        if (!SC_Utils_Ex::isBlank($str)) {
                            $source = $str;
                        }
                        $template_file = '';
                        $selecter_method = '';
                    } else {
                        $str = file_get_contents($template_dir . $template_file);
                        if (!SC_Utils_Ex::isBlank($str)) {
                            $source .= $str;
                        }
                        $template_file = '';
                    }
                } else if (strpos($filename, 'mypage/') !== false && strpos($filename, 'mypage/navi.tpl') === false) {
                    $template_file = 'mypage_index_add.tpl';
                    $selecter = 'h2';
                    $selecter_method = 'insertBefore';
                    GC_Utils::gfPrintLog($template_dir . $template_file);
                }
                // 退会制限対応
                if (strpos($filename, 'mypage/refusal.tpl') !== false) {
                    if (is_file($template_dir . 'mypage_refusal_add.tpl')) {
                        if ($objPage->arrPageLayout['device_type_id'] == DEVICE_TYPE_MOBILE) {
                            $str = file_get_contents($template_dir . 'mypage_refusal_add.tpl');
                            $source = str_replace('<input type="submit" name="complete" value="退会する">', $str, $source);
                        } else {
                            $objTransform = new SC_Helper_Transform($source);
                            $objTransform->select('h2.title')->insertAfter(file_get_contents($template_dir . 'mypage_refusal_add.tpl'));
                            $source = $objTransform->getHTML();
                        }
                    }
                }
                break;
            case DEVICE_TYPE_ADMIN:
            default:
                $template_dir .= 'admin/';
                if(preg_match('/^LC_Page_Admin.*_Ex$/', $class_name) && strpos($filename, 'mail_templates') === FALSE) {
                    $template_file = 'admin_common_subs_add.tpl';
                    $selecter = 'head';
                    $selecter_method = 'appendChild';
                }
                if (strpos($filename, 'customer/edit.tpl') !== false) {
                    $file = $template_dir . 'admin_customer_edit_add.tpl';
                    $objTransform = new SC_Helper_Transform($source);
                    $objTransform->select('table.form')->appendChild(file_get_contents($file));
                    $source = $objTransform->getHTML();
                }
                break;
        }

        if ($selecter && $selecter_method) {
            $objTransform = new SC_Helper_Transform($source);
            switch ($selecter_method) {
            case 'removeElement':
                if ($find_selecter) {
                    $objTransform->select($selecter, $index, $is_require)->find($find_selecter, $find_index, $find_is_require)->$selecter_method();
                } else {
                    $objTransform->select($selecter, $index, $is_require)->$selecter_method();
                }
                break;
            case 'insertAfter':
            case 'insertBefore':
            case 'appendFirst':
            case 'appendChild':
            case 'replaceElement':
            case 'appendFirst':
            default:
                if ($find_selecter && is_file($template_dir . $template_file)) {
                    $objTransform->select($selecter, $index, $is_require)
                        ->find($find_selecter, $find_index, $find_is_require)
                        ->$selecter_method(file_get_contents($template_dir . $template_file));
                } else if (is_file($template_dir . $template_file)) {
                    $objTransform->select($selecter, $index, $is_require)->$selecter_method(file_get_contents($template_dir . $template_file));
                } else {
                    GC_Utils::gfPrintLog('Plugin transform error: not found template=' . $template_dir . $template_file);
                }
                break;
            }
            if ($objPage->arrPageLayout['device_type_id'] == DEVICE_TYPE_MOBILE) {
                // Add card link in case go to subscription list
                if (strpos($filename, 'mypage_subs_order.tpl')) {
                    $template_dir_card = MDL_PG_MULPAY_TEMPLATE_PATH . 'mobile/';
                    $template_file_card = 'mypage_navi_add.tpl';
                    $snip = file_get_contents($template_dir_card . $template_file_card);
                    $source = str_replace("退会</a><br>", "退会</a><br>" . $snip, $source);
                }
                $snip = file_get_contents($template_dir . $template_file);
                $source = str_replace("カード情報編集</a><br>", "カード情報編集</a><br>" . $snip, $source);
            } else {
                $source = $objTransform->getHTML();
            }
        }
    }

    function actionHook($class_name, $hook_point, &$objPage, $objPlugin) {
        // 動的関数割当　アクションが多いときは、こっちで定義を推奨
        if (method_exists($this, $class_name . '_' . $hook_point)) {
            $this->{$class_name . '_' . $hook_point}($objPage, $objPlugin);
        }

        switch ($class_name) {
        case 'LC_Page_Shopping_Ex':
            if ($hook_point == 'before') {
                $this->lfDoShoppingBefore($objPage);
            }
            break;
        case 'LC_Page_Shopping_Deliv_Ex':
            if ($hook_point == 'before') {
                $this->lfDoShoppingDelivBefore($objPage);
            }
            break;
        case 'LC_Page_Shopping_Payment_Ex':
            if ($hook_point == 'before') {
                $this->lfDoShoppingPaymentBefore($objPage);
            } else if ($hook_point == 'after') {
                $this->lfDoShoppingPaymentAfter($objPage);
            }
            break;
        case 'LC_Page_Shopping_Confirm_Ex':
            if ($hook_point == 'before') {
                $this->lfDoShoppingConfirmBefore($objPage);
            }
            break;
        case 'LC_Page_Mypage_ChangeCard_Ex':
            if ($hook_point == 'after') {
                $this->lfDoMyPageChangeCardAfter($objPage);
            }
            break;
        case 'LC_Page_Mypage_Refusal_Ex':
            if ($hook_point == 'before') {
                $this->lfDoMyPageRefusalBefore($objPage);
            }
        case 'LC_Page_Mdl_PG_MULPAY_Helper_Ex':
            if ($hook_point == 'before') {
                $this->lfDoShoppingModuleBefore($objPage);
            }
            break;
        case 'LC_Page_Mdl_PG_MULPAY_Recv_Ex':
            if ($hook_point == 'mode') {
                $this->lfDoMdlRecvMode($objPage);
            }
            break;
        case 'LC_Page_Admin_Basis_DeliveryInput_Ex':
            if ($hook_point == 'before') {
                $this->lfDoAdminBasisDeliveryInputBefore($objPage);
            } else if ($hook_point == 'after') {
                $this->lfDoAdminBasisDeliveryInputAfter($objPage);
            }
            break;
        case 'LC_Page_Admin_Order_Ex':
            if ($hook_point == 'after') {
                $this->lfDoAdminOrderAfter($objPage);
            } else if($hook_point == 'before') {
                $this->lfDoAdminOrderBefore($objPage);
            }
            break;
        case 'LC_Page_Admin_Order_Edit_Ex':
            if ($hook_point == 'after') {
                $this->lfDoAdminOrderEditAfter($objPage);
            } else if($hook_point == 'before') {
                $this->lfDoAdminOrderEditBefore($objPage);
            }
            break;
        case 'LC_Page_Admin_Customer_Edit_Ex':
            if ($hook_point == 'after') {
                $this->lfDoAdminCustomerEditAfter($objPage);
            } else if ($hook_point == 'before') {
                $this->lfDoAdminCustomerEditBefore($objPage);
            }
            break;
        case 'LC_Page_Admin_Customer_Ex':
            $this->lfDoAdminCustomer($objPage);
            break;
        }
    }

    /**
     * プラグイン用テーブル作成
     *
     * @return void
     */
    function createPluginTable() {
        $objQuery =& SC_Query_Ex::getSingletonInstance();
        $arrTables = $objQuery->listTables();
        $arrTables = unserialize(strtolower(serialize($arrTables)));

        // テーブル生成
        $table = 'plg_PgSubs_order';
        $create_table_sql =<<<EOSQL
CREATE TABLE {$table} (
    subs_order_id int NOT NULL,
    customer_id int NOT NULL,
    product_id int NOT NULL,
    product_class_id int NOT NULL,
    product_name text NOT NULL,
    product_code text,
    classcategory_name1 text,
    classcategory_name2 text,
    price numeric,
    quantity numeric,
    point_rate numeric,
    first_order_id int,
    last_order_id int,
    payment_id int NOT NULL,
    deliv_id int,
    order_count int NOT NULL DEFAULT 1,
    memo text,
    status smallint NOT NULL DEFAULT 0,
    del_flg smallint NOT NULL DEFAULT 0,
    create_date timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
    update_date timestamp,
    PRIMARY KEY (subs_order_id)
);
EOSQL;
        $seq_name = $table . '_subs_order_id';
        if (!in_array(strtolower($table), $arrTables)) {
            $objQuery->query($create_table_sql);
            $objManager =& $objQuery->conn->loadModule('Manager');
            $objManager->createSequence($seq_name, 1);
        }

        // テーブル生成2
        $table = 'plg_PgSubs_csv';
        $create_table_sql =<<<EOSQL
CREATE TABLE {$table} (
    subs_csv_id int NOT NULL,
    type text,
    filename text,
    line_num text,
    memo text,
    member_id INT NOT NULL DEFAULT 0,
    create_date timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (subs_csv_id)
);
EOSQL;
        if (!in_array(strtolower($table), $arrTables)) {
            $seq_name = $table . '_subs_csv_id';
            $objQuery->query($create_table_sql);
            $objManager->createSequence($seq_name, 1);
        }
    }

    function lfInsertSubsOrder($arrOrder) {
        $objPurchase = new SC_Helper_Purchase_Ex();
        $arrOrderDetail = $objPurchase->getOrderDetail($arrOrder['order_id'], false);
        $objQuery =& SC_Query_Ex::getSingletonInstance();
        $table = 'plg_PgSubs_order';
        $arrCols = $objQuery->listTableFields($table);

        foreach ($arrOrderDetail as $arrDetail) {
            $sqlval = array();
            foreach ($arrDetail as $key => $val) {
                if (in_array($key, $arrCols)) {
                    $sqlval[$key] = $val;
                }
            }
            $sqlval['customer_id'] = $arrOrder['customer_id'];
            $sqlval['first_order_id'] = $arrOrder['order_id'];
            $sqlval['last_order_id'] = $arrOrder['order_id'];
            $sqlval['payment_id'] = $arrOrder['payment_id'];
            $sqlval['deliv_id'] = $arrOrder['deliv_id'];
            $sqlval['order_count'] = 1;
            $sqlval['status'] = MDL_PG_MULPAY_SUBS_STATUS_NEW;
            $sqlval['del_flg'] = '0';
            $sqlval['create_date'] = $arrOrder['create_date'];
            $sqlval['update_date'] = 'NOW()';
            $seq_name = $table . '_subs_order_id';
            $sqlval['subs_order_id'] = $objQuery->nextVal($seq_name);
            GC_Utils::gfPrintLog(print_r($sqlval,true));
            $objQuery->insert($table, $sqlval);
        }
    }

    function lfDoMyPageChangeCardAfter(&$objPage) {
        if ($objPage->tpl_is_success) {
            // カード情報への何らかの操作がOKな場合。
            // 削除によりデフォルトカードが変わった場合も対象
            $objQuery =& SC_Query_Ex::getSingletonInstance();
            $objCustomer = new SC_Customer_Ex();
            $table = 'dtb_customer';
            $sqlval = array();
            $sqlval['plg_pgsubs_status'] = '0';
            $sqlval['plg_pgsubs_data'] = '';
            $customer_id = $objCustomer->getValue('customer_id');
            $objQuery->update('dtb_customer', $sqlval, 'customer_id = ?', array($customer_id));
            GC_Utils::gfPrintLog('update customer pgsubs: status=0 customer_id=' . $customer_id);
        }
    }

    function lfDoMdlRecvMode(&$objPage) {
        $order_id = $objPage->lfGetOrderId($_REQUEST['OrderID']);
        if (!SC_Utils_Ex::isBlank($order_id)) {
            $objQuery =& SC_Query_Ex::getSingletonInstance();
            $arrOrder = SC_Util_PG_MULPAY_Ex::getOrderPayData($order_id);
            if (!SC_Utils_Ex::isBlank($arrOrder)
                    && $arrOrder[MDL_PG_MULPAY_ORDER_COL_SPFLG] == 'subscription'
                    && $arrOrder[MDL_PG_MULPAY_ORDER_COL_SPDATA] == ''
                    && ($arrOrder['status'] == ORDER_NEW || $arrOrder['status'] == ORDER_PRE_END || $arrOrder['status'] == ORDER_PAY_WAIT)
                    ) {

                $objPurchase = new SC_Helper_Purchase_Ex();
                $sqlval = array();
                $sqlval[MDL_PG_MULPAY_ORDER_COL_SPDATA] = MDL_PG_MULPAY_SUBS_STATUS_NEW;
                $objPurchase->sfUpdateOrderStatus($order_id, null, null, null, $sqlval);

                $this->lfInsertSubsOrder($arrOrder);
            }
            // カードを新規登録して新規購入した場合は有効性OK
            if (!SC_Utils_Ex::isBlank($arrOrder)
                    && $arrOrder['register_card'] == '1'
                    && ($arrOrder['status'] == ORDER_NEW || $arrOrder['status'] == ORDER_PRE_END || $arrOrder['status'] == ORDER_PAY_WAIT)
                    ) {

                $sqlval = array();
                $sqlval['plg_pgsubs_status'] = '0'; // 有効性OK
                $sqlval['plg_pgsubs_data'] = '';    //クリア
                $objQuery->update('dtb_customer', $sqlval, 'customer_id = ?', array($arrOrder['customer_id']));
                GC_Utils::gfPrintLog('update customer pgsubs: status=0 customer_id=' . $arrOrder['customer_id']);
            }
            // 登録カード決済で正常に購入出来た場合は有効性OK
            if (!SC_Utils_Ex::isBlank($arrOrder)
                    && $arrOrder[MDL_PG_MULPAY_ORDER_COL_PAYID] == MDL_PG_MULPAY_PAYID_REGIST_CREDIT) {
                $sqlval = array();
                $sqlval['plg_pgsubs_status'] = '0'; // 有効性OK
                $sqlval['plg_pgsubs_data'] = '';    //クリア
                $objQuery->update('dtb_customer', $sqlval, 'customer_id = ?', array($arrOrder['customer_id']));
                GC_Utils::gfPrintLog('update customer pgsubs: status=0 customer_id=' . $arrOrder['customer_id']);
            }
        }
    }

    function lfDoShoppingModuleBefore(&$objPage) {
        $objCartSess = new SC_CartSession_Ex();
        $cart_key = $objCartSess->getKey();
        $objMdl =& SC_Mdl_PG_MULPAY_Ex::getInstance();
        $objPurchase = new SC_Helper_Purchase_Ex();
        $arrConfig = $objMdl->getSubData();
        if ($arrConfig['subs']['subs_product_type'] == $cart_key) {
            $_REQUEST['register_card'] = '1';
            $_POST['register_card'] = '1';
            $objPage->tpl_plg_pg_mulpay_is_subscription = true;
            $objPage->tpl_plg_pg_mulpay_subscription_name = $arrConfig['subs']['subs_product_name'];

            // 受注データへのフラグ付
            if ($objPage->getMode() == '' && !SC_Utils_Ex::isBlank($_SESSION['order_id'])) {
                $order_id = $_SESSION['order_id'];

                $objPurchase = new SC_Helper_Purchase_Ex();
                $sqlval[MDL_PG_MULPAY_ORDER_COL_SPFLG] = 'subscription';
                $sqlval[MDL_PG_MULPAY_ORDER_COL_SPDATA] = '';   // この時点では空
                $objQuery =& SC_Query_Ex::getSingletonInstance();
                $objQuery->begin();
                $objPurchase->sfUpdateOrderStatus($order_id, null, null, null, $sqlval);
                $objQuery->commit();
            }
        } else {
            $objPage->tpl_plg_pg_mulpay_is_subscription = false;
        }
    }

    function lfDoShoppingConfirmBefore(&$objPage) {
        $objCartSess = new SC_CartSession_Ex();
        $cart_key = $objCartSess->getKey();
        $objMdl =& SC_Mdl_PG_MULPAY_Ex::getInstance();
        $arrConfig = $objMdl->getSubData();
        if ($arrConfig['subs']['subs_product_type'] == $cart_key) {
            $objPage->tpl_plg_pg_mulpay_is_subscription = true;
            $objPage->tpl_plg_pg_mulpay_subscription_name = $arrConfig['subs']['subs_product_name'];
        } else {
            $objPage->tpl_plg_pg_mulpay_is_subscription = false;
        }
    }

    function lfDoShoppingBefore(&$objPage) {
        $objCartSess = new SC_CartSession_Ex();
        $cart_key = $objCartSess->getKey();
        $objMdl =& SC_Mdl_PG_MULPAY_Ex::getInstance();
        $arrConfig = $objMdl->getSubData();
        if ($arrConfig['subs']['subs_product_type'] == $cart_key && $objPage->getMode() == 'nonmember') {
            $objPage->arrErr['login_email'] = '※ ' . $arrConfig['subs']['subs_product_name'] . 'は会員登録が必須です。<br />お手数ですが会員登録かログインをして下さい。<br />';
            $_POST['mode'] = '';
            $_REQUEST['mode'] = '';

            $objPage->tpl_plg_pg_mulpay_is_subscription = true;
            $objPage->tpl_plg_pg_mulpay_subscription_name = $arrConfig['subs']['subs_product_name'];
        } else if ($arrConfig['subs']['subs_product_type'] == $cart_key) {
            $objPage->tpl_plg_pg_mulpay_is_subscription = true;
            $objPage->tpl_plg_pg_mulpay_subscription_name = $arrConfig['subs']['subs_product_name'];
        } else {
            $objPage->tpl_plg_pg_mulpay_is_subscription = false;
        }
    }

    function lfDoShoppingDelivBefore(&$objPage) {
        $objCartSess = new SC_CartSession_Ex();
        $cart_key = $objCartSess->getKey();
        $objMdl =& SC_Mdl_PG_MULPAY_Ex::getInstance();
        $arrConfig = $objMdl->getSubData();
        if ($arrConfig['subs']['subs_product_type'] == $cart_key) {
            $objPage->tpl_plg_pg_mulpay_is_subscription = true;
            $objPage->tpl_plg_pg_mulpay_subscription_name = $arrConfig['subs']['subs_product_name'];
        } else {
            $objPage->tpl_plg_pg_mulpay_is_subscription = false;
        }
    }

    function lfDoShoppingPaymentAfter(&$objPage) {
        if ($objPage->tpl_plg_pg_mulpay_is_subscription) {
            $objPage->arrDelivDate = null;
        }
    }

    function lfDoShoppingPaymentBefore(&$objPage) {
        $objCartSess = new SC_CartSession_Ex();
        $cart_key = $objCartSess->getKey();
        $objMdl =& SC_Mdl_PG_MULPAY_Ex::getInstance();
        $arrConfig = $objMdl->getSubData();
        if ($arrConfig['subs']['subs_product_type'] == $cart_key) {
            $objPage->tpl_plg_pg_mulpay_is_subscription = true;
            $objPage->tpl_plg_pg_mulpay_subscription_name = $arrConfig['subs']['subs_product_name'];

            // ポイント利用は強制無効化
            $_POST['point_check'] = "2";
            $_REQUEST['point_check'] = "2";
            $_POST['use_point'] = '0';
            $_REQUEST['use_point'] = '0';

            if ($objPage->getMode() == 'confirm') {
                $payment_id = intval($_REQUEST['payment_id']);
                $arrPayConfig = SC_Util_PG_MULPAY_Ex::getPaymentTypeConfig($payment_id);
                $pay_id = $arrPayConfig[MDL_PG_MULPAY_PAYMENT_COL_PAYID];
                if ($pay_id == MDL_PG_MULPAY_PAYID_CREDIT) {
                    $objClientMember = new SC_Mdl_PG_MULPAY_Client_Member_Ex();
                    $objCustomer = new SC_Customer_Ex();
                    $ret = $objClientMember->searchCard(array('customer_id' => $objCustomer->getValue('customer_id')));
                    
                    if ($ret) {
                        if(count($objClientMember->arrResults) >= MDL_PG_MULPAY_REGIST_CARD_NUM) {
                            $_POST['mode'] = '';
                            $_REQUEST['mode'] = '';
                            $objPage->tpl_pg_regist_card_max = true;
                            $objPage->arrErr['payment_id'] = '※ カード情報が既に' . MDL_PG_MULPAY_REGIST_CARD_NUM . '個登録されています。';
                            $objPage->arrErr['payment_id'] .= '<br />これ以上は新規で登録出来ません。登録済みカードで決済をして頂くかMYページで編集して下さい。';
                        }
                    }
                }
                if ($arrConfig['subs']['subs_after_payment'] == '1'
                        && $pay_id == MDL_PG_MULPAY_PAYID_REGIST_CREDIT) {
                    $_POST['mode'] = '';
                    $_REQUEST['mode'] = '';
                    $objPage->arrErr['payment_id'] .= '※ この支払い方法は利用制限されています。お手数ですが他を選んで下さい。';

                }
            }

        } else {
            $objPage->tpl_plg_pg_mulpay_is_subscription = false;
        }
    }

    function lfDoAdminBasisDeliveryInputAfter(&$objPage) {
        if(!SC_Utils_Ex::isBlank($objPage->arrPlgPgErr)) {
            $objPage->arrErr = array_merge((array)$objPage->arrErr, (array)$objPage->arrPlgPgErr);
        }
    }


    function lfDoAdminBasisDeliveryInputBefore(&$objPage) {
        $objMdl =& SC_Mdl_PG_MULPAY_Ex::getInstance();
        $arrConfig = $objMdl->getSubData();
        if ($_POST['product_type_id'] == $arrConfig['subs']['subs_product_type']) {
            $arrErr = array();
            if (count($_POST['payment_ids']) > 0) {
                $is_payment = true;
                $objQuery =& SC_Query_Ex::getSingletonInstance();
                foreach ($_POST['payment_ids'] as $payment_id) {
                    $arrData = $objQuery->getRow('module_code, memo03', 'dtb_payment', "payment_id = ? and del_flg = 0", array($payment_id));
                    if ($arrData['module_code'] != $objMdl->getCode(true)
                         || (
                                $arrData[MDL_PG_MULPAY_PAYMENT_COL_PAYID] != MDL_PG_MULPAY_PAYID_CREDIT
                                 && $arrData[MDL_PG_MULPAY_PAYMENT_COL_PAYID] != MDL_PG_MULPAY_PAYID_REGIST_CREDIT
                                 && $arrData[MDL_PG_MULPAY_PAYMENT_COL_PAYID] != MDL_PG_MULPAY_PAYID_CREDIT_CHECK
                                 && $arrData[MDL_PG_MULPAY_PAYMENT_COL_PAYID] != MDL_PG_MULPAY_PAYID_CREDIT_SAUTH
                                 && $arrData[MDL_PG_MULPAY_PAYMENT_COL_PAYID] != MDL_PG_MULPAY_PAYID_BANKTRANS
                            )
                        ) {
                        $is_payment = false;
                    }
                }
                if (!$is_payment) {
                    $arrErr['payment_ids'] = '※ 定期購入用の商品種別で利用する支払方法には' . MDL_PG_MULPAY_SERVICE_NAME . 'の対応する決済のみ選択可能です。<br />';
                }
            }
            if(!SC_Utils_Ex::isBlank($arrErr)) {
                $_POST['mode'] = 'pre_edit';
                $objPage->arrPlgPgErr = $arrErr;
            }
        }
    }

    function lfDoAdminOrderAfter(&$objPage) {
    }

    function lfDoAdminOrderBefore(&$objPage) {
    }

    function lfDoAdminCustomer(&$objPage) {
    }

    function lfDoAdminOrderEditAfter(&$objPage) {
    }

    function lfDoAdminOrderEditBefore(&$objPage) {
    }

    function lfDoAdminCustomerEditAfter(&$objPage) {
        $customer_id = $objPage->arrForm['customer_id'];
        if ($objPage->getMode() == 'complete' && !SC_Utils_Ex::isBlank($customer_id)) {
            if ($_REQUEST['plg_pgsubs_status'] === '0' || !SC_Utils_Ex::isBlank($_REQUEST['plg_pgsubs_status'])) {
                $objQuery =& SC_Query_Ex::getSingletonInstance();
                $table = 'dtb_customer';
                $where = 'customer_id = ? AND del_flg = 0';
                $sqlval = array ('plg_pgsubs_status' => $_REQUEST['plg_pgsubs_status']);
                $objQuery->update($table, $sqlval, $where, array($customer_id));
                $objPage->arrForm['plg_pgsubs_status'] = $_REQUEST['plg_pgsubs_status'];
            }
        }

        if (!SC_Utils_Ex::isBlank($customer_id)) {
            if ($objPage->arrForm['plg_pgsubs_status'] === '0') {
                $objPage->tpl_plg_subs_status = true;
            } else if (!SC_Utils_Ex::isBlank($objPage->arrForm['plg_pgsubs_status'])) {
                $objPage->tpl_plg_subs_status = true;
            }

            $objPage->arrPlgPgSubsStatus = array( '0' => '有効性OK/洗替対象',
                                                '1' => '有効性NG',
                                                '2' => '照合エラー',
                                                '3' => 'データエラー');
        } else {
            $objPage->arrPlgPgSubsStatus = null;
        }
    }

    function lfDoAdminCustomerEditBefore(&$objPage) {
    }

    function lfDoMyPageRefusalBefore(&$objPage) {
        $objMdl =& SC_Mdl_PG_MULPAY_Ex::getInstance();
        $arrConfig = $objMdl->getSubData();
        if ($arrConfig['subs']['subs_block_refusal']) {
            $objCustomer = new SC_Customer_Ex();
            $customer_id = $objCustomer->getValue('customer_id');
            $objQuery =& SC_Query_Ex::getSingletonInstance();
            $table = 'plg_PgSubs_order';
            $where = 'customer_id = ? AND status NOT IN (?, ?) AND del_flg = 0';
            $arrVal = array($customer_id, MDL_PG_MULPAY_SUBS_STATUS_CANCEL, MDL_PG_MULPAY_SUBS_STATUS_END);
            if ($objQuery->exists($table, $where, $arrVal)) {
                $objPage->plg_pgsubs_refusal_block = true;
            } else {
                $objPage->plg_pgsubs_refusal_block = false;
            }
        } else {
            $objPage->plg_pgsubs_refusal_block = false;
        }
    }
}
