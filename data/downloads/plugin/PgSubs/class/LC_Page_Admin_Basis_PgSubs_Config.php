<?php
/*
 * Copyright(c) 2012 GMO Payment Gateway, Inc. All rights reserved.
 * http://www.gmo-pg.com/
 */

// {{{ requires
require_once(MODULE_REALDIR . 'mdl_pg_mulpay/inc/include.php');
require_once(CLASS_EX_REALDIR . "page_extends/admin/LC_Page_Admin_Ex.php");
require_once(MDL_PG_MULPAY_CLASSEX_PATH . "util_extends/SC_Util_PG_MULPAY_Ex.php");

/**
 *
 */
class LC_Page_Admin_Basis_PgSubs_Config extends LC_Page_Admin_Ex {

    /**
     * Page を初期化する.
     *
     * @return void
     */
    function init() {

        parent::init();
        $this->tpl_mainpage = PLUGIN_UPLOAD_REALDIR . 'PgSubs/templates/admin/subs_config.tpl';
        $this->tpl_mainno = 'basis';
        $this->tpl_maintitle = '基本情報管理';
        $this->tpl_subtitle = '定期購入機能設定';

        $masterData = new SC_DB_MasterData_Ex();
        $this->arrProductType = $masterData->getMasterData('mtb_product_type');
        $this->arrORDERSTATUS = $masterData->getMasterData('mtb_order_status');
        $this->arrPref = $masterData->getMasterData('mtb_pref');

        $this->httpCacheControl('nocache');
    }

    /**
     * Page のプロセス.
     *lo
     * @return void
     */
    function process() {
        $this->action();
        $this->sendResponse();
    }

    /**
     * Page のアクション.
     *
     * @return void
     */
    function action() {
        $objMdl =& SC_Mdl_PG_MULPAY_Ex::getInstance();

        $objFormParam = new SC_FormParam_Ex();
        $this->lfInitParam($objFormParam);

        $arrConfig = $objMdl->getSubData();
        $objFormParam->setParam($arrConfig['subs']);
        if ($this->getMode() != '' && SC_Utils_Ex::isBlank($_POST['subs_order_cancel_enable'])
                && $arrConfig['subs']['subs_order_cancel_enable'] == '1') {
            $_POST['subs_order_cancel_enable'] = '0';
        }
        if ($this->getMode() != '' && SC_Utils_Ex::isBlank($_POST['subs_after_payment'])
                && $arrConfig['subs']['subs_after_payment'] == '1') {
            $_POST['subs_after_payment'] = '0';
        }
        if ($this->getMode() != '' && SC_Utils_Ex::isBlank($_POST['subs_block_refusal'])
                && $arrConfig['subs']['subs_block_refusal'] == '1') {
            $_POST['subs_block_refusal'] = '0';
        }
        $objFormParam->setParam($_POST);
        $objFormParam->convParam();

        switch ($this->getMode()) {
        case 'add_product_type':
                $this->lfAddProductType($_SESSION['member_id']);
                $masterData = new SC_DB_MasterData_Ex();
                $masterData->clearCache('mtb_product_type');
                $this->arrProductType = $masterData->getMasterData('mtb_product_type');
            break;
        case 'confirm':
            $this->arrErr = $this->lfCheckError($objFormParam);
            if(SC_Utils_Ex::isBlank($this->arrErr)) {
                $arrParam = $objFormParam->getHashArray();
                $this->lfRegistData($arrParam);
                $this->tpl_onload = "window.alert('正常に更新されました。');";
                $arrConfig = $objMdl->getSubData();
                $objFormParam->setParam($arrConfig['subs']);
            }
            break;
        default:
            break;
        }

        $this->arrForm = $objFormParam->getFormParamList();
    }

    /**
     * デストラクタ.
     *
     * @return void
     */
    function destroy() {
        parent::destroy();
    }

    /**
     * パラメーター初期化
     *
     * @return void
     */
    function lfInitParam(&$objFormParam) {
        $objFormParam->addParam("定期購入の表示名称", "subs_product_name", STEXT_LEN, 'KVa', array("MAX_LENGTH_CHECK", "EXIST_CHECK"));
        $objFormParam->addParam("定期購入用の商品種別", "subs_product_type", INT_LEN, 'n', array("EXIST_CHECK", "MAX_LENGTH_CHECK"));
        $objFormParam->addParam("キャンセル機能の有効化", "subs_order_cancel_enable", INT_LEN, 'n', array("NUM_CHECK", "MAX_LENGTH_CHECK"));
        $objFormParam->addParam("定期購入初回締め切り日", "subs_order_limit_day", INT_LEN, 'n', array("NUM_CHECK", "MAX_LENGTH_CHECK"));
        $objFormParam->addParam("SFTPホストアドレス", 'subs_ftran_host', STEXT_LEN, 'a', array("GRAPH_CHECK", "MAX_LENGTH_CHECK"));
        $objFormParam->addParam("SFTPログインID", 'subs_ftran_id', STEXT_LEN, 'a', array("GRAPH_CHECK", "MAX_LENGTH_CHECK"));
        $objFormParam->addParam("SFTPパスワード", 'subs_ftran_password', STEXT_LEN, 'a', array("GRAPH_CHECK", "MAX_LENGTH_CHECK"));

        $objFormParam->addParam("売上結果反映:売上後の受注状況", 'uri_ok_status', INT_LEN, 'n', array("NUM_CHECK","MAX_LENGTH_CHECK"), ORDER_NEW);
        $objFormParam->addParam("売上結果反映:失敗時の受注状況", 'uri_ng_status', INT_LEN, 'n', array("NUM_CHECK","MAX_LENGTH_CHECK"), ORDER_CANCEL);

        $objFormParam->addParam("後払い方式で運用する", "subs_after_payment", INT_LEN, 'n', array("NUM_CHECK", "MAX_LENGTH_CHECK"));
        $objFormParam->addParam("退会制限機能", "subs_block_refusal", INT_LEN, 'n', array("NUM_CHECK", "MAX_LENGTH_CHECK"));
    }

    /**
     * 入力値チェック
     *
     * @return array()
     */
    function lfCheckError(&$objFormParam) {
        $arrParam = $objFormParam->getHashArray();
        $objErr = new SC_CheckError_Ex($arrParam);
        $objErr->arrErr = $objFormParam->checkError();
        if ($arrParam['subs_order_limit_day'] > 31 || $arrParam['subs_order_limit_day'] < 1) {
            $objErr->arrErr['subs_order_limit_day'] = '※ 1～31日の範囲で指定して下さい。';
        }
        if (!SC_Util_PG_MULPAY_Ex::isRegistCardPaymentEnable()) {
            $objErr->arrErr['subs_product_name'] = '※ 設定エラー　登録済みクレジットカード決済を有効にする必要があります。';
        }
        return $objErr->arrErr;
    }

    /**
     * プロダクトタイプ追加
     *
     * @param $member_id 管理者ID
     * @return array()
     */
    function lfAddProductType($member_id) {
        $objMdl =& SC_Mdl_PG_MULPAY_Ex::getInstance();
        $objQuery =& SC_Query_Ex::getSingletonInstance();
        $objQuery->begin();
        $id = $objQuery->max('id', 'mtb_product_type');
        $rank = $objQuery->max('rank', 'mtb_product_type');
        $arrData = array( 'id' => $id + 1, 'name' => '定期購入対応商品' , 'rank' => $rank + 1);
        $objQuery->insert('mtb_product_type', $arrData);

        // 配送情報登録
        $sqlval = array();
        $sqlval['name'] = '定期購入配送業者';
        $sqlval['service_name'] = '定期購入配送';
        $sqlval['remark'] = '定期購入の配送用業者です。';
        $sqlval['product_type_id'] = $id + 1;
        $sqlval['creator_id'] = $member_id;
        $sqlval['update_date'] = 'now()';

        $deliv_id = $objQuery->nextVal('dtb_deliv_deliv_id');
        $sqlval['deliv_id'] = $deliv_id;
        $sqlval['rank'] = $objQuery->max('rank', "dtb_deliv") + 1;
        $sqlval['create_date'] = 'now()';
        $objQuery->insert("dtb_deliv", $sqlval);

        foreach ($this->arrPref as $pref_id => $pref_name) {
            $sqlval = array();
            $sqlval['deliv_id'] = $deliv_id;
            $sqlval['fee_id'] = $pref_id;
            $sqlval['fee'] = '0';
            $sqlval['pref'] = $pref_id;
            $objQuery->insert("dtb_delivfee", $sqlval);
        }

        // 支払い方法
        $sqlval = array();
        $sqlval['deliv_id'] = $deliv_id;
        $arrPaymentIds = $objQuery->getCol('payment_id', 'dtb_payment', "module_code = ? and (memo03 = ? OR memo03 = ?) and del_flg = 0", array($objMdl->getCode(true), MDL_PG_MULPAY_PAYID_CREDIT, MDL_PG_MULPAY_PAYID_REGIST_CREDIT));

        $counter = 1;
        foreach($arrPaymentIds as $payment_id) {
            $sqlval['payment_id'] = $payment_id;
            $sqlval['rank'] = $counter;
            $objQuery->insert('dtb_payment_options', $sqlval);
            $counter ++;
        }

        $objQuery->commit();
    }

    function lfRegistData($arrParam) {
        $objMdl =& SC_Mdl_PG_MULPAY_Ex::getInstance();
        if (SC_Utils_Ex::isBlank($arrParam['subs_order_cancel_enable'])) {
            $arrParam['subs_order_cancel_enable'] = '0';
        }
        if (SC_Utils_Ex::isBlank($arrParam['subs_after_payment'])) {
            $arrParam['subs_after_payment'] = '0';
        }
        $objMdl->registerSubData($arrParam, 'subs');
    }
}

