<?php
/*
 * Copyright(c) 2012 GMO Payment Gateway, Inc. All rights reserved.
 * http://www.gmo-pg.com/
 */

// {{{ requires
require_once(MODULE_REALDIR . 'mdl_pg_mulpay/inc/include.php');
require_once(CLASS_EX_REALDIR . "page_extends/admin/LC_Page_Admin_Ex.php");
require_once(MDL_PG_MULPAY_CLASSEX_PATH . "util_extends/SC_Util_PG_MULPAY_Ex.php");
require_once(MDL_PG_MULPAY_CLASSEX_PATH . 'util_extends/SC_Util_GetErrorMsg_Ex.php');
set_include_path(get_include_path() . PATH_SEPARATOR . MDL_PG_MULPAY_MODULE_PATH . 'phpseclib');
include('Net/SFTP.php');

if (!defined('MDL_PG_MULPAY_PAYID_BANKTRANS')) {
    define('MDL_PG_MULPAY_PAYID_BANKTRANS', '100');
}

if (!defined('MDL_PG_MULPAY_SUBS_STATUS_END')) {
    define('MDL_PG_MULPAY_SUBS_STATUS_END', '9');      // 終了
}

define ('PLG_SUBS_CSV_PATH', CSV_TEMP_REALDIR . 'plg_pgsubs/');

define('NET_SFTP_LOGGING', NET_SFTP_LOG_COMPLEX);
/**
 *
 */
class LC_Page_Admin_Order_PgSubs_Payment extends LC_Page_Admin_Ex {

    // }}}
    // {{{ functions

    /**
     * Page を初期化する.
     *
     * @return void
     */
    function init() {
        parent::init();
        $this->tpl_mainpage = PLUGIN_UPLOAD_REALDIR . 'PgSubs/templates/admin/subs_payment.tpl';
        $this->tpl_pager = 'pager.tpl';
        $this->tpl_mainno = 'order';
        $this->tpl_maintitle = '受注管理';
        $this->tpl_subtitle = '定期購入課金管理';
        $masterData = new SC_DB_MasterData_Ex();
        $this->arrORDERSTATUS = array(MDL_PG_MULPAY_SUBS_STATUS_NEW => '新規依頼',
                                        MDL_PG_MULPAY_SUBS_STATUS_WAIT => '次回待機（キャンセル可)',
                                        MDL_PG_MULPAY_SUBS_STATUS_WAIT_NOCANCEL => '次回待機（キャンセル不可)',
                                        MDL_PG_MULPAY_SUBS_STATUS_CANCEL_REQUEST => 'キャンセル依頼中',
                                        MDL_PG_MULPAY_SUBS_STATUS_CANCEL => 'キャンセル受付済',
                                        MDL_PG_MULPAY_SUBS_STATUS_PENDING => '売上処理中',
                                        MDL_PG_MULPAY_SUBS_STATUS_SALES => '売上成功',
                                        MDL_PG_MULPAY_SUBS_STATUS_SALES_FAIL => '売上失敗',
                                        MDL_PG_MULPAY_SUBS_STATUS_END => '終了',
                                        );

        $this->arrORDERSTATUS_COLOR = $masterData->getMasterData("mtb_order_status_color");
        $this->arrPageMax = $masterData->getMasterData("mtb_page_max");

        // 支払い方法の取得
        $this->arrPayments = SC_Helper_DB_Ex::sfGetIDValueList("dtb_payment", "payment_id", "payment_method");

        $objDate = new SC_Date_Ex(date('Y') - 1, date('Y') +1);
        $this->arrYear = $objDate->getYear('', false);
        $this->arrMonth = $objDate->getMonth();
        $this->arrDay = $objDate->getDay();

        $this->httpCacheControl('nocache');
    }

    /**
     * Page のプロセス.
     *
     * @return void
     */
    function process() {
        $this->action();
        $this->sendResponse();
    }

    /**
     * Page のアクション.
     *
     * @return void
     */
    function action() {
        $objFormParam = new SC_FormParam_Ex();
        $this->lfInitParam($objFormParam);
        $objFormParam->setParam($_POST);
        $objFormParam->convParam();
        $this->arrForm = $objFormParam->getFormParamList();

        switch($this->getMode()) {
        case 'commit':
            $this->tpl_error_commit = $this->lfDoCommit($objFormParam);
            break;
        case 'reversal':
            $this->tpl_error_reversal = $this->lfDoReversal($objFormParam);
            break;
        case 'commit_result':
            $this->tpl_error_commit_result = $this->lfDoCommitResult($objFormParam);
            break;
        case 'commit_result_file':
            $this->tpl_error_commit_result_file = $this->lfDoCommitResultFile($objFormParam);
            break;
        case 'reversal_result':
            $this->tpl_error_reversal_result = $this->lfDoReversalResult($objFormParam);
            break;
        case 'reversal_result_file':
            $this->tpl_error_reversal_result_file = $this->lfDoReversalResultFile($objFormParam);
            break;
        case 'csv_download':
            $this->lfCsvDownload($objFormParam);
            break;
        default:
        }
        list($this->arrResults, $this->tpl_linemax, $this->arrPagenavi, $this->arrErr) = $this->lfGetList($objFormParam);
    }

    function getSubsConfig () {
        $objMdl =& SC_Mdl_PG_MULPAY_Ex::getInstance();
        $arrConfig = $objMdl->getSubData();
        return $arrConfig['subs'];
    }

    function lfGetList(&$objFormParam) {
        $table = 'plg_PgSubs_csv';
        $objQuery =& SC_Query_Ex::getSingletonInstance();

        $objFormParam->convParam();
        $objFormParam->trimParam();
        $arrErr = $objFormParam->checkError();

        $tpl_linemax = $objQuery->count($table);

        $page_max = SC_Utils_Ex::sfGetSearchPageMax($objFormParam->getValue('search_page_max'));
        $objNavi = new SC_PageNavi_Ex($objFormParam->getValue('search_pageno'),
                                   $tpl_linemax, $page_max,
                                   'fnNaviSearchPage', NAVI_PMAX);
        $arrPagenavi = $objNavi->arrPagenavi;

        $objQuery->setOrder('subs_csv_id DESC');
        $objQuery->setLimitOffset($page_max, $objNavi->start_row);
        $arrData = $objQuery->select('*', $table);
        foreach ($arrData as $key => $data) {
            $arrData[$key]['is_file'] = is_file(PLG_SUBS_CSV_PATH . $arrData[$key]['filename']);
        }
        return array($arrData, $tpl_linemax, $arrPagenavi, $arrErr);
    }

    function lfCsvDownload(&$objFormParam) {
        $objFormParam->convParam();
        $objFormParam->trimParam();
        $arrErr = $objFormParam->checkError();
        if (!SC_Utils_Ex::isBlank($arrErr)) {
            return $arrErr;
        }
        $csv_id = $objFormParam->getValue('subs_csv_id');
        $objQuery =& SC_Query_Ex::getSingletonInstance();
        $arrData = $objQuery->getRow('*', 'plg_PgSubs_csv', 'subs_csv_id = ?', array($csv_id));
        if (SC_Utils_Ex::isBlank($arrData)) {
            return false;
        }
        if (is_file(PLG_SUBS_CSV_PATH . $arrData['filename'])) {
            $objFileMgr = new SC_Helper_FileManager_Ex();
            $objFileMgr->sfDownloadFile(PLG_SUBS_CSV_PATH . $arrData['filename']);
            SC_Response_Ex::actionExit();
        } else {
            return '※ 対象のファイルが見つかりません。';
        }
    }

    function lfDoReversal(&$objFormParam) {
        $objMdl =& SC_Mdl_PG_MULPAY_Ex::getInstance();
        $arrConfig = $objMdl->getUserSettings();
        $objQuery =& SC_Query_Ex::getSingletonInstance();
        if ($objQuery->count('dtb_customer', 'del_flg = 0 AND status = 2') == 0) {
            return '洗替対象の会員データがありません。';
        }

        $filepath = PLG_SUBS_CSV_PATH;
        if (!is_dir($filepath)) {
            mkdir($filepath);
        }
        $month_number = $this->lfGetLastCSVNumber('会員洗替');
        $month_number++;
        if ($month_number > 99) {
            $month_number = 1;
        }
        $month_number = sprintf('%02d', $month_number);
        $filename = 'arai' . $arrConfig['ShopID'] . date("Ym") . $month_number;
        $filepath .= $filename . '.csv';
        $counter = $this->lfDoOutputArai($filepath);
        if (!$counter) {
            return 'CSVファイル出力エラー。正常に終了しませんでした。';
        }
        $result = $this->checkSFTPConnect();
        if ($result === null) { // Dont have sftp config
            $filename = $filename . '.tar.gz';
        } else {
            $filename = $filename . '.csv';
        }
        $this->lfSaveCSVHistory('会員洗替', $filename, $counter, $month_number);
        $msg = $counter . '件の洗替用ファイル' . $filename . 'を出力します。';

        $res = $this->lfUploadCSV($filepath, $filename, $filename . '.ok');
        $msg .= "\n" . $res;
        return $msg;
    }


    function lfDoReversalResultFile(&$objFormParam) {
        $target_key = 'upload_reversal_file';
        $objUpFile = new SC_UploadFile_Ex(CSV_TEMP_REALDIR, PLG_SUBS_CSV_PATH);
        $objUpFile->addFile('洗替結果ファイル', $target_key, array('csv', 'txt'), CSV_SIZE, true, 0, 0, false);
        $objUpFile->makeTempFile($target_key, false);
        $arrErr = $objUpFile->checkExists($target_key);
        if (!SC_Utils_Ex::isBlank($arrErr)) {
            return implode("\n", $arrErr);
        }
        // 一時ファイル名の取得
        $filepath = $objUpFile->getTempFilePath($target_key);
        if (SC_Utils_Ex::isBlank($filepath)) {
            return '※ ファイルがアップロードされていません。';
        }
        if (!is_file($filepath)) {
            return '※ ファイルが見つかりません。ファイルの作成権限が' . CSV_TEMP_REALDIR . 'にありません。';
        }
        copy ($filepath , PLG_SUBS_CSV_PATH . basename($filepath));
        if (!is_file(PLG_SUBS_CSV_PATH . basename($filepath))) {
            return '※ ファイルが見つかりません。ファイルの作成権限が' .PLG_SUBS_CSV_PATH . 'にありません。';
        }
        $res = $this->lfDoRAraiSave(basename($filepath));
        if ($res !== true) {
            return '※ ファイルの反映処理が中断されました。';
        }
        unlink($filepath);
        return '※ ファイル' . basename($filepath) .  ' を反映しました。';
    }

    function lfDoReversalResult(&$objFormParam) {
        $objMdl =& SC_Mdl_PG_MULPAY_Ex::getInstance();
        $arrConfig = $objMdl->getUserSettings();
        $objQuery =& SC_Query_Ex::getSingletonInstance();

        $downpath = PLG_SUBS_CSV_PATH;
        $fileprefix = 'Rarai' . $arrConfig['ShopID'];
        $sftp_log = $this->lfGetSftp($downpath, $fileprefix);
        if ($sftp_log !== true) {
            return $sftp_log;
        }

        $objFileMgr = new SC_Helper_FileManager_Ex();
        $arrFileList = $objFileMgr->sfGetFileList($downpath);
        $arrRaraiFile = array();
        foreach ($arrFileList as $arrFile) {
            if ($arrFile['is_dir'] == true) continue;

            if(substr($arrFile['file_name'], 0, strlen($fileprefix)) == $fileprefix
                    && substr($arrFile['file_name'], -3) != '.ok') {
                $arrRaraiFile[] = $arrFile['file_name'];
            }
        }
        if (SC_Utils_Ex::isBlank($arrRaraiFile)) {
            return '※ 処理対象となるファイルがありませんでした。';
        }

        $counter = 0;
        foreach ($arrRaraiFile as $filename) {
            $res = $this->lfDoRAraiSave($filename);
            if ($res === true) {
                $this->lfDelFileSftp($filename . '.ok');
                $this->lfDelFileSftp($filename);
                $counter ++;
            }
        }
        return '※ ' . $counter . '個のファイルを反映しました。';
    }

    function lfDoCommitResultFile(&$objFormParam) {
        $target_key = 'upload_commit_file';
        $objUpFile = new SC_UploadFile_Ex(CSV_TEMP_REALDIR, PLG_SUBS_CSV_PATH);
        $objUpFile->addFile('売上結果ファイル', $target_key, array('csv', 'txt'), CSV_SIZE, true, 0, 0, false);
        $objUpFile->makeTempFile($target_key, false);
        $arrErr = $objUpFile->checkExists($target_key);
        if (!SC_Utils_Ex::isBlank($arrErr)) {
            return implode("\n", $arrErr);
        }
        // 一時ファイル名の取得
        $filepath = $objUpFile->getTempFilePath($target_key);
        if (SC_Utils_Ex::isBlank($filepath)) {
            return '※ ファイルがアップロードされていません。';
        }
        if (!is_file($filepath)) {
            return '※ ファイルが見つかりません。ファイルの作成権限が' . CSV_TEMP_REALDIR . 'にありません。';
        }
        copy ($filepath , PLG_SUBS_CSV_PATH . basename($filepath));
        if (!is_file(PLG_SUBS_CSV_PATH . basename($filepath))) {
            return '※ ファイルが見つかりません。ファイルの作成権限が' .PLG_SUBS_CSV_PATH . 'にありません。';
        }

        $res = $this->lfDoRUriSave(basename($filepath));
        if ($res !== true) {
            return '※ ファイルの反映処理が中断されました。';
        }
        unlink($filepath);
        return '※ ファイル' . basename($filepath) .  ' を反映しました。';
    }

    function lfDoCommitResult(&$objFormParam) {
        $objMdl =& SC_Mdl_PG_MULPAY_Ex::getInstance();
        $arrConfig = $objMdl->getUserSettings();
        $objQuery =& SC_Query_Ex::getSingletonInstance();

        $downpath = PLG_SUBS_CSV_PATH;
        $fileprefix = 'Ruri' . $arrConfig['ShopID'];
        $sftp_log = $this->lfGetSftp($downpath, $fileprefix);
        if ($sftp_log !== true) {
            return $sftp_log;
        }
        
        $objFileMgr = new SC_Helper_FileManager_Ex();
        $arrFileList = $objFileMgr->sfGetFileList($downpath);
        $arrRuriFile = array();
        foreach ($arrFileList as $arrFile) {
            if ($arrFile['is_dir'] == true) continue;

            if (substr($arrFile['file_name'], 0, strlen($fileprefix)) == $fileprefix
                    && substr($arrFile['file_name'], -3) != '.ok') {

                $arrRuriFile[] = $arrFile['file_name'];
            }
        }
        if (SC_Utils_Ex::isBlank($arrRuriFile)) {
            return '※ 処理対象となるファイルがありませんでした。';
        }

        $counter = 0;
        foreach ($arrRuriFile as $filename) {
            $res = $this->lfDoRUriSave($filename);
            if ($res === true) {
                $this->lfDelFileSftp($filename . '.ok');
                $this->lfDelFileSftp($filename);
                $counter ++;
            }
        }
        return '※ ファイル「' . basename($filename) .  '」を反映しました。';
    }

    function lfDoRUriSave($filename) {
        $arrErr = array();
        if (substr($filename, -3) == '.ok') {
            return false;
        }
        if ($this->lfIsSaveCSVFile($filename)) {
            return false;
        }
        $objMdl =& SC_Mdl_PG_MULPAY_Ex::getInstance();
        $arrSubsConfig = $this->getSubsConfig();

        $file_path = PLG_SUBS_CSV_PATH . $filename;

        $objMdl->printLog($file_path . 'を読み込みます。');

        $is_extract = false;
        if (substr($filename, -6) == 'tar.gz') {
            $tar = new Archive_Tar($file_path, true);
            $result = $tar->extractModify(PLG_SUBS_CSV_PATH);
            $msg = 'extract:' . PLG_SUBS_CSV_PATH . $filename;
            $objMdl->printLog($msg);
            $is_extract = true;
            $tmp_filename = str_replace('tar.gz', 'txt', $filename);
            if (is_file(PLG_SUBS_CSV_PATH . $tmp_filename)) {
                $file_path = PLG_SUBS_CSV_PATH . $tmp_filename;
            } else {
                $tmp_filename = str_replace('tar.gz', 'csv', $filename);
                if (is_file(PLG_SUBS_CSV_PATH . $tmp_filename)) {
                    $file_path = PLG_SUBS_CSV_PATH . $tmp_filename;
                } else {
                    $tmp_filename = str_replace('tar.gz', '', $filename);
                    if (is_file(PLG_SUBS_CSV_PATH . $tmp_filename)) {
                        $file_path = PLG_SUBS_CSV_PATH . $tmp_filename;
                    }
                }
            }
        }

        $fp = fopen($file_path, 'r');
        if ($fp === FALSE) {
            $msg = $filename . 'が開けませんでした。';
            $objMdl->printLog($msg);
            return false;
        }
        $line = 0;
        $colmax = 19;
        $counter = 0;
        $arrKeys = array('ShopID',  //1
                         'MemberID', //2 customer_id
                         'CardSeq', //3
                         'TradeType',//4
                         'TranDate',//5
                         'OrderID',//6
                         'ProdCode',//7
                         'Amount',//8
                         'Tax',//9
                         'Method',//10
                         'spare1',//11
                         'spare2',//12
                         'spare3',//13
                         'ReqNo', //14
                         'ClientField1', //15 order_id
                         'Approve',//16
                         'ResultCode', //17
                         'Forward', //18
                         'ErrInfo', // 19
                );

        $objPurchase = new SC_Helper_Purchase_Ex();
        $objQuery =& SC_Query_Ex::getSingletonInstance();
        $objQuery->begin();

        while (($arrVals = fgetcsv($fp, CSV_LINE_MAX, ',')) !== FALSE) {
            $line ++;
            $colnum = count($arrVals);
            if ($colnum != $colmax) {
                $msg = $line . '行目の項目数が' . $colmax . 'ではありません。';
                $objMdl->printLog($msg);
                $arrErr[] = $msg;
                continue;
            }
            $arrData = array_combine($arrKeys, $arrVals);
            $arrData['ErrCode'] = substr($arrData['ErrInfo'], 0, 3);
            $arrData['action_status'] = MDL_PG_MULPAY_ACTION_STATUS_RECV_NOTICE;
            $arrData['pay_status'] = MDL_PG_MULPAY_PAY_STATUS_REQUEST_SUCCESS;

            $arrOrder = SC_Util_PG_MULPAY_Ex::getOrderPayData($arrData['ClientField1']);
            SC_Util_PG_MULPAY_Ex::setOrderPayData($arrOrder, $arrData);

            if ($arrOrder['customer_id'] != $arrData['MemberID']
//                || $arrOrder['status'] != ORDER_PAY_WAIT
                || $arrOrder['del_flg'] != '0'
                || $arrOrder[MDL_PG_MULPAY_ORDER_COL_SPFLG] != 'subscription'
                || $arrOrder[MDL_PG_MULPAY_ORDER_COL_SPDATA] != MDL_PG_MULPAY_SUBS_STATUS_PENDING
                || $arrOrder['payment_total'] != $arrData['Amount']
               ) {
                $msg = $line . '行目 注文番号:' . $arrOrder['order_id'] . ' データ不一致。';
                $arrErr[] = $msg;
                $objMdl->printLog($msg . print_r($arrData,true));
                continue;
            }

            // 結果処理

            $sqlval = array();
            $arrPayData = array();
            $arrSubsData = array();
            $new_status = null;
            switch($arrData['ResultCode']) {
                case '2':   // オーソリ時エラー
                case '3':   // 売上処理時エラー
                    $msg = $this->createErrCode($arrData);
                case '1':   // アップロード時エラー
                    $new_status = ORDER_CANCEL;
                    $msg = '売上アップロード時エラー';
                    $sqlval[MDL_PG_MULPAY_ORDER_COL_SPDATA] = MDL_PG_MULPAY_SUBS_STATUS_SALES_FAIL;
                    $sqlval['note'] = $arrOrder['note'] . "\n" . $msg;
                    $arrPayData['action_status'] = MDL_PG_MULPAY_ACTION_STATUS_EXEC_FAIL;
                    $arrPayData['pay_status'] = MDL_PG_MULPAY_PAY_STATUS_FAIL;
                    $arrSubsData['status'] = MDL_PG_MULPAY_SUBS_STATUS_SALES_FAIL;
                    break;
                case '0':   // 成功
                    $new_status = ORDER_NEW;
                    $sqlval[MDL_PG_MULPAY_ORDER_COL_SPDATA] = MDL_PG_MULPAY_SUBS_STATUS_SALES;
                    $sqlval['payment_date'] = 'CURRENT_TIMESTAMP';
                    $arrPayData['action_status'] = MDL_PG_MULPAY_ACTION_STATUS_EXEC_SUCCESS;
                    $arrPayData['pay_status'] = MDL_PG_MULPAY_PAY_STATUS_CAPTURE;
                    $arrSubsData['status'] = MDL_PG_MULPAY_SUBS_STATUS_SALES;
                    break;
                default:
                    $new_status = ORDER_CANCEL;
                    $msg = '未知の売上処理結果:' . $arrData['ResultCode'];
                    $sqlval[MDL_PG_MULPAY_ORDER_COL_SPDATA] = MDL_PG_MULPAY_SUBS_STATUS_SALES_FAIL;
                    $sqlval['note'] = $arrOrder['note'] . "\n" . $msg;
                    $arrPayData['action_status'] = MDL_PG_MULPAY_ACTION_STATUS_EXEC_FAIL;
                    $arrPayData['pay_status'] = MDL_PG_MULPAY_PAY_STATUS_FAIL;
                    $arrSubsData['status'] = MDL_PG_MULPAY_SUBS_STATUS_SALES_FAIL;
                    break;
            }

            if (isset($arrSubsConfig['uri_ok_status']) && $new_status == ORDER_NEW) {
                $new_status = $arrSubsConfig['uri_ok_status'];
                if (SC_Utils_Ex::isBlank($new_status)) {
                    $new_status = null;
                }
            } else if (isset($arrSubsConfig['uri_ng_status']) && $new_status = ORDER_CANCEL) {
                $new_status = $arrSubsConfig['uri_ng_status'];
                if (SC_Utils_Ex::isBlank($new_status)) {
                    $new_status = null;
                }
            }

            // 結果に基づいてデータ反映
            $objPurchase->sfUpdateOrderStatus($arrOrder['order_id'], $new_status, null, null, $sqlval);
            $arrPayData['order_status'] = $new_status;
            SC_Util_PG_MULPAY_Ex::setOrderPayData($arrOrder, $arrPayData);
            $objQuery->update('plg_PgSubs_order', $arrSubsData, 'subs_order_id = ?', array($arrOrder['subs_order_id']));

            $counter ++;
        }
        fclose($fp);

        if (SC_Utils_Ex::isBlank($arrErr)) {
            $msg = '';
        } else {
            $msg = implode("\n", $arrErr);
        }

        $this->lfSaveCSVHistory('売上結果反映', $filename, $counter, $msg);

        if ($is_extract && $tmp_filename && is_file(PLG_SUBS_CSV_PATH . $tmp_filename)) {
            unlink (PLG_SUBS_CSV_PATH . $tmp_filename);
        }

        $objQuery->commit();
        return true;
    }

    function lfDoRAraiSave($filename) {
        if (substr($filename, -3) == '.ok') {
            return false;
        }
        if($this->lfIsSaveCSVFile($filename)) {
            return false;
        }

        $objMdl =& SC_Mdl_PG_MULPAY_Ex::getInstance();
        $file_path = PLG_SUBS_CSV_PATH . $filename;
        $objMdl->printLog($file_path . 'を読み込みます。');

        $is_extract = false;
        if (substr($filename, -6) == 'tar.gz') {
            $tar = new Archive_Tar($file_path, true);
            $result = $tar->extractModify(PLG_SUBS_CSV_PATH);
            $msg = 'extract:' . PLG_SUBS_CSV_PATH . $filename;
            $objMdl->printLog($msg);
            $is_extract = true;
            $tmp_filename = str_replace('tar.gz', 'txt', $filename);
            if (is_file(PLG_SUBS_CSV_PATH . $tmp_filename)) {
                $file_path = PLG_SUBS_CSV_PATH . $tmp_filename;
            } else {
                $tmp_filename = str_replace('tar.gz', 'csv', $filename);
                if (is_file(PLG_SUBS_CSV_PATH . $tmp_filename)) {
                    $file_path = PLG_SUBS_CSV_PATH . $tmp_filename;
                } else {
                    $tmp_filename = str_replace('tar.gz', '', $filename);
                    if (is_file(PLG_SUBS_CSV_PATH . $tmp_filename)) {
                        $file_path = PLG_SUBS_CSV_PATH . $tmp_filename;
                    }
                }
            }
        }

        $fp = fopen($file_path, 'r');
        if ($fp === FALSE) {
            $msg = $filename . 'が開けませんでした。';
            $objMdl->printLog($msg);
            return false;
        }
        $objQuery =& SC_Query_Ex::getSingletonInstance();
        $objQuery->begin();
        $line = 0;
        $colmax = 13;
        $counter = 0;
        $arrKeys = array('MemberID',    //1
                         'CardSeq',     //2 物理モード値
                         'OldCardNo',   //3
                         'OldExpire',   //4
                         'BrandName',   //5
                         'RegistForword',   //6
                         'ResCode',     //7
                         'CardNo',      //8
                         'Expire',      //9
                         'Forword',     //10
                         'TranDate',    //11
                         'ClientField1',    //12
                         'Approve',     //13
                );

        while (($arrVals = fgetcsv($fp, CSV_LINE_MAX, ',')) !== FALSE) {
            $line ++;
            $colnum = count($arrVals);
            if ($colnum != $colmax) {
                $msg = $line . '行目の項目数が' . $colmax . 'ではありません。';
                $objMdl->printLog($msg);
                $arrErr[] = $msg;
                continue;
            }
            $arrData = array_combine($arrKeys, $arrVals);
            $arrData['return_date'] = date('Y-m-d H:i:s');
            
            // 洗替結果判断により処理
            $sqlval = array();
            $sqlval['plg_pgsubs_data'] = serialize($arrData);
            switch ($arrData['ResCode']) {
                case '3': // データエラー
                case '2': // 照合エラー
                case '1': // 有効性NG
                    $sqlval['plg_pgsubs_status'] = $arrData['ResCode'];
                    break;
                case '0': // 有効性OK
                default:
                    $sqlval['plg_pgsubs_status'] = '0';
                    break;
            }
            $table = 'dtb_customer';
            $where = 'customer_id = ? AND del_flg = 0';
            $objQuery->update($table, $sqlval, $where, array($arrData['MemberID']));

            $counter ++;
        }
        fclose($fp);

        if (SC_Utils_Ex::isBlank($arrErr)) {
            $msg = '';
        } else {
            $msg = implode("\n", $arrErr);
        }

        $this->lfSaveCSVHistory('洗替結果反映', $filename, $counter, $msg);
        if ($is_extract && $tmp_filename && is_file(PLG_SUBS_CSV_PATH . $tmp_filename)) {
            unlink (PLG_SUBS_CSV_PATH . $tmp_filename);
        }

        $objQuery->commit();
        return true;
    }

    function lfDoCommit(&$objFormParam) {
        $objFormParam->trimParam();
        $arrErr = $this->lfCheckError($objFormParam);
        if (!SC_Utils_Ex::isBlank($arrErr)) {
            return implode("\n", $arrErr);
        }

        $objMdl =& SC_Mdl_PG_MULPAY_Ex::getInstance();
        $arrConfig = $objMdl->getUserSettings();
        
        $objQuery =& SC_Query_Ex::getSingletonInstance();
        $objQuery->begin();

        $arrOrderIds = $this->lfMakeOrder($objFormParam->getHashArray());
        if (SC_Utils_Ex::isBlank($arrOrderIds)) {
            $objQuery->commit();
            return '売上対象の状態の定期購入受注がありません。';
        }
        $filepath = PLG_SUBS_CSV_PATH;
        if (!is_dir($filepath)) {
            mkdir($filepath);
        }
        $month_number = $this->lfGetLastCSVNumber('受注売上');
        $month_number++;
        if ($month_number > 99) {
            $month_number = 1;
        }
        $month_number = sprintf('%02d', $month_number);
        $filename = 'uri' . $arrConfig['ShopID'] . date("Ym") . $month_number;
        $filepath .= $filename . '.csv';

        $counter = $this->lfDoOutputOrder($arrOrderIds, $filepath);
        if (!$counter) {
            $objQuery->rollback();
            return 'CSVファイル出力エラー。正常に終了しませんでした。';
        }
        $result = $this->checkSFTPConnect();
        if ($result === null) { // Dont have sftp config
            $filename = $filename . '.tar.gz';
        } else {
            $filename = $filename . '.csv';
        }
        $this->lfSaveCSVHistory('受注売上', $filename, $counter, $month_number);

        $objQuery->commit();
        $msg = $counter . '件の受注データを作成し、売上用ファイル' . $filename . 'を出力します。';

        $res = $this->lfUploadCSV($filepath, $filename, $filename . '.ok');
        $msg .= "\n" . $res;
        return $msg;
    }

    function lfDownloadOkFile($org_filename) {
        $filepath = PLG_SUBS_CSV_PATH . $org_filename . '.ok';
        touch($filepath);
        $res = $this->lfPostSftp('/', $filepath, PLG_SUBS_CSV_PATH);
        return $res === null ? true : false;
    }

    function lfUploadCSV($filepath, $pack_name, $is_ok_file = null) {
        $dir = dirname($filepath);
        chdir($dir);
        // Check config sftp 
        // In case not config or config wrong dont achive file
        $result = $this->checkSFTPConnect();
        if ($result === null) { // Have sftp config
            $tar = new Archive_Tar($pack_name, true);
            $res = $tar->create(array(basename($filepath)));
            if (!$res) {
                return '圧縮ファイルの作成に失敗しました。ファイル生成は完了しています。';
            }
        }
        $sendpath = '/';
        $res = $this->lfPostSftp($sendpath, $pack_name, $dir);
        if ($res === null) {
            if ($is_ok_file) {
                touch(PLG_SUBS_CSV_PATH . $is_ok_file);
                $res = $this->lfPostSftp($sendpath, $is_ok_file, PLG_SUBS_CSV_PATH);
                if ($res === null) {
                    return 'アップロードに成功しました。';
                } else {
                    return $res;
                }
            }
            return 'アップロードに成功しました。';
        }
        return $res;
    }

    function lfGetLastCSVNumber($type_str) {
        $objQuery =& SC_Query_Ex::getSingletonInstance();
        $target = date('Y-m') . '-01 00:00:00';
        $data = $objQuery->get('memo', 'plg_PgSubs_csv', 'type = ? AND create_date > ? ORDER BY subs_csv_id DESC LIMIT 1', array($type_str, $target));
        if (SC_Utils_Ex::isBlank($data)) {
            return 0;
        }
        return $data;
    }

    function lfSaveCSVHistory($type_str, $filename, $counter, $ext) {
        $objQuery =& SC_Query_Ex::getSingletonInstance();
        $csv_id = $objQuery->nextVal('plg_PgSubs_csv_subs_csv_id');
        $arrData = array( 'subs_csv_id' => $csv_id,
                          'type' => $type_str,
                          'filename' => $filename,
                          'line_num' => $counter,
                          'memo' => $ext,
                          'member_id' => $_SESSION['member_id'],
                          'create_date' => 'CURRENT_TIMESTAMP'
                          );
        $objQuery->insert('plg_PgSubs_csv', $arrData);
    }

    function lfIsSaveCSVFile($filename) {
        $objQuery =& SC_Query_Ex::getSingletonInstance();
        if ($objQuery->count('plg_PgSubs_csv', 'filename = ?', array($filename))) {
            return true;
        }
        return false;
    }

    function lfDoOutputArai($filepath) {
        $tmp_file = tempnam(CSV_TEMP_REALDIR, 'subs_arai');
        $fp = fopen($tmp_file, 'w+');
        $counter = 0;

        $objQuery =& SC_Query_Ex::getSingletonInstance();
        $arrCustomerIds = $objQuery->getCol('customer_id', 'dtb_customer', 'del_flg = 0 AND status = 2 AND plg_pgsubs_status = 0 AND EXISTS (SELECT customer_id FROM plg_PgSubs_order WHERE plg_PgSubs_order.customer_id = dtb_customer.customer_id AND plg_PgSubs_order.del_flg = 0)');
        foreach ($arrCustomerIds as $customer_id) {
            $arrData = array();
            $arrData[] = $customer_id;
            $arrData[] = '';
            $arrData[] = '';
            $res = $this->lfOutputCSV($arrData, $fp);
            if (!$res) {
                return false;
            }
            $counter ++;
        }
        fclose($fp);

        $res = copy($tmp_file, $filepath);
        if (!$res) {
            return false;
        }
        unlink($tmp_file);
        return $counter;
    }

    function lfDoOutputOrder($arrOrderIds, $filepath) {
        $objMdl =& SC_Mdl_PG_MULPAY_Ex::getInstance();
        $arrMdlSetting = $objMdl->getUserSettings();

        $tmp_file = tempnam(CSV_TEMP_REALDIR, 'subs_order_csv');
        $fp = fopen($tmp_file, 'w+');
        $counter = 0;
        $subs_counter = $objMdl->getSubData('subs_order_count');
        
        foreach ($arrOrderIds as $order_id) {
            $subs_counter ++;
            if ($subs_counter > 99999) {
                $subs_counter = 1;
            }
            $arrData = array();
            $arrOrder = SC_Helper_Purchase_Ex::getOrder($order_id);
            $arrData[] = $arrMdlSetting['ShopID'];  //1
            $arrData[] = $arrOrder['customer_id'];  //2
            $arrData[] = '';    //3
            $arrData[] = '0';   //4
            $arrData[] = date('Ymd', strtotime($arrOrder['create_date']));  //5
            $arrData[] = $arrOrder['order_id'] . '-' . date('dHis');    //6
            $arrData[] = '';    //7
            $arrData[] = $arrOrder['payment_total'];    //8
            $arrData[] = '';    //9
            $arrData[] = '1';   //10
            $arrData[] = '';    //11
            $arrData[] = '';    //12
            $arrData[] = '';    //13
            $arrData[] = sprintf('%05d', $subs_counter); //14
            $arrData[] = $arrOrder['order_id'];  //15
            $arrData[] = '';    //16
            $arrData[] = '';    //17
            $arrData[] = '';    //18
            $arrData[] = '';    //19
            $res = $this->lfOutputCSV($arrData, $fp);
            if (!$res) {
                return false;
            }
            $counter ++;
        }
        fclose($fp);
        $objMdl->registerSubData($subs_counter, 'subs_order_count');
        $res = copy($tmp_file, $filepath);
        if (!$res) {
            return false;
        }
        unlink($tmp_file);
        return $counter;
    }

    function lfOutputCSV($data, &$fp) {
        // 全てをダブルクォートで括る
        foreach ($data as $key => $val) {
            if (is_string($val)) {
                $data[$key] = '"' . preg_replace('/' . preg_quote('"') . '/', '""', $val) . '"';
            } else {
                $data[$key] = '""';
            }
        }
        $line = implode (',', $data);
        $line = mb_convert_encoding($line, 'SJIS-Win');
        $line .= "\r\n";
        fwrite($fp, $line);
        return true;
    }

    function lfMakeOrder($arrParam) {
        $objMdl =& SC_Mdl_PG_MULPAY_Ex::getInstance();
        $arrSubsConfig = $this->getSubsConfig();
        $limitday = $arrSubsConfig['subs_order_limit_day'];
        $limit_base_time = mktime(0,0,0,  $arrParam['order_month'] -1, $limitday + 1, $arrParam['order_year']);
        if (date('d', $limit_base_time) < $limitday) {
            $limit_base_time = mktime(0,0,0,  $arrParam['order_month'] , 1, $arrParam['order_year']);
        }

        $target_date = date('Y-m-d 00:00:00' , mktime(0,0,0,  $arrParam['order_month'] -1, $limitday + 1, $arrParam['order_year']));

        $arrNewOrderIds = array();
        $order_keys = array('name01', 'name02', 'kana01', 'kana02',
            'sex', 'zip01', 'zip02', 'pref', 'addr01', 'addr02',
            'tel01', 'tel02', 'tel03', 'fax01', 'fax02', 'fax03',
            'job', 'birth', 'email',
        );
        $detail_keys = array('product_id', 'product_class_id',
            'product_name', 'product_code', 'classcategory_name1',
            'classcategory_name2', 'point_rate', 'price', 'quantity',
            );

        $objPurchase = new SC_Helper_Purchase_Ex();

        $objQuery =& SC_Query_Ex::getSingletonInstance();
        $objQuery->begin();

        $objQuery->setOrder('customer_id');

        $cols = 'plg_PgSubs_order.subs_order_id, dtb_customer.customer_id';
        $from = 'plg_PgSubs_order left join dtb_customer on plg_PgSubs_order.customer_id = dtb_customer.customer_id';
        $where = '((plg_PgSubs_order.status = ? AND plg_PgSubs_order.create_date < ?) OR plg_PgSubs_order.status IN (?, ?)) AND plg_PgSubs_order.del_flg = 0 AND dtb_customer.del_flg = 0';
        $arrVal = array(MDL_PG_MULPAY_SUBS_STATUS_NEW, $target_date, MDL_PG_MULPAY_SUBS_STATUS_WAIT, MDL_PG_MULPAY_SUBS_STATUS_WAIT_NOCANCEL);

        $arrSubsOrders = $objQuery->select($cols, $from, $where, $arrVal);

        if (SC_Utils_Ex::isBlank($arrSubsOrders)) {
            $objQuery->rollback();
            return false;
        }

        foreach ($arrSubsOrders as $arrSubsOrder) {
            $arrOrder = array();
            $arrDetail = array();
            $arrShipping = array();

            $arrCustomer = SC_Helper_Customer_Ex::sfGetCustomerData($arrSubsOrder['customer_id']);
            $arrSubs = $objQuery->getRow('*', 'plg_PgSubs_order', 'subs_order_id = ?', array($arrSubsOrder['subs_order_id']));
            if (SC_Utils_Ex::isBlank($arrCustomer)) {
                continue;
            }

            // 受注IDの発行
            $order_id = $objQuery->nextVal('dtb_order_order_id');
            $arrOrder['order_id'] = $order_id;

            // 受注データの生成
            foreach ($order_keys as $key) {
                $arrOrder['order_' . $key] = $arrCustomer[$key];
            }
            $arrOrder['customer_id'] = $arrSubsOrder['customer_id'];
            $arrOrder['status'] = ORDER_PAY_WAIT;
            $arrOrder['del_flg'] = '0';
            $arrOrder['payment_id'] = $arrSubs['payment_id'];
            $arrOrder['deliv_id'] = $arrSubs['deliv_id'];
            $arrOrder['use_point'] = '0';
            $arrOrder['device_type_id'] = '0';
            $arrOrder[MDL_PG_MULPAY_ORDER_COL_SPFLG] = 'subscription';
            $arrOrder[MDL_PG_MULPAY_ORDER_COL_SPDATA] = MDL_PG_MULPAY_SUBS_STATUS_PENDING;
            
            // 受注明細の生成
            $arrDetail[0] = array();
            $arrDetail[0]['order_id'] = $order_id;
            foreach ($detail_keys as $key) {
                $arrDetail[0][$key] = $arrSubs[$key];
            }

            // 配送情報の生成
            $arrShipping[0] = array();
            $objPurchase->copyFromOrder($arrShipping[0], $arrOrder);
            // 手数料設定
            if (class_exists('SC_Helper_Payment_Ex')) {
                $arrPayment = SC_Helper_Payment_Ex::get($arrOrder['payment_id']);
            } else {
                $arrPayment = $objPurchase->getPaymentsByPaymentsId($arrOrder['payment_id']);
            }
            $arrOrder['charge'] = $arrPayment['charge'];
            
            // calculate
            // ポイント計算
            if (USE_POINT !== false) {
                $arrOrder['add_point'] = SC_Utils_Ex::sfPrePoint($arrDetail[0]['price'], $arrDetail[0]['point_rate']);
            }
            // 税計算
            if (class_exists('SC_Helper_TaxRule_Ex')) {
                $arrTaxRule = SC_Helper_TaxRule_Ex::getTaxRule($arrDetail[0]['price'], $arrDetail[0]['product_id'], $arrDetail[0]['product_class_id'], $arrOrder['order_pref'], $arrOrder['order_country_id']);
                $tax =  SC_Helper_TaxRule_Ex::calcTax($arrDetail[0]['price'], $arrTaxRule['tax_rate'], $arrTaxRule['tax_rule'], $arrTaxRule['tax_adjust']);
                $inc_tax = $arrDetail[0]['price'] + $tax;
                $arrDetail[0]['tax_rate'] = $arrTaxRule['tax_rate'];
                $arrDetail[0]['tax_rule'] = $arrTaxRule['tax_rule'];
                $arrDetail[0]['tax_adjust'] = $arrTaxRule['tax_adjust'];
            } else {
                $tax = SC_Helper_DB_Ex::sfTax($arrDetail[0]['price']);
                $inc_tax = SC_Helper_DB_Ex::sfCalcIncTax($arrDetail[0]['price']);
            }
            $arrOrder['tax'] = $tax * $arrDetail[0]['quantity'];
            $arrOrder['subtotal'] = $inc_tax * $arrDetail[0]['quantity'];
            // 送料
            $arrOrder['deliv_fee'] = 0;
            if (OPTION_PRODUCT_DELIV_FEE == 1) {
                // NOT SUPPORT
            }
            if (OPTION_DELIV_FEE == 1) {
                if (class_exists('SC_Helper_Delivery_Ex')) {
                    $arrOrder['deliv_fee'] += SC_Helper_Delivery_Ex::getDelivFee($arrOrder['order_pref'], $arrOrder['deliv_id']);
                } else {
                    $arrOrder['deliv_fee'] += SC_CartSession_Ex::sfGetDelivFee($arrOrder['order_pref'], $arrOrder['deliv_id']);
                }
            }
            if (DELIV_FREE_AMOUNT > 0) {
                if ($arrDetail[0]['quantity'] >= DELIV_FREE_AMOUNT) {
                    $arrOrder['deliv_fee'] = 0;
                }
            }
            $arrInfo = SC_Helper_DB_Ex::sfGetBasisData();
            if ($arrInfo['free_rule'] > 0) {
                if ($arrOrder['subtotal'] >= $arrInfo['free_rule']) {
                    $arrOrder['deliv_fee'] = 0;
                }
            }
            $arrOrder['total'] = $arrOrder['subtotal'];
            $arrOrder['total'] += $arrOrder['deliv_fee'];
            $arrOrder['total'] += $arrOrder['charge'];
            $arrOrder['total'] -= $arrOrder['discount'];
            $arrOrder['payment_total'] = $arrOrder['total'];
            // 誕生日ポイントは考慮しません

            // 各種保存処理
            $objPurchase->registerOrder($order_id, $arrOrder);
            $arrAddVal = array('create_date' =>  sprintf('%04d-%02d-%02d', $arrParam['order_year'], $arrParam['order_month'], $arrParam['order_day']));
            $objPurchase->sfUpdateOrderStatus($order_id, null, null, null, $arrAddVal);
            $objPurchase->registerOrderDetail($order_id, $arrDetail);
            $objPurchase->registerShipmentItem($order_id, '0', $arrDetail);
            $objPurchase->registerShipping($order_id, $arrShipping);

            SC_Customer_Ex::updateOrderSummary($arrOrder['customer_id']);

            // 定期購入情報の更新
            $arrVal = array();
            $arrVal['status'] = MDL_PG_MULPAY_SUBS_STATUS_PENDING;
            $arrVal['last_order_id'] = $order_id;
            $arrVal['order_count'] = $arrSubs['order_count'] + 1;
            $arrVal['update_date'] = 'CURRENT_TIMESTAMP';

            $objQuery->update('plg_PgSubs_order', $arrVal, 'subs_order_id = ?', array($arrSubsOrder['subs_order_id']));

            $arrPayData = array();
            $arrPayData['pay_mode'] = 'subscription';
            $arrPayData['Amount'] = $arrOrder['payment_total'];
            $arrPayData['JobCd'] = 'CAPTURE';
            $arrPayData['subs_order_id'] = $arrSubsOrder['subs_order_id'];
            $arrPayData['action_status'] = MDL_PG_MULPAY_ACTION_STATUS_EXEC_REQUEST;
            $arrPayData['pay_status'] = MDL_PG_MULPAY_PAY_STATUS_UNSETTLED;

            SC_Util_PG_MULPAY_Ex::setOrderPayData($arrOrder, $arrPayData);

            $arrNewOrderIds[] = $order_id;
        }

        $objQuery->commit();
        return $arrNewOrderIds;
    }

    /**
     * パラメーター情報の初期化を行う.
     *
     * @param SC_FormParam $objFormParam SC_FormParam インスタンス
     * @return void
     */
    function lfInitParam(&$objFormParam) {
        $objFormParam->addParam("表示件数", "search_page_max", INT_LEN, 'n', array("MAX_LENGTH_CHECK", "NUM_CHECK"));
        $objFormParam->addParam("ページ送り番号","search_pageno", INT_LEN, 'n', array("MAX_LENGTH_CHECK", "NUM_CHECK"));
        $objFormParam->addParam("CSV ID", "subs_csv_id", INT_LEN, 'n', array("MAX_LENGTH_CHECK", "NUM_CHECK"));
        $objFormParam->addParam("利用年", 'order_year', INT_LEN, 'n', array("MAX_LENGTH_CHECK", "NUM_CHECK"), date('Y'));
        $objFormParam->addParam("利用月", 'order_month', INT_LEN, 'n', array("MAX_LENGTH_CHECK", "NUM_CHECK"), date('n'));
        $objFormParam->addParam("利用日", 'order_day', INT_LEN, 'n', array("MAX_LENGTH_CHECK", "NUM_CHECK"), date('j'));
    }

    /**
     * 入力内容のチェックを行う.
     *
     * @param SC_FormParam $objFormParam SC_FormParam インスタンス
     * @return void
     */
    function lfCheckError(&$objFormParam) {
        $arrErr = $objFormParam->checkError();
        $objErr = new SC_CheckError_Ex($objFormParam->getHashArray());
        $objErr->doFunc(array('利用年月日', 'order_year', 'order_month', 'order_day'), array('CHECK_DATE'));
        if (!SC_Utils_Ex::isBlank($objErr->arrErr)) {
            $arrErr = array_merge($arrErr, $objErr->arrErr);
        }
        return $arrErr;
    }


    /*
     * デストラクタ.
     *
     * @return void
     */
    function destroy() {
        parent::destroy();
    }

    function lfDelFileSftp($filename) {
        $objMdl =& SC_Mdl_PG_MULPAY_Ex::getInstance();
        $arrSubsConfig = $this->getSubsConfig();
        $host = $arrSubsConfig['subs_ftran_host'];
        $objMdl->printLog('Connect SFTP host: ' . $host);
        $sftp = new Net_SFTP($host);
        if(!$sftp->login($arrSubsConfig['subs_ftran_id'] , $arrSubsConfig['subs_ftran_password'])){
            $log = $sftp->getSFTPLog();
            $objMdl->printLog($log);
            return '※サーバーへのSFTP接続に失敗しました。';
        }
        if(!$sftp->delete($filename)) {
            $log = $sftp->getSFTPLog();
            $objMdl->printLog($log);
            return '※ファイルの削除に失敗しました。';
        }
        $objMdl->printLog('SFTP delete:' . $filename);
        return true;
    }

    function lfDelSftp($fileprefix) {
        $objMdl =& SC_Mdl_PG_MULPAY_Ex::getInstance();
        $arrSubsConfig = $this->getSubsConfig();
        $host = $arrSubsConfig['subs_ftran_host'];
        $objMdl->printLog('Connect SFTP host: ' . $host);
        $sftp = new Net_SFTP($host);
        if(!$sftp->login($arrSubsConfig['subs_ftran_id'] , $arrSubsConfig['subs_ftran_password'])){
            $log = $sftp->getSFTPLog();
            $objMdl->printLog($log);
            return '※サーバーへのSFTP接続に失敗しました。';
        }
        $arrFiles = $sftp->nlist('.');
        $objMdl->printLog('SFTP list: ' . print_r($arrFiles,true));
        foreach($arrFiles as $file) {
            if(substr($file, 0, strlen($fileprefix)) == $fileprefix) {
                $sftp->delete($file);
            }
        }
        $arrFiles = $sftp->nlist('.');
        $objMdl->printLog('SFTP list: ' . print_r($arrFiles,true));
    }

    function lfGetSftp($downpath, $fileprefix) {
        $objMdl =& SC_Mdl_PG_MULPAY_Ex::getInstance();
        $arrSubsConfig = $this->getSubsConfig();

        $host = $arrSubsConfig['subs_ftran_host'];
        if (SC_Utils_Ex::isBlank($host)) {
            return '※ ホスト情報が設定されていません。';
        }

        $objMdl->printLog('Connect SFTP host: ' . $host);
        $sftp = new Net_SFTP($host);
        if(!$sftp->login($arrSubsConfig['subs_ftran_id'] , $arrSubsConfig['subs_ftran_password'])){
            $log = $sftp->getSFTPLog();
            $objMdl->printLog($log);
            return '※サーバーへのSFTP接続に失敗しました。';
        }
        $arrFiles = $sftp->nlist('.');
        $objMdl->printLog('SFTP list: ' . print_r($arrFiles,true));
        if(count($arrFiles) == 0) {
            return '※サーバー内にファイルがありません。';
        }
        $downcount = 0;
        foreach($arrFiles as $file) {
            if(substr($file, 0, strlen($fileprefix)) == $fileprefix) {
                if(!is_file($downpath . $file)) {
                    $objMdl->printLog('SFTP get: ' . $file);
                    if(!$sftp->get($file, $downpath . $file)) {
                        $log = $sftp->getSFTPLog();
                        $objMdl->printLog($log);
                        return '※ ファイルダウンロードエラーが発生しました。(' . $file . ')';
                    }
                    $downcount ++;
                }
            }
        }
        $objMdl->printLog('SFTP download get count: ' . $downcount);
        return true;
    }

    function lfPostSftp($sendpath, $filename, $filepath) {
        $objMdl =& SC_Mdl_PG_MULPAY_Ex::getInstance();
        $arrSubsConfig = $this->getSubsConfig();

        $host = $arrSubsConfig['subs_ftran_host'];
        if (SC_Utils_Ex::isBlank($host)) {
            return '※ ホスト情報が設定されていません。手動でアップロードをして下さい。';
        }

        $objMdl->printLog('Connect SFTP host: ' . $host);
        $sftp = new Net_SFTP($host);
        if(!$sftp->login($arrSubsConfig['subs_ftran_id'] , $arrSubsConfig['subs_ftran_password'])){
            $log = $sftp->getSFTPLog();
            $objMdl->printLog($log);
            return '※サーバーへのSFTP接続に失敗しました。';
        }
        $objMdl->printLog('SFTP put: ' . $filename . ' from: ' . $filepath);
        if(!$sftp->put($filename, $filepath . '/' . $filename, NET_SFTP_LOCAL_FILE )) {
            $log = $sftp->getSFTPLog();
            $objMdl->printLog($log);
            $objMdl->printLog(print_r($sftp->rawlist('.'),true));
            return '※サーバーへのSFTP転送に失敗しました。';
        }
        $objMdl->printLog(print_r($sftp->rawlist('.'),true));
        return null;
    }

    function createErrCode($arrRet) {
        $objErrMsg = new SC_Util_GetErrorMsg_Ex();
        $msg = '';
        $arrErrMsg = $objErrMsg->lfGetErrorInformation($arrRet['ErrInfo']);
        $error_text = SC_Utils_Ex::isBlank($arrErrMsg['message']) ? $arrErrMsg['context'] : $arrErrMsg['message'];
        $msg = $error_text . '(' . sprintf('%s-%s', $arrRet['ErrCode'], $arrRet['ErrInfo']) .')';
        return $msg;
    }
    
    /**
     * Check conection to sftp
     * @return error message if not conected
     */
    function checkSFTPConnect() 
    {
        $objMdl =& SC_Mdl_PG_MULPAY_Ex::getInstance();
        $arrSubsConfig = $this->getSubsConfig();
        $host = $arrSubsConfig['subs_ftran_host'];
        if (SC_Utils_Ex::isBlank($host)) {
            return '※ ホスト情報が設定されていません。手動でアップロードをして下さい。';
        }
        $sftp = new Net_SFTP($host);
        $objMdl->printLog('Connect SFTP host: ' . $host);
        if(!$sftp->login($arrSubsConfig['subs_ftran_id'] , $arrSubsConfig['subs_ftran_password'])){
            $log = $sftp->getSFTPLog();
            $objMdl->printLog($log);
            return '※サーバーへのSFTP接続に失敗しました。';
        }
        return null;
    }

}
