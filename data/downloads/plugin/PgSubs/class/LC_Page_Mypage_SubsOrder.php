<?php
/*
 * Copyright(c) 2012 GMO Payment Gateway, Inc. All rights reserved.
 * http://www.gmo-pg.com/
 */

// {{{ requires
require_once(MODULE_REALDIR . 'mdl_pg_mulpay/inc/include.php');
require_once CLASS_EX_REALDIR . 'page_extends/mypage/LC_Page_AbstractMypage_Ex.php';
require_once(MDL_PG_MULPAY_CLASSEX_PATH . "util_extends/SC_Util_PG_MULPAY_Ex.php");

if (!defined('MDL_PG_MULPAY_PAYID_BANKTRANS')) {
    define('MDL_PG_MULPAY_PAYID_BANKTRANS', '100');
}

if (!defined('MDL_PG_MULPAY_SUBS_STATUS_END')) {
    define('MDL_PG_MULPAY_SUBS_STATUS_END', '9');      // 終了
}


/**
 *
 */
class LC_Page_Mypage_SubsOrder extends LC_Page_AbstractMypage_Ex {

    /**
     * Page を初期化する.
     *
     * @return void
     */
    function init() {
        parent::init();
        $this->tpl_mypageno = 'subsorder';
        $this->tpl_subtitle = '定期購入履歴一覧';

        $this->httpCacheControl('nocache');
        $this->arrORDERSTATUS = array(MDL_PG_MULPAY_SUBS_STATUS_NEW => '新規依頼',
                                        MDL_PG_MULPAY_SUBS_STATUS_WAIT => '次回待機',
                                        MDL_PG_MULPAY_SUBS_STATUS_WAIT_NOCANCEL => '次回待機',
                                        MDL_PG_MULPAY_SUBS_STATUS_CANCEL_REQUEST => 'キャンセル依頼中',
                                        MDL_PG_MULPAY_SUBS_STATUS_CANCEL => 'キャンセル受付済',
                                        MDL_PG_MULPAY_SUBS_STATUS_PENDING => '売上処理中',
                                        MDL_PG_MULPAY_SUBS_STATUS_SALES => '売上成功',
                                        MDL_PG_MULPAY_SUBS_STATUS_SALES_FAIL => '売上失敗',
                                        MDL_PG_MULPAY_SUBS_STATUS_END => '終了',
                                        );
    }

    /**
     * Page のプロセス.
     *lo
     * @return void
     */
    function process() {
        parent::process();
    }

    /**
     * Page のAction.
     *
     * @return void
     */
    function action() {
        $objMdl =& SC_Mdl_PG_MULPAY_Ex::getInstance();
        $arrConfig = $objMdl->getSubData();
        $arrConfig = $arrConfig['subs'];
        $this->tpl_is_cancel = $arrConfig['subs_order_cancel_enable'];

        $objCustomer = new SC_Customer_Ex();
        $customer_id = $objCustomer->getValue('customer_id');

        //ページ送り用
        $this->objNavi = new SC_PageNavi_Ex($_REQUEST['pageno'],
                                            $this->lfGetSubsOrderHistory($customer_id),
                                            SEARCH_PMAX,
                                            'fnNaviPage',
                                            NAVI_PMAX,
                                            'pageno=#page#',
                                            SC_Display_Ex::detectDevice() !== DEVICE_TYPE_MOBILE);

        $this->arrOrder = $this->lfGetSubsOrderHistory($customer_id, $this->objNavi->start_row);

        switch ($this->getMode()) {
            case 'getList':
                echo SC_Utils_Ex::jsonEncode($this->arrOrder);
                SC_Response_Ex::actionExit();
                break;
            case 'cancel':
                $objFormParam = new SC_FormParam_Ex();
                $this->lfInitParam($objFormParam);
                $objFormParam->setParam($_POST);
                $this->arrErr = $objFormParam->checkError();
                if (SC_Utils_Ex::isBlank($this->arrErr) && $this->tpl_is_cancel == '1') {
                    $this->lfSetCancel($customer_id, $objFormParam->getHashArray());
                    SC_Response_Ex::reload();
                    SC_Response_Ex::actionExit();
                }
                break;
            default:
                break;
        }
        // 支払い方法の取得
        $this->arrPayment = SC_Helper_DB_Ex::sfGetIDValueList('dtb_payment', 'payment_id', 'payment_method');

        // 1ページあたりの件数
        $this->dispNumber = SEARCH_PMAX;
    }

    /**
     * デストラクタ.
     *
     * @return void
     */
    function destroy() {
        parent::destroy();
    }

    /**
     * パラメーター初期化
     *
     * @return void
     */
    function lfInitParam(&$objFormParam) {
        $objFormParam->addParam("管理番号", 'subs_order_id', INT_LEN, 'n', array("NUM_CHECK", "MAX_LENGTH_CHECK"));
    }

    /**
     * 入力値チェック
     *
     * @return array()
     */
    function lfCheckError(&$objFormParam) {
        $arrParam = $objFormParam->getHashArray();
        $objErr = new SC_CheckError_Ex($arrParam);
        $objErr->arrErr = $objFormParam->checkError();
        return $objErr->arrErr;
    }

    function lfSetCancel($customer_id, $arrParam) {
        $subs_order_id = $arrParam['subs_order_id'];

        $table = 'plg_PgSubs_order';
        $where = 'del_flg = 0 and customer_id = ? and status = ? and subs_order_id = ?';
        $arrWhereVal = array($customer_id, MDL_PG_MULPAY_SUBS_STATUS_WAIT, $arrParam['subs_order_id']);
        $arrUpdVal = array('status' => MDL_PG_MULPAY_SUBS_STATUS_CANCEL_REQUEST);

        $objQuery   = SC_Query_Ex::getSingletonInstance();
        $objQuery->update($table, $arrUpdVal, $where, $arrWhereVal);

    }

    function lfGetSubsOrderHistory($customer_id, $startno = -1) {
        $objQuery   = SC_Query_Ex::getSingletonInstance();
        $col = '*';
        $from = 'plg_PgSubs_order';
        $where = 'del_flg = 0 and customer_id = ?';
        $arrVal = array($customer_id);
        $order = 'subs_order_id DESC';
        if ($startno == -1) {
            return $objQuery->count($from, $where, $arrVal);
        }
        $objQuery->setLimitOffset(SEARCH_PMAX, $startno);
        $objQuery->setOrder($order);
        $arrList =  $objQuery->select($col, $from, $where, $arrVal);

        $objCustomer = new SC_Customer_Ex();
        foreach ($arrList as $key => $data) {
            if (class_exists("SC_Helper_TaxRule_Ex")) {
                $arrList[$key]['price_inctax'] = SC_Helper_TaxRule_Ex::sfCalcIncTax($data['price'], $data['product_id'], $data['product_class_id'], $objCustomer->getValue('pref'), $objCustomer->getValue('country_id'));
            } else {
                $arrList[$key]['price_inctax'] = SC_Helper_DB_Ex::sfCalcIncTax($data['price']);
            }
        }

        return $arrList;
    }
}

