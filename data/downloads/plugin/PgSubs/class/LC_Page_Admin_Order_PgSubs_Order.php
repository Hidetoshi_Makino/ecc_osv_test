<?php
/*
 * Copyright(c) 2012 GMO Payment Gateway, Inc. All rights reserved.
 * http://www.gmo-pg.com/
 */

// {{{ requires
require_once(MODULE_REALDIR . 'mdl_pg_mulpay/inc/include.php');
require_once(CLASS_EX_REALDIR . "page_extends/admin/LC_Page_Admin_Ex.php");
require_once(MDL_PG_MULPAY_CLASSEX_PATH . "util_extends/SC_Util_PG_MULPAY_Ex.php");

set_include_path(get_include_path() . PATH_SEPARATOR . MDL_PG_MULPAY_MODULE_PATH . 'phpseclib');
include('Net/SFTP.php');

if (!defined('MDL_PG_MULPAY_PAYID_BANKTRANS')) {
    define('MDL_PG_MULPAY_PAYID_BANKTRANS', '100');
}

if (!defined('MDL_PG_MULPAY_SUBS_STATUS_END')) {
    define('MDL_PG_MULPAY_SUBS_STATUS_END', '9');      // 終了
}


/**
 * 管理画面 定期購入受注管理クラス
 */
class LC_Page_Admin_Order_PgSubs_Order extends LC_Page_Admin_Ex {

    // }}}
    // {{{ functions

    /**
     * Page を初期化する.
     *
     * @return void
     */
    function init() {
        parent::init();
        $this->tpl_mainpage = PLUGIN_UPLOAD_REALDIR . 'PgSubs/templates/admin/subs_order.tpl';
        $this->tpl_pager = 'pager.tpl';
        $this->tpl_mainno = 'order';
        $this->tpl_maintitle = '受注管理';
        $this->tpl_subtitle = '定期購入受注管理';
        $masterData = new SC_DB_MasterData_Ex();
//        $this->arrORDERSTATUS = $masterData->getMasterData("mtb_order_status");
        $this->arrORDERSTATUS = array(MDL_PG_MULPAY_SUBS_STATUS_NEW => '新規依頼',
                                        MDL_PG_MULPAY_SUBS_STATUS_WAIT => '次回待機（キャンセル可)',
                                        MDL_PG_MULPAY_SUBS_STATUS_WAIT_NOCANCEL => '次回待機（キャンセル不可)',
                                        MDL_PG_MULPAY_SUBS_STATUS_CANCEL_REQUEST => 'キャンセル依頼中',
                                        MDL_PG_MULPAY_SUBS_STATUS_CANCEL => 'キャンセル受付済',
                                        MDL_PG_MULPAY_SUBS_STATUS_PENDING => '売上処理中',
                                        MDL_PG_MULPAY_SUBS_STATUS_SALES => '売上成功',
                                        MDL_PG_MULPAY_SUBS_STATUS_SALES_FAIL => '売上失敗',
                                        MDL_PG_MULPAY_SUBS_STATUS_END => '終了',
                                        );

        $this->arrORDERSTATUS_COLOR = $masterData->getMasterData("mtb_order_status_color");
        $this->arrSex = $masterData->getMasterData("mtb_sex");
        $this->arrPageMax = $masterData->getMasterData("mtb_page_max");

        $objDate = new SC_Date_Ex();
        // 登録・更新日検索用
        $objDate->setStartYear(RELEASE_YEAR);
        $objDate->setEndYear(DATE('Y'));
        $this->arrRegistYear = $objDate->getYear();
        // 月日の設定
        $this->arrMonth = $objDate->getMonth();
        $this->arrDay = $objDate->getDay();

        // 支払い方法の取得
        $this->arrPayments = SC_Helper_DB_Ex::sfGetIDValueList("dtb_payment", "payment_id", "payment_method");

        $this->httpCacheControl('nocache');
    }

    /**
     * Page のプロセス.
     *
     * @return void
     */
    function process() {
        $this->action();
        $this->sendResponse();
    }

    /**
     * Page のアクション.
     *
     * @return void
     */
    function action() {
        $objFormParam = new SC_FormParam_Ex();
        $this->lfInitParam($objFormParam);
        $objFormParam->setParam($_POST);
        $this->arrHidden = $objFormParam->getSearchArray();
        $this->arrForm = $objFormParam->getFormParamList();

        switch($this->getMode()) {
        // 削除
        case 'delete':
            $this->doDelete('subs_order_id = ?',
                            array($objFormParam->getValue('subs_order_id')));
            // 削除後に検索結果を表示するため breakしない
        case 'change_order':
            if ($this->getMode() == 'change_order') {
                $objFormParam->convParam();
                $objFormParam->trimParam();
                $this->arrErr = $this->lfCheckError($objFormParam);
                if (SC_Utils_Ex::isBlank($this->arrErr)) {
                    $this->changeSubsOrder($objFormParam->getHashArray());
                }
            }

        case 'change_subs_status':
            if ($this->getMode() == 'change_subs_status') {
                $objFormParam->convParam();
                $objFormParam->trimParam();
                $this->arrErr = $this->lfCheckError($objFormParam);
                if (SC_Utils_Ex::isBlank($this->arrErr)) {
                    $this->changeSubsOrderStatus($objFormParam->getValue('move'), $objFormParam->getValue('subs_change_status'));
                }
            }

        // 検索パラメーター生成後に処理実行するため breakしない
        case 'csv':
        // 検索パラメーターの生成
        case 'search':
            $objFormParam->convParam();
            $objFormParam->trimParam();
            $this->arrErr = $this->lfCheckError($objFormParam);

            if (count($this->arrErr) == 0) {
                // クエリの構築
                list($where, $arrval, $order) = $this->lfGetQueryParam($objFormParam);

                switch($this->getMode()) {
                // CSVを送信する。
                case 'csv':
                    $this->doOutputCSV($where, $arrval,$order);
                    exit;
                    break;
                // 検索実行
                default:
                    // 行数の取得
                    $this->tpl_linemax = $this->getNumberOfLines($where, $arrval);
                    // ページ送りの処理
                    $page_max = SC_Utils_Ex::sfGetSearchPageMax($objFormParam->getValue('search_page_max'));
                    // ページ送りの取得
                    $objNavi = new SC_PageNavi_Ex($this->arrHidden['search_pageno'],
                                               $this->tpl_linemax, $page_max,
                                               'fnNaviSearchPage', NAVI_PMAX);
                    $this->arrPagenavi = $objNavi->arrPagenavi;

                    // 検索結果の取得
                    $this->arrResults = $this->findOrders($where, $arrval,
                                                          $page_max, $objNavi->start_row, $order);
                }
            }
            break;
        default:
        }
    }

    /**
     * パラメーター情報の初期化を行う.
     *
     * @param SC_FormParam $objFormParam SC_FormParam インスタンス
     * @return void
     */
    function lfInitParam(&$objFormParam) {
        $objFormParam->addParam("注文番号1", "search_order_id1", INT_LEN, 'n', array("MAX_LENGTH_CHECK", "NUM_CHECK"));
        $objFormParam->addParam("注文番号2", "search_order_id2", INT_LEN, 'n', array("MAX_LENGTH_CHECK", "NUM_CHECK"));
        $objFormParam->addParam("対応状況", "search_order_status", INT_LEN, 'n', array("MAX_LENGTH_CHECK", "NUM_CHECK"));
        $objFormParam->addParam("洗替NGのみ表示", "search_arai_status", INT_LEN, 'n', array("MAX_LENGTH_CHECK", "NUM_CHECK"));
        $objFormParam->addParam("注文者 お名前", "search_order_name", STEXT_LEN, 'KVa', array("MAX_LENGTH_CHECK"));
        $objFormParam->addParam("注文者 お名前(フリガナ)", "search_order_kana", STEXT_LEN, 'KVCa', array("KANA_CHECK","MAX_LENGTH_CHECK"));
        $objFormParam->addParam("メールアドレス", "search_order_email", STEXT_LEN, 'KVa', array("MAX_LENGTH_CHECK"));
        $objFormParam->addParam('TEL', "search_order_tel", STEXT_LEN, 'KVa', array("MAX_LENGTH_CHECK"));
        $objFormParam->addParam("表示件数", "search_page_max", INT_LEN, 'n', array("MAX_LENGTH_CHECK", "NUM_CHECK"));
        // 受注日
        $objFormParam->addParam("開始年", "search_sorderyear", INT_LEN, 'n', array("MAX_LENGTH_CHECK", "NUM_CHECK"));
        $objFormParam->addParam("開始月", "search_sordermonth", INT_LEN, 'n', array("MAX_LENGTH_CHECK", "NUM_CHECK"));
        $objFormParam->addParam("開始日", "search_sorderday", INT_LEN, 'n', array("MAX_LENGTH_CHECK", "NUM_CHECK"));
        $objFormParam->addParam("終了年", "search_eorderyear", INT_LEN, 'n', array("MAX_LENGTH_CHECK", "NUM_CHECK"));
        $objFormParam->addParam("終了月", "search_eordermonth", INT_LEN, 'n', array("MAX_LENGTH_CHECK", "NUM_CHECK"));
        $objFormParam->addParam("終了日", "search_eorderday", INT_LEN, 'n', array("MAX_LENGTH_CHECK", "NUM_CHECK"));

        $objFormParam->addParam("購入商品","search_product_name",STEXT_LEN,'KVa',array("MAX_LENGTH_CHECK"));
        $objFormParam->addParam("購入商品コード","search_product_code",STEXT_LEN,'KVa',array("MAX_LENGTH_CHECK"));
        $objFormParam->addParam("ページ送り番号","search_pageno", INT_LEN, 'n', array("MAX_LENGTH_CHECK", "NUM_CHECK"));
        $objFormParam->addParam("受注ID", "subs_order_id", INT_LEN, 'n', array("MAX_LENGTH_CHECK", "NUM_CHECK"));

        $objFormParam->addParam("変更管理番号", "move", INT_LEN, 'n', array("MAX_LENGTH_CHECK", "NUM_CHECK"));
        $objFormParam->addParam("変更先ステータスID", "subs_change_status", INT_LEN, 'n', array("MAX_LENGTH_CHECK", "NUM_CHECK"));
        $objFormParam->addParam("変更ステータス", "status", INT_LEN, 'n', array("MAX_LENGTH_CHECK", "NUM_CHECK"));
        $objFormParam->addParam("変更購入単価", "price", INT_LEN, 'n', array("MAX_LENGTH_CHECK", "NUM_CHECK"));
        $objFormParam->addParam("変更数量", "quantity", INT_LEN, 'n', array("MAX_LENGTH_CHECK", "NUM_CHECK"));
    }


    /**
     * 入力内容のチェックを行う.
     *
     * @param SC_FormParam $objFormParam SC_FormParam インスタンス
     * @return void
     */
    function lfCheckError(&$objFormParam) {
        $objErr = new SC_CheckError_Ex($objFormParam->getHashArray());
        $objErr->arrErr = $objFormParam->checkError();

        // 相関チェック
        $objErr->doFunc(array("注文番号1", "注文番号2", "search_order_id1", "search_order_id2"), array("GREATER_CHECK"));
        // 受注日
        $objErr->doFunc(array("開始", "search_sorderyear", "search_sordermonth", "search_sorderday"), array("CHECK_DATE"));
        $objErr->doFunc(array("終了", "search_eorderyear", "search_eordermonth", "search_eorderday"), array("CHECK_DATE"));
        $objErr->doFunc(array("開始", "終了", "search_sorderyear", "search_sordermonth", "search_sorderday", "search_eorderyear", "search_eordermonth", "search_eorderday"), array("CHECK_SET_TERM"));
        // 次回日
        $objErr->doFunc(array("開始", "search_supdateyear", "search_supdatemonth", "search_supdateday"), array("CHECK_DATE"));
        $objErr->doFunc(array("終了", "search_eupdateyear", "search_eupdatemonth", "search_eupdateday"), array("CHECK_DATE"));
        $objErr->doFunc(array("開始", "終了", "search_supdateyear", "search_supdatemonth", "search_supdateday", "search_eupdateyear", "search_eupdatemonth", "search_eupdateday"), array("CHECK_SET_TERM"));
        return $objErr->arrErr;
    }

    /**
     * 検索クエリパラメーターの構築
     *
     * 検索条件のキーに応じた WHERE 句と, クエリパラメーターを構築する.
     * クエリパラメーターは, SC_FormParam の入力値から取得する.
     *
     * @param SC_FormParam $objFormParam SC_FormParam インスタンス
     * @return array($where, $arrVal, $order)
     */
    function lfGetQueryParam(&$objFormParam) {
        $objMdl =& SC_Mdl_PG_MULPAY_Ex::getInstance();
        $objQuery =& SC_Query_Ex::getSingletonInstance();
        $arrParam = $objFormParam->getHashArray();
        $where = "plg_PgSubs_order.del_flg = 0 AND dtb_customer.del_flg = 0 ";

        foreach ($arrParam as $key => $val) {
            if($val == "") {
                continue;
            }
            $this->lfBuildQuery($key, $where, $arrVal, $objFormParam);
        }
        $order = "plg_PgSubs_order.update_date DESC";
        return array($where, $arrVal, $order);
    }

    /**
     * クエリを構築する.
     *
     * 検索条件のキーに応じた WHERE 句と, クエリパラメーターを構築する.
     * クエリパラメーターは, SC_FormParam の入力値から取得する.
     *
     * 構築内容は, 引数の $where 及び $arrValues にそれぞれ追加される.
     *
     * @param string $key 検索条件のキー
     * @param string $where 構築する WHERE 句
     * @param array $arrValues 構築するクエリパラメーター
     * @param SC_FormParam $objFormParam SC_FormParam インスタンス
     * @return void
     */
    function lfBuildQuery($key, &$where, &$arrValues, &$objFormParam) {
        $dbFactory = SC_DB_DBFactory_Ex::getInstance();

        switch ($key) {

        case 'search_product_name':
            $where .= " AND plg_PgSubs_order.product_name ILIKE ?";
            $arrValues[] = sprintf('%%%s%%', $objFormParam->getValue($key));
            break;
        case 'search_product_code':
            $where .= " AND plg_PgSubs_order.product_code ILIKE ?";
            $arrValues[] = sprintf('%%%s%%', $objFormParam->getValue($key));
            break;
        case 'search_order_name':
            $where .= " AND " . $dbFactory->concatColumn(array("dtb_customer.name01", "dtb_customer.name02")) . " LIKE ?";
            $arrValues[] = sprintf('%%%s%%', $objFormParam->getValue($key));
            break;
        case 'search_order_kana':
            $where .= " AND " . $dbFactory->concatColumn(array("dtb_customer.kana01", "dtb_customer.kana02")) . " LIKE ?";
            $arrValues[] = sprintf('%%%s%%', $objFormParam->getValue($key));
            break;
        case 'search_order_email':
            $where .= " AND dtb_customer.email ILIKE ?";
            $arrValues[] = sprintf('%%%s%%', $objFormParam->getValue($key));
            break;

        case 'search_order_id1':
            $where .= " AND subs_order_id >= ?";
            $arrValues[] = sprintf('%d', $objFormParam->getValue($key));
            break;
        case 'search_order_id2':
            $where .= " AND subs_order_id <= ?";
            $arrValues[] = sprintf('%d', $objFormParam->getValue($key));
            break;
        case 'search_order_tel':
            $where .= " AND (" . $dbFactory->concatColumn(array("dtb_customer.tel01", "dtb_customer.tel02", "dtb_customer.tel03")) . " LIKE ?)";
            $arrValues[] = sprintf('%%%d%%', preg_replace('/[()-]+/','', $objFormParam->getValue($key)));
            break;
        case 'search_sorderyear':
            $date = SC_Utils_Ex::sfGetTimestamp($objFormParam->getValue('search_sorderyear'),
                                                $objFormParam->getValue('search_sordermonth'),
                                                $objFormParam->getValue('search_sorderday'));
            $where.= " AND plg_PgSubs_order.create_date >= ?";
            $arrValues[] = $date;
            break;
        case 'search_eorderyear':
            $date = SC_Utils_Ex::sfGetTimestamp($objFormParam->getValue('search_eorderyear'),
                                                $objFormParam->getValue('search_eordermonth'),
                                                $objFormParam->getValue('search_eorderday'), true);
            $where.= " AND plg_PgSubs_order.create_date <= ?";
            $arrValues[] = $date;
            break;
        case 'search_order_status':
            $where.= " AND plg_PgSubs_order.status = ?";
            $arrValues[] = $objFormParam->getValue($key);
            break;
        case 'search_arai_status':
            if ($objFormParam->getValue($key) > 0) {
                $where .= " AND dtb_customer.plg_pgsubs_status > 0";
            }
            break;
        default:
        }
    }

    /**
     * 受注を削除する.
     *
     * @param string $where 削除対象の WHERE 句
     * @param array $arrParam 削除対象の値
     * @return void
     */
    function doDelete($where, $arrParam = array()) {
        $objQuery =& SC_Query_Ex::getSingletonInstance();
        $sqlval['update_date'] = 'now()';
        $sqlval['del_flg'] = '1';
        $objQuery->update("plg_PgSubs_order", $sqlval, $where, $arrParam);
    }

    /**
     * CSV データを構築して取得する.
     *
     * 構築に成功した場合は, ファイル名と出力内容を配列で返す.
     * 構築に失敗した場合は, false を返す.
     *
     * @param string $where 検索条件の WHERE 句
     * @param array $arrVal 検索条件のパラメーター
     * @param string $order 検索結果の並び順
     * @return void
     */
    function doOutputCSV($where, $arrVal, $order) {
        require_once CLASS_EX_REALDIR . 'helper_extends/SC_Helper_CSV_Ex.php';
        $objQuery =& SC_Query_Ex::getSingletonInstance();
        $objQuery->setOrder($order);
        $cols = 'dtb_customer.name01, dtb_customer.name02, dtb_customer.email, plg_PgSubs_order.*';
        $from = 'plg_PgSubs_order left join dtb_customer on plg_PgSubs_order.customer_id = dtb_customer.customer_id';
        $sql = $objQuery->getSql($cols, $from , $where, $arrVal);

        $objCSV = new SC_Helper_CSV_Ex();
        $objCSV->sfDownloadCsvFromSql($sql, $arrVal, 'subs', null, true);
    }

    /**
     * 検索結果の行数を取得する.
     *
     * @param string $where 検索条件の WHERE 句
     * @param array $arrValues 検索条件のパラメーター
     * @return integer 検索結果の行数
     */
    function getNumberOfLines($where, $arrValues) {
        $objQuery =& SC_Query_Ex::getSingletonInstance();
        return $objQuery->count('plg_PgSubs_order left join dtb_customer on plg_PgSubs_order.customer_id = dtb_customer.customer_id', $where, $arrValues);
    }

    /**
     * 受注を検索する.
     *
     * @param string $where 検索条件の WHERE 句
     * @param array $arrValues 検索条件のパラメーター
     * @param integer $limit 表示件数
     * @param integer $offset 開始件数
     * @param string $order 検索結果の並び順
     * @return array 受注の検索結果
     */
    function findOrders($where, $arrValues, $limit, $offset, $order) {
        $objQuery =& SC_Query_Ex::getSingletonInstance();
        $objQuery->setLimitOffset($limit, $offset);
        $objQuery->setOrder($order);
        return $objQuery->select('plg_PgSubs_order.*, dtb_customer.name01, dtb_customer.name02', 'plg_PgSubs_order left join dtb_customer on plg_PgSubs_order.customer_id = dtb_customer.customer_id', $where, $arrValues);
    }

    function changeSubsOrderStatus($arrOrderIds, $target_status) {
        $objQuery =& SC_Query_Ex::getSingletonInstance();
        $sqlval = array();
        $sqlval['status'] = $target_status;
        $sqlval['update_date'] = 'CURRENT_TIMESTAMP';
        foreach ($arrOrderIds as $order_id) {
            $objQuery->update('plg_PgSubs_order', $sqlval, 'subs_order_id = ?', array($order_id));
        }
    }

    function changeSubsOrder($arrParam) {
        $objQuery =& SC_Query_Ex::getSingletonInstance();
        $sqlval = array();
        $sqlval['price'] = $arrParam['price'];
        $sqlval['quantity'] = $arrParam['quantity'];
        $sqlval['status'] = $arrParam['status'];
        $sqlval['update_date'] = 'CURRENT_TIMESTAMP';
        $objQuery->update('plg_PgSubs_order', $sqlval, 'subs_order_id = ?', array($arrParam['subs_order_id']));
    }

    /*
     * デストラクタ.
     *
     * @return void
     */
    function destroy() {
        parent::destroy();
    }
}
