<?php
/**
 * プラグイン のアップデート用クラス.
 *
 * @package PgSubs
 */
class plugin_update{
   /**
     * アップデート
     * updateはアップデート時に実行されます.
     * 引数にはdtb_pluginのプラグイン情報が渡されます.
     *
     * @param array $arrPlugin プラグイン情報の連想配列(dtb_plugin)
     * @return void
     */
    function update($arrPlugin) {
        // ファイルコピー
        $plugin_code = 'PgSubs';
        $plugin_dir_path = PLUGIN_UPLOAD_REALDIR . $plugin_code . '/';
        $this_dir = dirname(__FILE__) . '/';
        GC_Utils_Ex::gfPrintLog("plugin_update:" . $this_dir . " -> " . $plugin_dir_path);
        $bakup_dir_path = PLUGIN_UPLOAD_REALDIR . $plugin_code . '_' . date('YmdHis') . '/';
        mkdir ($bakup_dir_path);
        SC_Utils_Ex::copyDirectory($plugin_dir_path, $bakup_dir_path);

        SC_Utils_Ex::copyDirectory($this_dir, $plugin_dir_path);
        // ファイルコピー

        $objQuery =& SC_Query_Ex::getSingletonInstance();
        $masterData = new SC_DB_MasterData_Ex();
        $arrPref = $masterData->getMasterData('mtb_pref');
        $arrDeliv = $objQuery->select('*', 'dtb_deliv');
        foreach($arrDeliv as $deliv) {
            $deliv_id = $deliv['deliv_id'];
            $count = $objQuery->count('dtb_delivfee', 'deliv_id = ?', array($deliv_id));
            if (!$count) {
                foreach ($arrPref as $pref_id => $pref_name) {
                    $sqlval = array();
                    $sqlval['deliv_id'] = $deliv_id;
                    $sqlval['fee_id'] = $pref_id;
                    $sqlval['fee'] = '0';
                    $sqlval['pref'] = $pref_id;
                    $objQuery->insert("dtb_delivfee", $sqlval);
                }
            }
        }
    }
}

