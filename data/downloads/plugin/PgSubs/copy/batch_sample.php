<?php
/*
 * Copyright(c) 2012-2013 GMO Payment Gateway, Inc. All rights reserved.
 * http://www.gmo-pg.com/
 */
require_once('../require.php');
require_once(MODULE_REALDIR . 'mdl_pg_mulpay/inc/include.php');
require_once(PLUGIN_UPLOAD_REALDIR . 'PgSubs/class_extends/LC_Page_Admin_Order_PgSubs_Payment_Ex.php');

/// setting
$request_type = 'commit';   //売上実行時
//$request_type = 'reversal'; // 洗替実行時
//$request_type = 'commit_result'; // 売上結果取得
//$request_type = 'reversal_result'; // 洗替結果取得

$arrRequest = array(
  'order_year' => date('Y'),
  'order_month' => date('m'),
  'order_day' => date('d'),
);
// setting end

$obj = new LC_Page_Admin_Order_PgSubs_Payment_Ex();
$objFormParam = new SC_FormParam_Ex();
$obj->lfInitParam($objFormParam);

$objFormParam->setParam($_POST);
$objFormParam->convParam();

switch($request_type) {
case 'commit':
    $res = $obj->lfDoCommit($objFormParam);
    break;
case 'reversal':
    $res = $obj->lfDoReversal($objFormParam);
    break;
case 'commit_result':
    $res = $obj->lfDoCommitResult($objFormParam);
    break;
case 'reversal_result':
    $res = $obj->lfDoReversalResult($objFormParam);
    break;
}
// 結果出力
GC_Utils_Ex::gfPrintLog($res);
echo $res;

