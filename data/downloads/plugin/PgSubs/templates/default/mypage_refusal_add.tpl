<!--{*
 * Copyright(c) 2014 GMO Payment Gateway, Inc. All rights reserved.
 * http://www.gmo-pg.com/
 * Updated: 2014/07/15
 *}-->
<!--{if $plg_pgsubs_refusal_block}-->
<script type="text/javascript">//<![CDATA[
    $(function(){

        $('#complete_area').html('<div class="message">定期購入の契約があるため退会は出来ません。<br />お手数ですが、お問い合わせよりご相談下さい。</div>');
        });

//]]></script>
<!--{/if}-->

