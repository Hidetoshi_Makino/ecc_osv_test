<!--{*
 * Copyright(c) 2012-2013 GMO Payment Gateway, Inc. All rights reserved.
 * http://www.gmo-pg.com/
 * Updated: 2012/09/20
 *}-->

<div id="mypagecolumn">
    <h2 class="title"><!--{$tpl_title|h}--></h2>
    <!--{if $tpl_navi != ""}-->
        <!--{include file=$tpl_navi}-->
    <!--{else}-->
        <!--{include file=`$smarty.const.TEMPLATE_REALDIR`mypage/navi.tpl}-->
    <!--{/if}-->
    <div id="mycontents_area">
        <form name="form1" method="post" action="?">
        <input type="hidden" name="<!--{$smarty.const.TRANSACTION_ID_NAME}-->" value="<!--{$transactionid}-->" />
        <input type="hidden" name="subs_order_id" value="" id="subs_order_id" />
        <input type="hidden" name="pageno" value="<!--{$objNavi->nowpage}-->" />
        <input type="hidden" name="mode" value="" id="subs_mode" />
        <h3><!--{$tpl_subtitle|h}--></h3>

        <!--{if $objNavi->all_row > 0}-->

            <p><span class="attention"><!--{$objNavi->all_row}-->件</span>の定期購入履歴があります。</p>
            <p class="attention">配送先の変更は会員登録内容変更ページにて行って下さい。</p>
            <div class="pagenumber_area">
                <!--▼ページナビ-->
                <!--{$objNavi->strnavi}-->
                <!--▲ページナビ-->
            </div>

            <table summary="購入履歴">
                <tr>
                    <th class="alignC">申込日</th>
                    <th class="alignC">購入番号</th>
                    <th class="alignC">継続回数</th>
                    <th class="alignC">商品名</th>
                    <th class="alignC">数量</th>
                    <th class="alignC">商品単価</th>
                    <th class="alignC">ご注文状況</th>
                </tr>
                <!--{section name=cnt loop=$arrOrder}-->
                    <tr>
                        <td class="alignC"><!--{$arrOrder[cnt].create_date|sfDispDBDate}--></td>
                        <td class="alignC"><!--{$arrOrder[cnt].subs_order_id|h}--></td>
                        <td class="alignR"><!--{$arrOrder[cnt].order_count|number_format}--></td>
                        <td><!--{$arrOrder[cnt].product_name|h}--></td>
                        <td class="alignR"><!--{$arrOrder[cnt].quantity|number_format}--></td>
                        <td class="alignR"><!--{$arrOrder[cnt].price_inctax|number_format}--></td>
                        <!--{assign var=status_id value="`$arrOrder[cnt].status`"}-->
                        <td><!--{$arrORDERSTATUS[$status_id]|h}-->
                        <!--{if $tpl_is_cancel && $status_id == MDL_PG_MULPAY_SUBS_STATUS_WAIT}-->
                        <br />
                        <a href="#" onclick="fnSubsCancel('<!--{$arrOrder[cnt].subs_order_id|h}-->');">キャンセル依頼</a>
                        <!--{/if}-->
                        </td>
                    </tr>
                <!--{/section}-->
            </table>

        <!--{else}-->
            <p>購入履歴はありません。</p>
        <!--{/if}-->
        </form>
    </div>
</div>

<script type="text/javascript">//<![CDATA[
function fnSubsCancel(subs_order_id) {
    if (!window.confirm('本当にキャンセル依頼をしてもよろしいですか？\n※ご注意：手配のタイミングによりキャンセル依頼後も注文が継続される場合があります。\n「受付済」となるまでは有効ですのでご注意下さい。')) {
        return;
    }
    $('#subs_order_id').val(subs_order_id);
    $('#subs_mode').val('cancel');
    document.form1.submit();
}
//]]></script>

