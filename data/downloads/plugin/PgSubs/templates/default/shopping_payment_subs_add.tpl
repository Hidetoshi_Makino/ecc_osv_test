<!--{*
 * Copyright(c) 2012 GMO Payment Gateway, Inc. All rights reserved.
 * http://www.gmo-pg.com/
 * Updated: 2012/07/30
 *}-->

<!--{if $tpl_plg_pg_mulpay_is_subscription}-->
<!--{* 定期購入では、ポイント利用・お届け指定はデフォルトでは無効 *}-->
<script type="text/javascript">//<![CDATA[

    $(function(){
        $('.point_area').css('display','none');
        $('.pay_area02:first').css('display','none');

    });
//]]></script>
<!--{/if}-->

