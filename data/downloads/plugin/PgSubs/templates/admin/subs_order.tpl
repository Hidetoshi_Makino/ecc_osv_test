<!--{*
 * Copyright(c) 2012 GMO Payment Gateway, Inc. All rights reserved.
 * http://www.gmo-pg.com/
 *
 *}-->
<script type="text/javascript">
<!--
//-->
</script>

<div id="order" class="contents-main">
<form name="search_form" id="search_form" method="post" action="?">
<input type="hidden" name="<!--{$smarty.const.TRANSACTION_ID_NAME}-->" value="<!--{$transactionid}-->" />
<input type="hidden" name="mode" value="search" />
    <h2>検索条件設定</h2>
    <!--{* 検索条件設定テーブルここから *}-->
    <table>
        <tr>
            <th>定期管理番号</th>
            <td>
                <!--{assign var=key1 value="search_order_id1"}-->
                <!--{assign var=key2 value="search_order_id2"}-->
                <span class="attention"><!--{$arrErr[$key1]}--></span>
                <span class="attention"><!--{$arrErr[$key2]}--></span>
                <input type="text" name="<!--{$key1}-->" value="<!--{$arrForm[$key1].value|h}-->" maxlength="<!--{$arrForm[$key1].length}-->" style="<!--{$arrErr[$key1]|sfGetErrorColor}-->" size="6" class="box6" />
                ～
                <input type="text" name="<!--{$key2}-->" value="<!--{$arrForm[$key2].value|h}-->" maxlength="<!--{$arrForm[$key2].length}-->" style="<!--{$arrErr[$key2]|sfGetErrorColor}-->" size="6" class="box6" />
            </td>
            <th>定期購入注文状況</th>
            <td>
                <!--{assign var=key value="search_order_status"}-->
                <span class="attention"><!--{$arrErr[$key]}--></span>
                <select name="<!--{$key}-->" style="<!--{$arrErr[$key]|sfGetErrorColor}-->">
                <option value="">選択してください</option>
                <!--{html_options options=$arrORDERSTATUS selected=$arrForm[$key].value}-->
                </select>
                <br />
                <!--{assign var=key value="search_arai_status"}-->
                <label><input type="checkbox" name="<!--{$key}-->" "<!--{$arrErr[$key]|sfGetErrorColor}-->" value="1" <!--{if $arrForm[$key].value == '1'}-->checked<!--{/if}--> />洗替NGのみ表示</label>
            </td>
        </tr>
        <tr>
            <th>お名前</th>
            <td>
            <!--{assign var=key value="search_order_name"}-->
            <span class="attention"><!--{$arrErr[$key]}--></span>
            <input type="text" name="<!--{$key}-->" value="<!--{$arrForm[$key].value|h}-->" maxlength="<!--{$arrForm[$key].length}-->" style="<!--{$arrErr[$key]|sfGetErrorColor}-->" size="30" class="box30" />
            </td>
            <th>お名前(フリガナ)</th>
            <td>
            <!--{assign var=key value="search_order_kana"}-->
            <span class="attention"><!--{$arrErr[$key]}--></span>
            <input type="text" name="<!--{$key}-->" value="<!--{$arrForm[$key].value|h}-->" maxlength="<!--{$arrForm[$key].length}-->" style="<!--{$arrErr[$key]|sfGetErrorColor}-->" size="30" class="box30" />
            </td>
        </tr>
        <tr>
            <th>メールアドレス</th>
            <td>
                <!--{assign var=key value="search_order_email"}-->
                <span class="attention"><!--{$arrErr[$key]}--></span>
                <input type="text" name="<!--{$key}-->" value="<!--{$arrForm[$key].value|h}-->" maxlength="<!--{$arrForm[$key].length}-->" style="<!--{$arrErr[$key]|sfGetErrorColor}-->" size="30" class="box30" />
            </td>
            <th>TEL</th>
            <td>
                <!--{assign var=key value="search_order_tel"}-->
                <span class="attention"><!--{$arrErr[$key]}--></span>
                <input type="text" name="<!--{$key}-->" value="<!--{$arrForm[$key].value|h}-->" maxlength="<!--{$arrForm[$key].length}-->" style="<!--{$arrErr[$key]|sfGetErrorColor}-->" size="30" class="box30" />
            </td>
        </tr>
        <tr>
            <th>購入商品</th>
            <td>
                <!--{assign var=key value="search_product_name"}-->
                <!--{if $arrErr[$key]}--><span class="attention"><!--{$arrErr[$key]}--></span><!--{/if}-->
                <input type="text" name="<!--{$key}-->" value="<!--{$arrForm[$key].value|h}-->" maxlength="<!--{$arrForm[$key].length}-->" style="<!--{$arrErr[$key]|sfGetErrorColor}-->" size="6" class="box30" />
            </td>
            <th>購入商品コード</th>
            <td>
                <!--{assign var=key value="search_product_code"}-->
                <!--{if $arrErr[$key]}--><span class="attention"><!--{$arrErr[$key]}--></span><!--{/if}-->
                <input type="text" name="<!--{$key}-->" value="<!--{$arrForm[$key].value|h}-->" maxlength="<!--{$arrForm[$key].length}-->" style="<!--{$arrErr[$key]|sfGetErrorColor}-->" size="6" class="box30" />
            </td>
        </tr>
        <tr>
            <th>注文日</th>
            <td colspan="3">
                <!--{if $arrErr.search_sorderyear}--><span class="attention"><!--{$arrErr.search_sorderyear}--></span><!--{/if}-->
                <!--{if $arrErr.search_eorderyear}--><span class="attention"><!--{$arrErr.search_eorderyear}--></span><!--{/if}-->
                <select name="search_sorderyear" style="<!--{$arrErr.search_sorderyear|sfGetErrorColor}-->">
                <option value="">----</option>
                <!--{html_options options=$arrRegistYear selected=$arrForm.search_sorderyear.value}-->
                </select>年
                <select name="search_sordermonth" style="<!--{$arrErr.search_sorderyear|sfGetErrorColor}-->">
                <option value="">--</option>
                <!--{html_options options=$arrMonth selected=$arrForm.search_sordermonth.value}-->
                </select>月
                <select name="search_sorderday" style="<!--{$arrErr.search_sorderyear|sfGetErrorColor}-->">
                <option value="">--</option>
                <!--{html_options options=$arrDay selected=$arrForm.search_sorderday.value}-->
                </select>日～
                <select name="search_eorderyear" style="<!--{$arrErr.search_eorderyear|sfGetErrorColor}-->">
                <option value="">----</option>
                <!--{html_options options=$arrRegistYear selected=$arrForm.search_eorderyear.value}-->
                </select>年
                <select name="search_eordermonth" style="<!--{$arrErr.search_eorderyear|sfGetErrorColor}-->">
                <option value="">--</option>
                <!--{html_options options=$arrMonth selected=$arrForm.search_eordermonth.value}-->
                </select>月
                <select name="search_eorderday" style="<!--{$arrErr.search_eorderyear|sfGetErrorColor}-->">
                <option value="">--</option>
                <!--{html_options options=$arrDay selected=$arrForm.search_eorderday.value}-->
                </select>日
            </td>
        </tr>
    </table>

    <div class="btn">
        <p class="page_rows">検索結果表示件数
        <!--{assign var=key value="search_page_max"}-->
        <span class="attention"><!--{$arrErr[$key]}--></span>
        <select name="<!--{$arrForm[$key].keyname}-->" style="<!--{$arrErr[$key]|sfGetErrorColor}-->">
        <!--{html_options options=$arrPageMax selected=$arrForm[$key].value}-->
        </select> 件</p>
        <div class="btn-area">
            <ul>
                <li><a class="btn-action" href="javascript:;" onclick="fnFormModeSubmit('search_form', 'search', '', ''); return false;"><span class="btn-next">この条件で検索する</span></a></li>
            </ul>
        </div>
    </div>
    <!--検索条件設定テーブルここまで-->
</form>

<!--{if count($arrErr) == 0 and ($smarty.post.mode == 'search' or $smarty.post.mode == 'delete' or $smarty.post.mode == 'subs_neworder' or $smarty.post.mode == 'subs_commit' or $smarty.post.mode == 'subs_arai')}-->

<!--★★検索結果一覧★★-->
<form name="form1" id="form1" method="post" action="?">
<input type="hidden" name="<!--{$smarty.const.TRANSACTION_ID_NAME}-->" value="<!--{$transactionid}-->" />
<input type="hidden" name="mode" value="search" />
<input type="hidden" name="order_id" value="" />
<input type="hidden" name="subs_order_id" value="" id="subs_order_id" />
<input type="hidden" name="status" value="" id="form1_status" />
<input type="hidden" name="price" value="" id="form1_price" />
<input type="hidden" name="quantity" value="" id="form1_quantity" />

<!--{foreach key=key item=item from=$arrHidden}-->
    <!--{if is_array($item)}-->
        <!--{foreach item=c_item from=$item}-->
        <input type="hidden" name="<!--{$key|h}-->[]" value="<!--{$c_item|h}-->" />
        <!--{/foreach}-->
    <!--{else}-->
        <input type="hidden" name="<!--{$key|h}-->" value="<!--{$item|h}-->" />
    <!--{/if}-->
<!--{/foreach}-->
    <h2>検索結果一覧</h2>
        <div class="btn">
        <span class="attention"><!--検索結果数--><!--{$tpl_linemax}-->件</span>&nbsp;が該当しました。

        <a class="btn-normal" href="javascript:;" onclick="fnModeSubmit('csv','',''); return false;"><span>CSVダウンロード</span></a>


        <a class="btn-normal" href="javascript:;" onclick="fnSelectCheckSubmit('change_subs_status'); return false;"><span>一括状況変更&gt;&gt;</span></a>
        <!--{assign var=key value="subs_change_status"}-->
        <span class="attention"><!--{$arrErr[$key]}--></span>
        <select name="<!--{$key}-->" style="<!--{$arrErr[$key]|sfGetErrorColor}-->">
        <option value="">選択してください</option>
        <!--{html_options options=$arrORDERSTATUS selected=$arrForm[$key].value}-->
        </select>

        &nbsp;&nbsp;
<!--{*
        <a class="btn-normal" href="javascript:;" onclick="fnSelectCheckSubmit('subs_neworder'); return false;"><span>次回用受注データ作成実行</span></a>
        <span class="attention"><!--{$tpl_error_subs_neworder}--></span>
*}-->
    </div>
    <!--{if count($arrResults) > 0}-->

    <!--{include file=$tpl_pager}-->

    <!--{* 検索結果表示テーブル *}-->
        <table class="list">
        <colgroup width="5%"></colgroup>
        <tr>
            <th rowspan="2"><label for="move_check">選択</label><br /><input type="checkbox" name="move_check" id="move_check" onclick="fnAllCheck(this, 'input[name=move[]]')" /></th>
            <th rowspan="2">管理<br />番号</th>
            <th>注文日</th>
            <th>初回受注</th>
            <th>最終受注</th>
            <th>購入回数</th>
            <th>お名前</th>
            <th colspan="3">お支払方法</th>
        </tr>
        <tr>
            <th colspan="2">商品名 / 商品コード</th>
            <th>数量</th>
            <th>購入単価</th>
            <th>定期購入注文状況</th>
            <th>変更</th>
            <th>通知</th>
            <th>削除</th>
        </tr>

        <!--{section name=cnt loop=$arrResults}-->
        <!--{assign var=status value="`$arrResults[cnt].status`"}-->
        <tr style="background:<!--{$arrORDERSTATUS_COLOR[$status]}-->;">
            <td class="center" rowspan="2"><input type="checkbox" name="move[]" value="<!--{$arrResults[cnt].subs_order_id}-->" ></td>
            <td class="center" rowspan="2"><!--{$arrResults[cnt].subs_order_id}--></td>
            <td class="center"><!--{$arrResults[cnt].create_date|sfDispDBDate}--></td>
            <td class="center">
                <a href="?" onclick="fnChangeAction('<!--{$smarty.const.ADMIN_ORDER_EDIT_URLPATH}-->'); fnModeSubmit('pre_edit', 'order_id', '<!--{$arrResults[cnt].first_order_id}-->'); return false;">
                <!--{$arrResults[cnt].first_order_id}-->
                </a>
            </td>
            <td class="center">
                <a href="?" onclick="fnChangeAction('<!--{$smarty.const.ADMIN_ORDER_EDIT_URLPATH}-->'); fnModeSubmit('pre_edit', 'order_id', '<!--{$arrResults[cnt].last_order_id}-->'); return false;">
                <!--{$arrResults[cnt].last_order_id}-->
                </a>
            </td>
            <td class="center">
                <!--{$arrResults[cnt].order_count|number_format}-->
            </td>
            <td><!--{$arrResults[cnt].name01|h}--> <!--{$arrResults[cnt].name02|h}--></td>
            <!--{assign var=payment_id value="`$arrResults[cnt].payment_id`"}-->
            <td class="center" colspan="3"><!--{$arrPayments[$payment_id]}--></td>
        </tr>
        <tr style="background:<!--{$arrORDERSTATUS_COLOR[$status]}-->;">
            <td colspan="2"><!--{$arrResults[cnt].product_name|h}--> <!--{$arrResults[cnt].classcategory_name1|h}--> <!--{$arrResults[cnt].classcategory_name2|h}-->
                <!--{if $arrResults[cnt].product_code !=""}--> / <!--{$arrResults[cnt].product_code|h}--><!--{/if}-->
            </td>
            <td class="right">
                <!--{assign var=key value="quantity_`$arrResults[cnt].subs_order_id`"}-->
                <span class="attention"><!--{$arrErr[$key]}--></span>
                <input type="text" class="box6" name="<!--{$key}-->" value="<!--{$arrResults[cnt].quantity|h}-->" />
            </td>
            <td class="right">
                <!--{assign var=key value="price_`$arrResults[cnt].subs_order_id`"}-->
                <span class="attention"><!--{$arrErr[$key]}--></span>
                <input type="text" class="box6" name="<!--{$key}-->" value="<!--{$arrResults[cnt].price|h}-->" />
            </td>
            <td class="center">
                <!--{assign var=key value="status_`$arrResults[cnt].subs_order_id`"}-->
                <span class="attention"><!--{$arrErr[$key]}--></span>
                <select name="<!--{$key}-->" style="<!--{$arrErr[$key]|sfGetErrorColor}-->">
                <!--{html_options options=$arrORDERSTATUS selected=$status}-->
                </select>
            </td>
            <td class="center">
                <a href="?" onclick="fnPgSubsChangeOrder('<!--{$arrResults[cnt].subs_order_id}-->');return false;"><span class="icon_change">変更</span></a>
            </td>
            <td class="center">
            <a href="?" onclick="fnChangeAction('<!--{$smarty.const.ADMIN_ORDER_MAIL_URLPATH}-->'); fnModeSubmit('pre_edit', 'order_id', '<!--{$arrResults[cnt].last_order_id}-->'); return false;"><span class="icon_mail">通知</span></a>
            </td>
            <td class="center"><a href="?" onclick="fnModeSubmit('delete_order', 'subs_order_id', <!--{$arrResults[cnt].subs_order_id}-->); return false;"><span class="icon_delete">削除</span></a></td>
        </tr>
        <!--{/section}-->
    </table>
    <!--{* 検索結果表示テーブル *}-->

    <!--{/if}-->

</form>
<!--{/if}-->
</div>

<script type="text/javascript">
<!--
function fnPgSubsChangeOrder(subs_order_id) {
    if(window.confirm('注文番号:' + subs_order_id + ' の編集内容を反映してよろしいですか？')) {
        var price = $('input[name=price_' + subs_order_id + ']').val();
        var qty = $('input[name=quantity_' + subs_order_id + ']').val();
        var change_status = $('select[name=status_' + subs_order_id + ']').val();
        $('#form1_price').val(price);
        $('#form1_quantity').val(qty);
        $('#form1_status').val(change_status);
        fnModeSubmit('change_order', 'subs_order_id', subs_order_id);
    }
}


function fnSelectCheckSubmit(mode){

    var selectflag = 0;
    var fm = document.form1;

    if(mode == 'change_subs_status') {
        if(fm.subs_change_status.options[document.form1.subs_change_status.selectedIndex].value == ""){
            selectflag = 1;
        }
    }

    if(selectflag == 1){
        alert('セレクトボックスが選択されていません');
        return false;
    }
    var i;
    var checkflag = 0;
    var max = fm["move[]"].length;

    if(max) {
        for (i=0;i<max;i++){
            if(fm["move[]"][i].checked == true){
                checkflag = 1;
            }
        }
    } else {
        if(fm["move[]"].checked == true) {
            checkflag = 1;
        }
    }

    if(checkflag == 0){
        alert('チェックボックスが選択されていません');
        return false;
    }

    if(window.confirm('選択された定期購入受注に対して処理を実行してよろしいですか？')) {
        fnModeSubmit(mode, '','');
    }
}
//-->
</script>

