<!--{*
 * Copyright(c) 2011 e-context All rights reserved.
 * http://www.econtext.jp/
 * Updated: 2012/07/30
 *}-->
<script type="text/javascript">//<![CDATA[
function fnPgMulpay(master_data_name) {
    $('input[name="mode"]').val('show');
    $('input[name="master_data_name"]').val(master_data_name);
    $('#form1').attr('action', '../system/masterdata.php');
    document.form1.submit();
}

function fnEconModeSubmit(mode) {
    switch(mode) {
    case 'add_product_type':
        if(!window.confirm('商品種別を追加してよろしいですか？')) {
            return;
        }
        break;
    default:
        break;
    }
    fnModeSubmit(mode,'','');
}
</script>

<form name="form1" id="form1" method="post" action="?" onSubmit="return window.confirm('登録しても宜しいですか');">
<input type="hidden" name="<!--{$smarty.const.TRANSACTION_ID_NAME}-->" value="<!--{$transactionid}-->" />
<input type="hidden" name="mode" value="confirm">
<input type="hidden" name="master_data_name" value="">

<div id="basis" class="contents-main">
  <h1><span class="subtitle">基本動作設定</span></h1>
  <h2>※全体の動作に影響がある初期設定項目です。</h2>
  <div id="econ_subs_basis">
    <table class="form">
      <tr>
        <th>定期購入(継続課金)の表示名称<span class="attention">※</span></th>
        <td>
          <!--{assign var=key value="subs_product_name"}-->
          <span class="attention"><!--{$arrErr[$key]}--></span>
          <input type="text" name="<!--{$key}-->" value="<!--{$arrForm[$key].value|h}-->" maxlength="<!--{$arrForm[$key].length}-->" style="<!--{$arrErr[$key1]|sfGetErrorColor}-->" size="60" class="box60" />
          <span class="attention"> (上限<!--{$arrForm[$key].length}-->文字)</span><br />
          <span>各種画面に追加される定期購入の表示用名称です。（例. 「お得な毎月お届け」）</span>
        </td>
      </tr>
      <tr>
        <th>定期購入用の商品種別<span class="attention">※</span></th>
        <td id="subs_product_status_list">
          <!--{assign var=key value="subs_product_type"}-->
          <span class="attention"><!--{$arrErr[$key]}--></span>
          <!--{html_radios name=$key options=$arrProductType selected=$arrForm[$key].value separator='&nbsp;&nbsp;'}-->
          <a class="btn-tool" href="javascript:;" name="product_type_add" onclick="fnEconModeSubmit('add_product_type'); return false;">&nbsp;&nbsp;&lt;-&nbsp;新たに追加する&nbsp;&nbsp;</a><br />
          <span>新たに追加すると対応する配送方法も自動的に追加されます。名称変更や削除をしたい場合は<a href="javascript:void();" onclick="fnPgMulpay('mtb_product_type');">マスターデータ管理</a>で設定して下さい。<br />配送方法の名称や詳細、配送料金を指定したい場合は、<a href="./delivery.php">配送方法設定</a>で設定して下さい。</span>
        </td>
      </tr>
      <tr>
        <th>キャンセル機能の有効化</th>
        <td>
        <!--{assign var=key value="subs_order_cancel_enable"}-->
        ユーザーによるキャンセル依頼機能を有効にする場合はチェックを入れて下さい &minus;&gt;<input type="checkbox" name="<!--{$key}-->" value="1" <!--{if $arrForm[$key].value == '1'}-->checked<!--{/if}--> /><br />
        </td>
      </tr>
      <tr>
        <th>定期購入 初回締め切り日</th>
        <td id="subs_order_limit_day">
        <!--{assign var=key value="subs_order_limit_day"}-->
        <span class="attention"><!--{$arrErr[$key]}--></span>
        <input type="text" name="<!--{$key}-->" value="<!--{$arrForm[$key].value|default:"31"|h}-->" maxlength="<!--{$arrForm[$key].length}-->" style="<!--{$arrErr[$key1]|sfGetErrorColor}-->" class="box6" />日までを当月受付分として扱う。<br />
        </td>
      </tr>
      <tr>
        <th>売上結果反映 売上成功時の受注状況</th>
        <td id="subs_product_status_list">
          <!--{assign var=key value="uri_ok_status"}-->
          <span class="attention"><!--{$arrErr[$key]}--></span>
          <select name="<!--{$key}-->" style="<!--{$arrErr[$key1]|sfGetErrorColor}-->">
          <option value="">--変更しない--</option>
          <!--{html_options options=$arrORDERSTATUS selected=$arrForm[$key].value}-->
          </select>
          <br />
          売上結果反映した際に売上成功の受注情報に対して指定の受注状況に変更します。<br />
          標準状態は「新規受付」となります。</span>
        </td>
      </tr>
      <tr>
        <th>売上結果反映 売上失敗時の受注状況</th>
        <td id="subs_product_status_list">
          <!--{assign var=key value="uri_ng_status"}-->
          <span class="attention"><!--{$arrErr[$key]}--></span>
          <select name="<!--{$key}-->" style="<!--{$arrErr[$key1]|sfGetErrorColor}-->">
          <option value="">--変更しない--</option>
          <!--{html_options options=$arrORDERSTATUS selected=$arrForm[$key].value}-->
          </select>
          <br />
          売上結果反映した際に売上失敗など何らかのエラー時に、受注情報に対して指定の受注状況に変更します。<br />
          標準状態は「キャンセル」となります。</span>
        </td>
      </tr>
      <tr>
        <th>後払い方式で運用する</th>
        <td>
        <!--{assign var=key value="subs_after_payment"}-->
        後払い方式で運用する場合はチェックを入れて下さい &minus;&gt;<input type="checkbox" name="<!--{$key}-->" value="1" <!--{if $arrForm[$key].value == '1'}-->checked<!--{/if}--> /><br />
        チェックを入れた場合、定期購入商品でのクレジットカード決済は自動的に「有効性チェック」となり、申込時には与信をとらなくなります。
        </td>
      </tr>
      <tr>
        <th>退会制限機能</th>
        <td>
        <!--{assign var=key value="subs_block_refusal"}-->
        定期購入の受注がある場合、ＭＹページにおける退会を制限する場合はチェックを入れて下さい &minus;&gt;<input type="checkbox" name="<!--{$key}-->" value="1" <!--{if $arrForm[$key].value == '1'}-->checked<!--{/if}--> /><br />
        </td>
      </tr>
      <tr>
        <th>定期購入（継続課金）の流れ</th>
        <td>
        1.初回分を受注<br />
        2.初回分のお届け<br />
        3.翌月、データ提出日までに課金管理から下記の定期購入注文を課金対象として抽出・課金実行（自動で受注データが作られます）。<br />
        &nbsp;(a)前月までの初回分で初回締め切り日以前に注文された定期購入注文。<br />
        &nbsp;(b)「次回待機状態」の定期購入注文。<br />
        4.別途定めるスケジュールにて返される結果を課金管理より反映させる。<br />
        5.「売上成功」している分の受注が上記設定の受注状況となっていますのでお届け（または完了処理）をして下さい。売上が成功した定期購入注文は「次回待機状態」となります。<br />
        <br />
        以下、3から5を毎月繰り返すこととなります。
        </td>
      </tr>
    </table>
  </div>
  <h1><span class="subtitle">定期購入ファイル交換設定</span></h1>
  <h2>※ファイル交換による売上を行う場合には設定が必要です。</h2>
  <div id="econ_subs_basis">
    <table class="form">
      <tr>
        <th>SFTPホストアドレス</th>
        <td>
          <!--{assign var=key value="subs_ftran_host"}-->
          <span class="attention"><!--{$arrErr[$key]}--></span>
          <input type="text" name="<!--{$key}-->" value="<!--{$arrForm[$key].value|h}-->" maxlength="<!--{$arrForm[$key].length}-->" style="<!--{$arrErr[$key1]|sfGetErrorColor}-->" size="60" class="box60" />
          <br />
        </td>
      </tr>
      <tr>
        <th>SFTPログインID</th>
        <td>
          <!--{assign var=key value="subs_ftran_id"}-->
          <span class="attention"><!--{$arrErr[$key]}--></span>
          <input type="text" name="<!--{$key}-->" value="<!--{$arrForm[$key].value|h}-->" maxlength="<!--{$arrForm[$key].length}-->" style="<!--{$arrErr[$key1]|sfGetErrorColor}-->" size="60" class="box60" />
          <br />
        </td>
      </tr> 
      <tr>
        <th>SFTPパスワード</th>
        <td>
          <!--{assign var=key value="subs_ftran_password"}-->
          <span class="attention"><!--{$arrErr[$key]}--></span>
          <input type="password" name="<!--{$key}-->" value="<!--{$arrForm[$key].value|h}-->" maxlength="<!--{$arrForm[$key].length}-->" style="<!--{$arrErr[$key1]|sfGetErrorColor}-->" size="60" class="box60" />
          <br />
        </td>
      </tr>
    </table>
  </div>
  <div class="btn-area">
    <ul>
      <li><a class="btn-action" href="javascript:;" onclick="fnFormModeSubmit('form1', 'confirm', '', ''); return false;"><span class="btn-next">この内容で登録する</span></a></li>
    </ul>
  </div>
</div>
</form>




