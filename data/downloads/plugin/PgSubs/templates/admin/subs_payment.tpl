<!--{*
 * Copyright(c) 2012 GMO Payment Gateway, Inc. All rights reserved.
 * http://www.gmo-pg.com/
 *
 *}-->
<script type="text/javascript">
<!--
//-->
</script>

<div id="order" class="contents-main">
<form name="search_form" id="search_form" method="post" action="?" enctype="multipart/form-data">
<input type="hidden" name="<!--{$smarty.const.TRANSACTION_ID_NAME}-->" value="<!--{$transactionid}-->" />
<input type="hidden" name="mode" value="search" />
    <h2>継続課金・洗替の依頼</h2>
    <table>
        <tr>
            <th>売上実行</th>
            <td>
                定期購入受注から、受注データと売上用ファイルを生成します。<br />
                SFTPサーバー情報が登録されている場合はアップロードも行います。<br />
                <br />
                利用年月日指定: 
                <span class="attention"><!--{$arrErr.order_year}--><!--{$arrErr.order_month}--><!--{$arrErr.order_day}--></span>
                <select name="order_year" <!--{if $arrErr.order_year != ""}--><!--{sfSetErrorStyle}--><!--{/if}--> >
                <!--{html_options options=$arrYear selected=$arrForm.order_year.value}-->
                </select>年
                <select name="order_month" <!--{if $arrErr.order_month != ""}--><!--{sfSetErrorStyle}--><!--{/if}--> >
                <!--{html_options options=$arrMonth selected=$arrForm.order_month.value}-->
                </select>月
                <select name="order_day" <!--{if $arrErr.order_day != ""}--><!--{sfSetErrorStyle}--><!--{/if}--> >
                <!--{html_options options=$arrDay selected=$arrForm.order_day.value}-->
                </select>日

            </td>
            <td>
                <!--{if $tpl_error_commit}--><span class="attention"><!--{$tpl_error_commit|nl2br}--></span><br /><!--{/if}-->
                <a class="btn-action" href="javascript:;" onclick="fnFormModeSubmit('search_form', 'commit', '', ''); return false;"><span class="btn-next">生成する</span></a>
            </td>
        </tr>
        <tr>
            <th>会員洗替実行</th>
            <td>
                クレジットカード登録済み会員から洗替用ファイルを生成します。<br />
                SFTPサーバー情報が登録されている場合はアップロードも行います。
            </td>
            <td>
                <!--{if $tpl_error_reversal}--><span class="attention"><!--{$tpl_error_reversal|nl2br}--></span><br /><!--{/if}-->
                <a class="btn-action" href="javascript:;" onclick="fnFormModeSubmit('search_form', 'reversal', '', ''); return false;"><span class="btn-next">生成する</span></a>
            </td>
        </tr>
    </table>

    <h2>継続課金・洗替の結果の反映</h2>
    <table>
        <tr>
            <th>売上結果</th>
            <td>
                売上結果ファイルを取得して反映します。
            </td>
            <td>
                <!--{if $tpl_error_commit_result}--><span class="attention"><!--{$tpl_error_commit_result|nl2br}--></span><br /><!--{/if}-->
                <a class="btn-action" href="javascript:;" onclick="fnFormModeSubmit('search_form', 'commit_result', '', ''); return false;"><span class="btn-next">実行する</span></a>
            </td>
            <td>
                <!--{if $tpl_error_commit_result_file}--><span class="attention"><!--{$tpl_error_commit_result_file}--></span><br /><!--{/if}-->
                <input type="file" name="upload_commit_file" size="40" />
                <a class="btn-action" href="javascript:;" onclick="fnFormModeSubmit('search_form', 'commit_result_file','',''); return false;"><span span class="btn-next">手動アップロード</span></a>
            </td>
        </tr>
        <tr>
            <th>洗替結果</th>
            <td>
                洗替結果ファイルを取得して反映します。
            </td>
            <td>
                <!--{if $tpl_error_reversal_result}--><span class="attention"><!--{$tpl_error_reversal_result|nl2br}--></span><br /><!--{/if}-->
                <a class="btn-action" href="javascript:;" onclick="fnFormModeSubmit('search_form', 'reversal_result', '', ''); return false;"><span class="btn-next">実行する</span></a>
            </td>
            <td>
                <!--{if $tpl_error_reversal_result_file}--><span class="attention"><!--{$tpl_error_reversal_result_file}--></span><br /><!--{/if}-->
                <input type="file" name="upload_reversal_file" size="40" />
                <a class="btn-action" href="javascript:;" onclick="fnFormModeSubmit('search_form', 'reversal_result_file','',''); return false;"><span span class="btn-next">手動アップロード</span></a>
            </td>
        </tr>
    </table>
</form>

<!--★★検索結果一覧★★-->
<form name="form1" id="form1" method="post" action="?">
<input type="hidden" name="<!--{$smarty.const.TRANSACTION_ID_NAME}-->" value="<!--{$transactionid}-->" />
<input type="hidden" name="mode" value="search" />
<input type="hidden" name="search_pageno" value="" />
<input type="hidden" name="subs_csv_id" value="" id="subs_order_id" />

    <h2>実行結果一覧</h2>
        <div class="btn">
        <span class="attention"><!--検索結果数--><!--{$tpl_linemax|default:0}-->件</span>&nbsp;が該当しました。

    </div>
    <!--{if count($arrResults) > 0}-->

    <!--{include file=$tpl_pager}-->

    <!--{* 検索結果表示テーブル *}-->
        <table class="list">
        <colgroup width="15%"></colgroup>
        <colgroup width="15%"></colgroup>
        <colgroup width="10%"></colgroup>
        <colgroup width="30%"></colgroup>
        <colgroup width="20%"></colgroup>
        <tr>
            <th>実行日時</th>
            <th>実行種類</th>
            <th>処理件数</th>
            <th>処理結果等</th>
            <th>ファイル</th>
        </th>

        <!--{section name=cnt loop=$arrResults}-->
        <tr>
            <td class="center"><!--{$arrResults[cnt].create_date|sfDispDBDate}--></td>
            <td><!--{$arrResults[cnt].type|h}--></td>
            <td class="right"><!--{$arrResults[cnt].line_num|h}--></td>
            <td><!--{if !is_numeric($arrResults[cnt].memo)}--><!--{$arrResults[cnt].memo|h|nl2br}--><!--{/if}--></td>
            <td>
                <!--{if $arrResults[cnt].is_file}-->
                <a href="#" onclick="fnModeSubmit('csv_download', 'subs_csv_id', '<!--{$arrResults[cnt].subs_csv_id|h}-->');return false;">
                    <!--{$arrResults[cnt].filename|h}-->
                </a>
                <!--{else}-->
                    <!--{$arrResults[cnt].filename|h}-->
                <!--{/if}-->
            </td>
        </tr>
        <!--{/section}-->
    </table>
    <!--{* 検索結果表示テーブル *}-->

    <!--{/if}-->

</form>
</div>

