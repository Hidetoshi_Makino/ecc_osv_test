<!--{*
 * Copyright(c) 2012 GMO Payment Gateway, Inc. All rights reserved.
 * http://www.gmo-pg.com/
 * Updated: 2012/07/30
 *}-->

<!--{if $tpl_plg_pg_mulpay_is_subscription}-->
<!--{* 定期購入では、ポイント利用・お届け指定はデフォルトでは無効 *}-->
<script type="text/javascript">//<![CDATA[

    $(function(){
        $('#point_on').css('display','none');
        $('.point_announce').css('display','none');
        $('.check_point').css('display','none');
        $('label[for=point_on]').css('display','none');
        $('label[for=point_off]').text('この商品ではポイントは使用できません。');
        $('#point_off').css('display','none');
        $('.btn_area_btm').css('display','none');
        $('.pay_area02').each(
            function() {
                var str = $(this).html();
                $(this).html(str.replace(/お届け時間の指定/g,''));
            });

    });
//]]></script>
<!--{/if}-->

