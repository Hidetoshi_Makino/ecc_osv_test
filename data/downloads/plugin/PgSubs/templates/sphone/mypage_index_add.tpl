<!--{*
 * Copyright(c) 2012 GMO Payment Gateway, Inc. All rights reserved.
 * http://www.gmo-pg.com/
 * Updated: 2012/08/30
 *}-->
<!--{if !$is_link_load_pgsubs}--> <!--{* 多重ロードを防ぐ *}-->

<script type="text/javascript">//<![CDATA[
    $(function(){
        $('nav#mypage_nav li:last').after('<li class="nav_refusal"><a href="mypage_subs_order.php" class="ui-link" rel="external">定期購入一覧</a></li>');
        });

//]]></script>

<!--{/if}-->
<!--{assign var="is_link_load_pgsubs" value="1"}-->

