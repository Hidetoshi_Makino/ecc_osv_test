<!--{*
/*
 * This file is part of EC-CUBE
 *
 * Copyright(c) 2000-2014 LOCKON CO.,LTD. All Rights Reserved.
 *
 * http://www.lockon.co.jp/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
*}-->

<!--{strip}-->
    <!--{$CustomerName1|h}--> <!--{$CustomerName2|h}-->様<br>
    いつもご利用いただきありがとうございます。<br>
    <br>
    <!--★現在のポイント★-->
    <!--{if $smarty.const.USE_POINT !== false}-->
        現在の所持ポイントは「<font color="#ff0000"><!--{$CustomerPoint|n2s|default:"0"|h}-->Pt</font>」です。<br>
        <br>
    <!--{/if}-->

    <hr>
    <a href="change.php" accesskey="1"><!--{1|numeric_emoji}-->登録内容変更</a><br>
    <a href="refusal.php" accesskey="2"><!--{2|numeric_emoji}-->退会</a><br>
    <br>
    <hr>

    ■定期購入履歴一覧<br>
    <!--{if $objNavi->all_row > 0}-->
        <!--{$objNavi->all_row}-->件の定期購入履歴があります。<br>
        <font color="#ff0000">配送先の変更は会員登録内容変更ページにて行って下さい。</font>
        <br>
        <form name="form1" method="post" action="?">
        <input type="hidden" name="<!--{$smarty.const.TRANSACTION_ID_NAME}-->" value="<!--{$transactionid}-->" />
        <input type="hidden" name="subs_order_id" value="" id="subs_order_id" />
        <input type="hidden" name="pageno" value="<!--{$objNavi->nowpage}-->" />
        <input type="hidden" name="mode" value="" id="subs_mode" />
        <!--{section name=cnt loop=$arrOrder}-->
            <hr>
            ▽申込日<br>
            <!--{$arrOrder[cnt].create_date|sfDispDBDate}--><br>
            ▽購入番号<br>
            <!--{$arrOrder[cnt].subs_order_id|h}--><br>
            ▽継続回数<br>
            <!--{$arrOrder[cnt].order_count|number_format}--><br>
            ▽商品名<br>
            <!--{$arrOrder[cnt].product_name|h}--><br>
            ▽数量<br>
            <!--{$arrOrder[cnt].quantity|number_format}--><br>
            ▽商品単価<br>
            <font color="#ff0000"><!--{$arrOrder[cnt].price_inctax|number_format}--></font><br>
            ▽ご注文状況<br>
            <!--{assign var=status_id value="`$arrOrder[cnt].status`"}-->
            <!--{$arrORDERSTATUS[$status_id]|h}-->
                <!--{if $tpl_is_cancel && $status_id == MDL_PG_MULPAY_SUBS_STATUS_WAIT}-->
                <br />
            <a href="#" onclick="fnSubsCancel('<!--{$arrOrder[cnt].subs_order_id|h}-->');">キャンセル依頼</a>
            <!--{/if}-->
        <!--{/section}-->
        <hr>
    <!--{else}-->
        購入履歴はありません。<br>
    <!--{/if}-->
     </form>
    <!--{if $objNavi->strnavi != ""}-->
        <!--{$objNavi->strnavi}-->
        <br>
    <!--{/if}-->
<!--{/strip}-->
<script type="text/javascript">//<![CDATA[
function fnSubsCancel(subs_order_id) {
    document.getElementById('subs_order_id').value = subs_order_id;
    document.getElementById('subs_mode').value = 'cancel';
    document.form1.submit();
}
//]]></script>
