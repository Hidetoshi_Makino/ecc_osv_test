<!--{*
 * Copyright(c) 2013 Spirit of Co.,Ltd. All rights reserved.
 * http://sbps.ec-cube.pro/
 * Updated: 2013/08/01
 * sps_cvsapi.tpl
 *}-->
<script type="text/javascript">//<![CDATA[
var send = true;

function fnCheckSubmit(mode) {
    $('#payment_form_body').slideToggle();
    $('#payment_form_loading').slideToggle();

    if(send) {
        send = false;
        fnModeSubmit(mode,'','');
        return false;
    } else {
        alert("只今、処理中です。しばらくお待ち下さい。");
        return false;
    }
}

function fnAutoLoadSubmit() {

    var mode = 'next';
    send = false;
    setTimeout("fnModeSubmit('next','','')", 500);
}

function fnAutoLoadSubmitSPS() {
    setTimeout("document.form_sps.submit()", 500);
}

//]]>
</script>

        <div id="payment_form_loading" style="display:none;">
            <div class="information">
                <p>決済処理中です。そのまま、お待ち下さい。</p>
                <span class="attention">※画面が切り替るまで少々時間がかかる場合がございますが、そのままお待ちください。</span>
            </div>
            <table summary="">
                <tr>
                <td class="alignC">
                    <img src="<!--{$smarty.const.MDL_SPS_MEDIAFILE_URL}-->loading.gif" />
                </td>
                </tr>
            </table>
        </div>
        <div id="payment_form_body">
            <div class="information">
                <p>下記項目をご選択ください。
                入力後、一番下の「決済する」ボタンをクリックしてください。</p>

                <!--{if count($arrErr) > 0}-->
                <p>決済エラー<br />
                大変お手数ですがエラーの内容を確認して、再度お手続きを進めて下さい。</p>
                <!--{foreach from=$arrErr item=err}-->
                <p class="attention"><!--{$err}--></p>
                <!--{/foreach}-->
                <!--{/if}-->
            </div>
            <table summary="コンビニ選択">
                <colgroup width="20%"></colgroup>
                <colgroup width="80%"></colgroup>
                <tr>
                    <th class="alignC">選択</th>
                    <th class="alignC">お支払いをするコンビニエンスストア</th>
                </tr>
                <!--{foreach from=$arrCvs item=cvsname key=cvstype}-->
                <tr>
                    <td class="alignC">
                    <input type="radio" name="webcvstype" value="<!--{$cvstype|h}-->" <!--{if $arrForm[$key].value == $cvstype}-->checked="checked"<!--{/if}--> />
                    </td>
                    <td>
                    <!--{$cvsname|h}-->
                    </td>
                </tr>
                <!--{/foreach}-->
            </table>

            <table>
                <tr>
                    <td>
                        以上の内容で間違いなければ、下記「次へ」ボタンをクリックしてください。<br />
                        <span class="attention">※画面が切り替るまで少々時間がかかる場合がございますが、そのままお待ちください。</span>
                    </td>
                </tr>
            </table>

            <div class="btn_area">
                <ul>
                    <li>
                        <input type="image" onclick="return fnCheckSubmit('return');" onmouseover="chgImg('<!--{$TPL_URLPATH}-->img/button/btn_back_on.jpg',this)" onmouseout="chgImg('<!--{$TPL_URLPATH}-->img/button/btn_back.jpg',this)" src="<!--{$TPL_URLPATH}-->img/button/btn_back.jpg" alt="戻る" border="0" name="back" id="back"/>
                    </li>

                    <li>
                        <input type="image" onclick="return fnCheckSubmit('next');" onmouseover="chgImg('<!--{$TPL_URLPATH}-->img/button/btn_next_on.jpg',this)" onmouseout="chgImg('<!--{$TPL_URLPATH}-->img/button/btn_next.jpg',this)" src="<!--{$TPL_URLPATH}-->img/button/btn_next.jpg" alt="次へ" border="0" name="next" id="next" />
                    </li>
                </ul>
            </div>

       </div><!--{* /payment_form_body *}-->


