<!--{*
 * Copyright(c) 2013 Spirit of Co.,Ltd. All rights reserved.
 * http://sbps.ec-cube.pro/
 * Updated: 2013/08/01
 *}-->

<!--{if $plg_sps_payment_type_id == $smarty.const.SPS_CREDIT_API_PAYID}-->
            <tr>
                <th>売上計上設定<span class="attention"> *</span></th>
                <td>
                    <!--{assign var=key value="sps_auth_type"}-->
                    <span class="attention"><!--{$arrErr[$key]}--></span>
                    <!--{html_radios name=$arrForm[$key].keyname options=$arrAuthType selected=$arrForm[$key].value separator="&nbsp;"}-->

                    <span style="font-size:80%">
                    ※自動売上は予め申込みが必要です。
                    </span>
                </td>
            </tr>

            <tr>
                <th>与信後の受注状況<span class="attention"> *</span></th>
                <td>
                    <!--{assign var=key value="auth_order_status"}-->
                    <span class="attention"><!--{$arrErr[$key]}--></span>
                    <select name="<!--{$key}-->" style="<!--{$arrErr[$key]|sfGetErrorColor}-->" id="<!--{$key}-->">
                    <option value="">--選択して下さい--</option>
                    <!--{html_options options=$arrORDERSTATUS selected=$arrForm[$key].value}-->
                    </select>
                    <span style="font-size:80%"><br />
                    ※与信成功時に注文データの受注状況を設定した状態に変更します。
                    </span>
                </td>
            </tr>

            <tr>
                <th>売上確定後の受注状況</th>
                <td>
                    <!--{assign var=key value="commit_order_status"}-->
                    <span class="attention"><!--{$arrErr[$key]}--></span>
                    <select name="<!--{$key}-->" style="<!--{$arrErr[$key]|sfGetErrorColor}-->" id="<!--{$key}-->">
                    <option value="0">変更しない</option>
                    <!--{html_options options=$arrORDERSTATUS selected=$arrForm[$key].value}-->
                    </select>
                    <span style="font-size:80%"><br />
                    ※売上確定時に注文データの受注状況を設定した状態に変更します。
                    </span>
                </td>
            </tr>

            <tr>
                <th>与信取消後の受注状況</th>
                <td>
                    <!--{assign var=key value="cancel_order_status"}-->
                    <span class="attention"><!--{$arrErr[$key]}--></span>
                    <select name="<!--{$key}-->" style="<!--{$arrErr[$key]|sfGetErrorColor}-->" id="<!--{$key}-->">
                    <option value="0">変更しない</option>
                    <!--{html_options options=$arrORDERSTATUS selected=$arrForm[$key].value}-->
                    </select>
                    <span style="font-size:80%"><br />
                    ※与信取消時に注文データの受注状況を設定した状態に変更します。
                    </span>
                </td>
            </tr>

            <tr>
                <th>セキュリティコード<span class="attention"> *</span></th>
                <td>
                    <!--{assign var=key value="is_security_code"}-->
                    <span class="attention"><!--{$arrErr[$key]}--></span>
                    <!--{html_radios name=$arrForm[$key].keyname options=$arrOptionActive selected=$arrForm[$key].value separator="&nbsp;"}-->
                    <span style="font-size:80%"><br />
                    ※セキュリティコードの入力を求めるかを設定します。
                    </span>
                </td>
            </tr>

            <tr>
                <th>カード情報登録機能<span class="attention"> *</span></th>
                <td>
                    <!--{assign var=key value="is_regist_card"}-->
                    <span class="attention"><!--{$arrErr[$key]}--></span>
                    <!--{html_radios name=$arrForm[$key].keyname options=$arrOptionActive selected=$arrForm[$key].value separator="&nbsp;"}-->
                    <span style="font-size:80%"><br />
                    ※カード情報の登録機能を有効にするかを設定します。
                    </span>
                </td>
            </tr>

            <tr>
                <th>有効にする取引区分<span class="attention"> *</span></th>
                <td>
                    <!--{assign var=key value="enable_dealings_types"}-->
                    <span class="attention"><!--{$arrErr[$key]}--></span>
                    <!--{html_checkboxes name="$key" options=$arrDealingsTypes selected=$arrForm[$key].value separator="&nbsp;"}-->
                    <span style="font-size:80%"><br />
                    ※カード会社との契約に合わせて有効にするものを選択して下さい。
                    </span>
                </td>
            </tr>

            <tr>
                <th>有効にする分割回数</th>
                <td>
                    <!--{assign var=key value="enable_divide_times"}-->
                    <span class="attention"><!--{$arrErr[$key]}--></span>
                    <!--{html_checkboxes name="$key" options=$arrDivideTimes selected=$arrForm[$key].value separator="&nbsp;"}-->
                    <span style="font-size:80%"><br />
                    ※取引区分で分割払いを有効にした場合は必須です。<br />
                    ※カード会社との契約に合わせて有効にするものを選択して下さい。
                    </span>
                </td>
            </tr>

<!--{/if}-->
