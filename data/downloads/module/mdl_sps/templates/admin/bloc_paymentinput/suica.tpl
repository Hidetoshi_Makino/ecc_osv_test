<!--{*
 * Copyright(c) 2013 Spirit of Co.,Ltd. All rights reserved.
 * http://sbps.ec-cube.pro/
 * Updated: 2013/08/01
 *}-->

<!--{if $plg_sps_payment_type_id == $smarty.const.SPS_SUICA_LINK_PAYID}-->

            <tr>
                <th>売上確定後の受注状況</th>
                <td>
                    <!--{assign var=key value="commit_order_status"}-->
                    <span class="attention"><!--{$arrErr[$key]}--></span>
                    <select name="<!--{$key}-->" style="<!--{$arrErr[$key]|sfGetErrorColor}-->" id="<!--{$key}-->">
                    <option value="0">変更しない</option>
                    <!--{html_options options=$arrORDERSTATUS selected=$arrForm[$key].value}-->
                    </select>
                    <span style="font-size:80%"><br />
                    ※売上確定時に注文データの受注状況を設定した状態に変更します。
                    </span>
                </td>
            </tr>
<!--{/if}-->
