<!--{*
/*
 * Copyright(c) 2013 Spirit of Co.,Ltd. All Rights Reserved.
 *
 * http://sbps.ec-cube.pro/
 */
*}-->
<!--{include file="`$smarty.const.TEMPLATE_ADMIN_REALDIR`admin_popup_header.tpl"}-->
<!--{include file='css/contents.tpl'}-->
<script type="text/javascript">
<!--
self.moveTo(20,20);self.focus();

function win_open(URL){
    var WIN;
    WIN = window.open(URL);
    WIN.focus();
}

//-->
</script>


<h1><span class="title"><!--{$tpl_subtitle}--></span></h1>
<h2>
                                                <!--{$smarty.const.MDL_SPS_MODULE_NAME}-->をご利用頂く為には、ユーザ様ご自身で
                                                <!--{$smarty.const.MDL_SPS_COMPANY_NAME|h}-->とご契約を行っていただく必要があります。 <br/>
                                                お申し込みにつきましては、下記のページから、お申し込みを行って下さい。<br/><br/>
                                                <a href="#" onClick="win_open('http://www.sbpayment.jp/')" > ＞＞<!--{$smarty.const.MDL_SPS_COMPANY_NAME|h}--></a>
                                                <br />
                                                <br />
                                                決済モジュールのサポートに関する情報やFAQは下記でご覧になれます。<br />
                                                <a href="#" onClick="win_open('<!--{$smarty.const.MDL_SPS_INFO_URI}-->')" > ＞＞SBPS決済EC-CUBE用モジュールサポート事務局サイト</a>
</h2>

<form name="form1" id="form1" method="post" action="<!--{$smarty.server.REQUEST_URI|h}-->">
  <input type="hidden" name="mode" value="" />
  <input type="hidden" name="<!--{$smarty.const.TRANSACTION_ID_NAME}-->" value="<!--{$transactionid}-->" />
  <table class="form">
    <colgroup width="35%">
    <colgroup width="65%">
    <!--{if $arrErr.err != ""}-->
    <tr>
      <td colspan="2"><span class="attention"><!--{$arrErr.err}--></span></td>
    </tr>
    <!--{/if}-->
    <!--{if $tpl_recommend_check}-->
    <tr>
      <td colspan="2"><span class="attention"><!--{$tpl_recommend_check}--></span></td>
    </tr>
    <!--{/if}-->

    <!--{assign var=key value="id_sps_marchant"}-->
    <tr id="<!--{$key}-->">
      <th>マーチャントID<span class="attention"> *</span></th>
      <td>
        <!--{if $arrErr[$key]}--><span class="attention"><!--{$arrErr[$key]}--></span><!--{/if}-->
        <input type="text" name="<!--{$key}-->" style="ime-mode:disabled; <!--{$arrErr[$key]|sfGetErrorColor}-->" value="<!--{$arrForm[$key].value|h}-->" class="box60" maxlength="<!--{$arrForm[$key].length}-->" />
      </td>
    </tr>
    <!--{assign var=key value="id_sps_service"}-->
    <tr id="<!--{$key}-->">
      <th>サービスID<span class="attention"> *</span></th>
      <td>
        <!--{if $arrErr[$key]}--><span class="attention"><!--{$arrErr[$key]}--></span><!--{/if}-->
        <input type="text" name="<!--{$key}-->" style="ime-mode:disabled; <!--{$arrErr[$key]|sfGetErrorColor}-->" value="<!--{$arrForm[$key].value|h}-->" class="box60" maxlength="<!--{$arrForm[$key].length}-->" />
      </td>
    </tr>
    <!--{assign var=key value="id_sps_hash"}-->
    <tr id="<!--{$key}-->">
      <th>ハッシュ値<span class="attention"> *</span></th>
      <td>
        <!--{if $arrErr[$key]}--><span class="attention"><!--{$arrErr[$key]}--></span><!--{/if}-->
        <input type="text" name="<!--{$key}-->" style="ime-mode:disabled; <!--{$arrErr[$key]|sfGetErrorColor}-->" value="<!--{$arrForm[$key].value|h}-->" class="box60" maxlength="<!--{$arrForm[$key].length}-->" />
      </td>
    </tr>
    <!--{assign var=key value="key_sps_3des"}-->
    <tr id="<!--{$key}-->">
      <th>3DES暗号化キー<span class="attention"> *</span></th>
      <td>
        <!--{if $arrErr[$key]}--><span class="attention"><!--{$arrErr[$key]}--></span><!--{/if}-->
        <input type="text" name="<!--{$key}-->" style="ime-mode:disabled; <!--{$arrErr[$key]|sfGetErrorColor}-->" value="<!--{$arrForm[$key].value|h}-->" class="box60" maxlength="<!--{$arrForm[$key].length}-->" />
      </td>
    </tr>
    <!--{assign var=key value="key_sps_3des_iv"}-->
    <tr id="<!--{$key}-->">
      <th>3DES初期化キー<span class="attention"> *</span></th>
      <td>
        <!--{if $arrErr[$key]}--><span class="attention"><!--{$arrErr[$key]}--></span><!--{/if}-->
        <input type="text" name="<!--{$key}-->" style="ime-mode:disabled; <!--{$arrErr[$key]|sfGetErrorColor}-->" value="<!--{$arrForm[$key].value|h}-->" class="box60" maxlength="<!--{$arrForm[$key].length}-->" />
      </td>
    </tr>
    <!--{assign var=key value="url_sps_api"}-->
    <tr id="<!--{$key}-->">
      <th>API接続先情報</th>
      <td>
        <!--{if $arrErr[$key]}--><span class="attention"><!--{$arrErr[$key]}--></span><!--{/if}-->
        <input type="text" name="<!--{$key}-->" style="ime-mode:disabled; <!--{$arrErr[$key]|sfGetErrorColor}-->" value="<!--{$arrForm[$key].value|h}-->" class="box60" maxlength="<!--{$arrForm[$key].length}-->" />
      </td>
    </tr>
    <!--{assign var=key value="url_sps_buyservice_api"}-->
    <tr id="<!--{$key}-->">
      <th>リンク型 購入要求接続先 URL</th>
      <td>
        <!--{if $arrErr[$key]}--><span class="attention"><!--{$arrErr[$key]}--></span><!--{/if}-->
        <input type="text" name="<!--{$key}-->" style="ime-mode:disabled; <!--{$arrErr[$key]|sfGetErrorColor}-->" value="<!--{$arrForm[$key].value|h}-->" class="box60" maxlength="<!--{$arrForm[$key].length}-->" />
      </td>
    </tr>
    <!--{assign var=key value="url_sps_registservice_api"}-->
    <tr id="<!--{$key}-->">
      <th>リンク型 顧客決済登録要求 接続先URL</th>
      <td>
        <!--{if $arrErr[$key]}--><span class="attention"><!--{$arrErr[$key]}--></span><!--{/if}-->
        <input type="text" name="<!--{$key}-->" style="ime-mode:disabled; <!--{$arrErr[$key]|sfGetErrorColor}-->" value="<!--{$arrForm[$key].value|h}-->" class="box60" maxlength="<!--{$arrForm[$key].length}-->" />
      </td>
    </tr>
    <!--{assign var=key value="url_sps_deleteservice_api"}-->
    <tr id="<!--{$key}-->">
      <th>リンク型 顧客決済登録消去 接続先URL</th>
      <td>
        <!--{if $arrErr[$key]}--><span class="attention"><!--{$arrErr[$key]}--></span><!--{/if}-->
        <input type="text" name="<!--{$key}-->" style="ime-mode:disabled; <!--{$arrErr[$key]|sfGetErrorColor}-->" value="<!--{$arrForm[$key].value|h}-->" class="box60" maxlength="<!--{$arrForm[$key].length}-->" />
      </td>
    </tr>
    <!--{assign var=key value="connect_from_ip"}-->
    <tr id="<!--{$key}-->">
      <th>通信元IPアドレス</th>
      <td>
        <!--{if $arrErr[$key]}--><span class="attention"><!--{$arrErr[$key]}--></span><!--{/if}-->
        <input type="text" name="<!--{$key}-->" style="ime-mode:disabled; <!--{$arrErr[$key]|sfGetErrorColor}-->" value="<!--{$arrForm[$key].value|h}-->" class="box60" maxlength="<!--{$arrForm[$key].length}-->" />
      </td>
    </tr>


    <!--{assign var=key value="enable_payment_type"}-->
    <tr id="<!--{$key}-->">
      <th>有効にする決済方法<span class="attention"> *</span></th>
      <td>
        <!--{if $arrErr[$key]}--><span class="attention"><!--{$arrErr[$key]}--></span><!--{/if}-->
        <strong>API型決済</strong><br />
        <!--{foreach from=$arrPayments item=payment key=payment_type_id}-->
            <!--{if $arrPaymentLinks[$payment_type_id] == $smarty.const.MDL_SPS_CONNECT_TYPE_API}-->
            <label><input type="checkbox" name="<!--{$key}-->[]" value="<!--{$payment_type_id|h}-->"
                <!--{if in_array($payment_type_id, $arrForm[$key].value)}--> checked="checked"<!--{/if}-->
                > <!--{$payment|h}-->
                </label><br />
            <!--{/if}-->
        <!--{/foreach}-->
        <strong>リンク型決済</strong><br />
        <!--{foreach from=$arrPayments item=payment key=payment_type_id}-->
            <!--{if $arrPaymentLinks[$payment_type_id] == $smarty.const.MDL_SPS_CONNECT_TYPE_LINK}-->
            <label><input type="checkbox" name="<!--{$key}-->[]" value="<!--{$payment_type_id|h}-->"
                <!--{if in_array($payment_type_id, $arrForm[$key].value)}--> checked="checked"<!--{/if}-->
                > <!--{$payment|h}-->
                </label><br />
            <!--{/if}-->
        <!--{/foreach}-->
        <strong>FreeCSV型決済</strong><br />
        <!--{foreach from=$arrPayments item=payment key=payment_type_id}-->
            <!--{if $arrPaymentLinks[$payment_type_id] == $smarty.const.MDL_SPS_CONNECT_TYPE_FREECSV}-->
            <label><input type="checkbox" name="<!--{$key}-->[]" value="<!--{$payment_type_id|h}-->"
                <!--{if in_array($payment_type_id, $arrForm[$key].value)}--> checked="checked"<!--{/if}-->
                > <!--{$payment|h}-->
                </label><br />
            <!--{/if}-->
        <!--{/foreach}-->

        <br />
        <span class="info">*決済方法毎の詳細設定は有効後に<a href="javascript:void(win_open('./basis/payment.php'));">支払方法設定画面</a>で行います。</span>
      </td>
    </tr>
    <!--{assign var=key value="type_cust_code"}-->
    <tr id="<!--{$key}-->">
      <th>顧客IDの方式<span class="attention"> *</span></th>
      <td>
        <!--{if $arrErr[$key]}--><span class="attention"><!--{$arrErr[$key]}--></span><!--{/if}-->
        <!--{html_radios name=$key options=$arrTypeCustCode selected=$arrForm[$key].value separator="&nbsp;"}-->
        <br />
        <span class="info">※ 変更すると決済サーバーの顧客IDの紐付けが切れますのでご注意下さい。</span>
      </td>
    </tr>

    <tr>
      <th>決済用テンプレート初期化</th>
      <td>
        <input type="checkbox" name="is_tpl_init" value="1" id="is_tpl_init" /><label for="is_tpl_init">決済用テンプレートを初期化する。</label>
        <br />
        <span class="attention">*ページやブロックのデザインテンプレートの内容を初期化します。</span><br />
        <span class="info">*決済に関するテンプレートを修正されている場合には取り扱いに注意して下さい。<br />
        初期化時には、初期化前のテンプレートデータを保存ディレクトリ内にバックアップが作成されます。
        </span>
      </td>
    </tr>


  </table>
  <div class="btn-area">
    <ul>
      <li>
        <a class="btn-action" href="javascript:;" onclick="document.form1.mode.value='register';document.body.style.cursor = 'wait';document.form1.submit();return false;"><span class="btn-next">この内容で登録する</span></a>
      </li>
    </ul>
  </div>

</form>

<!--{include file="`$smarty.const.TEMPLATE_ADMIN_REALDIR`admin_popup_footer.tpl"}-->


