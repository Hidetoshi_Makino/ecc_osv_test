<!--{*
 * Copyright(c) 2013 Spirit of Co.,Ltd. All rights reserved.
 * http://sbps.ec-cube.pro/
 * Updated: 2013/08/01
 *}-->

<!--{if $plg_sps_payment_type_id != ""}-->
    <!--{assign var=key value="is_enable_config"}-->
    <input type="hidden" name="<!--{$arrForm[$key].keyname}-->" value="1" />
<!--{/if}-->

<!--{foreach from=$arrSpsTpls item=sps_paymentinput_tpl}-->
    <!--{include file="$sps_paymentinput_tpl"}-->
<!--{/foreach}-->

<!--{if $plg_sps_payment_type_id != $smarty.const.SPS_CONVENI_API_PAYID}-->

            <tr>
                <th>支払い説明タイトル</th>
                <td>
                    <!--{assign var=key value="mail_title"}-->
                    <span class="attention"><!--{$arrErr[$key]}--></span>
                    <input type="text" name="<!--{$arrForm[$key].keyname}-->" value="<!--{$arrForm[$key].value|h}-->" size="60" class="box200" maxlength="<!--{$arrForm[$key].length}-->" style="<!--{$arrErr[$key]|sfGetErrorColor}-->" />                    <span class="attention">（上限<!--{$arrForm[$key].length}-->文字）</span>
                </td>
            </tr>
            <tr>
                <th>支払い説明本文</th>
                <td>
                    <!--{assign var=key value="mail_body"}-->
                    <span class="attention"><!--{$arrErr[$key]}--></span>                                                                        <textarea name="<!--{$arrForm[$key].keyname}-->" maxlength="<!--{$arrForm[$key].length}-->" cols="60" rows="4" class="area60" style="<!--{$arrErr[$key]|sfGetErrorColor}-->"><!--{"\n"}--><!--{$arrForm[$key].value|h}--></textarea>
                    <span class="attention">（上限<!--{$arrForm[$key].length}-->文字）</span>
                    <span style="font-size:80%"><br />
                    ※有効にするにはタイトルと本文の両方を指定する必要があります。
                    </span>
                </td>
            </tr>
<!--{/if}-->

<!--{if $plg_sps_payment_type_id != ""}-->
            <tr>
                <th>自由欄</th>
                <td>
                    <!--{assign var=key value="free3"}-->
                    <span class="attention"><!--{$arrErr[$key]}--></span>
                    <input type="text" name="<!--{$arrForm[$key].keyname}-->" value="<!--{$arrForm[$key].value|h}-->" size="30" class="box30" maxlength="<!--{$arrForm[$key].length}-->" style="<!--{$arrErr[$key]|sfGetErrorColor}-->" />                    <span class="attention">（上限<!--{$arrForm[$key].length}-->文字）</span>
                </td>
            </tr>
<!--{/if}-->
