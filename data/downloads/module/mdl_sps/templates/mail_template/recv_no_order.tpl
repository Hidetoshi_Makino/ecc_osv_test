<!--{*
 * Copyright(c) 2013 Spirit of Co.,Ltd. All rights reserved.
 * http://sbps.ec-cube.pro/
 * Updated: 2013/08/01
 *}-->

受注情報に存在しないオーダーIDの決済結果を受信しました。

受注ID: <!--{$arrParam.free3|h}-->
決済オーダーID： <!--{$arrParam.order_id|h}-->
利用金額: <!--{$arrParam.amount|h}-->円
決済方法: <!--{$arrParam.pay_method|h}-->
受付日時: <!--{$arrParam.request_date|h}-->
処理日時: <!--{$arrParam.res_date|h}-->

大変お手数ですが、ご確認お願い致します。

結果通知プログラムURLに結果を返却した際、EC-CUBE側（dtb_order）
に該当データが存在しないため「不一致」となり、本メールが送信され
ています。

まずは、EC-CUBE管理画面とソフトバンクペイメントサービスのショップ
管理画面とで決済データをご確認いただき、決済結果に相違がないこと
をご確認ください。

