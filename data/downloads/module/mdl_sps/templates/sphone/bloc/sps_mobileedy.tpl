<!--{*
 * Copyright(c) 2013 Spirit of Co.,Ltd. All rights reserved.
 * http://sbps.ec-cube.pro/
 * Updated: 2013/08/01
 * sps_mobileedy.tpl
 *}-->
<script type="text/javascript">//<![CDATA[
var send = true;

function fnCheckSubmit(mode) {
    $('#payment_form_body').slideToggle();
    $('#payment_form_loading').slideToggle();

    if(send) {
        send = false;
        fnModeSubmit(mode,'','');
        return false;
    } else {
        alert("只今、処理中です。しばらくお待ち下さい。");
        return false;
    }
}

function fnAutoLoadSubmit() {

    var mode = 'next';
    send = false;
    setTimeout("fnModeSubmit('next','','')", 500);
}

function fnAutoLoadSubmitSPS() {
    setTimeout("document.form_sps.submit()", 500);
}

$(function() {
    <!--{$tpl_payment_onload}-->
});
//]]>
</script>
    <!--{* SPS移動用フォームは別フォーム *}-->
    <!--{if $tpl_next_form == 'form_sps'}-->
    </form>

    <form name="form_sps" id="form_sps" method="post" action="<!--{$sps_url|h}-->">
    <!--{foreach from=$arrSpsData item=data key=key}-->
        <!--{if !is_array($data)}-->
            <input type="hidden" name="<!--{$key}-->" value="<!--{$data|h}-->" />
        <!--{else}-->
            <!--{foreach from=$data item=ddata key=dkey}-->
                <!--{foreach from=$ddata item=d3data key=d3key}-->
                <input type="hidden" name="<!--{$d3key}-->[]" value="<!--{$d3data|h}-->" />
                <!--{/foreach}-->
            <!--{/foreach}-->
        <!--{/if}-->
    <!--{/foreach}-->
    </form>
    <!--{/if}-->


        <!--{if count($arrErr) > 0}-->
        <section id="payment_form_body">
            <div class="information end">
                <p>決済エラー<br />
                大変お手数ですがエラーの内容を確認して、再度お手続きを進めて下さい。</p>
                <!--{foreach from=$arrErr item=err}-->
                <p class="attention"><!--{$err|h}--></p>
                <!--{/foreach}-->
            </div>
            <div class="btn_area">
                <ul class="btn_btm">
                        <li>
                            <a rel="external" href="javascript:void(fnCheckSubmit('return'));" class="btn_back"/>戻る</a>
                        </li>
                </ul>
            </div>

       </section><!--{* /payment_form_body *}-->

        <!--{else}-->
        <section id="payment_form_loading">
            <div class="information end">
                <p>決済処理中です。そのまま、お待ち下さい。</p>
                <span class="attention">※画面が切り替るまで少々時間がかかる場合がございますが、そのままお待ちください。</span>
            </div>
            <div class="bubbleBox hot">
                <div class="bubble_announce clearfix">
                    <img src="<!--{$smarty.const.MDL_SPS_MEDIAFILE_URL}-->loading.gif" />
                </div>
            </div>
            <div class="btn_area">
                <ul class="btn_btm">
                    <li>
                            <a rel="external" href="javascript:void(fnCheckSubmit('return'));" class="btn_back"/>戻る</a>
                    </li>
                    <!--{if $tpl_next_form == 'form_sps'}-->
                    <li>
                            <a rel="external" href="javascript:document.form_sps.submit();return false;" class="btn">次へ</a>
                    </li>
                    <!--{else}-->
                    <li>
                            <a rel="external" href="javascript:void(fnCheckSubmit('next'));" class="btn">次へ</a>
                    </li>
                    <!--{/if}-->
                </ul>
            </div>

       </section><!--{* /payment_form_body *}-->
       <!--{/if}-->


    <form name="dummy">

