<!--{*
 * Copyright(c) 2013 Spirit of Co.,Ltd. All rights reserved.
 * http://sbps.ec-cube.pro/
 * Updated: 2013/08/01
 * sps_creditapi.tpl
 *}-->
<script type="text/javascript">//<![CDATA[
var send = true;

function fnCheckSubmit(mode) {
    $('#payment_form_body').slideToggle();
    $('#payment_form_loading').slideToggle();

    if(send) {
        send = false;
        fnModeSubmit(mode,'','');
        return false;
    } else {
        alert("只今、処理中です。しばらくお待ち下さい。");
        return false;
    }
}

function fnAutoLoadSubmit() {

    var mode = 'next';
    send = false;
    setTimeout("fnModeSubmit('next','','')", 500);
}

function fnAutoLoadSubmitSPS() {
    setTimeout("document.form_sps.submit()", 500);
}

function next(now, next) {
    if (now.value.length >= now.getAttribute('maxlength')) {
    next.focus();
    }
}

function changeDeal(obj) {
    if($(obj).val() == '61') {
        $('#divide_times').show();
    }else{
        $('#divide_times').hide();
    }
}

$(function() {
    if ($('#dealings_type').val() == '61') {
        $('#divide_times').show();
    }

    <!--{$tpl_payment_onload}-->
});
//]]>
</script>

        <section id="payment_form_loading" style="display:none;">
            <div class="information end">
                <p>決済処理中です。そのまま、お待ち下さい。</p>
                <span class="attention">※画面が切り替るまで少々時間がかかる場合がございますが、そのままお待ちください。</span>
            </div>
            <div class="bubbleBox hot">
                <div class="bubble_announce clearfix">
                    <img src="<!--{$smarty.const.MDL_SPS_MEDIAFILE_URL}-->loading.gif" />
                </div>
            </div>
        </section>

        <section id="payment_form_body">
            <div class="information">
                <p>下記項目にご入力ください。「<span class="attention">※</span>」印は入力必須項目です。<br />
                入力後、一番下の「決済する」ボタンをクリックしてください。</p>

                <!--{if count($arrErr) > 0}-->
                <p>決済エラー<br />
                大変お手数ですがエラーの内容を確認して、再度お手続きを進めて下さい。</p>
                <!--{foreach from=$arrErr item=err}-->
                <p class="attention"><!--{$err}--></p>
                <!--{/foreach}-->
                <!--{/if}-->
            </div>
            <h3 class="subtitle"><!--{$tpl_title|h}-->番号入力</h3>
                <dl class="form_entry">
                    <dt>
                        カード番号<span class="attention">※</span>
                    </dt>
                    <dd>
                      <!--{assign var=key1 value="card_no01"}-->
                      <!--{assign var=key2 value="card_no02"}-->
                      <!--{assign var=key3 value="card_no03"}-->
                      <!--{assign var=key4 value="card_no04"}-->
                      <span class="attention"><!--{$arrErr[$key1]}--></span>
                      <span class="attention"><!--{$arrErr[$key2]}--></span>
                      <span class="attention"><!--{$arrErr[$key3]}--></span>
                      <span class="attention"><!--{$arrErr[$key4]}--></span>
                      <input type="text" name="<!--{$key1}-->" value="<!--{$arrForm[$key1].value|escape}-->" maxlength="4" style="ime-mode: disabled; <!--{$arrErr[$key1]|sfGetErrorColor}-->"  class="boxShort data-role-none">
                      &minus;
                      <input type="text" name="<!--{$key2}-->" value="<!--{$arrForm[$key2].value|escape}-->" maxlength="4" style="ime-mode: disabled; <!--{$arrErr[$key1]|sfGetErrorColor}-->"  class="boxShort data-role-none">
                      &minus;
                      <input type="text" name="<!--{$key3}-->" value="<!--{$arrForm[$key3].value|escape}-->" maxlength="4" style="ime-mode: disabled; <!--{$arrErr[$key1]|sfGetErrorColor}-->"  class="boxShort data-role-none">
                      &minus;
                      <input type="text" name="<!--{$key4}-->" value="<!--{$arrForm[$key4].value|escape}-->" maxlength="4" style="ime-mode: disabled; <!--{$arrErr[$key1]|sfGetErrorColor}-->"  class="boxShort data-role-none">
                      <br /><span class="attention">※ご本人名義のカードをご使用ください。</span><br />
                      <p class="mini">半角入力（例：1234-5678-9012-3456）</p>
                    </dd>
                    <dt>
                        カード有効期限<span class="attention">※</span>
                    </dt>
                    <dd>
                        <!--{assign var=key1 value="card_month"}-->
                        <!--{assign var=key2 value="card_year"}-->
                        <span class="attention"><!--{$arrErr[$key1]}--></span>
                        <span class="attention"><!--{$arrErr[$key2]}--></span>
                        <select name="<!--{$key1}-->" style="<!--{$arrErr[$key1]|sfGetErrorColor}-->" class="boxShort data-role-none">
                        <option value="">&minus;&minus;</option>
                        <!--{html_options options=$arrMonth selected=$arrForm[$key1].value}-->
                        </select>月
                        &nbsp;/&nbsp;
                        <select name="<!--{$key2}-->" style="<!--{$arrErr[$key2]|sfGetErrorColor}-->" class="boxShort data-role-none">
                        <option value="">&minus;&minus;</option>
                        <!--{html_options options=$arrYear selected=$arrForm[$key2].value}-->
                        </select>年
                    </dd>
                <!--{if $arrPayment.is_security_code == '1'}-->
                    <dt>
                        セキュリティコード <span class="attention">※</span>
                    </dt>
                    <dd>
                        <!--{assign var=key value="security_code"}-->
                        <span class="attention"><!--{$arrErr[$key]}--></span>
                        <input type="text" name="<!--{$key}-->" value="<!--{$arrForm[$key].value|h}-->" maxlength="<!--{$arrForm[$key].length}-->" style="ime-mode: disabled; <!--{$arrErr[$key]|sfGetErrorColor}-->"  size="4"  class="boxShort data-role-none"/>
                        <p class="mini"><span class="attention">※カード裏面の3桁または4桁の番号を記入して下さい。</span>半角入力 (例: 123)</p>
                    </dd>
                <!--{/if}-->
                    <dt>
                        支払い方法<span class="attention">※</span>
                    </dt>
                    <dd>
                        <!--{assign var=key1 value="dealings_type"}-->
                        <span class="attention"><!--{$arrErr[$key1]}--></span>
                        <select name="<!--{$key1}-->" id="<!--{$key1}-->" style="<!--{$arrErr[$key1]|sfGetErrorColor}-->" onchange="changeDeal(this);">
                        <!--{html_options options=$arrPaymentClass selected=$arrForm[$key1].value}-->
                        </select>
                    </dd>
                <!--{assign var=key1 value="divide_times"}-->
                <span id="<!--{$key1}-->" style="display:none;">
                    <dt>
                        分割回数<span class="attention">※</span>
                    </dt>
                    <dd>
                        <span class="attention"><!--{$arrErr[$key1]}--></span>
                        <select name="<!--{$key1}-->" style="<!--{$arrErr[$key1]|sfGetErrorColor}-->">
                        <!--{html_options options=$arrDivideTimes selected=$arrForm[$key1].value}-->
                        </select>
                    </dd>
                </span>
                <!--{if $arrPayment.is_regist_card == '1'}-->
                    <dt>
                        カード情報登録
                    </dt>
                    <dd>
                        <!--{assign var=key value="cust_manage_flg"}-->
                        <span class="attention"><!--{$arrErr[$key]}--></span>

                        <input type="checkbox" name="<!--{$key}-->" value="1" class="radio_btn data-role-none"  <!--{if $arrForm[$key].value != ""}-->checked<!--{/if}--> >
                        <label for="<!--{$key}-->">このカードを登録する。</label>
                        <p class="mini">カード情報を登録すると次回より入力無しで購入出来ます。<br />
                        カード情報は当店では保管いたしません。
                        <br />委託する決済代行会社にて安全に保管されます。</p>
                    </dd>
                <!--{/if}-->
                </dl>

            <div class="information">
                <p>
                        以上の内容で間違いなければ、下記「次へ」ボタンをクリックしてください。<br />
                        <span class="attention">※画面が切り替るまで少々時間がかかる場合がございますが、そのままお待ちください。</span>
                </p>
            </div>

            <div class="btn_area">
                <ul class="btn_btm">
                    <li>
                         <a rel="external" href="javascript:void(fnCheckSubmit('next'));" class="btn"/>次へ</a>
                    </li>
                    <li>
                        <a rel="external" href="javascript:void(fnCheckSubmit('return'));" class="btn_back"/>戻る</a>
                    </li>
                </ul>
            </div>

       </section><!--{* /payment_form_body *}-->



