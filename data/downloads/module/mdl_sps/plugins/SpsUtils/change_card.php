<?php
/*
 * Copyright(c) 2013 Spirit of Co.,Ltd. All rights reserved.
 * http://sbps.ec-cube.pro/
 */
require_once('../require.php');
require_once(MODULE_REALDIR . 'mdl_sps/inc/include.php');
require_once(MDL_SPS_CLASSEX_PATH . 'page_extends/LC_Page_Mypage_ChangeCard_Ex.php');

// }}}
// {{{ generate page

$objPage = new LC_Page_Mypage_ChangeCard_Ex();
$objPage->init();
$objPage->process();
