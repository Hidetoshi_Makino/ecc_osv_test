<!--{*
 * Copyright(c) 2013 Spirit of Co.,Ltd. All rights reserved.
 * http://sbps.ec-cube.pro/
 *}-->
<!--{include file="`$smarty.const.TEMPLATE_ADMIN_REALDIR`admin_popup_header.tpl"}-->
<!--{include file='css/contents.tpl'}-->
<script type="text/javascript">
<!--
self.moveTo(20,20);self.focus();

function win_open(URL){
    var WIN;
    WIN = window.open(URL);
    WIN.focus();
}

function fnSpsMasterOpen (master_data_name) {
    $('input[name="mode"]').val('show');
    $('input[name="master_data_name"]').val(master_data_name);
    $('#form1').attr('action', '../system/masterdata.php');
    document.form1.submit();
}

function fnPlgSpsEveryModeSubmit(mode) {
    switch(mode) {
    case 'add_product_type':
        if(!window.confirm('商品種別を追加してよろしいですか？')) {
            return;
        }
        break;
    default:
        break;
    }
    fnModeSubmit(mode,'','');
}

//-->
</script>
<style type="text/css">
.info { font-size: 90%; };
</style>



<h1><span class="title"><!--{$tpl_subtitle}--></span></h1>

<h2>継続課金用設定</h2>
<form name="form1" id="form1" method="post" action="<!--{$smarty.server.REQUEST_URI|escape}-->">
  <input type="hidden" name="mode" value="" />
  <input type="hidden" name="<!--{$smarty.const.TRANSACTION_ID_NAME}-->" value="<!--{$transactionid}-->" />
  <input type="hidden" name="master_data_name" value="">
  <table class="form">
    <colgroup width="20%">
    <colgroup width="80%">
    <!--{if $arrErr.err != ""}-->
    <tr>
      <td colspan="2"><!--{$arrErr.err}--></td>
    </tr>
    <!--{/if}-->

    <!--{assign var=key value="every_product_type"}-->
    <tr id="<!--{$key}-->">
      <th>継続課金用 商品種別<span class="attention"> *</span></th>
      <td>
        <!--{if $arrErr[$key]}--><span class="attention"><!--{$arrErr[$key]}--></span><!--{/if}-->
        <!--{html_radios name=$key options=$arrEnableType selected=$arrForm[$key] separator="&nbsp;"}-->
        <br />
          <a class="btn-tool" href="javascript:;" name="product_type_add" onclick="fnPlgSpsEveryModeSubmit('add_product_type'); return false;">&nbsp;&nbsp;&lt;-&nbsp;新たに追加する&nbsp;&nbsp;</a><br />
          <span>新たに追加すると対応する配送方法も自動的に追加されます。名称変更や削除をしたい場合は<a href="javascript:void();" onclick="fnSpsMasterOpen('mtb_product_type');">マスターデータ管理</a>で設定して下さい。<br />配送方法の名称や詳細、配送料金を指定したい場合は、<a href="./delivery.php">配送方法設定</a>で設定して下さい。</span>

      </td>
    </tr>
    <tr>
      <th>ユーザー解約機能の有効化</th>
      <td>
        <!--{assign var=key value="every_order_cancel_enable"}-->
        ユーザーによる解約機能を有効にする場合はチェックを入れて下さい &minus;&gt;<input type="checkbox" name="<!--{$key}-->" value="1" <!--{if $arrForm[$key] == '1'}-->checked<!--{/if}--> /><br />
      </td>
    </tr>

    <tr>
      <th>キャンペーン設定</th>
      <td>
        <!--{assign var=key value="every_order_cancel_enable"}-->
        キャンペーン（初月無料）を有効にする場合はチェックを入れて下さい &minus;&gt;<input type="checkbox" name="<!--{$key}-->" value="1" <!--{if $arrForm[$key] == '1'}-->checked<!--{/if}--> /><br />
      </td>
    </tr>

  </table>
  <div class="btn-area">
    <ul>
      <li>
        <a class="btn-action" href="javascript:;" onclick="document.form1.mode.value='register';document.body.style.cursor = 'wait';document.form1.submit();return false;"><span class="btn-next">この内容で登録する</span></a>
      </li>
    </ul>
  </div>

</form>

<!--{include file="`$smarty.const.TEMPLATE_ADMIN_REALDIR`admin_popup_footer.tpl"}-->
