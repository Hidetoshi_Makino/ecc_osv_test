<?php
/*
 * Copyright(c) 2013 Spirit of Co.,Ltd. All rights reserved.
 * http://www.spiritof.jp/
 */

/**
 * プラグインの処理クラス
 */
class LC_SpsEvery {

    function getInstallFiles(&$objPlugin) {
        $arrFiles = array(
            array(
                'src' => PLUGIN_UPLOAD_REALDIR . $objPlugin->plugin_name . "/logo.png",
                'dst' => PLUGIN_HTML_REALDIR . $objPlugin->plugin_name ."/logo.png",
                ),
        );
        return $arrFiles;
    }

    function getEnableFiles(&$objPlugin) {
    }

    function actionInstall(&$objPlugin) {
    }

    function actionUninstall(&$objPlugin) {
    }

    function actionEnable(&$objPlugin) {
    }

    function actionDisable(&$objPlugin) {
    }

    function actionPrefilterTransform($class_name, &$source, &$objPage, $filename, &$objPlugin) {
        $template_dir = MDL_SPS_TEMPLATE_PATH;
        $template_file = '';
        $selecter = '';
        $selecter_method = '';
        $index = NULL;
        $is_require = false;
        $find_selecter = '';
        $find_index = NULL;
        $find_is_require = false;

        switch($objPage->arrPageLayout['device_type_id']){
            case DEVICE_TYPE_MOBILE:
                $template_dir .= 'mobile/';
                break;
            case DEVICE_TYPE_SMARTPHONE:
                $template_dir .= 'sphone/';
                break;
            case DEVICE_TYPE_PC:
                $template_dir .= 'default/';
                break;
            case DEVICE_TYPE_ADMIN:
            default:
                $template_dir .= 'admin/';
/*
                if(preg_match('/^LC_Page_Admin.*_Ex$/', $class_name)) {
                    if(strpos($filename, 'basis/payment_input.tpl') !== false) {
                        $template_file = 'basis_payment_input_add.tpl';
                        $selecter = 'table.form';
                        $selecter_method = 'appendChild';
                    }
                }
*/
                break;
        }

        if ($selecter && $selecter_method) {
            $objTransform = new SC_Helper_Transform($source);
            switch ($selecter_method) {
            case 'removeElement':
                if ($find_selecter) {
                    $objTransform->select($selecter, $index, $is_require)->find($find_selecter, $find_index, $find_is_require)->$method();
                } else {
                    $objTransform->select($selecter, $index, $is_require)->$selecter_method();
                }
                break;
            case 'insertAfter':
            case 'insertBefore':
            case 'appendFirst':
            case 'appendChild':
            case 'replaceElement':
            case 'appendFirst':
            default:
                if (!file_exists($template_dir . $template_file)) {
                    SC_Utils_Ex::gfPrintLog('Not found plugin template:' . $template_dir . $template_file);
                    return;
                }
                if ($find_selecter) {
                    $objTransform->select($selecter, $index, $is_require)
                        ->find($find_selecter, $find_index, $find_is_require)
                        ->$selecter_method(file_get_contents($template_dir . $template_file));
                } else {
                    $objTransform->select($selecter, $index, $is_require)->$selecter_method(file_get_contents($template_dir . $template_file));
                }
                break;
            }
            $source = $objTransform->getHTML();
        }
    }

    function actionHook($class_name, $hook_point, &$objPage, $objPlugin) {
        switch ($class_name) {
        case 'LC_Page_Admin_Basis_DeliveryInput_Ex':
            if ($hook_point == 'after') {
                if ($objPlugin->getValue('every_product_type') == $objPage->arrForm['product_type_id']['value']) {
                    $this->checkEveryPayments($objPage->arrPayments);
                } else {
                    $this->checkNoEveryPayments($objPage->arrPayments);
                }
            }
        }
    }

    function checkNoEveryPayments(&$arrPayments, &$arrForm) {
        foreach ($arrPayments as $key => $arrPayment) {
            $arrConfig = SC_Util_SPS_Ex::getPaymentTypeConfig($data['payment_id']);
            switch($arrConfig[MDL_SPS_PAYMENT_COL_PAYID]) {
            case SPS_CREDIT_EVERY_LINK_PAYID:
            case SPS_CREDIT_EVERY_API_PAYID:
            case SPS_AU_EVERY_LINK_PAYID:
            case SPS_DOCOMO_EVERY_LINK_PAYID:
                unset($arrPayments[$key]);
                break;
            default:
                break;
            }
        }
    }

    function checkEveryPayments(&$arrPayments, &$arrForm) {
        foreach ($arrPayments as $key => $arrPayment) {
            $arrConfig = SC_Util_SPS_Ex::getPaymentTypeConfig($data['payment_id']);
            switch($arrConfig[MDL_SPS_PAYMENT_COL_PAYID]) {
            case SPS_CREDIT_EVERY_LINK_PAYID:
            case SPS_CREDIT_EVERY_API_PAYID:
            case SPS_AU_EVERY_LINK_PAYID:
            case SPS_DOCOMO_EVERY_LINK_PAYID:
                break;
            default:
                unset($arrPayments[$key]);
            }
        }
    }
    function LC_Page_Admin_Basis_PaymentInput_Ex_after(&$objPage) {
        $objFormParam = new SC_FormParam_Ex();

        $payment_id = $objPage->tpl_payment_id;
        if (SC_Utils_Ex::isBlank($payment_id)) {
            return;
        }

        $arrConfig = SC_Util_SPS_Ex::getPaymentTypeConfig($payment_id);
        $payment_type_id = $arrConfig[MDL_SPS_PAYMENT_COL_PAYID];
        if (SC_Utils_Ex::isBlank($payment_type_id)) {
            return;
        }
        $objPage->plg_sps_payment_type_id = $payment_type_id;

        $objPage->arrSpsTpls = array();
        $arrFiles = SC_Helper_FileManager_Ex::sfGetFileList(MDL_SPS_TEMPLATE_PATH . 'admin/bloc_paymentinput/');
        foreach ($arrFiles as $arrFile) {
            if (!$arrFile['is_dir']) {
                $objPage->arrSpsTpls[] = $arrFile['file_path'];
            }
        }

        $objPayment =& SC_Mdl_SPS_Helper_Ex::getPaymentHelper($payment_type_id);

        $objPayment->initParamPaymentInput($objPage, $objFormParam, $arrConfig);
        $objFormParam->setParam($_POST);
        $objFormParam->convParam();

        switch ($objPage->getMode()) {
        case 'edit':
            $arrErr = $objPayment->checkErrorPaymentInput($objFormParam);
            $objPage->arrErr = array_merge((array)$objPage->arrErr, (array)$arrErr);
            if (SC_Utils_Ex::isBlank($objPage->arrErr)) {
                SC_Util_SPS_Ex::setPaymentTypeConfig($payment_id, $objFormParam->getHashArray());
            } else {
                $objPage->tpl_onload = '';
            }
            break;
        default:
            break;
        }
        $objPage->arrForm = array_merge((array)$objPage->arrForm, (array)$objFormParam->getFormParamList());
    }

}
