<?php
/**
 *
 * @copyright 2013 Spirit of Co.,Ltd. All Rights Reserved.
 * @link http://sbps.ec-cube.pro/
 *
 */

// モジュール基本設定
define('MDL_SPS', true);
define("MDL_SPS_CODE", 'mdl_sps');
define('MDL_SPS_VERSION', '4.3.1');

// モジュールデバッグ設定
// true の場合、顧客の入力内容がログに残りますので運用時は必ずfalseにして下さい。
define('MDL_SPS_DEBUG', false);

// 表示名など
define('MDL_SPS_SERVICE_NAME', 'ソフトバンク・ペイメント・サービス');
define('MDL_SPS_MODULE_NAME', 'ソフトバンク・ペイメント・サービス決済モジュール');
define('MDL_SPS_COMPANY_NAME', 'ソフトバンク・ペイメント・サービス株式会社');
define('MDL_SPS_INFO_URI', 'http://sbps.ec-cube.pro/');


// モジュールパス設定
define('MDL_SPS_PATH', MODULE_REALDIR . 'mdl_sps/');
define('MDL_SPS_CLASS_PATH', MDL_SPS_PATH . 'class/');
define('MDL_SPS_CLASSEX_PATH', MDL_SPS_PATH . 'class_extends/');
define('MDL_SPS_MODULE_PATH', MDL_SPS_PATH . 'module/');
define('MDL_SPS_TEMPLATE_PATH', MDL_SPS_PATH . 'templates/');
define('MDL_SPS_INCLUDEFILE_PATH', MDL_SPS_PATH . 'inc/include.php');

define('MDL_SPS_HELPER_PATH', MDL_SPS_PATH . 'class/helper/');
define('MDL_SPS_HELPEREX_PATH', MDL_SPS_PATH . 'class_extends/helper_extends/');

// エラーメッセージファイル
define('MDL_SPS_ERROR_CODE_MSG_FILE', MDL_SPS_PATH .'inc/sps_errors.txt');

// 決済通知先
define('MDL_SPS_SETTLEMENT_FILE', 'sps_recv.php');
define("MDL_SPS_SETTLEMENT_URL", HTTPS_URL . USER_DIR . MDL_SPS_SETTLEMENT_FILE);
define("MDL_SPS_SETTLEMENT_PATH", USER_REALDIR . MDL_SPS_SETTLEMENT_FILE);

// メディアファイル設置先
define('MDL_SPS_MEDIAFILE_URL', HTTPS_URL . USER_DIR . MDL_SPS_CODE . '/');
define('MDL_SPS_MEDIAFILE_PATH', USER_REALDIR . MDL_SPS_CODE . '/');

// 非会員時の顧客IDプレフィクス
define('SPS_NON_MEMBER_PREFIX', 'reg_sps');

// 会員顧客IDのプレフィクス
// サイトによって区別したい場合に付与します。
define('SPS_MEMBER_PREFIX', '');

// 受注データ毎の情報保存カラム設定
define('MDL_SPS_ORDER_COL_REQDATE', 'memo01');  // リクエスト日時(commit)
define('MDL_SPS_ORDER_COL_PAYVIEW', 'memo02');  // 受注表示用 (シリアライズ)
define('MDL_SPS_ORDER_COL_REQTYPE', 'memo03');   //決済状態 (null or 0: 与信、1:自動売上、2 cancel、3 error、4 commit
define('MDL_SPS_ORDER_COL_PAYID', 'memo04');    // payment_type_id
define('MDL_SPS_ORDER_COL_RESDATA', 'memo05');   //リクエスト結果
define('MDL_SPS_ORDER_COL_TRANSID', 'memo06');   //トラッキングID
define('MDL_SPS_ORDER_COL_PAYDATA', 'memo07');   //最新データセット(シリアライズ)
define('MDL_SPS_ORDER_COL_EVERY', 'memo08'); //継続課金状態
define('MDL_SPS_ORDER_COL_PAYLOG', 'memo09');    //ログデータ
define('MDL_SPS_ORDER_COL_TOKEN', 'memo10');

// 支払い方法毎の情報保存カラム設定
define('MDL_SPS_PAYMENT_COL_PAYID', 'memo03');
define('MDL_SPS_PAYMENT_COL_CONFIG', 'memo05');

// 支払い方法毎の表示名
define('SPS_CREDIT_LINK_NAME', 'クレジットカード');
define('SPS_CREDIT3D_LINK_NAME', 'クレジット３Ｄセキュア');
define('SPS_CREDIT_REGIST_LINK_NAME', 'クレジット登録');
define('SPS_CREDIT3D_REGIST_LINK_NAME', 'クレジット登録(３Ｄセキュア)');
define('SPS_SOFTBANK_LINK_NAME', 'ソフトバンクまとめて支払い');
define('SPS_DOCOMO_LINK_NAME', 'ドコモケータイ払い');
define('SPS_AU_LINK_NAME', 'auかんたん決済');
define('SPS_WEBMONEY_LINK_NAME', 'WebMoney');
define('SPS_BITCASH_LINK_NAME', 'BitCash');
define('SPS_NETCASH_LINK_NAME', 'NetCash');
define('SPS_YWALLET_LINK_NAME', 'Yahoo!ウォレット');
define('SPS_YWALLETD_LINK_NAME', 'Yahoo!ウォレットデジコン');
define('SPS_RAKUTEN_LINK_NAME', '楽天ID決済');
define('SPS_RAKUTEN2_LINK_NAME', '楽天ID決済(非ギャザ版)');
define('SPS_ALIPAY_LINK_NAME', 'Alipay');
define('SPS_PAYPAL_LINK_NAME', 'PayPal');
define('SPS_NETMILE_LINK_NAME', 'NetMile');
define('SPS_SAISON_LINK_NAME', '永久不滅ポイント決済');
define('SPS_CYBEREDY_LINK_NAME', 'サイバーEdy');
define('SPS_MOBILEEDY_LINK_NAME', 'モバイルEdy');
define('SPS_SUICA_LINK_NAME', 'Suica');
define('SPS_CONVENI_LINK_NAME', 'コンビニ払い');
define('SPS_PAYEASY_LINK_NAME', 'PayEasy');
define('SPS_JNB_LINK_NAME', 'ジャパンネット銀行');
define('SPS_BANKTRANS_LINK_NAME', '総合振込');
define('SPS_GMONEY_LINK_NAME', 'G-Money');
define('SPS_POLLET_LINK_NAME', 'Pollet決済');
define('SPS_OEMPIN_LINK_NAME', 'oemPin決済');
define('SPS_CREDIT_API_NAME', 'クレジットカード(API)');
define('SPS_CREDIT_FCSV_NAME', 'クレジットカード(FCSV)');
define('SPS_CREDIT3D_FCSV_NAME', 'クレジット３Ｄセキュア(FCSV)');
define('SPS_CREDIT_REGIST_API_NAME', '登録済みクレジット(API)');
define('SPS_REGIST_CREDIT_API_NAME', 'クレジット登録(API)');
define('SPS_BITCASH_FCSV_NAME', 'BitCash(クレジットカード)(FCSV)');
define('SPS_CONVENI_API_NAME', 'コンビニ払い(API)');
define('SPS_PAYEASY_API_NAME', 'PayEasy(API)');
define('SPS_BANKTRANS_API_NAME', '総合振込(API)');

define('SPS_CREDIT_EVERY_LINK_NAME', 'クレジット３Ｄセキュア継続売上（簡易継続）');
define('SPS_CREDIT_EVERY_API_NAME', 'クレジット継続売上（簡易継続API）');
define('SPS_AU_EVERY_LINK_NAME', 'auかんたん決済継続売上（簡易継続）');
define('SPS_DOCOMO_EVERY_LINK_NAME', 'ドコモケータイ払い（簡易継続）');


// 決済タイプ (EC-CUBE内の決済種別 識別ID)
define('SPS_CREDIT_LINK_PAYID', 1); // compatible id
define('SPS_CREDIT3D_LINK_PAYID', 6);       // compatible id
define('SPS_CREDIT_REGIST_LINK_PAYID', 7);
define('SPS_CREDIT3D_REGIST_LINK_PAYID', 8);
define('SPS_SOFTBANK_LINK_PAYID', 9);
define('SPS_DOCOMO_LINK_PAYID', 10);
define('SPS_AU_LINK_PAYID', 11);
define('SPS_WEBMONEY_LINK_PAYID', 4);   // compatible id
define('SPS_BITCASH_LINK_PAYID', 12);
define('SPS_NETCASH_LINK_PAYID', 13);
define('SPS_YWALLET_LINK_PAYID', 5);    // compatible id
define('SPS_YWALLETD_LINK_PAYID', 14);
define('SPS_RAKUTEN_LINK_PAYID', 15);
define('SPS_ALIPAY_LINK_PAYID', 16);
define('SPS_PAYPAL_LINK_PAYID', 17);
define('SPS_NETMILE_LINK_PAYID', 18);
define('SPS_SAISON_LINK_PAYID', 19);
define('SPS_CYBEREDY_LINK_PAYID', 20);
define('SPS_MOBILEEDY_LINK_PAYID', 21);
define('SPS_SUICA_LINK_PAYID', 22);
define('SPS_CONVENI_LINK_PAYID', 2);    // compatible id
define('SPS_PAYEASY_LINK_PAYID', 3);    // compatible id
define('SPS_JNB_LINK_PAYID', 23);
define('SPS_BANKTRANS_LINK_PAYID', 24);
define('SPS_GMONEY_LINK_PAYID', 25);
define('SPS_POLLET_LINK_PAYID', 26);
define('SPS_OEMPIN_LINK_PAYID', 27);

define('SPS_CREDIT_API_PAYID', 28);
define('SPS_CREDIT_FCSV_PAYID', 29);
define('SPS_CREDIT3D_FCSV_PAYID', 30);
define('SPS_CREDIT_REGIST_API_PAYID', 31);
define('SPS_BITCASH_FCSV_PAYID', 32);
define('SPS_CONVENI_API_PAYID', 33);
define('SPS_PAYEASY_API_PAYID', 34);
define('SPS_BANKTRANS_API_PAYID', 35);
define('SPS_REGIST_CREDIT_API_PAYID', 36);
define('SPS_RAKUTEN2_LINK_PAYID', 37);

define('SPS_CREDIT_EVERY_LINK_PAYID', '100');
define('SPS_CREDIT_EVERY_API_PAYID', '101');
define('SPS_AU_EVERY_LINK_PAYID', '102');
define('SPS_DOCOMO_EVERY_LINK_PAYID', '103');


// 決済タイプ (EC-CUBE内の決済種別 識別コード)
define('SPS_CREDIT_LINK_CODE', 'Credit');
define('SPS_CREDIT3D_LINK_CODE', 'Credit3d');
define('SPS_CREDIT_REGIST_LINK_CODE', 'RegistCredit');
define('SPS_CREDIT3D_REGIST_LINK_CODE', 'RegistCredit3d');
define('SPS_SOFTBANK_LINK_CODE', 'Softbank');
define('SPS_DOCOMO_LINK_CODE', 'Docomo');
define('SPS_AU_LINK_CODE', 'Au');
define('SPS_WEBMONEY_LINK_CODE', 'WebMoney');
define('SPS_BITCASH_LINK_CODE', 'BitCash');
define('SPS_NETCASH_LINK_CODE', 'NetCash');
define('SPS_YWALLET_LINK_CODE', 'YWallet');
define('SPS_YWALLETD_LINK_CODE', 'YWalletD');
define('SPS_RAKUTEN_LINK_CODE', 'Rakuten');
define('SPS_RAKUTEN2_LINK_CODE', 'Rakuten2');
define('SPS_ALIPAY_LINK_CODE', 'Alipay');
define('SPS_PAYPAL_LINK_CODE', 'Paypal');
define('SPS_NETMILE_LINK_CODE', 'NetMile');
define('SPS_SAISON_LINK_CODE', 'Saison');
define('SPS_CYBEREDY_LINK_CODE', 'CyberEdy');
define('SPS_MOBILEEDY_LINK_CODE', 'MobileEdy');
define('SPS_SUICA_LINK_CODE', 'Suica');
define('SPS_CONVENI_LINK_CODE', 'Conveni');
define('SPS_PAYEASY_LINK_CODE', 'PayEasy');
define('SPS_JNB_LINK_CODE', 'JNB');
define('SPS_BANKTRANS_LINK_CODE', 'BankTrans');
define('SPS_GMONEY_LINK_CODE', 'GMoney');
define('SPS_POLLET_LINK_CODE', 'Pollet');
define('SPS_OEMPIN_LINK_CODE', 'OemPin');

define('SPS_CREDIT_API_CODE', 'CreditApi');
define('SPS_CREDIT_FCSV_CODE', 'CreditFCsv');
define('SPS_CREDIT3D_FCSV_CODE', 'Credit3dFCsv');
define('SPS_CREDIT_REGIST_API_CODE', 'CreditRegistApi');
define('SPS_BITCASH_FCSV_CODE', 'BitCashFCsv');
define('SPS_CONVENI_API_CODE', 'ConveniApi');
define('SPS_PAYEASY_API_CODE', 'PayEasyApi');
define('SPS_BANKTRANS_API_CODE', 'BankTransApi');
define('SPS_REGIST_CREDIT_API_CODE', 'RegistCreditApi');

define('SPS_CREDIT_EVERY_LINK_CODE', 'CreditEvery');
define('SPS_CREDIT_EVERY_API_CODE', 'CreditEveryApi');
define('SPS_AU_EVERY_LINK_CODE', 'AuEvery');
define('SPS_DOCOMO_EVERY_LINK_CODE', 'DocomoEvery');


// 入力状態
define('MDL_SPS_INPUT_STATUS_VIEW_FORM', 0);      // 入力前（フォーム表示済み）
define('MDL_SPS_INPUT_STATUS_ENTRY_FORM', 1);     // 入力受付済み

// 操作状態
define('MDL_SPS_ACTION_STATUS_UNSETTLED', 0);        // 操作前
define('MDL_SPS_ACTION_STATUS_ENTRY_REQUEST', 1);    // 取引登録要求
define('MDL_SPS_ACTION_STATUS_ENTRY_SUCCESS', 2);    // 取引登録成功
define('MDL_SPS_ACTION_STATUS_EXEC_REQUEST', 3);     // 決済要求
define('MDL_SPS_ACTION_STATUS_EXEC_SUCCESS', 6);     // 決済要求成功
define('MDL_SPS_ACTION_STATUS_EXEC_FAIL', 7);        // 決済要求失敗
define('MDL_SPS_ACTION_STATUS_WAIT_NOTICE', 4);      // 通知待ち
define('MDL_SPS_ACTION_STATUS_RECV_NOTICE', 5);      // 通知受信(済)
define('MDL_SPS_ACTION_STATUS_EXPIRE', 8);           // 期限切れ
define('MDL_SPS_ACTION_STATUS_CANCEL', 9);           // キャンセル

// 金額制限
define("MDL_SPS_DEVIDE_TOTAL_MIN", 10000);

// 決済状態
// リクエスト結果 【dtb_orderのmemo05へ登録】
define("SPS_COMMIT_OK", 1);     // 確定
define("SPS_CANCEL_OK", 2);     // 取消（返金）
define("SPS_ERROR", 3);         // 通信失敗
define("SPS_DATECOMMIT_OK", 4); // 確定
define("SPS_WAIT", 5);          // 結果通知待ち

// 売上計上設定
define("SPS_AUTH_DATE_COMMIT", '0'); // 売上日指定要求(与信のみ)
define("SPS_AUTH_AUTO", '1');        // 自動売上


// 決済通信タイムアウト値 (接続タイムアウト、ソケットタイムアウト共通)
define('MDL_SPS_HTTP_TIMEOUT', 10);

// 遷移URLデフォルト
define("MDL_SPS_COMPLETE_URL", HTTPS_URL . "shopping/load_payment_module.php?mode=spsreturn&ecr=success");  // 信用要求成功URL
define("MDL_SPS_RETURN_URL", HTTPS_URL  . "shopping/load_payment_module.php?mode=spsreturn&ecr=cancel");  // キャンセル時
define("MDL_SPS_ERROR_URL", HTTPS_URL  . "shopping/load_payment_module.php?mode=spsreturn&ecr=error");  // エラー時

// 接続方式
define("MDL_SPS_CONNECT_TYPE_API", 1);
define("MDL_SPS_CONNECT_TYPE_LINK", 2);
define("MDL_SPS_CONNECT_TYPE_FREECSV", 3);

// 継続課金状態
define('SPS_EVERY_OK' , 1);
define('SPS_EVERY_CR' , 2);
define('SPS_EVERY_CC' , 3);
define('SPS_EVERY_CANCEL' , 9);

//禁止文字 置き換え
define("SPS_REPLACE_NG_CHAR", '　');

require_once(MDL_SPS_CLASSEX_PATH . 'SC_Mdl_SPS_Ex.php');
require_once(MDL_SPS_CLASSEX_PATH . 'util_extends/SC_Util_SPS_Ex.php');
require_once(MDL_SPS_HELPEREX_PATH . 'SC_Mdl_SPS_Helper_Ex.php');

