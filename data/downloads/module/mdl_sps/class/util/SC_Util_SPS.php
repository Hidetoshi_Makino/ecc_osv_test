<?php
/**
 *
 * @copyright 2013 Spirit of Co.,Ltd. All Rights Reserved.
 * @link http://sbps.ec-cube.pro/
 *
 */

/**
 * 決済モジュール用 汎用関数クラス
 */
class SC_Util_SPS {

    /**
     * 禁止文字か判定を行う。
     *
     * @param string $value 判定対象
     * @return boolean 結果
     */
    function isProhibitedChar($value) {
        $check_char = mb_convert_encoding($value, "SJIS-win", "UTF-8");
        if (hexdec('8740') <= hexdec(bin2hex($check_char)) && hexdec('879E') >= hexdec(bin2hex($check_char))) {
            return true;
        }
        if ((hexdec('ED40') <= hexdec(bin2hex($check_char)) && hexdec('ED9E') >= hexdec(bin2hex($check_char))) ||
            (hexdec('ED9F') <= hexdec(bin2hex($check_char)) && hexdec('EDFC') >= hexdec(bin2hex($check_char))) ||
            (hexdec('EE40') <= hexdec(bin2hex($check_char)) && hexdec('EE9E') >= hexdec(bin2hex($check_char))) ||
            (hexdec('FA40') <= hexdec(bin2hex($check_char)) && hexdec('FA9E') >= hexdec(bin2hex($check_char))) ||
            (hexdec('FA9F') <= hexdec(bin2hex($check_char)) && hexdec('FAFC') >= hexdec(bin2hex($check_char))) ||
            (hexdec('FB40') <= hexdec(bin2hex($check_char)) && hexdec('FB9E') >= hexdec(bin2hex($check_char))) ||
            (hexdec('FB9F') <= hexdec(bin2hex($check_char)) && hexdec('FBFC') >= hexdec(bin2hex($check_char))) ||
            (hexdec('FC40') <= hexdec(bin2hex($check_char)) && hexdec('FC4B') >= hexdec(bin2hex($check_char)))){
            return true;
        }
        if ((hexdec('EE9F') <= hexdec(bin2hex($check_char)) && hexdec('EEFC') >= hexdec(bin2hex($check_char))) ||
            (hexdec('F040') <= hexdec(bin2hex($check_char)) && hexdec('F9FC') >= hexdec(bin2hex($check_char)))) {
            return true;
        }

        return false;
    }

    /**
     * 禁止文字を全角スペースに置換する。
     *
     * @param string $value 対象文字列
     * @return string 結果
     */
    function convertProhibitedChar($value) {
        $ret = $value;
        for ($i = 0; $i < mb_strlen($value); $i++) {
            $tmp = mb_substr($value, $i , 1);
            if (SC_Util_SPS_Ex::isProhibitedChar($tmp)) {
               $ret = str_replace($tmp, "　", $value);
            }
        }
        return $ret;
    }

    /**
     * 禁止半角記号を半角スペースに変換する。
     *
     * @param string $value
     * @return string 変換した値
     */
    function convertProhibitedKigo($value) {
        $arrProhiditedKigo = array('^','`','{','|','}','~','&','<','>','"','\'');
        foreach ($arrProhiditedKigo as $prohidited_kigo) {
            if(strstr($value, $prohidited_kigo)) {
                $value = str_replace($prohidited_kigo, " ", $value);
            }
        }
        return $value;
    }

    /**
     * 文字列から指定バイト数を切り出す。
     *
     * @param string $value
     * @param integer $len
     * @return string 結果
     */
    function subString($value, $len) {
        $value = mb_convert_encoding($value, "SJIS", "UTF-8");
        for ($i = 1; $i <= mb_strlen($value); $i++) {
            $tmp = mb_substr($value, 0 , $i);
            if (strlen($tmp) <= $len) {
                $ret = mb_convert_encoding($tmp, "UTF-8", "SJIS");
            } else {
                break;
            }
        }
        return $ret;
    }

    /**
     * 禁止文字か判定を行う。
     *
     * @param string $value 判定対象
     * @return boolean 結果
     */
    function isNgChar($char) {
        if ($char == '') return false;
        // unicode base
        if (bin2hex($char) == 'e28094') return true; // ダッシュ亜種
        if (bin2hex($char) == 'e28095') return true; // ダッシュ
        if (bin2hex($char) == '5c') return true; // バックスラッシュ　＝　￥マーク
        if (bin2hex($char) == 'e3809c') return true; // 波ダッシュ
        if (bin2hex($char) == 'e28096') return true; // パラレル
        if (bin2hex($char) == 'e28892') return true; // 二分ダッシュ
        if (bin2hex($char) == 'c2a2') return true; // セント
        if (bin2hex($char) == 'c2a3') return true; // ポンド
        if (bin2hex($char) == 'c2ac') return true; // 否定算術記号

        $check_char = mb_convert_encoding($char, "SJIS-win", "UTF-8");
        // convert sjis base
        if ($check_char == '') return true;
        if (bin2hex($check_char) == '815d') return true;
        if (bin2hex($check_char) == '815f') return true;
        if (bin2hex($check_char) == '8160') return true;
        if (bin2hex($check_char) == '8161') return true;
        if (bin2hex($check_char) == '817c') return true;
        if (bin2hex($check_char) == '8191') return true;
        if (bin2hex($check_char) == '8192') return true;
        if (bin2hex($check_char) == '81ca') return true;
        return false;
    }

    /**
     * NG文字を置換する。
     *
     * @param string $str 対象文字列
     * @return string 結果
     */
    function convNgChar($str) {
        for ($i = 0; $i < mb_strlen($str); $i++) {
            $tmp = mb_substr($str, $i , 1);
            if (SC_Util_SPS_Ex::isNgChar($tmp)) {
                $ret .= SPS_REPLACE_NG_CHAR;
            } else {
                $ret .= $tmp;
            }
        }
        return $ret;
    }

    /**
     * SPS登録用顧客IDを生成する
     *
     * @param integer $customer_id 会員ID
     * @return text 顧客ID
     */
    function getCustomerCode($customer_id) {
        if ($customer_id == '0') {
            return SPS_NON_MEMBER_PREFIX . md5(uniqid(rand(), true));
        } else {
            $objMdl =& SC_Mdl_SPS_Ex::getInstance();
            $arrModuleConfig = $objMdl->getUserSettings();

            if ($arrModuleConfig['type_cust_code'] == '2') {
                // 新方式
                $objQuery =& SC_Query::getSingletonInstance();
                $customer_secret = $objQuery->get('secret_key', 'dtb_customer', 'customer_id = ?', array($customer_id));
                $customer_code = SPS_MEMBER_PREFIX . $customer_id . '-' . $customer_secret;
                if (strlen($customer_code) > 64) {
                    $customer_code = substr($customer_code, 0, 64);
                }
                return $customer_code;
            } else {
                return $customer_id;
            }
        }
    }

    function convSjisParam($arrData) {
        foreach ($arrData as $key => $val) {
            if (is_array($val)) {
                $arrData[$key] = SC_Util_SPS_Ex::convSjisParam($val);
            } else {
                if (!$val || preg_match('/^[\w\s]+$/i', $val)) {
                    continue;
                }
                $temp = mb_convert_encoding($val, 'sjis-win', CHAR_CODE);
                $temp = mb_convert_encoding($temp, CHAR_CODE, 'sjis-win');
                if ($val !== $temp) {
                    $temp = mb_convert_encoding($val, CHAR_CODE, 'sjis-win');
                    $temp = mb_convert_encoding($temp, 'sjis-win', CHAR_CODE);
                    if ($val === $temp) {
                        $arrData[$key] = mb_convert_encoding($val, CHAR_CODE, 'sjis-win');
                    } else {
                        $arrData[$key] = 'unknown encoding strings';
                    }
                }
            }
        }
        return $arrData;
    }

    function getOrderPayData($order_id) {
        $objPurchase = new SC_Helper_Purchase_Ex();
        $arrOrder = $objPurchase->getOrder($order_id);
        if ($arrOrder['del_flg'] == '1') {
            $objMdl =& SC_Mdl_SPS_Ex::getInstance();
            $objMdl->printLog('getOrderPayData Error: deleted order. order_id = ' . $order_id);
            return false;
        }

        if (SC_Utils_Ex::isBlank($arrOrder[MDL_SPS_ORDER_COL_PAYDATA])) {
            $arrPayData = array();
        } else {
            $arrPayData = unserialize($arrOrder[MDL_SPS_ORDER_COL_PAYDATA]);
        }
        if (SC_Utils_Ex::isBlank($arrOrder[MDL_SPS_ORDER_COL_PAYLOG])) {
            $arrPayData['payment_log'] = array();
        } else {
            $arrPayData['payment_log'] = unserialize($arrOrder[MDL_SPS_ORDER_COL_PAYLOG]);
        }
        if(isset($arrPayData[0]) and is_array($arrPayData[0])) {
            $arrTemp = $arrPayData[0];
            unset($arrPayData[0]);
            $arrPayData = array_merge((array)$arrData, (array)$arrTemp);
        }
        if (isset($arrPayData['order_id'])) {
            $arrPayData['sbps_order_id'] = $arrPayData['order_id'];
            unset($arrPayData['order_id']);
        }
        $arrOrder = array_merge($arrOrder, (array)$arrPayData);
        return $arrOrder;
    }

    function setOrderPayData($arrOrder, $arrData, $is_only_log = false, $new_status = null, $sqlval = array()) {
        if(isset($arrData[0]) and is_array($arrData[0])) {
            $arrTemp = $arrData[0];
            unset($arrData[0]);
            $arrData = array_merge((array)$arrData, (array)$arrTemp);
        }

        $objPurchase = new SC_Helper_Purchase_Ex();

        $objQuery =& SC_Query_Ex::getSingletonInstance();
        $objQuery->begin();

        $arrOrder = $objPurchase->getOrder($arrOrder['order_id']);
        if (SC_Utils_Ex::isBlank($arrOrder[MDL_SPS_ORDER_COL_PAYLOG])) {
            $arrLog = array();
        } else {
            $arrLog = unserialize($arrOrder[MDL_SPS_ORDER_COL_PAYLOG]);
        }
        $arrLog[] = array( date('Y-m-d H:i:s') => $arrData );
        $sqlval[MDL_SPS_ORDER_COL_PAYLOG] = serialize($arrLog);

        if (!$is_only_log) {
            if (SC_Utils_Ex::isBlank($arrOrder[MDL_SPS_ORDER_COL_PAYDATA])) {
                $arrPayData = array();
            } else {
                $arrPayData = unserialize($arrOrder[MDL_SPS_ORDER_COL_PAYDATA]);
            }

            foreach ($arrData as $key => $val) {
                if (SC_Utils_Ex::isBlank($val) && !SC_Utils_Ex::isBlank($arrPayData[$key])) {
                    unset($arrData[$key]);
                }
            }

            $arrPayData = array_merge($arrPayData, (array)$arrData);

            $sqlval[MDL_SPS_ORDER_COL_PAYDATA] = serialize($arrPayData);
        }

        $objMdl =& SC_Mdl_SPS_Ex::getInstance();
        if (MDL_SPS_DEBUG) {
            $arrBacktrace = debug_backtrace();
            if (is_object($arrBacktrace[0]['object'])) {
                $class_name = get_class($arrBacktrace[0]['object']);
                $parent_class_name = get_parent_class($arrBacktrace[0]['object']);
                $class_msg = $parent_class_name . ' -> ' . $class_name . ' -> ';
            }
        } else {
            $class_msg = basename($_SERVER["SCRIPT_NAME"]);
        }
        if ($new_status == 0) {
            $new_status = null;
        }
        $objMdl->printLog($class_msg . ' set payment data:' . count($arrData) . ' change_status =' . $new_status);

        $objPurchase->sfUpdateOrderStatus($arrOrder['order_id'], $new_status, null, null, $sqlval);

        $objQuery->commit();
    }

    function isPlugin($plugin_name) {
        $arrRet = array();
        $objQuery =& SC_Query_Ex::getSingletonInstance();
        $where = "plugin_code = ? and enable = 1";
        $count = $objQuery->count('dtb_plugin', $where, array($plugin_name));
        return $count;
    }

    /**
     * 決済モジュールで利用出来る決済方式の名前一覧を取得する
     *
     * @param integer $payment_id 支払いID
     * @return array 支払方法
     */
    function getPaymentTypeNames() {
        $arrData = array(
            SPS_CREDIT_LINK_PAYID => SPS_CREDIT_LINK_NAME,
            SPS_CREDIT3D_LINK_PAYID => SPS_CREDIT3D_LINK_NAME,
            SPS_CREDIT_REGIST_LINK_PAYID => SPS_CREDIT_REGIST_LINK_NAME,
            SPS_CREDIT3D_REGIST_LINK_PAYID => SPS_CREDIT3D_REGIST_LINK_NAME,
            SPS_SOFTBANK_LINK_PAYID => SPS_SOFTBANK_LINK_NAME,
            SPS_DOCOMO_LINK_PAYID => SPS_DOCOMO_LINK_NAME,
            SPS_AU_LINK_PAYID => SPS_AU_LINK_NAME,
            SPS_WEBMONEY_LINK_PAYID => SPS_WEBMONEY_LINK_NAME,
            SPS_BITCASH_LINK_PAYID => SPS_BITCASH_LINK_NAME,
            SPS_NETCASH_LINK_PAYID => SPS_NETCASH_LINK_NAME,
            SPS_YWALLET_LINK_PAYID => SPS_YWALLET_LINK_NAME,
            SPS_YWALLETD_LINK_PAYID => SPS_YWALLETD_LINK_NAME,
            SPS_RAKUTEN_LINK_PAYID => SPS_RAKUTEN_LINK_NAME,
            SPS_RAKUTEN2_LINK_PAYID => SPS_RAKUTEN2_LINK_NAME,
            SPS_ALIPAY_LINK_PAYID => SPS_ALIPAY_LINK_NAME,
            SPS_PAYPAL_LINK_PAYID => SPS_PAYPAL_LINK_NAME,
            SPS_NETMILE_LINK_PAYID => SPS_NETMILE_LINK_NAME,
            SPS_SAISON_LINK_PAYID => SPS_SAISON_LINK_NAME,
            //SPS_CYBEREDY_LINK_PAYID => SPS_CYBEREDY_LINK_NAME,
            SPS_MOBILEEDY_LINK_PAYID => SPS_MOBILEEDY_LINK_NAME,
            SPS_SUICA_LINK_PAYID => SPS_SUICA_LINK_NAME,
            SPS_CONVENI_LINK_PAYID => SPS_CONVENI_LINK_NAME,
            SPS_PAYEASY_LINK_PAYID => SPS_PAYEASY_LINK_NAME,
            //SPS_JNB_LINK_PAYID => SPS_JNB_LINK_NAME,
            SPS_BANKTRANS_LINK_PAYID => SPS_BANKTRANS_LINK_NAME,
            //SPS_GMONEY_LINK_PAYID => SPS_GMONEY_LINK_NAME,
            //SPS_POLLET_LINK_PAYID => SPS_POLLET_LINK_NAME,
            //SPS_OEMPIN_LINK_PAYID => SPS_OEMPIN_LINK_NAME,
            SPS_CREDIT_API_PAYID => SPS_CREDIT_API_NAME,
            //SPS_CREDIT_FCSV_PAYID => SPS_CREDIT_FCSV_NAME,
            SPS_CREDIT3D_FCSV_PAYID => SPS_CREDIT3D_FCSV_NAME,
            SPS_CREDIT_REGIST_API_PAYID => SPS_CREDIT_REGIST_API_NAME,
            SPS_REGIST_CREDIT_API_PAYID => SPS_REGIST_CREDIT_API_NAME,
            //SPS_BITCASH_FCSV_PAYID => SPS_BITCASH_FCSV_NAME,
            SPS_CONVENI_API_PAYID => SPS_CONVENI_API_NAME,
            SPS_PAYEASY_API_PAYID => SPS_PAYEASY_API_NAME,
            SPS_BANKTRANS_API_PAYID => SPS_BANKTRANS_API_NAME,
        );

        if (SC_Util_SPS_Ex::isPlugin('SpsEvery')) {
            $arrData[SPS_CREDIT_EVERY_LINK_PAYID] = SPS_CREDIT_EVERY_LINK_NAME;
            $arrData[SPS_CREDIT_EVERY_API_PAYID] = SPS_CREDIT_EVERY_API_NAME;
            $arrData[SPS_AU_EVERY_LINK_PAYID] = SPS_AU_EVERY_LINK_NAME;
            $arrData[SPS_DOCOMO_EVERY_LINK_PAYID] = SPS_DOCOMO_EVERY_LINK_NAME;
        }
        return $arrData;
    }

    /**
     * 決済モジュールで利用出来る決済方式の内部名一覧を取得する
     *
     * @return array 支払方法コード
     */
    function getPaymentTypeCodes() {
        return array(
            SPS_CREDIT_LINK_PAYID => SPS_CREDIT_LINK_CODE,
            SPS_CREDIT3D_LINK_PAYID => SPS_CREDIT3D_LINK_CODE,
            SPS_CREDIT_REGIST_LINK_PAYID => SPS_CREDIT_REGIST_LINK_CODE,
            SPS_CREDIT3D_REGIST_LINK_PAYID => SPS_CREDIT3D_REGIST_LINK_CODE,
            SPS_SOFTBANK_LINK_PAYID => SPS_SOFTBANK_LINK_CODE,
            SPS_DOCOMO_LINK_PAYID => SPS_DOCOMO_LINK_CODE,
            SPS_AU_LINK_PAYID => SPS_AU_LINK_CODE,
            SPS_WEBMONEY_LINK_PAYID => SPS_WEBMONEY_LINK_CODE,
            SPS_BITCASH_LINK_PAYID => SPS_BITCASH_LINK_CODE,
            SPS_NETCASH_LINK_PAYID => SPS_NETCASH_LINK_CODE,
            SPS_YWALLET_LINK_PAYID => SPS_YWALLET_LINK_CODE,
            SPS_YWALLETD_LINK_PAYID => SPS_YWALLETD_LINK_CODE,
            SPS_RAKUTEN_LINK_PAYID => SPS_RAKUTEN_LINK_CODE,
            SPS_RAKUTEN2_LINK_PAYID => SPS_RAKUTEN2_LINK_CODE,
            SPS_ALIPAY_LINK_PAYID => SPS_ALIPAY_LINK_CODE,
            SPS_PAYPAL_LINK_PAYID => SPS_PAYPAL_LINK_CODE,
            SPS_NETMILE_LINK_PAYID => SPS_NETMILE_LINK_CODE,
            SPS_SAISON_LINK_PAYID => SPS_SAISON_LINK_CODE,
            SPS_CYBEREDY_LINK_PAYID => SPS_CYBEREDY_LINK_CODE,
            SPS_MOBILEEDY_LINK_PAYID => SPS_MOBILEEDY_LINK_CODE,
            SPS_SUICA_LINK_PAYID => SPS_SUICA_LINK_CODE,
            SPS_CONVENI_LINK_PAYID => SPS_CONVENI_LINK_CODE,
            SPS_PAYEASY_LINK_PAYID => SPS_PAYEASY_LINK_CODE,
            SPS_JNB_LINK_PAYID => SPS_JNB_LINK_CODE,
            SPS_BANKTRANS_LINK_PAYID => SPS_BANKTRANS_LINK_CODE,
            SPS_GMONEY_LINK_PAYID => SPS_GMONEY_LINK_CODE,
            SPS_POLLET_LINK_PAYID => SPS_POLLET_LINK_CODE,
            SPS_OEMPIN_LINK_PAYID => SPS_OEMPIN_LINK_CODE,
            SPS_CREDIT_API_PAYID => SPS_CREDIT_API_CODE,
            SPS_CREDIT_FCSV_PAYID => SPS_CREDIT_FCSV_CODE,
            SPS_CREDIT3D_FCSV_PAYID => SPS_CREDIT3D_FCSV_CODE,
            SPS_CREDIT_REGIST_API_PAYID => SPS_CREDIT_REGIST_API_CODE,
            SPS_BITCASH_FCSV_PAYID => SPS_BITCASH_FCSV_CODE,
            SPS_CONVENI_API_PAYID => SPS_CONVENI_API_CODE,
            SPS_PAYEASY_API_PAYID => SPS_PAYEASY_API_CODE,
            SPS_REGIST_CREDIT_API_PAYID => SPS_REGIST_CREDIT_API_CODE,
            SPS_BANKTRANS_API_PAYID => SPS_BANKTRANS_API_CODE,
            SPS_CREDIT_EVERY_LINK_PAYID => SPS_CREDIT_EVERY_LINK_CODE,
            SPS_CREDIT_EVERY_API_PAYID => SPS_CREDIT_EVERY_API_CODE,
            SPS_AU_EVERY_LINK_PAYID => SPS_AU_EVERY_LINK_CODE,
            SPS_DOCOMO_EVERY_LINK_PAYID => SPS_DOCOMO_EVERY_LINK_CODE,
                );
    }

    /**
     * 支払方法情報を取得する
     *
     * @param integer $payment_id 支払いID
     * @return array 支払方法情報。決済モジュール管理対象である場合、内部識別コードを同時に設定する
     */
    function getPaymentInfo($payment_id) {
        $objQuery =& SC_Query_Ex::getSingletonInstance();
        $arrData = $objQuery->getRow('*', 'dtb_payment', 'payment_id = ?', array($payment_id));

        // 決済モジュールの対象決済であるかの判断と内部識別コードの設定を同時に行う。
        $arrPaymentCode = SC_Util_SPS_Ex::getPaymentTypeCodes();
        $arrData[MDL_SPS_CODE . '_payment_code'] = $arrPaymentCode[$arrData[MDL_SPS_PAYMENT_COL_PAYID]];
        return $arrData;
    }

    /**
     * 支払方法設定情報を取得する
     *
     * @param integer $payment_id 支払いID
     * @return array 支払方法情報。決済モジュール管理対象である場合、内部識別コードを同時に設定する
     */
    function getPaymentTypeConfig($payment_id) {
        $arrData = SC_Util_SPS_Ex::getPaymentInfo($payment_id);
        if (!SC_Utils_Ex::isBlank($arrData[MDL_SPS_PAYMENT_COL_CONFIG])) {
            $arrData = array_merge($arrData, unserialize($arrData[MDL_SPS_PAYMENT_COL_CONFIG]));
        }
        return $arrData;
    }

    function setPaymentTypeConfig($payment_id, $arrData) {
        if (SC_Utils_Ex::isBlank($arrData)) {
            $arrData = array();
        }
        $objQuery =& SC_Query_Ex::getSingletonInstance();
        $arrVal = array(MDL_SPS_PAYMENT_COL_CONFIG => serialize($arrData));
        $objMdl =& SC_Mdl_SPS::getInstance();
        $objMdl->printLog($arrData);
        $objQuery->update('dtb_payment', $arrVal, 'payment_id = ?', array($payment_id));
    }

    function setBlocData($plugin_id, $bloc_data, $device_type_id, $bloc_name, $filename, $php_path = "", $is_force = false) {
        $objMdl =& SC_Mdl_SPS::getInstance();
        $objLayout = new SC_Helper_PageLayout_Ex();
        $objQuery = SC_Query_Ex::getSingletonInstance();
        $objQuery->begin();

        $bloc_dir = $objLayout->getTemplatePath($device_type_id) . BLOC_DIR;
        $tpl_path = $filename . '.tpl';

        if (!SC_Utils_Ex::isBlank($plugin_id)) {
            $where = 'filename = ?';
            $arrval = array($filename);
        } else {
            $where = 'filename = ?';
            $arrval = array($filename);
        }

        $arrExists = $objLayout->getBlocs($device_type_id, $where, $arrval);
        $exists_file = $bloc_dir . $arrExists[0]['filename'] . '.tpl';
        if (file_exists($exists_file)) {
            if ($is_force) {
                $old_file = $exists_file . '.bak.' . date('YmdHis');
                $objMdl->printLog("set blocdata file copy: $exists_file -> $old_file");
                @copy($exists_file, $old_file);
                unlink($exists_file);
            } else {
                return $arrExists[0]['bloc_id'];
            }
        }

        $sqlval_bloc = array();
        $sqlval_bloc['device_type_id'] = $device_type_id;
        $sqlval_bloc['bloc_name'] = $bloc_name;
        $sqlval_bloc['tpl_path'] = $filename . '.tpl';
        $sqlval_bloc['filename'] = $filename;
        $sqlval_bloc['update_date'] = "CURRENT_TIMESTAMP";
        if (!SC_Utils_Ex::isBlank($php_path)) {
            $sqlval_bloc['php_path'] = $php_path;
        }
        $sqlval_bloc['deletable_flg'] = 0;
        $sqlval_bloc['plugin_id'] = $plugin_id;
        $objQuery->setOrder('');

        if (SC_Utils_Ex::isBlank($arrExists[0]['bloc_id'])) {
            $sqlval_bloc['bloc_id'] = $objQuery->max('bloc_id', "dtb_bloc", "device_type_id = ?", array($device_type_id)) + 1;
            $sqlval_bloc['create_date'] = "CURRENT_TIMESTAMP";
            $objQuery->insert("dtb_bloc", $sqlval_bloc);
            $bloc_id = $sqlval_bloc['bloc_id'];
        } else {
            $objQuery->update("dtb_bloc", $sqlval_bloc, "device_type_id = ? AND bloc_id = ?", array($device_type_id, $arrExists[0]['bloc_id']));
            $bloc_id = $arrExists[0]['bloc_id'];
        }

        $bloc_path = $bloc_dir . $tpl_path;
        if (!SC_Helper_FileManager_Ex::sfWriteFile($bloc_path, $bloc_data)) {
            $objMdl->printLog("ブロックファイルの複製に失敗しました。");
            $objQuery->rollback();
            return false;
        }

        $objQuery->commit();
        $objMdl->printLog("set blocdata: device=$device_type_id, name=$bloc_name, filename=$filename, bloc_id=$bloc_id");
        return $bloc_id;
    }

    function setPageData($tpl_data, $page_name, $url, $filename, $device_type_id, $is_force = false) {
        $objMdl =& SC_Mdl_SPS::getInstance();
        $objLayout = new SC_Helper_PageLayout_Ex();
        $objQuery =& SC_Query_Ex::getSingletonInstance();
        $objQuery->begin();

        $tpl_dir = $objLayout->getTemplatePath($device_type_id);
        $tpl_path = $filename . '.tpl';

        $arrExists = $objLayout->getPageProperties($device_type_id, null, 'device_type_id = ? and filename = ?', array($device_type_id, $filename));

        $exists_file = $tpl_dir . $arrExists[0]['filename'] . '.tpl';
        if (file_exists($exists_file)) {
            if ($is_force) {
                $old_file = $exists_file . '.bak.' . date('YmdHis');
                $objMdl->printLog("set blocdata file copy: $exists_file -> $old_file");
                @copy($exists_file, $old_file);
            } else {
                return $arrExists[0]['page_id'];
            }
        }
        $table = 'dtb_pagelayout';
        $arrValues = array();
        $arrValues['device_type_id'] = $device_type_id;
        $arrValues['header_chk'] = 1;
        $arrValues['footer_chk'] = 1;
        $arrValues['page_name'] = $page_name;
        $arrValues['url'] = $url;
        $arrValues['filename'] = $filename;
        $arrValues['edit_flg'] = '2';
        $arrValues['update_url'] = $_SERVER['HTTP_REFERER'];
        $arrValues['update_date'] = 'CURRENT_TIMESTAMP';
        $objQuery->setOrder('');
        if (SC_Utils_Ex::isBlank($arrExists[0]['page_id'])) {
            $arrValues['page_id'] = 1 + $objQuery->max('page_id', $table, 'device_type_id = ?',
                                                       array($arrValues['device_type_id']));
            $arrValues['create_date'] = 'CURRENT_TIMESTAMP';
            $objQuery->insert($table, $arrValues);
            $page_id = $arrValues['page_id'];
        } else {
            $objQuery->update($table, $arrValues, 'page_id = ? AND device_type_id = ?',
                                                       array($arrExists[0]['page_id'], $arrValues['device_type_id']));
            $page_id = $arrExists[0]['page_id'];
        }

        $tpl_path = $tpl_dir . $filename . '.tpl';

        if (!SC_Helper_FileManager_Ex::sfWriteFile($tpl_path, $tpl_data)) {
            $objMdl->printLog("ページテンプレートファイルの複製に失敗しました。");
            $objQuery->rollback();
            return false;
        }
        $objQuery->commit();
        $objMdl->printLog("set pagedata: device=$device_type_id, name=$page_name, filename=$filename, page_id=$page_id");
        return $page_id;
    }

    function installPluginFromPluginInfo($plugin_code, $is_enable = false, $priority = '0') {
        if (file_exists(MDL_SPS_PATH . 'plugins/' . $plugin_code . '/plugin_info.php')) {
            $plugin_info_text = file_get_contents(MDL_SPS_PATH . 'plugins/' . $plugin_code . '/plugin_info.php');
            $plugin_info_text = str_replace('plugin_info', 'plugin_info_' . $plugin_code, $plugin_info_text);
            $plugin_info_text = str_replace(array('<?php', '?>'), '', $plugin_info_text);
            eval($plugin_info_text);

            $objReflection = new ReflectionClass('plugin_info_' . $plugin_code);
            $arrPluginInfo = SC_Util_SPS_Ex::getPluginInfo($objReflection);
            return SC_Util_SPS_Ex::installPlugin($arrPluginInfo, $is_enable, $priority);
        }
        return false;
    }

    /**
     * プラグイン情報を取得します.
     *
     * @param ReflectionClass $objReflection
     * @return array プラグイン情報の配列
     */
    function getPluginInfo(ReflectionClass $objReflection) {
        $arrStaticProps = $objReflection->getStaticProperties();
        $arrConstants   = $objReflection->getConstants();

        $arrPluginInfoKey = array(
            'PLUGIN_CODE',
            'PLUGIN_NAME',
            'CLASS_NAME',
            'PLUGIN_VERSION',
            'COMPLIANT_VERSION',
            'AUTHOR',
            'DESCRIPTION',
            'PLUGIN_SITE_URL',
            'AUTHOR_SITE_URL',
            'HOOK_POINTS',
        );
        $arrPluginInfo = array();
        foreach ($arrPluginInfoKey as $key) {
            // クラス変数での定義を優先
            if (isset($arrStaticProps[$key])) {
                $arrPluginInfo[$key] = $arrStaticProps[$key];
            // クラス変数定義がなければ, クラス定数での定義を読み込み.
            } elseif ($arrConstants[$key]) {
                $arrPluginInfo[$key] = $arrConstants[$key];
            } else {
                $arrPluginInfo[$key] = null;
            }
        }
        return $arrPluginInfo;
    }

    function installPlugin($arrPluginInfo, $is_enable = false, $plugin_priority = '0') {
        // プラグインコード
        $plugin_code = $arrPluginInfo['PLUGIN_CODE'];
        // プラグイン名
        $plugin_name = $arrPluginInfo['PLUGIN_NAME'];

        $plugin_id = SC_Util_SPS_Ex::getPluginId($plugin_code);
        $plugin_id = SC_Util_SPS_Ex::registerPluginData($plugin_id, $arrPluginInfo, $is_enable, $plugin_priority);

        $plugin_dir_path = PLUGIN_UPLOAD_REALDIR . $plugin_code . '/';
        $plugin_html_dir = PLUGIN_HTML_REALDIR . $plugin_code;
        if (!file_exists(PLUGIN_UPLOAD_REALDIR)) {
            mkdir(PLUGIN_UPLOAD_REALDIR, 0777);
        }
        if (!file_exists($plugin_dir_path)) {
            mkdir($plugin_dir_path, 0777);
        }
        SC_Utils_Ex::copyDirectory(MDL_SPS_PATH . 'plugins/' . $plugin_code . '/', $plugin_dir_path);

        $plugin = SC_Plugin_Util_Ex::getPluginByPluginId($plugin_id);
        $plugin_class_file_path = $plugin_dir_path . $plugin['class_name'] . '.php';
        require_once($plugin_class_file_path);

        if (!file_exists(PLUGIN_HTML_REALDIR)) {
            mkdir(PLUGIN_HTML_REALDIR, 0777);
        }
        if (!file_exists($plugin_html_dir)) {
            mkdir($plugin_html_dir, 0777);
        }

        if (method_exists($plugin['class_name'], 'install') === true) {
            call_user_func(array($plugin['class_name'], 'install'), $plugin);
        }

        if ($is_enable && method_exists($plugin['class_name'], 'enable') === true) {
            call_user_func(array($plugin['class_name'], 'enable'), $plugin);
        }

        return $plugin_id;
    }

    /**
     * プラグイン情報をDB登録.
     *
     * @param array $arrPluginInfo プラグイン情報を格納した連想配列.
     * @return array エラー情報を格納した連想配列.
     */
    function registerPluginData($plugin_id, $arrPluginInfo, $is_enable = false, $priority = '0') {
        // プラグイン情報をDB登録.
        $objQuery =& SC_Query_Ex::getSingletonInstance();
        $objQuery->begin();
        $arr_sqlval_plugin = array();
        $arr_sqlval_plugin['plugin_name'] = $arrPluginInfo['PLUGIN_NAME'];
        $arr_sqlval_plugin['plugin_code'] = $arrPluginInfo['PLUGIN_CODE'];
        $arr_sqlval_plugin['class_name'] = $arrPluginInfo['CLASS_NAME'];
        $arr_sqlval_plugin['author'] = $arrPluginInfo['AUTHOR'];
        // AUTHOR_SITE_URLが定義されているか判定.
        $author_site_url = $arrPluginInfo['AUTHOR_SITE_URL'];
        if ($author_site_url !== null) {
            $arr_sqlval_plugin['author_site_url'] = $arrPluginInfo['AUTHOR'];
        }
        // PLUGIN_SITE_URLが定義されているか判定.
        $plugin_site_url = $arrPluginInfo['PLUGIN_SITE_URL'];
        if ($plugin_site_url !== null) {
            $arr_sqlval_plugin['plugin_site_url'] = $plugin_site_url;
        }
        $arr_sqlval_plugin['plugin_version'] = $arrPluginInfo['PLUGIN_VERSION'];
        $arr_sqlval_plugin['compliant_version'] = $arrPluginInfo['COMPLIANT_VERSION'];
        $arr_sqlval_plugin['plugin_description'] = $arrPluginInfo['DESCRIPTION'];
        $arr_sqlval_plugin['priority'] = $priority;
        $arr_sqlval_plugin['enable'] = $is_enable ? PLUGIN_ENABLE_TRUE : PLUGIN_ENABLE_FALSE;
        $arr_sqlval_plugin['update_date'] = 'CURRENT_TIMESTAMP';

        if (SC_Utils_Ex::isBlank($plugin_id)) {
            $plugin_id = $objQuery->nextVal('dtb_plugin_plugin_id');
            $arr_sqlval_plugin['plugin_id'] = $plugin_id;
            $objQuery->insert('dtb_plugin', $arr_sqlval_plugin);
        } else {
            $objQuery->update('dtb_plugin', $arr_sqlval_plugin, 'plugin_id = ?', array($plugin_id));
        }

        // フックポイントをDB登録.
        $hook_point = $arrPluginInfo['HOOK_POINTS'];
        if ($hook_point !== null) {
            // 一回削除する
            $objQuery->delete('dtb_plugin_hookpoint', 'plugin_id = ?', array($plugin_id));
            // フックポイントが配列で定義されている場合
            if (is_array($hook_point)) {
                foreach ($hook_point as $h) {
                    $arr_sqlval_plugin_hookpoint = array();
                    $id = $objQuery->nextVal('dtb_plugin_hookpoint_plugin_hookpoint_id');
                    $arr_sqlval_plugin_hookpoint['plugin_hookpoint_id'] = $id;
                    $arr_sqlval_plugin_hookpoint['plugin_id'] = $plugin_id;
                    $arr_sqlval_plugin_hookpoint['hook_point'] = $h[0];
                    $arr_sqlval_plugin_hookpoint['callback'] = $h[1];
                    $arr_sqlval_plugin_hookpoint['update_date'] = 'CURRENT_TIMESTAMP';
                    $objQuery->insert('dtb_plugin_hookpoint', $arr_sqlval_plugin_hookpoint);
                }
            // 文字列定義の場合
            } else {
                $array_hook_point = explode(',', $hook_point);
                foreach ($array_hook_point as $h) {
                    $arr_sqlval_plugin_hookpoint = array();
                    $id = $objQuery->nextVal('dtb_plugin_hookpoint_plugin_hookpoint_id');
                    $arr_sqlval_plugin_hookpoint['plugin_hookpoint_id'] = $id;
                    $arr_sqlval_plugin_hookpoint['plugin_id'] = $plugin_id;
                    $arr_sqlval_plugin_hookpoint['hook_point'] = $h;
                    $arr_sqlval_plugin_hookpoint['update_date'] = 'CURRENT_TIMESTAMP';
                    $objQuery->insert('dtb_plugin_hookpoint', $arr_sqlval_plugin_hookpoint);
                }
            }
        }
        $objQuery->commit();
        return $plugin_id;
    }

    /**
     * 既にインストールされているプラグインのIDを取得します。
     *
     * @param string $plugin_code プラグインコード
     * @return integer インストール済の場合 plugin_id インストールされていない場合false
     */
    function getPluginId($plugin_code) {
        $plugin = SC_Plugin_Util_Ex::getPluginByPluginCode($plugin_code);
        if (!empty($plugin)) {
            return $plugin['plugin_id'];
        }
        return false;
    }


    /**
     * 配列データからログに記録しないデータをマスクする
     *
     * @param array $arrData
     * @return array マスク後データ
     */
    function setMaskData($arrData) {
        foreach ($arrData as $key => $val) {
            switch($key) {
                case 'CardNo':
                    $arrData[$key] = str_repeat('*', 13) . substr($val,-3);
                    break;
                case 'SecurityCode':
                    $arrData[$key] = str_repeat('*', 4);
                    break;
                case 'MemberName':
                case 'CustomerName':
                case 'CustomerKana':
                case 'ShopPass':
                case 'SitePass':
                case 'MemberName':
                case 'MailAddress':
                    $arrData[$key] = str_repeat('*', 6);
                    break;
                default:
                    break;
            }
        }
        return $arrData;
    }

    /**
     * SPSへリクエストを送信
     * @param string $requestXml
     * @param array $spsSetting
     * @return string $r_code
     * @return string $response
     */
    function sendRequestSps($requestXml, $spsSetting) {
        $objSPS =& SC_Mdl_SPS_Ex::getInstance();

        $api_url = $objSPS->getUserSettings('url_sps_api');
        if (MDL_SPS_DEBUG) {
            $msg = "request: url=$api_url req=$requestXml config=". print_r($spsSetting, true);
            $objSPS->printLog($msg);
        } else {
            $objSPS->printLog("request: url=$api_url");
        }
        // リクエスト処理
        $req = new HTTP_Request($api_url);
        $req->addHeader('Content-Type', 'text/xml');
        $req->setMethod('POST');
        $req->setBasicAuth($spsSetting['id_sps_marchant']. $spsSetting['id_sps_service'], $spsSetting['id_sps_hash']);
        $req->setBody($requestXml);
        $req->sendRequest();
        $r_code = $req->getResponseCode();
        $response = $req->getResponseBody();

        $objSPS->printLog("response: url=$api_url response=$r_code \ndata=" . mb_convert_encoding($response, 'sjis-win', CHAR_CODE));
        return array($r_code, $response);
    }

    /**
     * XMLを配列化
     * @param string $xml
     * @return array $arrXml
     */
    function changeXml2Array($xml) {
        // 不要なデータを排除
        $xml = mb_convert_encoding($xml, 'UTF-8', 'sjis-win');
        $xml = preg_replace("/<\?xml.*\?>/", "", $xml);

        // XMLを配列化
        $objXmlUnserializer = new XML_Unserializer();
        $objXmlUnserializer->setOption("parseAttributes", true);
        $objXmlUnserializer->unserialize($xml);
        $arrXml = $objXmlUnserializer->getUnserializedData();

        return $arrXml;
    }

    /**
     * 配列をXML化
     * @param array $arrXml
     * @param string $request_code
     * @return string $xml
     */
    function changeArray2Xml($arrXml, $request_code) {
        // 配列をXML化
        $objXmlSerializer = new XML_Serializer();
        $objXmlSerializer->setOption("indent", "    ");
        $objXmlSerializer->setOption("addDecl", true);
        $objXmlSerializer->setOption("encoding", "Shift_JIS");
        $objXmlSerializer->setOption("rootName", "sps-api-request");
        $objXmlSerializer->setOption("rootAttributes", array("id" => $request_code));
        $objXmlSerializer->serialize($arrXml);
        $xml = $objXmlSerializer->getSerializedData();

        return $xml;
    }


    /**
     * マルチバイト文字列のエンコード
     * @param array $arrData
     * @param array $key
     * @return array $arrData
     */
    function encodeMbstring($arrData, $arrKey) {
        foreach ($arrKey as $key) {
            $arrData[$key] = base64_encode(SC_Util_SPS_Ex::convNgChar($arrData[$key]));
        }
        return $arrData;
    }

    function decodeData($arrData, $spsSetting) {
        $em = mcrypt_module_open(MCRYPT_3DES, "",  MCRYPT_MODE_CBC, "");

        foreach ($arrData as $key => $val) {
            if ($val == '') continue;

            mcrypt_generic_init($em, $spsSetting['key_sps_3des'], $spsSetting['key_sps_3des_iv']);
            $val = base64_decode($val);
            $val = mdecrypt_generic($em, $val);
            $arrData[$key] = $val;
            mcrypt_generic_deinit($em);
        }
        mcrypt_module_close($em);
        return $arrData;
    }

    /**
     * 暗号化
     * @param array $arrData
     * @param string $pkey
     * @param array $spsSetting
     * @return array $arrData
     */
    function encryptData($arrData, $pkey, $spsSetting, $is_conv = false) {
        if ($is_conv) {
            foreach ($arrData[$pkey] as $key => $val) {
                $arrData[$pkey][$key] = mb_convert_encoding($val , 'sjis-win', CHAR_SET);
            }
        }
        // パディング（最後の8バイトブロックに半角スペースを補完）
        foreach ($arrData[$pkey] as $key => $val) {
            $val_len = strlen($val);
            if ($val_len > 0) {
                $val_last_8byte = $val_len % 8;
                $val_null_8byte = ($val_last_8byte != 0) ? 8 - $val_last_8byte : 0;
                for ($i = 0; $i < $val_null_8byte; $i++) {
                    $arrData[$pkey][$key] .= " ";
                }
            }
        }

        // 暗号化
        $em = mcrypt_module_open(MCRYPT_3DES, "",  MCRYPT_MODE_CBC, "");
        foreach ($arrData[$pkey] as $key => $val) {
            if (strlen($val) > 0) {
                mcrypt_generic_init($em, $spsSetting['key_sps_3des'], $spsSetting['key_sps_3des_iv']);
                $encrypt_val = mcrypt_generic($em, $val);
                mcrypt_generic_deinit($em);
                $arrData[$pkey][$key] = base64_encode($encrypt_val);
            }
        }
        mcrypt_module_close($em);
        return $arrData;
    }


    function printLog($msg) {
        if (is_array($msg) || is_object($msg)) {
            $msg = print_r($msg,true);
        }
        $objMdl =& SC_MDL_SPS_Ex::getInstance();
        $objMdl->printLog($msg);
    }
}

