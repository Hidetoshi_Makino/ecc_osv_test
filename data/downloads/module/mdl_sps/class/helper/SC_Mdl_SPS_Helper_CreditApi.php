<?php
/**
 *
 * @copyright 2013 Spirit of Co.,Ltd. All Rights Reserved.
 * @link http://sbps.ec-cube.pro/
 *
 */
require_once(MDL_SPS_HELPEREX_PATH . 'SC_Mdl_SPS_Helper_Base_Ex.php');

/**
 * 決済個別用ヘルパー関数 CreditApiクラス
 */
class SC_Mdl_SPS_Helper_CreditApi extends SC_Mdl_SPS_Helper_Base_Ex {

    function init() {
        $this->payment_type_id = SPS_CREDIT_API_PAYID;
        $this->payment_name = 'CreditApi';
    }

    function getPayMethod() {
        return 'creditapi';
    }

    function getPaymentBlocName() {
        return 'sps_creditapi';
    }

    function getConnectType() {
        return MDL_SPS_CONNECT_TYPE_API;
    }

    function getSpsPayType() {
        // 購入タイプ(固定　0:都度購入、1:継続購入)
        return '0';
    }

    function getPaymentRequestApiId() {
        return 'ST01-00111-101';
    }

    function getPaymentCommitApiId() {
        return 'ST02-00101-101';
    }

    function isSales() {
        return true;
    }

    function isCancel() {
        return true;
    }

    function isReAuth() {
        return true;
    }

    function isGetStatus() {
        return true;
    }

    function isThreePhase() {
        return true;
    }

    function getPaymentRuleDefault() {
        $arrData = parent::getPaymentRuleDefault();
        $arrData['rule_max'] = '2';
        $arrData['upper_rule'] = '9999999';
        $arrData['upper_rule_max'] = $arrData['upper_rule'];
        return $arrData;
    }

    function initParamPaymentInput(&$objPage, &$objFormParam, &$arrConfig) {
        parent::initParamPaymentInput($objPage, $objFormParam, $arrConfig);

        $objPage->arrAuthType = array(
                SPS_AUTH_DATE_COMMIT => '売上日指定要求(与信のみ)',
                SPS_AUTH_AUTO => '自動売上',
            );
        $objPage->arrOptionActive = array('1' => '要', '0' => '不要');

        $objPage->arrDealingsTypes = array(
            '10' =>'一括',
            '21' =>'ボーナス一括',
            '61' =>'分割',
            '80' =>'リボ',
            );
        $objPage->arrDivideTimes = array();
        for($i = 3; $i < 99; $i++) {
            $objPage->arrDivideTimes[ $i ] = $i ."回";
        }


        $key = 'sps_auth_type';
        $objFormParam->addParam('売上計上設定', $key, 1, 'n',
                array('EXIST_CHECK','MAX_LENGTH_CHECK', 'NUM_CHECK'),
                SC_Utils_Ex::isBlank($arrConfig[$key]) ? '0' : $arrConfig[$key]);

        $key = 'auth_order_status';
        $objFormParam->addParam('与信後の受注状況', $key, INT_LEN, 'n',
                array('EXIST_CHECK','MAX_LENGTH_CHECK', 'NUM_CHECK'),
                SC_Utils_Ex::isBlank($arrConfig[$key]) ? ORDER_NEW : $arrConfig[$key]);

        $key = 'commit_order_status';
        $objFormParam->addParam('売上確定後の受注状況', $key, INT_LEN, 'n',
                array('MAX_LENGTH_CHECK', 'NUM_CHECK'),
                SC_Utils_Ex::isBlank($arrConfig[$key]) ? ORDER_PRE_END : $arrConfig[$key]);

        $key = 'cancel_order_status';
        $objFormParam->addParam('与信取消後の受注状況', $key, INT_LEN, 'n',
                array('MAX_LENGTH_CHECK', 'NUM_CHECK'),
                SC_Utils_Ex::isBlank($arrConfig[$key]) ? ORDER_CANCEL : $arrConfig[$key]);

        $key = 'is_security_code';
        $objFormParam->addParam('セキュリティコード', $key, INT_LEN, 'n',
                array('MAX_LENGTH_CHECK', 'NUM_CHECK'),
                SC_Utils_Ex::isBlank($arrConfig[$key]) ? '0' : $arrConfig[$key]);

        $key = 'is_regist_card';
        $objFormParam->addParam('カード情報登録機能', $key, INT_LEN, 'n',
                array('MAX_LENGTH_CHECK', 'NUM_CHECK'),
                SC_Utils_Ex::isBlank($arrConfig[$key]) ? '0' : $arrConfig[$key]);

        $key = 'enable_dealings_types';
        $objFormParam->addParam('取引区分', $key, INT_LEN, 'n',
                array('MAX_LENGTH_CHECK', 'NUM_CHECK', 'EXIST_CHECK'),
                SC_Utils_Ex::isBlank($arrConfig[$key]) ? '' : $arrConfig[$key]);

        $key = 'enable_divide_times';
        $objFormParam->addParam('分割回数', $key, INT_LEN, 'n',
                array('MAX_LENGTH_CHECK', 'NUM_CHECK'),
                SC_Utils_Ex::isBlank($arrConfig[$key]) ? '' : $arrConfig[$key]);

    }

    function checkErrorPaymentInput(&$objFormParam) {
        return parent::checkErrorPaymentInput($objFormParam);
    }

    function initParam(&$objFormParam, &$arrPayment, &$arrOrder) {
        parent::initParam($objFormParam, $arrPayment, $arrOrder);

        $objFormParam->addParam('カード番号1', 'card_no01', 4, 'n', array('MAX_LENGTH_CHECK', 'NUM_CHECK', 'EXIST_CHECK'));
        $objFormParam->addParam('カード番号2', 'card_no02', 4, 'n', array('MAX_LENGTH_CHECK', 'NUM_CHECK', 'EXIST_CHECK'));
        $objFormParam->addParam('カード番号3', 'card_no03', 4, 'n', array('MAX_LENGTH_CHECK', 'NUM_CHECK', 'EXIST_CHECK'));
        $objFormParam->addParam('カード番号4', 'card_no04', 4, 'n', array('MAX_LENGTH_CHECK', 'NUM_CHECK', 'EXIST_CHECK'));
        $objFormParam->addParam('有効期限:年', 'card_year', 4, 'n', array('MAX_LENGTH_CHECK', 'NUM_CHECK', 'EXIST_CHECK'));
        $objFormParam->addParam('有効期限:月', 'card_month', 4, 'n', array('MAX_LENGTH_CHECK', 'NUM_CHECK', 'EXIST_CHECK'));
        if ($arrPayment['is_security_code']) {
            $objFormParam->addParam('セキュリティコード', 'security_code', 4, 'n', array('MAX_LENGTH_CHECK', 'NUM_CHECK', 'EXIST_CHECK'));
        }
        $objFormParam->addParam('取引区分', 'dealings_type', 4, 'n', array('MAX_LENGTH_CHECK', 'NUM_CHECK'));
        $objFormParam->addParam('分割回数', 'divide_times', 3, 'n', array('MAX_LENGTH_CHECK', 'NUM_CHECK'));
        $objFormParam->addParam("カード情報保持", "cust_manage_flg", 1, "n", array("NUM_CHECK"));
    }

    /**
     * 入力内容のチェックを行なう.
     *
     * @param SC_FormParam $objFormParam SC_FormParam インスタンス
     * @return array 入力チェック結果の配列
     */
    function checkError(&$objFormParam) {
        $arrParam = $objFormParam->getHashArray();
        $objErr = new SC_CheckError_Ex($arrParam);
        $objErr->arrErr = $objFormParam->checkError();

        if ($arrParam['dealings_type'] == '61') {
            $objErr->doFunc(array("分割回数", "divide_times", 3), array("EXIST_CHECK", "MAX_LENGTH_CHECK", "NUM_CHECK", "ZERO_CHECK"));
        }
        return $objErr->arrErr;
    }



    function modeAction($mode, &$objFormParam, &$arrOrder, $objPage) {
        // 月日配列
        $objDate = new SC_Date_Ex();
        $objDate->setStartYear(date('Y'));
        $objDate->setEndYear(date('Y') + 15);
        $objPage->arrYear = $objDate->getYear();
        $objPage->arrMonth = $objDate->getZeroMonth();
        $objPage->arrPaymentClass = $this->getPaymentClass($objPage->arrPayment, $arrOrder);
        $objPage->arrDivideTimes = $this->getDividTimes($objPage->arrPayment, $arrOrder);
        if ($mode == 'next' && $objPage->getMode() == '') {
            $mode = '';
        }
        parent::modeAction($mode, $objFormParam, $arrOrder, $objPage);
    }

    function getPaymentClass(&$arrPayment, $arrOrder) {
        $arrData = array();
        foreach ($arrPayment['enable_dealings_types'] as $id) {
            switch ($id) {
            case '10':
                $arrData['10'] = '一括';
                break;
            case '21':
                $arrData['21'] = 'ボーナス一括';
                break;
            case '61':
                $arrData['61'] = '分割';
                break;
            case '80':
                $arrData['80'] = 'リボ';
                break;
            }
        }
        if ($arrOrder['payment_total'] < MDL_SPS_DEVIDE_TOTAL_MIN) {
            if (isset($arrData['21'])) {
                unset($arrData['21']);
            }
            if (isset($arrData['61'])) {
                unset($arrData['61']);
            }
        }
        return $arrData;
    }

    function getDividTimes(&$arrPayment, $arrOrder) {
        if ($arrOrder['payment_total'] < MDL_SPS_DEVIDE_TOTAL_MIN) {
            return array();
        }
        $arrData = array();
        foreach ($arrPayment['enable_divide_times'] as $id) {
            $arrData[ $id ] = $id;
        }
        return $arrData;
    }

}
