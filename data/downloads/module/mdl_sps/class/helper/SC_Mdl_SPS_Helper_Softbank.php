<?php
/**
 *
 * @copyright 2013 Spirit of Co.,Ltd. All Rights Reserved.
 * @link http://sbps.ec-cube.pro/
 *
 */
require_once(MDL_SPS_HELPEREX_PATH . 'SC_Mdl_SPS_Helper_Base_Ex.php');

/**
 * 決済個別用ヘルパー関数 ソフトバンククラス
 */
class SC_Mdl_SPS_Helper_Softbank extends SC_Mdl_SPS_Helper_Base_Ex {

    function init() {
        $this->payment_type_id = SPS_CREDIT_LINK_PAYID;
        $this->payment_name = 'Softbank';
    }

    function getPayMethod() {
        return 'softbank2';
    }

    function getConnectType() {
        return MDL_SPS_CONNECT_TYPE_LINK;
    }

    function getSpsPayType() {
        // 購入タイプ(固定　0:都度購入、1:継続購入)
        return '0';
    }

    function getPaymentSalesApiId() {
        return 'ST02-00202-405';
    }

    function getPaymentCancelApiId() {
        return 'ST02-00306-405';
    }

    function isSales() {
        return true;
    }

    function isCancel() {
        return true;
    }

    function isReAuth() {
        return false;
    }

    function isGetStatus() {
        return false;
    }

    function getPaymentRuleDefault() {
        $arrData = parent::getPaymentRuleDefault();
        $arrData['rule_max'] = '1';
        $arrData['upper_rule'] = '100000';
        $arrData['upper_rule_max'] = $arrData['upper_rule'];
        return $arrData;
    }

    function getPaymentBlocName() {
        return 'sps_softbank';
    }

    function initParamPaymentInput(&$objPage, &$objFormParam, &$arrConfig) {
        parent::initParamPaymentInput($objPage, $objFormParam, $arrConfig);
        $objPage->arrAuthType = array(
                SPS_AUTH_DATE_COMMIT => '売上日指定要求(与信のみ)',
                SPS_AUTH_AUTO => '自動売上',
            );

        $key = 'sps_auth_type';
        $objFormParam->addParam('売上計上設定', $key, 1, 'n',
                array('EXIST_CHECK','MAX_LENGTH_CHECK', 'NUM_CHECK'),
                SC_Utils_Ex::isBlank($arrConfig[$key]) ? '0' : $arrConfig[$key]);

        $key = 'auth_order_status';
        $objFormParam->addParam('与信後の受注状況', $key, INT_LEN, 'n',
                array('EXIST_CHECK','MAX_LENGTH_CHECK', 'NUM_CHECK'),
                SC_Utils_Ex::isBlank($arrConfig[$key]) ? ORDER_NEW : $arrConfig[$key]);

        $key = 'commit_order_status';
        $objFormParam->addParam('売上確定後の受注状況', $key, INT_LEN, 'n',
                array('MAX_LENGTH_CHECK', 'NUM_CHECK'),
                SC_Utils_Ex::isBlank($arrConfig[$key]) ? ORDER_PRE_END : $arrConfig[$key]);

        $key = 'cancel_order_status';
        $objFormParam->addParam('与信取消後の受注状況', $key, INT_LEN, 'n',
                array('MAX_LENGTH_CHECK', 'NUM_CHECK'),
                SC_Utils_Ex::isBlank($arrConfig[$key]) ? ORDER_CANCEL : $arrConfig[$key]);

    }

    function checkErrorPaymentInput(&$objFormParam) {
        return parent::checkErrorPaymentInput($objFormParam);
    }

    function initParam(&$objFormParam, &$arrPayment, &$arrOrder) {
        parent::initParam($objFormParam, $arrPayment, $arrOrder);
    }

    function modeAction($mode, &$objFormParam, &$arrOrder, $objPage) {
        parent::modeAction($mode, $objFormParam, $arrOrder, $objPage);
    }

    function createSalesXmlData(&$arrOrder, &$arrPayment, $target_datetime) {
        $arrData = array();
        $objMdl =& SC_Mdl_SPS_Ex::getInstance();
        $arrModuleConfig = $objMdl->getUserSettings();
        $now_time = date("YmdHis");

        $arrData['merchant_id'] = $arrModuleConfig['id_sps_marchant'];
        $arrData['service_id'] = $arrModuleConfig['id_sps_service'];
        $arrData['tracking_id'] = $arrOrder[MDL_SPS_ORDER_COL_TRANSID];
        $arrData['processing_datetime'] = $target_datetime;
        $arrData['request_date'] = $now_time;
        $arrData['limit_second'] = '600';

        $arrData = $this->normalizeData($arrData, false);
        $arrData['sps_hashcode'] = $this->createSpsHash($arrData);
        return $arrData;
    }

    function createCancelXmlData(&$arrOrder, &$arrPayment, $target_datetime) {
        $arrData = array();
        $objMdl =& SC_Mdl_SPS_Ex::getInstance();
        $arrModuleConfig = $objMdl->getUserSettings();
        $now_time = date("YmdHis");

        $arrData['merchant_id'] = $arrModuleConfig['id_sps_marchant'];
        $arrData['service_id'] = $arrModuleConfig['id_sps_service'];
        $arrData['tracking_id'] = $arrOrder[MDL_SPS_ORDER_COL_TRANSID];
        $arrData['processing_datetime'] = $target_datetime;
        $arrData['request_date'] = $now_time;
        $arrData['limit_second'] = '600';

        $arrData = $this->normalizeData($arrData, false);
        $arrData['sps_hashcode'] = $this->createSpsHash($arrData);
        return $arrData;

    }


}
