<?php
/**
 *
 * @copyright 2013 Spirit of Co.,Ltd. All Rights Reserved.
 * @link http://sbps.ec-cube.pro/
 *
 */
require_once(MDL_SPS_HELPEREX_PATH . 'SC_Mdl_SPS_Helper_Base_Ex.php');

/**
 * 決済個別用ヘルパー関数 Rakutenクラス
 */
class SC_Mdl_SPS_Helper_Rakuten extends SC_Mdl_SPS_Helper_Base_Ex {

    function init() {
        $this->payment_type_id = SPS_RAKUTEN_LINK_PAYID;
        $this->payment_name = 'Rakuten';
    }

    function getPayMethod() {
        return 'rakuten';
    }

    function getConnectType() {
        return MDL_SPS_CONNECT_TYPE_LINK;
    }

    function getSpsPayType() {
        // 購入タイプ(固定　0:都度購入、1:継続購入)
        return '0';
    }

    function getPaymentCancelApiId() {
//        return 'ST02-00303-305';
        return '';
    }

    function getPaymentGetStatusApiId() {
        return 'MG01-00101-305';
    }

    function isSales() {
        return false;
    }

    function isCancel() {
        return false;
    }

    function isReAuth() {
        return false;
    }

    function isGetStatus() {
        return true;
    }

    function getPaymentRuleDefault() {
        $arrData = parent::getPaymentRuleDefault();
        $arrData['rule_max'] = '100';
        $arrData['upper_rule'] = '100000';
        $arrData['upper_rule_max'] = $arrData['upper_rule'];
        return $arrData;
    }

    function getPaymentBlocName() {
        return 'sps_rakuten';
    }

    function initParamPaymentInput(&$objPage, &$objFormParam, &$arrConfig) {
        parent::initParamPaymentInput($objPage, $objFormParam, $arrConfig);

        $key = 'commit_order_status';
        $objFormParam->addParam('売上確定後の受注状況', $key, INT_LEN, 'n',
                array('MAX_LENGTH_CHECK', 'NUM_CHECK'),
                SC_Utils_Ex::isBlank($arrConfig[$key]) ? ORDER_PRE_END : $arrConfig[$key]);

        $key = 'cancel_order_status';
        $objFormParam->addParam('取消後の受注状況', $key, INT_LEN, 'n',
                array('MAX_LENGTH_CHECK', 'NUM_CHECK'),
                SC_Utils_Ex::isBlank($arrConfig[$key]) ? ORDER_CANCEL : $arrConfig[$key]);

    }

    function checkErrorPaymentInput(&$objFormParam) {
        return parent::checkErrorPaymentInput($objFormParam);
    }

    function initParam(&$objFormParam, &$arrPayment, &$arrOrder) {
        parent::initParam($objFormParam, $arrPayment, $arrOrder);
    }

    function modeAction($mode, &$objFormParam, &$arrOrder, $objPage) {
        parent::modeAction($mode, $objFormParam, $arrOrder, $objPage);
    }

    function doPaymentRequest (&$objPage, &$objFormParam, &$arrOrder, &$arrPaymentInfo) {
        $this->setSJISConv();   // SJIS出力

        $objMdl =& SC_Mdl_SPS_Ex::getInstance();
        $connect_type = $this->getConnectType();

        switch ($connect_type) {
        case MDL_SPS_CONNECT_TYPE_LINK:
            $arrData = $this->createLinkData($arrOrder, $arrPaymentInfo, $objFormParam);
            $objMdl->printLog($arrData);
            $objPage->arrSpsData = $arrData;
            $objPage->tpl_next_form = 'form_sps';
            $objPage->sps_url = $this->getRequestUrl();
            // 楽天は遷移時のロゴ表示が必須なので自動遷移無し
//            $objPage->tpl_payment_onload = 'fnAutoLoadSubmitSPS();';
            break;
        case MDL_SPS_CONNECT_TYPE_API:
            break;
        case MDL_SPS_CONNECT_TYPE_FREECSV:
            break;
        }

        return true;
    }


    function doPaymentRecvOK(&$arrParam, &$arrOrder, &$arrPayment) {
        $objPurchase = new SC_Helper_Purchase_Ex();

        $sqlval = array();
        $sqlval[MDL_SPS_ORDER_COL_TRANSID] = $arrParam['res_tracking_id'];

        $new_status = $arrPayment['commit_order_status'];

        $objPurchase->sfUpdateOrderStatus($arrOrder['order_id'], $new_status, null, null, $sqlval);
        if (SC_Helper_Mobile_Ex::gfIsMobileMailAddress($arrOrder['order_email'])) {
            $template_id = '2';
        } else {
            $template_id = '1';
        }

        $objMailHelper = new SC_Helper_Mail_Ex();
        $objMailHelper->sfSendOrderMail($arrOrder['order_id'], $template_id);
        return true;
    }

    function doPaymentRecvNG(&$arrParam, &$arrOrder, &$arrPayment) {
        return true;
    }

    function doPaymentRecvCR(&$arrParam, &$arrOrder, &$arrPayment) {
        return true;
    }

    function doPaymentRecvCC(&$arrParam, &$arrOrder, &$arrPayment) {
        return true;
    }

    function createCancelXmlData(&$arrOrder, &$arrPayment, $target_datetime) {
        $arrData = array();
        $objMdl =& SC_Mdl_SPS_Ex::getInstance();
        $arrModuleConfig = $objMdl->getUserSettings();
        $now_time = date("YmdHis");

        $arrData['merchant_id'] = $arrModuleConfig['id_sps_marchant'];
        $arrData['service_id'] = $arrModuleConfig['id_sps_service'];
        $arrData['tracking_id'] = $arrOrder[MDL_SPS_ORDER_COL_TRANSID];
        $arrData['processing_datetime'] = $target_datetime;
        $arrData['request_date'] = $now_time;
        $arrData['limit_second'] = '600';

        $arrData = $this->normalizeData($arrData, false);
        $arrData['sps_hashcode'] = $this->createSpsHash($arrData);
        return $arrData;
    }


    function createGetStatusXmlData(&$arrOrder, &$arrPayment, $target_datetime) {
        $arrData = array();
        $objMdl =& SC_Mdl_SPS_Ex::getInstance();
        $arrModuleConfig = $objMdl->getUserSettings();
        $now_time = date("YmdHis");

        $arrData['merchant_id'] = $arrModuleConfig['id_sps_marchant'];
        $arrData['service_id'] = $arrModuleConfig['id_sps_service'];
        $arrData['tracking_id'] = $arrOrder[MDL_SPS_ORDER_COL_TRANSID];
        $arrData['encrypted_flg'] = '1';
        $arrData['request_date'] = $now_time;
        $arrData['limit_second'] = '600';

        $arrData = $this->normalizeData($arrData, false);
        $arrData['sps_hashcode'] = $this->createSpsHash($arrData);
        return $arrData;
    }

}
