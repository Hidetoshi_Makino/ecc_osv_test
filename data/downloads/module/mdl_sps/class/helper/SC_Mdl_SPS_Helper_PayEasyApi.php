<?php
/**
 *
 * @copyright 2013 Spirit of Co.,Ltd. All Rights Reserved.
 * @link http://sbps.ec-cube.pro/
 *
 */
require_once(MDL_SPS_HELPEREX_PATH . 'SC_Mdl_SPS_Helper_Base_Ex.php');

/**
 * 決済個別用ヘルパー関数 ConveniApiクラス
 */
class SC_Mdl_SPS_Helper_PayEasyApi extends SC_Mdl_SPS_Helper_Base_Ex {

    function init() {
        $this->payment_type_id = SPS_CREDIT_API_PAYID;
        $this->payment_name = 'PayEasyApi';
    }

    function getPayMethod() {
        return 'payeasyapi';
    }

    function getPaymentBlocName() {
        return 'sps_payeasyapi';
    }

    function getConnectType() {
        return MDL_SPS_CONNECT_TYPE_API;
    }

    function getSpsPayType() {
        // 購入タイプ(固定　0:都度購入、1:継続購入)
        return '0';
    }

    function getPaymentRequestApiId() {
        return 'ST01-00101-703';
    }

    function getPaymentCommitApiId() {
        return '';
    }

    function isThreePhase() {
        return false;
    }

    function getPaymentRuleDefault() {
        $arrData = parent::getPaymentRuleDefault();
        $arrData['rule_max'] = '1';
        $arrData['upper_rule'] = '990000';
        $arrData['upper_rule_max'] = $arrData['upper_rule'];
        return $arrData;
    }

    function getIssueType() {
        $arrData = array();
        $arrData['0'] = '発行済み';
        $arrData['1'] = '要発行';
        $arrData['2'] = '発行不要';
        return $arrData;
    }

    function initParamPaymentInput(&$objPage, &$objFormParam, &$arrConfig) {
        parent::initParamPaymentInput($objPage, $objFormParam, $arrConfig);

        $objPage->arrIssueType = $this->getIssueType();
        $objPage->arrAgency = $this->getAgency();

        $key = 'auth_order_status';
        $objFormParam->addParam('決済後の受注状況', $key, INT_LEN, 'n',
                array('EXIST_CHECK','MAX_LENGTH_CHECK', 'NUM_CHECK'),
                SC_Utils_Ex::isBlank($arrConfig[$key]) ? ORDER_PAY_WAIT : $arrConfig[$key]);

        $key = 'commit_order_status';
        $objFormParam->addParam('入金後の受注状況', $key, INT_LEN, 'n',
                array('MAX_LENGTH_CHECK', 'NUM_CHECK'),
                SC_Utils_Ex::isBlank($arrConfig[$key]) ? ORDER_PRE_END : $arrConfig[$key]);

        $key = 'cancel_order_status';
        $objFormParam->addParam('期限切れ後の受注状況', $key, INT_LEN, 'n',
                array('MAX_LENGTH_CHECK', 'NUM_CHECK'),
                SC_Utils_Ex::isBlank($arrConfig[$key]) ? ORDER_CANCEL : $arrConfig[$key]);

        $key = 'issue_type';
        $objFormParam->addParam('請求書発行区分', $key, INT_LEN, 'n',
                array('MAX_LENGTH_CHECK', 'NUM_CHECK', 'EXIST_CHECK'),
                SC_Utils_Ex::isBlank($arrConfig[$key]) ? '' : $arrConfig[$key]);

        $key = 'agency_type';
        $objFormParam->addParam('取次契約先', $key, INT_LEN, 'n',
                array('MAX_LENGTH_CHECK', 'NUM_CHECK', 'EXIST_CHECK'),
                SC_Utils_Ex::isBlank($arrConfig[$key]) ? '' : $arrConfig[$key]);

/*
        $key = 'payeasy_type';
        $objFormParam->addParam('ペイジータイプ', $key, INT_LEN, 'n',
                array('MAX_LENGTH_CHECK', 'NUM_CHECK', 'EXIST_CHECK'),
                SC_Utils_Ex::isBlank($arrConfig[$key]) ? '' : $arrConfig[$key]);
*/

    }

    function getAgency() {
        $arrData = array(
            '3' => '電算システム',
            '1' => 'ウェルネット',
            '4' => 'SMBC-FS',
        );
        return $arrData;
    }


    function checkErrorPaymentInput(&$objFormParam) {
        return parent::checkErrorPaymentInput($objFormParam);
    }

    function initParam(&$objFormParam, &$arrPayment, &$arrOrder) {
        parent::initParam($objFormParam, $arrPayment, $arrOrder);
    }

    /**
     * 入力内容のチェックを行なう.
     *
     * @param SC_FormParam $objFormParam SC_FormParam インスタンス
     * @return array 入力チェック結果の配列
     */
    function checkError(&$objFormParam) {
        $arrParam = $objFormParam->getHashArray();
        $objErr = new SC_CheckError_Ex($arrParam);
        $objErr->arrErr = $objFormParam->checkError();

        return $objErr->arrErr;
    }

    function makePaymentMemo(&$arrRes, &$arrPayment) {
        if (SC_Utils_Ex::isBlank($arrPayment['mail_title']) || SC_Utils_Ex::isBlank($arrPayment['mail_body'])) {
            return '';
        }
        $arrMemo02 = array();

        $arrMemo02['title'] = array(
                'name'  => '',
                'value' => true
            );

        $arrMemo02['paylimit'] = array(
                'name' => '払込期限',
                'value' => substr($arrRes['bill_date'],0,4) . '年' . substr($arrRes['bill_date'],4,2) . '月' . substr($arrRes['bill_date'],6,2) . '日'
            );

        $arrMemo02['skno'] = array(
                'name' => '収納機関番号',
                'value' => $arrRes['skno']
            );
        if (substr($arrRes['cust_number'],0,4) == 'http') {
            $arrMemo02['paylink'] = array(
                    'name' => 'リンク先',
                    'value' => $arrRes['cust_number']
                    );
        } else {
            list($no1, $no2) = explode('-', $arrRes['cust_number']);

            $arrMemo02['no1'] = array(
                    'name' => 'お客様番号',
                    'value' => $no1
                );
            $arrMemo02['no2'] = array(
                    'name' => '確認番号',
                    'value' => $no2
                );
        }
        if (substr($arrRes['bank_form'],0,4) == 'http') {
            $arrMemo02['paylink'] = array(
                    'name' => 'リンク先',
                    'value' => $arrRes['cust_number']
                    );
        }
        $arrMemo02['payinfo'] = array(
                'name'  => $arrPayment['mail_title'],
                'value' => $arrPayment['mail_body']
            );

        return serialize($arrMemo02);
    }

    function doPaymentRecvOK(&$arrParam, &$arrOrder, &$arrPayment, $arrInput) {
        $objPurchase = new SC_Helper_Purchase_Ex();

        $sqlval = array();
        $sqlval[MDL_SPS_ORDER_COL_TRANSID] = $arrParam['res_tracking_id'];

        $new_status = $arrPayment['auth_order_status'];

        $sqlval[MDL_SPS_ORDER_COL_PAYVIEW] = $this->makePaymentMemo($arrParam, $arrPayment);

        $objPurchase->sfUpdateOrderStatus($arrOrder['order_id'], $new_status, null, null, $sqlval);
        if (SC_Helper_Mobile_Ex::gfIsMobileMailAddress($arrOrder['order_email'])) {
            $template_id = '2';
        } else {
            $template_id = '1';
        }

        $objMailHelper = new SC_Helper_Mail_Ex();
        $objMailHelper->sfSendOrderMail($arrOrder['order_id'], $template_id);
        return true;
    }

    function doApiPaymentFix($arrRes, &$objPage, &$objFormParam, &$arrOrder, &$arrPayment) {
        $res = $this->doPaymentRecvOK($arrRes, $arrOrder, $arrPayment, $objFormParam->getHashArray());

        if ($res) {
            $this->actionCompletePage($objFormParam, $arrOrder, $arrPayment);
        }
    }

    function getApiPayMethodInfoData(&$arrInput, &$arrPayment, &$arrOrder) {
        $masterData = new SC_DB_MasterData_Ex();
        $arrPref = $masterData->getMasterData('mtb_pref');

        $arrData = array();

        $arrData['issue_type'] = '0';
        $arrData['last_name'] = SC_Util_SPS_Ex::subString(mb_convert_kana($arrOrder['order_name01'], 'ASKV'),20);
        $arrData['first_name'] =SC_Util_SPS_Ex::subString(mb_convert_kana($arrOrder['order_name02'], 'ASKV'),20);
        $arrData['last_name_kana'] = SC_Util_SPS_Ex::subString(mb_convert_kana($arrOrder['order_kana01'],'ASKV'), 20);
        $arrData['first_name_kana'] = SC_Util_SPS_Ex::subString(mb_convert_kana($arrOrder['order_kana02'], 'ASKV'),20);
        $arrData['first_zip'] = $arrOrder['order_zip01'];
        $arrData['second_zip'] = $arrOrder['order_zip02'];
        $arrData['add1'] = $arrPref[$arrOrder['order_pref']];
        $arrData['add2'] = SC_Util_SPS_Ex::subString(mb_convert_kana($arrOrder['order_addr01'], 'ASKV'),50);
        $arrData['add3'] = SC_Util_SPS_Ex::subString(mb_convert_kana($arrOrder['order_addr02'], 'ASKV'),50);
        $arrData['tel'] = $arrOrder['order_tel01'] . $arrOrder['order_tel02'] . $arrOrder['order_tel03'];
        $arrData['mail'] = $arrOrder['order_email'];
        $arrData['seiyakudate'] = date('Ymd', strtotime($arrOrder['create_date']));

        $arrData['payeasy_type'] = 'O';
        $arrData['terminal_value'] = 'P';
        $objDb = new SC_Helper_DB_Ex();
        $arrInfo = $objDb->sfGetBasisData();
        $arrData['bill_info_kana'] = SC_Util_SPS_Ex::subString(mb_convert_kana($arrInfo['shop_kana'], 'ASKV'), 48);
        if ($arrData['bill_info_kana'] == '') {
            $arrData['bill_info_kana'] = 'ショップ';
        }
        $arrData['bill_info'] = SC_Util_SPS_Ex::subString(mb_convert_kana($arrInfo['shop_name'], 'ASKV'), 24);
        $arrData['bill_note'] = '';

        return $arrData;
    }


    function doPaymentRecvPY(&$arrParam, &$arrOrder, &$arrPayment) {
        $sqlval = array();
        $sqlval[MDL_SPS_ORDER_COL_TRANSID] = $arrParam['res_tracking_id'];

        list($recv_type, $payment, $total, $email) = split(',', $arrParam['res_payinfo_key']);

        $arrMemo02 = array(
            'title' => array(
                'name'  => '',
                'value' => true
            ),
            'pay' => array(
                'name'  => '入金額',
                'value' => $payment
            ),
            'total' => array(
                'name'  => '入金累計額',
                'value' => $total
            ),
            'email' => array(
                'name'  => 'メールアドレス',
                'value' => $email
            ),
        );
        $sqlval[MDL_SPS_ORDER_COL_PAYVIEW] = serialize($arrMemo02);

        switch($recv_type) {
        case 'P':
            if ($total == $arrOrder['payment_total']) {
                $new_status =  $arrPayment['commit_order_status'];
            } else {
                $new_status =  $arrPayment['auth_order_status'];
            }
            break;
        case 'D':
            if (SC_Utils_Ex::isBlank($arrPayment['agency_type'])
                    && $arrPayment['agency_type'] == '1') {
                // ウェルネット
                if ($total == $arrOrder['payment_total']) {
                    $new_status =  $arrPayment['commit_order_status'];
                }
            } else if ($total == $arrOrder['payment_total'] 
                    && $arrOrder['status'] == $arrPayment['auth_order_status']) {
                $new_status =  $arrPayment['commit_order_status'];
            }
            break;
        case 'C':
            $new_status =  $arrPayment['auth_order_status'];
            break;
        case 'G':
            $new_status =  $arrPayment['auth_order_status'];
            break;
        default:
            return false;
            break;
        }

        if ($new_status === '0') {
            $new_status = null;
        }
        $objPurchase = new SC_Helper_Purchase_Ex();
        $objPurchase->sfUpdateOrderStatus($arrOrder['order_id'], $new_status, null, null, $sqlval);

        return true;
    }

    function modeAction($mode, &$objFormParam, &$arrOrder, $objPage) {

        switch($mode) {
        case 'return':
            break;
        case 'next':
            break;
        case 'spsreturn':
            break;
        default:
            $objPage->tpl_payment_onload = 'fnAutoLoadSubmit();';
            break;
        }
        parent::modeAction($mode, $objFormParam, $arrOrder, $objPage);
    }
}
