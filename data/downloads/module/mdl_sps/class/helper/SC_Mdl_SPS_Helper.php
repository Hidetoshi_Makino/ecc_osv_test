<?php
/**
 *
 * @copyright 2013 Spirit of Co.,Ltd. All Rights Reserved.
 * @link http://sbps.ec-cube.pro/
 *
 */

/**
 * 決済個別用ヘルパー関数クラス
 */
class SC_Mdl_SPS_Helper {

    /**
     * インスタンスを取得する
     *
     * @return SC_Mdl_SPS_Helper
     */
    function &getInstance() {
        static $_objHelper;
        if (empty($_objHelper)) {
            $_objHelper = new SC_Mdl_SPS_Helper_Ex();
        }
        $_objHelper->init();
        return $_objHelper;
    }

    function &getPaymentHelper($payment_type_id) {
        static $_objPayment;
        if (empty($_objPayment[$payment_type_id])) {
            $arrCodes = SC_Util_SPS_Ex::getPaymentTypeCodes();
            if (isset($arrCodes[$payment_type_id])) {
                $class_name = 'SC_Mdl_SPS_Helper_' . $arrCodes[$payment_type_id] . '_Ex';
            } else {
                SC_Util_SPS_Ex::printLog('Not find payment helper id:' . $payment_type_id);
                $class_name = 'SC_Mdl_SPS_Helper_Base_Ex';
            }
            if (!is_file(MDL_SPS_HELPEREX_PATH . $class_name . '.php')) {
                SC_Util_SPS_Ex::printLog('Not find payment helper:' . $class_name);
                $class_name = 'SC_Mdl_SPS_Helper_Base_Ex';
            }
            include_once(MDL_SPS_HELPEREX_PATH . $class_name . '.php');
            $_objPayment[$payment_type_id] = new $class_name();
            $objMdl =& SC_Mdl_SPS_Ex::getInstance();
            $objMdl->printLog("load payment helper: $class_name ($payment_type_id)");
        }
        $_objPayment[$payment_type_id]->init();
        return $_objPayment[$payment_type_id];
    }

    function doPaymentGetStatus($order_id, $target_datetime = null) {
        $arrOrder = SC_Util_SPS_Ex::getOrderPayData($order_id);
        if (SC_Utils_Ex::isBlank($target_datetime)) {
            $target_datetime = date("YmdHis");
        } else if (strlen($target_datetime) == 8) {
            $target_datetime .= '000000';
        }

        $arrPayment = SC_Util_SPS_Ex::getPaymentTypeConfig($arrOrder['payment_id']);
        $payment_type_id = $arrPayment[MDL_SPS_PAYMENT_COL_PAYID];

        if (SC_Utils_Ex::isBlank($payment_type_id)) {
            return false;
        }

        $objPayment =& SC_Mdl_SPS_Helper_Ex::getPaymentHelper($payment_type_id);

        if (!$objPayment->isGetStatus()) {
            return false;
        }
        return $objPayment->doApiPaymentGetStatus($arrOrder, $arrPayment, $target_datetime);
    }

    function doPaymentSales($order_id, $target_datetime = null) {
        $arrOrder = SC_Util_SPS_Ex::getOrderPayData($order_id);

        if (SC_Utils_Ex::isBlank($target_datetime)) {
            $target_datetime = date("YmdHis");
        } else if (strlen($target_datetime) == 8) {
            $target_datetime .= '000000';
        }

        $order_datetime = str_replace('-', '', substr($arrOrder['create_date'], 0, 10)) . '000000';
        if ($target_datetime < $order_datetime) {
            $objMdl =& SC_Mdl_SPS_Ex::getInstance();
            $objMdl->printLog("order_datetime: $order_datetime order_id = $order_id");
            $objMdl->printLog("proccess_datetime: $target_datetime order_id = $order_id");
            return '受注日より以前の日付は指定できません。order_id =' . $order_id;
        }

        $arrPayment = SC_Util_SPS_Ex::getPaymentTypeConfig($arrOrder['payment_id']);
        $payment_type_id = $arrPayment[MDL_SPS_PAYMENT_COL_PAYID];

        if (SC_Utils_Ex::isBlank($payment_type_id)) {
            return false;
        }

        $objPayment =& SC_Mdl_SPS_Helper_Ex::getPaymentHelper($payment_type_id);

        if (!$objPayment->isSales()) {
            return false;
        }
        return $objPayment->doApiPaymentSales($arrOrder, $arrPayment, $target_datetime);
    }

    function doPaymentCancel($order_id, $target_datetime = null) {
        $arrOrder = SC_Util_SPS_Ex::getOrderPayData($order_id);
        if (SC_Utils_Ex::isBlank($target_datetime)) {
            $target_datetime = date("YmdHis");
        } else if (strlen($target_datetime) == 8) {
            $target_datetime .= '000000';
        }

        $order_datetime = str_replace('-', '', substr($arrOrder['create_date'], 0, 10)) . '000000';
        if ($target_datetime < $order_datetime) {
            $objMdl =& SC_Mdl_SPS_Ex::getInstance();
            $objMdl->printLog("order_datetime: $order_datetime order_id = $order_id");
            $objMdl->printLog("proccess_datetime: $target_datetime order_id = $order_id");
            return '受注日より以前の日付は指定できません。order_id =' . $order_id;
        }

        $arrPayment = SC_Util_SPS_Ex::getPaymentTypeConfig($arrOrder['payment_id']);
        $payment_type_id = $arrPayment[MDL_SPS_PAYMENT_COL_PAYID];

        if (SC_Utils_Ex::isBlank($payment_type_id)) {
            return false;
        }

        $objPayment =& SC_Mdl_SPS_Helper_Ex::getPaymentHelper($payment_type_id);

        if (!$objPayment->isCancel()) {
            return false;
        }
        return $objPayment->doApiPaymentCancel($arrOrder, $arrPayment, $target_datetime);
    }

    function doPaymentReAuth($order_id, $target_datetime = null) {
        $arrOrder = SC_Util_SPS_Ex::getOrderPayData($order_id);
        if (SC_Utils_Ex::isBlank($target_datetime)) {
            $target_datetime = date("YmdHis");
        } else if (strlen($target_datetime) == 8) {
            $target_datetime .= '000000';
        }

        $arrPayment = SC_Util_SPS_Ex::getPaymentTypeConfig($arrOrder['payment_id']);
        $payment_type_id = $arrPayment[MDL_SPS_PAYMENT_COL_PAYID];

        if (SC_Utils_Ex::isBlank($payment_type_id)) {
            return false;
        }

        $objPayment =& SC_Mdl_SPS_Helper_Ex::getPaymentHelper($payment_type_id);

        if (!$objPayment->isReAuth()) {
            return false;
        }
        return $objPayment->doApiPaymentReAuth($arrOrder, $arrPayment, $target_datetime);
    }

    function doPaymentEveryStop($order_id, $target_datetime = null) {
        $arrOrder = SC_Util_SPS_Ex::getOrderPayData($order_id);
        if (SC_Utils_Ex::isBlank($target_datetime)) {
            $target_datetime = date("YmdHis");
        } else if (strlen($target_datetime) == 8) {
            $target_datetime .= '000000';
        }

        $arrPayment = SC_Util_SPS_Ex::getPaymentTypeConfig($arrOrder['payment_id']);
        $payment_type_id = $arrPayment[MDL_SPS_PAYMENT_COL_PAYID];

        if (SC_Utils_Ex::isBlank($payment_type_id)) {
            return false;
        }

        $objPayment =& SC_Mdl_SPS_Helper_Ex::getPaymentHelper($payment_type_id);

        if (!$objPayment->isEveryStop()) {
            return false;
        }
        return $objPayment->doApiPaymentEveryStop($arrOrder, $arrPayment, $target_datetime);
    }

    function doPaymentDoPayment($order_id, &$objFormParam) {
        $arrOrder = SC_Util_SPS_Ex::getOrderPayData($order_id);
        $target_datetime = date("YmdHis");

        $arrPayment = SC_Util_SPS_Ex::getPaymentTypeConfig($arrOrder['payment_id']);
        $payment_type_id = $arrPayment[MDL_SPS_PAYMENT_COL_PAYID];

        if (SC_Utils_Ex::isBlank($payment_type_id)) {
            return false;
        }

        $objPayment =& SC_Mdl_SPS_Helper_Ex::getPaymentHelper($payment_type_id);

        if (!$objPayment->isDoPayment()) {
            return false;
        }
        $obj = new stdClass();
        return $objPayment->doApiPayment($obj, $objFormParam, $arrOrder, $arrPayment, true);
    }
}
