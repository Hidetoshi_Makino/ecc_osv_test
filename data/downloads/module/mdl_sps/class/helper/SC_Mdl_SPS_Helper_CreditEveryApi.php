<?php
/**
 *
 * @copyright 2013 Spirit of Co.,Ltd. All Rights Reserved.
 * @link http://sbps.ec-cube.pro/
 *
 */
require_once(MDL_SPS_HELPEREX_PATH . 'SC_Mdl_SPS_Helper_Base_Ex.php');

/**
 * 決済個別用ヘルパー関数 CreditEveryApiクラス
 */
class SC_Mdl_SPS_Helper_CreditEveryApi extends SC_Mdl_SPS_Helper_Base_Ex {

    function init() {
        $this->payment_type_id = SPS_CREDIT_EVERY_API_PAYID;
        $this->payment_name = 'CreditEveryApi';
    }

    function getPayMethod() {
        return 'creditapi';
    }

    function getPaymentBlocName() {
        return 'sps_crediteveryapi';
    }

    function getConnectType() {
        return MDL_SPS_CONNECT_TYPE_API;
    }

    function getSpsPayType() {
        // 購入タイプ(固定　0:都度購入、1:継続購入)
        return '1';
    }

    function getPaymentRequestApiId() {
        return 'ST01-00112-101';
    }

    function getPaymentCommitApiId() {
        return 'ST02-00101-101';
    }

    function isGetStatus() {
        return false;
        //return true;
    }

    function getPaymentEveryStopApiId() {
        return 'ST02-00302-101';
    }

    function isEveryStop() {
        return true;
    }

    function isThreePhase() {
        return true;
    }
    function isReAuth() {
        return true;
    }

    function getPaymentReAuthApiId() {
        return 'ST01-00112-101';
    }

    function getPaymentRuleDefault() {
        $arrData = parent::getPaymentRuleDefault();
        $arrData['rule_max'] = '2';
        $arrData['upper_rule'] = '9999999';
        $arrData['upper_rule_max'] = $arrData['upper_rule'];
        return $arrData;
    }

    function initParamPaymentInput(&$objPage, &$objFormParam, &$arrConfig) {
        parent::initParamPaymentInput($objPage, $objFormParam, $arrConfig);

        $objPage->arrOptionActive = array('1' => '要', '0' => '不要');


        $key = 'commit_order_status';
        $objFormParam->addParam('決済後の受注状況', $key, INT_LEN, 'n',
                array('MAX_LENGTH_CHECK', 'NUM_CHECK'),
                SC_Utils_Ex::isBlank($arrConfig[$key]) ? ORDER_PRE_END : $arrConfig[$key]);

        $key = 'cancel_order_status';
        $objFormParam->addParam('与信取消後の受注状況', $key, INT_LEN, 'n',
                array('MAX_LENGTH_CHECK', 'NUM_CHECK'),
                SC_Utils_Ex::isBlank($arrConfig[$key]) ? ORDER_CANCEL : $arrConfig[$key]);

        $key = 'is_security_code';
        $objFormParam->addParam('セキュリティコード', $key, INT_LEN, 'n',
                array('MAX_LENGTH_CHECK', 'NUM_CHECK'),
                SC_Utils_Ex::isBlank($arrConfig[$key]) ? '0' : $arrConfig[$key]);

        $key = 'is_regist_card';
        $objFormParam->addParam('カード情報登録機能', $key, INT_LEN, 'n',
                array('MAX_LENGTH_CHECK', 'NUM_CHECK'),
                SC_Utils_Ex::isBlank($arrConfig[$key]) ? '0' : $arrConfig[$key]);

    }

    function checkErrorPaymentInput(&$objFormParam) {
        return parent::checkErrorPaymentInput($objFormParam);
    }

    function initParam(&$objFormParam, &$arrPayment, &$arrOrder) {
        parent::initParam($objFormParam, $arrPayment, $arrOrder);

        $objFormParam->addParam('カード番号1', 'card_no01', 4, 'n', array('MAX_LENGTH_CHECK', 'NUM_CHECK', 'EXIST_CHECK'));
        $objFormParam->addParam('カード番号2', 'card_no02', 4, 'n', array('MAX_LENGTH_CHECK', 'NUM_CHECK', 'EXIST_CHECK'));
        $objFormParam->addParam('カード番号3', 'card_no03', 4, 'n', array('MAX_LENGTH_CHECK', 'NUM_CHECK', 'EXIST_CHECK'));
        $objFormParam->addParam('カード番号4', 'card_no04', 4, 'n', array('MAX_LENGTH_CHECK', 'NUM_CHECK', 'EXIST_CHECK'));
        $objFormParam->addParam('有効期限:年', 'card_year', 4, 'n', array('MAX_LENGTH_CHECK', 'NUM_CHECK', 'EXIST_CHECK'));
        $objFormParam->addParam('有効期限:月', 'card_month', 4, 'n', array('MAX_LENGTH_CHECK', 'NUM_CHECK', 'EXIST_CHECK'));
        if ($arrPayment['is_security_code']) {
            $objFormParam->addParam('セキュリティコード', 'security_code', 4, 'n', array('MAX_LENGTH_CHECK', 'NUM_CHECK', 'EXIST_CHECK'));
        }
    }

    /**
     * 入力内容のチェックを行なう.
     *
     * @param SC_FormParam $objFormParam SC_FormParam インスタンス
     * @return array 入力チェック結果の配列
     */
    function checkError(&$objFormParam) {
        $arrParam = $objFormParam->getHashArray();
        $objErr = new SC_CheckError_Ex($arrParam);
        $objErr->arrErr = $objFormParam->checkError();

        return $objErr->arrErr;
    }

    function doPaymentRecvOK(&$arrParam, &$arrOrder, &$arrPayment) {
        $objPurchase = new SC_Helper_Purchase_Ex();

        $sqlval = array();
        $sqlval[MDL_SPS_ORDER_COL_TRANSID] = $arrParam['res_tracking_id'];

        $new_status =  $arrPayment['commit_order_status'];

        $sqlval[MDL_SPS_ORDER_COL_EVERY] = SPS_EVERY_OK;
        $sqlval[MDL_SPS_ORDER_COL_PAYVIEW] = $this->makePaymentMemo($arrParam, $arrPayment);

        $objPurchase->sfUpdateOrderStatus($arrOrder['order_id'], $new_status, null, null, $sqlval);
        if (SC_Helper_Mobile_Ex::gfIsMobileMailAddress($arrOrder['order_email'])) {
            $template_id = '2';
        } else {
            $template_id = '1';
        }

        $objMailHelper = new SC_Helper_Mail_Ex();
        $objMailHelper->sfSendOrderMail($arrOrder['order_id'], $template_id);
        return true;
    }

    function doPaymentRecvCR(&$arrParam, &$arrOrder, &$arrPayment) {
        $new_status = null;
        if (!SC_Utils_Ex::isBlank($arrPayment['cancel_order_status']) && $arrPayment['cancel_order_status'] != '0') {
            $new_status = $arrPayment['cancel_order_status'];
        }
        $objPurchase = new SC_Helper_Purchase_Ex();
        $sqlval[MDL_SPS_ORDER_COL_EVERY] = SPS_EVERY_CR;
        $objPurchase->sfUpdateOrderStatus($arrOrder['order_id'], $new_status, null, null, $sqlval);
        return true;
    }

    function doApiPaymentReAuth($arrOrder, $arrPayment, $target_datetime) {
        $res = parent::doApiPaymentReAuth(&$arrOrder, $arrPayment, $target_datetime);
        if($res === true) {
            $objPurchase = new SC_Helper_Purchase_Ex();
            $sqlval[MDL_SPS_ORDER_COL_EVERY] = SPS_EVERY_OK;
            $objPurchase->sfUpdateOrderStatus($arrOrder['order_id'], null, null, null, $sqlval);
        }
        return true;
    }

    function getApiPayMethodInfoData(&$arrInput, &$arrPayment, &$arrOrder) {
        $arrData = array();
        $arrData['cc_number'] = $arrInput['card_no01']. $arrInput['card_no02']. $arrInput['card_no03']. $arrInput['card_no04'];
        $arrData['cc_expiration'] = $arrInput['card_year']. $arrInput['card_month'];
        $arrData['security_code'] = $arrInput['security_code'];
        return $arrData;
    }

    function createReAuthXmlData(&$arrOrder, &$arrPayment, $target_datetime) {
        $arrData = array();
        $objMdl =& SC_Mdl_SPS_Ex::getInstance();
        $arrModuleConfig = $objMdl->getUserSettings();
        $now_time = date("YmdHis");

        $objDb = new SC_Helper_DB_Ex();
        $arrInfo = $objDb->sfGetBasisData();

        $arrData['merchant_id'] = $arrModuleConfig['id_sps_marchant'];
        $arrData['service_id'] = $arrModuleConfig['id_sps_service'];
        $arrData['cust_code'] =SC_Util_SPS::getCustomerCode($arrOrder['customer_id']);
        $arrData['order_id'] = $arrOrder['order_id']. '_' . $now_time; // マーチャント購入ID + リクエスト日時(YYYYMMDDHHMMSS)
        $arrData['item_id'] = 'ec-cube';
        $arrData['item_name'] = '';
        $arrData['tax'] = '';
        $arrData['amount'] = $arrOrder['payment_total'];
        $arrData['free1'] = $arrOrder['order_id'];
        $arrData['free2'] = MDL_SPS_VERSION . '_' .ECCUBE_VERSION;
        $arrData['free3'] = SC_Util_SPS_Ex::subString($arrPaymentInfo['free3'], 20);
        $arrData['sps_cust_info_return_flg'] = '1';
        $arrPluginConfig = $this->loadPluginData();
        $arrData['monthly_charge']['div_settele'] = '0';
        $arrData['monthly_charge']['camp_type'] = $arrPluginConfig['camp_type'] ? '1' : '0';

        $arrData['encrypted_flg'] = '1';
        $arrData['request_date'] = $now_time;
        $arrData['limit_second'] = '600';

        $arrData = $this->normalizeData($arrData, false);
        $arrData['sps_hashcode'] = $this->createSpsHash($arrData);

        $arrData = SC_Util_SPS_Ex::encryptData($arrData, 'pay_method_info', $arrModuleConfig);
        $arrData = SC_Util_SPS_Ex::encodeMbstring($arrData, array('free1','free2','free3'));

        $arrPayment['reauth_order_status'] = $arrPayment['commit_order_status'];
        return $arrData;
    }

    function createXmlData(&$arrOrder, &$arrPayment, &$objFormParam) {
        $arrData = array();
        $objMdl =& SC_Mdl_SPS_Ex::getInstance();
        $arrModuleConfig = $objMdl->getUserSettings();
        $now_time = date("YmdHis");

        $arrInput = $objFormParam->getHashArray();

        $objDb = new SC_Helper_DB_Ex();
        $arrInfo = $objDb->sfGetBasisData();

        $arrData['merchant_id'] = $arrModuleConfig['id_sps_marchant'];
        $arrData['service_id'] = $arrModuleConfig['id_sps_service'];
        $arrData['cust_code'] =SC_Util_SPS::getCustomerCode($arrOrder['customer_id']);
        $arrData['order_id'] = $arrOrder['order_id']. '_' . $now_time; // マーチャント購入ID + リクエスト日時(YYYYMMDDHHMMSS)
        $arrData['item_id'] = 'ec-cube';
        $arrData['item_name'] = '';
        $arrData['tax'] = '';
        $arrData['amount'] = $arrOrder['payment_total'];
        $arrData['free1'] = $arrOrder['order_id'];
        $arrData['free2'] = MDL_SPS_VERSION . '_' .ECCUBE_VERSION;
        $arrData['free3'] = SC_Util_SPS_Ex::subString($arrPaymentInfo['free3'], 20);
        $arrData['sps_cust_info_return_flg'] = '1';

        $arrData['pay_method_info'] = $this->getApiPayMethodInfoData($arrInput, $arrPayment, $arrOrder);

        $arrPluginConfig = $this->loadPluginData();
        $arrData['monthly_charge']['div_settele'] = '0';
        $arrData['monthly_charge']['camp_type'] = $arrPluginConfig['camp_type'] ? '1' : '0';

        $arrData['encrypted_flg'] = '1';
        $arrData['request_date'] = $now_time;
        $arrData['limit_second'] = '600';

        $arrData = $this->normalizeData($arrData, false);
        $arrData['sps_hashcode'] = $this->createSpsHash($arrData);

        $arrData = SC_Util_SPS_Ex::encryptData($arrData, 'pay_method_info', $arrModuleConfig);
        $arrData = SC_Util_SPS_Ex::encodeMbstring($arrData, array('free1','free2','free3'));
        return $arrData;
    }

    function modeAction($mode, &$objFormParam, &$arrOrder, $objPage) {
        // 月日配列
        $objDate = new SC_Date_Ex();
        $objDate->setStartYear(date('Y'));
        $objDate->setEndYear(date('Y') + 15);
        $objPage->arrYear = $objDate->getYear();
        $objPage->arrMonth = $objDate->getZeroMonth();
        if ($mode == 'next' && $objPage->getMode() == '') {
            $mode = '';
        }

        parent::modeAction($mode, $objFormParam, $arrOrder, $objPage);
    }

    function loadPluginData() {
        $arrRet = array();
        $objQuery =& SC_Query_Ex::getSingletonInstance();
        $where = "plugin_code = 'SpsEvery'";
        $arrData = $objQuery->getRow('*', 'dtb_plugin', $where);
        if (!SC_Utils_Ex::isBlank($arrData['free_field1'])) {
            $arrRet = unserialize($arrData['free_field1']);
        }
        return $arrRet;
    }

}
