<?php
/*
 * Copyright(c) 2013 Spirit of Co.,Ltd. Inc. All rights reserved.
 * http://sbps.ec-cube.pro/
 */

// {{{ requires
require_once CLASS_EX_REALDIR . 'page_extends/LC_Page_Ex.php';
require_once(MODULE_REALDIR . 'mdl_sps/inc/include.php');

/**
 * 決済モジュール 結果受信クラス
 *
 */
class LC_Page_Mdl_SPS_Recv extends LC_Page_Ex {

    var $objMdl;
    var $arrSetting;

    // }}}
    // {{{ functions

    /**
     * Page を初期化する.
     *
     * @return void
     */
    function init() {
        parent::init();
        $this->skip_load_page_layout = true;
    }

    /**
     * Page のプロセス.
     *
     * @return void
     */
    function process() {
        $this->action();
    }

    /**
     * Page のアクション.
     *
     * @return void
     */
    function action() {
        $objMdl =& SC_Mdl_SPS_Ex::getInstance();

        $arrResults = SC_Util_SPS_Ex::convSjisParam($_REQUEST);

        $this->lfSetPostLog($arrResults);

        $objFormParam = new SC_FormParam_Ex();
        $this->lfInitParam($objFormParam);
        $objFormParam->setParam($arrResults);
        $objFormParam->convParam();
        $objFormParam->trimParam();

        $arrErr = $this->lfCheckError($objFormParam);
        if (SC_Utils_Ex::isBlank($arrErr)) {

            $order_id = $objFormParam->getValue('free1');

            if (SC_Utils_Ex::isBlank($order_id)) {
                $this->lfDoNoOrder($objFormParam->getHashArray());
                $this->lfSendResponse(false, 'no order_id');
                SC_Response_Ex::actionExit();
            }

            if ($order_id != $objFormParam->getValue('ec_order_id')) {
                $this->lfDoNoOrder($objFormParam->getHashArray());
                $this->lfSendResponse(false, 'order_id no match');
                SC_Response_Ex::actionExit();
            }

            $objQuery =& SC_Query_Ex::getSingletonInstance();
            $objQuery->begin();

            $arrOrder = SC_Util_SPS_Ex::getOrderPayData($order_id);
            if (SC_Utils_Ex::isBlank($arrOrder)) {
                $objQuery->rollback();
                $this->lfDoNoOrder($objFormParam->getHashArray());
                $this->lfSendResponse(false, 'not order');
                SC_Response_Ex::actionExit();
            }

            if ($arrOrder[MDL_SPS_ORDER_COL_TOKEN] != $objFormParam->getValue('tid')) {
                $objQuery->rollback();
                $this->lfDoUnMatchAccessID($objFormParam->getHashArray(), $arrOrder);
                $this->lfSendResponse(false, 'transid no match');
                SC_Response_Ex::actionExit();
            }

            $res = $this->lfDoReceive($objFormParam->getHashArray(), $arrOrder);

            $objQuery->commit();

            $this->lfSendResponse($res);
            SC_Response_Ex::actionExit();
        } else {
            $objMdl =& SC_Mdl_SPS_Ex::getInstance();
            $objMdl->printLog('param_error:' . print_r($arrErr,true));
            $this->lfDoNoOrder($objFormParam->getHashArray());
            $this->lfSendResponse(false, 'param_error');
            SC_Response_Ex::actionExit();
        }
    }

    function lfDoReceive(&$arrParam, &$arrOrder) {
        $objQuery =& SC_Query_Ex::getSingletonInstance();

        // 決済手段毎のヘルパークラスを読み込み
        $arrPayment = SC_Util_SPS_Ex::getPaymentTypeConfig($arrOrder['payment_id']);
        $payment_type_id = $arrPayment[MDL_SPS_PAYMENT_COL_PAYID];

        if (SC_Utils_Ex::isBlank($payment_type_id)) {
                $objQuery->rollback();
                $this->lfDoUnMatchPayType($objFormParam->getHashArray(), $arrOrder);
                $this->lfSendResponse(false, 'payment unmatch');
                SC_Response_Ex::actionExit();
        }

        // ヘルパー呼び出し
        $objPayment =& SC_Mdl_SPS_Helper_Ex::getPaymentHelper($payment_type_id);

        if ($objPayment->getPayMethod() != $arrParam['res_pay_method']) {
            if (!($arrParam['res_pay_method'] == 'credit' &&
                    $objPayment->getPayMethod() == 'credit3d')) {
                $objQuery->rollback();
                $this->lfDoUnMatchPayType($objFormParam->getHashArray(), $arrOrder);
                $this->lfSendResponse(false, 'payment method unmatch');
                SC_Response_Ex::actionExit();
            }
        }

        $res = $objPayment->doPaymentRecv($arrParam, $arrOrder, $arrPayment);

        if ($res) {
            SC_Util_SPS_Ex::setOrderPayData($arrOrder, $arrParam);
        } else {
            SC_Util_SPS_Ex::setOrderPayData($arrOrder, $arrParam, true);
        }
        return $res;
    }

    /**
     * デストラクタ.
     *
     * @return void
     */
    function destroy() {
    }

    /**
     * POST アクセスの妥当性を検証する.無効化
     *
     */
    function doValidToken() {
    }

    function lfGetOrderId($param_OrderID) {
        list($order_id, $dummy) = explode('-', $param_OrderID);
        if (SC_Utils_Ex::isBlank($order_id) && !SC_Utils::sfIsInt($order_id)) {
            return;
        } else {
            return $order_id;
        }
    }

    /**
     * POST ログは全て残す事が推奨されている。
     *
     */
    function lfSetPostLog($arrPost) {
        $objMdl =& SC_Mdl_SPS_Ex::getInstance();
        $str = '******* sps receiveer data start *******';

        if (!SC_Utils_Ex::isBlank($arrPost)) {
            foreach($arrPost as $key => $val) {
                $str .= "\n\t" . $key . " => " . $val;
            }
        } else {
            global $HTTP_RAW_POST_DATA;
            if (strlen($HTTP_RAW_POST_DATA) > 0) {
                $str .= print_r($HTTP_RAW_POST_DATA ,true);
            } else {
                $str .= file_get_contents("php://input");
            }
        }
        $str .= "\n" . '******* sps receiveer data end *******';
        $objMdl->printLog($str);
    }

    /**
     * レスポンスを返す。
     *
     * @param boolean
     * @param mode
     * @return void
     */
    function lfSendResponse($result, $msg = '') {
        $objMdl =& SC_Mdl_SPS_Ex::getInstance();

        if ($this->is_xml_response) {
            $res_result = $result ? 'OK' : 'NG';
            $xml_api_id = $this->xml_api_id;
            $str = <<<EOS
<sps-api-response id="$xml_api_id">
<res_result>$res_result</res_result>
<res_err_msg>$msg</res_err_msg>
</sps-api-response>

EOS;
            echo $str;
            $objMdl->printLog($str);
            return;
        }
        if($result) {
            $objMdl->printLog('response: OK');
            echo 'OK';
        } else {
            $objMdl->printLog('response: NG,' . $msg);
            echo 'NG,' . $msg;
        }
        return;
    }

    /**
     * パラメーター情報の初期化を行う.
     *
     * @param SC_FormParam $objFormParam SC_FormParam インスタンス
     * @return void
     */
    function lfInitParam(&$objFormParam) {
        $objFormParam->addParam("ec_order_id", "ec_order_id", INT_LEN, 'n', array("MAX_LENGTH_CHECK", "EXIST_CHECK", "NUM_CHECK"));
        $objFormParam->addParam("tid", "tid", STEXT_LEN, 'a', array("MAX_LENGTH_CHECK", "EXIST_CHECK"));

        $objFormParam->addParam("pay_method", "pay_method", 1000, 'a', array("MAX_LENGTH_CHECK", "EXIST_CHECK"));
        $objFormParam->addParam("merchant_id", "merchant_id", 5, 'n', array("MAX_LENGTH_CHECK", "EXIST_CHECK"));
        $objFormParam->addParam("service_id", "service_id", 3, 'n', array("MAX_LENGTH_CHECK", "EXIST_CHECK"));
        $objFormParam->addParam("cust_code", "cust_code", 64, 'a', array("MAX_LENGTH_CHECK", "EXIST_CHECK"));
        $objFormParam->addParam("sps_cust_no", "sps_cust_no", 12, 'n', array("MAX_LENGTH_CHECK"));
        $objFormParam->addParam("sps_payment_no", "sps_payment_no", 3, 'n', array("MAX_LENGTH_CHECK"));
        $objFormParam->addParam("order_id", "order_id", 38, 'a', array("MAX_LENGTH_CHECK", "EXIST_CHECK"));
        $objFormParam->addParam("item_id", "item_id", 32, 'a', array("MAX_LENGTH_CHECK"));
        $objFormParam->addParam("pay_item_id", "pay_item_id", 32, 'a', array("MAX_LENGTH_CHECK"));
        $objFormParam->addParam("item_name", "pay_item_id", 40, 'KVa', array("MAX_LENGTH_CHECK"));
        $objFormParam->addParam("tax", "tax", 8, 'n', array("MAX_LENGTH_CHECK", "NUM_CHECK"));
        $objFormParam->addParam("amount", "amount", 8, 'n', array("MAX_LENGTH_CHECK", "NUM_CHECK", "EXIST_CHECK"));
        $objFormParam->addParam("pay_type", "pay_type", 1, 'n', array("MAX_LENGTH_CHECK", "NUM_CHECK"));
        $objFormParam->addParam("auto_charge_type", "auto_charge_type", 1, 'n', array("MAX_LENGTH_CHECK"));
        $objFormParam->addParam("service_type", "service_type", 1, 'n', array("MAX_LENGTH_CHECK"));
        $objFormParam->addParam("div_settele", "div_settele", 1, 'n', array("MAX_LENGTH_CHECK"));
        $objFormParam->addParam("last_charge_month", "last_charge_month", 6, 'n', array("MAX_LENGTH_CHECK", "NUM_CHECK"));
        $objFormParam->addParam("camp_type", "camp_type", 1, 'n', array("MAX_LENGTH_CHECK", "NUM_CHECK"));
        $objFormParam->addParam("tracking_id", "tracking_id", 14, 'n', array("MAX_LENGTH_CHECK"));
        $objFormParam->addParam("terminal_type", "terminal_type", 1, 'n', array("MAX_LENGTH_CHECK"));
        $objFormParam->addParam("free1", "free1", 20, 'a', array("MAX_LENGTH_CHECK"));
        $objFormParam->addParam("free2", "free2", 20, 'a', array("MAX_LENGTH_CHECK"));
        $objFormParam->addParam("free3", "free3", 20, 'a', array("MAX_LENGTH_CHECK"));
        $objFormParam->addParam("dtl_rowno", "dtl_rowno", 2, 'n', array("MAX_LENGTH_CHECK"));
        $objFormParam->addParam("dtl_item_id", "dtl_item_id", 20, 'a', array("MAX_LENGTH_CHECK"));
        $objFormParam->addParam("dtl_item_name", "dtl_item_name", 40, 'KVa', array("MAX_LENGTH_CHECK"));
        $objFormParam->addParam("dtl_item_count", "dtl_item_count", 6, 'n', array("MAX_LENGTH_CHECK"));
        $objFormParam->addParam("dtl_tax", "dtl_tax", 8, 'n', array("MAX_LENGTH_CHECK"));
        $objFormParam->addParam("dtl_amount", "dtl_amount", 8, 'n', array("MAX_LENGTH_CHECK"));
        $objFormParam->addParam("request_date", "request_date", 14, 'n', array("MAX_LENGTH_CHECK"));

        $objFormParam->addParam("res_pay_method", "res_pay_method", 20, 'a', array("MAX_LENGTH_CHECK", "EXIST_CHECK"));
        $objFormParam->addParam("res_result", "res_result", 2, 'a', array("MAX_LENGTH_CHECK", "EXIST_CHECK"));
        $objFormParam->addParam("res_tracking_id", "res_tracking_id", 14, 'a', array("MAX_LENGTH_CHECK", "EXIST_CHECK"));
        $objFormParam->addParam("res_sps_cust_no", "res_sps_cust_no", 12, 'n', array("MAX_LENGTH_CHECK"));
        $objFormParam->addParam("res_sps_payment_no", "res_sps_payment_no", 3, 'n', array("MAX_LENGTH_CHECK"));
        $objFormParam->addParam("res_payinfo_key", "res_payinfo_key", 145, 'a', array("MAX_LENGTH_CHECK"));
        $objFormParam->addParam("res_payment_date", "res_payment_date", 14, 'n', array("MAX_LENGTH_CHECK"));
        $objFormParam->addParam("res_err_code", "res_err_code", 4, 'a', array("MAX_LENGTH_CHECK"));
        $objFormParam->addParam("res_date", "res_date", 14, 'n', array("MAX_LENGTH_CHECK", "EXIST_CHECK"));
        $objFormParam->addParam("limit_second", "limit_second", 4, 'n', array("MAX_LENGTH_CHECK"));
        $objFormParam->addParam("sps_hashcode", "sps_hashcode", 40, 'n', array("MAX_LENGTH_CHECK"));
        $objFormParam->addParam('is_xml', 'is_xml');

    }

    /**
     * 入力内容のチェックを行なう.
     *
     * @param SC_FormParam $objFormParam SC_FormParam インスタンス
     * @return array 入力チェック結果の配列
     */
    function lfCheckError(&$objFormParam) {
        $arrForm = $objFormParam->getHashArray();
        $objErr = new SC_CheckError_Ex($arrForm);
        $objErr->arrErr = $objFormParam->checkError();

        $objMdl =& SC_Mdl_SPS_Ex::getInstance();
        $arrMdlSetting = $objMdl->getUserSettings();

        if (!SC_Utils_Ex::isBlank($objErr->arrErr)
                && SC_Utils_Ex::isBlank($_POST)) {
            global $HTTP_RAW_POST_DATA;
            if (strpos($HTTP_RAW_POST_DATA, 'sps-api-') !== FALSE) {
                $arrRes = SC_Util_SPS_Ex::changeXml2Array($HTTP_RAW_POST_DATA);
                if (!SC_Utils_Ex::isBlank($arrRes['pay_method_info'])) {
                    $arrModuleConfig = $objMdl->getUserSettings();
                    $arrDec = SC_Util_SPS_Ex::decodeData($arrRes['pay_method_info'], $arrModuleConfig);
//                    $arrDec = $arrRes['pay_method_info'];
                    if (is_array($arrDec)) {
                        foreach ($arrDec as $key => $val) {
                            $arrRes[$key] = $val;
                        }
                    }
                    unset ($arrRes['pay_method_info']);
                    $arrParam = $this->lfConvApiXmlParam($arrRes);
                    $objFormParam->setParam($arrParam);
                    $objFormParam->convParam();
                    $objFormParam->trimParam();
                    $arrForm = $objFormParam->getHashArray();
                    $objErr = new SC_CheckError_Ex($arrForm);
                    $objErr->arrErr = $objFormParam->checkError();
                }

            }
        }

        if (!SC_Utils_Ex::isBlank($arrMdlSetting['connect_from_ip'])) {
            if ($_SERVER['REMOTE_ADDR'] != trim($arrMdlSetting['connect_from_ip'])) {
                $objErr->arrErr['merchant_id'] = '※ 通信元IPアドレスとは異なるIPアドレスからの通知です。';
            }

        }
        if ($objFormParam->getValue('merchant_id') != trim($arrMdlSetting['id_sps_marchant'])) {
            $objErr->arrErr['merchant_id'] = '※merchant_idが一致しません。';
        }
        if ($objFormParam->getValue('service_id') != trim($arrMdlSetting['id_sps_service'])) {
            $objErr->arrErr['service_id'] = '※service_idが一致しません。';
        }

        return $objErr->arrErr;
    }

    function lfConvApiXmlParam($arrRes) {
        if (SC_Utils_Ex::isBlank($arrRes['tracking_id'])) {
            return false;
        }
        $objQuery =& SC_Query_Ex::getSingletonInstance();

        $order_id = $objQuery->get('order_id', 'dtb_order', 'del_flg = 0 AND ' . MDL_SPS_ORDER_COL_TRANSID . ' = ?',
                array($arrRes['tracking_id']));
        if (SC_Utils_Ex::isBlank($order_id)) {
            return false;
        }
        $arrRes['res_tracking_id'] = $arrRes['tracking_id'];
        $arrRes['rec_type'] = trim($arrRes['rec_type']);
        switch ($arrRes['rec_type']) {
            case '1':
                $arrRes['res_payinfo_key'] = 'P';
                $arrRes['res_result'] = 'PY';
                break;
            case '2':
                $arrRes['res_payinfo_key'] = 'C';
                $arrRes['res_result'] = 'PY';
                break;
            case '3':
                $arrRes['res_payinfo_key'] = 'D';
                $arrRes['res_result'] = 'PY';
                break;
            case '4':
                $arrRes['res_payinfo_key'] = 'G';
                $arrRes['res_result'] = 'PY';
                break;
            default:
                $arrRes['res_payinfo_key'] = 'NG';
                $arrRes['res_result'] = 'CN';
                break;
        }
        $arrRes['res_payinfo_key'] .= ',' . trim($arrRes['rec_amount']) . ',' . trim($arrRes['rec_amount_total'])
                                . ',' . trim($arrRes['res_mail']);
        $arrRes['res_payment_date'] = $arrRes['request_date'];

        $arrRes['ec_order_id'] = $order_id;
        $arrRes['free1'] = $order_id;
        $arrRes['is_xml'] = true;
        $arrOrder = SC_Util_SPS_Ex::getOrderPayData($order_id);
        $arrRes['tid'] = $arrOrder[MDL_SPS_ORDER_COL_TOKEN];
        $arrRes['cust_code'] = SC_Util_SPS::getCustomerCode($arrOrder['customer_id']);
        $arrRes['order_id'] = $arrOrder['order_id'];

        $arrPayment = SC_Util_SPS_Ex::getPaymentTypeConfig($arrOrder['payment_id']);
        $payment_type_id = $arrPayment[MDL_SPS_PAYMENT_COL_PAYID];
        $objPayment =& SC_Mdl_SPS_Helper_Ex::getPaymentHelper($payment_type_id);
        $arrRes['pay_method'] = $objPayment->getPayMethod();
        $arrRes['amount'] = $arrRes['rec_amount'];
        $arrRes['res_pay_method'] = $arrRes['pay_method'];
        $arrRes['res_date'] = $arrRes['request_date'];
        $this->is_xml_response = true;
        $this->xml_api_id = $arrRes['id'];
        return $arrRes;
    }


    function lfSendMail($tplpath, $subject, $arrParam, $arrOrder = array()) {
        $objMdl =& SC_Mdl_SPS_Ex::getInstance();

        $objPage = new LC_Page_Ex();
        $objPage->arrParam = $arrParam;
        $objPage->arrOrder = $arrOrder;
        $objMailView = new SC_SiteView_Ex();
        $arrInfo = SC_Helper_DB_Ex::sfGetBasisData();
        $objMailView->assignobj($objPage);
        $body = $objMailView->fetch($tplpath);
        // メール送信処理
        $objSendMail = new SC_SendMail_Ex();
        $to = $arrInfo['email02'];
        $from = $arrInfo['email03'];
        $error = $arrInfo['email04'];
        $objSendMail->setItem($to, $subject, $body, $from, $arrInfo['shop_name'], $from, $error, $error);
        $objSendMail->sendMail();
    }

    function lfDoNoOrder($arrParam) {
        $tplpath = MDL_SPS_TEMPLATE_PATH . 'mail_template/recv_no_order.tpl';
        $subject = MDL_SPS_MODULE_NAME . ' 不一致データ検出';

        $this->lfSendMail($tplpath, $subject, $arrParam);
    }

    function lfDoUnMatchAccessID(&$arrParam, &$arrOrder) {
        $tplpath = MDL_SPS_TEMPLATE_PATH . 'mail_template/recv_unmatch_transid.tpl';
        $subject = MDL_SPS_MODULE_NAME . ' Transaction ID不一致データ検出';
        $this->lfSendMail($tplpath, $subject, $arrParam, $arrOrder);
    }

    function lfDoUnMatchPayType(&$arrParam, $arrOrder) {
        $tplpath = MDL_SPS_TEMPLATE_PATH . 'mail_template/recv_unmatch_paytype.tpl';
        $subject = MDL_SPS_MODULE_NAME . ' 支払い方法不一致データ検出';
        $this->lfSendMail($tplpath, $subject, $arrParam, $arrOrder);
    }

}


