<?php
/*
 * Copyright(c) 2013 Spirit of Co.,Ltd. All rights reserved.
 * http://sbps.ec-cube.pro/
 */

require_once(MODULE_REALDIR . 'mdl_sps/inc/include.php');
require_once(CLASS_EX_REALDIR . 'page_extends/mypage/LC_Page_AbstractMypage_Ex.php');

/**
 * カード登録内容変更 のページクラス.
 *
 * @package SpsUtils
 * @author Spirit of Co.,Ltd.
 */

class LC_Page_Mypage_ChangeCard extends LC_Page_AbstractMypage_Ex {

    // }}}
    // {{{ functions

    /**
     * Page を初期化する.
     *
     * @return void
     */
    function init() {
        parent::init();
        $this->tpl_subtitle = 'カード情報登録内容変更';
        $this->tpl_mypageno = 'change_card';
        $this->httpCacheControl('nocache');

        $objDate = new SC_Date_Ex();
        $objDate->setStartYear(date('Y'));
        $objDate->setEndYear(date('Y') + 15);
        $this->arrYear = $objDate->getYear();
        $this->arrMonth = $objDate->getZeroMonth();

        $this->objMdl =& SC_Mdl_SPS_Ex::getInstance();
        $this->arrConfig = $this->objMdl->getUserSettings();

    }

    /**
     * Page のプロセス.
     *
     * @return void
     */
    function process() {
        parent::process();
    }

    /**
     * Page のプロセス
     * @return void
     */
    function action() {

        $objCustomer = new SC_Customer_Ex();
        $customer_id = $objCustomer->getValue('customer_id');

        // パラメーター管理クラス,パラメーター情報の初期化
        $objFormParam = new SC_FormParam_Ex();

        $arrPluginConfig = $this->loadPluginConfigData();

        if ($arrPluginConfig['mypage_enable'] != '1') {
            $msg =  "例外エラー<br />カード変更機能は無効です。";
            SC_Utils_Ex::sfDispSiteError(FREE_ERROR_MSG, "", true, $msg);
        }

        // データロードはクレジットカードのベースオブジェクト
        if ($arrPluginConfig['mypage_type'] == '1') {
            $payment_type_id = SPS_CREDIT_API_PAYID;
            $this->is_move_payment_service = false;
        } else {
            $this->is_move_payment_service = true;
            $payment_type_id = SPS_CREDIT3D_LINK_PAYID;
        }
        $objPayment =& SC_Mdl_SPS_Helper_Ex::getPaymentHelper($payment_type_id);
        $payment_id = $this->getPaymentId($payment_type_id);
        $this->arrPayment = SC_Util_SPS_Ex::getPaymentTypeConfig($payment_id);
        $objCustomer->updateSession();
        $arrCustomer = $objCustomer->customer_data;
        $arrCustomer['order_id'] = 'cc_' . $arrCustomer['customer_id'];

        // 処理は登録用モジュールを使う
        if ($arrPluginConfig['mypage_type'] == '1') {
            $payment_type_id = SPS_REGIST_CREDIT_API_PAYID;
        } else {
            $payment_type_id = SPS_CREDIT3D_REGIST_LINK_PAYID;
        }
        $objPayment =& SC_Mdl_SPS_Helper_Ex::getPaymentHelper($payment_type_id);

        $objPayment->initParam($objFormParam, $this->arrPayment, $arrCustomer);

        $objFormParam->setParam($this->arrPayment);
        $objFormParam->setParam($_POST);
        $objFormParam->convParam();

        $this->tpl_url = "?";
        $mode = $this->getMode();
        if(SC_Display_Ex::detectDevice() === DEVICE_TYPE_MOBILE && $this->getMode() == '') {
            $mode = 'next';
        }else if(SC_Display_Ex::detectDevice() === DEVICE_TYPE_SMARTPHONE) {
            $this->tpl_url = './change_card.php';
        }
        $connect_type = $objPayment->getConnectType();

        switch ($this->getMode()) {
            case 'next':
                switch ($connect_type) {
                case MDL_SPS_CONNECT_TYPE_LINK:
                case MDL_SPS_CONNECT_TYPE_FREECSV:
                    $objPayment->setSJISConv($this);

                    $url = HTTPS_URL . 'mypage/change_card.php?mode=spsreturn';
                    if (SC_Display_Ex::detectDevice() === DEVICE_TYPE_MOBILE) {
                        $url .= '&' . session_name() . '=' . session_id();
                    }
                    $url .= '&' . TRANSACTION_ID_NAME . '=' . SC_Helper_Session_Ex::getToken();
                    SC_Utils_Ex::sfRmDupSlash($url);

                    $arrData = $objPayment->createLinkData($arrCustomer, $arrPaymentInfo, $objFormParam, $url);

                    $this->objMdl->printLog($arrData);
                    $this->arrSpsData = $arrData;
                    $this->tpl_next_form = 'form_sps';
                    $this->sps_url = $objPayment->getRequestUrl();
                    $this->tpl_onload = 'fnAutoLoadSubmitSPS();';
                    break;
                case MDL_SPS_CONNECT_TYPE_API:
                    $this->arrErr = $objPayment->checkError($objFormParam);
                    if (SC_Utils_Ex::isBlank($this->arrErr)) {
                        $objMdl =& SC_Mdl_SPS_Ex::getInstance();
                        $arrModuleConfig = $objMdl->getUserSettings();
                        $arrData = $objPayment->createXmlData($arrCustomer, $arrPaymentInfo, $objFormParam);
                        $request_xml = SC_Util_SPS_Ex::changeArray2Xml($arrData, $objPayment->getPaymentRequestApiId());
                        list($r_code, $response) = SC_Util_SPS_Ex::sendRequestSps($request_xml, $arrModuleConfig);
                        $arrRes = $objPayment->checkResponse($r_code, $response, $this);
                        if ($arrRes !== false) {
                            $this->tpl_error = '正常に完了しました';
                        } else {
                            $this->tpl_error = '処理を中断しました。エラーまたはキャンセルが生じました。';
                            $this->tpl_error .= implode("\n", $this->arrErr);
                        }
                    }
                }

                break;
            case 'spsreturn':
                $this->objMdl->printLog($_REQUEST);
                if ($_REQUEST['res_result'] == 'OK') {
                    $this->tpl_error = '正常に完了しました';
                } else {
                    $this->tpl_error = '処理を中断しました。エラーまたはキャンセルが生じました。';
                }
                break;
            default:
                switch ($connect_type) {
                    case MDL_SPS_CONNECT_TYPE_LINK:
                    case MDL_SPS_CONNECT_TYPE_FREECSV:
                        $this->tpl_onload = 'fnAutoLoadSubmit();';
                        break;
                }
                break;
        }

        if ($connect_type == MDL_SPS_CONNECT_TYPE_API) {
            $this->tpl_onload .= "$('#dealings_type').parent().parent().hide();";
        }

        $this->tpl_form_bloc_path = $objPayment->getFormBloc();
        if ($this->tpl_form_bloc_path == "") {
            $msg = "例外エラー<br />決済用ブロックテンプレートが見つかりません。<br />この手続きは無効となりました。<br />管理者に連絡をして下さい。";
            $this->objMdl->printLog($msg . print_r($_REQUEST, true));
            SC_Utils_Ex::sfDispSiteError(FREE_ERROR_MSG, "", true, $msg);
            SC_Response_Ex::actionExit();
        }
        $this->arrForm = $objFormParam->getFormParamList();
    }

    /**
     * デストラクタ.
     *
     * @return void
     */
    function destroy() {
    }

    function loadPluginConfigData() {
        $arrRet = array();
        $objQuery =& SC_Query_Ex::getSingletonInstance();
        $where = "plugin_code = 'SpsUtils'";
        $arrData = $objQuery->getRow('*', 'dtb_plugin', $where);
        if (!SC_Utils_Ex::isBlank($arrData['free_field1'])) {
            $arrRet = unserialize($arrData['free_field1']);
        }
        return $arrRet;
    }

    // DBからデータを取得する
    function getPaymentId($type){
        $objMdl =& SC_Mdl_SPS_Ex::getInstance();
        $objQuery =& SC_Query::getSingletonInstance();
        $arrVal = array($objMdl->getCode(true), (string)$type);
        $payment_id = $objQuery->get("payment_id", "dtb_payment", "module_code = ? AND " . MDL_SPS_PAYMENT_COL_PAYID . " = ? AND del_flg = 0", $arrVal);
        return $payment_id;
    }
}
