<?php
/*
 * Copyright(c) 2013 Spirit of Co.,Ltd. All rights reserved.
 * http://sbps.ec-cube.pro/
 */

// {{{ requires
require_once(MDL_SPS_CLASS_PATH . "pages/LC_Page_Mdl_SPS_Helper.php");

/**
 * SPS決済画面のページクラス.
 *
 * @package Page
 * @author SOFTBANK PAYMENT SERVICE Corp.
 */
class LC_Page_Mdl_SPS_Helper_Ex extends LC_Page_Mdl_SPS_Helper {

	/**
	 * Page action extends
	 * @return [type] [description]
	 */
	function action() {
		// parent::action();

        $objSiteSess = new SC_SiteSession_Ex();
        $objPurchase = new SC_Helper_Purchase_Ex();
        $objCartSess = new SC_CartSession_Ex();

        if (!SC_Utils_Ex::isBlank($_SESSION['order_id'])) {
            $order_id = $_SESSION['order_id'];
        } else if (!SC_Utils_Ex::isBlank($_REQUEST['order_id'])
                     && SC_Utils_Ex::sfIsInt($_REQUEST['order_id'])
                     && $this->lfIsValidToken($_REQUEST['order_id'], $_REQUEST[TRANSACTION_ID_NAME])) {
            $order_id = $_REQUEST['order_id'];
            $_SESSION['order_id'] = $order_id;
        } else {
            $msg =  "例外エラー<br />注文情報の取得が出来ませんでした。<br />この手続きは無効となりました。";
            $this->objMdl->printLog($msg);
            SC_Utils_Ex::sfDispSiteError(FREE_ERROR_MSG, "", true, $msg);
        }

        $arrOrder = $objPurchase->getOrder($order_id);
        $this->tpl_title = $arrOrder['payment_method'];
        $objFormParam = new SC_FormParam_Ex();

        if ($arrOrder['status'] != ORDER_PENDING) {
            switch ($arrOrder['status']) {
            case ORDER_NEW:
            case ORDER_PRE_END:
                SC_Response_Ex::sendRedirect(SHOPPING_COMPLETE_URLPATH);
                SC_Response_Ex::actionExit();
                break;
            case ORDER_PAY_WAIT:
                // リンク型遷移での戻りは各ヘルパーに処理させる場合があるため、リダイレクトしない。
                if ($this->getMode() != 'spsreturn') {
                    SC_Response_Ex::sendRedirect(SHOPPING_COMPLETE_URLPATH);
                    SC_Response_Ex::actionExit();
                }
                break;
            default:
                if ($this->getMode() != 'spsreturn' && !SC_Utils_Ex::isBlank($arrOrder['status'])) {
                    $msg = "例外エラー<br />注文情報が無効です。<br />この手続きは無効となりました。";
                    $this->objMdl->printLog($msg);
                    SC_Utils_Ex::sfDispSiteError(FREE_ERROR_MSG, "", true, $msg);
                    SC_Response_Ex::actionExit();
                }
                break;
            }
        }

        // 決済手段毎のヘルパークラスを読み込み
        $arrPayment = SC_Util_SPS_Ex::getPaymentTypeConfig($arrOrder['payment_id']);
        $this->arrPayment = $arrPayment;
        $payment_type_id = $arrPayment[MDL_SPS_PAYMENT_COL_PAYID];

        if (SC_Utils_Ex::isBlank($payment_type_id)) {
            $msg = "例外エラー<br />注文情報の決済方法と決済モジュールの設定が一致していません。<br />この手続きは無効となりました。<br />管理者に連絡をして下さい。";
            $this->objMdl->printLog($msg);
            SC_Utils_Ex::sfDispSiteError(FREE_ERROR_MSG, "", true, $msg);
            SC_Response_Ex::actionExit();
        }

        // ヘルパー呼び出し
        $objPayment =& SC_Mdl_SPS_Helper_Ex::getPaymentHelper($payment_type_id);

        $this->lfSetToken($arrOrder, $arrPayment[MDL_SPS_PAYMENT_COL_PAYID]);

        $objPayment->initParam($objFormParam, $arrPayment, $arrOrder);

        $objFormParam->setParam($arrPayment);
        $objFormParam->setParam($_REQUEST);
        $objFormParam->convParam();

        $this->tpl_url = "?";
        $mode = $this->getMode();
        if(SC_Display_Ex::detectDevice() === DEVICE_TYPE_MOBILE && $this->getMode() == '') {
            $mode = 'next';
        }else if(SC_Display_Ex::detectDevice() === DEVICE_TYPE_SMARTPHONE) {
            $this->tpl_url = SHOPPING_MODULE_URLPATH;
        }

        $objPayment->modeAction($mode, $objFormParam, $arrOrder, $this);

		// Check paid member: Start
		// In the case of paid members, rewrite to continuing data.
GC_Utils_Ex::gfDebugLog('----------Before infomation---------');
GC_Utils_Ex::gfDebugLog($this->arrSpsData);
		if($arrOrder[product_type] == PRODUCT_TYPE_MONTHLY) {
			// Change prodcess
GC_Utils_Ex::gfDebugLog('-----------Pay type change---------');
			// pay_type: 1: 継続課金（簡易） 2: 継続課金（定期・従量）
			$this->arrSpsData['pay_type'] = 2;
			$this->arrSpsData['auto_charge_type'] = 0;
			$this->arrSpsData['div_settele'] = 0;
			// camp_type 継続課金（簡易）の場合のみ指定可能
			// $this->arrSpsData['camp_type'] = 0;
			unset($this->arrSpsData['sps_hashcode']);
			$this->arrSpsData['sps_hashcode'] = $this->createSpsHash($this->arrSpsData);
		} else {
			// Not change
GC_Utils_Ex::gfDebugLog('-----------Pay type not change-----');
		}
GC_Utils_Ex::gfDebugLog('----------After infomation---------');
GC_Utils_Ex::gfDebugLog($this->arrSpsData);
		// Chaeck paid member: End


        $this->tpl_form_bloc_path = $objPayment->getFormBloc();
        if ($this->tpl_form_bloc_path == "") {
            $msg = "例外エラー<br />決済用ブロックテンプレートが見つかりません。<br />この手続きは無効となりました。<br />管理者に連絡をして下さい。";
            $this->objMdl->printLog($msg);
            SC_Utils_Ex::sfDispSiteError(FREE_ERROR_MSG, "", true, $msg);
            SC_Response_Ex::actionExit();
        }

        $this->arrForm = $objFormParam->getFormParamList();

        $this->tpl_payment_onload = $this->tpl_payment_onload;

        if (defined('MDL_SPS_DEBUG') && MDL_SPS_DEBUG) {
            $this->objMdl->printLog(print_r($this,true));
        }
	}

	function createSpsHash($arrData) {
        $objMdl =& SC_Mdl_SPS_Ex::getInstance();
        $key_for_checksum = $objMdl->getUserSettings('id_sps_hash');

        $join_param = '';
        foreach ($arrData as $val) {
            if (is_array($val) === false) {
                $join_param .= $val;
            } else {
                foreach ($val as $val2) {
                    if (is_array($val2) === false) {
                        $join_param .= $val2;
                    } else {
                        foreach ($val2 as $val3) {
                            $join_param .= $val3;
                        }
                    }
                }
            }
        }
        $hashcode = sha1($join_param . trim($key_for_checksum));
        return $hashcode;
    }
}
