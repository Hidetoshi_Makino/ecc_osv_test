<?php
/*
 * Copyright(c) 2013 Spirit of Co.,Ltd. All rights reserved.
 * http://sbps.ec-cube.pro/
 */

// {{{ requires
require_once(MDL_SPS_CLASS_PATH . "pages/LC_Page_Mdl_SPS_Recv.php");

/**
 * SPS決済モジュールの通知受信のページクラス.
 *
 * @package Page
 * @author SOFTBANK PAYMENT SERVICE Corp.
 */
class LC_Page_Mdl_SPS_Recv_Ex extends LC_Page_Mdl_SPS_Recv {

}
