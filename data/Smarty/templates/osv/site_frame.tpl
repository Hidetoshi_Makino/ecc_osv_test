<!DOCTYPE html>
<!--{*
 * This file is part of EC-CUBE
 *
 * Copyright(c) 2000-2014 LOCKON CO.,LTD. All Rights Reserved.
 *
 * http://www.lockon.co.jp/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *}-->

<html lang="ja">
<head>
<meta charset=<!--{$smarty.const.CHAR_CODE}-->" />
<meta name="viewport" content="width=device-width, initial-scale=1, viewport-fit=cover">
<meta http-equiv="X-UA-Compatible" content="IE=edge">

<title><!--{$arrSiteInfo.shop_name|h}--><!--{if $tpl_subtitle|strlen >= 1}--> | <!--{$tpl_subtitle|h}--><!--{elseif $tpl_title|strlen >= 1}--> | <!--{$tpl_title|h}--><!--{/if}--></title>
<link rel="apple-touch-icon" sizes="180x180" href="<!--{$TPL_URLPATH}-->img/common/apple-touch-icon.png">
<link rel="icon" type="image/x-icon" href="<!--{$TPL_URLPATH}-->img/common/favicon.ico">
<meta http-equiv="Content-Script-Type" content="text/javascript" />
<meta http-equiv="Content-Style-Type" content="text/css" />
<!--{if $arrPageLayout.author|strlen >= 1}-->
    <meta name="author" content="<!--{$arrPageLayout.author|h}-->" />
<!--{/if}-->
<!--{if $arrPageLayout.description|strlen >= 1}-->
    <meta name="description" content="<!--{$arrPageLayout.description|h}-->" />
<!--{/if}-->
<!--{if $arrPageLayout.keyword|strlen >= 1}-->
    <meta name="keywords" content="<!--{$arrPageLayout.keyword|h}-->" />
<!--{/if}-->
<!--{if $arrPageLayout.meta_robots|strlen >= 1}-->
    <meta name="robots" content="<!--{$arrPageLayout.meta_robots|h}-->" />
<!--{/if}-->

        <!--{* EC-Cube defalt archive: Start }-->
<link rel="stylesheet" href="<!--{$smarty.const.ROOT_URLPATH}-->js/jquery.colorbox/colorbox.css" type="text/css" media="all" />
<link rel="stylesheet" href="<!--{$TPL_URLPATH}-->css/import.css" type="text/css" media="all" />
<!--{if $tpl_page_class_name === "LC_Page_Products_Detail"}-->
    <link rel="stylesheet" href="<!--{$smarty.const.ROOT_URLPATH}-->js/jquery.ui/theme/jquery.ui.core.css" type="text/css" media="all" />
    <link rel="stylesheet" href="<!--{$smarty.const.ROOT_URLPATH}-->js/jquery.ui/theme/jquery.ui.tooltip.css" type="text/css" media="all" />
    <link rel="stylesheet" href="<!--{$smarty.const.ROOT_URLPATH}-->js/jquery.ui/theme/jquery.ui.theme.css" type="text/css" media="all" />
<!--{/if}-->
<link rel="alternate" type="application/rss+xml" title="RSS" href="<!--{$smarty.const.HTTP_URL}-->rss/<!--{$smarty.const.DIR_INDEX_PATH}-->" />
<!--[if lt IE 9]>
<script src="<!--{$smarty.const.ROOT_URLPATH}-->js/jquery-1.11.1.min.js"></script>
<![endif]-->
<!--[if gte IE 9]><!-->
<script src="<!--{$smarty.const.ROOT_URLPATH}-->js/jquery-2.1.1.min.js"></script>
<!--<![endif]-->
<script type="text/javascript" src="<!--{$smarty.const.ROOT_URLPATH}-->js/eccube.js"></script>
<!-- #2342 次期メジャーバージョン(2.14)にてeccube.legacy.jsは削除予定.モジュール、プラグインの互換性を考慮して2.13では残します. -->
<script type="text/javascript" src="<!--{$smarty.const.ROOT_URLPATH}-->js/eccube.legacy.js"></script>
<script type="text/javascript" src="<!--{$smarty.const.ROOT_URLPATH}-->js/jquery.colorbox/jquery.colorbox-min.js"></script>


<script type="text/javascript">//<![CDATA[
    <!--{$tpl_javascript}-->
    $(function(){
        <!--{$tpl_onload}-->
    });
//]]></script>
        <!--{ EC-Cube defalt archive: End *}-->



        <!--{* New Styles: Start *}-->
    <!--{* 編集のため一時利用: Start *}-->
<!--{ if false }--><!--{* using: true *}-->
<link rel="stylesheet" href="<!--{$TPL_URLPATH}-->css/import.css" type="text/css" media="all" />
<!--{if $tpl_page_class_name === "LC_Page_Products_Detail"}-->
    <link rel="stylesheet" href="<!--{$smarty.const.ROOT_URLPATH}-->js/jquery.ui/theme/jquery.ui.core.css" type="text/css" media="all" />
    <link rel="stylesheet" href="<!--{$smarty.const.ROOT_URLPATH}-->js/jquery.ui/theme/jquery.ui.tooltip.css" type="text/css" media="all" />
    <link rel="stylesheet" href="<!--{$smarty.const.ROOT_URLPATH}-->js/jquery.ui/theme/jquery.ui.theme.css" type="text/css" media="all" />
<!--{/if}-->
<!--{ /if }-->
    <!--{* 編集の為一時利用: End *}-->


<script src="<!--{$TPL_URLPATH}-->js/jquery_2.2.4.min.js"></script>

<script src="<!--{$TPL_URLPATH}-->js/lib/common.js"></script>
<script src="<!--{$TPL_URLPATH}-->js/lib/function.js"></script>
<script src="<!--{$TPL_URLPATH}-->js/lib/main.js"></script>
<script src="<!--{$TPL_URLPATH}-->js/vendor/svgxuse.js"></script>

<link rel="stylesheet" href="<!--{$TPL_URLPATH}-->css/vendor/fakeLoader.css">
<script src="<!--{$TPL_URLPATH}-->js/vendor/fakeLoader.js"></script>

<!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"> -->
<link rel="stylesheet" href="<!--{$TPL_URLPATH}-->css/fontawesome_5.0.13_all.css">

<!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Swiper/3.4.2/css/swiper.min.css"> -->
<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/Swiper/3.4.2/js/swiper.min.js"></script> -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.3.3/css/swiper.min.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.3.3/js/swiper.min.js"></script>


<!-- <link rel="stylesheet" href="css/vendor/megamenu.css"> -->
<script src="<!--{$TPL_URLPATH}-->js/vendor/megamenu.js"></script>

<!-- Product detail only? -->
<link rel="stylesheet" href="<!--{$TPL_URLPATH}-->css/vendor/drift-basic.css">
<script src="<!--{$TPL_URLPATH}-->js/vendor/Drift.js"></script>

<link rel="stylesheet" href="<!--{$TPL_URLPATH}-->css/lib/main.css">
        <!--{* New Styles: End *}-->

        <!--{**
            * Fix stylesheet
            * Without: index, product-list, product-detail
            *}-->
<!--{ if $arrPageLayout.page_id != 1
        && $arrPageLayout.page_id != 2
        && $arrPageLayout.page_id != 3 }-->
<link rel="stylesheet" href="<!--{$TPL_URLPATH}-->css/regular.css?201809031614
">
<!--{ /if }-->

        <!--{**
            * Read only the specified page.
            * >> Change without
            * - index.php
            * - products/list.php
            * - products/detail.php
            * >> Before: $arrPageLayout.page_id == 14
            *}-->
<!--{ if $arrPageLayout.page_id != 1
        && $arrPageLayout.page_id != 2
        && $arrPageLayout.page_id != 3}-->
<script type="text/javascript" src="<!--{$smarty.const.ROOT_URLPATH}-->js/eccube.js"></script>
<script type="text/javascript" src="<!--{$smarty.const.ROOT_URLPATH}-->js/eccube.legacy.js"></script>
<script type="text/javascript">//<![CDATA[
    <!--{$tpl_javascript}-->
    $(function(){
        <!--{$tpl_onload}-->
    });
//]]></script>
<!--{ /if }-->



<!--{strip}-->
    <!--{* ▼Head COLUMN*}-->
    <!--{if $arrPageLayout.HeadNavi|@count > 0}-->
        <!--{* ▼上ナビ *}-->
        <!--{foreach key=HeadNaviKey item=HeadNaviItem from=$arrPageLayout.HeadNavi}-->
            <!--{* ▼<!--{$HeadNaviItem.bloc_name}--> *}-->
            <!--{if $HeadNaviItem.php_path != ""}-->
                <!--{include_php file=$HeadNaviItem.php_path items=$HeadNaviItem}-->
            <!--{else}-->
                <!--{include file=$HeadNaviItem.tpl_path}-->
            <!--{/if}-->
            <!--{* ▲<!--{$HeadNaviItem.bloc_name}--> *}-->
        <!--{/foreach}-->
        <!--{* ▲上ナビ *}-->
    <!--{/if}-->
    <!--{* ▲Head COLUMN*}-->
<!--{/strip}-->
</head>

<!-- ▼BODY部 スタート -->
<!--{include file='./site_main.tpl'}-->
<!-- ▲BODY部 エンド -->

</html>
