<!--{*
 * This file is part of EC-CUBE
 *
 * Copyright(c) 2000-2014 LOCKON CO.,LTD. All Rights Reserved.
 *
 * http://www.lockon.co.jp/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *}-->

<!--▼FOOTER-->
<!--{strip}-->
<div class="common-footer">
	<div id="to-top" class="-triangle"></div>
	<div class="footer-nav nav">
		<div class="footercontent-outer contents left -border">
			<h4 class="heading">HELP &amp; GUIDE</h4>
			<div class="footercontent-inner inner left">
				<ul class="footer-menu menu">

					<li class="items">
						<a href="<!--{$smarty.const.USER_URL}-->terms.php">
						ご利用規約</a></li>

					<li class="items">
						<a href="javascript:void(0)" onclick="alert('Nothing page')">初めてのお客様</a></li>

					<li class="items">
						<a href="javascript:void(0)" onclick="alert('Nothing page')">
						ヘルプ</a></li>

<!--{ if false }-->
<!-- Display none: Start -->
					<li class="items">
						<a href="javascript:void(0)" onclick="alert('Nothing page')">
						注意事項</a></li>

					<li class="items">
						<a href="javascript:void(0)" onclick="alert('Nothing page')">INTERNATIONAL</a></li>

<!-- Display none: End -->
<!--{ /if }-->

				</ul>
				<ul class="footer-menu menu">

<!--{ if false }-->
<!-- Display none: Start -->
					<li class="items">
						<a href="javascript:void(0)" onclick="alert('Nothing page')">
						最新ニュース</a></li>


<!-- Display none: End -->
<!--{ /if }-->

					<li class="items">
						<a href="<!--{$smarty.const.HTTPS_URL}-->order/">
						特定商取引法に基づく表記</a></li>

					<li class="items">
						<a href="<!--{$smarty.const.HTTPS_URL}-->guide/privacy.php">
						個人情報保護方針</a></li>

					<li class="items">
						<a href="<!--{$smarty.const.HTTPS_URL}-->contact/">
						お問い合わせ</a></li>

				</ul>
			</div>
		</div>

		<div class="footercontent-outer contents center">
			<h4 class="heading">CATEGORY</h4>
			<div class="footercontent-inner inner center">
				<ul class="footer-menu menu">

					<li class="items">
						<a href="javascript:void(0)" onclick="alert('Nothing page')">
						SKIN CARE</a></li>

					<li class="items">
						<a href="javascript:void(0)" onclick="alert('Nothing page')">
						MAKE UP</a></li>

<!--{ if false }-->
<!-- Display none: Start -->
					<li class="items"><a href="#">FRAGRANCE</a></li>

					<li class="items"><a href="#">HAIR</a></li>

					<li class="items"><a href="#">BODY</a></li>

					<li class="items"><a href="#">TOOL &amp; ACCESSORIES</a></li>

					<li class="items"><a href="#">MEN&#39;S GROOMING</a></li>

<!-- Display none: End -->
<!--{ /if }-->

				</ul>
				<ul class="footer-menu menu">

					<li class="items">
						<a href="javascript:void(0)" onclick="alert('Nothing page')">
						SKIN CARE</a></li>

					<li class="items">
						<a href="javascript:void(0)" onclick="alert('Nothing page')">
						MAKE UP</a></li>

<!--{ if false }-->
<!-- Display none: Start -->
					<li class="items"><a href="#">FRAGRANCE</a></li>

					<li class="items"><a href="#">HAIR</a></li>

					<li class="items"><a href="#">BODY</a></li>

					<li class="items"><a href="#">TOOL &amp; ACCESSORIES</a></li>

					<li class="items"><a href="#">MEN&#39;S GROOMING</a></li>

<!-- Display none: End -->
<!--{ /if }-->

				</ul>
			</div>
		</div>

		<div class="footercontent-outer contents right">
			<div class="footercontent-inner inner right">
				<a class="footer-logo logo" href="<!--{$smarty.const.HTTPS_URL}-->">
					<img src="<!--{$TPL_URLPATH}-->img/svg/osv_logo.svg">
<!--{ if false }-->
					<svg viewBox="0 0 182.78 38.84" class="use-logo svg">
	<use xlink:href="<!--{$TPL_URLPATH}-->img/svg/sprite.svg#use_logo"></use>
	<use class="shel" xlink:href="<!--{$TPL_URLPATH}-->img/svg/sprite.svg#shel"></use>
	<use class="mag" xlink:href="<!--{$TPL_URLPATH}-->img/svg/sprite.svg#mag"></use>
					</svg>
<!--{ /if }-->
				</a>

<!--{ if false }-->
<!-- Display none: Start -->
				<ul class="sns-list sns">

					<li class="sns-item items"><a href="" target="_blank"><svg viewBox="0 0 100 100" class="svg use-instagram"><use xlink:href="<!--{$TPL_URLPATH}-->img/svg/sprite.svg#use_instagram"></use></svg></a></li>

					<li class="sns-item items"><a href="" target="_blank"><svg viewBox="0 0 100 100" class="svg use-twitter"><use xlink:href="<!--{$TPL_URLPATH}-->img/svg/sprite.svg#use_twitter"></use></svg></a></li>

					<li class="sns-item items"><a href="" target="_blank"><svg viewBox="0 0 100 100" class="svg use-facebook"><use xlink:href="<!--{$TPL_URLPATH}-->img/svg/sprite.svg#use_facebook"></use></svg></a></li>

				</ul>
<!-- Display none: End -->
<!--{ /if }-->

			</div>
		</div>
	</div>

	<footer>
		<small>© <!--{$smarty.now|date_format:'%Y'}--> YOU inc.</small>
	</footer>
</div>


<!--{* }-->
<div id="overlay" class="overlay"></div>
	<div class="common-nav outer -sp">
	<nav class="nav-container nav">

<!--{ if false }-->
<!-- Display none: Start -->
		<div class="member-status status">
			<a id="status" class="status items" href="<!--{$smarty.const.HTTPS_URL}-->mypage/login.php"></a>
			<a class="member items" href="<!--{$smarty.const.HTTPS_URL}-->entry/kiyaku.php">新規会員登録</a>
		</div>
<!-- Display none: End -->
<!--{ /if }-->

		<ul class="nav-list list">

			<li class="list-item items"><a class="anchor" href="/">HOME</a></li>

			<li class="list-item items"><a class="anchor" href="javascript:void(0)">CATEGORY</a>

				<ul class="nav-list">

					<li class="list-item items"><a class="anchor" href="tops.html">TOPS</a></li>

					<li class="list-item items"><a class="anchor" href="bottoms.html">BOTTOMS</a></li>

					<li class="list-item items"><a class="anchor" href="acc.html">ACCESSORIES</a></li>

				</ul>

			</li>

			<li class="list-item items"><a class="anchor" href="javascript:void(0)">BRAND</a>
				<ul class="nav-list">

					<li class="list-item items"><a class="anchor" href="mou.html">MOUSSY</a></li>

					<li class="list-item items"><a class="anchor" href="sly.html">SLY</a></li>

					<li class="list-item items"><a class="anchor" href="rcs.html">RCS</a></li>

					<li class="list-item items"><a class="anchor" href="azl.html">AZUL by moussy</a></li>

				</ul>
			</li>


			<li class="list-item items"><a class="anchor" href="rank.html">RANKING</a></li>

			<li class="list-item items"><a class="anchor" href="topics.html">TOPICS</a></li>

			<li class="list-item items"><a class="anchor" href="feat.html">FEATURE</a></li>

		</ul>
	</nav>
</div>
<!--{ *}-->


<!--{/strip}-->
<!--▲FOOTER-->
