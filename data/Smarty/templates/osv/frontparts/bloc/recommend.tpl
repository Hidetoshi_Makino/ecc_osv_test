<!--{*
 * This file is part of EC-CUBE
 *
 * Copyright(c) 2000-2014 LOCKON CO.,LTD. All Rights Reserved.
 *
 * http://www.lockon.co.jp/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *}-->

<!--{strip}-->
<!--{ if count($arrBestProducts) > 0 }-->
<div class="item-section section -lastchild">
    <h3 class="heading">RECOMMEND</h3>
    <ul class="item-block -col5">

        <!--{ foreach from=$arrBestProducts item=arrProduct name="recommend_products" }-->
            <!--{* $arrProduct|@var_dump *}-->
        <li class="items -recommend -panel">
            <div class="items-inner inner -center">
                <a href="<!--{$smarty.const.P_DETAIL_URLPATH}--><!--{$arrProduct.product_id|u}-->">
                    <div class="image-outer outer">
                        <img src="<!--{$smarty.const.IMAGE_SAVE_URLPATH}--><!--{$arrProduct.main_list_image|sfNoImageMainList|h}-->" alt="<!--{$arrProduct.name|h}-->">
                    </div>
                    <div class="credit-outer">
                        <!--{* <span class="credit brand">BRAND</span> *}-->
                        <span class="credit name"><!--{$arrProduct.name|h}--></span>
                        <!--{* <span class="credit category">MAKE UP</span> *}-->
<!--{ if $arrProduct.price01_min == $arrProduct.price02_min
        && $arrProduct.price01_max == $arrProduct.price02_max }-->
                        <div class="price-outer credit -list -default">
                            <span class="price">¥<!--{$arrProduct.price02_min_inctax|n2s}--></span>
                        </div>
<!--{ else }-->
    <!--{php}-->
/**
 * 値引率を計算
 * 本来ならば class_extends に書くべき
 */
//var_dump($this->_tpl_vars['arrProduct']);
$price01 = $this->_tpl_vars['arrProduct']['price01_min'];
$price02 = $this->_tpl_vars['arrProduct']['price02_min'];
if ($price01 != $price02) {
    $rate = floor(100 - $price02 * 100 / $price01);
    //var_dump($rate);
    $this->_tpl_vars['arrProduct']['discount_rate'] = $rate;
}
    <!--{/php}-->

                        <div class="price-outer credit -list -sale">
                            <span class="price">¥<!--{$arrProduct.price02_min_inctax|n2s}--></span>
                            <span class="rate">(<!--{$arrProduct.discount_rate}-->% OFF)</span>
                        </div>
                        <span class="credit status -sale"></span>
<!--{ /if }-->

    <!--{* Item status: Start *}-->
<!--{ if count($productStatus[$id]) > 0 }-->
    <!--{foreach from=$productStatus[$id] item=status}-->
        <!--{ if $status == 1 }-->
                    <span class="credit status -new"></span>
        <!--{ elseif $status == 2 }-->
                    <span>残りわずか</span>
        <!--{ elseif $status == 3 }-->
                    <span>ポイント２倍</span>
        <!--{ elseif $status == 4 }-->
                    <span>オススメ</span>
        <!--{ elseif $status == 5 }-->
                    <span>限定品</span>
        <!--{ else }-->
                    <span>Other</span>
        <!--{ /if }-->
    <!--{ /foreach }-->
<!--{ /if }-->
    <!--{* Item status: End *}-->

<!--{ if $arrProduct.stock_max == 0 && $arrProduct.stock_unlimited_max == 0}-->
                        <span class="credit status -sold"></span>
<!--{ /if }-->
                    </div>
                </a>

<!--{ if false }-->
<!-- Display none: Start -->
                <div class="color-outer -hide">
                    <ul class="color-list list">
                        <li class="tip-outer colors"><img class="tip" src="http://placehold.jp/ff6464/ff6464/30x30.png"><br><span class="num">9409</span></li>
                        <li class="tip-outer colors"><img class="tip" src="http://placehold.jp/46e2a1/46e2a1/30x30.png"><br><span class="num">9414</span></li>
                        <li class="tip-outer colors"><img class="tip" src="http://placehold.jp/ce1818/ce1818/30x30.png"><br><span class="num">9413</span></li>
                    </ul>
                </div>
<!-- Display none: End -->
<!--{ /if }-->

            </div>
        </li>
        <!--{ /foreach }-->

    </ul>

<!--{ if false }-->
<!-- Display none: Start -->
        <p class="view-all"><a class="anchor" href="">VIEW ALL</a></p>
<!-- Display none: End -->
<!--{ /if }-->

</div>
<!--{ /if }-->

<!--{/strip}-->
