<!--{*
 * This file is part of EC-CUBE
 *
 * Copyright(c) 2000-2014 LOCKON CO.,LTD. All Rights Reserved.
 *
 * http://www.lockon.co.jp/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *}-->

<!--{ foreach from=$children item=child }-->
<div class="item-category category">
    <input id="<!--{ $child.category_name|h }-->" class="accordion-tab tab" type="checkbox" name="tabs">
    <label for="<!--{ $child.category_name|h }-->" class="title"><!--{ $child.category_name|h }--></label>
    <ul class="category-child child">
        <!--{ if isset($child.children|smarty:nodefaults) }-->
            <!--{ foreach from=$child.children item=child_item }-->
        <li class="children-list children">
            <a class="link" href="<!--{ $smarty.const.ROOT_URLPATH }-->products/list.php?category_id=<!--{ $child_item.category_id }-->">
            <!--{ $child_item.category_name|h }--></a></li>
            <!--{ /foreach }-->
        <!--{ /if }-->

        <li class="children-list children">
            <a class="link" href="<!--{$smarty.const.ROOT_URLPATH}-->products/list.php?category_id=<!--{$child.category_id}-->">
            ALL</a></li>
    </ul>
</div>
<!--{/foreach}-->
