<!--{*
 * This file is part of EC-CUBE
 *
 * Copyright(c) 2000-2014 LOCKON CO.,LTD. All Rights Reserved.
 *
 * http://www.lockon.co.jp/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *}-->



<div class="pagetitle">
    <h2 class="title">CONTACT</h2>
    <span>お問い合わせ</span>
</div>

<div class="detail">

    <p class="message">お問い合わせ内容の送信が完了いたしました。</p>
    <p>
        万一、ご回答メールが届かない場合は、トラブルの可能性もありますので<br />大変お手数ではございますがもう一度お問い合わせいただくか、お電話にてお問い合わせください。<br />
        今後ともご愛顧賜りますようよろしくお願い申し上げます。
    </p>
    <div class="shop_information">
        <p class="name">ViVi SHOP運営事務局</p>
    </div>

    <div class="btn_area">
        <div class="btn_submit" onclick="javascript:location.href='<!--{$smarty.const.TOP_URL}-->'">トップページへ</div>
    </div>

</div>
