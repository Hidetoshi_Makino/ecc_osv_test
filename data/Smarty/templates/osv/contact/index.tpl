<!--{*
 * This file is part of EC-CUBE
 *
 * Copyright(c) 2000-2014 LOCKON CO.,LTD. All Rights Reserved.
 *
 * http://www.lockon.co.jp/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *}-->



<div class="pagetitle">
    <h2 class="title">CONTACT</h2>
    <span>お問い合わせ</span>
</div>

<div class="detail">

    <!--{* Test: Start *}-->
    <!--{ if false }-->
    <div class="input-group">
        <input type="text" name="" class="input-group__control" placeholder="test01">
    </div>
    <input type="text" name="" class="input-group__control" placeholder="test02">
    <!--{ /if }-->
    <!--{* Test: End *}-->


    <p class="topTxt">内容によっては回答をさしあげるのにお時間をいただくこともございます。<br />
    また、休業日は翌営業日以降の対応となりますのでご了承ください。</p>

    <form name="form1" id="form1" method="post" action="?">
        <input type="hidden" name="<!--{$smarty.const.TRANSACTION_ID_NAME}-->" value="<!--{$transactionid}-->" />
        <input type="hidden" name="mode" value="confirm" />


        <dl>
            <dt>お名前<span class="attention">※</span></dt>
            <dd>
                <span class="attention"><!--{$arrErr.name01}--><!--{$arrErr.name02}--></span>
                <input type="text" class="input-group__control" name="name01" value="<!--{$arrForm.name01.value|default:$arrData.name01|h}-->" maxlength="<!--{$smarty.const.STEXT_LEN}-->" style="<!--{$arrErr.name01|sfGetErrorColor}-->; ime-mode: active;" placeholder="姓" />　
                <input type="text" class="input-group__control" name="name02" value="<!--{$arrForm.name02.value|default:$arrData.name02|h}-->" maxlength="<!--{$smarty.const.STEXT_LEN}-->" style="<!--{$arrErr.name02|sfGetErrorColor}-->; ime-mode: active;" placeholder="名" />
            </dd>
        </dl>

        <dl>
            <dt>お名前(フリガナ)<span class="attention">※</span></dt>
            <dd>
                <span class="attention"><!--{$arrErr.kana01}--><!--{$arrErr.kana02}--></span>
                <input type="text" class="input-group__control" name="kana01" value="<!--{$arrForm.kana01.value|default:$arrData.kana01|h}-->" maxlength="<!--{$smarty.const.STEXT_LEN}-->" style="<!--{$arrErr.kana01|sfGetErrorColor}-->; ime-mode: active;" placeholder="セイ" />　
                <input type="text" class="input-group__control" name="kana02" value="<!--{$arrForm.kana02.value|default:$arrData.kana02|h}-->" maxlength="<!--{$smarty.const.STEXT_LEN}-->" style="<!--{$arrErr.kana02|sfGetErrorColor}-->; ime-mode: active;" placeholder="メイ" />
            </dd>
        </dl>
        <dl>
            <dt>郵便番号</dt>
            <dd>
                <span class="attention"><!--{$arrErr.zip01}--><!--{$arrErr.zip02}--></span>
                <input type="text" name="zip01" class="input-group__control -zip" value="<!--{$arrForm.zip01.value|default:$arrData.zip01|h}-->" maxlength="<!--{$smarty.const.ZIP01_LEN}-->" style="<!--{$arrErr.zip01|sfGetErrorColor}-->; ime-mode: disabled;" />
                <i class="split-bar"></i>
                <input type="text" name="zip02" class="input-group__control -zip" value="<!--{$arrForm.zip02.value|default:$arrData.zip02|h}-->" maxlength="<!--{$smarty.const.ZIP02_LEN}-->" style="<!--{$arrErr.zip02|sfGetErrorColor}-->; ime-mode: disabled;" />
            </dd>
        </dl>
        <dl>
            <dt>住所</dt>
            <dd>
                <span class="attention"><!--{$arrErr.pref}--><!--{$arrErr.addr01}--><!--{$arrErr.addr02}--></span>

                <select name="pref" class="input-group__control" style="<!--{$arrErr.pref|sfGetErrorColor}-->">
                    <option value="">都道府県を選択</option><!--{html_options options=$arrPref selected=$arrForm.pref.value|default:$arrData.pref|h}-->
                </select>
                <br /><br />
                <input type="text" class="input-group__control -address" name="addr01" value="<!--{$arrForm.addr01.value|default:$arrData.addr01|h}-->" style="<!--{$arrErr.addr01|sfGetErrorColor}-->; ime-mode: active;" placeholder="<!--{$smarty.const.SAMPLE_ADDRESS1}-->" />
                <input type="text" class="input-group__control -address" name="addr02" value="<!--{$arrForm.addr02.value|default:$arrData.addr02|h}-->" style="<!--{$arrErr.addr02|sfGetErrorColor}-->; ime-mode: active;" placeholder="<!--{$smarty.const.SAMPLE_ADDRESS2}-->" />
                <span class="attention">住所は2つに分けてご記入ください。マンション名は必ず記入してください。
                </span>
            </dd>
        </dl>
        <dl>
            <dt>電話番号</dt>
            <dd>
                <span class="attention"><!--{$arrErr.tel01}--><!--{$arrErr.tel02}--><!--{$arrErr.tel03}--></span>
                <input type="text" class="input-group__control -phone_num" name="tel01" value="<!--{$arrForm.tel01.value|default:$arrData.tel01|h}-->" maxlength="<!--{$smarty.const.TEL_ITEM_LEN}-->" style="<!--{$arrErr.tel01|sfGetErrorColor}-->; ime-mode: disabled;" />
                <i class="split-bar"></i>
                <input type="text" class="input-group__control -phone_num" name="tel02" value="<!--{$arrForm.tel02.value|default:$arrData.tel02|h}-->" maxlength="<!--{$smarty.const.TEL_ITEM_LEN}-->" style="<!--{$arrErr.tel02|sfGetErrorColor}-->; ime-mode: disabled;" />
                <i class="split-bar"></i>
                <input type="text" class="input-group__control -phone_num" name="tel03" value="<!--{$arrForm.tel03.value|default:$arrData.tel03|h}-->" maxlength="<!--{$smarty.const.TEL_ITEM_LEN}-->" style="<!--{$arrErr.tel03|sfGetErrorColor}-->; ime-mode: disabled;" />
            </dd>
        </dl>
        <dl>
            <dt>メールアドレス<span class="attention">※</span></dt>
            <dd>
                <span class="attention"><!--{$arrErr.email}--><!--{$arrErr.email02}--></span>
                <input type="text" class="input-group__control -email" name="email" value="<!--{$arrForm.email.value|default:$arrData.email|h}-->" style="<!--{$arrErr.email|sfGetErrorColor}-->; ime-mode: disabled;" />
                <!--{* ログインしていれば入力済みにする }-->
                <!--{if $smarty.session.customer}-->
                <!--{assign var=email02 value=$arrData.email}-->
                <!--{/if*}-->
                <input type="text" class="input-group__control -email" name="email02" value="<!--{$arrForm.email02.value|default:$email02|h}-->" style="<!--{$arrErr.email02|sfGetErrorColor}-->; ime-mode: disabled;" />
                <span class="attention">確認のため2度入力してください。</span>
            </dd>
        </dl>
        <dl>
            <dt>お問い合わせ内容<span class="attention">※</span><br />
                <span class="mini">（全角<!--{$smarty.const.MLTEXT_LEN}-->字以下）</span>
            </dt>
            <dd>
                <span class="attention"><!--{$arrErr.contents}--></span>
                <textarea name="contents" class="input-group__control" cols="60" rows="20" style="<!--{$arrErr.contents.value|h|sfGetErrorColor}-->; ime-mode: active;"><!--{"\n"}--><!--{$arrForm.contents.value|h}--></textarea>
                <br />
                <span class="attention">※ご注文に関するお問い合わせには、必ず「ご注文番号」をご記入くださいますようお願いいたします。
                </span>
            </dd>
        </dl>

<!--{*
        <div class="btn_area">
            <ul>
                <li>
                    <input type="submit" value="確認ページへ" name="confirm" />
                </li>
            </ul>
        </div>
*}-->

        <div class="btn_area">
            <div class="btn_submit" onclick="javascript:void(document.form1.submit())">確認ページへ</div>
        </div>
    </form>
</div>
