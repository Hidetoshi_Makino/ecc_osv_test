"use strict";

var _typeof3 = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

(function (f) {
  if ((typeof exports === "undefined" ? "undefined" : _typeof3(exports)) === "object" && typeof module !== "undefined") {
    module.exports = f();
  } else if (typeof define === "function" && define.amd) {
    define([], f);
  } else {
    var g;if (typeof window !== "undefined") {
      g = window;
    } else if (typeof global !== "undefined") {
      g = global;
    } else if (typeof self !== "undefined") {
      g = self;
    } else {
      g = this;
    }g.Drift = f();
  }
})(function () {
  var define, module, exports;return function e(t, n, r) {
    function s(o, u) {
      if (!n[o]) {
        if (!t[o]) {
          var a = typeof require == "function" && require;if (!u && a) return a(o, !0);if (i) return i(o, !0);var f = new Error("Cannot find module '" + o + "'");throw f.code = "MODULE_NOT_FOUND", f;
        }var l = n[o] = { exports: {} };t[o][0].call(l.exports, function (e) {
          var n = t[o][1][e];return s(n ? n : e);
        }, l, l.exports, e, t, n, r);
      }return n[o].exports;
    }var i = typeof require == "function" && require;for (var o = 0; o < r.length; o++) {
      s(r[o]);
    }return s;
  }({ 1: [function (require, module, exports) {
      'use strict';

      Object.defineProperty(exports, "__esModule", {
        value: true
      });

      var _createClass = function () {
        function defineProperties(target, props) {
          for (var i = 0; i < props.length; i++) {
            var descriptor = props[i];descriptor.enumerable = descriptor.enumerable || false;descriptor.configurable = true;if ("value" in descriptor) descriptor.writable = true;Object.defineProperty(target, descriptor.key, descriptor);
          }
        }return function (Constructor, protoProps, staticProps) {
          if (protoProps) defineProperties(Constructor.prototype, protoProps);if (staticProps) defineProperties(Constructor, staticProps);return Constructor;
        };
      }();

      var _throwIfMissing = require('./util/throwIfMissing');

      var _throwIfMissing2 = _interopRequireDefault(_throwIfMissing);

      var _dom = require('./util/dom');

      function _interopRequireDefault(obj) {
        return obj && obj.__esModule ? obj : { default: obj };
      }

      function _classCallCheck(instance, Constructor) {
        if (!(instance instanceof Constructor)) {
          throw new TypeError("Cannot call a class as a function");
        }
      }

      var BoundingBox = function () {
        function BoundingBox(options) {
          _classCallCheck(this, BoundingBox);

          this.isShowing = false;

          var _options$namespace = options.namespace,
              namespace = _options$namespace === undefined ? null : _options$namespace,
              _options$zoomFactor = options.zoomFactor,
              zoomFactor = _options$zoomFactor === undefined ? (0, _throwIfMissing2.default)() : _options$zoomFactor,
              _options$containerEl = options.containerEl,
              containerEl = _options$containerEl === undefined ? (0, _throwIfMissing2.default)() : _options$containerEl;

          this.settings = { namespace: namespace, zoomFactor: zoomFactor, containerEl: containerEl };

          this.openClasses = this._buildClasses('open');

          this._buildElement();
        }

        _createClass(BoundingBox, [{
          key: '_buildClasses',
          value: function _buildClasses(suffix) {
            var classes = ['drift-' + suffix];

            var ns = this.settings.namespace;
            if (ns) {
              classes.push(ns + '-' + suffix);
            }

            return classes;
          }
        }, {
          key: '_buildElement',
          value: function _buildElement() {
            this.el = document.createElement('div');
            (0, _dom.addClasses)(this.el, this._buildClasses('bounding-box'));
          }
        }, {
          key: 'show',
          value: function show(zoomPaneWidth, zoomPaneHeight) {
            this.isShowing = true;

            this.settings.containerEl.appendChild(this.el);

            var style = this.el.style;
            style.width = Math.round(zoomPaneWidth / this.settings.zoomFactor) + 'px';
            style.height = Math.round(zoomPaneHeight / this.settings.zoomFactor) + 'px';

            (0, _dom.addClasses)(this.el, this.openClasses);
          }
        }, {
          key: 'hide',
          value: function hide() {
            if (this.isShowing) {
              this.settings.containerEl.removeChild(this.el);
            }

            this.isShowing = false;

            (0, _dom.removeClasses)(this.el, this.openClasses);
          }
        }, {
          key: 'setPosition',
          value: function setPosition(percentageOffsetX, percentageOffsetY, triggerRect) {
            var pageXOffset = window.pageXOffset;
            var pageYOffset = window.pageYOffset;

            var inlineLeft = triggerRect.left + percentageOffsetX * triggerRect.width - this.el.clientWidth / 2 + pageXOffset;
            var inlineTop = triggerRect.top + percentageOffsetY * triggerRect.height - this.el.clientHeight / 2 + pageYOffset;

            var elRect = this.el.getBoundingClientRect();

            if (inlineLeft < triggerRect.left + pageXOffset) {
              inlineLeft = triggerRect.left + pageXOffset;
            } else if (inlineLeft + this.el.clientWidth > triggerRect.left + triggerRect.width + pageXOffset) {
              inlineLeft = triggerRect.left + triggerRect.width - this.el.clientWidth + pageXOffset;
            }

            if (inlineTop < triggerRect.top + pageYOffset) {
              inlineTop = triggerRect.top + pageYOffset;
            } else if (inlineTop + this.el.clientHeight > triggerRect.top + triggerRect.height + pageYOffset) {
              inlineTop = triggerRect.top + triggerRect.height - this.el.clientHeight + pageYOffset;
            }

            this.el.style.left = inlineLeft + 'px';
            this.el.style.top = inlineTop + 'px';
          }
        }]);

        return BoundingBox;
      }();

      exports.default = BoundingBox;
    }, { "./util/dom": 6, "./util/throwIfMissing": 7 }], 2: [function (require, module, exports) {
      'use strict';

      var _createClass = function () {
        function defineProperties(target, props) {
          for (var i = 0; i < props.length; i++) {
            var descriptor = props[i];descriptor.enumerable = descriptor.enumerable || false;descriptor.configurable = true;if ("value" in descriptor) descriptor.writable = true;Object.defineProperty(target, descriptor.key, descriptor);
          }
        }return function (Constructor, protoProps, staticProps) {
          if (protoProps) defineProperties(Constructor.prototype, protoProps);if (staticProps) defineProperties(Constructor, staticProps);return Constructor;
        };
      }();

      var _dom = require('./util/dom');

      var _injectBaseStylesheet = require('./injectBaseStylesheet');

      var _injectBaseStylesheet2 = _interopRequireDefault(_injectBaseStylesheet);

      var _Trigger = require('./Trigger');

      var _Trigger2 = _interopRequireDefault(_Trigger);

      var _ZoomPane = require('./ZoomPane');

      var _ZoomPane2 = _interopRequireDefault(_ZoomPane);

      function _interopRequireDefault(obj) {
        return obj && obj.__esModule ? obj : { default: obj };
      }

      function _classCallCheck(instance, Constructor) {
        if (!(instance instanceof Constructor)) {
          throw new TypeError("Cannot call a class as a function");
        }
      }

      module.exports = function () {
        function Drift(triggerEl) {
          var _this = this;

          var options = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};

          _classCallCheck(this, Drift);

          this.VERSION = '1.2.0';

          this.destroy = function () {
            _this.trigger._unbindEvents();
          };

          this.triggerEl = triggerEl;

          if (!(0, _dom.isDOMElement)(this.triggerEl)) {
            throw new TypeError('`new Drift` requires a DOM element as its first argument.');
          }

          // A bit unexpected if you haven't seen this pattern before.
          // Based on the pattern here:
          // https://github.com/getify/You-Dont-Know-JS/blob/master/es6%20&%20beyond/ch2.md#nested-defaults-destructured-and-restructured
          var _options$namespace = options.namespace,
              namespace = _options$namespace === undefined ? null : _options$namespace,
              _options$showWhitespa = options.showWhitespaceAtEdges,
              showWhitespaceAtEdges = _options$showWhitespa === undefined ? false : _options$showWhitespa,
              _options$containInlin = options.containInline,
              containInline = _options$containInlin === undefined ? false : _options$containInlin,
              _options$inlineOffset = options.inlineOffsetX,
              inlineOffsetX = _options$inlineOffset === undefined ? 0 : _options$inlineOffset,
              _options$inlineOffset2 = options.inlineOffsetY,
              inlineOffsetY = _options$inlineOffset2 === undefined ? 0 : _options$inlineOffset2,
              _options$inlineContai = options.inlineContainer,
              inlineContainer = _options$inlineContai === undefined ? document.body : _options$inlineContai,
              _options$sourceAttrib = options.sourceAttribute,
              sourceAttribute = _options$sourceAttrib === undefined ? 'data-zoom' : _options$sourceAttrib,
              _options$zoomFactor = options.zoomFactor,
              zoomFactor = _options$zoomFactor === undefined ? 3 : _options$zoomFactor,
              _options$paneContaine = options.paneContainer,
              paneContainer = _options$paneContaine === undefined ? document.body : _options$paneContaine,
              _options$inlinePane = options.inlinePane,
              inlinePane = _options$inlinePane === undefined ? 375 : _options$inlinePane,
              _options$handleTouch = options.handleTouch,
              handleTouch = _options$handleTouch === undefined ? true : _options$handleTouch,
              _options$onShow = options.onShow,
              onShow = _options$onShow === undefined ? null : _options$onShow,
              _options$onHide = options.onHide,
              onHide = _options$onHide === undefined ? null : _options$onHide,
              _options$injectBaseSt = options.injectBaseStyles,
              injectBaseStyles = _options$injectBaseSt === undefined ? true : _options$injectBaseSt,
              _options$hoverDelay = options.hoverDelay,
              hoverDelay = _options$hoverDelay === undefined ? 0 : _options$hoverDelay,
              _options$touchDelay = options.touchDelay,
              touchDelay = _options$touchDelay === undefined ? 0 : _options$touchDelay,
              _options$hoverBoundin = options.hoverBoundingBox,
              hoverBoundingBox = _options$hoverBoundin === undefined ? false : _options$hoverBoundin,
              _options$touchBoundin = options.touchBoundingBox,
              touchBoundingBox = _options$touchBoundin === undefined ? false : _options$touchBoundin;

          if (inlinePane !== true && !(0, _dom.isDOMElement)(paneContainer)) {
            throw new TypeError('`paneContainer` must be a DOM element when `inlinePane !== true`');
          }
          if (!(0, _dom.isDOMElement)(inlineContainer)) {
            throw new TypeError('`inlineContainer` must be a DOM element');
          }

          this.settings = { namespace: namespace, showWhitespaceAtEdges: showWhitespaceAtEdges, containInline: containInline, inlineOffsetX: inlineOffsetX, inlineOffsetY: inlineOffsetY, inlineContainer: inlineContainer, sourceAttribute: sourceAttribute, zoomFactor: zoomFactor, paneContainer: paneContainer, inlinePane: inlinePane, handleTouch: handleTouch, onShow: onShow, onHide: onHide, injectBaseStyles: injectBaseStyles, hoverDelay: hoverDelay, touchDelay: touchDelay, hoverBoundingBox: hoverBoundingBox, touchBoundingBox: touchBoundingBox };

          if (this.settings.injectBaseStyles) {
            (0, _injectBaseStylesheet2.default)();
          }

          this._buildZoomPane();
          this._buildTrigger();
        }

        _createClass(Drift, [{
          key: '_buildZoomPane',
          value: function _buildZoomPane() {
            this.zoomPane = new _ZoomPane2.default({
              container: this.settings.paneContainer,
              zoomFactor: this.settings.zoomFactor,
              showWhitespaceAtEdges: this.settings.showWhitespaceAtEdges,
              containInline: this.settings.containInline,
              inline: this.settings.inlinePane,
              namespace: this.settings.namespace,
              inlineOffsetX: this.settings.inlineOffsetX,
              inlineOffsetY: this.settings.inlineOffsetY,
              inlineContainer: this.settings.inlineContainer
            });
          }
        }, {
          key: '_buildTrigger',
          value: function _buildTrigger() {
            this.trigger = new _Trigger2.default({
              el: this.triggerEl,
              zoomPane: this.zoomPane,
              handleTouch: this.settings.handleTouch,
              onShow: this.settings.onShow,
              onHide: this.settings.onHide,
              sourceAttribute: this.settings.sourceAttribute,
              hoverDelay: this.settings.hoverDelay,
              touchDelay: this.settings.touchDelay,
              hoverBoundingBox: this.settings.hoverBoundingBox,
              touchBoundingBox: this.settings.touchBoundingBox,
              namespace: this.settings.namespace,
              zoomFactor: this.settings.zoomFactor
            });
          }
        }, {
          key: 'setZoomImageURL',
          value: function setZoomImageURL(imageURL) {
            this.zoomPane._setImageURL(imageURL);
          }
        }, {
          key: 'disable',
          value: function disable() {
            this.trigger.enabled = false;
          }
        }, {
          key: 'enable',
          value: function enable() {
            this.trigger.enabled = true;
          }
        }, {
          key: 'isShowing',
          get: function get() {
            return this.zoomPane.isShowing;
          }
        }, {
          key: 'zoomFactor',
          get: function get() {
            return this.settings.zoomFactor;
          },
          set: function set(zf) {
            this.settings.zoomFactor = zf;
            this.zoomPane.settings.zoomFactor = zf;
          }
        }]);

        return Drift;
      }();
    }, { "./Trigger": 3, "./ZoomPane": 4, "./injectBaseStylesheet": 5, "./util/dom": 6 }], 3: [function (require, module, exports) {
      'use strict';

      Object.defineProperty(exports, "__esModule", {
        value: true
      });

      var _createClass = function () {
        function defineProperties(target, props) {
          for (var i = 0; i < props.length; i++) {
            var descriptor = props[i];descriptor.enumerable = descriptor.enumerable || false;descriptor.configurable = true;if ("value" in descriptor) descriptor.writable = true;Object.defineProperty(target, descriptor.key, descriptor);
          }
        }return function (Constructor, protoProps, staticProps) {
          if (protoProps) defineProperties(Constructor.prototype, protoProps);if (staticProps) defineProperties(Constructor, staticProps);return Constructor;
        };
      }();

      var _throwIfMissing = require('./util/throwIfMissing');

      var _throwIfMissing2 = _interopRequireDefault(_throwIfMissing);

      var _BoundingBox = require('./BoundingBox');

      var _BoundingBox2 = _interopRequireDefault(_BoundingBox);

      function _interopRequireDefault(obj) {
        return obj && obj.__esModule ? obj : { default: obj };
      }

      function _classCallCheck(instance, Constructor) {
        if (!(instance instanceof Constructor)) {
          throw new TypeError("Cannot call a class as a function");
        }
      }

      var Trigger = function () {
        function Trigger() {
          var options = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};

          _classCallCheck(this, Trigger);

          _initialiseProps.call(this);

          var _options$el = options.el,
              el = _options$el === undefined ? (0, _throwIfMissing2.default)() : _options$el,
              _options$zoomPane = options.zoomPane,
              zoomPane = _options$zoomPane === undefined ? (0, _throwIfMissing2.default)() : _options$zoomPane,
              _options$sourceAttrib = options.sourceAttribute,
              sourceAttribute = _options$sourceAttrib === undefined ? (0, _throwIfMissing2.default)() : _options$sourceAttrib,
              _options$handleTouch = options.handleTouch,
              handleTouch = _options$handleTouch === undefined ? (0, _throwIfMissing2.default)() : _options$handleTouch,
              _options$onShow = options.onShow,
              onShow = _options$onShow === undefined ? null : _options$onShow,
              _options$onHide = options.onHide,
              onHide = _options$onHide === undefined ? null : _options$onHide,
              _options$hoverDelay = options.hoverDelay,
              hoverDelay = _options$hoverDelay === undefined ? 0 : _options$hoverDelay,
              _options$touchDelay = options.touchDelay,
              touchDelay = _options$touchDelay === undefined ? 0 : _options$touchDelay,
              _options$hoverBoundin = options.hoverBoundingBox,
              hoverBoundingBox = _options$hoverBoundin === undefined ? (0, _throwIfMissing2.default)() : _options$hoverBoundin,
              _options$touchBoundin = options.touchBoundingBox,
              touchBoundingBox = _options$touchBoundin === undefined ? (0, _throwIfMissing2.default)() : _options$touchBoundin,
              _options$namespace = options.namespace,
              namespace = _options$namespace === undefined ? null : _options$namespace,
              _options$zoomFactor = options.zoomFactor,
              zoomFactor = _options$zoomFactor === undefined ? (0, _throwIfMissing2.default)() : _options$zoomFactor;

          this.settings = { el: el, zoomPane: zoomPane, sourceAttribute: sourceAttribute, handleTouch: handleTouch, onShow: onShow, onHide: onHide, hoverDelay: hoverDelay, touchDelay: touchDelay, hoverBoundingBox: hoverBoundingBox, touchBoundingBox: touchBoundingBox, namespace: namespace, zoomFactor: zoomFactor };

          if (this.settings.hoverBoundingBox || this.settings.touchBoundingBox) {
            this.boundingBox = new _BoundingBox2.default({
              namespace: this.settings.namespace,
              zoomFactor: this.settings.zoomFactor,
              containerEl: this.settings.el.offsetParent
            });
          }

          this.enabled = true;

          this._bindEvents();
        }

        _createClass(Trigger, [{
          key: '_bindEvents',
          value: function _bindEvents() {
            this.settings.el.addEventListener('mouseenter', this._handleEntry, false);
            this.settings.el.addEventListener('mouseleave', this._hide, false);
            this.settings.el.addEventListener('mousemove', this._handleMovement, false);

            if (this.settings.handleTouch) {
              this.settings.el.addEventListener('touchstart', this._handleEntry, false);
              this.settings.el.addEventListener('touchend', this._hide, false);
              this.settings.el.addEventListener('touchmove', this._handleMovement, false);
            }
          }
        }, {
          key: '_unbindEvents',
          value: function _unbindEvents() {
            this.settings.el.removeEventListener('mouseenter', this._handleEntry, false);
            this.settings.el.removeEventListener('mouseleave', this._hide, false);
            this.settings.el.removeEventListener('mousemove', this._handleMovement, false);

            if (this.settings.handleTouch) {
              this.settings.el.removeEventListener('touchstart', this._handleEntry, false);
              this.settings.el.removeEventListener('touchend', this._hide, false);
              this.settings.el.removeEventListener('touchmove', this._handleMovement, false);
            }
          }
        }, {
          key: 'isShowing',
          get: function get() {
            return this.settings.zoomPane.isShowing;
          }
        }]);

        return Trigger;
      }();

      var _initialiseProps = function _initialiseProps() {
        var _this = this;

        this._handleEntry = function (e) {
          e.preventDefault();
          _this._lastMovement = e;

          if (e.type == 'mouseenter' && _this.settings.hoverDelay) {
            _this.entryTimeout = setTimeout(_this._show, _this.settings.hoverDelay);
          } else if (_this.settings.touchDelay) {
            _this.entryTimeout = setTimeout(_this._show, _this.settings.touchDelay);
          } else {
            _this._show();
          }
        };

        this._show = function () {
          if (!_this.enabled) {
            return;
          }

          var onShow = _this.settings.onShow;
          if (onShow && typeof onShow === 'function') {
            onShow();
          }

          _this.settings.zoomPane.show(_this.settings.el.getAttribute(_this.settings.sourceAttribute), _this.settings.el.clientWidth, _this.settings.el.clientHeight);

          if (_this._lastMovement) {
            var touchActivated = _this._lastMovement.touches;
            if (touchActivated && _this.settings.touchBoundingBox || !touchActivated && _this.settings.hoverBoundingBox) {
              _this.boundingBox.show(_this.settings.zoomPane.el.clientWidth, _this.settings.zoomPane.el.clientHeight);
            }
          }

          _this._handleMovement();
        };

        this._hide = function (e) {
          e.preventDefault();

          _this._lastMovement = null;

          if (_this.entryTimeout) {
            clearTimeout(_this.entryTimeout);
          }

          if (_this.boundingBox) {
            _this.boundingBox.hide();
          }

          var onHide = _this.settings.onHide;
          if (onHide && typeof onHide === 'function') {
            onHide();
          }

          _this.settings.zoomPane.hide();
        };

        this._handleMovement = function (e) {
          if (e) {
            e.preventDefault();
            _this._lastMovement = e;
          } else if (_this._lastMovement) {
            e = _this._lastMovement;
          } else {
            return;
          }

          var movementX = void 0,
              movementY = void 0;

          if (e.touches) {
            var firstTouch = e.touches[0];
            movementX = firstTouch.clientX;
            movementY = firstTouch.clientY;
          } else {
            movementX = e.clientX;
            movementY = e.clientY;
          }

          var el = _this.settings.el;
          var rect = el.getBoundingClientRect();
          var offsetX = movementX - rect.left;
          var offsetY = movementY - rect.top;

          var percentageOffsetX = offsetX / _this.settings.el.clientWidth;
          var percentageOffsetY = offsetY / _this.settings.el.clientHeight;

          if (_this.boundingBox) {
            _this.boundingBox.setPosition(percentageOffsetX, percentageOffsetY, rect);
          }

          _this.settings.zoomPane.setPosition(percentageOffsetX, percentageOffsetY, rect);
        };
      };

      exports.default = Trigger;
    }, { "./BoundingBox": 1, "./util/throwIfMissing": 7 }], 4: [function (require, module, exports) {
      'use strict';

      Object.defineProperty(exports, "__esModule", {
        value: true
      });

      var _createClass = function () {
        function defineProperties(target, props) {
          for (var i = 0; i < props.length; i++) {
            var descriptor = props[i];descriptor.enumerable = descriptor.enumerable || false;descriptor.configurable = true;if ("value" in descriptor) descriptor.writable = true;Object.defineProperty(target, descriptor.key, descriptor);
          }
        }return function (Constructor, protoProps, staticProps) {
          if (protoProps) defineProperties(Constructor.prototype, protoProps);if (staticProps) defineProperties(Constructor, staticProps);return Constructor;
        };
      }();

      var _throwIfMissing = require('./util/throwIfMissing');

      var _throwIfMissing2 = _interopRequireDefault(_throwIfMissing);

      var _dom = require('./util/dom');

      function _interopRequireDefault(obj) {
        return obj && obj.__esModule ? obj : { default: obj };
      }

      function _classCallCheck(instance, Constructor) {
        if (!(instance instanceof Constructor)) {
          throw new TypeError("Cannot call a class as a function");
        }
      }

      // All officially-supported browsers have this, but it's easy to
      // account for, just in case.
      var divStyle = document.createElement('div').style;

      var HAS_ANIMATION = typeof document === 'undefined' ? false : 'animation' in divStyle || 'webkitAnimation' in divStyle;

      var ZoomPane = function () {
        function ZoomPane() {
          var _this = this;

          var options = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};

          _classCallCheck(this, ZoomPane);

          this._completeShow = function () {
            _this.el.removeEventListener('animationend', _this._completeShow, false);
            _this.el.removeEventListener('webkitAnimationEnd', _this._completeShow, false);

            (0, _dom.removeClasses)(_this.el, _this.openingClasses);
          };

          this._completeHide = function () {
            _this.el.removeEventListener('animationend', _this._completeHide, false);
            _this.el.removeEventListener('webkitAnimationEnd', _this._completeHide, false);

            (0, _dom.removeClasses)(_this.el, _this.openClasses);
            (0, _dom.removeClasses)(_this.el, _this.closingClasses);
            (0, _dom.removeClasses)(_this.el, _this.inlineClasses);

            _this.el.setAttribute('style', '');

            // The window could have been resized above or below `inline`
            // limits since the ZoomPane was shown. Because of this, we
            // can't rely on `this._isInline` here.
            if (_this.el.parentElement === _this.settings.container) {
              _this.settings.container.removeChild(_this.el);
            } else if (_this.el.parentElement === _this.settings.inlineContainer) {
              _this.settings.inlineContainer.removeChild(_this.el);
            }
          };

          this._handleLoad = function () {
            _this.imgEl.removeEventListener('load', _this._handleLoad, false);
            (0, _dom.removeClasses)(_this.el, _this.loadingClasses);
          };

          this.isShowing = false;

          var _options$container = options.container,
              container = _options$container === undefined ? null : _options$container,
              _options$zoomFactor = options.zoomFactor,
              zoomFactor = _options$zoomFactor === undefined ? (0, _throwIfMissing2.default)() : _options$zoomFactor,
              _options$inline = options.inline,
              inline = _options$inline === undefined ? (0, _throwIfMissing2.default)() : _options$inline,
              _options$namespace = options.namespace,
              namespace = _options$namespace === undefined ? null : _options$namespace,
              _options$showWhitespa = options.showWhitespaceAtEdges,
              showWhitespaceAtEdges = _options$showWhitespa === undefined ? (0, _throwIfMissing2.default)() : _options$showWhitespa,
              _options$containInlin = options.containInline,
              containInline = _options$containInlin === undefined ? (0, _throwIfMissing2.default)() : _options$containInlin,
              _options$inlineOffset = options.inlineOffsetX,
              inlineOffsetX = _options$inlineOffset === undefined ? 0 : _options$inlineOffset,
              _options$inlineOffset2 = options.inlineOffsetY,
              inlineOffsetY = _options$inlineOffset2 === undefined ? 0 : _options$inlineOffset2,
              _options$inlineContai = options.inlineContainer,
              inlineContainer = _options$inlineContai === undefined ? document.body : _options$inlineContai;

          this.settings = { container: container, zoomFactor: zoomFactor, inline: inline, namespace: namespace, showWhitespaceAtEdges: showWhitespaceAtEdges, containInline: containInline, inlineOffsetX: inlineOffsetX, inlineOffsetY: inlineOffsetY, inlineContainer: inlineContainer };

          this.openClasses = this._buildClasses('open');
          this.openingClasses = this._buildClasses('opening');
          this.closingClasses = this._buildClasses('closing');
          this.inlineClasses = this._buildClasses('inline');
          this.loadingClasses = this._buildClasses('loading');

          this._buildElement();
        }

        _createClass(ZoomPane, [{
          key: '_buildClasses',
          value: function _buildClasses(suffix) {
            var classes = ['drift-' + suffix];

            var ns = this.settings.namespace;
            if (ns) {
              classes.push(ns + '-' + suffix);
            }

            return classes;
          }
        }, {
          key: '_buildElement',
          value: function _buildElement() {
            this.el = document.createElement('div');
            (0, _dom.addClasses)(this.el, this._buildClasses('zoom-pane'));

            var loaderEl = document.createElement('div');
            (0, _dom.addClasses)(loaderEl, this._buildClasses('zoom-pane-loader'));
            this.el.appendChild(loaderEl);

            this.imgEl = document.createElement('img');
            this.el.appendChild(this.imgEl);
          }
        }, {
          key: '_setImageURL',
          value: function _setImageURL(imageURL) {
            this.imgEl.setAttribute('src', imageURL);
          }
        }, {
          key: '_setImageSize',
          value: function _setImageSize(triggerWidth, triggerHeight) {
            this.imgEl.style.width = triggerWidth * this.settings.zoomFactor + 'px';
            this.imgEl.style.height = triggerHeight * this.settings.zoomFactor + 'px';
          }

          // `percentageOffsetX` and `percentageOffsetY` must be percentages
          // expressed as floats between `0' and `1`.

        }, {
          key: 'setPosition',
          value: function setPosition(percentageOffsetX, percentageOffsetY, triggerRect) {
            var left = -(this.imgEl.clientWidth * percentageOffsetX - this.el.clientWidth / 2);
            var top = -(this.imgEl.clientHeight * percentageOffsetY - this.el.clientHeight / 2);
            var maxLeft = -(this.imgEl.clientWidth - this.el.clientWidth);
            var maxTop = -(this.imgEl.clientHeight - this.el.clientHeight);

            if (this.el.parentElement === this.settings.inlineContainer) {
              // This may be needed in the future to deal with browser event
              // inconsistencies, but it's difficult to tell for sure.
              // let scrollX = isTouch ? 0 : window.scrollX;
              // let scrollY = isTouch ? 0 : window.scrollY;
              var scrollX = window.pageXOffset;
              var scrollY = window.pageYOffset;

              var inlineLeft = triggerRect.left + percentageOffsetX * triggerRect.width - this.el.clientWidth / 2 + this.settings.inlineOffsetX + scrollX;
              var inlineTop = triggerRect.top + percentageOffsetY * triggerRect.height - this.el.clientHeight / 2 + this.settings.inlineOffsetY + scrollY;

              if (this.settings.containInline) {
                var elRect = this.el.getBoundingClientRect();

                if (inlineLeft < triggerRect.left + scrollX) {
                  inlineLeft = triggerRect.left + scrollX;
                } else if (inlineLeft + this.el.clientWidth > triggerRect.left + triggerRect.width + scrollX) {
                  inlineLeft = triggerRect.left + triggerRect.width - this.el.clientWidth + scrollX;
                }

                if (inlineTop < triggerRect.top + scrollY) {
                  inlineTop = triggerRect.top + scrollY;
                } else if (inlineTop + this.el.clientHeight > triggerRect.top + triggerRect.height + scrollY) {
                  inlineTop = triggerRect.top + triggerRect.height - this.el.clientHeight + scrollY;
                }
              }

              this.el.style.left = inlineLeft + 'px';
              this.el.style.top = inlineTop + 'px';
            }

            if (!this.settings.showWhitespaceAtEdges) {
              if (left > 0) {
                left = 0;
              } else if (left < maxLeft) {
                left = maxLeft;
              }

              if (top > 0) {
                top = 0;
              } else if (top < maxTop) {
                top = maxTop;
              }
            }

            this.imgEl.style.transform = 'translate(' + left + 'px, ' + top + 'px)';
            this.imgEl.style.webkitTransform = 'translate(' + left + 'px, ' + top + 'px)';
          }
        }, {
          key: '_removeListenersAndResetClasses',
          value: function _removeListenersAndResetClasses() {
            this.el.removeEventListener('animationend', this._completeShow, false);
            this.el.removeEventListener('animationend', this._completeHide, false);
            this.el.removeEventListener('webkitAnimationEnd', this._completeShow, false);
            this.el.removeEventListener('webkitAnimationEnd', this._completeHide, false);
            (0, _dom.removeClasses)(this.el, this.openClasses);
            (0, _dom.removeClasses)(this.el, this.closingClasses);
          }
        }, {
          key: 'show',
          value: function show(imageURL, triggerWidth, triggerHeight) {
            this._removeListenersAndResetClasses();
            this.isShowing = true;

            (0, _dom.addClasses)(this.el, this.openClasses);
            (0, _dom.addClasses)(this.el, this.loadingClasses);

            this.imgEl.addEventListener('load', this._handleLoad, false);
            this._setImageURL(imageURL);
            this._setImageSize(triggerWidth, triggerHeight);

            if (this._isInline) {
              this._showInline();
            } else {
              this._showInContainer();
            }

            if (HAS_ANIMATION) {
              this.el.addEventListener('animationend', this._completeShow, false);
              this.el.addEventListener('webkitAnimationEnd', this._completeShow, false);
              (0, _dom.addClasses)(this.el, this.openingClasses);
            }
          }
        }, {
          key: '_showInline',
          value: function _showInline() {
            this.settings.inlineContainer.appendChild(this.el);
            (0, _dom.addClasses)(this.el, this.inlineClasses);
          }
        }, {
          key: '_showInContainer',
          value: function _showInContainer() {
            this.settings.container.appendChild(this.el);
          }
        }, {
          key: 'hide',
          value: function hide() {
            this._removeListenersAndResetClasses();
            this.isShowing = false;

            if (HAS_ANIMATION) {
              this.el.addEventListener('animationend', this._completeHide, false);
              this.el.addEventListener('webkitAnimationEnd', this._completeHide, false);
              (0, _dom.addClasses)(this.el, this.closingClasses);
            } else {
              (0, _dom.removeClasses)(this.el, this.openClasses);
              (0, _dom.removeClasses)(this.el, this.inlineClasses);
            }
          }
        }, {
          key: '_isInline',
          get: function get() {
            var inline = this.settings.inline;

            return inline === true || typeof inline === 'number' && window.innerWidth <= inline;
          }
        }]);

        return ZoomPane;
      }();

      exports.default = ZoomPane;
    }, { "./util/dom": 6, "./util/throwIfMissing": 7 }], 5: [function (require, module, exports) {
      'use strict';

      Object.defineProperty(exports, "__esModule", {
        value: true
      });
      exports.default = injectBaseStylesheet;
      var RULES = '\n@keyframes noop {\n  0% { zoom: 1; }\n}\n\n@-webkit-keyframes noop {\n  0% { zoom: 1; }\n}\n\n.drift-zoom-pane.drift-open {\n  display: block;\n}\n\n.drift-zoom-pane.drift-opening, .drift-zoom-pane.drift-closing {\n  animation: noop 1ms;\n  -webkit-animation: noop 1ms;\n}\n\n.drift-zoom-pane {\n  position: absolute;\n  overflow: hidden;\n  width: 100%;\n  height: 100%;\n  top: 0;\n  left: 0;\n  pointer-events: none;\n}\n\n.drift-zoom-pane-loader {\n  display: none;\n}\n\n.drift-zoom-pane img {\n  position: absolute;\n  display: block;\n  max-width: none;\n  max-height: none;\n}\n\n.drift-bounding-box {\n  position: absolute;\n  pointer-events: none;\n}\n';

      function injectBaseStylesheet() {
        if (document.querySelector('.drift-base-styles')) {
          return;
        }

        var styleEl = document.createElement('style');
        styleEl.type = 'text/css';
        styleEl.classList.add('drift-base-styles');

        styleEl.appendChild(document.createTextNode(RULES));

        var head = document.head;
        head.insertBefore(styleEl, head.firstChild);
      }
    }, {}], 6: [function (require, module, exports) {
      'use strict';

      var _typeof2 = typeof Symbol === "function" && _typeof3(Symbol.iterator) === "symbol" ? function (obj) {
        return typeof obj === "undefined" ? "undefined" : _typeof3(obj);
      } : function (obj) {
        return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj === "undefined" ? "undefined" : _typeof3(obj);
      };

      Object.defineProperty(exports, "__esModule", {
        value: true
      });

      var _typeof = typeof Symbol === "function" && _typeof2(Symbol.iterator) === "symbol" ? function (obj) {
        return typeof obj === "undefined" ? "undefined" : _typeof2(obj);
      } : function (obj) {
        return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj === "undefined" ? "undefined" : _typeof2(obj);
      };

      exports.isDOMElement = isDOMElement;
      exports.addClasses = addClasses;
      exports.removeClasses = removeClasses;
      // This is not really a perfect check, but works fine.
      // From http://stackoverflow.com/questions/384286
      var HAS_DOM_2 = (typeof HTMLElement === 'undefined' ? 'undefined' : _typeof(HTMLElement)) === 'object';

      function isDOMElement(obj) {
        return HAS_DOM_2 ? obj instanceof HTMLElement : obj && (typeof obj === 'undefined' ? 'undefined' : _typeof(obj)) === 'object' && obj !== null && obj.nodeType === 1 && typeof obj.nodeName === 'string';
      }

      function addClasses(el, classNames) {
        classNames.forEach(function (className) {
          el.classList.add(className);
        });
      }

      function removeClasses(el, classNames) {
        classNames.forEach(function (className) {
          el.classList.remove(className);
        });
      }
    }, {}], 7: [function (require, module, exports) {
      'use strict';

      Object.defineProperty(exports, "__esModule", {
        value: true
      });
      exports.default = throwIfMissing;
      function throwIfMissing() {
        throw new Error('Missing parameter');
      }
    }, {}] }, {}, [2])(2);
});
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInZlbmRvci9EcmlmdC5qcyJdLCJuYW1lcyI6WyJmIiwiZXhwb3J0cyIsIm1vZHVsZSIsImRlZmluZSIsImFtZCIsImciLCJ3aW5kb3ciLCJnbG9iYWwiLCJzZWxmIiwiRHJpZnQiLCJlIiwidCIsIm4iLCJyIiwicyIsIm8iLCJ1IiwiYSIsInJlcXVpcmUiLCJpIiwiRXJyb3IiLCJjb2RlIiwibCIsImNhbGwiLCJsZW5ndGgiLCJPYmplY3QiLCJkZWZpbmVQcm9wZXJ0eSIsInZhbHVlIiwiX2NyZWF0ZUNsYXNzIiwiZGVmaW5lUHJvcGVydGllcyIsInRhcmdldCIsInByb3BzIiwiZGVzY3JpcHRvciIsImVudW1lcmFibGUiLCJjb25maWd1cmFibGUiLCJ3cml0YWJsZSIsImtleSIsIkNvbnN0cnVjdG9yIiwicHJvdG9Qcm9wcyIsInN0YXRpY1Byb3BzIiwicHJvdG90eXBlIiwiX3Rocm93SWZNaXNzaW5nIiwiX3Rocm93SWZNaXNzaW5nMiIsIl9pbnRlcm9wUmVxdWlyZURlZmF1bHQiLCJfZG9tIiwib2JqIiwiX19lc01vZHVsZSIsImRlZmF1bHQiLCJfY2xhc3NDYWxsQ2hlY2siLCJpbnN0YW5jZSIsIlR5cGVFcnJvciIsIkJvdW5kaW5nQm94Iiwib3B0aW9ucyIsImlzU2hvd2luZyIsIl9vcHRpb25zJG5hbWVzcGFjZSIsIm5hbWVzcGFjZSIsInVuZGVmaW5lZCIsIl9vcHRpb25zJHpvb21GYWN0b3IiLCJ6b29tRmFjdG9yIiwiX29wdGlvbnMkY29udGFpbmVyRWwiLCJjb250YWluZXJFbCIsInNldHRpbmdzIiwib3BlbkNsYXNzZXMiLCJfYnVpbGRDbGFzc2VzIiwiX2J1aWxkRWxlbWVudCIsInN1ZmZpeCIsImNsYXNzZXMiLCJucyIsInB1c2giLCJlbCIsImRvY3VtZW50IiwiY3JlYXRlRWxlbWVudCIsImFkZENsYXNzZXMiLCJzaG93Iiwiem9vbVBhbmVXaWR0aCIsInpvb21QYW5lSGVpZ2h0IiwiYXBwZW5kQ2hpbGQiLCJzdHlsZSIsIndpZHRoIiwiTWF0aCIsInJvdW5kIiwiaGVpZ2h0IiwiaGlkZSIsInJlbW92ZUNoaWxkIiwicmVtb3ZlQ2xhc3NlcyIsInNldFBvc2l0aW9uIiwicGVyY2VudGFnZU9mZnNldFgiLCJwZXJjZW50YWdlT2Zmc2V0WSIsInRyaWdnZXJSZWN0IiwicGFnZVhPZmZzZXQiLCJwYWdlWU9mZnNldCIsImlubGluZUxlZnQiLCJsZWZ0IiwiY2xpZW50V2lkdGgiLCJpbmxpbmVUb3AiLCJ0b3AiLCJjbGllbnRIZWlnaHQiLCJlbFJlY3QiLCJnZXRCb3VuZGluZ0NsaWVudFJlY3QiLCJfaW5qZWN0QmFzZVN0eWxlc2hlZXQiLCJfaW5qZWN0QmFzZVN0eWxlc2hlZXQyIiwiX1RyaWdnZXIiLCJfVHJpZ2dlcjIiLCJfWm9vbVBhbmUiLCJfWm9vbVBhbmUyIiwidHJpZ2dlckVsIiwiX3RoaXMiLCJhcmd1bWVudHMiLCJWRVJTSU9OIiwiZGVzdHJveSIsInRyaWdnZXIiLCJfdW5iaW5kRXZlbnRzIiwiaXNET01FbGVtZW50IiwiX29wdGlvbnMkc2hvd1doaXRlc3BhIiwic2hvd1doaXRlc3BhY2VBdEVkZ2VzIiwiX29wdGlvbnMkY29udGFpbklubGluIiwiY29udGFpbklubGluZSIsIl9vcHRpb25zJGlubGluZU9mZnNldCIsImlubGluZU9mZnNldFgiLCJfb3B0aW9ucyRpbmxpbmVPZmZzZXQyIiwiaW5saW5lT2Zmc2V0WSIsIl9vcHRpb25zJGlubGluZUNvbnRhaSIsImlubGluZUNvbnRhaW5lciIsImJvZHkiLCJfb3B0aW9ucyRzb3VyY2VBdHRyaWIiLCJzb3VyY2VBdHRyaWJ1dGUiLCJfb3B0aW9ucyRwYW5lQ29udGFpbmUiLCJwYW5lQ29udGFpbmVyIiwiX29wdGlvbnMkaW5saW5lUGFuZSIsImlubGluZVBhbmUiLCJfb3B0aW9ucyRoYW5kbGVUb3VjaCIsImhhbmRsZVRvdWNoIiwiX29wdGlvbnMkb25TaG93Iiwib25TaG93IiwiX29wdGlvbnMkb25IaWRlIiwib25IaWRlIiwiX29wdGlvbnMkaW5qZWN0QmFzZVN0IiwiaW5qZWN0QmFzZVN0eWxlcyIsIl9vcHRpb25zJGhvdmVyRGVsYXkiLCJob3ZlckRlbGF5IiwiX29wdGlvbnMkdG91Y2hEZWxheSIsInRvdWNoRGVsYXkiLCJfb3B0aW9ucyRob3ZlckJvdW5kaW4iLCJob3ZlckJvdW5kaW5nQm94IiwiX29wdGlvbnMkdG91Y2hCb3VuZGluIiwidG91Y2hCb3VuZGluZ0JveCIsIl9idWlsZFpvb21QYW5lIiwiX2J1aWxkVHJpZ2dlciIsInpvb21QYW5lIiwiY29udGFpbmVyIiwiaW5saW5lIiwic2V0Wm9vbUltYWdlVVJMIiwiaW1hZ2VVUkwiLCJfc2V0SW1hZ2VVUkwiLCJkaXNhYmxlIiwiZW5hYmxlZCIsImVuYWJsZSIsImdldCIsInNldCIsInpmIiwiX0JvdW5kaW5nQm94IiwiX0JvdW5kaW5nQm94MiIsIlRyaWdnZXIiLCJfaW5pdGlhbGlzZVByb3BzIiwiX29wdGlvbnMkZWwiLCJfb3B0aW9ucyR6b29tUGFuZSIsImJvdW5kaW5nQm94Iiwib2Zmc2V0UGFyZW50IiwiX2JpbmRFdmVudHMiLCJhZGRFdmVudExpc3RlbmVyIiwiX2hhbmRsZUVudHJ5IiwiX2hpZGUiLCJfaGFuZGxlTW92ZW1lbnQiLCJyZW1vdmVFdmVudExpc3RlbmVyIiwicHJldmVudERlZmF1bHQiLCJfbGFzdE1vdmVtZW50IiwidHlwZSIsImVudHJ5VGltZW91dCIsInNldFRpbWVvdXQiLCJfc2hvdyIsImdldEF0dHJpYnV0ZSIsInRvdWNoQWN0aXZhdGVkIiwidG91Y2hlcyIsImNsZWFyVGltZW91dCIsIm1vdmVtZW50WCIsIm1vdmVtZW50WSIsImZpcnN0VG91Y2giLCJjbGllbnRYIiwiY2xpZW50WSIsInJlY3QiLCJvZmZzZXRYIiwib2Zmc2V0WSIsImRpdlN0eWxlIiwiSEFTX0FOSU1BVElPTiIsIlpvb21QYW5lIiwiX2NvbXBsZXRlU2hvdyIsIm9wZW5pbmdDbGFzc2VzIiwiX2NvbXBsZXRlSGlkZSIsImNsb3NpbmdDbGFzc2VzIiwiaW5saW5lQ2xhc3NlcyIsInNldEF0dHJpYnV0ZSIsInBhcmVudEVsZW1lbnQiLCJfaGFuZGxlTG9hZCIsImltZ0VsIiwibG9hZGluZ0NsYXNzZXMiLCJfb3B0aW9ucyRjb250YWluZXIiLCJfb3B0aW9ucyRpbmxpbmUiLCJsb2FkZXJFbCIsIl9zZXRJbWFnZVNpemUiLCJ0cmlnZ2VyV2lkdGgiLCJ0cmlnZ2VySGVpZ2h0IiwibWF4TGVmdCIsIm1heFRvcCIsInNjcm9sbFgiLCJzY3JvbGxZIiwidHJhbnNmb3JtIiwid2Via2l0VHJhbnNmb3JtIiwiX3JlbW92ZUxpc3RlbmVyc0FuZFJlc2V0Q2xhc3NlcyIsIl9pc0lubGluZSIsIl9zaG93SW5saW5lIiwiX3Nob3dJbkNvbnRhaW5lciIsImlubmVyV2lkdGgiLCJpbmplY3RCYXNlU3R5bGVzaGVldCIsIlJVTEVTIiwicXVlcnlTZWxlY3RvciIsInN0eWxlRWwiLCJjbGFzc0xpc3QiLCJhZGQiLCJjcmVhdGVUZXh0Tm9kZSIsImhlYWQiLCJpbnNlcnRCZWZvcmUiLCJmaXJzdENoaWxkIiwiX3R5cGVvZjIiLCJTeW1ib2wiLCJpdGVyYXRvciIsImNvbnN0cnVjdG9yIiwiX3R5cGVvZiIsIkhBU19ET01fMiIsIkhUTUxFbGVtZW50Iiwibm9kZVR5cGUiLCJub2RlTmFtZSIsImNsYXNzTmFtZXMiLCJmb3JFYWNoIiwiY2xhc3NOYW1lIiwicmVtb3ZlIiwidGhyb3dJZk1pc3NpbmciXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxDQUFDLFVBQVNBLENBQVQsRUFBVztBQUFDLE1BQUcsUUFBT0MsT0FBUCwwQ0FBT0EsT0FBUCxPQUFpQixRQUFqQixJQUEyQixPQUFPQyxNQUFQLEtBQWdCLFdBQTlDLEVBQTBEO0FBQUNBLFdBQU9ELE9BQVAsR0FBZUQsR0FBZjtBQUFtQixHQUE5RSxNQUFtRixJQUFHLE9BQU9HLE1BQVAsS0FBZ0IsVUFBaEIsSUFBNEJBLE9BQU9DLEdBQXRDLEVBQTBDO0FBQUNELFdBQU8sRUFBUCxFQUFVSCxDQUFWO0FBQWEsR0FBeEQsTUFBNEQ7QUFBQyxRQUFJSyxDQUFKLENBQU0sSUFBRyxPQUFPQyxNQUFQLEtBQWdCLFdBQW5CLEVBQStCO0FBQUNELFVBQUVDLE1BQUY7QUFBUyxLQUF6QyxNQUE4QyxJQUFHLE9BQU9DLE1BQVAsS0FBZ0IsV0FBbkIsRUFBK0I7QUFBQ0YsVUFBRUUsTUFBRjtBQUFTLEtBQXpDLE1BQThDLElBQUcsT0FBT0MsSUFBUCxLQUFjLFdBQWpCLEVBQTZCO0FBQUNILFVBQUVHLElBQUY7QUFBTyxLQUFyQyxNQUF5QztBQUFDSCxVQUFFLElBQUY7QUFBTyxPQUFFSSxLQUFGLEdBQVVULEdBQVY7QUFBYztBQUFDLENBQS9ULEVBQWlVLFlBQVU7QUFBQyxNQUFJRyxNQUFKLEVBQVdELE1BQVgsRUFBa0JELE9BQWxCLENBQTBCLE9BQVEsU0FBU1MsQ0FBVCxDQUFXQyxDQUFYLEVBQWFDLENBQWIsRUFBZUMsQ0FBZixFQUFpQjtBQUFDLGFBQVNDLENBQVQsQ0FBV0MsQ0FBWCxFQUFhQyxDQUFiLEVBQWU7QUFBQyxVQUFHLENBQUNKLEVBQUVHLENBQUYsQ0FBSixFQUFTO0FBQUMsWUFBRyxDQUFDSixFQUFFSSxDQUFGLENBQUosRUFBUztBQUFDLGNBQUlFLElBQUUsT0FBT0MsT0FBUCxJQUFnQixVQUFoQixJQUE0QkEsT0FBbEMsQ0FBMEMsSUFBRyxDQUFDRixDQUFELElBQUlDLENBQVAsRUFBUyxPQUFPQSxFQUFFRixDQUFGLEVBQUksQ0FBQyxDQUFMLENBQVAsQ0FBZSxJQUFHSSxDQUFILEVBQUssT0FBT0EsRUFBRUosQ0FBRixFQUFJLENBQUMsQ0FBTCxDQUFQLENBQWUsSUFBSWYsSUFBRSxJQUFJb0IsS0FBSixDQUFVLHlCQUF1QkwsQ0FBdkIsR0FBeUIsR0FBbkMsQ0FBTixDQUE4QyxNQUFNZixFQUFFcUIsSUFBRixHQUFPLGtCQUFQLEVBQTBCckIsQ0FBaEM7QUFBa0MsYUFBSXNCLElBQUVWLEVBQUVHLENBQUYsSUFBSyxFQUFDZCxTQUFRLEVBQVQsRUFBWCxDQUF3QlUsRUFBRUksQ0FBRixFQUFLLENBQUwsRUFBUVEsSUFBUixDQUFhRCxFQUFFckIsT0FBZixFQUF1QixVQUFTUyxDQUFULEVBQVc7QUFBQyxjQUFJRSxJQUFFRCxFQUFFSSxDQUFGLEVBQUssQ0FBTCxFQUFRTCxDQUFSLENBQU4sQ0FBaUIsT0FBT0ksRUFBRUYsSUFBRUEsQ0FBRixHQUFJRixDQUFOLENBQVA7QUFBZ0IsU0FBcEUsRUFBcUVZLENBQXJFLEVBQXVFQSxFQUFFckIsT0FBekUsRUFBaUZTLENBQWpGLEVBQW1GQyxDQUFuRixFQUFxRkMsQ0FBckYsRUFBdUZDLENBQXZGO0FBQTBGLGNBQU9ELEVBQUVHLENBQUYsRUFBS2QsT0FBWjtBQUFvQixTQUFJa0IsSUFBRSxPQUFPRCxPQUFQLElBQWdCLFVBQWhCLElBQTRCQSxPQUFsQyxDQUEwQyxLQUFJLElBQUlILElBQUUsQ0FBVixFQUFZQSxJQUFFRixFQUFFVyxNQUFoQixFQUF1QlQsR0FBdkI7QUFBMkJELFFBQUVELEVBQUVFLENBQUYsQ0FBRjtBQUEzQixLQUFtQyxPQUFPRCxDQUFQO0FBQVMsR0FBemIsQ0FBMmIsRUFBQyxHQUFFLENBQUMsVUFBU0ksT0FBVCxFQUFpQmhCLE1BQWpCLEVBQXdCRCxPQUF4QixFQUFnQztBQUM1MEI7O0FBRUF3QixhQUFPQyxjQUFQLENBQXNCekIsT0FBdEIsRUFBK0IsWUFBL0IsRUFBNkM7QUFDM0MwQixlQUFPO0FBRG9DLE9BQTdDOztBQUlBLFVBQUlDLGVBQWUsWUFBWTtBQUM3QixpQkFBU0MsZ0JBQVQsQ0FBMEJDLE1BQTFCLEVBQWtDQyxLQUFsQyxFQUF5QztBQUN2QyxlQUFLLElBQUlaLElBQUksQ0FBYixFQUFnQkEsSUFBSVksTUFBTVAsTUFBMUIsRUFBa0NMLEdBQWxDLEVBQXVDO0FBQ3JDLGdCQUFJYSxhQUFhRCxNQUFNWixDQUFOLENBQWpCLENBQTBCYSxXQUFXQyxVQUFYLEdBQXdCRCxXQUFXQyxVQUFYLElBQXlCLEtBQWpELENBQXVERCxXQUFXRSxZQUFYLEdBQTBCLElBQTFCLENBQStCLElBQUksV0FBV0YsVUFBZixFQUEyQkEsV0FBV0csUUFBWCxHQUFzQixJQUF0QixDQUEyQlYsT0FBT0MsY0FBUCxDQUFzQkksTUFBdEIsRUFBOEJFLFdBQVdJLEdBQXpDLEVBQThDSixVQUE5QztBQUN2SztBQUNGLGdCQUFPLFVBQVVLLFdBQVYsRUFBdUJDLFVBQXZCLEVBQW1DQyxXQUFuQyxFQUFnRDtBQUN0RCxjQUFJRCxVQUFKLEVBQWdCVCxpQkFBaUJRLFlBQVlHLFNBQTdCLEVBQXdDRixVQUF4QyxFQUFvRCxJQUFJQyxXQUFKLEVBQWlCVixpQkFBaUJRLFdBQWpCLEVBQThCRSxXQUE5QixFQUEyQyxPQUFPRixXQUFQO0FBQ2pJLFNBRkE7QUFHRixPQVJrQixFQUFuQjs7QUFVQSxVQUFJSSxrQkFBa0J2QixRQUFRLHVCQUFSLENBQXRCOztBQUVBLFVBQUl3QixtQkFBbUJDLHVCQUF1QkYsZUFBdkIsQ0FBdkI7O0FBRUEsVUFBSUcsT0FBTzFCLFFBQVEsWUFBUixDQUFYOztBQUVBLGVBQVN5QixzQkFBVCxDQUFnQ0UsR0FBaEMsRUFBcUM7QUFDbkMsZUFBT0EsT0FBT0EsSUFBSUMsVUFBWCxHQUF3QkQsR0FBeEIsR0FBOEIsRUFBRUUsU0FBU0YsR0FBWCxFQUFyQztBQUNEOztBQUVELGVBQVNHLGVBQVQsQ0FBeUJDLFFBQXpCLEVBQW1DWixXQUFuQyxFQUFnRDtBQUM5QyxZQUFJLEVBQUVZLG9CQUFvQlosV0FBdEIsQ0FBSixFQUF3QztBQUN0QyxnQkFBTSxJQUFJYSxTQUFKLENBQWMsbUNBQWQsQ0FBTjtBQUNEO0FBQ0Y7O0FBRUQsVUFBSUMsY0FBYyxZQUFZO0FBQzVCLGlCQUFTQSxXQUFULENBQXFCQyxPQUFyQixFQUE4QjtBQUM1QkosMEJBQWdCLElBQWhCLEVBQXNCRyxXQUF0Qjs7QUFFQSxlQUFLRSxTQUFMLEdBQWlCLEtBQWpCOztBQUVBLGNBQUlDLHFCQUFxQkYsUUFBUUcsU0FBakM7QUFBQSxjQUNJQSxZQUFZRCx1QkFBdUJFLFNBQXZCLEdBQW1DLElBQW5DLEdBQTBDRixrQkFEMUQ7QUFBQSxjQUVJRyxzQkFBc0JMLFFBQVFNLFVBRmxDO0FBQUEsY0FHSUEsYUFBYUQsd0JBQXdCRCxTQUF4QixHQUFvQyxDQUFDLEdBQUdkLGlCQUFpQkssT0FBckIsR0FBcEMsR0FBc0VVLG1CQUh2RjtBQUFBLGNBSUlFLHVCQUF1QlAsUUFBUVEsV0FKbkM7QUFBQSxjQUtJQSxjQUFjRCx5QkFBeUJILFNBQXpCLEdBQXFDLENBQUMsR0FBR2QsaUJBQWlCSyxPQUFyQixHQUFyQyxHQUF1RVksb0JBTHpGOztBQU9BLGVBQUtFLFFBQUwsR0FBZ0IsRUFBRU4sV0FBV0EsU0FBYixFQUF3QkcsWUFBWUEsVUFBcEMsRUFBZ0RFLGFBQWFBLFdBQTdELEVBQWhCOztBQUVBLGVBQUtFLFdBQUwsR0FBbUIsS0FBS0MsYUFBTCxDQUFtQixNQUFuQixDQUFuQjs7QUFFQSxlQUFLQyxhQUFMO0FBQ0Q7O0FBRURwQyxxQkFBYXVCLFdBQWIsRUFBMEIsQ0FBQztBQUN6QmYsZUFBSyxlQURvQjtBQUV6QlQsaUJBQU8sU0FBU29DLGFBQVQsQ0FBdUJFLE1BQXZCLEVBQStCO0FBQ3BDLGdCQUFJQyxVQUFVLENBQUMsV0FBV0QsTUFBWixDQUFkOztBQUVBLGdCQUFJRSxLQUFLLEtBQUtOLFFBQUwsQ0FBY04sU0FBdkI7QUFDQSxnQkFBSVksRUFBSixFQUFRO0FBQ05ELHNCQUFRRSxJQUFSLENBQWFELEtBQUssR0FBTCxHQUFXRixNQUF4QjtBQUNEOztBQUVELG1CQUFPQyxPQUFQO0FBQ0Q7QUFYd0IsU0FBRCxFQVl2QjtBQUNEOUIsZUFBSyxlQURKO0FBRURULGlCQUFPLFNBQVNxQyxhQUFULEdBQXlCO0FBQzlCLGlCQUFLSyxFQUFMLEdBQVVDLFNBQVNDLGFBQVQsQ0FBdUIsS0FBdkIsQ0FBVjtBQUNBLGFBQUMsR0FBRzNCLEtBQUs0QixVQUFULEVBQXFCLEtBQUtILEVBQTFCLEVBQThCLEtBQUtOLGFBQUwsQ0FBbUIsY0FBbkIsQ0FBOUI7QUFDRDtBQUxBLFNBWnVCLEVBa0J2QjtBQUNEM0IsZUFBSyxNQURKO0FBRURULGlCQUFPLFNBQVM4QyxJQUFULENBQWNDLGFBQWQsRUFBNkJDLGNBQTdCLEVBQTZDO0FBQ2xELGlCQUFLdEIsU0FBTCxHQUFpQixJQUFqQjs7QUFFQSxpQkFBS1EsUUFBTCxDQUFjRCxXQUFkLENBQTBCZ0IsV0FBMUIsQ0FBc0MsS0FBS1AsRUFBM0M7O0FBRUEsZ0JBQUlRLFFBQVEsS0FBS1IsRUFBTCxDQUFRUSxLQUFwQjtBQUNBQSxrQkFBTUMsS0FBTixHQUFjQyxLQUFLQyxLQUFMLENBQVdOLGdCQUFnQixLQUFLYixRQUFMLENBQWNILFVBQXpDLElBQXVELElBQXJFO0FBQ0FtQixrQkFBTUksTUFBTixHQUFlRixLQUFLQyxLQUFMLENBQVdMLGlCQUFpQixLQUFLZCxRQUFMLENBQWNILFVBQTFDLElBQXdELElBQXZFOztBQUVBLGFBQUMsR0FBR2QsS0FBSzRCLFVBQVQsRUFBcUIsS0FBS0gsRUFBMUIsRUFBOEIsS0FBS1AsV0FBbkM7QUFDRDtBQVpBLFNBbEJ1QixFQStCdkI7QUFDRDFCLGVBQUssTUFESjtBQUVEVCxpQkFBTyxTQUFTdUQsSUFBVCxHQUFnQjtBQUNyQixnQkFBSSxLQUFLN0IsU0FBVCxFQUFvQjtBQUNsQixtQkFBS1EsUUFBTCxDQUFjRCxXQUFkLENBQTBCdUIsV0FBMUIsQ0FBc0MsS0FBS2QsRUFBM0M7QUFDRDs7QUFFRCxpQkFBS2hCLFNBQUwsR0FBaUIsS0FBakI7O0FBRUEsYUFBQyxHQUFHVCxLQUFLd0MsYUFBVCxFQUF3QixLQUFLZixFQUE3QixFQUFpQyxLQUFLUCxXQUF0QztBQUNEO0FBVkEsU0EvQnVCLEVBMEN2QjtBQUNEMUIsZUFBSyxhQURKO0FBRURULGlCQUFPLFNBQVMwRCxXQUFULENBQXFCQyxpQkFBckIsRUFBd0NDLGlCQUF4QyxFQUEyREMsV0FBM0QsRUFBd0U7QUFDN0UsZ0JBQUlDLGNBQWNuRixPQUFPbUYsV0FBekI7QUFDQSxnQkFBSUMsY0FBY3BGLE9BQU9vRixXQUF6Qjs7QUFFQSxnQkFBSUMsYUFBYUgsWUFBWUksSUFBWixHQUFtQk4sb0JBQW9CRSxZQUFZVixLQUFuRCxHQUEyRCxLQUFLVCxFQUFMLENBQVF3QixXQUFSLEdBQXNCLENBQWpGLEdBQXFGSixXQUF0RztBQUNBLGdCQUFJSyxZQUFZTixZQUFZTyxHQUFaLEdBQWtCUixvQkFBb0JDLFlBQVlQLE1BQWxELEdBQTJELEtBQUtaLEVBQUwsQ0FBUTJCLFlBQVIsR0FBdUIsQ0FBbEYsR0FBc0ZOLFdBQXRHOztBQUVBLGdCQUFJTyxTQUFTLEtBQUs1QixFQUFMLENBQVE2QixxQkFBUixFQUFiOztBQUVBLGdCQUFJUCxhQUFhSCxZQUFZSSxJQUFaLEdBQW1CSCxXQUFwQyxFQUFpRDtBQUMvQ0UsMkJBQWFILFlBQVlJLElBQVosR0FBbUJILFdBQWhDO0FBQ0QsYUFGRCxNQUVPLElBQUlFLGFBQWEsS0FBS3RCLEVBQUwsQ0FBUXdCLFdBQXJCLEdBQW1DTCxZQUFZSSxJQUFaLEdBQW1CSixZQUFZVixLQUEvQixHQUF1Q1csV0FBOUUsRUFBMkY7QUFDaEdFLDJCQUFhSCxZQUFZSSxJQUFaLEdBQW1CSixZQUFZVixLQUEvQixHQUF1QyxLQUFLVCxFQUFMLENBQVF3QixXQUEvQyxHQUE2REosV0FBMUU7QUFDRDs7QUFFRCxnQkFBSUssWUFBWU4sWUFBWU8sR0FBWixHQUFrQkwsV0FBbEMsRUFBK0M7QUFDN0NJLDBCQUFZTixZQUFZTyxHQUFaLEdBQWtCTCxXQUE5QjtBQUNELGFBRkQsTUFFTyxJQUFJSSxZQUFZLEtBQUt6QixFQUFMLENBQVEyQixZQUFwQixHQUFtQ1IsWUFBWU8sR0FBWixHQUFrQlAsWUFBWVAsTUFBOUIsR0FBdUNTLFdBQTlFLEVBQTJGO0FBQ2hHSSwwQkFBWU4sWUFBWU8sR0FBWixHQUFrQlAsWUFBWVAsTUFBOUIsR0FBdUMsS0FBS1osRUFBTCxDQUFRMkIsWUFBL0MsR0FBOEROLFdBQTFFO0FBQ0Q7O0FBRUQsaUJBQUtyQixFQUFMLENBQVFRLEtBQVIsQ0FBY2UsSUFBZCxHQUFxQkQsYUFBYSxJQUFsQztBQUNBLGlCQUFLdEIsRUFBTCxDQUFRUSxLQUFSLENBQWNrQixHQUFkLEdBQW9CRCxZQUFZLElBQWhDO0FBQ0Q7QUF6QkEsU0ExQ3VCLENBQTFCOztBQXNFQSxlQUFPM0MsV0FBUDtBQUNELE9BM0ZpQixFQUFsQjs7QUE2RkFsRCxjQUFROEMsT0FBUixHQUFrQkksV0FBbEI7QUFFQyxLQWhJMHlCLEVBZ0l6eUIsRUFBQyxjQUFhLENBQWQsRUFBZ0IseUJBQXdCLENBQXhDLEVBaEl5eUIsQ0FBSCxFQWdJMXZCLEdBQUUsQ0FBQyxVQUFTakMsT0FBVCxFQUFpQmhCLE1BQWpCLEVBQXdCRCxPQUF4QixFQUFnQztBQUNqRjs7QUFFQSxVQUFJMkIsZUFBZSxZQUFZO0FBQzdCLGlCQUFTQyxnQkFBVCxDQUEwQkMsTUFBMUIsRUFBa0NDLEtBQWxDLEVBQXlDO0FBQ3ZDLGVBQUssSUFBSVosSUFBSSxDQUFiLEVBQWdCQSxJQUFJWSxNQUFNUCxNQUExQixFQUFrQ0wsR0FBbEMsRUFBdUM7QUFDckMsZ0JBQUlhLGFBQWFELE1BQU1aLENBQU4sQ0FBakIsQ0FBMEJhLFdBQVdDLFVBQVgsR0FBd0JELFdBQVdDLFVBQVgsSUFBeUIsS0FBakQsQ0FBdURELFdBQVdFLFlBQVgsR0FBMEIsSUFBMUIsQ0FBK0IsSUFBSSxXQUFXRixVQUFmLEVBQTJCQSxXQUFXRyxRQUFYLEdBQXNCLElBQXRCLENBQTJCVixPQUFPQyxjQUFQLENBQXNCSSxNQUF0QixFQUE4QkUsV0FBV0ksR0FBekMsRUFBOENKLFVBQTlDO0FBQ3ZLO0FBQ0YsZ0JBQU8sVUFBVUssV0FBVixFQUF1QkMsVUFBdkIsRUFBbUNDLFdBQW5DLEVBQWdEO0FBQ3RELGNBQUlELFVBQUosRUFBZ0JULGlCQUFpQlEsWUFBWUcsU0FBN0IsRUFBd0NGLFVBQXhDLEVBQW9ELElBQUlDLFdBQUosRUFBaUJWLGlCQUFpQlEsV0FBakIsRUFBOEJFLFdBQTlCLEVBQTJDLE9BQU9GLFdBQVA7QUFDakksU0FGQTtBQUdGLE9BUmtCLEVBQW5COztBQVVBLFVBQUlPLE9BQU8xQixRQUFRLFlBQVIsQ0FBWDs7QUFFQSxVQUFJaUYsd0JBQXdCakYsUUFBUSx3QkFBUixDQUE1Qjs7QUFFQSxVQUFJa0YseUJBQXlCekQsdUJBQXVCd0QscUJBQXZCLENBQTdCOztBQUVBLFVBQUlFLFdBQVduRixRQUFRLFdBQVIsQ0FBZjs7QUFFQSxVQUFJb0YsWUFBWTNELHVCQUF1QjBELFFBQXZCLENBQWhCOztBQUVBLFVBQUlFLFlBQVlyRixRQUFRLFlBQVIsQ0FBaEI7O0FBRUEsVUFBSXNGLGFBQWE3RCx1QkFBdUI0RCxTQUF2QixDQUFqQjs7QUFFQSxlQUFTNUQsc0JBQVQsQ0FBZ0NFLEdBQWhDLEVBQXFDO0FBQ25DLGVBQU9BLE9BQU9BLElBQUlDLFVBQVgsR0FBd0JELEdBQXhCLEdBQThCLEVBQUVFLFNBQVNGLEdBQVgsRUFBckM7QUFDRDs7QUFFRCxlQUFTRyxlQUFULENBQXlCQyxRQUF6QixFQUFtQ1osV0FBbkMsRUFBZ0Q7QUFDOUMsWUFBSSxFQUFFWSxvQkFBb0JaLFdBQXRCLENBQUosRUFBd0M7QUFDdEMsZ0JBQU0sSUFBSWEsU0FBSixDQUFjLG1DQUFkLENBQU47QUFDRDtBQUNGOztBQUVEaEQsYUFBT0QsT0FBUCxHQUFpQixZQUFZO0FBQzNCLGlCQUFTUSxLQUFULENBQWVnRyxTQUFmLEVBQTBCO0FBQ3hCLGNBQUlDLFFBQVEsSUFBWjs7QUFFQSxjQUFJdEQsVUFBVXVELFVBQVVuRixNQUFWLEdBQW1CLENBQW5CLElBQXdCbUYsVUFBVSxDQUFWLE1BQWlCbkQsU0FBekMsR0FBcURtRCxVQUFVLENBQVYsQ0FBckQsR0FBb0UsRUFBbEY7O0FBRUEzRCwwQkFBZ0IsSUFBaEIsRUFBc0J2QyxLQUF0Qjs7QUFFQSxlQUFLbUcsT0FBTCxHQUFlLE9BQWY7O0FBRUEsZUFBS0MsT0FBTCxHQUFlLFlBQVk7QUFDekJILGtCQUFNSSxPQUFOLENBQWNDLGFBQWQ7QUFDRCxXQUZEOztBQUlBLGVBQUtOLFNBQUwsR0FBaUJBLFNBQWpCOztBQUVBLGNBQUksQ0FBQyxDQUFDLEdBQUc3RCxLQUFLb0UsWUFBVCxFQUF1QixLQUFLUCxTQUE1QixDQUFMLEVBQTZDO0FBQzNDLGtCQUFNLElBQUl2RCxTQUFKLENBQWMsMkRBQWQsQ0FBTjtBQUNEOztBQUVEO0FBQ0E7QUFDQTtBQUNBLGNBQUlJLHFCQUFxQkYsUUFBUUcsU0FBakM7QUFBQSxjQUNJQSxZQUFZRCx1QkFBdUJFLFNBQXZCLEdBQW1DLElBQW5DLEdBQTBDRixrQkFEMUQ7QUFBQSxjQUVJMkQsd0JBQXdCN0QsUUFBUThELHFCQUZwQztBQUFBLGNBR0lBLHdCQUF3QkQsMEJBQTBCekQsU0FBMUIsR0FBc0MsS0FBdEMsR0FBOEN5RCxxQkFIMUU7QUFBQSxjQUlJRSx3QkFBd0IvRCxRQUFRZ0UsYUFKcEM7QUFBQSxjQUtJQSxnQkFBZ0JELDBCQUEwQjNELFNBQTFCLEdBQXNDLEtBQXRDLEdBQThDMkQscUJBTGxFO0FBQUEsY0FNSUUsd0JBQXdCakUsUUFBUWtFLGFBTnBDO0FBQUEsY0FPSUEsZ0JBQWdCRCwwQkFBMEI3RCxTQUExQixHQUFzQyxDQUF0QyxHQUEwQzZELHFCQVA5RDtBQUFBLGNBUUlFLHlCQUF5Qm5FLFFBQVFvRSxhQVJyQztBQUFBLGNBU0lBLGdCQUFnQkQsMkJBQTJCL0QsU0FBM0IsR0FBdUMsQ0FBdkMsR0FBMkMrRCxzQkFUL0Q7QUFBQSxjQVVJRSx3QkFBd0JyRSxRQUFRc0UsZUFWcEM7QUFBQSxjQVdJQSxrQkFBa0JELDBCQUEwQmpFLFNBQTFCLEdBQXNDYyxTQUFTcUQsSUFBL0MsR0FBc0RGLHFCQVg1RTtBQUFBLGNBWUlHLHdCQUF3QnhFLFFBQVF5RSxlQVpwQztBQUFBLGNBYUlBLGtCQUFrQkQsMEJBQTBCcEUsU0FBMUIsR0FBc0MsV0FBdEMsR0FBb0RvRSxxQkFiMUU7QUFBQSxjQWNJbkUsc0JBQXNCTCxRQUFRTSxVQWRsQztBQUFBLGNBZUlBLGFBQWFELHdCQUF3QkQsU0FBeEIsR0FBb0MsQ0FBcEMsR0FBd0NDLG1CQWZ6RDtBQUFBLGNBZ0JJcUUsd0JBQXdCMUUsUUFBUTJFLGFBaEJwQztBQUFBLGNBaUJJQSxnQkFBZ0JELDBCQUEwQnRFLFNBQTFCLEdBQXNDYyxTQUFTcUQsSUFBL0MsR0FBc0RHLHFCQWpCMUU7QUFBQSxjQWtCSUUsc0JBQXNCNUUsUUFBUTZFLFVBbEJsQztBQUFBLGNBbUJJQSxhQUFhRCx3QkFBd0J4RSxTQUF4QixHQUFvQyxHQUFwQyxHQUEwQ3dFLG1CQW5CM0Q7QUFBQSxjQW9CSUUsdUJBQXVCOUUsUUFBUStFLFdBcEJuQztBQUFBLGNBcUJJQSxjQUFjRCx5QkFBeUIxRSxTQUF6QixHQUFxQyxJQUFyQyxHQUE0QzBFLG9CQXJCOUQ7QUFBQSxjQXNCSUUsa0JBQWtCaEYsUUFBUWlGLE1BdEI5QjtBQUFBLGNBdUJJQSxTQUFTRCxvQkFBb0I1RSxTQUFwQixHQUFnQyxJQUFoQyxHQUF1QzRFLGVBdkJwRDtBQUFBLGNBd0JJRSxrQkFBa0JsRixRQUFRbUYsTUF4QjlCO0FBQUEsY0F5QklBLFNBQVNELG9CQUFvQjlFLFNBQXBCLEdBQWdDLElBQWhDLEdBQXVDOEUsZUF6QnBEO0FBQUEsY0EwQklFLHdCQUF3QnBGLFFBQVFxRixnQkExQnBDO0FBQUEsY0EyQklBLG1CQUFtQkQsMEJBQTBCaEYsU0FBMUIsR0FBc0MsSUFBdEMsR0FBNkNnRixxQkEzQnBFO0FBQUEsY0E0QklFLHNCQUFzQnRGLFFBQVF1RixVQTVCbEM7QUFBQSxjQTZCSUEsYUFBYUQsd0JBQXdCbEYsU0FBeEIsR0FBb0MsQ0FBcEMsR0FBd0NrRixtQkE3QnpEO0FBQUEsY0E4QklFLHNCQUFzQnhGLFFBQVF5RixVQTlCbEM7QUFBQSxjQStCSUEsYUFBYUQsd0JBQXdCcEYsU0FBeEIsR0FBb0MsQ0FBcEMsR0FBd0NvRixtQkEvQnpEO0FBQUEsY0FnQ0lFLHdCQUF3QjFGLFFBQVEyRixnQkFoQ3BDO0FBQUEsY0FpQ0lBLG1CQUFtQkQsMEJBQTBCdEYsU0FBMUIsR0FBc0MsS0FBdEMsR0FBOENzRixxQkFqQ3JFO0FBQUEsY0FrQ0lFLHdCQUF3QjVGLFFBQVE2RixnQkFsQ3BDO0FBQUEsY0FtQ0lBLG1CQUFtQkQsMEJBQTBCeEYsU0FBMUIsR0FBc0MsS0FBdEMsR0FBOEN3RixxQkFuQ3JFOztBQXFDQSxjQUFJZixlQUFlLElBQWYsSUFBdUIsQ0FBQyxDQUFDLEdBQUdyRixLQUFLb0UsWUFBVCxFQUF1QmUsYUFBdkIsQ0FBNUIsRUFBbUU7QUFDakUsa0JBQU0sSUFBSTdFLFNBQUosQ0FBYyxrRUFBZCxDQUFOO0FBQ0Q7QUFDRCxjQUFJLENBQUMsQ0FBQyxHQUFHTixLQUFLb0UsWUFBVCxFQUF1QlUsZUFBdkIsQ0FBTCxFQUE4QztBQUM1QyxrQkFBTSxJQUFJeEUsU0FBSixDQUFjLHlDQUFkLENBQU47QUFDRDs7QUFFRCxlQUFLVyxRQUFMLEdBQWdCLEVBQUVOLFdBQVdBLFNBQWIsRUFBd0IyRCx1QkFBdUJBLHFCQUEvQyxFQUFzRUUsZUFBZUEsYUFBckYsRUFBb0dFLGVBQWVBLGFBQW5ILEVBQWtJRSxlQUFlQSxhQUFqSixFQUFnS0UsaUJBQWlCQSxlQUFqTCxFQUFrTUcsaUJBQWlCQSxlQUFuTixFQUFvT25FLFlBQVlBLFVBQWhQLEVBQTRQcUUsZUFBZUEsYUFBM1EsRUFBMFJFLFlBQVlBLFVBQXRTLEVBQWtURSxhQUFhQSxXQUEvVCxFQUE0VUUsUUFBUUEsTUFBcFYsRUFBNFZFLFFBQVFBLE1BQXBXLEVBQTRXRSxrQkFBa0JBLGdCQUE5WCxFQUFnWkUsWUFBWUEsVUFBNVosRUFBd2FFLFlBQVlBLFVBQXBiLEVBQWdjRSxrQkFBa0JBLGdCQUFsZCxFQUFvZUUsa0JBQWtCQSxnQkFBdGYsRUFBaEI7O0FBRUEsY0FBSSxLQUFLcEYsUUFBTCxDQUFjNEUsZ0JBQWxCLEVBQW9DO0FBQ2xDLGFBQUMsR0FBR3JDLHVCQUF1QnJELE9BQTNCO0FBQ0Q7O0FBRUQsZUFBS21HLGNBQUw7QUFDQSxlQUFLQyxhQUFMO0FBQ0Q7O0FBRUR2SCxxQkFBYW5CLEtBQWIsRUFBb0IsQ0FBQztBQUNuQjJCLGVBQUssZ0JBRGM7QUFFbkJULGlCQUFPLFNBQVN1SCxjQUFULEdBQTBCO0FBQy9CLGlCQUFLRSxRQUFMLEdBQWdCLElBQUk1QyxXQUFXekQsT0FBZixDQUF1QjtBQUNyQ3NHLHlCQUFXLEtBQUt4RixRQUFMLENBQWNrRSxhQURZO0FBRXJDckUsMEJBQVksS0FBS0csUUFBTCxDQUFjSCxVQUZXO0FBR3JDd0QscUNBQXVCLEtBQUtyRCxRQUFMLENBQWNxRCxxQkFIQTtBQUlyQ0UsNkJBQWUsS0FBS3ZELFFBQUwsQ0FBY3VELGFBSlE7QUFLckNrQyxzQkFBUSxLQUFLekYsUUFBTCxDQUFjb0UsVUFMZTtBQU1yQzFFLHlCQUFXLEtBQUtNLFFBQUwsQ0FBY04sU0FOWTtBQU9yQytELDZCQUFlLEtBQUt6RCxRQUFMLENBQWN5RCxhQVBRO0FBUXJDRSw2QkFBZSxLQUFLM0QsUUFBTCxDQUFjMkQsYUFSUTtBQVNyQ0UsK0JBQWlCLEtBQUs3RCxRQUFMLENBQWM2RDtBQVRNLGFBQXZCLENBQWhCO0FBV0Q7QUFka0IsU0FBRCxFQWVqQjtBQUNEdEYsZUFBSyxlQURKO0FBRURULGlCQUFPLFNBQVN3SCxhQUFULEdBQXlCO0FBQzlCLGlCQUFLckMsT0FBTCxHQUFlLElBQUlSLFVBQVV2RCxPQUFkLENBQXNCO0FBQ25Dc0Isa0JBQUksS0FBS29DLFNBRDBCO0FBRW5DMkMsd0JBQVUsS0FBS0EsUUFGb0I7QUFHbkNqQiwyQkFBYSxLQUFLdEUsUUFBTCxDQUFjc0UsV0FIUTtBQUluQ0Usc0JBQVEsS0FBS3hFLFFBQUwsQ0FBY3dFLE1BSmE7QUFLbkNFLHNCQUFRLEtBQUsxRSxRQUFMLENBQWMwRSxNQUxhO0FBTW5DViwrQkFBaUIsS0FBS2hFLFFBQUwsQ0FBY2dFLGVBTkk7QUFPbkNjLDBCQUFZLEtBQUs5RSxRQUFMLENBQWM4RSxVQVBTO0FBUW5DRSwwQkFBWSxLQUFLaEYsUUFBTCxDQUFjZ0YsVUFSUztBQVNuQ0UsZ0NBQWtCLEtBQUtsRixRQUFMLENBQWNrRixnQkFURztBQVVuQ0UsZ0NBQWtCLEtBQUtwRixRQUFMLENBQWNvRixnQkFWRztBQVduQzFGLHlCQUFXLEtBQUtNLFFBQUwsQ0FBY04sU0FYVTtBQVluQ0csMEJBQVksS0FBS0csUUFBTCxDQUFjSDtBQVpTLGFBQXRCLENBQWY7QUFjRDtBQWpCQSxTQWZpQixFQWlDakI7QUFDRHRCLGVBQUssaUJBREo7QUFFRFQsaUJBQU8sU0FBUzRILGVBQVQsQ0FBeUJDLFFBQXpCLEVBQW1DO0FBQ3hDLGlCQUFLSixRQUFMLENBQWNLLFlBQWQsQ0FBMkJELFFBQTNCO0FBQ0Q7QUFKQSxTQWpDaUIsRUFzQ2pCO0FBQ0RwSCxlQUFLLFNBREo7QUFFRFQsaUJBQU8sU0FBUytILE9BQVQsR0FBbUI7QUFDeEIsaUJBQUs1QyxPQUFMLENBQWE2QyxPQUFiLEdBQXVCLEtBQXZCO0FBQ0Q7QUFKQSxTQXRDaUIsRUEyQ2pCO0FBQ0R2SCxlQUFLLFFBREo7QUFFRFQsaUJBQU8sU0FBU2lJLE1BQVQsR0FBa0I7QUFDdkIsaUJBQUs5QyxPQUFMLENBQWE2QyxPQUFiLEdBQXVCLElBQXZCO0FBQ0Q7QUFKQSxTQTNDaUIsRUFnRGpCO0FBQ0R2SCxlQUFLLFdBREo7QUFFRHlILGVBQUssU0FBU0EsR0FBVCxHQUFlO0FBQ2xCLG1CQUFPLEtBQUtULFFBQUwsQ0FBYy9GLFNBQXJCO0FBQ0Q7QUFKQSxTQWhEaUIsRUFxRGpCO0FBQ0RqQixlQUFLLFlBREo7QUFFRHlILGVBQUssU0FBU0EsR0FBVCxHQUFlO0FBQ2xCLG1CQUFPLEtBQUtoRyxRQUFMLENBQWNILFVBQXJCO0FBQ0QsV0FKQTtBQUtEb0csZUFBSyxTQUFTQSxHQUFULENBQWFDLEVBQWIsRUFBaUI7QUFDcEIsaUJBQUtsRyxRQUFMLENBQWNILFVBQWQsR0FBMkJxRyxFQUEzQjtBQUNBLGlCQUFLWCxRQUFMLENBQWN2RixRQUFkLENBQXVCSCxVQUF2QixHQUFvQ3FHLEVBQXBDO0FBQ0Q7QUFSQSxTQXJEaUIsQ0FBcEI7O0FBZ0VBLGVBQU90SixLQUFQO0FBQ0QsT0E5SWdCLEVBQWpCO0FBZ0pDLEtBckwrQyxFQXFMOUMsRUFBQyxhQUFZLENBQWIsRUFBZSxjQUFhLENBQTVCLEVBQThCLDBCQUF5QixDQUF2RCxFQUF5RCxjQUFhLENBQXRFLEVBckw4QyxDQWhJd3ZCLEVBcVQ1dEIsR0FBRSxDQUFDLFVBQVNTLE9BQVQsRUFBaUJoQixNQUFqQixFQUF3QkQsT0FBeEIsRUFBZ0M7QUFDL0c7O0FBRUF3QixhQUFPQyxjQUFQLENBQXNCekIsT0FBdEIsRUFBK0IsWUFBL0IsRUFBNkM7QUFDM0MwQixlQUFPO0FBRG9DLE9BQTdDOztBQUlBLFVBQUlDLGVBQWUsWUFBWTtBQUM3QixpQkFBU0MsZ0JBQVQsQ0FBMEJDLE1BQTFCLEVBQWtDQyxLQUFsQyxFQUF5QztBQUN2QyxlQUFLLElBQUlaLElBQUksQ0FBYixFQUFnQkEsSUFBSVksTUFBTVAsTUFBMUIsRUFBa0NMLEdBQWxDLEVBQXVDO0FBQ3JDLGdCQUFJYSxhQUFhRCxNQUFNWixDQUFOLENBQWpCLENBQTBCYSxXQUFXQyxVQUFYLEdBQXdCRCxXQUFXQyxVQUFYLElBQXlCLEtBQWpELENBQXVERCxXQUFXRSxZQUFYLEdBQTBCLElBQTFCLENBQStCLElBQUksV0FBV0YsVUFBZixFQUEyQkEsV0FBV0csUUFBWCxHQUFzQixJQUF0QixDQUEyQlYsT0FBT0MsY0FBUCxDQUFzQkksTUFBdEIsRUFBOEJFLFdBQVdJLEdBQXpDLEVBQThDSixVQUE5QztBQUN2SztBQUNGLGdCQUFPLFVBQVVLLFdBQVYsRUFBdUJDLFVBQXZCLEVBQW1DQyxXQUFuQyxFQUFnRDtBQUN0RCxjQUFJRCxVQUFKLEVBQWdCVCxpQkFBaUJRLFlBQVlHLFNBQTdCLEVBQXdDRixVQUF4QyxFQUFvRCxJQUFJQyxXQUFKLEVBQWlCVixpQkFBaUJRLFdBQWpCLEVBQThCRSxXQUE5QixFQUEyQyxPQUFPRixXQUFQO0FBQ2pJLFNBRkE7QUFHRixPQVJrQixFQUFuQjs7QUFVQSxVQUFJSSxrQkFBa0J2QixRQUFRLHVCQUFSLENBQXRCOztBQUVBLFVBQUl3QixtQkFBbUJDLHVCQUF1QkYsZUFBdkIsQ0FBdkI7O0FBRUEsVUFBSXVILGVBQWU5SSxRQUFRLGVBQVIsQ0FBbkI7O0FBRUEsVUFBSStJLGdCQUFnQnRILHVCQUF1QnFILFlBQXZCLENBQXBCOztBQUVBLGVBQVNySCxzQkFBVCxDQUFnQ0UsR0FBaEMsRUFBcUM7QUFDbkMsZUFBT0EsT0FBT0EsSUFBSUMsVUFBWCxHQUF3QkQsR0FBeEIsR0FBOEIsRUFBRUUsU0FBU0YsR0FBWCxFQUFyQztBQUNEOztBQUVELGVBQVNHLGVBQVQsQ0FBeUJDLFFBQXpCLEVBQW1DWixXQUFuQyxFQUFnRDtBQUM5QyxZQUFJLEVBQUVZLG9CQUFvQlosV0FBdEIsQ0FBSixFQUF3QztBQUN0QyxnQkFBTSxJQUFJYSxTQUFKLENBQWMsbUNBQWQsQ0FBTjtBQUNEO0FBQ0Y7O0FBRUQsVUFBSWdILFVBQVUsWUFBWTtBQUN4QixpQkFBU0EsT0FBVCxHQUFtQjtBQUNqQixjQUFJOUcsVUFBVXVELFVBQVVuRixNQUFWLEdBQW1CLENBQW5CLElBQXdCbUYsVUFBVSxDQUFWLE1BQWlCbkQsU0FBekMsR0FBcURtRCxVQUFVLENBQVYsQ0FBckQsR0FBb0UsRUFBbEY7O0FBRUEzRCwwQkFBZ0IsSUFBaEIsRUFBc0JrSCxPQUF0Qjs7QUFFQUMsMkJBQWlCNUksSUFBakIsQ0FBc0IsSUFBdEI7O0FBRUEsY0FBSTZJLGNBQWNoSCxRQUFRaUIsRUFBMUI7QUFBQSxjQUNJQSxLQUFLK0YsZ0JBQWdCNUcsU0FBaEIsR0FBNEIsQ0FBQyxHQUFHZCxpQkFBaUJLLE9BQXJCLEdBQTVCLEdBQThEcUgsV0FEdkU7QUFBQSxjQUVJQyxvQkFBb0JqSCxRQUFRZ0csUUFGaEM7QUFBQSxjQUdJQSxXQUFXaUIsc0JBQXNCN0csU0FBdEIsR0FBa0MsQ0FBQyxHQUFHZCxpQkFBaUJLLE9BQXJCLEdBQWxDLEdBQW9Fc0gsaUJBSG5GO0FBQUEsY0FJSXpDLHdCQUF3QnhFLFFBQVF5RSxlQUpwQztBQUFBLGNBS0lBLGtCQUFrQkQsMEJBQTBCcEUsU0FBMUIsR0FBc0MsQ0FBQyxHQUFHZCxpQkFBaUJLLE9BQXJCLEdBQXRDLEdBQXdFNkUscUJBTDlGO0FBQUEsY0FNSU0sdUJBQXVCOUUsUUFBUStFLFdBTm5DO0FBQUEsY0FPSUEsY0FBY0QseUJBQXlCMUUsU0FBekIsR0FBcUMsQ0FBQyxHQUFHZCxpQkFBaUJLLE9BQXJCLEdBQXJDLEdBQXVFbUYsb0JBUHpGO0FBQUEsY0FRSUUsa0JBQWtCaEYsUUFBUWlGLE1BUjlCO0FBQUEsY0FTSUEsU0FBU0Qsb0JBQW9CNUUsU0FBcEIsR0FBZ0MsSUFBaEMsR0FBdUM0RSxlQVRwRDtBQUFBLGNBVUlFLGtCQUFrQmxGLFFBQVFtRixNQVY5QjtBQUFBLGNBV0lBLFNBQVNELG9CQUFvQjlFLFNBQXBCLEdBQWdDLElBQWhDLEdBQXVDOEUsZUFYcEQ7QUFBQSxjQVlJSSxzQkFBc0J0RixRQUFRdUYsVUFabEM7QUFBQSxjQWFJQSxhQUFhRCx3QkFBd0JsRixTQUF4QixHQUFvQyxDQUFwQyxHQUF3Q2tGLG1CQWJ6RDtBQUFBLGNBY0lFLHNCQUFzQnhGLFFBQVF5RixVQWRsQztBQUFBLGNBZUlBLGFBQWFELHdCQUF3QnBGLFNBQXhCLEdBQW9DLENBQXBDLEdBQXdDb0YsbUJBZnpEO0FBQUEsY0FnQklFLHdCQUF3QjFGLFFBQVEyRixnQkFoQnBDO0FBQUEsY0FpQklBLG1CQUFtQkQsMEJBQTBCdEYsU0FBMUIsR0FBc0MsQ0FBQyxHQUFHZCxpQkFBaUJLLE9BQXJCLEdBQXRDLEdBQXdFK0YscUJBakIvRjtBQUFBLGNBa0JJRSx3QkFBd0I1RixRQUFRNkYsZ0JBbEJwQztBQUFBLGNBbUJJQSxtQkFBbUJELDBCQUEwQnhGLFNBQTFCLEdBQXNDLENBQUMsR0FBR2QsaUJBQWlCSyxPQUFyQixHQUF0QyxHQUF3RWlHLHFCQW5CL0Y7QUFBQSxjQW9CSTFGLHFCQUFxQkYsUUFBUUcsU0FwQmpDO0FBQUEsY0FxQklBLFlBQVlELHVCQUF1QkUsU0FBdkIsR0FBbUMsSUFBbkMsR0FBMENGLGtCQXJCMUQ7QUFBQSxjQXNCSUcsc0JBQXNCTCxRQUFRTSxVQXRCbEM7QUFBQSxjQXVCSUEsYUFBYUQsd0JBQXdCRCxTQUF4QixHQUFvQyxDQUFDLEdBQUdkLGlCQUFpQkssT0FBckIsR0FBcEMsR0FBc0VVLG1CQXZCdkY7O0FBeUJBLGVBQUtJLFFBQUwsR0FBZ0IsRUFBRVEsSUFBSUEsRUFBTixFQUFVK0UsVUFBVUEsUUFBcEIsRUFBOEJ2QixpQkFBaUJBLGVBQS9DLEVBQWdFTSxhQUFhQSxXQUE3RSxFQUEwRkUsUUFBUUEsTUFBbEcsRUFBMEdFLFFBQVFBLE1BQWxILEVBQTBISSxZQUFZQSxVQUF0SSxFQUFrSkUsWUFBWUEsVUFBOUosRUFBMEtFLGtCQUFrQkEsZ0JBQTVMLEVBQThNRSxrQkFBa0JBLGdCQUFoTyxFQUFrUDFGLFdBQVdBLFNBQTdQLEVBQXdRRyxZQUFZQSxVQUFwUixFQUFoQjs7QUFFQSxjQUFJLEtBQUtHLFFBQUwsQ0FBY2tGLGdCQUFkLElBQWtDLEtBQUtsRixRQUFMLENBQWNvRixnQkFBcEQsRUFBc0U7QUFDcEUsaUJBQUtxQixXQUFMLEdBQW1CLElBQUlMLGNBQWNsSCxPQUFsQixDQUEwQjtBQUMzQ1EseUJBQVcsS0FBS00sUUFBTCxDQUFjTixTQURrQjtBQUUzQ0csMEJBQVksS0FBS0csUUFBTCxDQUFjSCxVQUZpQjtBQUczQ0UsMkJBQWEsS0FBS0MsUUFBTCxDQUFjUSxFQUFkLENBQWlCa0c7QUFIYSxhQUExQixDQUFuQjtBQUtEOztBQUVELGVBQUtaLE9BQUwsR0FBZSxJQUFmOztBQUVBLGVBQUthLFdBQUw7QUFDRDs7QUFFRDVJLHFCQUFhc0ksT0FBYixFQUFzQixDQUFDO0FBQ3JCOUgsZUFBSyxhQURnQjtBQUVyQlQsaUJBQU8sU0FBUzZJLFdBQVQsR0FBdUI7QUFDNUIsaUJBQUszRyxRQUFMLENBQWNRLEVBQWQsQ0FBaUJvRyxnQkFBakIsQ0FBa0MsWUFBbEMsRUFBZ0QsS0FBS0MsWUFBckQsRUFBbUUsS0FBbkU7QUFDQSxpQkFBSzdHLFFBQUwsQ0FBY1EsRUFBZCxDQUFpQm9HLGdCQUFqQixDQUFrQyxZQUFsQyxFQUFnRCxLQUFLRSxLQUFyRCxFQUE0RCxLQUE1RDtBQUNBLGlCQUFLOUcsUUFBTCxDQUFjUSxFQUFkLENBQWlCb0csZ0JBQWpCLENBQWtDLFdBQWxDLEVBQStDLEtBQUtHLGVBQXBELEVBQXFFLEtBQXJFOztBQUVBLGdCQUFJLEtBQUsvRyxRQUFMLENBQWNzRSxXQUFsQixFQUErQjtBQUM3QixtQkFBS3RFLFFBQUwsQ0FBY1EsRUFBZCxDQUFpQm9HLGdCQUFqQixDQUFrQyxZQUFsQyxFQUFnRCxLQUFLQyxZQUFyRCxFQUFtRSxLQUFuRTtBQUNBLG1CQUFLN0csUUFBTCxDQUFjUSxFQUFkLENBQWlCb0csZ0JBQWpCLENBQWtDLFVBQWxDLEVBQThDLEtBQUtFLEtBQW5ELEVBQTBELEtBQTFEO0FBQ0EsbUJBQUs5RyxRQUFMLENBQWNRLEVBQWQsQ0FBaUJvRyxnQkFBakIsQ0FBa0MsV0FBbEMsRUFBK0MsS0FBS0csZUFBcEQsRUFBcUUsS0FBckU7QUFDRDtBQUNGO0FBWm9CLFNBQUQsRUFhbkI7QUFDRHhJLGVBQUssZUFESjtBQUVEVCxpQkFBTyxTQUFTb0YsYUFBVCxHQUF5QjtBQUM5QixpQkFBS2xELFFBQUwsQ0FBY1EsRUFBZCxDQUFpQndHLG1CQUFqQixDQUFxQyxZQUFyQyxFQUFtRCxLQUFLSCxZQUF4RCxFQUFzRSxLQUF0RTtBQUNBLGlCQUFLN0csUUFBTCxDQUFjUSxFQUFkLENBQWlCd0csbUJBQWpCLENBQXFDLFlBQXJDLEVBQW1ELEtBQUtGLEtBQXhELEVBQStELEtBQS9EO0FBQ0EsaUJBQUs5RyxRQUFMLENBQWNRLEVBQWQsQ0FBaUJ3RyxtQkFBakIsQ0FBcUMsV0FBckMsRUFBa0QsS0FBS0QsZUFBdkQsRUFBd0UsS0FBeEU7O0FBRUEsZ0JBQUksS0FBSy9HLFFBQUwsQ0FBY3NFLFdBQWxCLEVBQStCO0FBQzdCLG1CQUFLdEUsUUFBTCxDQUFjUSxFQUFkLENBQWlCd0csbUJBQWpCLENBQXFDLFlBQXJDLEVBQW1ELEtBQUtILFlBQXhELEVBQXNFLEtBQXRFO0FBQ0EsbUJBQUs3RyxRQUFMLENBQWNRLEVBQWQsQ0FBaUJ3RyxtQkFBakIsQ0FBcUMsVUFBckMsRUFBaUQsS0FBS0YsS0FBdEQsRUFBNkQsS0FBN0Q7QUFDQSxtQkFBSzlHLFFBQUwsQ0FBY1EsRUFBZCxDQUFpQndHLG1CQUFqQixDQUFxQyxXQUFyQyxFQUFrRCxLQUFLRCxlQUF2RCxFQUF3RSxLQUF4RTtBQUNEO0FBQ0Y7QUFaQSxTQWJtQixFQTBCbkI7QUFDRHhJLGVBQUssV0FESjtBQUVEeUgsZUFBSyxTQUFTQSxHQUFULEdBQWU7QUFDbEIsbUJBQU8sS0FBS2hHLFFBQUwsQ0FBY3VGLFFBQWQsQ0FBdUIvRixTQUE5QjtBQUNEO0FBSkEsU0ExQm1CLENBQXRCOztBQWlDQSxlQUFPNkcsT0FBUDtBQUNELE9BbEZhLEVBQWQ7O0FBb0ZBLFVBQUlDLG1CQUFtQixTQUFTQSxnQkFBVCxHQUE0QjtBQUNqRCxZQUFJekQsUUFBUSxJQUFaOztBQUVBLGFBQUtnRSxZQUFMLEdBQW9CLFVBQVVoSyxDQUFWLEVBQWE7QUFDL0JBLFlBQUVvSyxjQUFGO0FBQ0FwRSxnQkFBTXFFLGFBQU4sR0FBc0JySyxDQUF0Qjs7QUFFQSxjQUFJQSxFQUFFc0ssSUFBRixJQUFVLFlBQVYsSUFBMEJ0RSxNQUFNN0MsUUFBTixDQUFlOEUsVUFBN0MsRUFBeUQ7QUFDdkRqQyxrQkFBTXVFLFlBQU4sR0FBcUJDLFdBQVd4RSxNQUFNeUUsS0FBakIsRUFBd0J6RSxNQUFNN0MsUUFBTixDQUFlOEUsVUFBdkMsQ0FBckI7QUFDRCxXQUZELE1BRU8sSUFBSWpDLE1BQU03QyxRQUFOLENBQWVnRixVQUFuQixFQUErQjtBQUNwQ25DLGtCQUFNdUUsWUFBTixHQUFxQkMsV0FBV3hFLE1BQU15RSxLQUFqQixFQUF3QnpFLE1BQU03QyxRQUFOLENBQWVnRixVQUF2QyxDQUFyQjtBQUNELFdBRk0sTUFFQTtBQUNMbkMsa0JBQU15RSxLQUFOO0FBQ0Q7QUFDRixTQVhEOztBQWFBLGFBQUtBLEtBQUwsR0FBYSxZQUFZO0FBQ3ZCLGNBQUksQ0FBQ3pFLE1BQU1pRCxPQUFYLEVBQW9CO0FBQ2xCO0FBQ0Q7O0FBRUQsY0FBSXRCLFNBQVMzQixNQUFNN0MsUUFBTixDQUFld0UsTUFBNUI7QUFDQSxjQUFJQSxVQUFVLE9BQU9BLE1BQVAsS0FBa0IsVUFBaEMsRUFBNEM7QUFDMUNBO0FBQ0Q7O0FBRUQzQixnQkFBTTdDLFFBQU4sQ0FBZXVGLFFBQWYsQ0FBd0IzRSxJQUF4QixDQUE2QmlDLE1BQU03QyxRQUFOLENBQWVRLEVBQWYsQ0FBa0IrRyxZQUFsQixDQUErQjFFLE1BQU03QyxRQUFOLENBQWVnRSxlQUE5QyxDQUE3QixFQUE2Rm5CLE1BQU03QyxRQUFOLENBQWVRLEVBQWYsQ0FBa0J3QixXQUEvRyxFQUE0SGEsTUFBTTdDLFFBQU4sQ0FBZVEsRUFBZixDQUFrQjJCLFlBQTlJOztBQUVBLGNBQUlVLE1BQU1xRSxhQUFWLEVBQXlCO0FBQ3ZCLGdCQUFJTSxpQkFBaUIzRSxNQUFNcUUsYUFBTixDQUFvQk8sT0FBekM7QUFDQSxnQkFBSUQsa0JBQWtCM0UsTUFBTTdDLFFBQU4sQ0FBZW9GLGdCQUFqQyxJQUFxRCxDQUFDb0MsY0FBRCxJQUFtQjNFLE1BQU03QyxRQUFOLENBQWVrRixnQkFBM0YsRUFBNkc7QUFDM0dyQyxvQkFBTTRELFdBQU4sQ0FBa0I3RixJQUFsQixDQUF1QmlDLE1BQU03QyxRQUFOLENBQWV1RixRQUFmLENBQXdCL0UsRUFBeEIsQ0FBMkJ3QixXQUFsRCxFQUErRGEsTUFBTTdDLFFBQU4sQ0FBZXVGLFFBQWYsQ0FBd0IvRSxFQUF4QixDQUEyQjJCLFlBQTFGO0FBQ0Q7QUFDRjs7QUFFRFUsZ0JBQU1rRSxlQUFOO0FBQ0QsU0FwQkQ7O0FBc0JBLGFBQUtELEtBQUwsR0FBYSxVQUFVakssQ0FBVixFQUFhO0FBQ3hCQSxZQUFFb0ssY0FBRjs7QUFFQXBFLGdCQUFNcUUsYUFBTixHQUFzQixJQUF0Qjs7QUFFQSxjQUFJckUsTUFBTXVFLFlBQVYsRUFBd0I7QUFDdEJNLHlCQUFhN0UsTUFBTXVFLFlBQW5CO0FBQ0Q7O0FBRUQsY0FBSXZFLE1BQU00RCxXQUFWLEVBQXVCO0FBQ3JCNUQsa0JBQU00RCxXQUFOLENBQWtCcEYsSUFBbEI7QUFDRDs7QUFFRCxjQUFJcUQsU0FBUzdCLE1BQU03QyxRQUFOLENBQWUwRSxNQUE1QjtBQUNBLGNBQUlBLFVBQVUsT0FBT0EsTUFBUCxLQUFrQixVQUFoQyxFQUE0QztBQUMxQ0E7QUFDRDs7QUFFRDdCLGdCQUFNN0MsUUFBTixDQUFldUYsUUFBZixDQUF3QmxFLElBQXhCO0FBQ0QsU0FuQkQ7O0FBcUJBLGFBQUswRixlQUFMLEdBQXVCLFVBQVVsSyxDQUFWLEVBQWE7QUFDbEMsY0FBSUEsQ0FBSixFQUFPO0FBQ0xBLGNBQUVvSyxjQUFGO0FBQ0FwRSxrQkFBTXFFLGFBQU4sR0FBc0JySyxDQUF0QjtBQUNELFdBSEQsTUFHTyxJQUFJZ0csTUFBTXFFLGFBQVYsRUFBeUI7QUFDOUJySyxnQkFBSWdHLE1BQU1xRSxhQUFWO0FBQ0QsV0FGTSxNQUVBO0FBQ0w7QUFDRDs7QUFFRCxjQUFJUyxZQUFZLEtBQUssQ0FBckI7QUFBQSxjQUNJQyxZQUFZLEtBQUssQ0FEckI7O0FBR0EsY0FBSS9LLEVBQUU0SyxPQUFOLEVBQWU7QUFDYixnQkFBSUksYUFBYWhMLEVBQUU0SyxPQUFGLENBQVUsQ0FBVixDQUFqQjtBQUNBRSx3QkFBWUUsV0FBV0MsT0FBdkI7QUFDQUYsd0JBQVlDLFdBQVdFLE9BQXZCO0FBQ0QsV0FKRCxNQUlPO0FBQ0xKLHdCQUFZOUssRUFBRWlMLE9BQWQ7QUFDQUYsd0JBQVkvSyxFQUFFa0wsT0FBZDtBQUNEOztBQUVELGNBQUl2SCxLQUFLcUMsTUFBTTdDLFFBQU4sQ0FBZVEsRUFBeEI7QUFDQSxjQUFJd0gsT0FBT3hILEdBQUc2QixxQkFBSCxFQUFYO0FBQ0EsY0FBSTRGLFVBQVVOLFlBQVlLLEtBQUtqRyxJQUEvQjtBQUNBLGNBQUltRyxVQUFVTixZQUFZSSxLQUFLOUYsR0FBL0I7O0FBRUEsY0FBSVQsb0JBQW9Cd0csVUFBVXBGLE1BQU03QyxRQUFOLENBQWVRLEVBQWYsQ0FBa0J3QixXQUFwRDtBQUNBLGNBQUlOLG9CQUFvQndHLFVBQVVyRixNQUFNN0MsUUFBTixDQUFlUSxFQUFmLENBQWtCMkIsWUFBcEQ7O0FBRUEsY0FBSVUsTUFBTTRELFdBQVYsRUFBdUI7QUFDckI1RCxrQkFBTTRELFdBQU4sQ0FBa0JqRixXQUFsQixDQUE4QkMsaUJBQTlCLEVBQWlEQyxpQkFBakQsRUFBb0VzRyxJQUFwRTtBQUNEOztBQUVEbkYsZ0JBQU03QyxRQUFOLENBQWV1RixRQUFmLENBQXdCL0QsV0FBeEIsQ0FBb0NDLGlCQUFwQyxFQUF1REMsaUJBQXZELEVBQTBFc0csSUFBMUU7QUFDRCxTQW5DRDtBQW9DRCxPQS9GRDs7QUFpR0E1TCxjQUFROEMsT0FBUixHQUFrQm1ILE9BQWxCO0FBRUMsS0ExTjZFLEVBME41RSxFQUFDLGlCQUFnQixDQUFqQixFQUFtQix5QkFBd0IsQ0FBM0MsRUExTjRFLENBclQwdEIsRUErZ0J2dkIsR0FBRSxDQUFDLFVBQVNoSixPQUFULEVBQWlCaEIsTUFBakIsRUFBd0JELE9BQXhCLEVBQWdDO0FBQ3BGOztBQUVBd0IsYUFBT0MsY0FBUCxDQUFzQnpCLE9BQXRCLEVBQStCLFlBQS9CLEVBQTZDO0FBQzNDMEIsZUFBTztBQURvQyxPQUE3Qzs7QUFJQSxVQUFJQyxlQUFlLFlBQVk7QUFDN0IsaUJBQVNDLGdCQUFULENBQTBCQyxNQUExQixFQUFrQ0MsS0FBbEMsRUFBeUM7QUFDdkMsZUFBSyxJQUFJWixJQUFJLENBQWIsRUFBZ0JBLElBQUlZLE1BQU1QLE1BQTFCLEVBQWtDTCxHQUFsQyxFQUF1QztBQUNyQyxnQkFBSWEsYUFBYUQsTUFBTVosQ0FBTixDQUFqQixDQUEwQmEsV0FBV0MsVUFBWCxHQUF3QkQsV0FBV0MsVUFBWCxJQUF5QixLQUFqRCxDQUF1REQsV0FBV0UsWUFBWCxHQUEwQixJQUExQixDQUErQixJQUFJLFdBQVdGLFVBQWYsRUFBMkJBLFdBQVdHLFFBQVgsR0FBc0IsSUFBdEIsQ0FBMkJWLE9BQU9DLGNBQVAsQ0FBc0JJLE1BQXRCLEVBQThCRSxXQUFXSSxHQUF6QyxFQUE4Q0osVUFBOUM7QUFDdks7QUFDRixnQkFBTyxVQUFVSyxXQUFWLEVBQXVCQyxVQUF2QixFQUFtQ0MsV0FBbkMsRUFBZ0Q7QUFDdEQsY0FBSUQsVUFBSixFQUFnQlQsaUJBQWlCUSxZQUFZRyxTQUE3QixFQUF3Q0YsVUFBeEMsRUFBb0QsSUFBSUMsV0FBSixFQUFpQlYsaUJBQWlCUSxXQUFqQixFQUE4QkUsV0FBOUIsRUFBMkMsT0FBT0YsV0FBUDtBQUNqSSxTQUZBO0FBR0YsT0FSa0IsRUFBbkI7O0FBVUEsVUFBSUksa0JBQWtCdkIsUUFBUSx1QkFBUixDQUF0Qjs7QUFFQSxVQUFJd0IsbUJBQW1CQyx1QkFBdUJGLGVBQXZCLENBQXZCOztBQUVBLFVBQUlHLE9BQU8xQixRQUFRLFlBQVIsQ0FBWDs7QUFFQSxlQUFTeUIsc0JBQVQsQ0FBZ0NFLEdBQWhDLEVBQXFDO0FBQ25DLGVBQU9BLE9BQU9BLElBQUlDLFVBQVgsR0FBd0JELEdBQXhCLEdBQThCLEVBQUVFLFNBQVNGLEdBQVgsRUFBckM7QUFDRDs7QUFFRCxlQUFTRyxlQUFULENBQXlCQyxRQUF6QixFQUFtQ1osV0FBbkMsRUFBZ0Q7QUFDOUMsWUFBSSxFQUFFWSxvQkFBb0JaLFdBQXRCLENBQUosRUFBd0M7QUFDdEMsZ0JBQU0sSUFBSWEsU0FBSixDQUFjLG1DQUFkLENBQU47QUFDRDtBQUNGOztBQUVEO0FBQ0E7QUFDQSxVQUFJOEksV0FBVzFILFNBQVNDLGFBQVQsQ0FBdUIsS0FBdkIsRUFBOEJNLEtBQTdDOztBQUVBLFVBQUlvSCxnQkFBZ0IsT0FBTzNILFFBQVAsS0FBb0IsV0FBcEIsR0FBa0MsS0FBbEMsR0FBMEMsZUFBZTBILFFBQWYsSUFBMkIscUJBQXFCQSxRQUE5Rzs7QUFFQSxVQUFJRSxXQUFXLFlBQVk7QUFDekIsaUJBQVNBLFFBQVQsR0FBb0I7QUFDbEIsY0FBSXhGLFFBQVEsSUFBWjs7QUFFQSxjQUFJdEQsVUFBVXVELFVBQVVuRixNQUFWLEdBQW1CLENBQW5CLElBQXdCbUYsVUFBVSxDQUFWLE1BQWlCbkQsU0FBekMsR0FBcURtRCxVQUFVLENBQVYsQ0FBckQsR0FBb0UsRUFBbEY7O0FBRUEzRCwwQkFBZ0IsSUFBaEIsRUFBc0JrSixRQUF0Qjs7QUFFQSxlQUFLQyxhQUFMLEdBQXFCLFlBQVk7QUFDL0J6RixrQkFBTXJDLEVBQU4sQ0FBU3dHLG1CQUFULENBQTZCLGNBQTdCLEVBQTZDbkUsTUFBTXlGLGFBQW5ELEVBQWtFLEtBQWxFO0FBQ0F6RixrQkFBTXJDLEVBQU4sQ0FBU3dHLG1CQUFULENBQTZCLG9CQUE3QixFQUFtRG5FLE1BQU15RixhQUF6RCxFQUF3RSxLQUF4RTs7QUFFQSxhQUFDLEdBQUd2SixLQUFLd0MsYUFBVCxFQUF3QnNCLE1BQU1yQyxFQUE5QixFQUFrQ3FDLE1BQU0wRixjQUF4QztBQUNELFdBTEQ7O0FBT0EsZUFBS0MsYUFBTCxHQUFxQixZQUFZO0FBQy9CM0Ysa0JBQU1yQyxFQUFOLENBQVN3RyxtQkFBVCxDQUE2QixjQUE3QixFQUE2Q25FLE1BQU0yRixhQUFuRCxFQUFrRSxLQUFsRTtBQUNBM0Ysa0JBQU1yQyxFQUFOLENBQVN3RyxtQkFBVCxDQUE2QixvQkFBN0IsRUFBbURuRSxNQUFNMkYsYUFBekQsRUFBd0UsS0FBeEU7O0FBRUEsYUFBQyxHQUFHekosS0FBS3dDLGFBQVQsRUFBd0JzQixNQUFNckMsRUFBOUIsRUFBa0NxQyxNQUFNNUMsV0FBeEM7QUFDQSxhQUFDLEdBQUdsQixLQUFLd0MsYUFBVCxFQUF3QnNCLE1BQU1yQyxFQUE5QixFQUFrQ3FDLE1BQU00RixjQUF4QztBQUNBLGFBQUMsR0FBRzFKLEtBQUt3QyxhQUFULEVBQXdCc0IsTUFBTXJDLEVBQTlCLEVBQWtDcUMsTUFBTTZGLGFBQXhDOztBQUVBN0Ysa0JBQU1yQyxFQUFOLENBQVNtSSxZQUFULENBQXNCLE9BQXRCLEVBQStCLEVBQS9COztBQUVBO0FBQ0E7QUFDQTtBQUNBLGdCQUFJOUYsTUFBTXJDLEVBQU4sQ0FBU29JLGFBQVQsS0FBMkIvRixNQUFNN0MsUUFBTixDQUFld0YsU0FBOUMsRUFBeUQ7QUFDdkQzQyxvQkFBTTdDLFFBQU4sQ0FBZXdGLFNBQWYsQ0FBeUJsRSxXQUF6QixDQUFxQ3VCLE1BQU1yQyxFQUEzQztBQUNELGFBRkQsTUFFTyxJQUFJcUMsTUFBTXJDLEVBQU4sQ0FBU29JLGFBQVQsS0FBMkIvRixNQUFNN0MsUUFBTixDQUFlNkQsZUFBOUMsRUFBK0Q7QUFDcEVoQixvQkFBTTdDLFFBQU4sQ0FBZTZELGVBQWYsQ0FBK0J2QyxXQUEvQixDQUEyQ3VCLE1BQU1yQyxFQUFqRDtBQUNEO0FBQ0YsV0FsQkQ7O0FBb0JBLGVBQUtxSSxXQUFMLEdBQW1CLFlBQVk7QUFDN0JoRyxrQkFBTWlHLEtBQU4sQ0FBWTlCLG1CQUFaLENBQWdDLE1BQWhDLEVBQXdDbkUsTUFBTWdHLFdBQTlDLEVBQTJELEtBQTNEO0FBQ0EsYUFBQyxHQUFHOUosS0FBS3dDLGFBQVQsRUFBd0JzQixNQUFNckMsRUFBOUIsRUFBa0NxQyxNQUFNa0csY0FBeEM7QUFDRCxXQUhEOztBQUtBLGVBQUt2SixTQUFMLEdBQWlCLEtBQWpCOztBQUVBLGNBQUl3SixxQkFBcUJ6SixRQUFRaUcsU0FBakM7QUFBQSxjQUNJQSxZQUFZd0QsdUJBQXVCckosU0FBdkIsR0FBbUMsSUFBbkMsR0FBMENxSixrQkFEMUQ7QUFBQSxjQUVJcEosc0JBQXNCTCxRQUFRTSxVQUZsQztBQUFBLGNBR0lBLGFBQWFELHdCQUF3QkQsU0FBeEIsR0FBb0MsQ0FBQyxHQUFHZCxpQkFBaUJLLE9BQXJCLEdBQXBDLEdBQXNFVSxtQkFIdkY7QUFBQSxjQUlJcUosa0JBQWtCMUosUUFBUWtHLE1BSjlCO0FBQUEsY0FLSUEsU0FBU3dELG9CQUFvQnRKLFNBQXBCLEdBQWdDLENBQUMsR0FBR2QsaUJBQWlCSyxPQUFyQixHQUFoQyxHQUFrRStKLGVBTC9FO0FBQUEsY0FNSXhKLHFCQUFxQkYsUUFBUUcsU0FOakM7QUFBQSxjQU9JQSxZQUFZRCx1QkFBdUJFLFNBQXZCLEdBQW1DLElBQW5DLEdBQTBDRixrQkFQMUQ7QUFBQSxjQVFJMkQsd0JBQXdCN0QsUUFBUThELHFCQVJwQztBQUFBLGNBU0lBLHdCQUF3QkQsMEJBQTBCekQsU0FBMUIsR0FBc0MsQ0FBQyxHQUFHZCxpQkFBaUJLLE9BQXJCLEdBQXRDLEdBQXdFa0UscUJBVHBHO0FBQUEsY0FVSUUsd0JBQXdCL0QsUUFBUWdFLGFBVnBDO0FBQUEsY0FXSUEsZ0JBQWdCRCwwQkFBMEIzRCxTQUExQixHQUFzQyxDQUFDLEdBQUdkLGlCQUFpQkssT0FBckIsR0FBdEMsR0FBd0VvRSxxQkFYNUY7QUFBQSxjQVlJRSx3QkFBd0JqRSxRQUFRa0UsYUFacEM7QUFBQSxjQWFJQSxnQkFBZ0JELDBCQUEwQjdELFNBQTFCLEdBQXNDLENBQXRDLEdBQTBDNkQscUJBYjlEO0FBQUEsY0FjSUUseUJBQXlCbkUsUUFBUW9FLGFBZHJDO0FBQUEsY0FlSUEsZ0JBQWdCRCwyQkFBMkIvRCxTQUEzQixHQUF1QyxDQUF2QyxHQUEyQytELHNCQWYvRDtBQUFBLGNBZ0JJRSx3QkFBd0JyRSxRQUFRc0UsZUFoQnBDO0FBQUEsY0FpQklBLGtCQUFrQkQsMEJBQTBCakUsU0FBMUIsR0FBc0NjLFNBQVNxRCxJQUEvQyxHQUFzREYscUJBakI1RTs7QUFtQkEsZUFBSzVELFFBQUwsR0FBZ0IsRUFBRXdGLFdBQVdBLFNBQWIsRUFBd0IzRixZQUFZQSxVQUFwQyxFQUFnRDRGLFFBQVFBLE1BQXhELEVBQWdFL0YsV0FBV0EsU0FBM0UsRUFBc0YyRCx1QkFBdUJBLHFCQUE3RyxFQUFvSUUsZUFBZUEsYUFBbkosRUFBa0tFLGVBQWVBLGFBQWpMLEVBQWdNRSxlQUFlQSxhQUEvTSxFQUE4TkUsaUJBQWlCQSxlQUEvTyxFQUFoQjs7QUFFQSxlQUFLNUQsV0FBTCxHQUFtQixLQUFLQyxhQUFMLENBQW1CLE1BQW5CLENBQW5CO0FBQ0EsZUFBS3FJLGNBQUwsR0FBc0IsS0FBS3JJLGFBQUwsQ0FBbUIsU0FBbkIsQ0FBdEI7QUFDQSxlQUFLdUksY0FBTCxHQUFzQixLQUFLdkksYUFBTCxDQUFtQixTQUFuQixDQUF0QjtBQUNBLGVBQUt3SSxhQUFMLEdBQXFCLEtBQUt4SSxhQUFMLENBQW1CLFFBQW5CLENBQXJCO0FBQ0EsZUFBSzZJLGNBQUwsR0FBc0IsS0FBSzdJLGFBQUwsQ0FBbUIsU0FBbkIsQ0FBdEI7O0FBRUEsZUFBS0MsYUFBTDtBQUNEOztBQUVEcEMscUJBQWFzSyxRQUFiLEVBQXVCLENBQUM7QUFDdEI5SixlQUFLLGVBRGlCO0FBRXRCVCxpQkFBTyxTQUFTb0MsYUFBVCxDQUF1QkUsTUFBdkIsRUFBK0I7QUFDcEMsZ0JBQUlDLFVBQVUsQ0FBQyxXQUFXRCxNQUFaLENBQWQ7O0FBRUEsZ0JBQUlFLEtBQUssS0FBS04sUUFBTCxDQUFjTixTQUF2QjtBQUNBLGdCQUFJWSxFQUFKLEVBQVE7QUFDTkQsc0JBQVFFLElBQVIsQ0FBYUQsS0FBSyxHQUFMLEdBQVdGLE1BQXhCO0FBQ0Q7O0FBRUQsbUJBQU9DLE9BQVA7QUFDRDtBQVhxQixTQUFELEVBWXBCO0FBQ0Q5QixlQUFLLGVBREo7QUFFRFQsaUJBQU8sU0FBU3FDLGFBQVQsR0FBeUI7QUFDOUIsaUJBQUtLLEVBQUwsR0FBVUMsU0FBU0MsYUFBVCxDQUF1QixLQUF2QixDQUFWO0FBQ0EsYUFBQyxHQUFHM0IsS0FBSzRCLFVBQVQsRUFBcUIsS0FBS0gsRUFBMUIsRUFBOEIsS0FBS04sYUFBTCxDQUFtQixXQUFuQixDQUE5Qjs7QUFFQSxnQkFBSWdKLFdBQVd6SSxTQUFTQyxhQUFULENBQXVCLEtBQXZCLENBQWY7QUFDQSxhQUFDLEdBQUczQixLQUFLNEIsVUFBVCxFQUFxQnVJLFFBQXJCLEVBQStCLEtBQUtoSixhQUFMLENBQW1CLGtCQUFuQixDQUEvQjtBQUNBLGlCQUFLTSxFQUFMLENBQVFPLFdBQVIsQ0FBb0JtSSxRQUFwQjs7QUFFQSxpQkFBS0osS0FBTCxHQUFhckksU0FBU0MsYUFBVCxDQUF1QixLQUF2QixDQUFiO0FBQ0EsaUJBQUtGLEVBQUwsQ0FBUU8sV0FBUixDQUFvQixLQUFLK0gsS0FBekI7QUFDRDtBQVpBLFNBWm9CLEVBeUJwQjtBQUNEdkssZUFBSyxjQURKO0FBRURULGlCQUFPLFNBQVM4SCxZQUFULENBQXNCRCxRQUF0QixFQUFnQztBQUNyQyxpQkFBS21ELEtBQUwsQ0FBV0gsWUFBWCxDQUF3QixLQUF4QixFQUErQmhELFFBQS9CO0FBQ0Q7QUFKQSxTQXpCb0IsRUE4QnBCO0FBQ0RwSCxlQUFLLGVBREo7QUFFRFQsaUJBQU8sU0FBU3FMLGFBQVQsQ0FBdUJDLFlBQXZCLEVBQXFDQyxhQUFyQyxFQUFvRDtBQUN6RCxpQkFBS1AsS0FBTCxDQUFXOUgsS0FBWCxDQUFpQkMsS0FBakIsR0FBeUJtSSxlQUFlLEtBQUtwSixRQUFMLENBQWNILFVBQTdCLEdBQTBDLElBQW5FO0FBQ0EsaUJBQUtpSixLQUFMLENBQVc5SCxLQUFYLENBQWlCSSxNQUFqQixHQUEwQmlJLGdCQUFnQixLQUFLckosUUFBTCxDQUFjSCxVQUE5QixHQUEyQyxJQUFyRTtBQUNEOztBQUVEO0FBQ0E7O0FBUkMsU0E5Qm9CLEVBd0NwQjtBQUNEdEIsZUFBSyxhQURKO0FBRURULGlCQUFPLFNBQVMwRCxXQUFULENBQXFCQyxpQkFBckIsRUFBd0NDLGlCQUF4QyxFQUEyREMsV0FBM0QsRUFBd0U7QUFDN0UsZ0JBQUlJLE9BQU8sRUFBRSxLQUFLK0csS0FBTCxDQUFXOUcsV0FBWCxHQUF5QlAsaUJBQXpCLEdBQTZDLEtBQUtqQixFQUFMLENBQVF3QixXQUFSLEdBQXNCLENBQXJFLENBQVg7QUFDQSxnQkFBSUUsTUFBTSxFQUFFLEtBQUs0RyxLQUFMLENBQVczRyxZQUFYLEdBQTBCVCxpQkFBMUIsR0FBOEMsS0FBS2xCLEVBQUwsQ0FBUTJCLFlBQVIsR0FBdUIsQ0FBdkUsQ0FBVjtBQUNBLGdCQUFJbUgsVUFBVSxFQUFFLEtBQUtSLEtBQUwsQ0FBVzlHLFdBQVgsR0FBeUIsS0FBS3hCLEVBQUwsQ0FBUXdCLFdBQW5DLENBQWQ7QUFDQSxnQkFBSXVILFNBQVMsRUFBRSxLQUFLVCxLQUFMLENBQVczRyxZQUFYLEdBQTBCLEtBQUszQixFQUFMLENBQVEyQixZQUFwQyxDQUFiOztBQUVBLGdCQUFJLEtBQUszQixFQUFMLENBQVFvSSxhQUFSLEtBQTBCLEtBQUs1SSxRQUFMLENBQWM2RCxlQUE1QyxFQUE2RDtBQUMzRDtBQUNBO0FBQ0E7QUFDQTtBQUNBLGtCQUFJMkYsVUFBVS9NLE9BQU9tRixXQUFyQjtBQUNBLGtCQUFJNkgsVUFBVWhOLE9BQU9vRixXQUFyQjs7QUFFQSxrQkFBSUMsYUFBYUgsWUFBWUksSUFBWixHQUFtQk4sb0JBQW9CRSxZQUFZVixLQUFuRCxHQUEyRCxLQUFLVCxFQUFMLENBQVF3QixXQUFSLEdBQXNCLENBQWpGLEdBQXFGLEtBQUtoQyxRQUFMLENBQWN5RCxhQUFuRyxHQUFtSCtGLE9BQXBJO0FBQ0Esa0JBQUl2SCxZQUFZTixZQUFZTyxHQUFaLEdBQWtCUixvQkFBb0JDLFlBQVlQLE1BQWxELEdBQTJELEtBQUtaLEVBQUwsQ0FBUTJCLFlBQVIsR0FBdUIsQ0FBbEYsR0FBc0YsS0FBS25DLFFBQUwsQ0FBYzJELGFBQXBHLEdBQW9IOEYsT0FBcEk7O0FBRUEsa0JBQUksS0FBS3pKLFFBQUwsQ0FBY3VELGFBQWxCLEVBQWlDO0FBQy9CLG9CQUFJbkIsU0FBUyxLQUFLNUIsRUFBTCxDQUFRNkIscUJBQVIsRUFBYjs7QUFFQSxvQkFBSVAsYUFBYUgsWUFBWUksSUFBWixHQUFtQnlILE9BQXBDLEVBQTZDO0FBQzNDMUgsK0JBQWFILFlBQVlJLElBQVosR0FBbUJ5SCxPQUFoQztBQUNELGlCQUZELE1BRU8sSUFBSTFILGFBQWEsS0FBS3RCLEVBQUwsQ0FBUXdCLFdBQXJCLEdBQW1DTCxZQUFZSSxJQUFaLEdBQW1CSixZQUFZVixLQUEvQixHQUF1Q3VJLE9BQTlFLEVBQXVGO0FBQzVGMUgsK0JBQWFILFlBQVlJLElBQVosR0FBbUJKLFlBQVlWLEtBQS9CLEdBQXVDLEtBQUtULEVBQUwsQ0FBUXdCLFdBQS9DLEdBQTZEd0gsT0FBMUU7QUFDRDs7QUFFRCxvQkFBSXZILFlBQVlOLFlBQVlPLEdBQVosR0FBa0J1SCxPQUFsQyxFQUEyQztBQUN6Q3hILDhCQUFZTixZQUFZTyxHQUFaLEdBQWtCdUgsT0FBOUI7QUFDRCxpQkFGRCxNQUVPLElBQUl4SCxZQUFZLEtBQUt6QixFQUFMLENBQVEyQixZQUFwQixHQUFtQ1IsWUFBWU8sR0FBWixHQUFrQlAsWUFBWVAsTUFBOUIsR0FBdUNxSSxPQUE5RSxFQUF1RjtBQUM1RnhILDhCQUFZTixZQUFZTyxHQUFaLEdBQWtCUCxZQUFZUCxNQUE5QixHQUF1QyxLQUFLWixFQUFMLENBQVEyQixZQUEvQyxHQUE4RHNILE9BQTFFO0FBQ0Q7QUFDRjs7QUFFRCxtQkFBS2pKLEVBQUwsQ0FBUVEsS0FBUixDQUFjZSxJQUFkLEdBQXFCRCxhQUFhLElBQWxDO0FBQ0EsbUJBQUt0QixFQUFMLENBQVFRLEtBQVIsQ0FBY2tCLEdBQWQsR0FBb0JELFlBQVksSUFBaEM7QUFDRDs7QUFFRCxnQkFBSSxDQUFDLEtBQUtqQyxRQUFMLENBQWNxRCxxQkFBbkIsRUFBMEM7QUFDeEMsa0JBQUl0QixPQUFPLENBQVgsRUFBYztBQUNaQSx1QkFBTyxDQUFQO0FBQ0QsZUFGRCxNQUVPLElBQUlBLE9BQU91SCxPQUFYLEVBQW9CO0FBQ3pCdkgsdUJBQU91SCxPQUFQO0FBQ0Q7O0FBRUQsa0JBQUlwSCxNQUFNLENBQVYsRUFBYTtBQUNYQSxzQkFBTSxDQUFOO0FBQ0QsZUFGRCxNQUVPLElBQUlBLE1BQU1xSCxNQUFWLEVBQWtCO0FBQ3ZCckgsc0JBQU1xSCxNQUFOO0FBQ0Q7QUFDRjs7QUFFRCxpQkFBS1QsS0FBTCxDQUFXOUgsS0FBWCxDQUFpQjBJLFNBQWpCLEdBQTZCLGVBQWUzSCxJQUFmLEdBQXNCLE1BQXRCLEdBQStCRyxHQUEvQixHQUFxQyxLQUFsRTtBQUNBLGlCQUFLNEcsS0FBTCxDQUFXOUgsS0FBWCxDQUFpQjJJLGVBQWpCLEdBQW1DLGVBQWU1SCxJQUFmLEdBQXNCLE1BQXRCLEdBQStCRyxHQUEvQixHQUFxQyxLQUF4RTtBQUNEO0FBdkRBLFNBeENvQixFQWdHcEI7QUFDRDNELGVBQUssaUNBREo7QUFFRFQsaUJBQU8sU0FBUzhMLCtCQUFULEdBQTJDO0FBQ2hELGlCQUFLcEosRUFBTCxDQUFRd0csbUJBQVIsQ0FBNEIsY0FBNUIsRUFBNEMsS0FBS3NCLGFBQWpELEVBQWdFLEtBQWhFO0FBQ0EsaUJBQUs5SCxFQUFMLENBQVF3RyxtQkFBUixDQUE0QixjQUE1QixFQUE0QyxLQUFLd0IsYUFBakQsRUFBZ0UsS0FBaEU7QUFDQSxpQkFBS2hJLEVBQUwsQ0FBUXdHLG1CQUFSLENBQTRCLG9CQUE1QixFQUFrRCxLQUFLc0IsYUFBdkQsRUFBc0UsS0FBdEU7QUFDQSxpQkFBSzlILEVBQUwsQ0FBUXdHLG1CQUFSLENBQTRCLG9CQUE1QixFQUFrRCxLQUFLd0IsYUFBdkQsRUFBc0UsS0FBdEU7QUFDQSxhQUFDLEdBQUd6SixLQUFLd0MsYUFBVCxFQUF3QixLQUFLZixFQUE3QixFQUFpQyxLQUFLUCxXQUF0QztBQUNBLGFBQUMsR0FBR2xCLEtBQUt3QyxhQUFULEVBQXdCLEtBQUtmLEVBQTdCLEVBQWlDLEtBQUtpSSxjQUF0QztBQUNEO0FBVEEsU0FoR29CLEVBMEdwQjtBQUNEbEssZUFBSyxNQURKO0FBRURULGlCQUFPLFNBQVM4QyxJQUFULENBQWMrRSxRQUFkLEVBQXdCeUQsWUFBeEIsRUFBc0NDLGFBQXRDLEVBQXFEO0FBQzFELGlCQUFLTywrQkFBTDtBQUNBLGlCQUFLcEssU0FBTCxHQUFpQixJQUFqQjs7QUFFQSxhQUFDLEdBQUdULEtBQUs0QixVQUFULEVBQXFCLEtBQUtILEVBQTFCLEVBQThCLEtBQUtQLFdBQW5DO0FBQ0EsYUFBQyxHQUFHbEIsS0FBSzRCLFVBQVQsRUFBcUIsS0FBS0gsRUFBMUIsRUFBOEIsS0FBS3VJLGNBQW5DOztBQUVBLGlCQUFLRCxLQUFMLENBQVdsQyxnQkFBWCxDQUE0QixNQUE1QixFQUFvQyxLQUFLaUMsV0FBekMsRUFBc0QsS0FBdEQ7QUFDQSxpQkFBS2pELFlBQUwsQ0FBa0JELFFBQWxCO0FBQ0EsaUJBQUt3RCxhQUFMLENBQW1CQyxZQUFuQixFQUFpQ0MsYUFBakM7O0FBRUEsZ0JBQUksS0FBS1EsU0FBVCxFQUFvQjtBQUNsQixtQkFBS0MsV0FBTDtBQUNELGFBRkQsTUFFTztBQUNMLG1CQUFLQyxnQkFBTDtBQUNEOztBQUVELGdCQUFJM0IsYUFBSixFQUFtQjtBQUNqQixtQkFBSzVILEVBQUwsQ0FBUW9HLGdCQUFSLENBQXlCLGNBQXpCLEVBQXlDLEtBQUswQixhQUE5QyxFQUE2RCxLQUE3RDtBQUNBLG1CQUFLOUgsRUFBTCxDQUFRb0csZ0JBQVIsQ0FBeUIsb0JBQXpCLEVBQStDLEtBQUswQixhQUFwRCxFQUFtRSxLQUFuRTtBQUNBLGVBQUMsR0FBR3ZKLEtBQUs0QixVQUFULEVBQXFCLEtBQUtILEVBQTFCLEVBQThCLEtBQUsrSCxjQUFuQztBQUNEO0FBQ0Y7QUF4QkEsU0ExR29CLEVBbUlwQjtBQUNEaEssZUFBSyxhQURKO0FBRURULGlCQUFPLFNBQVNnTSxXQUFULEdBQXVCO0FBQzVCLGlCQUFLOUosUUFBTCxDQUFjNkQsZUFBZCxDQUE4QjlDLFdBQTlCLENBQTBDLEtBQUtQLEVBQS9DO0FBQ0EsYUFBQyxHQUFHekIsS0FBSzRCLFVBQVQsRUFBcUIsS0FBS0gsRUFBMUIsRUFBOEIsS0FBS2tJLGFBQW5DO0FBQ0Q7QUFMQSxTQW5Jb0IsRUF5SXBCO0FBQ0RuSyxlQUFLLGtCQURKO0FBRURULGlCQUFPLFNBQVNpTSxnQkFBVCxHQUE0QjtBQUNqQyxpQkFBSy9KLFFBQUwsQ0FBY3dGLFNBQWQsQ0FBd0J6RSxXQUF4QixDQUFvQyxLQUFLUCxFQUF6QztBQUNEO0FBSkEsU0F6SW9CLEVBOElwQjtBQUNEakMsZUFBSyxNQURKO0FBRURULGlCQUFPLFNBQVN1RCxJQUFULEdBQWdCO0FBQ3JCLGlCQUFLdUksK0JBQUw7QUFDQSxpQkFBS3BLLFNBQUwsR0FBaUIsS0FBakI7O0FBRUEsZ0JBQUk0SSxhQUFKLEVBQW1CO0FBQ2pCLG1CQUFLNUgsRUFBTCxDQUFRb0csZ0JBQVIsQ0FBeUIsY0FBekIsRUFBeUMsS0FBSzRCLGFBQTlDLEVBQTZELEtBQTdEO0FBQ0EsbUJBQUtoSSxFQUFMLENBQVFvRyxnQkFBUixDQUF5QixvQkFBekIsRUFBK0MsS0FBSzRCLGFBQXBELEVBQW1FLEtBQW5FO0FBQ0EsZUFBQyxHQUFHekosS0FBSzRCLFVBQVQsRUFBcUIsS0FBS0gsRUFBMUIsRUFBOEIsS0FBS2lJLGNBQW5DO0FBQ0QsYUFKRCxNQUlPO0FBQ0wsZUFBQyxHQUFHMUosS0FBS3dDLGFBQVQsRUFBd0IsS0FBS2YsRUFBN0IsRUFBaUMsS0FBS1AsV0FBdEM7QUFDQSxlQUFDLEdBQUdsQixLQUFLd0MsYUFBVCxFQUF3QixLQUFLZixFQUE3QixFQUFpQyxLQUFLa0ksYUFBdEM7QUFDRDtBQUNGO0FBZEEsU0E5SW9CLEVBNkpwQjtBQUNEbkssZUFBSyxXQURKO0FBRUR5SCxlQUFLLFNBQVNBLEdBQVQsR0FBZTtBQUNsQixnQkFBSVAsU0FBUyxLQUFLekYsUUFBTCxDQUFjeUYsTUFBM0I7O0FBRUEsbUJBQU9BLFdBQVcsSUFBWCxJQUFtQixPQUFPQSxNQUFQLEtBQWtCLFFBQWxCLElBQThCaEosT0FBT3VOLFVBQVAsSUFBcUJ2RSxNQUE3RTtBQUNEO0FBTkEsU0E3Sm9CLENBQXZCOztBQXNLQSxlQUFPNEMsUUFBUDtBQUNELE9BL09jLEVBQWY7O0FBaVBBak0sY0FBUThDLE9BQVIsR0FBa0JtSixRQUFsQjtBQUVDLEtBMVJrRCxFQTBSakQsRUFBQyxjQUFhLENBQWQsRUFBZ0IseUJBQXdCLENBQXhDLEVBMVJpRCxDQS9nQnF2QixFQXl5QjF2QixHQUFFLENBQUMsVUFBU2hMLE9BQVQsRUFBaUJoQixNQUFqQixFQUF3QkQsT0FBeEIsRUFBZ0M7QUFDakY7O0FBRUF3QixhQUFPQyxjQUFQLENBQXNCekIsT0FBdEIsRUFBK0IsWUFBL0IsRUFBNkM7QUFDM0MwQixlQUFPO0FBRG9DLE9BQTdDO0FBR0ExQixjQUFROEMsT0FBUixHQUFrQitLLG9CQUFsQjtBQUNBLFVBQUlDLFFBQVEsMHBCQUFaOztBQUVBLGVBQVNELG9CQUFULEdBQWdDO0FBQzlCLFlBQUl4SixTQUFTMEosYUFBVCxDQUF1QixvQkFBdkIsQ0FBSixFQUFrRDtBQUNoRDtBQUNEOztBQUVELFlBQUlDLFVBQVUzSixTQUFTQyxhQUFULENBQXVCLE9BQXZCLENBQWQ7QUFDQTBKLGdCQUFRakQsSUFBUixHQUFlLFVBQWY7QUFDQWlELGdCQUFRQyxTQUFSLENBQWtCQyxHQUFsQixDQUFzQixtQkFBdEI7O0FBRUFGLGdCQUFRckosV0FBUixDQUFvQk4sU0FBUzhKLGNBQVQsQ0FBd0JMLEtBQXhCLENBQXBCOztBQUVBLFlBQUlNLE9BQU8vSixTQUFTK0osSUFBcEI7QUFDQUEsYUFBS0MsWUFBTCxDQUFrQkwsT0FBbEIsRUFBMkJJLEtBQUtFLFVBQWhDO0FBQ0Q7QUFFQSxLQXhCK0MsRUF3QjlDLEVBeEI4QyxDQXp5Qnd2QixFQWkwQmx5QixHQUFFLENBQUMsVUFBU3JOLE9BQVQsRUFBaUJoQixNQUFqQixFQUF3QkQsT0FBeEIsRUFBZ0M7QUFDekM7O0FBRUEsVUFBSXVPLFdBQVcsT0FBT0MsTUFBUCxLQUFrQixVQUFsQixJQUFnQyxTQUFPQSxPQUFPQyxRQUFkLE1BQTJCLFFBQTNELEdBQXNFLFVBQVU3TCxHQUFWLEVBQWU7QUFBRSxzQkFBY0EsR0FBZCwwQ0FBY0EsR0FBZDtBQUFvQixPQUEzRyxHQUE4RyxVQUFVQSxHQUFWLEVBQWU7QUFBRSxlQUFPQSxPQUFPLE9BQU80TCxNQUFQLEtBQWtCLFVBQXpCLElBQXVDNUwsSUFBSThMLFdBQUosS0FBb0JGLE1BQTNELElBQXFFNUwsUUFBUTRMLE9BQU9qTSxTQUFwRixHQUFnRyxRQUFoRyxVQUFrSEssR0FBbEgsMENBQWtIQSxHQUFsSCxDQUFQO0FBQStILE9BQTdROztBQUVBcEIsYUFBT0MsY0FBUCxDQUFzQnpCLE9BQXRCLEVBQStCLFlBQS9CLEVBQTZDO0FBQzNDMEIsZUFBTztBQURvQyxPQUE3Qzs7QUFJQSxVQUFJaU4sVUFBVSxPQUFPSCxNQUFQLEtBQWtCLFVBQWxCLElBQWdDRCxTQUFTQyxPQUFPQyxRQUFoQixNQUE4QixRQUE5RCxHQUF5RSxVQUFVN0wsR0FBVixFQUFlO0FBQ3BHLGVBQU8sT0FBT0EsR0FBUCxLQUFlLFdBQWYsR0FBNkIsV0FBN0IsR0FBMkMyTCxTQUFTM0wsR0FBVCxDQUFsRDtBQUNELE9BRmEsR0FFVixVQUFVQSxHQUFWLEVBQWU7QUFDakIsZUFBT0EsT0FBTyxPQUFPNEwsTUFBUCxLQUFrQixVQUF6QixJQUF1QzVMLElBQUk4TCxXQUFKLEtBQW9CRixNQUEzRCxJQUFxRTVMLFFBQVE0TCxPQUFPak0sU0FBcEYsR0FBZ0csUUFBaEcsR0FBMkcsT0FBT0ssR0FBUCxLQUFlLFdBQWYsR0FBNkIsV0FBN0IsR0FBMkMyTCxTQUFTM0wsR0FBVCxDQUE3SjtBQUNELE9BSkQ7O0FBTUE1QyxjQUFRK0csWUFBUixHQUF1QkEsWUFBdkI7QUFDQS9HLGNBQVF1RSxVQUFSLEdBQXFCQSxVQUFyQjtBQUNBdkUsY0FBUW1GLGFBQVIsR0FBd0JBLGFBQXhCO0FBQ0E7QUFDQTtBQUNBLFVBQUl5SixZQUFZLENBQUMsT0FBT0MsV0FBUCxLQUF1QixXQUF2QixHQUFxQyxXQUFyQyxHQUFtREYsUUFBUUUsV0FBUixDQUFwRCxNQUE4RSxRQUE5Rjs7QUFFQSxlQUFTOUgsWUFBVCxDQUFzQm5FLEdBQXRCLEVBQTJCO0FBQ3pCLGVBQU9nTSxZQUFZaE0sZUFBZWlNLFdBQTNCLEdBQXlDak0sT0FBTyxDQUFDLE9BQU9BLEdBQVAsS0FBZSxXQUFmLEdBQTZCLFdBQTdCLEdBQTJDK0wsUUFBUS9MLEdBQVIsQ0FBNUMsTUFBOEQsUUFBckUsSUFBaUZBLFFBQVEsSUFBekYsSUFBaUdBLElBQUlrTSxRQUFKLEtBQWlCLENBQWxILElBQXVILE9BQU9sTSxJQUFJbU0sUUFBWCxLQUF3QixRQUEvTDtBQUNEOztBQUVELGVBQVN4SyxVQUFULENBQW9CSCxFQUFwQixFQUF3QjRLLFVBQXhCLEVBQW9DO0FBQ2xDQSxtQkFBV0MsT0FBWCxDQUFtQixVQUFVQyxTQUFWLEVBQXFCO0FBQ3RDOUssYUFBRzZKLFNBQUgsQ0FBYUMsR0FBYixDQUFpQmdCLFNBQWpCO0FBQ0QsU0FGRDtBQUdEOztBQUVELGVBQVMvSixhQUFULENBQXVCZixFQUF2QixFQUEyQjRLLFVBQTNCLEVBQXVDO0FBQ3JDQSxtQkFBV0MsT0FBWCxDQUFtQixVQUFVQyxTQUFWLEVBQXFCO0FBQ3RDOUssYUFBRzZKLFNBQUgsQ0FBYWtCLE1BQWIsQ0FBb0JELFNBQXBCO0FBQ0QsU0FGRDtBQUdEO0FBRUEsS0F0Q08sRUFzQ04sRUF0Q00sQ0FqMEJneUIsRUF1MkJseUIsR0FBRSxDQUFDLFVBQVNqTyxPQUFULEVBQWlCaEIsTUFBakIsRUFBd0JELE9BQXhCLEVBQWdDO0FBQ3pDOztBQUVBd0IsYUFBT0MsY0FBUCxDQUFzQnpCLE9BQXRCLEVBQStCLFlBQS9CLEVBQTZDO0FBQzNDMEIsZUFBTztBQURvQyxPQUE3QztBQUdBMUIsY0FBUThDLE9BQVIsR0FBa0JzTSxjQUFsQjtBQUNBLGVBQVNBLGNBQVQsR0FBMEI7QUFDeEIsY0FBTSxJQUFJak8sS0FBSixDQUFVLG1CQUFWLENBQU47QUFDRDtBQUVBLEtBWE8sRUFXTixFQVhNLENBdjJCZ3lCLEVBQTNiLEVBazNCdFcsRUFsM0JzVyxFQWszQm5XLENBQUMsQ0FBRCxDQWwzQm1XLEVBazNCOVYsQ0FsM0I4VixDQUFQO0FBbTNCclcsQ0FuM0JEIiwiZmlsZSI6InZlbmRvci9EcmlmdC5qcyIsInNvdXJjZXNDb250ZW50IjpbIihmdW5jdGlvbihmKXtpZih0eXBlb2YgZXhwb3J0cz09PVwib2JqZWN0XCImJnR5cGVvZiBtb2R1bGUhPT1cInVuZGVmaW5lZFwiKXttb2R1bGUuZXhwb3J0cz1mKCl9ZWxzZSBpZih0eXBlb2YgZGVmaW5lPT09XCJmdW5jdGlvblwiJiZkZWZpbmUuYW1kKXtkZWZpbmUoW10sZil9ZWxzZXt2YXIgZztpZih0eXBlb2Ygd2luZG93IT09XCJ1bmRlZmluZWRcIil7Zz13aW5kb3d9ZWxzZSBpZih0eXBlb2YgZ2xvYmFsIT09XCJ1bmRlZmluZWRcIil7Zz1nbG9iYWx9ZWxzZSBpZih0eXBlb2Ygc2VsZiE9PVwidW5kZWZpbmVkXCIpe2c9c2VsZn1lbHNle2c9dGhpc31nLkRyaWZ0ID0gZigpfX0pKGZ1bmN0aW9uKCl7dmFyIGRlZmluZSxtb2R1bGUsZXhwb3J0cztyZXR1cm4gKGZ1bmN0aW9uIGUodCxuLHIpe2Z1bmN0aW9uIHMobyx1KXtpZighbltvXSl7aWYoIXRbb10pe3ZhciBhPXR5cGVvZiByZXF1aXJlPT1cImZ1bmN0aW9uXCImJnJlcXVpcmU7aWYoIXUmJmEpcmV0dXJuIGEobywhMCk7aWYoaSlyZXR1cm4gaShvLCEwKTt2YXIgZj1uZXcgRXJyb3IoXCJDYW5ub3QgZmluZCBtb2R1bGUgJ1wiK28rXCInXCIpO3Rocm93IGYuY29kZT1cIk1PRFVMRV9OT1RfRk9VTkRcIixmfXZhciBsPW5bb109e2V4cG9ydHM6e319O3Rbb11bMF0uY2FsbChsLmV4cG9ydHMsZnVuY3Rpb24oZSl7dmFyIG49dFtvXVsxXVtlXTtyZXR1cm4gcyhuP246ZSl9LGwsbC5leHBvcnRzLGUsdCxuLHIpfXJldHVybiBuW29dLmV4cG9ydHN9dmFyIGk9dHlwZW9mIHJlcXVpcmU9PVwiZnVuY3Rpb25cIiYmcmVxdWlyZTtmb3IodmFyIG89MDtvPHIubGVuZ3RoO28rKylzKHJbb10pO3JldHVybiBzfSkoezE6W2Z1bmN0aW9uKHJlcXVpcmUsbW9kdWxlLGV4cG9ydHMpe1xuJ3VzZSBzdHJpY3QnO1xuXG5PYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgXCJfX2VzTW9kdWxlXCIsIHtcbiAgdmFsdWU6IHRydWVcbn0pO1xuXG52YXIgX2NyZWF0ZUNsYXNzID0gZnVuY3Rpb24gKCkge1xuICBmdW5jdGlvbiBkZWZpbmVQcm9wZXJ0aWVzKHRhcmdldCwgcHJvcHMpIHtcbiAgICBmb3IgKHZhciBpID0gMDsgaSA8IHByb3BzLmxlbmd0aDsgaSsrKSB7XG4gICAgICB2YXIgZGVzY3JpcHRvciA9IHByb3BzW2ldO2Rlc2NyaXB0b3IuZW51bWVyYWJsZSA9IGRlc2NyaXB0b3IuZW51bWVyYWJsZSB8fCBmYWxzZTtkZXNjcmlwdG9yLmNvbmZpZ3VyYWJsZSA9IHRydWU7aWYgKFwidmFsdWVcIiBpbiBkZXNjcmlwdG9yKSBkZXNjcmlwdG9yLndyaXRhYmxlID0gdHJ1ZTtPYmplY3QuZGVmaW5lUHJvcGVydHkodGFyZ2V0LCBkZXNjcmlwdG9yLmtleSwgZGVzY3JpcHRvcik7XG4gICAgfVxuICB9cmV0dXJuIGZ1bmN0aW9uIChDb25zdHJ1Y3RvciwgcHJvdG9Qcm9wcywgc3RhdGljUHJvcHMpIHtcbiAgICBpZiAocHJvdG9Qcm9wcykgZGVmaW5lUHJvcGVydGllcyhDb25zdHJ1Y3Rvci5wcm90b3R5cGUsIHByb3RvUHJvcHMpO2lmIChzdGF0aWNQcm9wcykgZGVmaW5lUHJvcGVydGllcyhDb25zdHJ1Y3Rvciwgc3RhdGljUHJvcHMpO3JldHVybiBDb25zdHJ1Y3RvcjtcbiAgfTtcbn0oKTtcblxudmFyIF90aHJvd0lmTWlzc2luZyA9IHJlcXVpcmUoJy4vdXRpbC90aHJvd0lmTWlzc2luZycpO1xuXG52YXIgX3Rocm93SWZNaXNzaW5nMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3Rocm93SWZNaXNzaW5nKTtcblxudmFyIF9kb20gPSByZXF1aXJlKCcuL3V0aWwvZG9tJyk7XG5cbmZ1bmN0aW9uIF9pbnRlcm9wUmVxdWlyZURlZmF1bHQob2JqKSB7XG4gIHJldHVybiBvYmogJiYgb2JqLl9fZXNNb2R1bGUgPyBvYmogOiB7IGRlZmF1bHQ6IG9iaiB9O1xufVxuXG5mdW5jdGlvbiBfY2xhc3NDYWxsQ2hlY2soaW5zdGFuY2UsIENvbnN0cnVjdG9yKSB7XG4gIGlmICghKGluc3RhbmNlIGluc3RhbmNlb2YgQ29uc3RydWN0b3IpKSB7XG4gICAgdGhyb3cgbmV3IFR5cGVFcnJvcihcIkNhbm5vdCBjYWxsIGEgY2xhc3MgYXMgYSBmdW5jdGlvblwiKTtcbiAgfVxufVxuXG52YXIgQm91bmRpbmdCb3ggPSBmdW5jdGlvbiAoKSB7XG4gIGZ1bmN0aW9uIEJvdW5kaW5nQm94KG9wdGlvbnMpIHtcbiAgICBfY2xhc3NDYWxsQ2hlY2sodGhpcywgQm91bmRpbmdCb3gpO1xuXG4gICAgdGhpcy5pc1Nob3dpbmcgPSBmYWxzZTtcblxuICAgIHZhciBfb3B0aW9ucyRuYW1lc3BhY2UgPSBvcHRpb25zLm5hbWVzcGFjZSxcbiAgICAgICAgbmFtZXNwYWNlID0gX29wdGlvbnMkbmFtZXNwYWNlID09PSB1bmRlZmluZWQgPyBudWxsIDogX29wdGlvbnMkbmFtZXNwYWNlLFxuICAgICAgICBfb3B0aW9ucyR6b29tRmFjdG9yID0gb3B0aW9ucy56b29tRmFjdG9yLFxuICAgICAgICB6b29tRmFjdG9yID0gX29wdGlvbnMkem9vbUZhY3RvciA9PT0gdW5kZWZpbmVkID8gKDAsIF90aHJvd0lmTWlzc2luZzIuZGVmYXVsdCkoKSA6IF9vcHRpb25zJHpvb21GYWN0b3IsXG4gICAgICAgIF9vcHRpb25zJGNvbnRhaW5lckVsID0gb3B0aW9ucy5jb250YWluZXJFbCxcbiAgICAgICAgY29udGFpbmVyRWwgPSBfb3B0aW9ucyRjb250YWluZXJFbCA9PT0gdW5kZWZpbmVkID8gKDAsIF90aHJvd0lmTWlzc2luZzIuZGVmYXVsdCkoKSA6IF9vcHRpb25zJGNvbnRhaW5lckVsO1xuXG4gICAgdGhpcy5zZXR0aW5ncyA9IHsgbmFtZXNwYWNlOiBuYW1lc3BhY2UsIHpvb21GYWN0b3I6IHpvb21GYWN0b3IsIGNvbnRhaW5lckVsOiBjb250YWluZXJFbCB9O1xuXG4gICAgdGhpcy5vcGVuQ2xhc3NlcyA9IHRoaXMuX2J1aWxkQ2xhc3Nlcygnb3BlbicpO1xuXG4gICAgdGhpcy5fYnVpbGRFbGVtZW50KCk7XG4gIH1cblxuICBfY3JlYXRlQ2xhc3MoQm91bmRpbmdCb3gsIFt7XG4gICAga2V5OiAnX2J1aWxkQ2xhc3NlcycsXG4gICAgdmFsdWU6IGZ1bmN0aW9uIF9idWlsZENsYXNzZXMoc3VmZml4KSB7XG4gICAgICB2YXIgY2xhc3NlcyA9IFsnZHJpZnQtJyArIHN1ZmZpeF07XG5cbiAgICAgIHZhciBucyA9IHRoaXMuc2V0dGluZ3MubmFtZXNwYWNlO1xuICAgICAgaWYgKG5zKSB7XG4gICAgICAgIGNsYXNzZXMucHVzaChucyArICctJyArIHN1ZmZpeCk7XG4gICAgICB9XG5cbiAgICAgIHJldHVybiBjbGFzc2VzO1xuICAgIH1cbiAgfSwge1xuICAgIGtleTogJ19idWlsZEVsZW1lbnQnLFxuICAgIHZhbHVlOiBmdW5jdGlvbiBfYnVpbGRFbGVtZW50KCkge1xuICAgICAgdGhpcy5lbCA9IGRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoJ2RpdicpO1xuICAgICAgKDAsIF9kb20uYWRkQ2xhc3NlcykodGhpcy5lbCwgdGhpcy5fYnVpbGRDbGFzc2VzKCdib3VuZGluZy1ib3gnKSk7XG4gICAgfVxuICB9LCB7XG4gICAga2V5OiAnc2hvdycsXG4gICAgdmFsdWU6IGZ1bmN0aW9uIHNob3coem9vbVBhbmVXaWR0aCwgem9vbVBhbmVIZWlnaHQpIHtcbiAgICAgIHRoaXMuaXNTaG93aW5nID0gdHJ1ZTtcblxuICAgICAgdGhpcy5zZXR0aW5ncy5jb250YWluZXJFbC5hcHBlbmRDaGlsZCh0aGlzLmVsKTtcblxuICAgICAgdmFyIHN0eWxlID0gdGhpcy5lbC5zdHlsZTtcbiAgICAgIHN0eWxlLndpZHRoID0gTWF0aC5yb3VuZCh6b29tUGFuZVdpZHRoIC8gdGhpcy5zZXR0aW5ncy56b29tRmFjdG9yKSArICdweCc7XG4gICAgICBzdHlsZS5oZWlnaHQgPSBNYXRoLnJvdW5kKHpvb21QYW5lSGVpZ2h0IC8gdGhpcy5zZXR0aW5ncy56b29tRmFjdG9yKSArICdweCc7XG5cbiAgICAgICgwLCBfZG9tLmFkZENsYXNzZXMpKHRoaXMuZWwsIHRoaXMub3BlbkNsYXNzZXMpO1xuICAgIH1cbiAgfSwge1xuICAgIGtleTogJ2hpZGUnLFxuICAgIHZhbHVlOiBmdW5jdGlvbiBoaWRlKCkge1xuICAgICAgaWYgKHRoaXMuaXNTaG93aW5nKSB7XG4gICAgICAgIHRoaXMuc2V0dGluZ3MuY29udGFpbmVyRWwucmVtb3ZlQ2hpbGQodGhpcy5lbCk7XG4gICAgICB9XG5cbiAgICAgIHRoaXMuaXNTaG93aW5nID0gZmFsc2U7XG5cbiAgICAgICgwLCBfZG9tLnJlbW92ZUNsYXNzZXMpKHRoaXMuZWwsIHRoaXMub3BlbkNsYXNzZXMpO1xuICAgIH1cbiAgfSwge1xuICAgIGtleTogJ3NldFBvc2l0aW9uJyxcbiAgICB2YWx1ZTogZnVuY3Rpb24gc2V0UG9zaXRpb24ocGVyY2VudGFnZU9mZnNldFgsIHBlcmNlbnRhZ2VPZmZzZXRZLCB0cmlnZ2VyUmVjdCkge1xuICAgICAgdmFyIHBhZ2VYT2Zmc2V0ID0gd2luZG93LnBhZ2VYT2Zmc2V0O1xuICAgICAgdmFyIHBhZ2VZT2Zmc2V0ID0gd2luZG93LnBhZ2VZT2Zmc2V0O1xuXG4gICAgICB2YXIgaW5saW5lTGVmdCA9IHRyaWdnZXJSZWN0LmxlZnQgKyBwZXJjZW50YWdlT2Zmc2V0WCAqIHRyaWdnZXJSZWN0LndpZHRoIC0gdGhpcy5lbC5jbGllbnRXaWR0aCAvIDIgKyBwYWdlWE9mZnNldDtcbiAgICAgIHZhciBpbmxpbmVUb3AgPSB0cmlnZ2VyUmVjdC50b3AgKyBwZXJjZW50YWdlT2Zmc2V0WSAqIHRyaWdnZXJSZWN0LmhlaWdodCAtIHRoaXMuZWwuY2xpZW50SGVpZ2h0IC8gMiArIHBhZ2VZT2Zmc2V0O1xuXG4gICAgICB2YXIgZWxSZWN0ID0gdGhpcy5lbC5nZXRCb3VuZGluZ0NsaWVudFJlY3QoKTtcblxuICAgICAgaWYgKGlubGluZUxlZnQgPCB0cmlnZ2VyUmVjdC5sZWZ0ICsgcGFnZVhPZmZzZXQpIHtcbiAgICAgICAgaW5saW5lTGVmdCA9IHRyaWdnZXJSZWN0LmxlZnQgKyBwYWdlWE9mZnNldDtcbiAgICAgIH0gZWxzZSBpZiAoaW5saW5lTGVmdCArIHRoaXMuZWwuY2xpZW50V2lkdGggPiB0cmlnZ2VyUmVjdC5sZWZ0ICsgdHJpZ2dlclJlY3Qud2lkdGggKyBwYWdlWE9mZnNldCkge1xuICAgICAgICBpbmxpbmVMZWZ0ID0gdHJpZ2dlclJlY3QubGVmdCArIHRyaWdnZXJSZWN0LndpZHRoIC0gdGhpcy5lbC5jbGllbnRXaWR0aCArIHBhZ2VYT2Zmc2V0O1xuICAgICAgfVxuXG4gICAgICBpZiAoaW5saW5lVG9wIDwgdHJpZ2dlclJlY3QudG9wICsgcGFnZVlPZmZzZXQpIHtcbiAgICAgICAgaW5saW5lVG9wID0gdHJpZ2dlclJlY3QudG9wICsgcGFnZVlPZmZzZXQ7XG4gICAgICB9IGVsc2UgaWYgKGlubGluZVRvcCArIHRoaXMuZWwuY2xpZW50SGVpZ2h0ID4gdHJpZ2dlclJlY3QudG9wICsgdHJpZ2dlclJlY3QuaGVpZ2h0ICsgcGFnZVlPZmZzZXQpIHtcbiAgICAgICAgaW5saW5lVG9wID0gdHJpZ2dlclJlY3QudG9wICsgdHJpZ2dlclJlY3QuaGVpZ2h0IC0gdGhpcy5lbC5jbGllbnRIZWlnaHQgKyBwYWdlWU9mZnNldDtcbiAgICAgIH1cblxuICAgICAgdGhpcy5lbC5zdHlsZS5sZWZ0ID0gaW5saW5lTGVmdCArICdweCc7XG4gICAgICB0aGlzLmVsLnN0eWxlLnRvcCA9IGlubGluZVRvcCArICdweCc7XG4gICAgfVxuICB9XSk7XG5cbiAgcmV0dXJuIEJvdW5kaW5nQm94O1xufSgpO1xuXG5leHBvcnRzLmRlZmF1bHQgPSBCb3VuZGluZ0JveDtcblxufSx7XCIuL3V0aWwvZG9tXCI6NixcIi4vdXRpbC90aHJvd0lmTWlzc2luZ1wiOjd9XSwyOltmdW5jdGlvbihyZXF1aXJlLG1vZHVsZSxleHBvcnRzKXtcbid1c2Ugc3RyaWN0JztcblxudmFyIF9jcmVhdGVDbGFzcyA9IGZ1bmN0aW9uICgpIHtcbiAgZnVuY3Rpb24gZGVmaW5lUHJvcGVydGllcyh0YXJnZXQsIHByb3BzKSB7XG4gICAgZm9yICh2YXIgaSA9IDA7IGkgPCBwcm9wcy5sZW5ndGg7IGkrKykge1xuICAgICAgdmFyIGRlc2NyaXB0b3IgPSBwcm9wc1tpXTtkZXNjcmlwdG9yLmVudW1lcmFibGUgPSBkZXNjcmlwdG9yLmVudW1lcmFibGUgfHwgZmFsc2U7ZGVzY3JpcHRvci5jb25maWd1cmFibGUgPSB0cnVlO2lmIChcInZhbHVlXCIgaW4gZGVzY3JpcHRvcikgZGVzY3JpcHRvci53cml0YWJsZSA9IHRydWU7T2JqZWN0LmRlZmluZVByb3BlcnR5KHRhcmdldCwgZGVzY3JpcHRvci5rZXksIGRlc2NyaXB0b3IpO1xuICAgIH1cbiAgfXJldHVybiBmdW5jdGlvbiAoQ29uc3RydWN0b3IsIHByb3RvUHJvcHMsIHN0YXRpY1Byb3BzKSB7XG4gICAgaWYgKHByb3RvUHJvcHMpIGRlZmluZVByb3BlcnRpZXMoQ29uc3RydWN0b3IucHJvdG90eXBlLCBwcm90b1Byb3BzKTtpZiAoc3RhdGljUHJvcHMpIGRlZmluZVByb3BlcnRpZXMoQ29uc3RydWN0b3IsIHN0YXRpY1Byb3BzKTtyZXR1cm4gQ29uc3RydWN0b3I7XG4gIH07XG59KCk7XG5cbnZhciBfZG9tID0gcmVxdWlyZSgnLi91dGlsL2RvbScpO1xuXG52YXIgX2luamVjdEJhc2VTdHlsZXNoZWV0ID0gcmVxdWlyZSgnLi9pbmplY3RCYXNlU3R5bGVzaGVldCcpO1xuXG52YXIgX2luamVjdEJhc2VTdHlsZXNoZWV0MiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2luamVjdEJhc2VTdHlsZXNoZWV0KTtcblxudmFyIF9UcmlnZ2VyID0gcmVxdWlyZSgnLi9UcmlnZ2VyJyk7XG5cbnZhciBfVHJpZ2dlcjIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9UcmlnZ2VyKTtcblxudmFyIF9ab29tUGFuZSA9IHJlcXVpcmUoJy4vWm9vbVBhbmUnKTtcblxudmFyIF9ab29tUGFuZTIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9ab29tUGFuZSk7XG5cbmZ1bmN0aW9uIF9pbnRlcm9wUmVxdWlyZURlZmF1bHQob2JqKSB7XG4gIHJldHVybiBvYmogJiYgb2JqLl9fZXNNb2R1bGUgPyBvYmogOiB7IGRlZmF1bHQ6IG9iaiB9O1xufVxuXG5mdW5jdGlvbiBfY2xhc3NDYWxsQ2hlY2soaW5zdGFuY2UsIENvbnN0cnVjdG9yKSB7XG4gIGlmICghKGluc3RhbmNlIGluc3RhbmNlb2YgQ29uc3RydWN0b3IpKSB7XG4gICAgdGhyb3cgbmV3IFR5cGVFcnJvcihcIkNhbm5vdCBjYWxsIGEgY2xhc3MgYXMgYSBmdW5jdGlvblwiKTtcbiAgfVxufVxuXG5tb2R1bGUuZXhwb3J0cyA9IGZ1bmN0aW9uICgpIHtcbiAgZnVuY3Rpb24gRHJpZnQodHJpZ2dlckVsKSB7XG4gICAgdmFyIF90aGlzID0gdGhpcztcblxuICAgIHZhciBvcHRpb25zID0gYXJndW1lbnRzLmxlbmd0aCA+IDEgJiYgYXJndW1lbnRzWzFdICE9PSB1bmRlZmluZWQgPyBhcmd1bWVudHNbMV0gOiB7fTtcblxuICAgIF9jbGFzc0NhbGxDaGVjayh0aGlzLCBEcmlmdCk7XG5cbiAgICB0aGlzLlZFUlNJT04gPSAnMS4yLjAnO1xuXG4gICAgdGhpcy5kZXN0cm95ID0gZnVuY3Rpb24gKCkge1xuICAgICAgX3RoaXMudHJpZ2dlci5fdW5iaW5kRXZlbnRzKCk7XG4gICAgfTtcblxuICAgIHRoaXMudHJpZ2dlckVsID0gdHJpZ2dlckVsO1xuXG4gICAgaWYgKCEoMCwgX2RvbS5pc0RPTUVsZW1lbnQpKHRoaXMudHJpZ2dlckVsKSkge1xuICAgICAgdGhyb3cgbmV3IFR5cGVFcnJvcignYG5ldyBEcmlmdGAgcmVxdWlyZXMgYSBET00gZWxlbWVudCBhcyBpdHMgZmlyc3QgYXJndW1lbnQuJyk7XG4gICAgfVxuXG4gICAgLy8gQSBiaXQgdW5leHBlY3RlZCBpZiB5b3UgaGF2ZW4ndCBzZWVuIHRoaXMgcGF0dGVybiBiZWZvcmUuXG4gICAgLy8gQmFzZWQgb24gdGhlIHBhdHRlcm4gaGVyZTpcbiAgICAvLyBodHRwczovL2dpdGh1Yi5jb20vZ2V0aWZ5L1lvdS1Eb250LUtub3ctSlMvYmxvYi9tYXN0ZXIvZXM2JTIwJiUyMGJleW9uZC9jaDIubWQjbmVzdGVkLWRlZmF1bHRzLWRlc3RydWN0dXJlZC1hbmQtcmVzdHJ1Y3R1cmVkXG4gICAgdmFyIF9vcHRpb25zJG5hbWVzcGFjZSA9IG9wdGlvbnMubmFtZXNwYWNlLFxuICAgICAgICBuYW1lc3BhY2UgPSBfb3B0aW9ucyRuYW1lc3BhY2UgPT09IHVuZGVmaW5lZCA/IG51bGwgOiBfb3B0aW9ucyRuYW1lc3BhY2UsXG4gICAgICAgIF9vcHRpb25zJHNob3dXaGl0ZXNwYSA9IG9wdGlvbnMuc2hvd1doaXRlc3BhY2VBdEVkZ2VzLFxuICAgICAgICBzaG93V2hpdGVzcGFjZUF0RWRnZXMgPSBfb3B0aW9ucyRzaG93V2hpdGVzcGEgPT09IHVuZGVmaW5lZCA/IGZhbHNlIDogX29wdGlvbnMkc2hvd1doaXRlc3BhLFxuICAgICAgICBfb3B0aW9ucyRjb250YWluSW5saW4gPSBvcHRpb25zLmNvbnRhaW5JbmxpbmUsXG4gICAgICAgIGNvbnRhaW5JbmxpbmUgPSBfb3B0aW9ucyRjb250YWluSW5saW4gPT09IHVuZGVmaW5lZCA/IGZhbHNlIDogX29wdGlvbnMkY29udGFpbklubGluLFxuICAgICAgICBfb3B0aW9ucyRpbmxpbmVPZmZzZXQgPSBvcHRpb25zLmlubGluZU9mZnNldFgsXG4gICAgICAgIGlubGluZU9mZnNldFggPSBfb3B0aW9ucyRpbmxpbmVPZmZzZXQgPT09IHVuZGVmaW5lZCA/IDAgOiBfb3B0aW9ucyRpbmxpbmVPZmZzZXQsXG4gICAgICAgIF9vcHRpb25zJGlubGluZU9mZnNldDIgPSBvcHRpb25zLmlubGluZU9mZnNldFksXG4gICAgICAgIGlubGluZU9mZnNldFkgPSBfb3B0aW9ucyRpbmxpbmVPZmZzZXQyID09PSB1bmRlZmluZWQgPyAwIDogX29wdGlvbnMkaW5saW5lT2Zmc2V0MixcbiAgICAgICAgX29wdGlvbnMkaW5saW5lQ29udGFpID0gb3B0aW9ucy5pbmxpbmVDb250YWluZXIsXG4gICAgICAgIGlubGluZUNvbnRhaW5lciA9IF9vcHRpb25zJGlubGluZUNvbnRhaSA9PT0gdW5kZWZpbmVkID8gZG9jdW1lbnQuYm9keSA6IF9vcHRpb25zJGlubGluZUNvbnRhaSxcbiAgICAgICAgX29wdGlvbnMkc291cmNlQXR0cmliID0gb3B0aW9ucy5zb3VyY2VBdHRyaWJ1dGUsXG4gICAgICAgIHNvdXJjZUF0dHJpYnV0ZSA9IF9vcHRpb25zJHNvdXJjZUF0dHJpYiA9PT0gdW5kZWZpbmVkID8gJ2RhdGEtem9vbScgOiBfb3B0aW9ucyRzb3VyY2VBdHRyaWIsXG4gICAgICAgIF9vcHRpb25zJHpvb21GYWN0b3IgPSBvcHRpb25zLnpvb21GYWN0b3IsXG4gICAgICAgIHpvb21GYWN0b3IgPSBfb3B0aW9ucyR6b29tRmFjdG9yID09PSB1bmRlZmluZWQgPyAzIDogX29wdGlvbnMkem9vbUZhY3RvcixcbiAgICAgICAgX29wdGlvbnMkcGFuZUNvbnRhaW5lID0gb3B0aW9ucy5wYW5lQ29udGFpbmVyLFxuICAgICAgICBwYW5lQ29udGFpbmVyID0gX29wdGlvbnMkcGFuZUNvbnRhaW5lID09PSB1bmRlZmluZWQgPyBkb2N1bWVudC5ib2R5IDogX29wdGlvbnMkcGFuZUNvbnRhaW5lLFxuICAgICAgICBfb3B0aW9ucyRpbmxpbmVQYW5lID0gb3B0aW9ucy5pbmxpbmVQYW5lLFxuICAgICAgICBpbmxpbmVQYW5lID0gX29wdGlvbnMkaW5saW5lUGFuZSA9PT0gdW5kZWZpbmVkID8gMzc1IDogX29wdGlvbnMkaW5saW5lUGFuZSxcbiAgICAgICAgX29wdGlvbnMkaGFuZGxlVG91Y2ggPSBvcHRpb25zLmhhbmRsZVRvdWNoLFxuICAgICAgICBoYW5kbGVUb3VjaCA9IF9vcHRpb25zJGhhbmRsZVRvdWNoID09PSB1bmRlZmluZWQgPyB0cnVlIDogX29wdGlvbnMkaGFuZGxlVG91Y2gsXG4gICAgICAgIF9vcHRpb25zJG9uU2hvdyA9IG9wdGlvbnMub25TaG93LFxuICAgICAgICBvblNob3cgPSBfb3B0aW9ucyRvblNob3cgPT09IHVuZGVmaW5lZCA/IG51bGwgOiBfb3B0aW9ucyRvblNob3csXG4gICAgICAgIF9vcHRpb25zJG9uSGlkZSA9IG9wdGlvbnMub25IaWRlLFxuICAgICAgICBvbkhpZGUgPSBfb3B0aW9ucyRvbkhpZGUgPT09IHVuZGVmaW5lZCA/IG51bGwgOiBfb3B0aW9ucyRvbkhpZGUsXG4gICAgICAgIF9vcHRpb25zJGluamVjdEJhc2VTdCA9IG9wdGlvbnMuaW5qZWN0QmFzZVN0eWxlcyxcbiAgICAgICAgaW5qZWN0QmFzZVN0eWxlcyA9IF9vcHRpb25zJGluamVjdEJhc2VTdCA9PT0gdW5kZWZpbmVkID8gdHJ1ZSA6IF9vcHRpb25zJGluamVjdEJhc2VTdCxcbiAgICAgICAgX29wdGlvbnMkaG92ZXJEZWxheSA9IG9wdGlvbnMuaG92ZXJEZWxheSxcbiAgICAgICAgaG92ZXJEZWxheSA9IF9vcHRpb25zJGhvdmVyRGVsYXkgPT09IHVuZGVmaW5lZCA/IDAgOiBfb3B0aW9ucyRob3ZlckRlbGF5LFxuICAgICAgICBfb3B0aW9ucyR0b3VjaERlbGF5ID0gb3B0aW9ucy50b3VjaERlbGF5LFxuICAgICAgICB0b3VjaERlbGF5ID0gX29wdGlvbnMkdG91Y2hEZWxheSA9PT0gdW5kZWZpbmVkID8gMCA6IF9vcHRpb25zJHRvdWNoRGVsYXksXG4gICAgICAgIF9vcHRpb25zJGhvdmVyQm91bmRpbiA9IG9wdGlvbnMuaG92ZXJCb3VuZGluZ0JveCxcbiAgICAgICAgaG92ZXJCb3VuZGluZ0JveCA9IF9vcHRpb25zJGhvdmVyQm91bmRpbiA9PT0gdW5kZWZpbmVkID8gZmFsc2UgOiBfb3B0aW9ucyRob3ZlckJvdW5kaW4sXG4gICAgICAgIF9vcHRpb25zJHRvdWNoQm91bmRpbiA9IG9wdGlvbnMudG91Y2hCb3VuZGluZ0JveCxcbiAgICAgICAgdG91Y2hCb3VuZGluZ0JveCA9IF9vcHRpb25zJHRvdWNoQm91bmRpbiA9PT0gdW5kZWZpbmVkID8gZmFsc2UgOiBfb3B0aW9ucyR0b3VjaEJvdW5kaW47XG5cbiAgICBpZiAoaW5saW5lUGFuZSAhPT0gdHJ1ZSAmJiAhKDAsIF9kb20uaXNET01FbGVtZW50KShwYW5lQ29udGFpbmVyKSkge1xuICAgICAgdGhyb3cgbmV3IFR5cGVFcnJvcignYHBhbmVDb250YWluZXJgIG11c3QgYmUgYSBET00gZWxlbWVudCB3aGVuIGBpbmxpbmVQYW5lICE9PSB0cnVlYCcpO1xuICAgIH1cbiAgICBpZiAoISgwLCBfZG9tLmlzRE9NRWxlbWVudCkoaW5saW5lQ29udGFpbmVyKSkge1xuICAgICAgdGhyb3cgbmV3IFR5cGVFcnJvcignYGlubGluZUNvbnRhaW5lcmAgbXVzdCBiZSBhIERPTSBlbGVtZW50Jyk7XG4gICAgfVxuXG4gICAgdGhpcy5zZXR0aW5ncyA9IHsgbmFtZXNwYWNlOiBuYW1lc3BhY2UsIHNob3dXaGl0ZXNwYWNlQXRFZGdlczogc2hvd1doaXRlc3BhY2VBdEVkZ2VzLCBjb250YWluSW5saW5lOiBjb250YWluSW5saW5lLCBpbmxpbmVPZmZzZXRYOiBpbmxpbmVPZmZzZXRYLCBpbmxpbmVPZmZzZXRZOiBpbmxpbmVPZmZzZXRZLCBpbmxpbmVDb250YWluZXI6IGlubGluZUNvbnRhaW5lciwgc291cmNlQXR0cmlidXRlOiBzb3VyY2VBdHRyaWJ1dGUsIHpvb21GYWN0b3I6IHpvb21GYWN0b3IsIHBhbmVDb250YWluZXI6IHBhbmVDb250YWluZXIsIGlubGluZVBhbmU6IGlubGluZVBhbmUsIGhhbmRsZVRvdWNoOiBoYW5kbGVUb3VjaCwgb25TaG93OiBvblNob3csIG9uSGlkZTogb25IaWRlLCBpbmplY3RCYXNlU3R5bGVzOiBpbmplY3RCYXNlU3R5bGVzLCBob3ZlckRlbGF5OiBob3ZlckRlbGF5LCB0b3VjaERlbGF5OiB0b3VjaERlbGF5LCBob3ZlckJvdW5kaW5nQm94OiBob3ZlckJvdW5kaW5nQm94LCB0b3VjaEJvdW5kaW5nQm94OiB0b3VjaEJvdW5kaW5nQm94IH07XG5cbiAgICBpZiAodGhpcy5zZXR0aW5ncy5pbmplY3RCYXNlU3R5bGVzKSB7XG4gICAgICAoMCwgX2luamVjdEJhc2VTdHlsZXNoZWV0Mi5kZWZhdWx0KSgpO1xuICAgIH1cblxuICAgIHRoaXMuX2J1aWxkWm9vbVBhbmUoKTtcbiAgICB0aGlzLl9idWlsZFRyaWdnZXIoKTtcbiAgfVxuXG4gIF9jcmVhdGVDbGFzcyhEcmlmdCwgW3tcbiAgICBrZXk6ICdfYnVpbGRab29tUGFuZScsXG4gICAgdmFsdWU6IGZ1bmN0aW9uIF9idWlsZFpvb21QYW5lKCkge1xuICAgICAgdGhpcy56b29tUGFuZSA9IG5ldyBfWm9vbVBhbmUyLmRlZmF1bHQoe1xuICAgICAgICBjb250YWluZXI6IHRoaXMuc2V0dGluZ3MucGFuZUNvbnRhaW5lcixcbiAgICAgICAgem9vbUZhY3RvcjogdGhpcy5zZXR0aW5ncy56b29tRmFjdG9yLFxuICAgICAgICBzaG93V2hpdGVzcGFjZUF0RWRnZXM6IHRoaXMuc2V0dGluZ3Muc2hvd1doaXRlc3BhY2VBdEVkZ2VzLFxuICAgICAgICBjb250YWluSW5saW5lOiB0aGlzLnNldHRpbmdzLmNvbnRhaW5JbmxpbmUsXG4gICAgICAgIGlubGluZTogdGhpcy5zZXR0aW5ncy5pbmxpbmVQYW5lLFxuICAgICAgICBuYW1lc3BhY2U6IHRoaXMuc2V0dGluZ3MubmFtZXNwYWNlLFxuICAgICAgICBpbmxpbmVPZmZzZXRYOiB0aGlzLnNldHRpbmdzLmlubGluZU9mZnNldFgsXG4gICAgICAgIGlubGluZU9mZnNldFk6IHRoaXMuc2V0dGluZ3MuaW5saW5lT2Zmc2V0WSxcbiAgICAgICAgaW5saW5lQ29udGFpbmVyOiB0aGlzLnNldHRpbmdzLmlubGluZUNvbnRhaW5lclxuICAgICAgfSk7XG4gICAgfVxuICB9LCB7XG4gICAga2V5OiAnX2J1aWxkVHJpZ2dlcicsXG4gICAgdmFsdWU6IGZ1bmN0aW9uIF9idWlsZFRyaWdnZXIoKSB7XG4gICAgICB0aGlzLnRyaWdnZXIgPSBuZXcgX1RyaWdnZXIyLmRlZmF1bHQoe1xuICAgICAgICBlbDogdGhpcy50cmlnZ2VyRWwsXG4gICAgICAgIHpvb21QYW5lOiB0aGlzLnpvb21QYW5lLFxuICAgICAgICBoYW5kbGVUb3VjaDogdGhpcy5zZXR0aW5ncy5oYW5kbGVUb3VjaCxcbiAgICAgICAgb25TaG93OiB0aGlzLnNldHRpbmdzLm9uU2hvdyxcbiAgICAgICAgb25IaWRlOiB0aGlzLnNldHRpbmdzLm9uSGlkZSxcbiAgICAgICAgc291cmNlQXR0cmlidXRlOiB0aGlzLnNldHRpbmdzLnNvdXJjZUF0dHJpYnV0ZSxcbiAgICAgICAgaG92ZXJEZWxheTogdGhpcy5zZXR0aW5ncy5ob3ZlckRlbGF5LFxuICAgICAgICB0b3VjaERlbGF5OiB0aGlzLnNldHRpbmdzLnRvdWNoRGVsYXksXG4gICAgICAgIGhvdmVyQm91bmRpbmdCb3g6IHRoaXMuc2V0dGluZ3MuaG92ZXJCb3VuZGluZ0JveCxcbiAgICAgICAgdG91Y2hCb3VuZGluZ0JveDogdGhpcy5zZXR0aW5ncy50b3VjaEJvdW5kaW5nQm94LFxuICAgICAgICBuYW1lc3BhY2U6IHRoaXMuc2V0dGluZ3MubmFtZXNwYWNlLFxuICAgICAgICB6b29tRmFjdG9yOiB0aGlzLnNldHRpbmdzLnpvb21GYWN0b3JcbiAgICAgIH0pO1xuICAgIH1cbiAgfSwge1xuICAgIGtleTogJ3NldFpvb21JbWFnZVVSTCcsXG4gICAgdmFsdWU6IGZ1bmN0aW9uIHNldFpvb21JbWFnZVVSTChpbWFnZVVSTCkge1xuICAgICAgdGhpcy56b29tUGFuZS5fc2V0SW1hZ2VVUkwoaW1hZ2VVUkwpO1xuICAgIH1cbiAgfSwge1xuICAgIGtleTogJ2Rpc2FibGUnLFxuICAgIHZhbHVlOiBmdW5jdGlvbiBkaXNhYmxlKCkge1xuICAgICAgdGhpcy50cmlnZ2VyLmVuYWJsZWQgPSBmYWxzZTtcbiAgICB9XG4gIH0sIHtcbiAgICBrZXk6ICdlbmFibGUnLFxuICAgIHZhbHVlOiBmdW5jdGlvbiBlbmFibGUoKSB7XG4gICAgICB0aGlzLnRyaWdnZXIuZW5hYmxlZCA9IHRydWU7XG4gICAgfVxuICB9LCB7XG4gICAga2V5OiAnaXNTaG93aW5nJyxcbiAgICBnZXQ6IGZ1bmN0aW9uIGdldCgpIHtcbiAgICAgIHJldHVybiB0aGlzLnpvb21QYW5lLmlzU2hvd2luZztcbiAgICB9XG4gIH0sIHtcbiAgICBrZXk6ICd6b29tRmFjdG9yJyxcbiAgICBnZXQ6IGZ1bmN0aW9uIGdldCgpIHtcbiAgICAgIHJldHVybiB0aGlzLnNldHRpbmdzLnpvb21GYWN0b3I7XG4gICAgfSxcbiAgICBzZXQ6IGZ1bmN0aW9uIHNldCh6Zikge1xuICAgICAgdGhpcy5zZXR0aW5ncy56b29tRmFjdG9yID0gemY7XG4gICAgICB0aGlzLnpvb21QYW5lLnNldHRpbmdzLnpvb21GYWN0b3IgPSB6ZjtcbiAgICB9XG4gIH1dKTtcblxuICByZXR1cm4gRHJpZnQ7XG59KCk7XG5cbn0se1wiLi9UcmlnZ2VyXCI6MyxcIi4vWm9vbVBhbmVcIjo0LFwiLi9pbmplY3RCYXNlU3R5bGVzaGVldFwiOjUsXCIuL3V0aWwvZG9tXCI6Nn1dLDM6W2Z1bmN0aW9uKHJlcXVpcmUsbW9kdWxlLGV4cG9ydHMpe1xuJ3VzZSBzdHJpY3QnO1xuXG5PYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgXCJfX2VzTW9kdWxlXCIsIHtcbiAgdmFsdWU6IHRydWVcbn0pO1xuXG52YXIgX2NyZWF0ZUNsYXNzID0gZnVuY3Rpb24gKCkge1xuICBmdW5jdGlvbiBkZWZpbmVQcm9wZXJ0aWVzKHRhcmdldCwgcHJvcHMpIHtcbiAgICBmb3IgKHZhciBpID0gMDsgaSA8IHByb3BzLmxlbmd0aDsgaSsrKSB7XG4gICAgICB2YXIgZGVzY3JpcHRvciA9IHByb3BzW2ldO2Rlc2NyaXB0b3IuZW51bWVyYWJsZSA9IGRlc2NyaXB0b3IuZW51bWVyYWJsZSB8fCBmYWxzZTtkZXNjcmlwdG9yLmNvbmZpZ3VyYWJsZSA9IHRydWU7aWYgKFwidmFsdWVcIiBpbiBkZXNjcmlwdG9yKSBkZXNjcmlwdG9yLndyaXRhYmxlID0gdHJ1ZTtPYmplY3QuZGVmaW5lUHJvcGVydHkodGFyZ2V0LCBkZXNjcmlwdG9yLmtleSwgZGVzY3JpcHRvcik7XG4gICAgfVxuICB9cmV0dXJuIGZ1bmN0aW9uIChDb25zdHJ1Y3RvciwgcHJvdG9Qcm9wcywgc3RhdGljUHJvcHMpIHtcbiAgICBpZiAocHJvdG9Qcm9wcykgZGVmaW5lUHJvcGVydGllcyhDb25zdHJ1Y3Rvci5wcm90b3R5cGUsIHByb3RvUHJvcHMpO2lmIChzdGF0aWNQcm9wcykgZGVmaW5lUHJvcGVydGllcyhDb25zdHJ1Y3Rvciwgc3RhdGljUHJvcHMpO3JldHVybiBDb25zdHJ1Y3RvcjtcbiAgfTtcbn0oKTtcblxudmFyIF90aHJvd0lmTWlzc2luZyA9IHJlcXVpcmUoJy4vdXRpbC90aHJvd0lmTWlzc2luZycpO1xuXG52YXIgX3Rocm93SWZNaXNzaW5nMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3Rocm93SWZNaXNzaW5nKTtcblxudmFyIF9Cb3VuZGluZ0JveCA9IHJlcXVpcmUoJy4vQm91bmRpbmdCb3gnKTtcblxudmFyIF9Cb3VuZGluZ0JveDIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9Cb3VuZGluZ0JveCk7XG5cbmZ1bmN0aW9uIF9pbnRlcm9wUmVxdWlyZURlZmF1bHQob2JqKSB7XG4gIHJldHVybiBvYmogJiYgb2JqLl9fZXNNb2R1bGUgPyBvYmogOiB7IGRlZmF1bHQ6IG9iaiB9O1xufVxuXG5mdW5jdGlvbiBfY2xhc3NDYWxsQ2hlY2soaW5zdGFuY2UsIENvbnN0cnVjdG9yKSB7XG4gIGlmICghKGluc3RhbmNlIGluc3RhbmNlb2YgQ29uc3RydWN0b3IpKSB7XG4gICAgdGhyb3cgbmV3IFR5cGVFcnJvcihcIkNhbm5vdCBjYWxsIGEgY2xhc3MgYXMgYSBmdW5jdGlvblwiKTtcbiAgfVxufVxuXG52YXIgVHJpZ2dlciA9IGZ1bmN0aW9uICgpIHtcbiAgZnVuY3Rpb24gVHJpZ2dlcigpIHtcbiAgICB2YXIgb3B0aW9ucyA9IGFyZ3VtZW50cy5sZW5ndGggPiAwICYmIGFyZ3VtZW50c1swXSAhPT0gdW5kZWZpbmVkID8gYXJndW1lbnRzWzBdIDoge307XG5cbiAgICBfY2xhc3NDYWxsQ2hlY2sodGhpcywgVHJpZ2dlcik7XG5cbiAgICBfaW5pdGlhbGlzZVByb3BzLmNhbGwodGhpcyk7XG5cbiAgICB2YXIgX29wdGlvbnMkZWwgPSBvcHRpb25zLmVsLFxuICAgICAgICBlbCA9IF9vcHRpb25zJGVsID09PSB1bmRlZmluZWQgPyAoMCwgX3Rocm93SWZNaXNzaW5nMi5kZWZhdWx0KSgpIDogX29wdGlvbnMkZWwsXG4gICAgICAgIF9vcHRpb25zJHpvb21QYW5lID0gb3B0aW9ucy56b29tUGFuZSxcbiAgICAgICAgem9vbVBhbmUgPSBfb3B0aW9ucyR6b29tUGFuZSA9PT0gdW5kZWZpbmVkID8gKDAsIF90aHJvd0lmTWlzc2luZzIuZGVmYXVsdCkoKSA6IF9vcHRpb25zJHpvb21QYW5lLFxuICAgICAgICBfb3B0aW9ucyRzb3VyY2VBdHRyaWIgPSBvcHRpb25zLnNvdXJjZUF0dHJpYnV0ZSxcbiAgICAgICAgc291cmNlQXR0cmlidXRlID0gX29wdGlvbnMkc291cmNlQXR0cmliID09PSB1bmRlZmluZWQgPyAoMCwgX3Rocm93SWZNaXNzaW5nMi5kZWZhdWx0KSgpIDogX29wdGlvbnMkc291cmNlQXR0cmliLFxuICAgICAgICBfb3B0aW9ucyRoYW5kbGVUb3VjaCA9IG9wdGlvbnMuaGFuZGxlVG91Y2gsXG4gICAgICAgIGhhbmRsZVRvdWNoID0gX29wdGlvbnMkaGFuZGxlVG91Y2ggPT09IHVuZGVmaW5lZCA/ICgwLCBfdGhyb3dJZk1pc3NpbmcyLmRlZmF1bHQpKCkgOiBfb3B0aW9ucyRoYW5kbGVUb3VjaCxcbiAgICAgICAgX29wdGlvbnMkb25TaG93ID0gb3B0aW9ucy5vblNob3csXG4gICAgICAgIG9uU2hvdyA9IF9vcHRpb25zJG9uU2hvdyA9PT0gdW5kZWZpbmVkID8gbnVsbCA6IF9vcHRpb25zJG9uU2hvdyxcbiAgICAgICAgX29wdGlvbnMkb25IaWRlID0gb3B0aW9ucy5vbkhpZGUsXG4gICAgICAgIG9uSGlkZSA9IF9vcHRpb25zJG9uSGlkZSA9PT0gdW5kZWZpbmVkID8gbnVsbCA6IF9vcHRpb25zJG9uSGlkZSxcbiAgICAgICAgX29wdGlvbnMkaG92ZXJEZWxheSA9IG9wdGlvbnMuaG92ZXJEZWxheSxcbiAgICAgICAgaG92ZXJEZWxheSA9IF9vcHRpb25zJGhvdmVyRGVsYXkgPT09IHVuZGVmaW5lZCA/IDAgOiBfb3B0aW9ucyRob3ZlckRlbGF5LFxuICAgICAgICBfb3B0aW9ucyR0b3VjaERlbGF5ID0gb3B0aW9ucy50b3VjaERlbGF5LFxuICAgICAgICB0b3VjaERlbGF5ID0gX29wdGlvbnMkdG91Y2hEZWxheSA9PT0gdW5kZWZpbmVkID8gMCA6IF9vcHRpb25zJHRvdWNoRGVsYXksXG4gICAgICAgIF9vcHRpb25zJGhvdmVyQm91bmRpbiA9IG9wdGlvbnMuaG92ZXJCb3VuZGluZ0JveCxcbiAgICAgICAgaG92ZXJCb3VuZGluZ0JveCA9IF9vcHRpb25zJGhvdmVyQm91bmRpbiA9PT0gdW5kZWZpbmVkID8gKDAsIF90aHJvd0lmTWlzc2luZzIuZGVmYXVsdCkoKSA6IF9vcHRpb25zJGhvdmVyQm91bmRpbixcbiAgICAgICAgX29wdGlvbnMkdG91Y2hCb3VuZGluID0gb3B0aW9ucy50b3VjaEJvdW5kaW5nQm94LFxuICAgICAgICB0b3VjaEJvdW5kaW5nQm94ID0gX29wdGlvbnMkdG91Y2hCb3VuZGluID09PSB1bmRlZmluZWQgPyAoMCwgX3Rocm93SWZNaXNzaW5nMi5kZWZhdWx0KSgpIDogX29wdGlvbnMkdG91Y2hCb3VuZGluLFxuICAgICAgICBfb3B0aW9ucyRuYW1lc3BhY2UgPSBvcHRpb25zLm5hbWVzcGFjZSxcbiAgICAgICAgbmFtZXNwYWNlID0gX29wdGlvbnMkbmFtZXNwYWNlID09PSB1bmRlZmluZWQgPyBudWxsIDogX29wdGlvbnMkbmFtZXNwYWNlLFxuICAgICAgICBfb3B0aW9ucyR6b29tRmFjdG9yID0gb3B0aW9ucy56b29tRmFjdG9yLFxuICAgICAgICB6b29tRmFjdG9yID0gX29wdGlvbnMkem9vbUZhY3RvciA9PT0gdW5kZWZpbmVkID8gKDAsIF90aHJvd0lmTWlzc2luZzIuZGVmYXVsdCkoKSA6IF9vcHRpb25zJHpvb21GYWN0b3I7XG5cbiAgICB0aGlzLnNldHRpbmdzID0geyBlbDogZWwsIHpvb21QYW5lOiB6b29tUGFuZSwgc291cmNlQXR0cmlidXRlOiBzb3VyY2VBdHRyaWJ1dGUsIGhhbmRsZVRvdWNoOiBoYW5kbGVUb3VjaCwgb25TaG93OiBvblNob3csIG9uSGlkZTogb25IaWRlLCBob3ZlckRlbGF5OiBob3ZlckRlbGF5LCB0b3VjaERlbGF5OiB0b3VjaERlbGF5LCBob3ZlckJvdW5kaW5nQm94OiBob3ZlckJvdW5kaW5nQm94LCB0b3VjaEJvdW5kaW5nQm94OiB0b3VjaEJvdW5kaW5nQm94LCBuYW1lc3BhY2U6IG5hbWVzcGFjZSwgem9vbUZhY3Rvcjogem9vbUZhY3RvciB9O1xuXG4gICAgaWYgKHRoaXMuc2V0dGluZ3MuaG92ZXJCb3VuZGluZ0JveCB8fCB0aGlzLnNldHRpbmdzLnRvdWNoQm91bmRpbmdCb3gpIHtcbiAgICAgIHRoaXMuYm91bmRpbmdCb3ggPSBuZXcgX0JvdW5kaW5nQm94Mi5kZWZhdWx0KHtcbiAgICAgICAgbmFtZXNwYWNlOiB0aGlzLnNldHRpbmdzLm5hbWVzcGFjZSxcbiAgICAgICAgem9vbUZhY3RvcjogdGhpcy5zZXR0aW5ncy56b29tRmFjdG9yLFxuICAgICAgICBjb250YWluZXJFbDogdGhpcy5zZXR0aW5ncy5lbC5vZmZzZXRQYXJlbnRcbiAgICAgIH0pO1xuICAgIH1cblxuICAgIHRoaXMuZW5hYmxlZCA9IHRydWU7XG5cbiAgICB0aGlzLl9iaW5kRXZlbnRzKCk7XG4gIH1cblxuICBfY3JlYXRlQ2xhc3MoVHJpZ2dlciwgW3tcbiAgICBrZXk6ICdfYmluZEV2ZW50cycsXG4gICAgdmFsdWU6IGZ1bmN0aW9uIF9iaW5kRXZlbnRzKCkge1xuICAgICAgdGhpcy5zZXR0aW5ncy5lbC5hZGRFdmVudExpc3RlbmVyKCdtb3VzZWVudGVyJywgdGhpcy5faGFuZGxlRW50cnksIGZhbHNlKTtcbiAgICAgIHRoaXMuc2V0dGluZ3MuZWwuYWRkRXZlbnRMaXN0ZW5lcignbW91c2VsZWF2ZScsIHRoaXMuX2hpZGUsIGZhbHNlKTtcbiAgICAgIHRoaXMuc2V0dGluZ3MuZWwuYWRkRXZlbnRMaXN0ZW5lcignbW91c2Vtb3ZlJywgdGhpcy5faGFuZGxlTW92ZW1lbnQsIGZhbHNlKTtcblxuICAgICAgaWYgKHRoaXMuc2V0dGluZ3MuaGFuZGxlVG91Y2gpIHtcbiAgICAgICAgdGhpcy5zZXR0aW5ncy5lbC5hZGRFdmVudExpc3RlbmVyKCd0b3VjaHN0YXJ0JywgdGhpcy5faGFuZGxlRW50cnksIGZhbHNlKTtcbiAgICAgICAgdGhpcy5zZXR0aW5ncy5lbC5hZGRFdmVudExpc3RlbmVyKCd0b3VjaGVuZCcsIHRoaXMuX2hpZGUsIGZhbHNlKTtcbiAgICAgICAgdGhpcy5zZXR0aW5ncy5lbC5hZGRFdmVudExpc3RlbmVyKCd0b3VjaG1vdmUnLCB0aGlzLl9oYW5kbGVNb3ZlbWVudCwgZmFsc2UpO1xuICAgICAgfVxuICAgIH1cbiAgfSwge1xuICAgIGtleTogJ191bmJpbmRFdmVudHMnLFxuICAgIHZhbHVlOiBmdW5jdGlvbiBfdW5iaW5kRXZlbnRzKCkge1xuICAgICAgdGhpcy5zZXR0aW5ncy5lbC5yZW1vdmVFdmVudExpc3RlbmVyKCdtb3VzZWVudGVyJywgdGhpcy5faGFuZGxlRW50cnksIGZhbHNlKTtcbiAgICAgIHRoaXMuc2V0dGluZ3MuZWwucmVtb3ZlRXZlbnRMaXN0ZW5lcignbW91c2VsZWF2ZScsIHRoaXMuX2hpZGUsIGZhbHNlKTtcbiAgICAgIHRoaXMuc2V0dGluZ3MuZWwucmVtb3ZlRXZlbnRMaXN0ZW5lcignbW91c2Vtb3ZlJywgdGhpcy5faGFuZGxlTW92ZW1lbnQsIGZhbHNlKTtcblxuICAgICAgaWYgKHRoaXMuc2V0dGluZ3MuaGFuZGxlVG91Y2gpIHtcbiAgICAgICAgdGhpcy5zZXR0aW5ncy5lbC5yZW1vdmVFdmVudExpc3RlbmVyKCd0b3VjaHN0YXJ0JywgdGhpcy5faGFuZGxlRW50cnksIGZhbHNlKTtcbiAgICAgICAgdGhpcy5zZXR0aW5ncy5lbC5yZW1vdmVFdmVudExpc3RlbmVyKCd0b3VjaGVuZCcsIHRoaXMuX2hpZGUsIGZhbHNlKTtcbiAgICAgICAgdGhpcy5zZXR0aW5ncy5lbC5yZW1vdmVFdmVudExpc3RlbmVyKCd0b3VjaG1vdmUnLCB0aGlzLl9oYW5kbGVNb3ZlbWVudCwgZmFsc2UpO1xuICAgICAgfVxuICAgIH1cbiAgfSwge1xuICAgIGtleTogJ2lzU2hvd2luZycsXG4gICAgZ2V0OiBmdW5jdGlvbiBnZXQoKSB7XG4gICAgICByZXR1cm4gdGhpcy5zZXR0aW5ncy56b29tUGFuZS5pc1Nob3dpbmc7XG4gICAgfVxuICB9XSk7XG5cbiAgcmV0dXJuIFRyaWdnZXI7XG59KCk7XG5cbnZhciBfaW5pdGlhbGlzZVByb3BzID0gZnVuY3Rpb24gX2luaXRpYWxpc2VQcm9wcygpIHtcbiAgdmFyIF90aGlzID0gdGhpcztcblxuICB0aGlzLl9oYW5kbGVFbnRyeSA9IGZ1bmN0aW9uIChlKSB7XG4gICAgZS5wcmV2ZW50RGVmYXVsdCgpO1xuICAgIF90aGlzLl9sYXN0TW92ZW1lbnQgPSBlO1xuXG4gICAgaWYgKGUudHlwZSA9PSAnbW91c2VlbnRlcicgJiYgX3RoaXMuc2V0dGluZ3MuaG92ZXJEZWxheSkge1xuICAgICAgX3RoaXMuZW50cnlUaW1lb3V0ID0gc2V0VGltZW91dChfdGhpcy5fc2hvdywgX3RoaXMuc2V0dGluZ3MuaG92ZXJEZWxheSk7XG4gICAgfSBlbHNlIGlmIChfdGhpcy5zZXR0aW5ncy50b3VjaERlbGF5KSB7XG4gICAgICBfdGhpcy5lbnRyeVRpbWVvdXQgPSBzZXRUaW1lb3V0KF90aGlzLl9zaG93LCBfdGhpcy5zZXR0aW5ncy50b3VjaERlbGF5KTtcbiAgICB9IGVsc2Uge1xuICAgICAgX3RoaXMuX3Nob3coKTtcbiAgICB9XG4gIH07XG5cbiAgdGhpcy5fc2hvdyA9IGZ1bmN0aW9uICgpIHtcbiAgICBpZiAoIV90aGlzLmVuYWJsZWQpIHtcbiAgICAgIHJldHVybjtcbiAgICB9XG5cbiAgICB2YXIgb25TaG93ID0gX3RoaXMuc2V0dGluZ3Mub25TaG93O1xuICAgIGlmIChvblNob3cgJiYgdHlwZW9mIG9uU2hvdyA9PT0gJ2Z1bmN0aW9uJykge1xuICAgICAgb25TaG93KCk7XG4gICAgfVxuXG4gICAgX3RoaXMuc2V0dGluZ3Muem9vbVBhbmUuc2hvdyhfdGhpcy5zZXR0aW5ncy5lbC5nZXRBdHRyaWJ1dGUoX3RoaXMuc2V0dGluZ3Muc291cmNlQXR0cmlidXRlKSwgX3RoaXMuc2V0dGluZ3MuZWwuY2xpZW50V2lkdGgsIF90aGlzLnNldHRpbmdzLmVsLmNsaWVudEhlaWdodCk7XG5cbiAgICBpZiAoX3RoaXMuX2xhc3RNb3ZlbWVudCkge1xuICAgICAgdmFyIHRvdWNoQWN0aXZhdGVkID0gX3RoaXMuX2xhc3RNb3ZlbWVudC50b3VjaGVzO1xuICAgICAgaWYgKHRvdWNoQWN0aXZhdGVkICYmIF90aGlzLnNldHRpbmdzLnRvdWNoQm91bmRpbmdCb3ggfHwgIXRvdWNoQWN0aXZhdGVkICYmIF90aGlzLnNldHRpbmdzLmhvdmVyQm91bmRpbmdCb3gpIHtcbiAgICAgICAgX3RoaXMuYm91bmRpbmdCb3guc2hvdyhfdGhpcy5zZXR0aW5ncy56b29tUGFuZS5lbC5jbGllbnRXaWR0aCwgX3RoaXMuc2V0dGluZ3Muem9vbVBhbmUuZWwuY2xpZW50SGVpZ2h0KTtcbiAgICAgIH1cbiAgICB9XG5cbiAgICBfdGhpcy5faGFuZGxlTW92ZW1lbnQoKTtcbiAgfTtcblxuICB0aGlzLl9oaWRlID0gZnVuY3Rpb24gKGUpIHtcbiAgICBlLnByZXZlbnREZWZhdWx0KCk7XG5cbiAgICBfdGhpcy5fbGFzdE1vdmVtZW50ID0gbnVsbDtcblxuICAgIGlmIChfdGhpcy5lbnRyeVRpbWVvdXQpIHtcbiAgICAgIGNsZWFyVGltZW91dChfdGhpcy5lbnRyeVRpbWVvdXQpO1xuICAgIH1cblxuICAgIGlmIChfdGhpcy5ib3VuZGluZ0JveCkge1xuICAgICAgX3RoaXMuYm91bmRpbmdCb3guaGlkZSgpO1xuICAgIH1cblxuICAgIHZhciBvbkhpZGUgPSBfdGhpcy5zZXR0aW5ncy5vbkhpZGU7XG4gICAgaWYgKG9uSGlkZSAmJiB0eXBlb2Ygb25IaWRlID09PSAnZnVuY3Rpb24nKSB7XG4gICAgICBvbkhpZGUoKTtcbiAgICB9XG5cbiAgICBfdGhpcy5zZXR0aW5ncy56b29tUGFuZS5oaWRlKCk7XG4gIH07XG5cbiAgdGhpcy5faGFuZGxlTW92ZW1lbnQgPSBmdW5jdGlvbiAoZSkge1xuICAgIGlmIChlKSB7XG4gICAgICBlLnByZXZlbnREZWZhdWx0KCk7XG4gICAgICBfdGhpcy5fbGFzdE1vdmVtZW50ID0gZTtcbiAgICB9IGVsc2UgaWYgKF90aGlzLl9sYXN0TW92ZW1lbnQpIHtcbiAgICAgIGUgPSBfdGhpcy5fbGFzdE1vdmVtZW50O1xuICAgIH0gZWxzZSB7XG4gICAgICByZXR1cm47XG4gICAgfVxuXG4gICAgdmFyIG1vdmVtZW50WCA9IHZvaWQgMCxcbiAgICAgICAgbW92ZW1lbnRZID0gdm9pZCAwO1xuXG4gICAgaWYgKGUudG91Y2hlcykge1xuICAgICAgdmFyIGZpcnN0VG91Y2ggPSBlLnRvdWNoZXNbMF07XG4gICAgICBtb3ZlbWVudFggPSBmaXJzdFRvdWNoLmNsaWVudFg7XG4gICAgICBtb3ZlbWVudFkgPSBmaXJzdFRvdWNoLmNsaWVudFk7XG4gICAgfSBlbHNlIHtcbiAgICAgIG1vdmVtZW50WCA9IGUuY2xpZW50WDtcbiAgICAgIG1vdmVtZW50WSA9IGUuY2xpZW50WTtcbiAgICB9XG5cbiAgICB2YXIgZWwgPSBfdGhpcy5zZXR0aW5ncy5lbDtcbiAgICB2YXIgcmVjdCA9IGVsLmdldEJvdW5kaW5nQ2xpZW50UmVjdCgpO1xuICAgIHZhciBvZmZzZXRYID0gbW92ZW1lbnRYIC0gcmVjdC5sZWZ0O1xuICAgIHZhciBvZmZzZXRZID0gbW92ZW1lbnRZIC0gcmVjdC50b3A7XG5cbiAgICB2YXIgcGVyY2VudGFnZU9mZnNldFggPSBvZmZzZXRYIC8gX3RoaXMuc2V0dGluZ3MuZWwuY2xpZW50V2lkdGg7XG4gICAgdmFyIHBlcmNlbnRhZ2VPZmZzZXRZID0gb2Zmc2V0WSAvIF90aGlzLnNldHRpbmdzLmVsLmNsaWVudEhlaWdodDtcblxuICAgIGlmIChfdGhpcy5ib3VuZGluZ0JveCkge1xuICAgICAgX3RoaXMuYm91bmRpbmdCb3guc2V0UG9zaXRpb24ocGVyY2VudGFnZU9mZnNldFgsIHBlcmNlbnRhZ2VPZmZzZXRZLCByZWN0KTtcbiAgICB9XG5cbiAgICBfdGhpcy5zZXR0aW5ncy56b29tUGFuZS5zZXRQb3NpdGlvbihwZXJjZW50YWdlT2Zmc2V0WCwgcGVyY2VudGFnZU9mZnNldFksIHJlY3QpO1xuICB9O1xufTtcblxuZXhwb3J0cy5kZWZhdWx0ID0gVHJpZ2dlcjtcblxufSx7XCIuL0JvdW5kaW5nQm94XCI6MSxcIi4vdXRpbC90aHJvd0lmTWlzc2luZ1wiOjd9XSw0OltmdW5jdGlvbihyZXF1aXJlLG1vZHVsZSxleHBvcnRzKXtcbid1c2Ugc3RyaWN0JztcblxuT2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIFwiX19lc01vZHVsZVwiLCB7XG4gIHZhbHVlOiB0cnVlXG59KTtcblxudmFyIF9jcmVhdGVDbGFzcyA9IGZ1bmN0aW9uICgpIHtcbiAgZnVuY3Rpb24gZGVmaW5lUHJvcGVydGllcyh0YXJnZXQsIHByb3BzKSB7XG4gICAgZm9yICh2YXIgaSA9IDA7IGkgPCBwcm9wcy5sZW5ndGg7IGkrKykge1xuICAgICAgdmFyIGRlc2NyaXB0b3IgPSBwcm9wc1tpXTtkZXNjcmlwdG9yLmVudW1lcmFibGUgPSBkZXNjcmlwdG9yLmVudW1lcmFibGUgfHwgZmFsc2U7ZGVzY3JpcHRvci5jb25maWd1cmFibGUgPSB0cnVlO2lmIChcInZhbHVlXCIgaW4gZGVzY3JpcHRvcikgZGVzY3JpcHRvci53cml0YWJsZSA9IHRydWU7T2JqZWN0LmRlZmluZVByb3BlcnR5KHRhcmdldCwgZGVzY3JpcHRvci5rZXksIGRlc2NyaXB0b3IpO1xuICAgIH1cbiAgfXJldHVybiBmdW5jdGlvbiAoQ29uc3RydWN0b3IsIHByb3RvUHJvcHMsIHN0YXRpY1Byb3BzKSB7XG4gICAgaWYgKHByb3RvUHJvcHMpIGRlZmluZVByb3BlcnRpZXMoQ29uc3RydWN0b3IucHJvdG90eXBlLCBwcm90b1Byb3BzKTtpZiAoc3RhdGljUHJvcHMpIGRlZmluZVByb3BlcnRpZXMoQ29uc3RydWN0b3IsIHN0YXRpY1Byb3BzKTtyZXR1cm4gQ29uc3RydWN0b3I7XG4gIH07XG59KCk7XG5cbnZhciBfdGhyb3dJZk1pc3NpbmcgPSByZXF1aXJlKCcuL3V0aWwvdGhyb3dJZk1pc3NpbmcnKTtcblxudmFyIF90aHJvd0lmTWlzc2luZzIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF90aHJvd0lmTWlzc2luZyk7XG5cbnZhciBfZG9tID0gcmVxdWlyZSgnLi91dGlsL2RvbScpO1xuXG5mdW5jdGlvbiBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KG9iaikge1xuICByZXR1cm4gb2JqICYmIG9iai5fX2VzTW9kdWxlID8gb2JqIDogeyBkZWZhdWx0OiBvYmogfTtcbn1cblxuZnVuY3Rpb24gX2NsYXNzQ2FsbENoZWNrKGluc3RhbmNlLCBDb25zdHJ1Y3Rvcikge1xuICBpZiAoIShpbnN0YW5jZSBpbnN0YW5jZW9mIENvbnN0cnVjdG9yKSkge1xuICAgIHRocm93IG5ldyBUeXBlRXJyb3IoXCJDYW5ub3QgY2FsbCBhIGNsYXNzIGFzIGEgZnVuY3Rpb25cIik7XG4gIH1cbn1cblxuLy8gQWxsIG9mZmljaWFsbHktc3VwcG9ydGVkIGJyb3dzZXJzIGhhdmUgdGhpcywgYnV0IGl0J3MgZWFzeSB0b1xuLy8gYWNjb3VudCBmb3IsIGp1c3QgaW4gY2FzZS5cbnZhciBkaXZTdHlsZSA9IGRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoJ2RpdicpLnN0eWxlO1xuXG52YXIgSEFTX0FOSU1BVElPTiA9IHR5cGVvZiBkb2N1bWVudCA9PT0gJ3VuZGVmaW5lZCcgPyBmYWxzZSA6ICdhbmltYXRpb24nIGluIGRpdlN0eWxlIHx8ICd3ZWJraXRBbmltYXRpb24nIGluIGRpdlN0eWxlO1xuXG52YXIgWm9vbVBhbmUgPSBmdW5jdGlvbiAoKSB7XG4gIGZ1bmN0aW9uIFpvb21QYW5lKCkge1xuICAgIHZhciBfdGhpcyA9IHRoaXM7XG5cbiAgICB2YXIgb3B0aW9ucyA9IGFyZ3VtZW50cy5sZW5ndGggPiAwICYmIGFyZ3VtZW50c1swXSAhPT0gdW5kZWZpbmVkID8gYXJndW1lbnRzWzBdIDoge307XG5cbiAgICBfY2xhc3NDYWxsQ2hlY2sodGhpcywgWm9vbVBhbmUpO1xuXG4gICAgdGhpcy5fY29tcGxldGVTaG93ID0gZnVuY3Rpb24gKCkge1xuICAgICAgX3RoaXMuZWwucmVtb3ZlRXZlbnRMaXN0ZW5lcignYW5pbWF0aW9uZW5kJywgX3RoaXMuX2NvbXBsZXRlU2hvdywgZmFsc2UpO1xuICAgICAgX3RoaXMuZWwucmVtb3ZlRXZlbnRMaXN0ZW5lcignd2Via2l0QW5pbWF0aW9uRW5kJywgX3RoaXMuX2NvbXBsZXRlU2hvdywgZmFsc2UpO1xuXG4gICAgICAoMCwgX2RvbS5yZW1vdmVDbGFzc2VzKShfdGhpcy5lbCwgX3RoaXMub3BlbmluZ0NsYXNzZXMpO1xuICAgIH07XG5cbiAgICB0aGlzLl9jb21wbGV0ZUhpZGUgPSBmdW5jdGlvbiAoKSB7XG4gICAgICBfdGhpcy5lbC5yZW1vdmVFdmVudExpc3RlbmVyKCdhbmltYXRpb25lbmQnLCBfdGhpcy5fY29tcGxldGVIaWRlLCBmYWxzZSk7XG4gICAgICBfdGhpcy5lbC5yZW1vdmVFdmVudExpc3RlbmVyKCd3ZWJraXRBbmltYXRpb25FbmQnLCBfdGhpcy5fY29tcGxldGVIaWRlLCBmYWxzZSk7XG5cbiAgICAgICgwLCBfZG9tLnJlbW92ZUNsYXNzZXMpKF90aGlzLmVsLCBfdGhpcy5vcGVuQ2xhc3Nlcyk7XG4gICAgICAoMCwgX2RvbS5yZW1vdmVDbGFzc2VzKShfdGhpcy5lbCwgX3RoaXMuY2xvc2luZ0NsYXNzZXMpO1xuICAgICAgKDAsIF9kb20ucmVtb3ZlQ2xhc3NlcykoX3RoaXMuZWwsIF90aGlzLmlubGluZUNsYXNzZXMpO1xuXG4gICAgICBfdGhpcy5lbC5zZXRBdHRyaWJ1dGUoJ3N0eWxlJywgJycpO1xuXG4gICAgICAvLyBUaGUgd2luZG93IGNvdWxkIGhhdmUgYmVlbiByZXNpemVkIGFib3ZlIG9yIGJlbG93IGBpbmxpbmVgXG4gICAgICAvLyBsaW1pdHMgc2luY2UgdGhlIFpvb21QYW5lIHdhcyBzaG93bi4gQmVjYXVzZSBvZiB0aGlzLCB3ZVxuICAgICAgLy8gY2FuJ3QgcmVseSBvbiBgdGhpcy5faXNJbmxpbmVgIGhlcmUuXG4gICAgICBpZiAoX3RoaXMuZWwucGFyZW50RWxlbWVudCA9PT0gX3RoaXMuc2V0dGluZ3MuY29udGFpbmVyKSB7XG4gICAgICAgIF90aGlzLnNldHRpbmdzLmNvbnRhaW5lci5yZW1vdmVDaGlsZChfdGhpcy5lbCk7XG4gICAgICB9IGVsc2UgaWYgKF90aGlzLmVsLnBhcmVudEVsZW1lbnQgPT09IF90aGlzLnNldHRpbmdzLmlubGluZUNvbnRhaW5lcikge1xuICAgICAgICBfdGhpcy5zZXR0aW5ncy5pbmxpbmVDb250YWluZXIucmVtb3ZlQ2hpbGQoX3RoaXMuZWwpO1xuICAgICAgfVxuICAgIH07XG5cbiAgICB0aGlzLl9oYW5kbGVMb2FkID0gZnVuY3Rpb24gKCkge1xuICAgICAgX3RoaXMuaW1nRWwucmVtb3ZlRXZlbnRMaXN0ZW5lcignbG9hZCcsIF90aGlzLl9oYW5kbGVMb2FkLCBmYWxzZSk7XG4gICAgICAoMCwgX2RvbS5yZW1vdmVDbGFzc2VzKShfdGhpcy5lbCwgX3RoaXMubG9hZGluZ0NsYXNzZXMpO1xuICAgIH07XG5cbiAgICB0aGlzLmlzU2hvd2luZyA9IGZhbHNlO1xuXG4gICAgdmFyIF9vcHRpb25zJGNvbnRhaW5lciA9IG9wdGlvbnMuY29udGFpbmVyLFxuICAgICAgICBjb250YWluZXIgPSBfb3B0aW9ucyRjb250YWluZXIgPT09IHVuZGVmaW5lZCA/IG51bGwgOiBfb3B0aW9ucyRjb250YWluZXIsXG4gICAgICAgIF9vcHRpb25zJHpvb21GYWN0b3IgPSBvcHRpb25zLnpvb21GYWN0b3IsXG4gICAgICAgIHpvb21GYWN0b3IgPSBfb3B0aW9ucyR6b29tRmFjdG9yID09PSB1bmRlZmluZWQgPyAoMCwgX3Rocm93SWZNaXNzaW5nMi5kZWZhdWx0KSgpIDogX29wdGlvbnMkem9vbUZhY3RvcixcbiAgICAgICAgX29wdGlvbnMkaW5saW5lID0gb3B0aW9ucy5pbmxpbmUsXG4gICAgICAgIGlubGluZSA9IF9vcHRpb25zJGlubGluZSA9PT0gdW5kZWZpbmVkID8gKDAsIF90aHJvd0lmTWlzc2luZzIuZGVmYXVsdCkoKSA6IF9vcHRpb25zJGlubGluZSxcbiAgICAgICAgX29wdGlvbnMkbmFtZXNwYWNlID0gb3B0aW9ucy5uYW1lc3BhY2UsXG4gICAgICAgIG5hbWVzcGFjZSA9IF9vcHRpb25zJG5hbWVzcGFjZSA9PT0gdW5kZWZpbmVkID8gbnVsbCA6IF9vcHRpb25zJG5hbWVzcGFjZSxcbiAgICAgICAgX29wdGlvbnMkc2hvd1doaXRlc3BhID0gb3B0aW9ucy5zaG93V2hpdGVzcGFjZUF0RWRnZXMsXG4gICAgICAgIHNob3dXaGl0ZXNwYWNlQXRFZGdlcyA9IF9vcHRpb25zJHNob3dXaGl0ZXNwYSA9PT0gdW5kZWZpbmVkID8gKDAsIF90aHJvd0lmTWlzc2luZzIuZGVmYXVsdCkoKSA6IF9vcHRpb25zJHNob3dXaGl0ZXNwYSxcbiAgICAgICAgX29wdGlvbnMkY29udGFpbklubGluID0gb3B0aW9ucy5jb250YWluSW5saW5lLFxuICAgICAgICBjb250YWluSW5saW5lID0gX29wdGlvbnMkY29udGFpbklubGluID09PSB1bmRlZmluZWQgPyAoMCwgX3Rocm93SWZNaXNzaW5nMi5kZWZhdWx0KSgpIDogX29wdGlvbnMkY29udGFpbklubGluLFxuICAgICAgICBfb3B0aW9ucyRpbmxpbmVPZmZzZXQgPSBvcHRpb25zLmlubGluZU9mZnNldFgsXG4gICAgICAgIGlubGluZU9mZnNldFggPSBfb3B0aW9ucyRpbmxpbmVPZmZzZXQgPT09IHVuZGVmaW5lZCA/IDAgOiBfb3B0aW9ucyRpbmxpbmVPZmZzZXQsXG4gICAgICAgIF9vcHRpb25zJGlubGluZU9mZnNldDIgPSBvcHRpb25zLmlubGluZU9mZnNldFksXG4gICAgICAgIGlubGluZU9mZnNldFkgPSBfb3B0aW9ucyRpbmxpbmVPZmZzZXQyID09PSB1bmRlZmluZWQgPyAwIDogX29wdGlvbnMkaW5saW5lT2Zmc2V0MixcbiAgICAgICAgX29wdGlvbnMkaW5saW5lQ29udGFpID0gb3B0aW9ucy5pbmxpbmVDb250YWluZXIsXG4gICAgICAgIGlubGluZUNvbnRhaW5lciA9IF9vcHRpb25zJGlubGluZUNvbnRhaSA9PT0gdW5kZWZpbmVkID8gZG9jdW1lbnQuYm9keSA6IF9vcHRpb25zJGlubGluZUNvbnRhaTtcblxuICAgIHRoaXMuc2V0dGluZ3MgPSB7IGNvbnRhaW5lcjogY29udGFpbmVyLCB6b29tRmFjdG9yOiB6b29tRmFjdG9yLCBpbmxpbmU6IGlubGluZSwgbmFtZXNwYWNlOiBuYW1lc3BhY2UsIHNob3dXaGl0ZXNwYWNlQXRFZGdlczogc2hvd1doaXRlc3BhY2VBdEVkZ2VzLCBjb250YWluSW5saW5lOiBjb250YWluSW5saW5lLCBpbmxpbmVPZmZzZXRYOiBpbmxpbmVPZmZzZXRYLCBpbmxpbmVPZmZzZXRZOiBpbmxpbmVPZmZzZXRZLCBpbmxpbmVDb250YWluZXI6IGlubGluZUNvbnRhaW5lciB9O1xuXG4gICAgdGhpcy5vcGVuQ2xhc3NlcyA9IHRoaXMuX2J1aWxkQ2xhc3Nlcygnb3BlbicpO1xuICAgIHRoaXMub3BlbmluZ0NsYXNzZXMgPSB0aGlzLl9idWlsZENsYXNzZXMoJ29wZW5pbmcnKTtcbiAgICB0aGlzLmNsb3NpbmdDbGFzc2VzID0gdGhpcy5fYnVpbGRDbGFzc2VzKCdjbG9zaW5nJyk7XG4gICAgdGhpcy5pbmxpbmVDbGFzc2VzID0gdGhpcy5fYnVpbGRDbGFzc2VzKCdpbmxpbmUnKTtcbiAgICB0aGlzLmxvYWRpbmdDbGFzc2VzID0gdGhpcy5fYnVpbGRDbGFzc2VzKCdsb2FkaW5nJyk7XG5cbiAgICB0aGlzLl9idWlsZEVsZW1lbnQoKTtcbiAgfVxuXG4gIF9jcmVhdGVDbGFzcyhab29tUGFuZSwgW3tcbiAgICBrZXk6ICdfYnVpbGRDbGFzc2VzJyxcbiAgICB2YWx1ZTogZnVuY3Rpb24gX2J1aWxkQ2xhc3NlcyhzdWZmaXgpIHtcbiAgICAgIHZhciBjbGFzc2VzID0gWydkcmlmdC0nICsgc3VmZml4XTtcblxuICAgICAgdmFyIG5zID0gdGhpcy5zZXR0aW5ncy5uYW1lc3BhY2U7XG4gICAgICBpZiAobnMpIHtcbiAgICAgICAgY2xhc3Nlcy5wdXNoKG5zICsgJy0nICsgc3VmZml4KTtcbiAgICAgIH1cblxuICAgICAgcmV0dXJuIGNsYXNzZXM7XG4gICAgfVxuICB9LCB7XG4gICAga2V5OiAnX2J1aWxkRWxlbWVudCcsXG4gICAgdmFsdWU6IGZ1bmN0aW9uIF9idWlsZEVsZW1lbnQoKSB7XG4gICAgICB0aGlzLmVsID0gZG9jdW1lbnQuY3JlYXRlRWxlbWVudCgnZGl2Jyk7XG4gICAgICAoMCwgX2RvbS5hZGRDbGFzc2VzKSh0aGlzLmVsLCB0aGlzLl9idWlsZENsYXNzZXMoJ3pvb20tcGFuZScpKTtcblxuICAgICAgdmFyIGxvYWRlckVsID0gZG9jdW1lbnQuY3JlYXRlRWxlbWVudCgnZGl2Jyk7XG4gICAgICAoMCwgX2RvbS5hZGRDbGFzc2VzKShsb2FkZXJFbCwgdGhpcy5fYnVpbGRDbGFzc2VzKCd6b29tLXBhbmUtbG9hZGVyJykpO1xuICAgICAgdGhpcy5lbC5hcHBlbmRDaGlsZChsb2FkZXJFbCk7XG5cbiAgICAgIHRoaXMuaW1nRWwgPSBkb2N1bWVudC5jcmVhdGVFbGVtZW50KCdpbWcnKTtcbiAgICAgIHRoaXMuZWwuYXBwZW5kQ2hpbGQodGhpcy5pbWdFbCk7XG4gICAgfVxuICB9LCB7XG4gICAga2V5OiAnX3NldEltYWdlVVJMJyxcbiAgICB2YWx1ZTogZnVuY3Rpb24gX3NldEltYWdlVVJMKGltYWdlVVJMKSB7XG4gICAgICB0aGlzLmltZ0VsLnNldEF0dHJpYnV0ZSgnc3JjJywgaW1hZ2VVUkwpO1xuICAgIH1cbiAgfSwge1xuICAgIGtleTogJ19zZXRJbWFnZVNpemUnLFxuICAgIHZhbHVlOiBmdW5jdGlvbiBfc2V0SW1hZ2VTaXplKHRyaWdnZXJXaWR0aCwgdHJpZ2dlckhlaWdodCkge1xuICAgICAgdGhpcy5pbWdFbC5zdHlsZS53aWR0aCA9IHRyaWdnZXJXaWR0aCAqIHRoaXMuc2V0dGluZ3Muem9vbUZhY3RvciArICdweCc7XG4gICAgICB0aGlzLmltZ0VsLnN0eWxlLmhlaWdodCA9IHRyaWdnZXJIZWlnaHQgKiB0aGlzLnNldHRpbmdzLnpvb21GYWN0b3IgKyAncHgnO1xuICAgIH1cblxuICAgIC8vIGBwZXJjZW50YWdlT2Zmc2V0WGAgYW5kIGBwZXJjZW50YWdlT2Zmc2V0WWAgbXVzdCBiZSBwZXJjZW50YWdlc1xuICAgIC8vIGV4cHJlc3NlZCBhcyBmbG9hdHMgYmV0d2VlbiBgMCcgYW5kIGAxYC5cblxuICB9LCB7XG4gICAga2V5OiAnc2V0UG9zaXRpb24nLFxuICAgIHZhbHVlOiBmdW5jdGlvbiBzZXRQb3NpdGlvbihwZXJjZW50YWdlT2Zmc2V0WCwgcGVyY2VudGFnZU9mZnNldFksIHRyaWdnZXJSZWN0KSB7XG4gICAgICB2YXIgbGVmdCA9IC0odGhpcy5pbWdFbC5jbGllbnRXaWR0aCAqIHBlcmNlbnRhZ2VPZmZzZXRYIC0gdGhpcy5lbC5jbGllbnRXaWR0aCAvIDIpO1xuICAgICAgdmFyIHRvcCA9IC0odGhpcy5pbWdFbC5jbGllbnRIZWlnaHQgKiBwZXJjZW50YWdlT2Zmc2V0WSAtIHRoaXMuZWwuY2xpZW50SGVpZ2h0IC8gMik7XG4gICAgICB2YXIgbWF4TGVmdCA9IC0odGhpcy5pbWdFbC5jbGllbnRXaWR0aCAtIHRoaXMuZWwuY2xpZW50V2lkdGgpO1xuICAgICAgdmFyIG1heFRvcCA9IC0odGhpcy5pbWdFbC5jbGllbnRIZWlnaHQgLSB0aGlzLmVsLmNsaWVudEhlaWdodCk7XG5cbiAgICAgIGlmICh0aGlzLmVsLnBhcmVudEVsZW1lbnQgPT09IHRoaXMuc2V0dGluZ3MuaW5saW5lQ29udGFpbmVyKSB7XG4gICAgICAgIC8vIFRoaXMgbWF5IGJlIG5lZWRlZCBpbiB0aGUgZnV0dXJlIHRvIGRlYWwgd2l0aCBicm93c2VyIGV2ZW50XG4gICAgICAgIC8vIGluY29uc2lzdGVuY2llcywgYnV0IGl0J3MgZGlmZmljdWx0IHRvIHRlbGwgZm9yIHN1cmUuXG4gICAgICAgIC8vIGxldCBzY3JvbGxYID0gaXNUb3VjaCA/IDAgOiB3aW5kb3cuc2Nyb2xsWDtcbiAgICAgICAgLy8gbGV0IHNjcm9sbFkgPSBpc1RvdWNoID8gMCA6IHdpbmRvdy5zY3JvbGxZO1xuICAgICAgICB2YXIgc2Nyb2xsWCA9IHdpbmRvdy5wYWdlWE9mZnNldDtcbiAgICAgICAgdmFyIHNjcm9sbFkgPSB3aW5kb3cucGFnZVlPZmZzZXQ7XG5cbiAgICAgICAgdmFyIGlubGluZUxlZnQgPSB0cmlnZ2VyUmVjdC5sZWZ0ICsgcGVyY2VudGFnZU9mZnNldFggKiB0cmlnZ2VyUmVjdC53aWR0aCAtIHRoaXMuZWwuY2xpZW50V2lkdGggLyAyICsgdGhpcy5zZXR0aW5ncy5pbmxpbmVPZmZzZXRYICsgc2Nyb2xsWDtcbiAgICAgICAgdmFyIGlubGluZVRvcCA9IHRyaWdnZXJSZWN0LnRvcCArIHBlcmNlbnRhZ2VPZmZzZXRZICogdHJpZ2dlclJlY3QuaGVpZ2h0IC0gdGhpcy5lbC5jbGllbnRIZWlnaHQgLyAyICsgdGhpcy5zZXR0aW5ncy5pbmxpbmVPZmZzZXRZICsgc2Nyb2xsWTtcblxuICAgICAgICBpZiAodGhpcy5zZXR0aW5ncy5jb250YWluSW5saW5lKSB7XG4gICAgICAgICAgdmFyIGVsUmVjdCA9IHRoaXMuZWwuZ2V0Qm91bmRpbmdDbGllbnRSZWN0KCk7XG5cbiAgICAgICAgICBpZiAoaW5saW5lTGVmdCA8IHRyaWdnZXJSZWN0LmxlZnQgKyBzY3JvbGxYKSB7XG4gICAgICAgICAgICBpbmxpbmVMZWZ0ID0gdHJpZ2dlclJlY3QubGVmdCArIHNjcm9sbFg7XG4gICAgICAgICAgfSBlbHNlIGlmIChpbmxpbmVMZWZ0ICsgdGhpcy5lbC5jbGllbnRXaWR0aCA+IHRyaWdnZXJSZWN0LmxlZnQgKyB0cmlnZ2VyUmVjdC53aWR0aCArIHNjcm9sbFgpIHtcbiAgICAgICAgICAgIGlubGluZUxlZnQgPSB0cmlnZ2VyUmVjdC5sZWZ0ICsgdHJpZ2dlclJlY3Qud2lkdGggLSB0aGlzLmVsLmNsaWVudFdpZHRoICsgc2Nyb2xsWDtcbiAgICAgICAgICB9XG5cbiAgICAgICAgICBpZiAoaW5saW5lVG9wIDwgdHJpZ2dlclJlY3QudG9wICsgc2Nyb2xsWSkge1xuICAgICAgICAgICAgaW5saW5lVG9wID0gdHJpZ2dlclJlY3QudG9wICsgc2Nyb2xsWTtcbiAgICAgICAgICB9IGVsc2UgaWYgKGlubGluZVRvcCArIHRoaXMuZWwuY2xpZW50SGVpZ2h0ID4gdHJpZ2dlclJlY3QudG9wICsgdHJpZ2dlclJlY3QuaGVpZ2h0ICsgc2Nyb2xsWSkge1xuICAgICAgICAgICAgaW5saW5lVG9wID0gdHJpZ2dlclJlY3QudG9wICsgdHJpZ2dlclJlY3QuaGVpZ2h0IC0gdGhpcy5lbC5jbGllbnRIZWlnaHQgKyBzY3JvbGxZO1xuICAgICAgICAgIH1cbiAgICAgICAgfVxuXG4gICAgICAgIHRoaXMuZWwuc3R5bGUubGVmdCA9IGlubGluZUxlZnQgKyAncHgnO1xuICAgICAgICB0aGlzLmVsLnN0eWxlLnRvcCA9IGlubGluZVRvcCArICdweCc7XG4gICAgICB9XG5cbiAgICAgIGlmICghdGhpcy5zZXR0aW5ncy5zaG93V2hpdGVzcGFjZUF0RWRnZXMpIHtcbiAgICAgICAgaWYgKGxlZnQgPiAwKSB7XG4gICAgICAgICAgbGVmdCA9IDA7XG4gICAgICAgIH0gZWxzZSBpZiAobGVmdCA8IG1heExlZnQpIHtcbiAgICAgICAgICBsZWZ0ID0gbWF4TGVmdDtcbiAgICAgICAgfVxuXG4gICAgICAgIGlmICh0b3AgPiAwKSB7XG4gICAgICAgICAgdG9wID0gMDtcbiAgICAgICAgfSBlbHNlIGlmICh0b3AgPCBtYXhUb3ApIHtcbiAgICAgICAgICB0b3AgPSBtYXhUb3A7XG4gICAgICAgIH1cbiAgICAgIH1cblxuICAgICAgdGhpcy5pbWdFbC5zdHlsZS50cmFuc2Zvcm0gPSAndHJhbnNsYXRlKCcgKyBsZWZ0ICsgJ3B4LCAnICsgdG9wICsgJ3B4KSc7XG4gICAgICB0aGlzLmltZ0VsLnN0eWxlLndlYmtpdFRyYW5zZm9ybSA9ICd0cmFuc2xhdGUoJyArIGxlZnQgKyAncHgsICcgKyB0b3AgKyAncHgpJztcbiAgICB9XG4gIH0sIHtcbiAgICBrZXk6ICdfcmVtb3ZlTGlzdGVuZXJzQW5kUmVzZXRDbGFzc2VzJyxcbiAgICB2YWx1ZTogZnVuY3Rpb24gX3JlbW92ZUxpc3RlbmVyc0FuZFJlc2V0Q2xhc3NlcygpIHtcbiAgICAgIHRoaXMuZWwucmVtb3ZlRXZlbnRMaXN0ZW5lcignYW5pbWF0aW9uZW5kJywgdGhpcy5fY29tcGxldGVTaG93LCBmYWxzZSk7XG4gICAgICB0aGlzLmVsLnJlbW92ZUV2ZW50TGlzdGVuZXIoJ2FuaW1hdGlvbmVuZCcsIHRoaXMuX2NvbXBsZXRlSGlkZSwgZmFsc2UpO1xuICAgICAgdGhpcy5lbC5yZW1vdmVFdmVudExpc3RlbmVyKCd3ZWJraXRBbmltYXRpb25FbmQnLCB0aGlzLl9jb21wbGV0ZVNob3csIGZhbHNlKTtcbiAgICAgIHRoaXMuZWwucmVtb3ZlRXZlbnRMaXN0ZW5lcignd2Via2l0QW5pbWF0aW9uRW5kJywgdGhpcy5fY29tcGxldGVIaWRlLCBmYWxzZSk7XG4gICAgICAoMCwgX2RvbS5yZW1vdmVDbGFzc2VzKSh0aGlzLmVsLCB0aGlzLm9wZW5DbGFzc2VzKTtcbiAgICAgICgwLCBfZG9tLnJlbW92ZUNsYXNzZXMpKHRoaXMuZWwsIHRoaXMuY2xvc2luZ0NsYXNzZXMpO1xuICAgIH1cbiAgfSwge1xuICAgIGtleTogJ3Nob3cnLFxuICAgIHZhbHVlOiBmdW5jdGlvbiBzaG93KGltYWdlVVJMLCB0cmlnZ2VyV2lkdGgsIHRyaWdnZXJIZWlnaHQpIHtcbiAgICAgIHRoaXMuX3JlbW92ZUxpc3RlbmVyc0FuZFJlc2V0Q2xhc3NlcygpO1xuICAgICAgdGhpcy5pc1Nob3dpbmcgPSB0cnVlO1xuXG4gICAgICAoMCwgX2RvbS5hZGRDbGFzc2VzKSh0aGlzLmVsLCB0aGlzLm9wZW5DbGFzc2VzKTtcbiAgICAgICgwLCBfZG9tLmFkZENsYXNzZXMpKHRoaXMuZWwsIHRoaXMubG9hZGluZ0NsYXNzZXMpO1xuXG4gICAgICB0aGlzLmltZ0VsLmFkZEV2ZW50TGlzdGVuZXIoJ2xvYWQnLCB0aGlzLl9oYW5kbGVMb2FkLCBmYWxzZSk7XG4gICAgICB0aGlzLl9zZXRJbWFnZVVSTChpbWFnZVVSTCk7XG4gICAgICB0aGlzLl9zZXRJbWFnZVNpemUodHJpZ2dlcldpZHRoLCB0cmlnZ2VySGVpZ2h0KTtcblxuICAgICAgaWYgKHRoaXMuX2lzSW5saW5lKSB7XG4gICAgICAgIHRoaXMuX3Nob3dJbmxpbmUoKTtcbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIHRoaXMuX3Nob3dJbkNvbnRhaW5lcigpO1xuICAgICAgfVxuXG4gICAgICBpZiAoSEFTX0FOSU1BVElPTikge1xuICAgICAgICB0aGlzLmVsLmFkZEV2ZW50TGlzdGVuZXIoJ2FuaW1hdGlvbmVuZCcsIHRoaXMuX2NvbXBsZXRlU2hvdywgZmFsc2UpO1xuICAgICAgICB0aGlzLmVsLmFkZEV2ZW50TGlzdGVuZXIoJ3dlYmtpdEFuaW1hdGlvbkVuZCcsIHRoaXMuX2NvbXBsZXRlU2hvdywgZmFsc2UpO1xuICAgICAgICAoMCwgX2RvbS5hZGRDbGFzc2VzKSh0aGlzLmVsLCB0aGlzLm9wZW5pbmdDbGFzc2VzKTtcbiAgICAgIH1cbiAgICB9XG4gIH0sIHtcbiAgICBrZXk6ICdfc2hvd0lubGluZScsXG4gICAgdmFsdWU6IGZ1bmN0aW9uIF9zaG93SW5saW5lKCkge1xuICAgICAgdGhpcy5zZXR0aW5ncy5pbmxpbmVDb250YWluZXIuYXBwZW5kQ2hpbGQodGhpcy5lbCk7XG4gICAgICAoMCwgX2RvbS5hZGRDbGFzc2VzKSh0aGlzLmVsLCB0aGlzLmlubGluZUNsYXNzZXMpO1xuICAgIH1cbiAgfSwge1xuICAgIGtleTogJ19zaG93SW5Db250YWluZXInLFxuICAgIHZhbHVlOiBmdW5jdGlvbiBfc2hvd0luQ29udGFpbmVyKCkge1xuICAgICAgdGhpcy5zZXR0aW5ncy5jb250YWluZXIuYXBwZW5kQ2hpbGQodGhpcy5lbCk7XG4gICAgfVxuICB9LCB7XG4gICAga2V5OiAnaGlkZScsXG4gICAgdmFsdWU6IGZ1bmN0aW9uIGhpZGUoKSB7XG4gICAgICB0aGlzLl9yZW1vdmVMaXN0ZW5lcnNBbmRSZXNldENsYXNzZXMoKTtcbiAgICAgIHRoaXMuaXNTaG93aW5nID0gZmFsc2U7XG5cbiAgICAgIGlmIChIQVNfQU5JTUFUSU9OKSB7XG4gICAgICAgIHRoaXMuZWwuYWRkRXZlbnRMaXN0ZW5lcignYW5pbWF0aW9uZW5kJywgdGhpcy5fY29tcGxldGVIaWRlLCBmYWxzZSk7XG4gICAgICAgIHRoaXMuZWwuYWRkRXZlbnRMaXN0ZW5lcignd2Via2l0QW5pbWF0aW9uRW5kJywgdGhpcy5fY29tcGxldGVIaWRlLCBmYWxzZSk7XG4gICAgICAgICgwLCBfZG9tLmFkZENsYXNzZXMpKHRoaXMuZWwsIHRoaXMuY2xvc2luZ0NsYXNzZXMpO1xuICAgICAgfSBlbHNlIHtcbiAgICAgICAgKDAsIF9kb20ucmVtb3ZlQ2xhc3NlcykodGhpcy5lbCwgdGhpcy5vcGVuQ2xhc3Nlcyk7XG4gICAgICAgICgwLCBfZG9tLnJlbW92ZUNsYXNzZXMpKHRoaXMuZWwsIHRoaXMuaW5saW5lQ2xhc3Nlcyk7XG4gICAgICB9XG4gICAgfVxuICB9LCB7XG4gICAga2V5OiAnX2lzSW5saW5lJyxcbiAgICBnZXQ6IGZ1bmN0aW9uIGdldCgpIHtcbiAgICAgIHZhciBpbmxpbmUgPSB0aGlzLnNldHRpbmdzLmlubGluZTtcblxuICAgICAgcmV0dXJuIGlubGluZSA9PT0gdHJ1ZSB8fCB0eXBlb2YgaW5saW5lID09PSAnbnVtYmVyJyAmJiB3aW5kb3cuaW5uZXJXaWR0aCA8PSBpbmxpbmU7XG4gICAgfVxuICB9XSk7XG5cbiAgcmV0dXJuIFpvb21QYW5lO1xufSgpO1xuXG5leHBvcnRzLmRlZmF1bHQgPSBab29tUGFuZTtcblxufSx7XCIuL3V0aWwvZG9tXCI6NixcIi4vdXRpbC90aHJvd0lmTWlzc2luZ1wiOjd9XSw1OltmdW5jdGlvbihyZXF1aXJlLG1vZHVsZSxleHBvcnRzKXtcbid1c2Ugc3RyaWN0JztcblxuT2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIFwiX19lc01vZHVsZVwiLCB7XG4gIHZhbHVlOiB0cnVlXG59KTtcbmV4cG9ydHMuZGVmYXVsdCA9IGluamVjdEJhc2VTdHlsZXNoZWV0O1xudmFyIFJVTEVTID0gJ1xcbkBrZXlmcmFtZXMgbm9vcCB7XFxuICAwJSB7IHpvb206IDE7IH1cXG59XFxuXFxuQC13ZWJraXQta2V5ZnJhbWVzIG5vb3Age1xcbiAgMCUgeyB6b29tOiAxOyB9XFxufVxcblxcbi5kcmlmdC16b29tLXBhbmUuZHJpZnQtb3BlbiB7XFxuICBkaXNwbGF5OiBibG9jaztcXG59XFxuXFxuLmRyaWZ0LXpvb20tcGFuZS5kcmlmdC1vcGVuaW5nLCAuZHJpZnQtem9vbS1wYW5lLmRyaWZ0LWNsb3Npbmcge1xcbiAgYW5pbWF0aW9uOiBub29wIDFtcztcXG4gIC13ZWJraXQtYW5pbWF0aW9uOiBub29wIDFtcztcXG59XFxuXFxuLmRyaWZ0LXpvb20tcGFuZSB7XFxuICBwb3NpdGlvbjogYWJzb2x1dGU7XFxuICBvdmVyZmxvdzogaGlkZGVuO1xcbiAgd2lkdGg6IDEwMCU7XFxuICBoZWlnaHQ6IDEwMCU7XFxuICB0b3A6IDA7XFxuICBsZWZ0OiAwO1xcbiAgcG9pbnRlci1ldmVudHM6IG5vbmU7XFxufVxcblxcbi5kcmlmdC16b29tLXBhbmUtbG9hZGVyIHtcXG4gIGRpc3BsYXk6IG5vbmU7XFxufVxcblxcbi5kcmlmdC16b29tLXBhbmUgaW1nIHtcXG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcXG4gIGRpc3BsYXk6IGJsb2NrO1xcbiAgbWF4LXdpZHRoOiBub25lO1xcbiAgbWF4LWhlaWdodDogbm9uZTtcXG59XFxuXFxuLmRyaWZ0LWJvdW5kaW5nLWJveCB7XFxuICBwb3NpdGlvbjogYWJzb2x1dGU7XFxuICBwb2ludGVyLWV2ZW50czogbm9uZTtcXG59XFxuJztcblxuZnVuY3Rpb24gaW5qZWN0QmFzZVN0eWxlc2hlZXQoKSB7XG4gIGlmIChkb2N1bWVudC5xdWVyeVNlbGVjdG9yKCcuZHJpZnQtYmFzZS1zdHlsZXMnKSkge1xuICAgIHJldHVybjtcbiAgfVxuXG4gIHZhciBzdHlsZUVsID0gZG9jdW1lbnQuY3JlYXRlRWxlbWVudCgnc3R5bGUnKTtcbiAgc3R5bGVFbC50eXBlID0gJ3RleHQvY3NzJztcbiAgc3R5bGVFbC5jbGFzc0xpc3QuYWRkKCdkcmlmdC1iYXNlLXN0eWxlcycpO1xuXG4gIHN0eWxlRWwuYXBwZW5kQ2hpbGQoZG9jdW1lbnQuY3JlYXRlVGV4dE5vZGUoUlVMRVMpKTtcblxuICB2YXIgaGVhZCA9IGRvY3VtZW50LmhlYWQ7XG4gIGhlYWQuaW5zZXJ0QmVmb3JlKHN0eWxlRWwsIGhlYWQuZmlyc3RDaGlsZCk7XG59XG5cbn0se31dLDY6W2Z1bmN0aW9uKHJlcXVpcmUsbW9kdWxlLGV4cG9ydHMpe1xuJ3VzZSBzdHJpY3QnO1xuXG52YXIgX3R5cGVvZjIgPSB0eXBlb2YgU3ltYm9sID09PSBcImZ1bmN0aW9uXCIgJiYgdHlwZW9mIFN5bWJvbC5pdGVyYXRvciA9PT0gXCJzeW1ib2xcIiA/IGZ1bmN0aW9uIChvYmopIHsgcmV0dXJuIHR5cGVvZiBvYmo7IH0gOiBmdW5jdGlvbiAob2JqKSB7IHJldHVybiBvYmogJiYgdHlwZW9mIFN5bWJvbCA9PT0gXCJmdW5jdGlvblwiICYmIG9iai5jb25zdHJ1Y3RvciA9PT0gU3ltYm9sICYmIG9iaiAhPT0gU3ltYm9sLnByb3RvdHlwZSA/IFwic3ltYm9sXCIgOiB0eXBlb2Ygb2JqOyB9O1xuXG5PYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgXCJfX2VzTW9kdWxlXCIsIHtcbiAgdmFsdWU6IHRydWVcbn0pO1xuXG52YXIgX3R5cGVvZiA9IHR5cGVvZiBTeW1ib2wgPT09IFwiZnVuY3Rpb25cIiAmJiBfdHlwZW9mMihTeW1ib2wuaXRlcmF0b3IpID09PSBcInN5bWJvbFwiID8gZnVuY3Rpb24gKG9iaikge1xuICByZXR1cm4gdHlwZW9mIG9iaiA9PT0gXCJ1bmRlZmluZWRcIiA/IFwidW5kZWZpbmVkXCIgOiBfdHlwZW9mMihvYmopO1xufSA6IGZ1bmN0aW9uIChvYmopIHtcbiAgcmV0dXJuIG9iaiAmJiB0eXBlb2YgU3ltYm9sID09PSBcImZ1bmN0aW9uXCIgJiYgb2JqLmNvbnN0cnVjdG9yID09PSBTeW1ib2wgJiYgb2JqICE9PSBTeW1ib2wucHJvdG90eXBlID8gXCJzeW1ib2xcIiA6IHR5cGVvZiBvYmogPT09IFwidW5kZWZpbmVkXCIgPyBcInVuZGVmaW5lZFwiIDogX3R5cGVvZjIob2JqKTtcbn07XG5cbmV4cG9ydHMuaXNET01FbGVtZW50ID0gaXNET01FbGVtZW50O1xuZXhwb3J0cy5hZGRDbGFzc2VzID0gYWRkQ2xhc3NlcztcbmV4cG9ydHMucmVtb3ZlQ2xhc3NlcyA9IHJlbW92ZUNsYXNzZXM7XG4vLyBUaGlzIGlzIG5vdCByZWFsbHkgYSBwZXJmZWN0IGNoZWNrLCBidXQgd29ya3MgZmluZS5cbi8vIEZyb20gaHR0cDovL3N0YWNrb3ZlcmZsb3cuY29tL3F1ZXN0aW9ucy8zODQyODZcbnZhciBIQVNfRE9NXzIgPSAodHlwZW9mIEhUTUxFbGVtZW50ID09PSAndW5kZWZpbmVkJyA/ICd1bmRlZmluZWQnIDogX3R5cGVvZihIVE1MRWxlbWVudCkpID09PSAnb2JqZWN0JztcblxuZnVuY3Rpb24gaXNET01FbGVtZW50KG9iaikge1xuICByZXR1cm4gSEFTX0RPTV8yID8gb2JqIGluc3RhbmNlb2YgSFRNTEVsZW1lbnQgOiBvYmogJiYgKHR5cGVvZiBvYmogPT09ICd1bmRlZmluZWQnID8gJ3VuZGVmaW5lZCcgOiBfdHlwZW9mKG9iaikpID09PSAnb2JqZWN0JyAmJiBvYmogIT09IG51bGwgJiYgb2JqLm5vZGVUeXBlID09PSAxICYmIHR5cGVvZiBvYmoubm9kZU5hbWUgPT09ICdzdHJpbmcnO1xufVxuXG5mdW5jdGlvbiBhZGRDbGFzc2VzKGVsLCBjbGFzc05hbWVzKSB7XG4gIGNsYXNzTmFtZXMuZm9yRWFjaChmdW5jdGlvbiAoY2xhc3NOYW1lKSB7XG4gICAgZWwuY2xhc3NMaXN0LmFkZChjbGFzc05hbWUpO1xuICB9KTtcbn1cblxuZnVuY3Rpb24gcmVtb3ZlQ2xhc3NlcyhlbCwgY2xhc3NOYW1lcykge1xuICBjbGFzc05hbWVzLmZvckVhY2goZnVuY3Rpb24gKGNsYXNzTmFtZSkge1xuICAgIGVsLmNsYXNzTGlzdC5yZW1vdmUoY2xhc3NOYW1lKTtcbiAgfSk7XG59XG5cbn0se31dLDc6W2Z1bmN0aW9uKHJlcXVpcmUsbW9kdWxlLGV4cG9ydHMpe1xuJ3VzZSBzdHJpY3QnO1xuXG5PYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgXCJfX2VzTW9kdWxlXCIsIHtcbiAgdmFsdWU6IHRydWVcbn0pO1xuZXhwb3J0cy5kZWZhdWx0ID0gdGhyb3dJZk1pc3Npbmc7XG5mdW5jdGlvbiB0aHJvd0lmTWlzc2luZygpIHtcbiAgdGhyb3cgbmV3IEVycm9yKCdNaXNzaW5nIHBhcmFtZXRlcicpO1xufVxuXG59LHt9XX0se30sWzJdKSgyKVxufSk7Il19
