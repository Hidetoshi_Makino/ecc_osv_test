<!--{*
 * Copyright(c) 2013 Spirit of Co.,Ltd. All rights reserved.
 * http://sbps.ec-cube.pro/
 * Updated: 2013/08/01
 *}-->
<script type="text/javascript">//<![CDATA[

//]]>
</script>


<div class="pagetitle">
    <h2 class="title">Input payment infomation</h2>
    <span><!--{$tpl_title|h}--></span>
</div>

<div class="detail">
    <form name="form1" id="form1" method="POST" action="<!--{$tpl_url}-->" autocomplete="off">
        <input type="hidden" name="<!--{$smarty.const.TRANSACTION_ID_NAME}-->" value="<!--{$transactionid}-->" />
        <input type="hidden" name="mode" value="register" />
        <input type="hidden" name="uniqid" value="<!--{$tpl_uniqid}-->" />
        <!--{foreach from=$arrForm item=data key=key}-->
        <input type="hidden" name="<!--{$key|h}-->" value="<!--{$data.value|h}-->" />
        <!--{/foreach}-->

        <div id="payment_form">
        <!--{if $tpl_form_bloc_path != ""}-->
            <!--{include file=$tpl_form_bloc_path}-->
        <!--{/if}-->
        </div>
    </form>
</div>
