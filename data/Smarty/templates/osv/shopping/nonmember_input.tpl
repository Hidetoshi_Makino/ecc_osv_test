<!--{*
 * This file is part of EC-CUBE
 *
 * Copyright(c) 2000-2014 LOCKON CO.,LTD. All Rights Reserved.
 *
 * http://www.lockon.co.jp/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *}-->



<div class="pagetitle">
    <h2 class="title">Enter customer information</h2>
    <span>お客様情報入力</span>
</div>

<div class="detail">

    <p class="topTxt">下記項目にご入力ください。「<span class="attention">※</span>」印は入力必須項目です。<br /></p>


    <form name="form1" id="form1" method="post" action="?">
        <input type="hidden" name="<!--{$smarty.const.TRANSACTION_ID_NAME}-->" value="<!--{$transactionid}-->" />
        <input type="hidden" name="mode" value="nonmember_confirm" />
        <input type="hidden" name="uniqid" value="<!--{$tpl_uniqid}-->" />
        
        <!--{include file="`$smarty.const.TEMPLATE_REALDIR`frontparts/form_personal_input.tpl" flgFields=2 emailMobile=false prefix="order_"}-->
                <!--{assign var=key value="deliv_check"}-->
        <dl>
            <dt>お届け先</dt>
            <dd>
                <input type="checkbox" name="<!--{$key}-->" value="1" onclick="eccube.toggleDeliveryForm();" <!--{$arrForm[$key].value|sfGetChecked:1}--> id="deliv_label" style="float: left;" />
                <label for="deliv_label">
                    <span class="attention">お届け先を指定</span>
                    <div>※上記に入力された住所と同一の場合は省略可能です。</div>
                </label>
            </dd>
        </dl>

        <section id="shippind_address"<!--{if $arrForm[$key].value == 0}--> style="display: none;"<!--{ /if }-->>
            <!--{include file="`$smarty.const.TEMPLATE_REALDIR`frontparts/form_personal_input.tpl" flgFields=1 emailMobile=false prefix="shipping_"}-->
        </section>

        <div class="btn_area">
            <div class="btn_submit" onclick="javascript:void(document.form1.submit())">確認ページへ</div>
        </div>
    </form>

</div>

<script>
$(function() {
    $('input[name="deliv_check"]').change(function() {
        if ($(this).prop('checked')) {
            document.getElementById("shippind_address").style.display = "block";
            //console.log("checked");
        } else {
            document.getElementById("shippind_address").style.display = "none";
            //console.log("un checked");
        }
    })
})
</script>
