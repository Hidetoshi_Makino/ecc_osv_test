<!--{*
 * This file is part of EC-CUBE
 *
 * Copyright(c) 2000-2014 LOCKON CO.,LTD. All Rights Reserved.
 *
 * http://www.lockon.co.jp/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.    See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA    02111-1307, USA.
 *}-->

<!--▼HEADER-->
<!--{strip}-->
    <!--{* Interrupt element: Start *}-->
    <!--{ if false }-->
<div class="glb-announce outer -html">
    <div class="announce-inner inner">
        <a href="" class="detail-outer anchor">
            <div class="announce-label left">
                <div class="top">会員限定</div>
                <div class="bottom">2018.7.1(SUN) 12:00まで</div>
            </div>
            <div class="announce-detail center">
                <div class="title">OUTLET</div>
                <div class="description-outer description">
                    <div class="detail">対象アイテム 約1万点！ 表示価格よりさらに</div>
                    <div class="detail">MAX 20%OFF</div>
                    <div class="detail -small">※一部対象外</div>
                </div>
            </div>
            <div class="announce-btn right">対象方品はこちら</div>
        </a>
    </div>
</div>
    <!--{ /if }--
    <!--{* Interrupt element: End *}-->


    <!--{* Header elements: Start *}-->
        <!--{**
            * View device for PC: Start
            *}-->
<div class="common-header outer -top">
    <div class="header-heading inner">
        <div class="heading-inner layout">
            <div class="menu-list -left">
                <a id="menu" class="menu-trigger trigger -sp" href="#">
                    <span></span>
                    <span></span>
                    <span></span>
                </a>
                <i id="search" class="items fas fa-search search -sp" onclick="javascript:location.href = '<!--{$smarty.const.USER_URL}-->search.php'"></i>
            </div>
            <a class="header-logo logo" href="<!--{$smarty.const.TOP_URL}-->">
                <img src="<!--{$TPL_URLPATH}-->img/svg/osv_logo.svg" height="63px">
<!--{ if false }-->
                <svg viewBox="0 0 182.78 38.84" class="use-logo svg">
    <use xlink:href="<!--{$TPL_URLPATH}-->img/svg/sprite.svg#use_logo"></use>
    <use class="shel" xlink:href="<!--{$TPL_URLPATH}-->img/svg/sprite.svg#shel"></use>
    <use class="mag" xlink:href="<!--{$TPL_URLPATH}-->img/svg/sprite.svg#mag"></use>
                </svg>
<!--{ /if }-->
            </a>
<style>
    div.-right a:link,
    div.-right a:visited {
        display: inline-block;
        color: none;
        text-decoration: none;
    }
</style>
            <div class="menu-list -right">
                <i id="search" class="items fas fa-search search -pc" onclick="javascript:location.href = '<!--{$smarty.const.USER_URL}-->search.php'"></i>
                <i class="items fas fa-user user" onclick="javascript:location.href = '<!--{$smarty.const.HTTPS_URL}-->mypage/login.php'"></i>
    <!--{ if false}-->
    <!-- Display none: Start -->
                <i class="items fas fa-comment announce"></i>
                <i class="items fas fa-heart favorite"></i>
    <!-- Display none: End -->
    <!--{ /if }-->
                <i data-items="<!--{$tpl_cart_quantity_total|number_format|default:0}-->" id="cart" class="items <!--{ if ($tpl_cart_quantity_total > 0) }-->cart<!--{ /if }--> fas fa-shopping-cart" onclick="javascript:location.href = '<!--{$smarty.const.CART_URL}-->'"></i>
            </div>
        </div>
    </div>
</div>

<div class="common-nav outer -pc">
    <nav class="nav-container nav">
        <div class="member-status status">
            <a id="status" class="status items" href=""></a>
            <a class="member items" href="">新規会員登録</a>
        </div>
        <ul class="nav-list list">

            <li class="list-item items"><a class="anchor" href="/">HOME</a></li>

            <li class="list-item items"><a class="anchor" href="javascript:void(0)">CATEGORY</a>

                <ul class="nav-list">

                    <li class="list-item items">
                        <a class="anchor" href="/products/list.php?category_id=7">
                        MAKE UP</a></li>

                    <li class="list-item items">
                        <a class="anchor" href="/products/list.php?category_id=1">
                        食品</a></li>

                    <li class="list-item items">
                        <a class="anchor" href="">
                        ACCESSORIES</a></li>

                </ul>

            </li>
<!--{ if false }-->
            <li class="list-item items"><a class="anchor" href="javascript:void(0)">BRAND</a>

                <ul class="nav-list">

                    <li class="list-item items"><a class="anchor" href="mou.html">MOUSSY</a></li>

                    <li class="list-item items"><a class="anchor" href="sly.html">SLY</a></li>

                    <li class="list-item items"><a class="anchor" href="rcs.html">RCS</a></li>

                    <li class="list-item items"><a class="anchor" href="azl.html">AZUL by moussy</a></li>

                </ul>
            </li>

            <li class="list-item items"><a class="anchor" href="rank.html">RANKING</a></li>

            <li class="list-item items"><a class="anchor" href="topics.html">TOPICS</a></li>

            <li class="list-item items"><a class="anchor" href="feat.html">FEATURE</a></li>
<!--{ /if }-->
        </ul>
    </nav>
</div>
        <!--{**
            * View device for PC: End
            *}-->

        <!--{**
            * View device for SmartPhone: Start
            *}-->
<div id="overlay" class="overlay"></div>
    <div class="common-nav outer -sp">
    <nav class="nav-container nav">

<!--{ if false }-->
<!-- Display none: Start -->
        <div class="member-status status">
            <a id="status" class="status items" href="<!--{$smarty.const.HTTPS_URL}-->mypage/login.php"></a>
            <a class="member items" href="<!--{$smarty.const.HTTPS_URL}-->entry/kiyaku.php">新規会員登録</a>
        </div>
<!-- Display none: End -->
<!--{ /if }-->

        <ul class="nav-list list">

            <li class="list-item items"><a class="anchor" href="/">HOME</a></li>

            <li class="list-item items"><a class="anchor" href="javascript:void(0)">CATEGORY</a>

                <ul class="nav-list">

                    <li class="list-item items">
                        <a class="anchor" href="/products/list.php?category_id=7">
                        MAKE UP</a></li>

                    <li class="list-item items">
                        <a class="anchor" href="/products/list.php?category_id=1">
                        食品</a></li>

                    <li class="list-item items"><a class="anchor" href="">
                        ACCESSORIES</a></li>

                </ul>

            </li>
<!--{ if false }-->
            <li class="list-item items"><a class="anchor" href="javascript:void(0)">BRAND</a>
                <ul class="nav-list">

                    <li class="list-item items"><a class="anchor" href="mou.html">MOUSSY</a></li>

                    <li class="list-item items"><a class="anchor" href="sly.html">SLY</a></li>

                    <li class="list-item items"><a class="anchor" href="rcs.html">RCS</a></li>

                    <li class="list-item items"><a class="anchor" href="azl.html">AZUL by moussy</a></li>

                </ul>
            </li>


            <li class="list-item items"><a class="anchor" href="rank.html">RANKING</a></li>

            <li class="list-item items"><a class="anchor" href="topics.html">TOPICS</a></li>

            <li class="list-item items"><a class="anchor" href="feat.html">FEATURE</a></li>
<!--{ /if }-->
        </ul>
    </nav>
</div>
        <!--{**
            * View device for SmartPhone: End
            *}-->
    <!--{* Header elements: End *}-->



    <!--{* Breadcrumb elements: Start *}-->
<!--{ if $arrPageLayout.page_id != 1 }--><!--{* Hidden top-page only *}-->
<div class="bread-outer">
    <div class="bread-inner inner">
        <a class="bread root" href="/">HOME</a>
    <!--{ section name=r loop=$arrRelativeCat[0] }-->
        <a class="bread" href="<!--{ $smarty.const.ROOT_URLPATH }-->products/list.php?category_id=<!--{ $arrRelativeCat[0][r].category_id }-->"><!--{ $arrRelativeCat[0][r].category_name|h }--></a>
    <!--{ sectionelse }-->
        <a class="bread current"><!--{ $tpl_subtitle }--></a>
    <!--{/section}-->

    <!--{ if $arrPageLayout.page_id == 3 }--><!--{* $arrProduct.name *}-->
        <a class="bread current"><!--{$arrProduct.name}--></a>
    <!--{ elseif $arrPageLayout.page_id == 2}-->
    <!--{ else }-->
        <a class="bread current"><!--{$arrPageLayout.page_name}--></a>
    <!--{ /if }-->
        <a href="#" class="bread back" onclick="javascript:window.history.back(-1);return false;">BACK</a>

    </div>
</div>
<!--{ /if }-->
<!--{* $arrSearchData|@var_dump *}-->
<!--{* $arrPageLayout|@var_dump *}-->
    <!--{* Breadcrumb elements: End *}-->


<!--{/strip}-->
<!--▲HEADER-->
