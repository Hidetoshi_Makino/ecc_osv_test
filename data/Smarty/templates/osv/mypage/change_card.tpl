<!--{*
 * Copyright(c) 2013 Spirit of Co.,Ltd. All rights reserved.
 * http://sbps.ec-cube.pro/
 * Updated: 2013/09/01
 * change_card.tpl
 */
*}-->
<div id="mypagecolumn">

    <h2 class="title"><!--{$tpl_title|h}--></h2>
    <!--{include file=$tpl_navi}-->
    <div id="mycontents_area">
        <!--{if $tpl_error}-->
        <p class="attention"><!--{$tpl_error}-->
        <!--{else}-->
        <!--{if $is_move_payment_service}-->
        <p class="attention">決済会社の入力画面に移動します。「次へ」ボタンを押して下さい。</p>
<script type="text/javascript">//<![CDATA[
$(function() {
    $('p.information').hide();
    });
//]]></script>
        <!--{/if}-->
        <form name="form1" id="form1" method="POST" action="<!--{$tpl_url}-->" autocomplete="off">
        <input type="hidden" name="<!--{$smarty.const.TRANSACTION_ID_NAME}-->" value="<!--{$transactionid}-->" />
        <input type="hidden" name="mode" value="register" />
        <input type="hidden" name="uniqid" value="<!--{$tpl_uniqid}-->" />
        <!--{foreach from=$arrForm item=data key=key}-->
        <input type="hidden" name="<!--{$key|h}-->" value="<!--{$data.value|h}-->" />
        <!--{/foreach}-->

        <div id="payment_form">
        <!--{if $tpl_form_bloc_path != ""}-->
            <!--{include file=$tpl_form_bloc_path}-->
        <!--{/if}-->
        </div>
        </form>
        <!--{/if}-->
    </div>
</div>

