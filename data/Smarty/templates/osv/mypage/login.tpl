<!--{*
/*
 * This file is part of EC-CUBE
 *
 * Copyright(c) 2000-2014 LOCKON CO.,LTD. All Rights Reserved.
 *
 * http://www.lockon.co.jp/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
*}-->



<div class="pagetitle">
    <h2>LOGIN</h2>
    <span>MYページ・ログイン</span>
</div>

<div class="detail">
    <p class="topTxt">
        会員の方は、登録時に入力されたメールアドレスとパスワードでログインしてください。
    </p>

    <form name="login_mypage" id="login_mypage" method="post" action="<!--{$smarty.const.HTTPS_URL}-->frontparts/login_check.php" onsubmit="return eccube.checkLoginFormInputted('login_mypage')">
        <input type="hidden" name="<!--{$smarty.const.TRANSACTION_ID_NAME}-->" value="<!--{$transactionid}-->" />
        <input type="hidden" name="mode" value="login" />
        <input type="hidden" name="url" value="<!--{$smarty.server.REQUEST_URI|h}-->" />

        <!--{assign var=key value="login_email"}-->
        <dl>
            <dt>E-MAIL</dt>
            <dd>
                <span class="attention"><!--{$arrErr[$key]}--></span>
                <input type="text" name="<!--{$key}-->" value="<!--{$tpl_login_email|h}-->" maxlength="<!--{$arrForm[$key].length}-->" style="<!--{$arrErr[$key]|sfGetErrorColor}-->; ime-mode: disabled;" class="input-group__control -email" />
                <p class="login_memory">
                    <!--{assign var=key value="login_memory"}-->
                    <input type="checkbox" name="<!--{$key}-->" value="1"<!--{$tpl_login_memory|sfGetChecked:1}--> id="login_memory" style="float: left;" />
                    <label for="login_memory">メールアドレスをコンピューターに記憶させる</label>
                </p>
            </dd>
        </dl>

        <!--{assign var=key value="login_pass"}-->
        <dl>
            <dt>PASSWORD</dt>
            <dd>
                <span class="attention"><!--{$arrErr[$key]}--></span>
                <input type="password" name="<!--{$key}-->" maxlength="<!--{$arrForm[$key].length}-->" style="<!--{$arrErr[$key]|sfGetErrorColor}-->" class="input-group__control -email" />

                <label class="title" id="forgot_help_view">パスワードを忘れた方</label>
                <div class="description-text text" id="forgot_help">
                    <p>パスワードを忘れた方は、再発行ページより再発行を行ってください。</p>
                    <p>メールアドレスを忘れた方は、お手数ですが問い合せよりお問い合わせください。</p>
                    <div class="btn_area">
                        <div class="btn_inner">
                            <div class="btn_submit -some" onclick="eccube.openWindow('<!--{$smarty.const.HTTPS_URL}-->forgot/<!--{$smarty.const.DIR_INDEX_PATH}-->','forget','600','460',{scrollbars:'no',resizable:'no'}); return false;" target="_blank">再発行</div>
                            <div class="btn_submit -some" onclick="javascript:location.href='<!--{$smarty.const.ROOT_URLPATH}-->contact/<!--{$smarty.const.DIR_INDEX_PATH}-->'">問い合せ</div>
                        </div>
                    </div>
                </div>
            </dd>
        </dl>
<script>
$(function() {
    $('#forgot_help').hide();

    $("#forgot_help_view").click(function() {
/*        if ($('#forgot_help').is(':hidden')) {
            $('#forgot_help').slideToggle(1000);
        } else {*/
            $('#forgot_help').slideToggle(1000);
        // }
        $('#log').fadeToggle(1000);
        $('#reg').fadeToggle(1000);
    });
})
</script>



        <div class="btn_area">
            <input type="submit" class="btn_submit" value="ログイン" name="log" id="log" />
<!--{ if false }--> Need use input tag.
            <div class="btn_submit" onclick="return eccube.checkLoginFormInputted('login_mypage')" name="log" id="log">ログイン</div>
<!--{ /if }-->
        </div>
    </form>
</div>


<div class="detail" id="reg">
    <p class="topTxt">まだ会員登録されていないお客様</p>
    <div class="btn_area">
        <div class="btn_submit" onclick="javascript:location.href='<!--{$smarty.const.ROOT_URLPATH}-->entry/kiyaku.php'">会員登録</div>
    </div>
</div>
