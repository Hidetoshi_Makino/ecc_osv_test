<!--{*
 * This file is part of EC-CUBE
 *
 * Copyright(c) 2000-2014 LOCKON CO.,LTD. All Rights Reserved.
 *
 * http://www.lockon.co.jp/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *}-->


<div class="item-list">


    <div class="list-items list" style="width: 100%;">
        <div class="list-heading heading">
            <div class="category"><!--{$tpl_subtitle|h}--></div>
        </div>

    <!--{* Pagenation: Start *}-->
<script type="text/javascript">//<![CDATA[
    
    // 並び順を変更
    function fnChangeOrderby(orderby) {
        eccube.setValue('orderby', orderby);
        eccube.setValue('pageno', 1);
        eccube.submitForm();
    }
    // 表示件数を変更
    function fnChangeDispNumber(dispNumber) {
        eccube.setValue('disp_number', dispNumber);
        eccube.setValue('pageno', 1);
        eccube.submitForm();
    }
    
//]]></script>
    <form name="form1" id="form1" method="get" action="?">
        <input type="hidden" name="<!--{$smarty.const.TRANSACTION_ID_NAME}-->" value="<!--{$transactionid}-->" />
        <input type="hidden" name="mode" value="<!--{$mode|h}-->" />
        <!--{* Search config: Start *}-->
        <input type="hidden" name="category_id" value="<!--{$arrSearchData.category_id|h}-->" />
        <input type="hidden" name="maker_id" value="<!--{$arrSearchData.maker_id|h}-->" />
        <input type="hidden" name="name" value="<!--{$arrSearchData.name|h}-->" />
        <!--{* Search config: End *}-->
        <!--{* Pagination: Start *}-->
        <input type="hidden" name="orderby" value="<!--{$orderby|h}-->" />
        <input type="hidden" name="disp_number" value="<!--{$disp_number|h}-->" />
        <input type="hidden" name="pageno" value="<!--{$tpl_pageno|h}-->" />
        <!--{* Pagenation: End *}-->
        <input type="hidden" name="rnd" value="<!--{$tpl_rnd|h}-->" />

<!--{ if $tpl_strnavi }-->
<!--{ assign var="page_max" value=$tpl_linemax/$disp_number|ceil}-->
<!--{* math equation=ceil($tpl_linemax/$disp_number) *}-->
<!--{ strip }-->
    <div class="pagination-outer pagination -top">
        <div class="items"><!--{$tpl_linemax}--></div>
        <div class="pages-outer pages">
    <!--{ if $tpl_pageno > 1}-->
            <a class="prev" href="?category_id=<!--{$arrSearchData.category_id|h}-->&amp;pageno=<!--{$tpl_pageno-1|h}-->" onclick="eccube.movePage('<!--{$tpl_pageno-1|h}-->'); return false;">PREVIOUS</a>
    <!--{ else }-->
            <a class="prev -inactive" href="">PREVIOUS</a>
    <!--{ /if }-->
            <div class="page">
                Page <!--{$tpl_pageno|default:1}--> of <!--{$page_max}--></div>
    <!--{ if $tpl_pageno != $page_max && $page_max != 1 }-->
            <a class="next" href="?category_id=<!--{$arrSearchData.category_id|h}-->&amp;pageno=<!--{$tpl_pageno+1|h}-->" onclick="eccube.movePage('<!--{$tpl_pageno+1|h}-->'); return false;">NEXT</a>
    <!--{ else }-->
            <a class="next -inactive" href="">NEXT</a>
    <!--{ /if }-->
        </div>
    </div>
<!--{ /strip }-->
<!--{ /if }-->
    </form>

    <!--{* Pagenation: End *}-->



<ul class="item-block">
    <!--{ foreach from=$arrProducts item=arrProduct name=arrProducts}-->
    <!--{assign var=id value=$arrProduct.product_id}-->
    <li class="items -all -panel">
        <div class="items-inner inner -center">
            <a href="<!--{$smarty.const.P_DETAIL_URLPATH}--><!--{$arrProduct.product_id|u}-->">
                <div class="image-outer outer">
                    <img src="<!--{$smarty.const.IMAGE_SAVE_URLPATH}--><!--{$arrProduct.product_id}-->/list.jpg" onerror="this.src='<!--{$smarty.const.IMAGE_SAVE_URLPATH}-->noimage_main_list.svg';">
                </div>
                <div class="credit-outer">
                    <!--{* <span class="credit brand">BRAND</span> *}-->
                    <span class="credit name"><!--{ $arrProduct.name|h }--></span>
                    <!--{* <span class="credit category">MAKE UP</span> *}-->
                    <!-- <span class="credit num">03704</span> -->
                        <div class="price-outer credit -list -default">
                        <span class="price">¥<!--{ strip }-->
<!--{if $arrProduct.price02_min_inctax == $arrProduct.price02_max_inctax}-->
    <!--{$arrProduct.price02_min_inctax|n2s}-->
<!--{else}-->
    <!--{$arrProduct.price02_min_inctax|n2s}-->～<!--{$arrProduct.price02_max_inctax|n2s}-->
<!--{/if}-->
                            <!--{ /strip }--></span>
                    </div>

<!--{ if count($productStatus[$id]) > 0 }-->
    <!--{foreach from=$productStatus[$id] item=status}-->
        <!--{ if $status == 1 }-->
                    <span class="credit status -new"></span>
        <!--{ elseif $status == 2 }-->
                    <span>残りわずか</span>
        <!--{ elseif $status == 3 }-->
                    <span>ポイント２倍</span>
        <!--{ elseif $status == 4 }-->
                    <span>オススメ</span>
        <!--{ elseif $status == 5 }-->
                    <span>限定品</span>
        <!--{ else }-->
                    <span>Other</span>
        <!--{ /if }-->
    <!--{ /foreach }-->
<!--{ /if }-->


                </div>
            </a>
<!--{*
            <div class="color-outer -hide">
                <ul class="color-list list">
                    <li class="tip-outer colors"><img class="tip" src="http://placehold.jp/ff6464/ff6464/30x30.png"><br><span class="num">9409</span></li>
                    <li class="tip-outer colors"><img class="tip" src="http://placehold.jp/46e2a1/46e2a1/30x30.png"><br><span class="num">9414</span></li>
                    <li class="tip-outer colors"><img class="tip" src="http://placehold.jp/ce1818/ce1818/30x30.png"><br><span class="num">9413</span></li>
                </ul>
            </div>
*}-->
        </div>
    </li>
    <!--{ /foreach }-->
</ul>



    <!--{* Pagenation: Start *}-->
    <form name="form1" id="form1" method="get" action="?">
        <input type="hidden" name="<!--{$smarty.const.TRANSACTION_ID_NAME}-->" value="<!--{$transactionid}-->" />
        <input type="hidden" name="mode" value="<!--{$mode|h}-->" />
        <!--{* Search config: Start *}-->
        <input type="hidden" name="category_id" value="<!--{$arrSearchData.category_id|h}-->" />
        <input type="hidden" name="maker_id" value="<!--{$arrSearchData.maker_id|h}-->" />
        <input type="hidden" name="name" value="<!--{$arrSearchData.name|h}-->" />
        <!--{* Search config: End *}-->
        <!--{* Pagination: Start *}-->
        <input type="hidden" name="orderby" value="<!--{$orderby|h}-->" />
        <input type="hidden" name="disp_number" value="<!--{$disp_number|h}-->" />
        <input type="hidden" name="pageno" value="<!--{$tpl_pageno|h}-->" />
        <!--{* Pagenation: End *}-->
        <input type="hidden" name="rnd" value="<!--{$tpl_rnd|h}-->" />

<!--{ if $tpl_strnavi }-->
<!--{ assign var="page_max" value=$tpl_linemax/$disp_number|ceil}-->
<!--{* math equation=ceil($tpl_linemax/$disp_number) *}-->
<!--{ strip }-->
    <div class="pagination-outer pagination -top">
        <div class="items"><!--{$tpl_linemax}--></div>
        <div class="pages-outer pages">
    <!--{ if $tpl_pageno > 1}-->
            <a class="prev" href="?category_id=<!--{$arrSearchData.category_id|h}-->&amp;pageno=<!--{$tpl_pageno-1|h}-->" onclick="eccube.movePage('<!--{$tpl_pageno-1|h}-->'); return false;">PREVIOUS</a>
    <!--{ else }-->
            <a class="prev -inactive" href="">PREVIOUS</a>
    <!--{ /if }-->
            <div class="page">
                Page <!--{$tpl_pageno|default:1}--> of <!--{$page_max}--></div>
    <!--{ if $tpl_pageno != $page_max && $page_max != 1 }-->
            <a class="next" href="?category_id=<!--{$arrSearchData.category_id|h}-->&amp;pageno=<!--{$tpl_pageno+1|h}-->" onclick="eccube.movePage('<!--{$tpl_pageno+1|h}-->'); return false;">NEXT</a>
    <!--{ else }-->
            <a class="next -inactive" href="">NEXT</a>
    <!--{ /if }-->
        </div>
    </div>
<!--{ /strip }-->
<!--{ /if }-->
    </form>

    <!--{* Pagenation: End *}-->


    </div>
</div>
