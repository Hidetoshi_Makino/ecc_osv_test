<!--{*
 * This file is part of EC-CUBE
 *
 * Copyright(c) 2000-2014 LOCKON CO.,LTD. All Rights Reserved.
 *
 * http://www.lockon.co.jp/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *}-->



<!--{* $arrProduct|@var_dump *}-->


<!---{* Moved header.tpl *}-->
<!--{ if false }-->
<div class="bread-outer">
    <div class="bread-inner inner">
        <a class="bread root" href="/">HOME</a>
    <!--{section name=r loop=$arrRelativeCat[0]}-->
        <a class="bread" href="<!--{$smarty.const.ROOT_URLPATH}-->products/list.php?category_id=<!--{$arrRelativeCat[0][r].category_id}-->"><!--{$arrRelativeCat[0][r].category_name|h}--></a>
    <!--{/section}-->

        <a class="bread current"><!--{$arrProduct.name}--></a>
        <a href="#" class="bread back" onclick="javascript:window.history.back(-1);return false;">BACK</a>
    </div>
</div>
<!--{ /if }-->



<!--{**
      *
      * 画像数を確認する。
      * 本来ならばclass_exに書く
      *
      * *}-->
<!--{ php }-->
$domain = $_SERVER['SERVER_NAME'];
$product_id = $_GET['product_id'];
$dir = IMAGE_SAVE_REALDIR.$product_id.'/item_image/';

$filelist=scandir($dir);
$count=count($filelist);
$count_jpg=count(array_filter($filelist, function($val){
    return stristr($val, ".jpg") !== false;
}));
$count_jpg = floor($count_jpg / 3);
#var_dump('JPG: '. $count_jpg);

/**
for($i =0; $i < $count; $i++){
    $filename = $filelist[$i];
    $file_ext = substr($filename, strrpos($filename, '.') + 1);
    if($file_ext == 'jpg'){
        #echo '<li><img src="/upload/save_image/'.$product_id.'/item_image/'.$filelist[$i].'"></li>';
     }

// 商品画像が存在しない場合の画像をセット
    if($count == 1){
    echo '<li><img src="/upload/save_image/noimage_main.svg"></li>';
    }
}
*/
<!--{ /php }-->

<div class="item-detail">
    <div id="" class="image-zoom zoom"></div>
    <div class="item-slider slider">
        <div class="swiper-container itemmainslide-outer swiper-container-item-main">
            <div class="drift-zoom-icon"></div>
            <div class="drift-flash-message"></div>
    <ul class="swiper-wrapper slider" data-image-length="10">


<!--{ php }-->
if($count_jpg > 0){
    for($i = 1; $i <= $count_jpg; $i++){
        echo '<li class="swiper-slide -zoom"><img class="image-zoom'. $i .'" src="/upload/save_image/'.$product_id.'/item_image/item_2_'. $i . '.jpg" data-zoom="/upload/save_image/'.$product_id.'/item_image/item_3_'. $i. '.jpg"></li>';
    }
} else {
    echo '<li class="swiper-slide -zoom"><img class="image-zoom1" src="/upload/save_image/noimage_main.svg"  data-zoom="/upload/save_image/noimage_main.svg"></li>';
}
<!--{ /php }-->


    </ul>
    <div class="swiper-button-prev swiper-button-black -main"></div>
    <div class="swiper-button-next swiper-button-black -main"></div>
</div>
<div class="swiper-container itemthumbslide-outer swiper-container-item-thumb">
    <ul class="swiper-wrapper slider">

<!--{ php }-->
if($count_jpg > 0){
    for($i = 1; $i <= $count_jpg; $i++){
        echo '<li class="swiper-slide"><img src="/upload/save_image/'.$product_id.'/item_image/item_1_'. $i . '.jpg"></li>';
    }
} else {
    echo '<li class="swiper-slide"><img src="/upload/save_image/noimage_main.svg"></li>';
}
<!--{ /php }-->

    </ul>
    <!--
    <div class="swiper-button-next swiper-button-black -thumb"></div>
    <div class="swiper-button-prev swiper-button-black -thumb"></div>
    -->
</div>

<script>
(function(){
    THISPAGE.sliderLength   = $('.swiper-slide.-zoom').length;
    THISPAGE.sliderThumb    = THISPAGE.sliderLength < 2? false:true;

    initItemSwiper(THISPAGE.sliderThumb);
})();
$(window).on('load',function(){
    initDrift($('body').attr('data-device'));
});

$(function(){

    // swiper initialize v4.3.3
    // var itemMainSlider = new Swiper('.swiper-container-item-main', {
    //  spaceBetween : 10,
    //  touchRatio: 0.2,
    //  followFinger: false,
    //  navigation: {
    //      nextEl: '.swiper-button-next',
    //      prevEl: '.swiper-button-prev',
    //  },
    //  slidesPerView: 1,
    // });
    // var itemThumbSlider = new Swiper('.swiper-container-item-thumb', {
    //  spaceBetween: 10,
    //  slidesPerView: 'auto',
    //  touchRatio: 0.9,
    //  slideToClickedSlide: true,
    //  centeredSlides: true,
    // });
    // itemMainSlider.controller.control = itemThumbSlider;
    // itemThumbSlider.controller.control = itemMainSlider;

    // Drift.jsの複数画像に対するイニシャライズの処理
    // let len = Number($('.swiper-wrapper').attr('data-image-length'));
    // for(let i=1; i<=len; i++){
    //  new Drift(document.querySelector('.image-zoom'+i+''), {
    //      paneContainer: document.querySelector('.image-zoom'),
    //      zoomFactor: 3,
    //      inlinePane: 768,
    //      inlineOffsetY: -70,
    //      touchDelay: 250
    //  }); 
    // }
    // $('.swiper-slide.-zoom').hover(
    //  function(){
    //      if(checkDevice() === 'pc'){
    //          $('.item-credit').addClass('-zoomed');
    //      }
    //  },
    //  function(){
    //      if(checkDevice() === 'pc'){
    //          $('.item-credit').removeClass('-zoomed');
    //      }
    //  }
    // );
});
</script>


    </div>
    <div class="item-credit credit">
        <div class="credit-block credit">
    <span class="credit brand"><!--{$arrProduct.maker_name}--></span>
    <!--{* Item status: Start *}-->
        <!--{ assign var=ps value=$productStatus[$tpl_product_id] }-->
            <!--{if count($ps) > 0}-->
                <!--{foreach from=$ps item=status}-->
                    <!--{ if $status == 1 }-->
    <span class="credit status -new"></span>
                    <!--{ elseif $status == 2 }-->
    <span>残りわずか</span>
                    <!--{ elseif $status == 3 }-->
    <span>ポイント２倍</span>
                    <!--{ elseif $status == 4 }-->
    <span>オススメ</span>
                    <!--{ elseif $status == 5 }-->
    <span>限定品</span>
                    <!--{ else }-->
    <span>Other</span>
                    <!--{ /if }-->
                <!--{ /foreach }-->
            <!--{ /if }-->
    <!--{* Item status: End *}-->
    <span class="credit name"><!--{$arrProduct.name}--></span>

<!--{ if ($arrProduct.price01_min == $arrProduct.price02_min
        && $arrProduct.price01_max == $arrProduct.price02_max)
        || $arrProduct.price01_min == NULL }-->
    <!-- 通常価格 -->
    <div class="price-outer credit -detail -default">
        <span class="proper"></span>
        <span class="arrow"></span>
        <span class="price"><!--{ strip }-->
            <!--{ if $arrProduct.price02_min_inctax != $arrProduct.price02_max_inctax }-->
                ¥<!--{$arrProduct.price02_min_inctax|n2s}-->&nbsp;~&nbsp;
            <!--{ /if }-->
            ¥<!--{$arrProduct.price02_max_inctax|n2s}--><!--{ /strip }--><span class="tax">(税込)</span></span>
        <span class="rate"></span>
    </div>
<!--{ else }-->
    <!-- セール価格 -->
        <!--{php}-->
/**
 * 値引率を計算
 * 本来ならば class_extends に書くべき
 */
//var_dump($this->_tpl_vars['arrProduct']);
$price01 = $this->_tpl_vars['arrProduct']['price01_min'];
$price02 = $this->_tpl_vars['arrProduct']['price02_min'];
if ($price01 != $price02) {
    $rate = floor(100 - $price02 * 100 / $price01);
    //var_dump($rate);
    $this->_tpl_vars['arrProduct']['discount_rate'] = $rate;
}
        <!--{/php}-->
    <div class="price-outer credit -detail -sale">
        <span class="proper"><!--{ strip }-->
            <!--{ if $arrProduct.price01_min_inctax != $arrProduct.price01_max_inctax }-->
                ¥<!--{$arrProduct.price01_min_inctax|n2s}-->&nbsp;~&nbsp;
            <!--{ /if }-->
            ¥<!--{$arrProduct.price01_max_inctax|n2s}--><!--{ /strip }--></span>
        <span class="arrow"></span>
        <span class="price"><!--{ strip }-->
            <!--{ if $arrProduct.price02_min_inctax != $arrProduct.price02_max_inctax }-->
                ¥<!--{$arrProduct.price02_min_inctax|n2s}-->&nbsp;~&nbsp;
            <!--{ /if }-->
            ¥<!--{$arrProduct.price02_max_inctax|n2s}--><!--{ /strip }--><span class="tax">(税込)</span></span>
        <span class="rate">(<!--{ $arrProduct.discount_rate }-->% OFF)</span>
    </div>
<!--{ /if }-->


    <!--{ if false }-->
    <!-- Display none: Start -->

<!--{ if $smarty.const.USE_POINT !== false }-->
    <span class="credit info -point">還元ポイント : <!--{strip}-->
        <!--{if $arrProduct.price02_min == $arrProduct.price02_max}-->
            <!--{$arrProduct.price02_min|sfPrePoint:$arrProduct.point_rate|n2s}-->
        <!--{else}-->
            <!--{if $arrProduct.price02_min|sfPrePoint:$arrProduct.point_rate == $arrProduct.price02_max|sfPrePoint:$arrProduct.point_rate}-->
                <!--{$arrProduct.price02_min|sfPrePoint:$arrProduct.point_rate|n2s}-->
            <!--{else}-->
                <!--{$arrProduct.price02_min|sfPrePoint:$arrProduct.point_rate|n2s}-->～<!--{$arrProduct.price02_max|sfPrePoint:$arrProduct.point_rate|n2s}-->
            <!--{/if}-->
        <!--{/if}-->
    <!--{/strip}-->ポイント</span>
    <!--{* point rate:<!--{$arrProduct.point_rate}--> *}-->
<!--{ /if }-->


    <span class="credit info -favorite">お気に入り登録数 : 119人</span>
    <span class="credit info -favorite">お気に入り登録</span>

    <!-- Display none: End -->
    <!--{ /if }-->
</div>


<!--{* Cart section: Start *}-->
<div class="cart-block cart -pc">
    <ul class="sku-list list">
<!--{section name=i loop=$arrProductsClassList}-->
    <!--{* $arrProductsClassList[i]|@var_dump *}-->


    <!--{ if ($arrProductsClassList[i].classcategory_id1 == 0
            && $arrProductsClassList[i].classcategory_id2 == 0)}-->

        <!--{**
            * Products without variations
            **}-->
        <li class="item-box items">
            <div class="box-inner inner" data-item-sku="9408">
                <div class="thumb">
                    <img class="clipimage" src="<!--{$smarty.const.IMAGE_SAVE_URLPATH}--><!--{$arrProduct.product_id|h}-->/item_image/item_1_1.jpg" onerror="this.src='<!--{$smarty.const.IMAGE_SAVE_URLPATH}-->noimage_main.svg';" />
                </div>
        <!--{ if ($arrProductsClassList[i].stock > 0
                    || $arrProductsClassList[i].stock_unlimited == 1)}-->
                <!-- Have stock -->
                <div class="item-status status">
                    <span class="num"><!--{ $arrProductsClassList[i].product_code|h }--></span>
                    <span class="name"></span>

                    <span class="stock -default">在庫あり</span>
                </div>
<form name="form<!--{$arrProductsClassList[i].classcategory_id1|h}--><!--{$arrProductsClassList[i].classcategory_id2|h}-->" id="form<!--{$arrProductsClassList[i].classcategory_id1|h}--><!--{$arrProductsClassList[i].classcategory_id2|h}-->" method="post" action="<!--{$smarty.server.REQUEST_URI|escape}-->">
    <input type="hidden" name="<!--{$smarty.const.TRANSACTION_ID_NAME}-->" value="<!--{$transactionid}-->" />
    <input type="hidden" name="mode" value="cart" />
    <input type="hidden" name="product_id" value="<!--{$tpl_product_id}-->" />
    <input type="hidden" class="box60" name="quantity" value="1" maxlength="<!--{$smarty.const.INT_LEN}-->" />
    <input type="hidden" name="product_class_id" value="<!--{$arrProductsClassList[i].product_class_id|h}-->" id="product_class_id" />
    <input type="hidden" name="classcategory_id1" id="<!--{$arrProductsClassList[i].classcategory_id1|h}-->" value="<!--{$arrProductsClassList[i].classcategory_id1|h}-->" />
    <input type="hidden" name="classcategory_id2" id="<!--{$arrProductsClassList[i].classcategory_id2|h}-->" value="<!--{$arrProductsClassList[i].classcategory_id2|h}-->" />
                <div class="cart -default" data-add-cart="false" data-add-request="false" onclick="javascript:void(document.form<!--{$arrProductsClassList[i].classcategory_id1|h}--><!--{$arrProductsClassList[i].classcategory_id2|h}-->.submit())"></div>
</form>
        <!--{ else }-->
                <!-- Dont have stock -->
                <div class="item-status status">
                    <span class="num"><!--{ $arrProductsClassList[i].product_code|h }--></span>
                    <span class="name"></span>
                    <span class="stock -sold">SOLDOUT</span>
                </div>
<form>
                <div class="cart -default" data-add-cart="false" data-add-request="false" style=";"></div>
</form>
        <!--{ /if }-->
<!--{*
                <div class="fav" data-add-fav="false"></div>
*}-->
            </div>
        </li>

    <!--{ else }-->


        <!--{**
            * Products with variations
            **}-->
        <!--{ if ($arrProductsClassList[i].classcategory_id1 !=
                $arrProductsClassList[i.index_prev].classcategory_id1)
                || $smarty.section.i.first }-->
        <li class="item-box items">
        <!--{ /if }-->
            <div class="box-inner inner">
                <div class="thumb">
                    <img class="clipimage" src="<!--{$smarty.const.IMAGE_SAVE_URLPATH}--><!--{$tpl_product_id}-->/tmb_<!--{$arrProductsClassList[i].classcategory_id1}-->.jpg" onerror="this.src='<!--{$smarty.const.IMAGE_SAVE_URLPATH}-->noimage_main.svg';" />
                </div>
        <!--{ if ($arrProductsClassList[i].stock > 0
                    || $arrProductsClassList[i].stock_unlimited == 1)}-->
                <!-- Have stock -->
                <div class="item-status status">
                    <span class="num"><!--{ $arrProductsClassList[i].product_code|h }--></span>
                    <span class="name"><!--{$arrProductsClassList[i].classcategory_name1 }--></span>
                    <span class="name"><!--{$arrProductsClassList[i].classcategory_name2 }--></span>

                    <span class="stock -default">在庫あり</span>
                </div>
<form name="form<!--{$arrProductsClassList[i].classcategory_id1|h}--><!--{$arrProductsClassList[i].classcategory_id2|h}-->" id="form<!--{$arrProductsClassList[i].classcategory_id1|h}--><!--{$arrProductsClassList[i].classcategory_id2|h}-->" method="post" action="<!--{$smarty.server.REQUEST_URI|escape}-->">
    <input type="hidden" name="<!--{$smarty.const.TRANSACTION_ID_NAME}-->" value="<!--{$transactionid}-->" />
    <input type="hidden" name="mode" value="cart" />
    <input type="hidden" name="product_id" value="<!--{$tpl_product_id}-->" />
    <input type="hidden" class="box60" name="quantity" value="1" maxlength="<!--{$smarty.const.INT_LEN}-->" />
    <input type="hidden" name="product_class_id" value="<!--{$arrProductsClassList[i].product_class_id|h}-->" id="product_class_id" />
    <input type="hidden" name="classcategory_id1" id="<!--{$arrProductsClassList[i].classcategory_id1|h}-->" value="<!--{$arrProductsClassList[i].classcategory_id1|h}-->" />
    <input type="hidden" name="classcategory_id2" id="<!--{$arrProductsClassList[i].classcategory_id2|h}-->" value="<!--{$arrProductsClassList[i].classcategory_id2|h}-->" />
                <div class="cart -default" data-add-cart="false" data-add-request="false" onclick="javascript:void(document.form<!--{$arrProductsClassList[i].classcategory_id1|h}--><!--{$arrProductsClassList[i].classcategory_id2|h}-->.submit())"></div>
</form>
        <!--{ else }-->
                <!-- Dont have stock -->
                <div class="item-status status">
                    <span class="num"><!--{ $arrProductsClassList[i].product_code|h }--></span>
                    <span class="name"><!--{$arrProductsClassList[i].classcategory_name1 }--></span>
                    <span class="name"><!--{$arrProductsClassList[i].classcategory_name2 }--></span>
                    <span class="stock -sold">SOLDOUT</span>
                </div>
<form>
                <div class="cart -default" data-add-cart="false" data-add-request="false" style="visibility: hidden;"></div>
</form>
        <!--{ /if }-->

<!--{* Product classcastegory level favalite
                <div class="fav" data-add-fav="false"></div>
*}-->
            </div>
        <!--{ if ($arrProductsClassList[i].classcategory_id1 !=
                $arrProductsClassList[i.index_next].classcategory_id1)
                || $smarty.section.i.last }-->
        </li>
        <!--{ /if }-->

    <!--{ /if }-->

<!--{/section}-->
    </ul>
</div>
<!--{* Cart section: End *}-->




<div class="description-block">
    <div class="item-descriptions detail items">
        <input id="tab-detail" class="accordion-tab tab" type="checkbox" name="tabs">
        <label for="tab-detail" class="title">ITEM DETAIL</label>
        <div class="description-text text">
            <p class="text"><!--{$arrProduct.main_comment|nl2br_html}--></p>
        </div>
    </div>

<!--{ if false }-->
<!-- Display none: Start -->
    <div class="item-descriptions materials items">
        <input id="tab-materials" class="accordion-tab tab" type="checkbox" name="tabs">
        <label for="tab-materials" class="title">MATERIALS</label>
        <div class="description-text text">
            <p class="text">マテリアル</p>
        </div>
    </div>
    <div class="item-descriptions color items">
        <input id="tab-colorlist" class="accordion-tab tab" type="checkbox" name="tabs">
        <label for="tab-colorlist" class="title">COLOR LIST</label>
        <div class="description-text text">
            <p class="text">カラー</p>
        </div>
    </div>
<!-- Display none: End -->
<!--{ /if }-->

    <div class="item-descriptions notes items">
        <div class="title"><a href="">返品・交換について</a></div>
    </div>

<!--{ if false }-->
<!-- Display none: Start -->
    <div class="item-descriptions share items">
        <div class="title">SHARE</div>
        <ul class="share-list share">
    <li class="share-item items"><a href="" target="_blank"><svg viewBox="0 0 100 100" class="svg use-instagram"><use xlink:href="<!--{$TPL_URLPATH}-->img/svg/sprite.svg#use_instagram"></use></svg></a></li>
    <li class="share-item items"><a href="" target="_blank"><svg viewBox="0 0 100 100" class="svg use-twitter"><use xlink:href="<!--{$TPL_URLPATH}-->img/svg/sprite.svg#use_twitter"></use></svg></a></li>
    <li class="share-item items"><a href="" target="_blank"><svg viewBox="0 0 100 100" class="svg use-facebook"><use xlink:href="<!--{$TPL_URLPATH}-->img/svg/sprite.svg#use_facebook"></use></svg></a></li>
        </ul>
    </div>
<!-- Display none: End -->
<!--{ /if }-->

</div>

    </div>
</div>


<!--{* $arrRecommend|@var_dump *}-->
<!--{**
    * Recommends
    *}-->
<!--{ if $arrRecommend }-->
<div class="item-section section -lastchild">
    <h3 class="heading">RECOMMEND</h3>
    <ul class="item-block -col5">
    <!--{ section name=i loop=$arrRecommend }-->
        <!--{ if ($arrRecommend[i].del_flg == 0)
                && ($arrRecommend[i].status == 1)}-->
        <li class="items -recommend -panel">
        <div class="items-inner inner -center">
            <a href="<!--{$smarty.const.P_DETAIL_URLPATH}--><!--{$arrRecommend[i].product_id|u}-->">
                <div class="image-outer outer">
                    <!--{**
                        * TODO: 画像サイズは考える
                        *}-->
                    <img src="<!--{$smarty.const.IMAGE_SAVE_URLPATH}--><!--{$arrRecommend[i].product_id|h}-->/list.jpg" onerror="this.src='<!--{$smarty.const.IMAGE_SAVE_URLPATH}-->noimage_main_list.svg';">
                </div>
                <div class="credit-outer">
                    <!--{* <span class="credit brand">BRAND</span> *}-->
                    <span class="credit name"><!--{ $arrRecommend[i].name|h }--></span>
                    <!--{* <span class="credit category">MAKE UP</span> *}-->
                    <div class="price-outer credit -list -default">
                        <span class="price">¥<!--{ $arrRecommend[i].price02_min_inctax|n2s }--><!--{ if ($arrRecommend[i].price02_min_inctax != $arrRecommend[i].price02_max_inctax) }--> ~<!--{ /if }--></span>
                    </div>
                </div>
            </a>
<!--{*
            <div class="color-outer -hide">
                <ul class="color-list list">
                    <li class="tip-outer colors"><img class="tip" src="http://placehold.jp/ff6464/ff6464/30x30.png"><br><span class="num">9409</span></li>
                    <li class="tip-outer colors"><img class="tip" src="http://placehold.jp/46e2a1/46e2a1/30x30.png"><br><span class="num">9414</span></li>
                    <li class="tip-outer colors"><img class="tip" src="http://placehold.jp/ce1818/ce1818/30x30.png"><br><span class="num">9413</span></li>
                </ul>
            </div>
*}-->
        </div>
        </li>
        <!--{ /if }-->
    <!--{ /section }-->
    </ul>
</div>
<!--{ /if }-->
