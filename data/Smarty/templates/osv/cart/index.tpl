<!--{*
 * This file is part of EC-CUBE
 *
 * Copyright(c) 2000-2014 LOCKON CO.,LTD. All Rights Reserved.
 *
 * http://www.lockon.co.jp/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *}-->



<div class="pagetitle">
    <h2 class="title">CART</h2>
    <span>現在のカゴの中</span>
</div>

<!--{* Point infomation: Start *}-->
    <!--{ if false }-->
    <!-- Display none: Start -->
<!--{if $smarty.const.USE_POINT !== false || count($arrProductsClass) > 0}-->
    <!--{if $smarty.const.USE_POINT !== false}-->
<div class="point_announce">
    <p>
        <!--{if $tpl_login}-->
        <span class="user_name"><!--{$tpl_name|h}--> 様</span>の、現在の所持ポイントは「<span class="point"><b><!--{$tpl_user_point|n2s|default:0|h}--></b> pt</span>」です。<br />
        <!--{else}-->
        ポイント制度をご利用になられる場合は、会員登録後ログインしてくださいますようお願い致します。<br />
        <!--{/if}-->
        ポイントは商品購入時に<span class="price">1pt＝<!--{$smarty.const.POINT_VALUE|h}-->円</span>として使用することができます。
    </p>
</div>
    <!--{/if}-->
<!--{/if}-->
    <!-- Display none: End -->
    <!--{ /if }-->
<!--{* Point infomation: End *}-->

<p class="totalmoney_area">
    <!--{* カゴの中に商品がある場合にのみ表示 *}-->
    <!--{if count($cartKeys) > 1}-->
    <span class="attentionSt"><!--{foreach from=$cartKeys item=key name=cartKey}--><!--{$arrProductType[$key]|h}--><!--{if !$smarty.foreach.cartKey.last}-->、<!--{/if}--><!--{/foreach}-->は同時購入できません。<br />
        お手数ですが、個別に購入手続きをお願い致します。
    </span>
    <!--{/if}-->

    <!--{if strlen($tpl_error) != 0}-->
    <p class="attention"><!--{$tpl_error|h}--></p>
    <!--{/if}-->

    <!--{if strlen($tpl_message) != 0}-->
    <p class="attention"><!--{$tpl_message|h|nl2br}--></p>
    <!--{/if}-->
</p>

<div>
    <!--{if count($cartItems) > 0}-->
    <!--{foreach from=$cartKeys item=key}-->

    <form name="form<!--{$key|h}-->" id="form<!--{$key|h}-->" class="cart" method="post" action="?">
        <input type="hidden" name="<!--{$smarty.const.TRANSACTION_ID_NAME|h}-->" value="<!--{$transactionid|h}-->" />
        <input type="hidden" name="mode" value="confirm" />
        <input type="hidden" name="cart_no" value="" />
        <input type="hidden" name="cartKey" value="<!--{$key|h}-->" />
        <input type="hidden" name="category_id" value="<!--{$tpl_category_id|h}-->" />
        <input type="hidden" name="product_id" value="<!--{$tpl_product_id|h}-->" />
        <!--{if count($cartKeys) > 1}-->
        <h3><!--{$arrProductType[$key]|h}--></h3>
        <!--{assign var=purchasing_goods_name value=$arrProductType[$key]}-->
        <!--{else}-->
        <!--{assign var=purchasing_goods_name value="カート内"}-->
        <!--{/if}-->
        <p class="cart_text">
            <!--{$purchasing_goods_name|h}-->の合計金額は「<span class="price"><!--{$tpl_total_inctax[$key]|n2s|h}-->円</span>」です。
            <!--{if $key != $smarty.const.PRODUCT_TYPE_DOWNLOAD}-->
            <!--{if $arrInfo.free_rule > 0}-->
            <!--{if !$arrData[$key].is_deliv_free}-->
            あと「<span class="price"><!--{$tpl_deliv_free[$key]|n2s|h}-->円</span>」で送料無料です！！
            <!--{else}-->
            現在、「<span class="attention">送料無料</span>」です！！
            <!--{/if}-->
            <!--{/if}-->
            <!--{/if}-->
        </p>

        <div class="cart_items">
            <!--{foreach from=$cartItems[$key] item=item}-->
            <div class="cart_item">
                <div class="thumb_image" onclick="javascript:location.href = '<!--{$smarty.const.P_DETAIL_URLPATH}--><!--{$item.productsClass.product_id|h}-->'">
                <!--{ if $item.productsClass.classcategory_id1 == 0 }-->
                    <img src="/upload/save_image/<!--{$item.productsClass.product_id|h}-->/list.jpg" onerror="this.src='<!--{$smarty.const.IMAGE_SAVE_URLPATH|h}-->noimage_main.svg';" />
                <!--{ else }-->
                    <img src="/upload/save_image/<!--{$item.productsClass.product_id|h}-->/tmb_<!--{$item.productsClass.classcategory_id1|h}-->.jpg" onerror="this.src='<!--{$smarty.const.IMAGE_SAVE_URLPATH}-->noimage_main.svg';" />
                <!--{ /if }-->
                </div>

                <div class="item_detail">
                    <h2 onclick="javascript:location.href = '<!--{$smarty.const.P_DETAIL_URLPATH}--><!--{$item.productsClass.product_id|h}-->'"><!--{$item.productsClass.name|h}--></h2>
                        <!--{if $item.productsClass.classcategory_name1 != ""}-->
                    <div class="item_sku">
                        <div class="title">Select</div>
                        <!--{$item.productsClass.classcategory_name1|h}-->
                            <!--{if $item.productsClass.classcategory_name2 != ""}-->
                            &nbsp;-&nbsp;<!--{$item.productsClass.classcategory_name2|h}-->
                            <!--{/if}-->
                    </div>
                        <!--{/if}-->
                    <div class="price">
                        <div class="title">Price</div>
                        &yen;<!--{$item.price_inctax|n2s|h}-->-</div>
                    <div class="subtotal">
                        <div class="title">Subtotal</div>
                        &yen;<!--{$item.total_inctax|n2s|h}-->-</div>


                    <div class="item_detail_quantity">
                        <div class="title">数量</div>
                        <div class="value"><!--{$item.quantity|h}--></div>
                        <ul id="quantity_level">

                            <li><a href="?" onclick="eccube.fnFormModeSubmit('form<!--{$key|h}-->','up','cart_no','<!--{$item.cart_no|h}-->'); return false">＋</a></li>
                            <!--{if $item.quantity > 1}-->
                            <li><a href="?" onclick="eccube.fnFormModeSubmit('form<!--{$key|h}-->','down','cart_no','<!--{$item.cart_no|h}-->'); return false">ー</a></li>

                            <!--{/if}-->
                            <li class="delete_btn"><a href="?" onclick="eccube.fnFormModeSubmit('form<!--{$key|h}-->', 'cartDelete', 'cart_no', '<!--{$item.cart_no|h}-->'); return false;">削除</a></li>

                        </ul>
                    </div>
                    <!--{* $item|@var_dump *}-->
                </div>
            </div>
                <!--{/foreach}-->
        </div>


        <!--{if strlen($tpl_error) == 0}-->
        <p class="cart_text">上記内容でよろしければ<br>「購入手続きへ」ボタンをクリックしてください。</p>
        <!--{/if}-->

        <div class="btn_area">
            <div class="btn_inner">
                <div class="btn_submit -some" onclick="javascript:window.history.back(-1);return false;">
                    戻る</div>

                <div class="btn_submit -some"  onclick="javascript:void(document.form1.submit())">
                    購入手続きへ</div>
            </div>
        </div>

    </form>

    <!--{/foreach}-->
    <!--{else}-->
    <p class="empty"><span class="attention">※ 現在カート内に商品はございません。</span></p>
    <!--{/if}-->
</div>

