<?php
/*
 * This file is part of EC-CUBE
 *
 * Copyright(c) 2000-2014 LOCKON CO.,LTD. All Rights Reserved.
 *
 * http://www.lockon.co.jp/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

require_once CLASS_REALDIR . 'SC_CartSession.php';

class SC_CartSession_Ex extends SC_CartSession
{

    public function __construct($sessKey = 'cart')
    {
        parent::__construct($sessKey);
    }

    /**
     * カート内に会員専用商品が含まれるかをチェック
     *
     * @param   integer $cartKey
     * @param   boolean $isLogin
     * @return array|boolean
     */
    public function chkPaidOnlyItem($cartKey, $isLogin = false)
    {
        $cartItems = $this->getCartList($cartKey);
        if ($isLogin) {
            return $cartItems;
        }

        $success = true;
        foreach ($cartItems as $item) {
            if (1 == $item['productsClass']['is_paid_only']) {
                $success = false;
                break;
            }
        }

        if ($success) {
            return $cartItems;
        }
        else {
            return $success;
        }
    }
}
