<?php
/*
 * This file is part of EC-CUBE
 *
 * Copyright(c) 2000-2014 LOCKON CO.,LTD. All Rights Reserved.
 *
 * http://www.lockon.co.jp/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

require_once CLASS_REALDIR . 'SC_SendMail.php';

/**
 * メール送信クラス(拡張).
 *
 * SC_SendMail をカスタマイズする場合はこのクラスを使用する.
 *
 * @package
 * @author LOCKON CO.,LTD.
 * @version $Id$
 */
class SC_SendMail_Ex extends SC_SendMail
{

    /**
     * TXTメール送信を実行する.
     *
     * 設定された情報を利用して, メールを送信する.
     *
     * @return boolean
     */
    public function sendMimeMail($files)
    {
        $recip = $this->getRecip();

        $objMime =& new Mail_mime("\n");
        $objMime->setTXTBody($this->body);

        foreach ($files as $file) {
            $objMime->addAttachment($file);
        }

        $build_param = array(
            "text_charset" => "JIS",
            "head_charset" => "JIS",
         );
        $body = $objMime->get($build_param);
        $header = $objMime->headers($this->getBaseHeader());

        // メール送信
        $result = $this->objMail->send($recip, $header, $body);

        foreach ($files as $file) {
            unlink($file);
        }
        if (PEAR::isError($result)) {
            // XXX Windows 環境では SJIS でメッセージを受け取るようなので変換する。
            $msg = mb_convert_encoding($result->getMessage(), CHAR_CODE, 'auto');
            $msg = 'メール送信に失敗しました。[' . $msg . ']';
            trigger_error($msg, E_USER_WARNING);
            GC_Utils_Ex::gfDebugLog($header);

            return false;
        }

        return true;
    }
}
