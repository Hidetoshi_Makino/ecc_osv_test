<?php
/*
 * This file is part of EC-CUBE
 *
 * Copyright(c) 2000-2014 LOCKON CO.,LTD. All Rights Reserved.
 *
 * http://www.lockon.co.jp/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

require_once CLASS_REALDIR . 'helper/SC_Helper_Bloc.php';

/**
 * ブロックを管理するヘルパークラス(拡張).
 *
 * LC_Helper_Bloc をカスタマイズする場合はこのクラスを編集する.
 *
 * @package Helper
 * @author pineray
 * @version $Id:$
 */
class SC_Helper_Bloc_Ex extends SC_Helper_Bloc
{

    /**
     * ブロックの情報を取得.
     *
     * @param  integer $bloc_id ブロックID
     * @return array
     */
    public function getBloc($bloc_id)
    {
        $objQuery =& SC_Query_Ex::getSingletonInstance();
        $col = '*';
        $where = 'bloc_id = ? AND device_type_id = ?';
        $arrRet = $objQuery->getRow($col, 'dtb_bloc', $where, array($bloc_id, DEVICE_TYPE_SMARTPHONE));
        if (SC_Utils_Ex::isAbsoluteRealPath($arrRet['tpl_path'])) {
            $tpl_path = $arrRet['tpl_path'];
        } else {
            $tpl_path = SC_Helper_PageLayout_Ex::getTemplatePath($this->device_type_id) . BLOC_DIR . $arrRet['tpl_path'];
        }
        if (file_exists($tpl_path)) {
            $arrRet['bloc_html'] = file_get_contents($tpl_path);
        }

        return $arrRet;
    }
}
