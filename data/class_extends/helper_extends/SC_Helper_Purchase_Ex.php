<?php
/*
 * This file is part of EC-CUBE
 *
 * Copyright(c) 2000-2014 LOCKON CO.,LTD. All Rights Reserved.
 *
 * http://www.lockon.co.jp/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

require_once CLASS_REALDIR . 'helper/SC_Helper_Purchase.php';

/**
 * 商品購入関連のヘルパークラス(拡張).
 *
 * LC_Helper_Purchase をカスタマイズする場合はこのクラスを編集する.
 *
 * @package Helper
 * @author Kentaro Ohkouchi
 * @version $Id$
 */
class SC_Helper_Purchase_Ex extends SC_Helper_Purchase
{

    /**
     * 受注登録を完了する.
     *
     * 引数の受注情報を受注テーブル及び受注詳細テーブルに登録する.
     * 登録後, 受注一時テーブルに削除フラグを立てる.
     *
     * @param array          $orderParams    登録する受注情報の配列
     * @param SC_CartSession $objCartSession カート情報のインスタンス
     * @param integer        $cartKey        登録を行うカート情報のキー
     * @param integer 受注ID
     */
    public function registerOrderComplete($orderParams, &$objCartSession, $cartKey)
    {
        $objQuery =& SC_Query_Ex::getSingletonInstance();

        // 不要な変数を unset
        $unsets = array('mailmaga_flg', 'deliv_check', 'point_check', 'password',
                        'reminder', 'reminder_answer', 'mail_flag', 'session');
        foreach ($unsets as $unset) {
            unset($orderParams[$unset]);
        }

        // 対応状況の指定が無い場合は新規受付
        if (SC_Utils_Ex::isBlank($orderParams['status'])) {
            $orderParams['status'] = ORDER_NEW;
        }

        $orderParams['del_flg'] = '0';
        $orderParams['create_date'] = 'CURRENT_TIMESTAMP';
        $orderParams['update_date'] = 'CURRENT_TIMESTAMP';

        $this->registerOrder($orderParams['order_id'], $orderParams, $cartKey);

        // 詳細情報を取得
        $cartItems = $objCartSession->getCartList($cartKey, $orderParams['order_pref'], $orderParams['order_country_id']);

        // 詳細情報を生成
        $objProduct = new SC_Product_Ex();
        $i = 0;
        $arrDetail = array();
        foreach ($cartItems as $item) {
            $p =& $item['productsClass'];
            $arrDetail[$i]['order_id'] = $orderParams['order_id'];
            $arrDetail[$i]['product_id'] = $p['product_id'];
            $arrDetail[$i]['product_class_id'] = $p['product_class_id'];
            $arrDetail[$i]['product_name'] = $p['name'];
            $arrDetail[$i]['product_code'] = $p['product_code'];
            $arrDetail[$i]['classcategory_name1'] = $p['classcategory_name1'];
            $arrDetail[$i]['classcategory_name2'] = $p['classcategory_name2'];
            $arrDetail[$i]['point_rate'] = $item['point_rate'];
            $arrDetail[$i]['price'] = $item['price'];
            $arrDetail[$i]['quantity'] = $item['quantity'];
            $arrDetail[$i]['tax_rate'] = $item['tax_rate'];
            $arrDetail[$i]['tax_rule'] = $item['tax_rule'];
            $arrDetail[$i]['tax_adjust'] = $item['tax_adjust'];

            // 在庫の減少処理
            if (!$objProduct->reduceStock($p['product_class_id'], $item['quantity'])) {
                $objQuery->rollback();
                SC_Utils_Ex::sfDispSiteError(SOLD_OUT, '', true);
            }
            $i++;
        }
        $this->registerOrderDetail($orderParams['order_id'], $arrDetail);

        $objQuery->update('dtb_order_temp', array('del_flg' => 1),
                          'order_temp_id = ?',
                          array(SC_SiteSession_Ex::getUniqId()));

        return $orderParams['order_id'];
    }


    /**
     * 受注情報を登録する.
     *
     * 既に受注IDが存在する場合は, 受注情報を更新する.
     * 引数の受注IDが, 空白又は null の場合は, 新しく受注IDを発行して登録する.
     *
     * @param  integer $order_id  受注ID
     * @param  array   $arrParams 受注情報の連想配列
     * @return integer 受注ID
     */
    public function registerOrder($order_id, $arrParams, $cartKey = null)
    {
        if (!is_null($cartKey)) {
            $arrParams['product_type'] = $cartKey;
        }

        $table = 'dtb_order';
        $where = 'order_id = ?';
        $objQuery = SC_Query_Ex::getSingletonInstance();
        $arrValues = $objQuery->extractOnlyColsOf($table, $arrParams);

        $exists = $objQuery->exists($table, $where, array($order_id));
        if ($exists) {
            $this->sfUpdateOrderStatus($order_id, $arrValues['status'],
                                       $arrValues['add_point'],
                                       $arrValues['use_point'],
                                       $arrValues);
            $this->sfUpdateOrderNameCol($order_id);

            $arrValues['update_date'] = 'CURRENT_TIMESTAMP';
            $objQuery->update($table, $arrValues, $where, array($order_id));
        }
        else {
            if (SC_Utils_Ex::isBlank($order_id)) {
                $order_id = $this->getNextOrderID();
            }
            /*
             * 新規受付の場合は対応状況 null で insert し,
             * sfUpdateOrderStatus で引数で受け取った値に変更する.
             */
            $status = $arrValues['status'];
            $arrValues['status'] = null;
            $arrValues['order_id'] = $order_id;
            $arrValues['customer_id'] =
                    SC_Utils_Ex::isBlank($arrValues['customer_id'])
                    ? 0 : $arrValues['customer_id'];
            $arrValues['create_date'] = 'CURRENT_TIMESTAMP';
            $arrValues['update_date'] = 'CURRENT_TIMESTAMP';
            $objQuery->insert($table, $arrValues);

            $this->sfUpdateOrderStatus($order_id, $status,
                                       $arrValues['add_point'],
                                       $arrValues['use_point'],
                                       $arrValues);
            $this->sfUpdateOrderNameCol($order_id);
        }

        return $order_id;
    }


    /**
     * 受注.対応状況の更新
     * 必ず呼び出し元でトランザクションブロックを開いておくこと。
     *
     * ポイント加算は
     *
     * @param  integer      $orderId     注文番号
     * @param  integer|null $newStatus   対応状況 (null=変更無し)
     * @param  integer|null $newAddPoint 加算ポイント (null=変更無し)
     * @param  integer|null $newUsePoint 使用ポイント (null=変更無し)
     * @param  array        $sqlval      更新後の値をリファレンスさせるためのパラメーター
     * @return void
     */
    public function sfUpdateOrderStatus($orderId, $newStatus = null, $newAddPoint = null, $newUsePoint = null, &$sqlval = array())
    {
        $objQuery =& SC_Query_Ex::getSingletonInstance();
        $arrOrderOld = $objQuery->getRow('status, add_point, use_point, customer_id', 'dtb_order', 'order_id = ?', array($orderId));

        // 対応状況が変更無しの場合、DB値を引き継ぐ
        if (is_null($newStatus)) {
            $newStatus = $arrOrderOld['status'];
        }

        // 使用ポイント、DB値を引き継ぐ
        if (is_null($newUsePoint)) {
            $newUsePoint = $arrOrderOld['use_point'];
        }

        // 加算ポイント、DB値を引き継ぐ
        if (is_null($newAddPoint)) {
            $newAddPoint = $arrOrderOld['add_point'];
        }

        // 会員.ポイントの加減値
        $addCustomerPoint = 0;
        $point = null;

        // 変更前ポイント
        $beforePoint = null;
        $getPointSql = 'SELECT point FROM dtb_customer WHERE customer_id = ?';
        if ($arrOrderOld['customer_id'] && ORDER_PENDING != $newStatus && ORDER_PENDING != $arrOrderOld['status']) {
            $beforePoint = $objQuery->getOne($getPointSql, array($arrOrderOld['customer_id']));
        }
        else if (ORDER_NEW == $newStatus && ORDER_PENDING == $arrOrderOld['status']) {
            $point = $objQuery->getOne($getPointSql, array($arrOrderOld['customer_id']));
            $beforePoint = $point + $arrOrderOld['use_point'];
        }

        if (USE_POINT !== false) {
            // ▼使用ポイント
            // 変更前の対応状況が利用対象の場合、変更前の使用ポイント分を戻す
            if ($this->isUsePoint($arrOrderOld['status'])) {
                $addCustomerPoint += $arrOrderOld['use_point'];
            }

            // 変更後の対応状況が利用対象の場合、変更後の使用ポイント分を引く
            if ($this->isUsePoint($newStatus)) {
                $addCustomerPoint -= $newUsePoint;
            }
            // ▲使用ポイント

            if ($addCustomerPoint != 0 && $newUsePoint) {
                // ▼会員テーブルの更新
                $objQuery->update('dtb_customer', array('update_date' => 'CURRENT_TIMESTAMP'),
                                  'customer_id = ?', array($arrOrderOld['customer_id']),
                                  array('point' => 'point + ?'), array($addCustomerPoint));
                // ▲会員テーブルの更新

                // 会員.ポイントをマイナスした場合、
                if ($addCustomerPoint < 0) {
                    $point = $objQuery->getOne($getPointSql, array($arrOrderOld['customer_id']));
                    // 変更後の会員.ポイントがマイナスの場合、
                    if ($point < 0) {
                        // ロールバック
                        $objQuery->rollback();
                        // エラー
                        SC_Utils_Ex::sfDispSiteError(LACK_POINT);
                    }
                }
            }
        }

        // 受注キャンセル時
        if (ORDER_CANCEL == $newStatus) {
            $point = $beforePoint + $addCustomerPoint;
        }

        // ▼受注テーブルの更新
        if (empty($sqlval)) {
            $sqlval = array();
        }

        if (USE_POINT !== false) {
            $sqlval['add_point'] = $newAddPoint;
            $sqlval['use_point'] = $newUsePoint;
        }
        // 対応状況が発送済みに変更の場合、発送日を更新
        if ($arrOrderOld['status'] != ORDER_DELIV && $newStatus == ORDER_DELIV) {
            $sqlval['commit_date'] = 'CURRENT_TIMESTAMP';
        }
        // 対応状況が入金済みに変更の場合、入金日を更新
        else if ($arrOrderOld['status'] != ORDER_PRE_END && $newStatus == ORDER_PRE_END) {
            $sqlval['payment_date'] = 'CURRENT_TIMESTAMP';
        }

        $sqlval['status'] = $newStatus;
        $sqlval['update_date'] = 'CURRENT_TIMESTAMP';

        $dest = $objQuery->extractOnlyColsOf('dtb_order', $sqlval);
        $objQuery->update('dtb_order', $dest, 'order_id = ?', array($orderId));
        // ▲受注テーブルの更新

        //会員情報の最終購入日、購入合計を更新
        if ($arrOrderOld['customer_id'] > 0 and $arrOrderOld['status'] != $newStatus) {
            SC_Customer_Ex::updateOrderSummary($arrOrderOld['customer_id']);
        }

        // ポイント変更履歴の記録
        if (USE_POINT !== false) {
            $this->writePointLog($newStatus, $orderId, $arrOrderOld['customer_id'], $beforePoint, $point);
        }
    }

    /**
     * ポイント履歴の記録
     * @param  int $orderId
     * @param  int $customerId
     * @param  int $beforePoint
     * @param  int $afterPoint
     * @return void
     */
    public function writePointLog($newStatus, $orderId, $customerId, $beforePoint = null, $afterPoint = null)
    {
        if ((ORDER_PENDING == $newStatus) || !$orderId || !$customerId || $beforePoint == $afterPoint
                || is_null($beforePoint) || is_null($afterPoint)) {
            return;
        }

        $isAdmin = GC_Utils::isAdminFunction();
        // ポイント履歴
        $param = array(
            'customer_id' => $customerId,
            'member_id' => ($isAdmin ? $_SESSION['member_id'] : null),
            'order_id' => $orderId,
            'add_point' => ($afterPoint - $beforePoint),
            'point' => $afterPoint,
            'note' => ($isAdmin ? 'admin_order' : 'front_order'),
        );
        SC_Helper_PointLog_Ex::insertRecord($param);
    }

    /**
     * 受注情報を取得する.
     *
     * @param  integer $order_id    受注ID
     * @param  integer $customer_id 会員ID
     * @return array   受注情報の配列
     */
    public function getOrder($order_id, $customer_id = null)
    {
        $objQuery =& SC_Query_Ex::getSingletonInstance();
        $where = 'order_id = ?';
        $arrValues = array($order_id);
        if (!SC_Utils_Ex::isBlank($customer_id)) {
            $where .= ' AND customer_id = ?';
            $arrValues[] = $customer_id;
        }

        $arrOrder = $objQuery->getRow('*', 'dtb_order', $where, $arrValues);

        $arrInfo = SC_Helper_DB_Ex::sfGetBasisDataCache();
        $shopPrefix = $arrInfo['prefix'];
        if (isset($arrOrder['customer_id']) && $arrOrder['customer_id']) {
            $arrOrder['gm_customer_id'] = $shopPrefix . '-' . $arrOrder['customer_id'];
        }

        if (isset($arrOrder['order_id']) && $arrOrder['order_id']) {
            $arrOrder['gm_order_id'] = $shopPrefix . '-' . $arrOrder['order_id'];
        }

        return $arrOrder;
    }

    /**
     * 受注詳細を取得する.
     *
     * @param  integer $order_id         受注ID
     * @param  boolean $has_order_status 対応状況, 入金日も含める場合 true
     * @return array   受注詳細の配列
     */
    public function getOrderDetail($order_id, $has_order_status = true)
    {
        $objQuery =& SC_Query_Ex::getSingletonInstance();
        $dbFactory  = SC_DB_DBFactory_Ex::getInstance();
        $col = <<< __EOS__
            T3.product_id,
            T3.product_class_id as product_class_id,
            T3.product_type_id AS product_type_id,
            T2.product_code,
            T2.product_name,
            T2.classcategory_name1 AS classcategory_name1,
            T2.classcategory_name2 AS classcategory_name2,
            T2.price,
            T2.quantity,
            T2.point_rate,
            T2.tax_rate,
            T2.tax_rule,
            T4.event_date,
            T4.event_time,
__EOS__;
        if ($has_order_status) {
            $col .= 'T1.status AS status, T1.payment_date AS payment_date,';
        }
        $col .= <<< __EOS__
            CASE WHEN
                (T4.del_flg = 0 AND T4.status = 1)
                THEN '1'
                ELSE '0'
            END AS enable,
__EOS__;
        $col .= $dbFactory->getDownloadableDaysWhereSql('T1') . ' AS effective';
        $from = <<< __EOS__
            dtb_order T1
            JOIN dtb_order_detail T2
                ON T1.order_id = T2.order_id
            LEFT JOIN dtb_products_class T3
                ON T2.product_class_id = T3.product_class_id
            LEFT JOIN dtb_products T4
                ON T3.product_id = T4.product_id
__EOS__;
        $objQuery->setOrder('T2.order_detail_id');

        return $objQuery->select($col, $from, 'T1.order_id = ?', array($order_id));
    }

    /**
     * 月額会員用の商品IDをカートに投入する
     * @param type $arrCustomer
     */
    public function inCartMonthlyProduct($arrCustomer)
    {
        // セッションを作成
        $_SESSION['pre_customer'] = $arrCustomer;

        // 受注情報をカートに投入
        $objProduct = new SC_Product_Ex();
        $objProduct->setProductsClassByProductIds(array(PAID_PRODUCT_ID));
        $productClassId = $objProduct->classCategories[PAID_PRODUCT_ID]['__unselected']['__unselected']['product_class_id'];

        $objSiteSess = new SC_SiteSession_Ex();
        $objCartSess = new SC_CartSession_Ex();
        $objCartSess->addProduct($productClassId, 1);
        $objCartSess->setQuantity(1, 1, PRODUCT_TYPE_MONTHLY, $productClassId);

        $cartList = $objCartSess->getCartList(PRODUCT_TYPE_MONTHLY);
        $cart = current($cartList);
        $total = $cart['price_inctax'];
        $tax = $total - $cart['price'];

        // 正常に登録されたことを記録しておく
        $objSiteSess->setRegistFlag();

        $objSiteSess->setUniqId();
        $uniqId = $objSiteSess->getUniqId();

        // カートを購入モードに設定
        $objCartSess->registerKey(PRODUCT_TYPE_MONTHLY);
        $objCartSess->saveCurrentCart($uniqId, PRODUCT_TYPE_MONTHLY);
        $order_id = $this->getNextOrderID();

        $orderTemp = array(
            'order_temp_id' => $uniqId,
            'customer_id' => $arrCustomer['customer_id'],
            'order_id' => $order_id,
            'subtotal' => $total,
            'deliv_id' => 0,
            'deliv_fee' => 0,
            'charge' => 0,
            'use_point' => null,
            'tax' => $tax,
            'total' => $total,
            'payment_total' => $total,
            'deliv_check' => -1,
            'point_check'=> 0,
            'update_date' => 'CURRENT_TIMESTAMP',
        );
        $sqlVal = array_merge($orderTemp, $this->copyFromMonthlyCustomer($arrCustomer));
        $this->saveOrderTemp($uniqId, $sqlVal);

        return $uniqId;
    }

    /**
     * 会員情報を受注情報用キーとして作成する.
     *
     * @param  array  $arrCustomer
     * @return array
     */
    public function copyFromMonthlyCustomer($arrCustomer) {
        $keys = array('name01', 'name02', 'kana01', 'kana02', 'company_name',
            'sex', 'zip01', 'zip02', 'country_id', 'zipcode', 'pref', 'addr01', 'addr02',
            'tel01', 'tel02', 'tel03', 'fax01', 'fax02', 'fax03',
            'job', 'birth', 'email',
        );

        $ret = array();
        foreach ($keys as $key) {
            if (isset($arrCustomer[$key]) && '' !== $arrCustomer[$key]) {
                $ret['order' . '_' . $key] = $arrCustomer[$key];
            }
        }

        return $ret;
    }
}
