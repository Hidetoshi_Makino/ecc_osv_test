<?php
/*
 * This file is part of EC-CUBE
 *
 * Copyright(c) 2000-2014 LOCKON CO.,LTD. All Rights Reserved.
 *
 * http://www.lockon.co.jp/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

require_once CLASS_REALDIR . 'helper/SC_Helper_Customer.php';

/**
 * CSV関連のヘルパークラス(拡張).
 *
 * LC_Helper_Customer をカスタマイズする場合はこのクラスを編集する.
 *
 * @package Helper
 * @author LOCKON CO.,LTD.
 * @version $Id:SC_Helper_DB_Ex.php 15532 2007-08-31 14:39:46Z nanasess $
 */
class SC_Helper_Customer_Ex extends SC_Helper_Customer
{

    /**
     * 会員検索パラメーター（管理画面用）
     *
     * @param SC_FormParam $objFormParam SC_FormParam インスタンス
     * @access public
     * @return void
     */
    public function sfSetSearchParam(&$objFormParam)
    {
        parent::sfSetSearchParam($objFormParam);

        $objFormParam->addParam('入会日(開始年)', 'search_start_entry_year', 4, 'n', array('NUM_CHECK', 'MAX_LENGTH_CHECK'));
        $objFormParam->addParam('入会日(開始月)', 'search_start_entry_month', 2, 'n', array('NUM_CHECK', 'MAX_LENGTH_CHECK'));
        $objFormParam->addParam('入会日(開始日)', 'search_start_entry_day', 2, 'n', array('NUM_CHECK', 'MAX_LENGTH_CHECK'));
        $objFormParam->addParam('入会日(終了年)', 'search_end_entry_year', 4, 'n', array('NUM_CHECK', 'MAX_LENGTH_CHECK'));
        $objFormParam->addParam('入会日(終了月)', 'search_end_entry_month', 2, 'n', array('NUM_CHECK', 'MAX_LENGTH_CHECK'));
        $objFormParam->addParam('入会日(終了日)', 'search_end_entry_day', 2, 'n', array('NUM_CHECK', 'MAX_LENGTH_CHECK'));

        $objFormParam->addParam('退会日(開始年)', 'search_start_resign_year', 4, 'n', array('NUM_CHECK', 'MAX_LENGTH_CHECK'));
        $objFormParam->addParam('退会日(開始月)', 'search_start_resign_month', 2, 'n', array('NUM_CHECK', 'MAX_LENGTH_CHECK'));
        $objFormParam->addParam('退会日(開始日)', 'search_start_resign_day', 2, 'n', array('NUM_CHECK', 'MAX_LENGTH_CHECK'));
        $objFormParam->addParam('退会日(終了年)', 'search_end_resign_year', 4, 'n', array('NUM_CHECK', 'MAX_LENGTH_CHECK'));
        $objFormParam->addParam('退会日(終了月)', 'search_end_resign_month', 2, 'n', array('NUM_CHECK', 'MAX_LENGTH_CHECK'));
        $objFormParam->addParam('退会日(終了日)', 'search_end_resign_day', 2, 'n', array('NUM_CHECK', 'MAX_LENGTH_CHECK'));

        $objFormParam->addParam('退会者', 'search_del', 1, 'na', array('NUM_CHECK','MAX_LENGTH_CHECK'));
    }

    /**
     * customer_idから会員情報を取得する
     *
     * @param mixed $customer_id
     * @param boolean $mask_flg
     * @access public
     * @return array 会員情報の配列を返す
     */
    public function sfGetCustomerData($customer_id, $mask_flg = true)
    {
        $objQuery =& SC_Query_Ex::getSingletonInstance();

        // 会員情報DB取得
        $ret = $objQuery->select('*', 'dtb_customer', 'customer_id=?', array($customer_id));
        $arrForm = $ret[0];

        // 確認項目に複製
        $arrForm['email02'] = $arrForm['email'];
        $arrForm['email_mobile02'] = $arrForm['email_mobile'];

        // 誕生日を年月日に分ける
        if (isset($arrForm['birth'])) {
            $birth = explode(' ', $arrForm['birth']);
            list($arrForm['year'], $arrForm['month'], $arrForm['day']) = explode('-', $birth[0]);
        }

        if ($mask_flg) {
            $arrForm['password']          = DEFAULT_PASSWORD;
            $arrForm['password02']        = DEFAULT_PASSWORD;
            $arrForm['reminder_answer']   = DEFAULT_PASSWORD;
        }

        if ($arrForm['customer_id']) {
            $arrInfo = SC_Helper_DB_Ex::sfGetBasisDataCache();
            $shopPrefix = $arrInfo['prefix'];
            $arrForm['gm_customer_id'] = $shopPrefix . '-' . $arrForm['customer_id'];
        }

        return $arrForm;
    }

    /**
     * ユニークIDを元にカスタマ情報の有無をチェック
     *
     * @param  string  $secret_key
     * @return boolean|array
     */
    public function getCustomerBySecretKey($secret_key)
    {
        if (!$secret_key) {
            return false;
        }

        $objQuery =& SC_Query_Ex::getSingletonInstance();
        $where = 'status = 1'
                . ' AND del_flg = 0'
                . ' AND secret_key = ?';

        return $objQuery->getRow('*', 'dtb_customer', $where, array($secret_key));
    }

    /**
     * 会員を削除する処理
     *
     * @param  integer $customer_id 会員ID
     * @return boolean true:成功 false:失敗
     */
    public static function delete($customer_id)
    {
        $arrData = SC_Helper_Customer_Ex::sfGetCustomerDataFromId($customer_id, 'del_flg = 0');
        if (SC_Utils_Ex::isBlank($arrData)) {
            //対象となるデータが見つからない。
            return false;
        }
        // XXXX: 仮会員は物理削除となっていたが論理削除に変更。
        $arrVal = array(
            'del_flg' => '1',
            'entry_date_end' => 'CURRENT_TIMESTAMP'
        );
        SC_Helper_Customer_Ex::sfEditCustomerData($arrVal, $customer_id);

        return true;
    }
}
