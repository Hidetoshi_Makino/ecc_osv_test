<?php
/*
 * This file is part of EC-CUBE
 *
 * Copyright(c) 2000-2014 LOCKON CO.,LTD. All Rights Reserved.
 *
 * http://www.lockon.co.jp/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

require_once CLASS_REALDIR . 'helper/SC_Helper_CSV.php';

/**
 * CSV関連のヘルパークラス(拡張).
 *
 * LC_Helper_CSV をカスタマイズする場合はこのクラスを編集する.
 *
 * @package Helper
 * @author LOCKON CO.,LTD.
 * @version $Id:SC_Helper_DB_Ex.php 15532 2007-08-31 14:39:46Z nanasess $
 */
class SC_Helper_CSV_Ex extends SC_Helper_CSV
{

    /**
     * 項目情報を初期化する.
     *
     * @access private
     * @return void
     */
    public function init()
    {
        parent::init();
        $this->arrSubnavi[6] = 'pointLog';
        $this->arrSubnaviName[6] = 'ポイントログ';
    }

    /**
     * CSVファイルを送信する
     *
     * @param  integer $csv_id      CSVフォーマットID
     * @param  string  $where       WHERE条件文
     * @param  array   $arrVal      プリペアドステートメントの実行時に使用される配列。配列の要素数は、クエリ内のプレースホルダの数と同じでなければなりません。
     * @param  string  $order       ORDER文
     * @param  boolean $is_download true:ダウンロード用出力までさせる false:CSVの内容を返す(旧方式、メモリを食います。）
     * @return boolean|string   $is_download = true時 成功失敗フラグ(boolean) 、$is_downalod = false時 string
     */
    public function sfDownloadCsv($csv_id, $where = '', $arrVal = array(), $order = '', $is_download = false)
    {
        if (6 > $csv_id) {
            return parent::sfDownloadCsv($csv_id, $where, $arrVal, $order, $is_download);
        }

        switch ($csv_id) {
            case 6:
                $from = 'dtb_point_log'
                    . ' LEFT JOIN dtb_customer ON dtb_customer.customer_id = dtb_point_log.customer_id'
                    . ' LEFT JOIN dtb_member ON dtb_member.member_id = dtb_point_log.member_id';

                $order = 'dtb_customer.customer_id, dtb_point_log.log_id';
                break;
        }

        $objQuery =& SC_Query_Ex::getSingletonInstance();
        // CSV出力タイトル行の作成
        $arrOutput = SC_Utils_Ex::sfSwapArray($this->sfGetCsvOutput($csv_id, 'status = ' . CSV_COLUMN_STATUS_FLG_ENABLE));
        // 失敗終了
        if (count($arrOutput) <= 0) {
            return false;
        }
        $arrOutputCols = $arrOutput['col'];

        $cols = SC_Utils_Ex::sfGetCommaList($arrOutputCols, true);
        $objQuery->setOrder($order);
        $sql = $objQuery->getSql($cols, $from, $where);

        return $this->sfDownloadCsvFromSql($sql, $arrVal, $this->arrSubnavi[$csv_id], $arrOutput['disp_name'], $is_download);
    }
}
