<?php
/*
 * This file is part of EC-CUBE
 *
 * Copyright(c) 2000-2014 LOCKON CO.,LTD. All Rights Reserved.
 *
 * http://www.lockon.co.jp/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

require_once CLASS_REALDIR . 'helper/SC_Helper_Mail.php';

/**
 * メール関連のヘルパークラス(拡張).
 *
 * LC_Helper_Mail をカスタマイズする場合はこのクラスを編集する.
 *
 * @package Helper
 * @author LOCKON CO.,LTD.
 * @version $Id:SC_Helper_DB_Ex.php 15532 2007-08-31 14:39:46Z nanasess $
 */
class SC_Helper_Mail_Ex extends SC_Helper_Mail
{

    /* 注文受付メール送信 */
    public function sfSendOrderMail($order_id, $template_id, $subject = '', $header = '', $footer = '', $send = true)
    {
        $arrTplVar = new stdClass();
        $arrInfo = SC_Helper_DB_Ex::sfGetBasisData();
        $arrTplVar->arrInfo = $arrInfo;

        $objQuery =& SC_Query_Ex::getSingletonInstance();

        if ($subject == '' && $header == '' && $footer == '') {
            // メールテンプレート情報の取得
            $objMailtemplate = new SC_Helper_Mailtemplate_Ex();
            $mailtemplate = $objMailtemplate->get($template_id);
            $arrTplVar->tpl_header = $mailtemplate['header'];
            $arrTplVar->tpl_footer = $mailtemplate['footer'];
            $tmp_subject = $mailtemplate['subject'];
        }
        else {
            $arrTplVar->tpl_header = $header;
            $arrTplVar->tpl_footer = $footer;
            $tmp_subject = $subject;
        }

        // 受注情報の取得
        $where = 'order_id = ? AND del_flg = 0';
        $arrOrder = $objQuery->getRow('*', 'dtb_order', $where, array($order_id));
        if (empty($arrOrder)) {
            trigger_error("該当する受注が存在しない。(注文番号: $order_id)", E_USER_ERROR);
        }

        $objPurchase = new SC_Helper_Purchase_Ex();
        $arrTplVar->arrOrderDetail = $objPurchase->getOrderDetail($order_id);

        // E-Ticket
        $isEticket = false;
        $eTicketData = array();
        $eticketStatus = (ORDER_PRE_END == $arrOrder['status'] && $arrOrder['payment_date'] != '');
        $eTicketParam = array();
        if ($send && $eticketStatus) {
            foreach ($arrTplVar->arrOrderDetail as $ix => $arrDetail) {
                if (PRODUCT_TYPE_ETICKET == $arrDetail['product_type_id']) {
                    $isEticket = true;
                    $dateTime = new DateTime($arrDetail['event_date'] . $arrDetail['event_time']);

                    $eTicketParam[$ix] = "会員ID: " . $arrOrder['customer_id'] . "\r\n"
                            . "会員名: " . $arrOrder['order_name01'] . ' ' . $arrOrder['order_name02'] . "\r\n"
                            . "受注番号: " . $order_id . "\r\n"
                            . "数量: " . (int) $arrDetail['quantity'] . "\r\n"
                            . "開催日: " . $dateTime->format('Y/m/d') . "\r\n"
                            . "開催時刻: " . $dateTime->format('H:i') . "\r\n"
                    ;
                }
            }
        }

        if ($send && $isEticket) {
            $objEticket = new SC_Helper_Eticket();
            foreach ($eTicketParam as $val) {
                $eTicketData[] = $objEticket->getQRPath($val, SC_Helper_Eticket::LEVEL_L, 6);
            }
        }

        // 配送情報の取得
        $arrTplVar->arrShipping = $this->sfGetShippingData($order_id);

        $arrTplVar->Message_tmp = $arrOrder['message'];

        // 会員情報の取得
        $customer_id = $arrOrder['customer_id'];
        $objQuery->setOrder('customer_id');
        $arrRet = $objQuery->select('point', 'dtb_customer', 'customer_id = ?', array($customer_id));
        $arrCustomer = isset($arrRet[0]) ? $arrRet[0] : '';

        $arrTplVar->arrCustomer = $arrCustomer;
        $arrTplVar->arrOrder = $arrOrder;

        //その他決済情報
        if ($arrOrder['memo02'] != '') {
            $arrOther = unserialize($arrOrder['memo02']);

            foreach ($arrOther as $other_key => $other_val) {
                if (SC_Utils_Ex::sfTrim($other_val['value']) == '') {
                    $arrOther[$other_key]['value'] = '';
                }
            }

            $arrTplVar->arrOther = $arrOther;
        }

        // 都道府県変換
        $arrTplVar->arrPref = $this->arrPref;
        // 国変換
        $arrTplVar->arrCountry = $this->arrCountry;

        $objCustomer = new SC_Customer_Ex();
        $arrTplVar->tpl_user_point = $objCustomer->getValue('point');

        $objMailView = null;
        // 注文受付メール(携帯)
        if ($template_id == 2) {
            $objMailView = new SC_MobileView_Ex();
        } else {
            $objMailView = new SC_SiteView_Ex();
        }
        // メール本文の取得
        $objMailView->setPage($this->getPage());
        $objMailView->assignobj($arrTplVar);
        $body = $objMailView->fetch($this->arrMAILTPLPATH[$template_id]);

        // メール送信処理
        $objSendMail = new SC_SendMail_Ex();
        $bcc = $arrInfo['email01'];
        $from = $arrInfo['email03'];
        $error = $arrInfo['email04'];
        $tosubject = $this->sfMakeSubject($tmp_subject, $objMailView);

        $objSendMail->setItem('', $tosubject, $body, $from, $arrInfo['shop_name'], $from, $error, $error, $bcc);
        $objSendMail->setTo($arrOrder['order_email'], $arrOrder['order_name01'] . ' '. $arrOrder['order_name02'] .' 様');

        // 送信フラグ:trueの場合は、送信する。
        $success = false;
        if ($send && $isEticket) {
            $success = $objSendMail->sendMimeMail($eTicketData);
        }
        else if ($send) {
            $success = $objSendMail->sendMail();
        }

        if ($success) {
            $this->sfSaveMailHistory($order_id, $template_id, $tosubject, $body);
        }

        return $objSendMail;
    }

    /**
     * 配送情報の取得
     *
     * @param integer $order_id 受注ID
     * @return array 配送情報を格納した配列
     */
    function sfGetShippingData($order_id)
    {
        $objQuery =& SC_Query_Ex::getSingletonInstance();

        $objQuery->setOrder('shipping_id');
        $whr = 'order_id = ? AND shipping_name01 IS NOT NULL';
        $whrVal = array($order_id);
        $arrRet = $objQuery->select('*', 'dtb_shipping', $whr, $whrVal);
        if (!$arrRet) {
            return array();
        }

        foreach ($arrRet as $key => $value) {
            if (!$value) {
                continue;
            }
            $col = 's_i.*, tax_rate, tax_rule';
            $from = 'dtb_shipment_item AS s_i JOIN dtb_order_detail AS o_d
                ON s_i.order_id = o_d.order_id AND s_i.product_class_id = o_d.product_class_id';
            $where = 'o_d.order_id = ? AND shipping_id = ?';
            $arrWhereVal = array($order_id, $arrRet[$key]['shipping_id']);
            $objQuery->setOrder('order_detail_id');
            $arrItems = $objQuery->select($col, $from, $where, $arrWhereVal);
            $arrRet[$key]['shipment_item'] = $arrItems;
        }

        return $arrRet;
    }
}
