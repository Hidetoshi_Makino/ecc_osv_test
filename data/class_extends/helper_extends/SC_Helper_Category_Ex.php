<?php
/*
 * This file is part of EC-CUBE
 *
 * Copyright(c) 2000-2014 LOCKON CO.,LTD. All Rights Reserved.
 *
 * http://www.lockon.co.jp/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

require_once CLASS_REALDIR . 'helper/SC_Helper_Category.php';

/**
 * カテゴリーを管理するヘルパークラス(拡張).
 *
 * LC_Helper_Category をカスタマイズする場合はこのクラスを編集する.
 *
 * @package Helper
 * @author pineray
 * @version $Id:$
 */
class SC_Helper_Category_Ex extends SC_Helper_Category
{

    /**
     * カテゴリー一覧の取得.
     *
     * @param  boolean $cid_to_key 配列のキーをカテゴリーIDにする場合はtrue
     * @return array   カテゴリー一覧の配列
     */
    public function getList($cid_to_key = false)
    {
        static $arrCategory = array(), $cidIsKey = array();

        if (!isset($arrCategory[$this->count_check])) {
            $objQuery =& SC_Query_Ex::getSingletonInstance();
            $col = 'dtb_category.*, dtb_category_total_count.product_count';
            $from = 'dtb_category left join dtb_category_total_count ON dtb_category.category_id = dtb_category_total_count.category_id';
            // 登録商品数のチェック
            if ($this->count_check) {
                $where = 'del_flg = 0 AND product_count > 0';
            } else {
                $where = 'del_flg = 0';
            }

            $isAdmin = GC_Utils::isAdminFunction();
            if (!$isAdmin) {
                $where .= ' AND dtb_category.category_id <> ' . (int) PAID_CATEGORY;
            }

            $objQuery->setOption('ORDER BY rank DESC');
            $arrTmp = $objQuery->select($col, $from, $where);

            $arrCategory[$this->count_check] = $arrTmp;
        }

        if ($cid_to_key) {
            if (!isset($cidIsKey[$this->count_check])) {
                // 配列のキーをカテゴリーIDに
                $cidIsKey[$this->count_check] = SC_Utils_Ex::makeArrayIDToKey('category_id', $arrCategory[$this->count_check]);
            }

            return $cidIsKey[$this->count_check];
        }

        return $arrCategory[$this->count_check];
    }

}
