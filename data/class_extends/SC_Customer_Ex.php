<?php
/*
 * This file is part of EC-CUBE
 *
 * Copyright(c) 2000-2014 LOCKON CO.,LTD. All Rights Reserved.
 *
 * http://www.lockon.co.jp/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

require_once CLASS_REALDIR . 'SC_Customer.php';

class SC_Customer_Ex extends SC_Customer
{

    // パラメーターの取得
    public function getValue($keyname)
    {
        // ポイントはリアルタイム表示
        if ($keyname == 'point') {
            $customer_id = isset($_SESSION['customer']['customer_id']) ? $_SESSION['customer']['customer_id'] : null;

            if (is_null($customer_id)) {
                return 0;
            }

            $objQuery =& SC_Query_Ex::getSingletonInstance();
            $point = $objQuery->get('point', 'dtb_customer', 'customer_id = ?', array($customer_id));
            $_SESSION['customer']['point'] = $point;

            return $point;
        }
        else {
            return isset($_SESSION['customer'][$keyname]) ? $_SESSION['customer'][$keyname] : '';
        }
    }
}
