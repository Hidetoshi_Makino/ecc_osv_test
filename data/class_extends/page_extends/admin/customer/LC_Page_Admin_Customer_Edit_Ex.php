<?php
/*
 * This file is part of EC-CUBE
 *
 * Copyright(c) 2000-2014 LOCKON CO.,LTD. All Rights Reserved.
 *
 * http://www.lockon.co.jp/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

require_once CLASS_REALDIR . 'pages/admin/customer/LC_Page_Admin_Customer_Edit.php';

/**
 * 会員情報修正 のページクラス(拡張).
 *
 * LC_Page_Admin_Customer_Edit をカスタマイズする場合はこのクラスを編集する.
 *
 * @package Page
 * @author LOCKON CO.,LTD.
 * @version $Id$
 */
class LC_Page_Admin_Customer_Edit_Ex extends LC_Page_Admin_Customer_Edit
{
    /**
     * Page を初期化する.
     *
     * @return void
     */
    public function init()
    {
        parent::init();
    }

    /**
     * Page のプロセス.
     *
     * @return void
     */
    public function process()
    {
        parent::process();
    }

    /**
     * 登録処理
     *
     * @param  array $objFormParam フォームパラメータークラス
     * @return integer エラー配列
     */
    public function lfRegistData(&$objFormParam)
    {
        // 登録用データ取得
        $arrData = $objFormParam->getDbArray();
        // 足りないものを作る
        if (!SC_Utils_Ex::isBlank($objFormParam->getValue('year'))) {
            $arrData['birth'] = $objFormParam->getValue('year') . '/'
                            . $objFormParam->getValue('month') . '/'
                            . $objFormParam->getValue('day')
                            . ' 00:00:00';
        }

        $isNew = false;
        if (!is_numeric($arrData['customer_id'])) {
            $arrData['secret_key'] = SC_Utils_Ex::sfGetUniqRandomId('r');
            $isNew = true;
        }
        else {
            $arrOldCustomerData = SC_Helper_Customer_Ex::sfGetCustomerData($arrData['customer_id']);
            if ($arrOldCustomerData['status'] != $arrData['status']) {
                $arrData['secret_key'] = SC_Utils_Ex::sfGetUniqRandomId('r');
            }
        }

        $success = SC_Helper_Customer_Ex::sfEditCustomerData($arrData, $arrData['customer_id']);

        if (!$success) {
            return $success;
        }

        // ポイント履歴
        if ($isNew && $arrData['point']) {
            $customerId = $success;
            $beforePoint = 0;
        }
        else {
            $customerId = $arrData['customer_id'];
            $beforePoint = $arrOldCustomerData['point'];
        }
        $afterPoint = $arrData['point'];
        $this->writePointLog($customerId, $beforePoint, $afterPoint);
    }

    /**
     * ポイント履歴の記録
     * @param  int $orderId
     * @param  int $customerId
     * @param  int $beforePoint
     * @param  int $afterPoint
     * @return void
     */
    public function writePointLog($customerId, $beforePoint = null, $afterPoint = null)
    {
        if (!$customerId || $beforePoint == $afterPoint || is_null($beforePoint) || is_null($afterPoint)) {
            return;
        }

        // ポイント履歴
        $param = array(
            'customer_id' => $customerId,
            'member_id' => $_SESSION['member_id'],
            'add_point' => ($afterPoint - $beforePoint),
            'point' => $afterPoint,
            'note' => 'admin_customer',
        );
        SC_Helper_PointLog_Ex::insertRecord($param);
    }
}
