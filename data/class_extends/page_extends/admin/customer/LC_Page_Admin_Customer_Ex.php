<?php
/*
 * This file is part of EC-CUBE
 *
 * Copyright(c) 2000-2014 LOCKON CO.,LTD. All Rights Reserved.
 *
 * http://www.lockon.co.jp/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

require_once CLASS_REALDIR . 'pages/admin/customer/LC_Page_Admin_Customer.php';

/**
 * 会員管理 のページクラス(拡張).
 *
 * LC_Page_Admin_Customer をカスタマイズする場合はこのクラスを編集する.
 *
 * @package Page
 * @author LOCKON CO.,LTD.
 * @version $Id$
 */
class LC_Page_Admin_Customer_Ex extends LC_Page_Admin_Customer
{
    /**
     * Page を初期化する.
     *
     * @return void
     */
    public function init()
    {
        parent::init();
    }

    /**
     * Page のプロセス.
     *
     * @return void
     */
    public function process()
    {
        parent::process();
    }

    /**
     * Page のアクション.
     *
     * @return void
     */
    public function action()
    {
        $mode = $this->getMode();
        if ('point_log_csv' !== $mode) {
            parent::action();
            return;
        }

        $objFormParam = new SC_FormParam_Ex();
        $this->lfInitParam($objFormParam);
        $post = filter_input_array(INPUT_POST);
        $objFormParam->setParam($post);
        $objFormParam->convParam();

        // パラメーター読み込み
        $this->arrForm = $objFormParam->getFormParamList();
        // 検索ワードの引き継ぎ
        $this->arrHidden = $objFormParam->getSearchArray();

        // 入力パラメーターチェック
        $this->arrErr = $this->lfCheckError($objFormParam);
        if (!SC_Utils_Ex::isBlank($this->arrErr)) {
            return;
        }

        $this->lfDoPointLogCSV($objFormParam->getHashArray());
        SC_Response_Ex::actionExit();
    }

    /**
     * 会員検索条件を元にポイントログのCSVをダウンロードする
     *
     * @param  array   $arrParam 検索パラメーター連想配列
     * @return boolean|string true:成功 false:失敗
     */
    public function lfDoPointLogCSV($arrParam)
    {
        $objSelect = new SC_CustomerList_Ex($arrParam, 'customer');
        $objCSV = new SC_Helper_CSV_Ex();

        $order = 'update_date DESC, customer_id DESC';

        list($where, $arrVal) = $objSelect->getWhere();

        return $objCSV->sfDownloadCsv('6', $where, $arrVal, $order, true);
    }
}
