<?php
/*
 * This file is part of EC-CUBE
 *
 * Copyright(c) 2000-2014 LOCKON CO.,LTD. All Rights Reserved.
 *
 * http://www.lockon.co.jp/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

require_once CLASS_REALDIR . 'pages/admin/products/LC_Page_Admin_Products_Product.php';

/**
 * 商品登録 のページクラス(拡張).
 *
 * LC_Page_Admin_Products_Product をカスタマイズする場合はこのクラスを編集する.
 *
 * @package Page
 * @author LOCKON CO.,LTD.
 * @version $Id$
 */
class LC_Page_Admin_Products_Product_Ex extends LC_Page_Admin_Products_Product
{
    /**
     * Page を初期化する.
     *
     * @return void
     */
    public function init()
    {
        parent::init();
    }

    /**
     * Page のプロセス.
     *
     * @return void
     */
    public function process()
    {
        parent::process();
    }


    /**
     * DBに商品データを登録する
     *
     * @param  SC_UploadFile_Ex  $objUpFile   SC_UploadFileインスタンス
     * @param  SC_UploadFile_Ex  $objDownFile SC_UploadFileインスタンス
     * @param  array   $arrList     フォーム入力パラメーター配列
     * @return integer 登録商品ID
     */
    public function lfRegistProduct(&$objUpFile, &$objDownFile, $arrList)
    {
        $objQuery =& SC_Query_Ex::getSingletonInstance();
        $objDb = new SC_Helper_DB_Ex();

        // 配列の添字を定義
        $checkArray = array(
            'name', 'status', 'main_list_comment', 'main_comment', 'deliv_fee',
            'comment1', 'comment2', 'comment3', 'comment4', 'comment5', 'comment6',
            'sale_limit', 'deliv_date_id', 'maker_id', 'note',
            'is_paid_only', 'event_date', 'event_time'
        );
        if ($arrList) {
            $arrList = SC_Utils_Ex::arrayDefineIndexes($arrList, $checkArray);
        }
        else {
            return false;
        }

        // INSERTする値を作成する。
        $sqlval['name'] = $arrList['name'];
        $sqlval['status'] = $arrList['status'];
        $sqlval['main_list_comment'] = $arrList['main_list_comment'];
        $sqlval['main_comment'] = $arrList['main_comment'];
        $sqlval['comment1'] = $arrList['comment1'];
        $sqlval['comment2'] = $arrList['comment2'];
        $sqlval['comment3'] = $arrList['comment3'];
        $sqlval['comment4'] = $arrList['comment4'];
        $sqlval['comment5'] = $arrList['comment5'];
        $sqlval['comment6'] = $arrList['comment6'];
        $sqlval['deliv_date_id'] = $arrList['deliv_date_id'];
        $sqlval['maker_id'] = $arrList['maker_id'];
        $sqlval['note'] = $arrList['note'];
        $sqlval['update_date'] = 'CURRENT_TIMESTAMP';
        $sqlval['creator_id'] = $_SESSION['member_id'];

        // 追加カラム
        $sqlval['is_paid_only'] = $arrList['is_paid_only'] ? 1 : null;
        $sqlval['event_date'] = $arrList['event_date'] ? $arrList['event_date'] : null;
        $sqlval['event_time'] = $arrList['event_time'] ? $arrList['event_time'] : null;

        $arrRet = $objUpFile->getDBFileList();
        $sqlval = array_merge($sqlval, $arrRet);

        for ($cnt = 1; $cnt <= PRODUCTSUB_MAX; $cnt++) {
            $sqlval['sub_title'.$cnt] = $arrList['sub_title'.$cnt];
            $sqlval['sub_comment'.$cnt] = $arrList['sub_comment'.$cnt];
        }

        $objQuery->begin();

        // 新規登録(複製時を含む)
        if ($arrList['product_id'] == '') {
            $product_id = $objQuery->nextVal('dtb_products_product_id');
            $sqlval['product_id'] = $product_id;

            // INSERTの実行
            $sqlval['create_date'] = 'CURRENT_TIMESTAMP';
            $objQuery->insert('dtb_products', $sqlval);

            $arrList['product_id'] = $product_id;

            // カテゴリを更新
            $objDb->updateProductCategories($arrList['category_id'], $product_id);

            // 複製商品の場合には規格も複製する
            if ($arrList['copy_product_id'] != '' && SC_Utils_Ex::sfIsInt($arrList['copy_product_id'])) {
                if (!$arrList['has_product_class']) {
                    //規格なしの場合、複製は価格等の入力が発生しているため、その内容で追加登録を行う
                    $this->lfCopyProductClass($arrList, $objQuery);
                } else {
                    //規格がある場合の複製は複製元の内容で追加登録を行う
                    // dtb_products_class のカラムを取得
                    $dbFactory = SC_DB_DBFactory_Ex::getInstance();
                    $arrColList = $objQuery->listTableFields('dtb_products_class');
                    $arrColList_tmp = array_flip($arrColList);

                    // 複製しない列
                    unset($arrColList[$arrColList_tmp['product_class_id']]);     //規格ID
                    unset($arrColList[$arrColList_tmp['product_id']]);           //商品ID
                    unset($arrColList[$arrColList_tmp['create_date']]);

                    // 複製元商品の規格データ取得
                    $col = SC_Utils_Ex::sfGetCommaList($arrColList);
                    $table = 'dtb_products_class';
                    $where = 'product_id = ?';
                    $objQuery->setOrder('product_class_id');
                    $arrProductsClass = $objQuery->select($col, $table, $where, array($arrList['copy_product_id']));

                    // 規格データ登録
                    $objQuery =& SC_Query_Ex::getSingletonInstance();
                    foreach ($arrProductsClass as $arrData) {
                        $sqlval = $arrData;
                        $sqlval['product_class_id'] = $objQuery->nextVal('dtb_products_class_product_class_id');
                        $sqlval['deliv_fee'] = $arrList['deliv_fee'];
                        $sqlval['point_rate'] = $arrList['point_rate'];
                        $sqlval['sale_limit'] = $arrList['sale_limit'];
                        $sqlval['product_id'] = $product_id;
                        $sqlval['create_date'] = 'CURRENT_TIMESTAMP';
                        $sqlval['update_date'] = 'CURRENT_TIMESTAMP';
                        $objQuery->insert($table, $sqlval);
                    }
                }
            }
        }
        // 更新
        else {
            $product_id = $arrList['product_id'];
            // 削除要求のあった既存ファイルの削除
            $arrRet = $this->lfGetProductData_FromDB($arrList['product_id']);
            // TODO: SC_UploadFile::deleteDBFileの画像削除条件見直し要
            $objImage = new SC_Image_Ex($objUpFile->temp_dir);
            $arrKeyName = $objUpFile->keyname;
            $arrSaveFile = $objUpFile->save_file;
            $arrImageKey = array();
            foreach ($arrKeyName as $key => $keyname) {
                if ($arrRet[$keyname] && !$arrSaveFile[$key]) {
                    $arrImageKey[] = $keyname;
                    $has_same_image = $this->lfHasSameProductImage($arrList['product_id'], $arrImageKey, $arrRet[$keyname]);
                    if (!$has_same_image) {
                        $objImage->deleteImage($arrRet[$keyname], $objUpFile->save_dir);
                    }
                }
            }
            $objDownFile->deleteDBDownFile($arrRet);
            // UPDATEの実行
            $where = 'product_id = ?';
            $objQuery->update('dtb_products', $sqlval, $where, array($product_id));

            // カテゴリを更新
            $objDb->updateProductCategories($arrList['category_id'], $product_id);
        }

        // 商品登録の時は規格を生成する。複製の場合は規格も複製されるのでこの処理は不要。
        if ($arrList['copy_product_id'] == '') {
            // 規格登録
            if ($objDb->sfHasProductClass($product_id)) {
                // 規格あり商品（商品規格テーブルのうち、商品登録フォームで設定するパラメーターのみ更新）
                $this->lfUpdateProductClass($arrList);
            }
            else {
                // 規格なし商品（商品規格テーブルの更新）
                $arrList['product_class_id'] = $this->lfInsertDummyProductClass($arrList);
            }
        }

        // 商品ステータス設定
        $objProduct = new SC_Product_Ex();
        $objProduct->setProductStatus($product_id, $arrList['product_status']);

        // 税情報設定
        if (OPTION_PRODUCT_TAX_RULE && !$objDb->sfHasProductClass($product_id)) {
            SC_Helper_TaxRule_Ex::setTaxRuleForProduct($arrList['tax_rate'], $arrList['product_id'], $arrList['product_class_id']);
        }

        // 関連商品登録
        $this->lfInsertRecommendProducts($objQuery, $arrList, $product_id);

        $objQuery->commit();

        return $product_id;
    }

    /**
     * フォーム入力パラメーターのエラーチェック
     *
     * @param  object $objFormParam SC_FormParamインスタンス
     * @param  SC_UploadFile_Ex $objUpFile    SC_UploadFileインスタンス
     * @param  SC_UploadFile_Ex $objDownFile  SC_UploadFileインスタンス
     * @param  array  $arrForm      フォーム入力パラメーター配列
     * @return array  エラー情報を格納した連想配列
     */
    public function lfCheckError_Edit(&$objFormParam, &$objUpFile, &$objDownFile, $arrForm)
    {
        $arrErr = parent::lfCheckError_Edit($objFormParam, $objUpFile, $objDownFile, $arrForm);

        // 追加チェック
        $event_date = isset($arrForm['event_date']) ? $arrForm['event_date'] : null;
        $event_time = isset($arrForm['event_time']) ? $arrForm['event_time'] : null;

        if (!$event_date && !$event_time) {
            return $arrErr;
        }
        else if (!$event_date || !$event_time) {
            $arrErr['event_date'] = 'イベント開催日時は年月日と時間の両方の指定が必要です。<br />';
        }
        else {
            $chkDate = $this->DATE_CHECK($event_date);
            if ($chkDate !== true) {
                $arrErr['event_date'] = $chkDate;
            }
            $chkTime = $this->TIME_CHECK($event_time);
            if ($chkTime !== true) {
                $arrErr['event_time'] = $chkTime;
            }
        }

        return $arrErr;
    }

    /**
     * 年月日として適正かをチェック
     * @param  date $value
     * @return void
     */
    public function DATE_CHECK($value)
    {
        if (!preg_match('/^[0-9]{4}(\-)[0-9]{1,2}(\-)[0-9]{1,2}$/', $value)) {
             return "※ イベント開催日の日付形式に問題があります。<br />";
        }
        else {
            list($y, $m, $d) = explode('-', $value);
            if (!$y || !$m || !$d) {
                 return "※ イベント開催日の日付形式に問題があります。<br />";
            }
            else if (!checkdate($m, $d, $y)) {
                 return "※ イベント開催日の年月日に問題があります。正しい日付を指定してください。<br />";
            }
        }
        return true;
    }

    /**
     * 年月日として適正かをチェック
     * @param  date $value
     * @return void
     */
    public function TIME_CHECK($value)
    {
        if (!preg_match('/^[0-9]{2}(:)[0-9]{2}$/', $value)) {
             return "※ イベント開催時間の形式に問題があります。<br />";
        }
        else {
            list($h, $i) = explode(':', $value);
            $err = '';
            if (23 < $h) {
                $err .= "イベント開催の時間指定は23時までとしてください。<br />";
            }
            if (59 < $i) {
                $err .= "分は59分までとしてください。<br />";
            }
            return $err !== '' ? $err : true;
        }
        return true;
    }

    /**
     * パラメーター情報の初期化
     *
     * @param  SC_FormParam_Ex $objFormParam SC_FormParamインスタンス
     * @param  array  $arrPost      $_POSTデータ
     * @return void
     */
    public function lfInitFormParam($objFormParam, $arrPost)
    {
        parent::lfInitFormParam($objFormParam, $arrPost);

        $objFormParam->addParam('有料会員専用', 'is_paid_only', 1, 'na', array('NUM_CHECK', 'MAX_LENGTH_CHECK'));
        $objFormParam->addParam('イベント開催日', 'event_date', 10, 'a', array('MAX_LENGTH_CHECK'));
        $objFormParam->addParam('イベント開催時間', 'event_time', 5, 'a', array('MAX_LENGTH_CHECK'));

        if (isset($arrPost['event_date']) && $arrPost['event_date'] != '') {
            $arrPost['event_date'] = str_replace('/', '-', $arrPost['event_date']);
        }

        if (isset($arrPost['event_time']) && $arrPost['event_time'] != '') {
            $arrPost['event_time'] = 5 < strlen($arrPost['event_time'])
                    ? substr($arrPost['event_time'], 0, 5)
                    : $arrPost['event_time']
            ;
        }

        $objFormParam->setParam($arrPost);
        $objFormParam->convParam();
    }
}
