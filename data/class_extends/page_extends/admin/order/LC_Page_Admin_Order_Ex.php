<?php
/*
 * This file is part of EC-CUBE
 *
 * Copyright(c) 2000-2014 LOCKON CO.,LTD. All Rights Reserved.
 *
 * http://www.lockon.co.jp/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

require_once CLASS_REALDIR . 'pages/admin/order/LC_Page_Admin_Order.php';

/**
 * 受注管理 のページクラス(拡張).
 *
 * LC_Page_Admin_Order をカスタマイズする場合はこのクラスを編集する.
 *
 * @package Page
 * @author LOCKON CO.,LTD.
 * @version $Id$
 */
class LC_Page_Admin_Order_Ex extends LC_Page_Admin_Order
{
    /**
     * Page を初期化する.
     *
     * @return void
     */
    public function init()
    {
        parent::init();

        $masterData = new SC_DB_MasterData_Ex();
        $this->arrProductType = $masterData->getMasterData('mtb_product_type');
    }

    /**
     * Page のプロセス.
     *
     * @return void
     */
    public function process()
    {
        parent::process();
    }

    /**
     * パラメーター情報の初期化を行う.
     *
     * @param  SC_FormParam $objFormParam SC_FormParam インスタンス
     * @return void
     */
    public function lfInitParam(&$objFormParam)
    {
        parent::lfInitParam($objFormParam);

        // 追加フォーム
        // 商品種別
        $objFormParam->addParam('商品種別', 'search_product_type', INT_LEN, 'n', array('MAX_LENGTH_CHECK', 'NUM_CHECK'));

        // ポイント付与
        $objFormParam->addParam('ポイント付与', 'search_add_point', INT_LEN, 'n', array('MAX_LENGTH_CHECK', 'NUM_CHECK'));
    }

    /**
     * クエリを構築する.
     *
     * 検索条件のキーに応じた WHERE 句と, クエリパラメーターを構築する.
     * クエリパラメーターは, SC_FormParam の入力値から取得する.
     *
     * 構築内容は, 引数の $where 及び $arrValues にそれぞれ追加される.
     *
     * @param  string       $key          検索条件のキー
     * @param  string       $where        構築する WHERE 句
     * @param  array        $arrValues    構築するクエリパラメーター
     * @param  SC_FormParam $objFormParam SC_FormParam インスタンス
     * @return void
     */
    public function buildQuery($key, &$where, &$arrValues, &$objFormParam)
    {

        switch ($key) {
            // 商品種別
            case 'search_product_type':
                $arrParam = $objFormParam->getValue($key);
                $tmpWhere = '';
                foreach ($arrParam as $param) {
                    $tmpWhere .= $tmpWhere ? ', ' : '';
                    $tmpWhere .= '?';
                    $arrValues[] = $param;
                }
                $where .= " AND dtb_order.product_type IN ({$tmpWhere})";
                break;
            // ポイント付与
            case 'search_add_point':
                $val = $objFormParam->getValue($key);
                if (0 < $val) {
                    $where .= " AND dtb_order.point_add_flg = ?";
                    $arrValues[] = 1 == $val ? 1 : 0;
                }
                break;
            default :
                parent::buildQuery($key, $where, $arrValues, $objFormParam);
                break;
        }
    }
}
