<?php
/*
 * This file is part of EC-CUBE
 *
 * Copyright(c) 2000-2014 LOCKON CO.,LTD. All Rights Reserved.
 *
 * http://www.lockon.co.jp/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

require_once CLASS_REALDIR . 'pages/admin/basis/LC_Page_Admin_Basis.php';

/**
 * 店舗基本情報 のページクラス(拡張).
 *
 * LC_Page_Admin_Basis をカスタマイズする場合はこのクラスを編集する.
 *
 * @package Page
 * @author LOCKON CO.,LTD.
 * @version $Id$
 */
class LC_Page_Admin_Basis_Ex extends LC_Page_Admin_Basis
{
    /**
     * Page を初期化する.
     *
     * @return void
     */
    function init()
    {
        parent::init();
    }

    /**
     * Page のプロセス.
     *
     * @return void
     */
    function process()
    {
        parent::process();
    }

    /**
     * @param SC_FormParam_Ex $objFormParam
     */
    public function lfInitParam(&$objFormParam, $post)
    {
        parent::lfInitParam($objFormParam, $post);

        $objFormParam->addParam('ショップ固有ID', 'prefix', 3, 'a', array('EXIST_CHECK', 'MAX_LENGTH_CHECK'));
    }

    /**
     * 入力エラーチェック
     * @param SC_FormParam_Ex $objFormParam
     */
    public function lfCheckError(&$objFormParam)
    {
        $arrErr = parent::lfCheckError($objFormParam);

        if (!SC_Utils_Ex::isBlank($arrErr)) {
            return $arrErr;
        }

        $prefix = $objFormParam->getValue('prefix');

        if (!preg_match('/^[0-9a-zA-Z]{3}$/', $prefix)) {
            $arrErr['prefix'] = 'ショップ固有IDは半角英数 3文字で指定して下さい。';
        }

        return $arrErr;
    }
}
