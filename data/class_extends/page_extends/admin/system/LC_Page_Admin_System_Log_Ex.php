<?php
/*
 * This file is part of EC-CUBE
 *
 * Copyright(c) 2000-2014 LOCKON CO.,LTD. All Rights Reserved.
 *
 * http://www.lockon.co.jp/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

require_once CLASS_REALDIR . 'pages/admin/system/LC_Page_Admin_System_Log.php';

/**
 * ログ のページクラス(拡張).
 *
 * LC_Page_Admin_System_Log をカスタマイズする場合はこのクラスを編集する.
 *
 * @package Page
 * @author Seasoft 塚田将久
 * @version $Id$
 */
class LC_Page_Admin_System_Log_Ex extends LC_Page_Admin_System_Log
{

    /**
     * Page を初期化する.
     *
     * @return void
     */
    public function init()
    {
        parent::init();
    }

    /**
     * Page のプロセス.
     *
     * @return void
     */
    public function process()
    {
        parent::process();
    }

    /**
     * ログファイルの一覧を読み込む
     *
     * TODO mtb_constants から動的生成したい。
     * @return void
     */
    public function loadLogList()
    {
        parent::loadLogList();

        $objBatch = new SC_Batch_Base();
        $exLogFile = $objBatch->getLogList();
        foreach ($exLogFile as $key => $val) {
            $this->arrLogList[$key] = $val;
        }
    }

    /**
     * ログファイルのパスを取得する
     *
     * セキュリティ面をカバーする役割もある。
     */
    public function getLogPath($log_name)
    {
        $objBatch = new SC_Batch_Base();
        $exLogFile = $objBatch->getLogList();
        if (array_key_exists($log_name, $exLogFile)) {
            return DATA_REALDIR . "logs/{$log_name}.log";
        }

        return parent::getLogPath($log_name);
    }

    public function getExLogList()
    {
        $objBatch = new SC_Batch_Base();
        return $objBatch->exLogFile;
    }
}
