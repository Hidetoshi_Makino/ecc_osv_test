<?php
/*
 * This file is part of EC-CUBE
 *
 * Copyright(c) 2000-2014 LOCKON CO.,LTD. All Rights Reserved.
 *
 * http://www.lockon.co.jp/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

require_once CLASS_REALDIR . 'pages/regist/LC_Page_Regist.php';

/**
 * 会員登録のページクラス(拡張).
 *
 * LC_Page_Regist をカスタマイズする場合はこのクラスを編集する.
 *
 * @package Page
 * @author LOCKON CO.,LTD.
 * @version $Id$
 */
class LC_Page_Regist_Ex extends LC_Page_Regist
{
    /**
     * Page を初期化する.
     *
     * @return void
     */
    public function init()
    {
        parent::init();
    }

    /**
     * Page のプロセス.
     *
     * @return void
     */
    public function process()
    {
        parent::process();
    }

    /**
     * Page のAction.
     *
     * @return void
     */
    public function action()
    {
        $mode = $this->getMode();
        if ('regist' !== $mode) {
            parent::action();
        }

        $get = filter_input_array(INPUT_GET);
        $secretKey = isset($get['id']) ? $get['id'] : null;
        if (is_null($secretKey)) {
            SC_Utils_Ex::sfDispSiteError(CUSTOMER_ERROR);
        }

        // 顧客情報を取得
        $objCustomer = new SC_Helper_Customer_Ex();
        $this->customer = $objCustomer->getCustomerBySecretKey($secretKey);

        // 入力チェック
        $this->arrErr = $this->lfCheckError($get);
        if (is_array($this->arrErr) && $this->arrErr['id']) {
            SC_Utils_Ex::sfDispSiteError(FREE_ERROR_MSG, '', true, $this->arrErr['id']);
        }

        $objPurchase = new SC_Helper_Purchase_Ex();
        $uniqId = $objPurchase->inCartMonthlyProduct($this->customer);

        if ($uniqId) {
            SC_Response_Ex::sendRedirect(SHOPPING_PAYMENT_URLPATH);
            SC_Response_Ex::actionExit();
        }
    }

    /**
     * 入力エラーチェック
     *
     * @param mixed $array
     * @access private
     * @return array エラーの配列
     */
    public function lfCheckError($array)
    {
        $objErr = new SC_CheckError_Ex($array);

        if (preg_match("/^[[:alnum:]]+$/", $array['id'])) {
            if (!is_numeric(SC_Helper_Customer_Ex::sfGetCustomerId($array['id'], true))) {
                $objErr->arrErr['id'] = '※ 既に会員登録が完了しているか、無効なURLです。<br>';
            }
        }
        else {
            $objErr->arrErr['id'] = '無効なURLです。メールに記載されている本会員登録用URLを再度ご確認ください。';
        }

        if ($objErr->arrErr['id']) {
            return $objErr->arrErr;
        }

        if (!$this->customer) {
            $objErr->arrErr['id'] = '無効なアクセスです。';
            return $objErr->arrErr;
        }

        return true;
    }


}
