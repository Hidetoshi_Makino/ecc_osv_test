<?php
/*
 * This file is part of EC-CUBE
 *
 * Copyright(c) 2000-2014 LOCKON CO.,LTD. All Rights Reserved.
 *
 * http://www.lockon.co.jp/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

require_once CLASS_REALDIR . 'pages/cart/LC_Page_Cart.php';

/**
 * カート のページクラス(拡張).
 *
 * LC_Page_Cart をカスタマイズする場合はこのクラスを編集する.
 *
 * @package Page
 * @author LOCKON CO.,LTD.
 * @version $Id$
 */
class LC_Page_Cart_Ex extends LC_Page_Cart
{
    /**
     * Page を初期化する.
     *
     * @return void
     */
    public function init()
    {
        parent::init();
    }

    /**
     * Page のプロセス.
     *
     * @return void
     */
    public function process()
    {
        parent::process();
    }

    /**
     * Page のアクション.
     *
     * @return void
     */
    public function action()
    {
        $objCustomer = new SC_Customer_Ex();
        $objCartSess = new SC_CartSession_Ex();
        $objSiteSess = new SC_SiteSession_Ex();
        $objPurchase = new SC_Helper_Purchase_Ex();

        $isLogin = $objCustomer->isLoginSuccess(true);
        $this->cartKeys = $cartKeys = $objCartSess->getKeys();
        $hasMonthlyProduct = in_array(PRODUCT_TYPE_MONTHLY, $cartKeys);
        $this->mode = $mode = $this->getMode();

        // ログイン状態 且つ 有料会員申込の形跡がある場合
        if ($isLogin && $hasMonthlyProduct) {
            $objCartSess->delAllProducts(PRODUCT_TYPE_MONTHLY);
            unset($_SESSION['pre_customer']);
        }
        else if ($hasMonthlyProduct && isset($_SESSION['pre_customer']) && $_SESSION['pre_customer']) {
            $tpl_uniqid = $objSiteSess->getUniqId();
            $arrOrderTemp = $objPurchase->getOrderTemp($tpl_uniqid);

            if ($arrOrderTemp) {
                $objProduct = new SC_Product_Ex();
                $arrProduct = $objProduct->getDetail(PAID_PRODUCT_ID);
                $productName = $arrProduct['name'];

                $this->tpl_onload .= "window.alert('"
                        . htmlspecialchars($productName)
                        . " の決済が完了していません。決済画面へ移動します。');\n"
                        . "window.location.href = '" . SHOPPING_PAYMENT_URLPATH ."';";
            }
            else {
                $objCartSess->delAllProducts(PRODUCT_TYPE_MONTHLY);
                unset($_SESSION['pre_customer']);

                SC_Utils_Ex::sfDispSiteError('');
                SC_Response_Ex::actionExit();
            }
        }

        if ($isLogin || 'confirm' != $mode) {
            return parent::action();
        }

        // 有料会員専用商品が含まれていないかチェック
        //決済処理中ステータスのロールバック
        $objPurchase->cancelPendingOrder(PENDING_ORDER_CANCEL_FLAG);

        $post = (array) filter_input_array(INPUT_POST);
        $get = (array) filter_input_array(INPUT_GET);
        $request = array_merge($get, $post);

        $objFormParam = $this->lfInitParam($post);
        $cartKey = $objFormParam->getValue('cartKey');

        // エラーチェック
        $arrError = $objFormParam->checkError();
        if (isset($arrError) && !empty($arrError)) {
            SC_Utils_Ex::sfDispSiteError(CART_NOT_FOUND);
            SC_Response_Ex::actionExit();
        }

        $cartList = $objCartSess->chkPaidOnlyItem($cartKey, $isLogin);
        if (false !== $cartList && is_array($cartList) && 0 < count($cartList)) {
            // カートを購入モードに設定
            $this->lfSetCurrentCart($objSiteSess, $objCartSess, $cartKey);

            // 購入ページへ
            SC_Response_Ex::sendRedirect(SHOPPING_URL);
            SC_Response_Ex::actionExit();
        }
        else {
            $this->tpl_message .= 'カート内に購入いただけない商品が含まれるか、商品がありません。';
        }

        $objFormParam4OpenCategoryTree =
            $this->lfInitParam4OpenCategoryTree($request);
        if ($objFormParam4OpenCategoryTree->getValue('product_id')) {
            $arrQueryString = array(
                'product_id' => $objFormParam4OpenCategoryTree->getValue(
                    'product_id'),
            );
        } else {
            $arrQueryString = array(
                'category_id' => $objFormParam4OpenCategoryTree->getValue(
                    'category_id'),
            );
        }

        $this->arrInfo = SC_Helper_DB_Ex::sfGetBasisData();
        $totalIncTax = 0;
        foreach ($this->cartKeys as $key) {
            // カート集計処理
            $this->tpl_message .= $objCartSess->checkProducts($key);
            $this->tpl_total_inctax[$key] = $objCartSess->getAllProductsTotal($key);
            $totalIncTax += $this->tpl_total_inctax[$key];
            $this->tpl_total_tax[$key] = $objCartSess->getAllProductsTax($key);
            // ポイント合計
            $this->tpl_total_point[$key] = $objCartSess->getAllProductsPoint($key);

            $this->arrData[$key] = $objCartSess->calculate($key, $objCustomer);

            // 送料無料チェック
            $this->arrData[$key]['is_deliv_free'] = $objCartSess->isDelivFree($key);

            // 送料無料までの金額を計算
            $this->tpl_deliv_free[$key] = $this->arrInfo['free_rule'] - $this->tpl_total_inctax[$key];
        }

        //商品の合計金額をセット
        $this->tpl_all_total_inctax = $totalIncTax;

        $this->tpl_category_id =
            $objFormParam4OpenCategoryTree->getValue('category_id');
        $this->tpl_product_id =
            $objFormParam4OpenCategoryTree->getValue('product_id');

        // ログイン判定
        if ($objCustomer->isLoginSuccess(true)) {
            $this->tpl_login = true;
            $this->tpl_user_point = $objCustomer->getValue('point');
            $this->tpl_name = $objCustomer->getValue('name01');
        }

        // 前頁のURLを取得
        $referer = filter_input(INPUT_SERVER, 'HTTP_REFERER');
        $this->lfGetCartPrevUrl($_SESSION, $referer);
        $this->tpl_prev_url = (isset($_SESSION['cart_prev_url'])) ? $_SESSION['cart_prev_url'] : '';

        // 全てのカートの内容を取得する
        $this->cartItems = $objCartSess->getAllCartList();
    }
}
