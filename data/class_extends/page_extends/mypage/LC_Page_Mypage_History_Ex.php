<?php
/*
 * This file is part of EC-CUBE
 *
 * Copyright(c) 2000-2014 LOCKON CO.,LTD. All Rights Reserved.
 *
 * http://www.lockon.co.jp/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

require_once CLASS_REALDIR . 'pages/mypage/LC_Page_Mypage_History.php';

/**
 * 購入履歴 のページクラス(拡張).
 *
 * LC_Page_Mypage_History をカスタマイズする場合はこのクラスを編集する.
 *
 * @package Page
 * @author LOCKON CO.,LTD.
 * @version $Id$
 */
class LC_Page_Mypage_History_Ex extends LC_Page_Mypage_History
{
    /**
     * Page を初期化する.
     *
     * @return void
     */
    public function init()
    {
        parent::init();
    }

    /**
     * Page のプロセス.
     *
     * @return void
     */
    public function process()
    {
        parent::process();
    }

    /**
     * Page のAction.
     *
     * @return void
     */
    public function action()
    {
        //決済処理中ステータスのロールバック
        $objPurchase = new SC_Helper_Purchase_Ex();
        $objPurchase->cancelPendingOrder(PENDING_ORDER_CANCEL_FLAG);

        $objCustomer = new SC_Customer_Ex();
        $objProduct = new SC_Product();
        $order_id = filter_input(INPUT_GET, 'order_id');
        $customer_id = $objCustomer->getValue('customer_id');
        $mode = $this->getMode();

        if (!SC_Utils_Ex::sfIsInt($order_id)) {
            SC_Utils_Ex::sfDispSiteError(CUSTOMER_ERROR);
        }

        $this->is_price_change  = false;
        $detailId = filter_input(INPUT_GET, 'detail');
        $arrOrderDetail = array();

        //受注データの取得
        $this->tpl_arrOrderData = $objPurchase->getOrder($order_id, $customer_id);
        $this->tpl_arrOrderDetail = array();

        // 受注商品明細の取得
        if (!empty($this->tpl_arrOrderData)) {
            $this->tpl_arrOrderDetail = $objPurchase->getOrderDetail($order_id);

            if ('qr' == $mode && !is_null($detailId) && isset($this->tpl_arrOrderDetail[$detailId])) {
                $arrOrderDetail = $this->tpl_arrOrderDetail[$detailId];
            }
        }

        // E-Ticket 用にタイムスタンプを渡す
        $this->time = time();

        // E-Ticket の場合
        if ('qr' == $mode && $arrOrderDetail) {
            $dateTime = new DateTime($arrOrderDetail['event_date'] . $arrOrderDetail['event_time']);

            $param = array(
                'customer_id' => $customer_id,
                'name' => $objCustomer->getValue('name01') . ' ' . $objCustomer->getValue('name02'),
                'quantity' => (int) $arrOrderDetail['quantity'],
                'order_id' => $order_id,
                'event_date' => $dateTime->format('Y/m/d'),
                'event_time' => $dateTime->format('H:i'),
            );

            $this->qrImageAction($param);
            SC_Response_Ex::actionExit();
        }

        if (empty($this->tpl_arrOrderData)) {
            SC_Utils_Ex::sfDispSiteError(CUSTOMER_ERROR);
        }

        $this->arrShipping = $this->lfGetShippingDate($objPurchase, $order_id, $this->arrWDAY);

        $this->isMultiple = count($this->arrShipping) > 1;

        // 支払い方法の取得
        $this->arrPayment = SC_Helper_Payment_Ex::getIDValueList();

        foreach ($this->tpl_arrOrderDetail as $product_index => $arrOrderProductDetail) {
            //必要なのは商品の販売金額のみなので、遅い場合は、別途SQL作成した方が良い
            $arrTempProductDetail = $objProduct->getProductsClass($arrOrderProductDetail['product_class_id']);
            // 税計算
            $this->tpl_arrOrderDetail[$product_index]['price_inctax'] = $this->tpl_arrOrderDetail[$product_index]['price']  +
                SC_Helper_TaxRule_Ex::calcTax(
                    $this->tpl_arrOrderDetail[$product_index]['price'],
                    $this->tpl_arrOrderDetail[$product_index]['tax_rate'],
                    $this->tpl_arrOrderDetail[$product_index]['tax_rule']
                    );
            $arrTempProductDetail['price02_inctax'] = SC_Helper_TaxRule_Ex::sfCalcIncTax(
                    $arrTempProductDetail['price02'],
                    $arrTempProductDetail['product_id'],
                    $arrTempProductDetail['product_class_id']
                    );
            if ($this->tpl_arrOrderDetail[$product_index]['price_inctax'] != $arrTempProductDetail['price02_inctax']) {
                $this->is_price_change = true;
            }
            $this->tpl_arrOrderDetail[$product_index]['product_price_inctax'] = ($arrTempProductDetail['price02_inctax']) ? $arrTempProductDetail['price02_inctax'] : 0 ;

            if (PRODUCT_TYPE_ETICKET === $arrOrderProductDetail['product_type_id']) {
                $this->tpl_arrOrderDetail[$product_index]['effective_time'] = strtotime($arrOrderProductDetail['event_date'] . '' . $arrOrderProductDetail['event_time']) - $this->time;
            }
        }

        $this->tpl_arrOrderDetail = $this->setMainListImage($this->tpl_arrOrderDetail);
        $objPurchase->setDownloadableFlgTo($this->tpl_arrOrderDetail);

        // 受注メール送信履歴の取得
        $this->tpl_arrMailHistory = $this->lfGetMailHistory($order_id);
    }

    /**
     * 生成されたQRコードを出力する
     * @param arrray $param
     */
    public function qrImageAction($param)
    {
        $image = null;

        if ($param) {
            $objEticket = new SC_Helper_Eticket();
            $str = "会員ID: " . $param['customer_id'] . "\r\n"
                    . "会員名: " . $param['name'] . "\r\n"
                    . "数量: " . $param['quantity'] . "\r\n"
                    . "受注番号: " . $param['order_id'] . "\r\n"
                    . "開催日: " . $param['event_date'] . "\r\n"
                    . "開催時刻: " . $param['event_time'] . "\r\n";
            $image = $objEticket->getQR($str, SC_Helper_Eticket::LEVEL_L, 6);
        }

        ob_clean();
        if ($image) {
            header('Content-Type: image/png');
            header('X-Content-Type-Options: nosniff');
            header('Content-Disposition: inline; filename=' . md5(time() . microtime()) . '.png');
            echo $image;
        }
        else {
            header("HTTP/1.0 404 Not Found");
        }

        exit();
    }
}
