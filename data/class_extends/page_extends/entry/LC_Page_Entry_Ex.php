<?php
/*
 * This file is part of EC-CUBE
 *
 * Copyright(c) 2000-2014 LOCKON CO.,LTD. All Rights Reserved.
 *
 * http://www.lockon.co.jp/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

require_once CLASS_REALDIR . 'pages/entry/LC_Page_Entry.php';

/**
 * 会員登録(入力ページ) のページクラス(拡張).
 *
 * LC_Page_Entry をカスタマイズする場合はこのクラスを編集する.
 *
 * @package Page
 * @author LOCKON CO.,LTD.
 * @version $Id$
 */
class LC_Page_Entry_Ex extends LC_Page_Entry
{
    /**
     * Page を初期化する.
     *
     * @return void
     */
    public function init()
    {
        parent::init();
    }

    /**
     * Page のプロセス.
     *
     * @return void
     */
    public function process()
    {
        parent::process();
    }

    /**
     * Page のプロセス
     * @return void
     */
    public function action()
    {
        $mode = $this->getMode();
        if ('complete' !== $mode) {
            return parent::action();
        }

        $objFormParam = new SC_FormParam_Ex();
        $objPurchase = new SC_Helper_Purchase_Ex();

        //決済処理中ステータスのロールバック
        $objPurchase->cancelPendingOrder(PENDING_ORDER_CANCEL_FLAG);

        // PC時は規約ページからの遷移でなければエラー画面へ遷移する
        if ($this->lfCheckReferer() === false) {
            SC_Utils_Ex::sfDispSiteError(PAGE_ERROR, '', true);
        }

        $post = filter_input_array(INPUT_POST);
        SC_Helper_Customer_Ex::sfCustomerEntryParam($objFormParam);
        $objFormParam->setParam($post);

        //-- 会員登録と完了画面
        $uniqid = null;
        $orderUniqid = null;
        $this->arrErr = SC_Helper_Customer_Ex::sfCustomerEntryErrorCheck($objFormParam);
        if (empty($this->arrErr)) {
            $uniqid = $this->lfRegistCustomerData($this->lfMakeSqlVal($objFormParam));
        }

        if ($uniqid) {
            // 顧客情報を取得
            $objCustomer = new SC_Helper_Customer_Ex();
            $arrCustomer = $objCustomer->getCustomerBySecretKey($uniqid);

            $objPurchase = new SC_Helper_Purchase_Ex();
            $orderUniqid = $objPurchase->inCartMonthlyProduct($arrCustomer);
            if ($orderUniqid) {
                SC_Response_Ex::sendRedirect(SHOPPING_PAYMENT_URLPATH);
                SC_Response_Ex::actionExit();
            }
        }

        $this->arrForm = $objFormParam->getFormParamList();
    }

}
