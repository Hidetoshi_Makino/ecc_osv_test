<?php
/*
 * This file is part of EC-CUBE
 *
 * Copyright(c) 2000-2014 LOCKON CO.,LTD. All Rights Reserved.
 *
 * http://www.lockon.co.jp/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

require_once CLASS_REALDIR . 'pages/shopping/LC_Page_Shopping_Payment.php';

/**
 * 支払い方法選択 のページクラス(拡張).
 *
 * LC_Page_Shopping_Payment をカスタマイズする場合はこのクラスを編集する.
 *
 * @package Page
 * @author LOCKON CO.,LTD.
 * @version $Id$
 */
class LC_Page_Shopping_Payment_Ex extends LC_Page_Shopping_Payment
{
    /**
     * Page を初期化する.
     *
     * @return void
     */
    public function init()
    {
        parent::init();
    }

    /**
     * Page のプロセス.
     *
     * @return void
     */
    public function process()
    {
        parent::process();
    }

    /**
     * Page のアクション.
     *
     * @return void
     */
    public function action()
    {
        $mode = $this->getMode();
        $objCartSess = new SC_CartSession_Ex();
        $this->cartKey = $objCartSess->getKey();
        $exTarget = array(
            PRODUCT_TYPE_DOWNLOAD, PRODUCT_TYPE_MONTHLY, PRODUCT_TYPE_ETICKET
        );
        $this->is_download = in_array($this->cartKey, $exTarget);

        // confirm 以外 及び 通常商品は 通常処理を行う。
        if ('confirm' !== $mode || !$this->is_download) {
            return parent::action();
        }

        $objPurchase = new SC_Helper_Purchase_Ex();
        $objSiteSess = new SC_SiteSession_Ex();
        $objCustomer = new SC_Customer_Ex();
        $objFormParam = new SC_FormParam_Ex();
        $objDelivery = new SC_Helper_Delivery_Ex();

        // 決済処理中ステータスのロールバック
        $objPurchase->cancelPendingOrder(PENDING_ORDER_CANCEL_FLAG);

        // 配送業者を取得
        $this->arrDeliv = $objDelivery->getList($this->cartKey);
        $this->is_single_deliv = $this->isSingleDeliv($this->arrDeliv);
        $this->is_multiple = false;

        // カートの情報を取得
        unset($_SESSION['shipping']);   // 配送先住所不要
        $this->arrShipping = array();


        $this->tpl_uniqid = $objSiteSess->getUniqId();
        $objPurchase->verifyChangeCart($this->tpl_uniqid, $objCartSess);

        // 戻り URL
        $this->tpl_back_url = '?mode=return';

        // 会員情報の取得
        if ($objCustomer->isLoginSuccess(true)) {
            $this->tpl_login = '1';
            $this->tpl_user_point = $objCustomer->getValue('point');
            $this->name01 = $objCustomer->getValue('name01');
            $this->name02 = $objCustomer->getValue('name02');
        }

        $arrOrderTemp = $objPurchase->getOrderTemp($this->tpl_uniqid);
        // 正常に受注情報が格納されていない場合
        if (SC_Utils_Ex::isBlank($arrOrderTemp)) {
            $backUrl = $this->cartKey != PRODUCT_TYPE_MONTHLY
                    ? CART_URL : TOP_URL;

            SC_Response_Ex::sendRedirect($backUrl);
            SC_Response_Ex::actionExit();
        }

        // カート内商品の妥当性チェック
        $this->tpl_message = $objCartSess->checkProducts($this->cartKey);
        if (strlen($this->tpl_message) >= 1) {
            SC_Response_Ex::sendRedirect(CART_URL);
            SC_Response_Ex::actionExit();
        }

        $this->arrPrices = $objCartSess->calculate($this->cartKey, $objCustomer);

        // パラメーター情報の初期化
        $post = filter_input_array(INPUT_POST);
        $this->setFormParams($objFormParam, $post, false, $this->arrShipping);
        $this->arrErr = $this->lfCheckError($objFormParam, $this->arrPrices['subtotal'], $this->tpl_user_point);

        $deliv_id = $objFormParam->getValue('deliv_id');
        if (strval($deliv_id) !== strval(intval($deliv_id))) {
            $deliv_id = $this->arrDeliv[0]['deliv_id'];
            $objFormParam->setValue('deliv_id', $deliv_id);
        }

        $arrSelectedDeliv = $this->getSelectedDeliv($objCartSess, $deliv_id);
        $this->arrPayment = $arrSelectedDeliv['arrPayment'];
        $this->arrDelivTime = $arrSelectedDeliv['arrDelivTime'];
        $this->img_show = $arrSelectedDeliv['img_show'];

        if (empty($this->arrErr)) {
            // DL系商品の為配送情報登録不要
            // $this->saveShippings($objFormParam, $this->arrDelivTime);
            $this->lfRegistData($this->tpl_uniqid, $objFormParam->getDbArray(), $objPurchase, $this->arrPayment);

            // 正常に登録されたことを記録しておく
            $objSiteSess->setRegistFlag();

            // 確認ページへ移動
            SC_Response_Ex::sendRedirect(SHOPPING_CONFIRM_URLPATH);
            SC_Response_Ex::actionExit();
        }

        $this->arrForm = $objFormParam->getFormParamList();
    }

    /**
     * パラメーター情報の初期化を行う.
     *
     * @param  SC_FormParam $objFormParam SC_FormParam インスタンス
     * @param  boolean      $deliv_only   必須チェックは deliv_id のみの場合 true
     * @param  array        $arrShipping  配送先情報の配列
     * @return void
     */
    public function lfInitParam(&$objFormParam, $deliv_only, &$arrShipping)
    {
        $objFormParam->addParam('配送業者', 'deliv_id', INT_LEN, 'n', array('EXIST_CHECK', 'MAX_LENGTH_CHECK', 'NUM_CHECK'));
        $objFormParam->addParam('ポイント', 'use_point', INT_LEN, 'n', array('MAX_LENGTH_CHECK', 'NUM_CHECK', 'ZERO_START'));
        $objFormParam->addParam('その他お問い合わせ', 'message', LTEXT_LEN, 'KVa', array('SPTAB_CHECK', 'MAX_LENGTH_CHECK'));
        $objFormParam->addParam('ポイントを使用する', 'point_check', INT_LEN, 'n', array('MAX_LENGTH_CHECK', 'NUM_CHECK'), '2');

        if ($deliv_only) {
            $objFormParam->addParam('お支払い方法', 'payment_id', INT_LEN, 'n', array('MAX_LENGTH_CHECK', 'NUM_CHECK'));
        }
        else {
            $objFormParam->addParam('お支払い方法', 'payment_id', INT_LEN, 'n', array('EXIST_CHECK', 'MAX_LENGTH_CHECK', 'NUM_CHECK'));
        }

        if (!$this->is_download) {
            foreach ($arrShipping as $val) {
                $objFormParam->addParam('お届け時間', 'deliv_time_id' . $val['shipping_id'], INT_LEN, 'n', array('MAX_LENGTH_CHECK', 'NUM_CHECK'));
                $objFormParam->addParam('お届け日', 'deliv_date' . $val['shipping_id'], STEXT_LEN, 'KVa', array('MAX_LENGTH_CHECK'));
            }
        }

        $objFormParam->setParam($arrShipping);
        $objFormParam->convParam();
    }
}
