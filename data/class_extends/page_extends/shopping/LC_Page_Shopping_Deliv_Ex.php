<?php
/*
 * This file is part of EC-CUBE
 *
 * Copyright(c) 2000-2014 LOCKON CO.,LTD. All Rights Reserved.
 *
 * http://www.lockon.co.jp/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

require_once CLASS_REALDIR . 'pages/shopping/LC_Page_Shopping_Deliv.php';

/**
 * お届け先の指定 のページクラス(拡張).
 *
 * LC_Page_Shopping_Deliv をカスタマイズする場合はこのクラスを編集する.
 *
 * @package Page
 * @author LOCKON CO.,LTD.
 * @version $Id$
 */
class LC_Page_Shopping_Deliv_Ex extends LC_Page_Shopping_Deliv
{
    /**
     * Page を初期化する.
     *
     * @return void
     */
    public function init()
    {
        parent::init();
    }

    /**
     * Page のプロセス.
     *
     * @return void
     */
    public function process()
    {
        parent::process();
    }

    public function action()
    {
        parent::action();

        $skipType = array(PRODUCT_TYPE_MONTHLY, PRODUCT_TYPE_ETICKET);
        // ダウンロード商品の場合は、支払方法画面に転送
        if (in_array($this->cartKey, $skipType)) {
            $objPurchase = new SC_Helper_Purchase_Ex();
            $objCustomer = new SC_Customer_Ex();
            $objSiteSess = new SC_SiteSession_Ex();

            $sqlval = array();
            $objPurchase->copyFromCustomer($sqlval, $objCustomer, 'shipping');
            $objPurchase->saveShippingTemp();
            $objPurchase->saveOrderTemp($this->tpl_uniqid, $sqlval, $objCustomer);
            $objSiteSess->setRegistFlag();

            SC_Response_Ex::sendRedirect(SHOPPING_PAYMENT_URLPATH);
            SC_Response_Ex::actionExit();
        }
    }
}
