<?php
/*
 * This file is part of EC-CUBE
 *
 * Copyright(c) 2000-2014 LOCKON CO.,LTD. All Rights Reserved.
 *
 * http://www.lockon.co.jp/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

require_once CLASS_REALDIR . 'pages/shopping/LC_Page_Shopping.php';

/**
 * ショッピングログイン のページクラス(拡張).
 *
 * LC_Page_Shopping をカスタマイズする場合はこのクラスを編集する.
 *
 * @package Page
 * @author LOCKON CO.,LTD.
 * @version $Id$
 */
class LC_Page_Shopping_Ex extends LC_Page_Shopping
{
    /**
     * Page を初期化する.
     *
     * @return void
     */
    function init()
    {
        parent::init();
    }

    /**
     * Page のプロセス.
     *
     * @return void
     */
    function process()
    {
        parent::process();
    }

    /**
     * Page のプロセス.
     *
     * @return void
     */
    public function action()
    {
        //決済処理中ステータスのロールバック
        $objPurchase = new SC_Helper_Purchase_Ex();
        $objPurchase->cancelPendingOrder(PENDING_ORDER_CANCEL_FLAG);

        $objSiteSess = new SC_SiteSession_Ex();
        $objCartSess = new SC_CartSession_Ex();
        $objCustomer = new SC_Customer_Ex();
        $objCookie = new SC_Cookie_Ex();
        $objFormParam = new SC_FormParam_Ex();

        $nonmember_mainpage = 'shopping/nonmember_input.tpl';
        $nonmember_title = 'お客様情報入力';

        $this->tpl_uniqid = $objSiteSess->getUniqId();
        $objPurchase->verifyChangeCart($this->tpl_uniqid, $objCartSess);

        $this->cartKey = $objCartSess->getKey();

        // ログイン済みの場合は次画面に遷移
        if ($objCustomer->isLoginSuccess(true)) {
            SC_Response_Ex::sendRedirect(
                    $this->getNextlocation($this->cartKey, $this->tpl_uniqid,
                                           $objCustomer, $objPurchase,
                                           $objSiteSess));
            SC_Response_Ex::actionExit();
        // 非会員かつ, ダウンロード商品の場合はエラー表示
        }
        else {
            if ($this->cartKey == PRODUCT_TYPE_DOWNLOAD) {
                $msg = 'ダウンロード商品を含むお買い物は、会員登録が必要です。<br/>'
                     . 'お手数ですが、会員登録をお願いします。';
                SC_Utils_Ex::sfDispSiteError(FREE_ERROR_MSG, $objSiteSess, false, $msg);
                SC_Response_Ex::actionExit();
            }
        }

        // 携帯端末IDが一致する会員が存在するかどうかをチェックする。
        if (SC_Display_Ex::detectDevice() === DEVICE_TYPE_MOBILE) {
            $this->tpl_valid_phone_id = $objCustomer->checkMobilePhoneId();
        }

        switch ($this->getMode()) {
            // ログイン実行
            case 'login':
                $this->lfInitLoginFormParam($objFormParam);
                $objFormParam->setParam($_POST);
                $objFormParam->trimParam();
                $objFormParam->convParam();
                $objFormParam->toLower('login_email');
                $this->arrErr = $objFormParam->checkError();

                // ログイン判定
                if (SC_Utils_Ex::isBlank($this->arrErr)
                    && $objCustomer->doLogin($objFormParam->getValue('login_email'),
                                             $objFormParam->getValue('login_pass'))) {
                    // クッキー保存判定
                    if ($objFormParam->getValue('login_memory') == '1' && strlen($objFormParam->getValue('login_email')) >= 1) {
                        $objCookie->setCookie('login_email', $objFormParam->getValue('login_email'));
                    }
                    else {
                        $objCookie->setCookie('login_email', '');
                    }

                    echo SC_Utils_Ex::jsonEncode(array('success' =>
                            $this->getNextLocation(
                                    $this->cartKey, $this->tpl_uniqid,
                                    $objCustomer, $objPurchase,
                                    $objSiteSess)));
                    SC_Response_Ex::actionExit();

                }
                // ログインに失敗した場合
                else {
                    // 仮登録の場合
                    if (SC_Helper_Customer_Ex::checkTempCustomer($objFormParam->getValue('login_email'))) {
                        echo $this->lfGetErrorMessage(TEMP_LOGIN_ERROR);
                        SC_Response_Ex::actionExit();
                    }
                    else {
                        echo $this->lfGetErrorMessage(SITE_LOGIN_ERROR);
                        SC_Response_Ex::actionExit();
                    }
                }
                break;
            // お客様情報登録
            case 'nonmember_confirm':
                $this->tpl_mainpage = $nonmember_mainpage;
                $this->tpl_title = $nonmember_title;
                $this->lfInitParam($objFormParam);
                $objFormParam->setParam($_POST);
                $this->arrErr = $this->lfCheckError($objFormParam);

                if (SC_Utils_Ex::isBlank($this->arrErr)) {
                    $this->lfRegistData($this->tpl_uniqid, $objPurchase, $objCustomer, $objFormParam);

                    $arrParams = $objFormParam->getHashArray();
                    $shipping_id = $arrParams['deliv_check'] == '1' ? 1 : 0;
                    $objPurchase->setShipmentItemTempForSole($objCartSess, $shipping_id);

                    $objSiteSess->setRegistFlag();

                    SC_Response_Ex::sendRedirect(SHOPPING_PAYMENT_URLPATH);
                    SC_Response_Ex::actionExit();
                }
                break;

            // 前のページに戻る
            case 'return':
                SC_Response_Ex::sendRedirect(CART_URL);
                SC_Response_Ex::actionExit();
                break;

            // 複数配送ページへ遷移
            case 'multiple':
                // 複数配送先指定が無効な場合はエラー
                if (USE_MULTIPLE_SHIPPING === false) {
                    SC_Utils_Ex::sfDispSiteError(PAGE_ERROR, '', true);
                    SC_Response_Ex::actionExit();
                }

                $this->lfInitParam($objFormParam);
                $objFormParam->setParam($_POST);
                $this->arrErr = $this->lfCheckError($objFormParam);

                if (SC_Utils_Ex::isBlank($this->arrErr)) {
                    $this->lfRegistData($this->tpl_uniqid, $objPurchase, $objCustomer, $objFormParam, true);

                    $objSiteSess->setRegistFlag();

                    SC_Response_Ex::sendRedirect(MULTIPLE_URLPATH);
                    SC_Response_Ex::actionExit();
                }
                $this->tpl_mainpage = $nonmember_mainpage;
                $this->tpl_title = $nonmember_title;
                break;

            // お客様情報入力ページの表示
            case 'nonmember':
                $this->tpl_mainpage = $nonmember_mainpage;
                $this->tpl_title = $nonmember_title;
                $this->lfInitParam($objFormParam);
                // ※breakなし

            default:
                // 前のページから戻ってきた場合は, お客様情報入力ページ
                if (isset($_GET['from']) && $_GET['from'] == 'nonmember') {
                    $this->tpl_mainpage = $nonmember_mainpage;
                    $this->tpl_title = $nonmember_title;
                    $this->lfInitParam($objFormParam);
                } else {
                    // 通常はログインページ
                    $this->lfInitLoginFormParam($objFormParam);
                }

                $this->setFormParams($objFormParam, $objPurchase, $this->tpl_uniqid);
                break;
        }

        // 入力値の取得
        $this->arrForm = $objFormParam->getFormParamList();

        // 記憶したメールアドレスを取得
        $this->tpl_login_email = $objCookie->getCookie('login_email');
        if (!SC_Utils_Ex::isBlank($this->tpl_login_email)) {
            $this->tpl_login_memory = '1';
        }
    }
}
