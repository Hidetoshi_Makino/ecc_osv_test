<?php
/**
 * 毎日のバッチ処理
 */

require_once 'batch_common.php';

$execute = array(
    'SC_Batch_AddPointOrder',
    'SC_Batch_ResetPoint',
);

$obj = null;
foreach ($execute as $name) {
    $batchPath = DATA_REALDIR . 'class/batch/' . $name . '.php';

    if (!file_exists($batchPath)) {
        continue;
    }
    unset($obj);
    require_once $batchPath;
    $obj = new $name;
    $obj->execute();
}
