<?php
/**
 * batch 共用
 */

define('SAFE', true);

/** require.php 相当箇所 **/
define('FRONT_FUNCTION', false);
define('ADMIN_FUNCTION', false);
/** require.php 相当箇所 **/


/** define.php 相当箇所 **/
define('DATA_REALDIR', realpath(__DIR__ . '/..') . '/');

// data/module パスを追加
set_include_path( DATA_REALDIR . 'module' . PATH_SEPARATOR . get_include_path());

// batch では不要だが 念のため定義
define('USE_FILENAME_DIR_INDEX', null);

// bufferを初期化する
while (ob_get_level() > 0 && ob_get_level() > 0) {
    ob_end_clean();
}
/** define.php 相当箇所 **/

require_once DATA_REALDIR . 'require_base.php';
