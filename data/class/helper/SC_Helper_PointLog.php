<?php
/*
 * This file is part of EC-CUBE
 *
 * Copyright(c) 2000-2014 LOCKON CO.,LTD. All Rights Reserved.
 *
 * http://www.lockon.co.jp/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

/**
 * ポイント付与ログのヘルパークラス.
 *
 * @package Helper
 * @author IDS Corporation
 * @version $Id$
 */
class SC_Helper_PointLog
{
    const TABLE = 'dtb_point_log';
    const PRIMARY = 'log_id';

    static protected $fields = array(
        "log_id",
        "customer_id",
        "member_id",
        "order_id",
        "add_point",
        "point",
        "note",
        "create_date",
    );

    /**
     * 指定 顧客ID に紐付くレコードを取得
     *
     * @param  integer $customer_id
     * @return array|boolean
     */
    public function getCustomerLog($customer_id)
    {
        if (!$customer_id) {
            return false;
        }

        $objQuery =& SC_Query_Ex::getSingletonInstance();
        $col = '*';
        $where = 'customer_id = ?';
        $arrWhereVal = array($customer_id);
        $objQuery->setOrder('create_date ASC');

        return $objQuery->select($col, SC_Helper_PointLog_Ex::TABLE, $where, $arrWhereVal);
    }

    /**
     * dtb_point_log への INSERT 処理
     *
     * @param  array   $arrParam
     * @return boolean
     */
    public function insertRecord($arrParam)
    {
        if (!$arrParam || !is_array($arrParam)) {
            return false;
        }

        $fields = SC_Helper_PointLog_Ex::getFields();
        $sqlVal = array();
        foreach ($fields as $field) {
            if (isset($arrParam[$field])) {
                $sqlVal[$field] = $arrParam[$field];
            }
        }
        if ($sqlVal[SC_Helper_PointLog_Ex::PRIMARY]) {
            unset($sqlVal[SC_Helper_PointLog_Ex::PRIMARY]);
        }
        $sqlVal['create_date'] = 'CURRENT_TIMESTAMP';

        $objQuery =& SC_Query_Ex::getSingletonInstance();
        $objQuery->begin();
        $ret = $objQuery->insert(SC_Helper_PointLog_Ex::TABLE, $sqlVal);
        if ($ret) {
            $objQuery->commit();
            return true;
        }
        else {
            $objQuery->rollback();
            return false;
        }
    }

    /**
     * テーブルのカラムキーを返却
     * @return type
     */
    public function getFields()
    {
        return SC_Helper_PointLog_Ex::$fields;
    }
}