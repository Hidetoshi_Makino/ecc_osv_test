<?php
/*
 * This file is part of EC-CUBE
 *
 * Copyright(c) 2000-2014 LOCKON CO.,LTD. All Rights Reserved.
 *
 * http://www.lockon.co.jp/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

/**
 * ポイント付与バッチクラス
 *
 * @package Batch
 * @author IDS Corporation
 * @version $Id$
 */
class SC_Batch_AddPointOrder extends SC_Batch_Base
{
    CONST TARGET_DATE = '14';
    public $LOG_FILE = 'batch_point_order';

    /**
     * バッチ処理を実行する.
     */
    public function execute()
    {
        $startTime = date('Y-m-d H:i:s');
        $this->setLogMsg("ポイント付与処理を開始します。start: {$startTime}");

        // 対象レコードを取得
    	$cnt = $this->getTargetCount();
        if (!$cnt) {
            $this->setLogMsg('処理対象が無い為処理を完了します。');
            $this->writeLogMsg();
            return;
        }
        $this->setLogMsg("{$cnt} レコードに対して処理を実行します。");

        // ポイント付与対象者を取得
        $targets = $this->getTargetCustomer();
        $msg = "【ポイント付与対象者】\n";
        foreach ($targets as $val) {
            $msg .= "\t\t顧客ID: {$val['customer_id']},"
                    . " 姓名: {$val['customer_name']}\n";
        }
        $this->setLogMsg($msg);

        // ポイント付与実行
        $success = $this->addPointAction();

        $endTime = date('Y-m-d H:i:s');
        if ($success) {
            $this->setLogMsg("【正常終了】ポイント付与処理を完了します。end: {$endTime}");
        }
        else {
            $subject = 'ポイント付与バッチ処理失敗';
            $msg = array(
                "【処理開始時刻】 " . $startTime,
                "【処理終了時刻】 " . $endTime,
                "ポイント付与バッチ処理に失敗した為、ポイント付与前の状態に戻しました。",
                "現在、本処理でのポイントは付与されていない状態です。",
                "",
                "エラーログ等を確認の上、対応をお願いいたします。",
            );

            $this->sendMail($subject, $msg);
            $this->setLogMsg("【処理失敗】{$subject}");
        }

        $this->writeLogMsg();

        return;
    }

    /**
     * 対象となるレコード数を取得
     */
    public function getTargetCount()
    {
        $objQuery =& SC_Query_Ex::getSingletonInstance();
        $table = 'dtb_order';
        $where = '';
        $arrWhereVal = array();
        $this->getWhere($where, $arrWhereVal);

        return $objQuery->count($table, $where, $arrWhereVal);
    }

    /**
     * ポイント付与対象者リストを返却
     * @return array
     */
    public function getTargetCustomer()
    {
        $objQuery =& SC_Query_Ex::getSingletonInstance();
        $table = 'dtb_order'
                . ' LEFT JOIN dtb_customer'
                . ' ON dtb_order.customer_id = dtb_customer.customer_id';
        $col = "dtb_customer.customer_id,"
                . " CONCAT(dtb_customer.name01, ' ', dtb_customer.name02) AS customer_name";
        $where = '';
        $arrWhereVal = array();
        $this->getWhere($where, $arrWhereVal);
        $objQuery->setGroupBy('dtb_customer.customer_id');

        // 追加の WHERE 句
        $where .= ' AND dtb_customer.del_flg = 0'
                . ' AND dtb_customer.status = 2';

        return $objQuery->select($col, $table, $where, $arrWhereVal);
    }

    /**
     * 処理対象絞り込み用の WHERE 句を作成
     * @param string $where
     * @param array  $arrWhereVal
     */
    public function getWhere(&$where, &$arrWhereVal)
    {
        $where = 'dtb_order.del_flg = 0'
                . ' AND dtb_order.customer_id IS NOT NULL'
                . ' AND dtb_order.customer_id <> 0'
                . ' AND dtb_order.add_point > 0'
                . ' AND dtb_order.point_add_flg = 0'
                . ' AND dtb_order.product_type IS NOT NULL'
                . ' AND dtb_order.product_type <> ?'
                . ' AND dtb_order.status NOT IN(?, ?, ?, ?)'
                . ' AND dtb_order.payment_date < DATE_ADD(CURRENT_TIMESTAMP, INTERVAL - ? DAY)'
                ;
        $arrWhereVal = array(
            (int) PRODUCT_TYPE_MONTHLY,
            (int) ORDER_NEW,
            (int) ORDER_PAY_WAIT,
            (int) ORDER_CANCEL,
            (int) ORDER_PENDING,
            (int) self::TARGET_DATE
        );
    }

    /**
     * 一連のポイント付与処理
     *
     * @return boolean
     */
    public function addPointAction()
    {
        $objQuery =& SC_Query_Ex::getSingletonInstance();
        $objQuery->begin();

        // ① 追加ポイントログ記録: ポイント付与を先に処理すると不都合な為、先にログ記録
        $success = $this->addPointLog();

        // ② ポイント付与
        if ($success) {
            $success = $this->addCustomerPoint();
        }

        // ③ ポイント付与済みフラグ
        if ($success) {
            $success = $this->addPointFlg();
        }

        if ($success) {
            $objQuery->commit();
            return true;
        }
        else {
            $objQuery->rollback();
            return false;
        }
    }

    /**
     * ポイントログ記録
     *
     * @return boolean
     */
    public function addPointLog()
    {
        $where = '';
        $arrWhereVal = array();
        $this->getWhere($where, $arrWhereVal);

        $sql = <<< __EOS__
INSERT INTO dtb_point_log
(order_id, add_point, point, customer_id, note)
SELECT
    dtb_order.order_id,
    dtb_order.add_point,
    IF (@customer = dtb_customer.customer_id,
        @point:=(@point + dtb_order.add_point),
        @point:=(dtb_customer.point + dtb_order.add_point)
    ) AS point,
    (@customer:=dtb_customer.customer_id) AS customer_id,
    '{$this->LOG_FILE}'
FROM
    dtb_customer
    LEFT JOIN dtb_order
        ON dtb_order.customer_id = dtb_customer.customer_id
WHERE
    {$where}
    AND dtb_customer.del_flg = 0
    AND dtb_customer.status = 2
ORDER BY dtb_customer.customer_id, dtb_order.order_id;
__EOS__;

        $ret = $this->query($sql, $arrWhereVal);
        if ($ret === false) {
            return false;
        }
        else {
            return true;
        }
    }

    /**
     * ポイント付与を実行
     *
     * @return boolean
     */
    public function addCustomerPoint()
    {
        $where = '';
        $arrWhereVal = array();
        $this->getWhere($where, $arrWhereVal);

        $sql = <<< __EOS__
UPDATE dtb_customer AS t1, (
    SELECT customer_id, SUM(add_point) AS add_point
    FROM dtb_order
    WHERE {$where}
    GROUP BY customer_id
) AS t2
SET t1.point = t1.point + t2.add_point,
    t1.update_date = CURRENT_TIMESTAMP
WHERE
    t1.customer_id = t2.customer_id AND t1.del_flg = 0
__EOS__;

        $ret = $this->query($sql, $arrWhereVal);
        if ($ret === false) {
            return false;
        }
        else {
            return true;
        }
    }

    /**
     * ポイント付与対象のレコードに付与済みフラグを立てる
     *
     * @return boolean
     */
    public function addPointFlg()
    {
        $where = '';
        $arrWhereVal = array();
        $this->getWhere($where, $arrWhereVal);

        $sql = <<< __EOS__
UPDATE dtb_order SET point_add_flg = 1
WHERE
    {$where}
__EOS__;

        $ret = $this->query($sql, $arrWhereVal);
        if ($ret === false) {
            return false;
        }
        else {
            return true;
        }
    }

}
