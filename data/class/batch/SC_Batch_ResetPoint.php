<?php
/*
 * This file is part of EC-CUBE
 *
 * Copyright(c) 2000-2014 LOCKON CO.,LTD. All Rights Reserved.
 *
 * http://www.lockon.co.jp/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

/**
 * ポイントリセットバッチクラス
 *
 * @package Batch
 * @author IDS Corporation
 * @version $Id$
 */
class SC_Batch_ResetPoint extends SC_Batch_Base
{
    CONST TARGET_YEAR = '1';
    public $LOG_FILE = 'batch_reset_point';


    /**
     * バッチ処理を実行する.
     */
    public function execute()
    {

        $startTime = date('Y-m-d H:i:s');
        $this->setLogMsg("ポイントリセット処理を開始します。start: {$startTime}");

        // 対象レコードを取得
    	$cnt = $this->getTargetCount();
        if (!$cnt) {
            $this->setLogMsg('処理対象が無い為処理を完了します。');
            $this->writeLogMsg();
            return;
        }
        $this->setLogMsg("{$cnt} レコードに対して処理を実行します。");

        // リセット対象者を取得
        $targets = $this->getTargetCustomer();
        $msg = "【ポイントリセット対象者】\n";
        foreach ($targets as $val) {
            $msg .= "\t\t顧客ID: {$val['customer_id']},"
                    . " 姓名: {$val['customer_name']}\n";
        }
        $this->setLogMsg($msg);

        // ポイントリセット実行
        $success = $this->resetPointAction();

        $endTime = date('Y-m-d H:i:s');
        if ($success) {
            $this->setLogMsg("【正常終了】ポイントリセット処理を完了します。end: {$endTime}");
        }
        else {
            $subject = 'ポイントリセットバッチ処理失敗';
            $msg = array(
                "【処理開始時刻】 " . $startTime,
                "【処理終了時刻】 " . $endTime,
                "ポイントリセット処理に失敗した為、リセット前の状態に戻しました。",
                "現在、リセットされていない状態です。",
                "",
                "エラーログ等を確認の上、対応をお願いいたします。",
            );

            $this->sendMail($subject, $msg);
            $this->setLogMsg("【処理失敗】{$subject}");
        }

        $this->writeLogMsg();

        return;
    }

    /**
     * 対象となるレコード数を取得
     */
    public function getTargetCount()
    {
        $objQuery =& SC_Query_Ex::getSingletonInstance();
        $table = 'dtb_customer';
        $where = '';
        $arrWhereVal = array();
        $this->getWhere($where, $arrWhereVal);

        return $objQuery->count($table, $where, $arrWhereVal);
    }

    /**
     * ポイント付与対象者リストを返却
     * @return array
     */
    public function getTargetCustomer()
    {
        $objQuery =& SC_Query_Ex::getSingletonInstance();
        $table = 'dtb_customer';
        $col = "dtb_customer.customer_id,"
                . " CONCAT(dtb_customer.name01, ' ', dtb_customer.name02) AS customer_name";
        $where = '';
        $arrWhereVal = array();
        $this->getWhere($where, $arrWhereVal);

        return $objQuery->select($col, $table, $where, $arrWhereVal);
    }

    /**
     * 処理対象絞り込み用の WHERE 句を作成
     * @param string $where
     * @param array  $arrWhereVal
     */
    public function getWhere(&$where, &$arrWhereVal)
    {
        $where = 'dtb_customer.del_flg = 0'
                . ' AND dtb_customer.status = 2'
                . ' AND dtb_customer.point > 0'
                . ' AND dtb_customer.entry_date_start IS NOT NULL'
                . ' AND dtb_customer.entry_date_end IS NULL'
                . ' AND dtb_customer.last_buy_date < DATE_ADD(CURRENT_TIMESTAMP, INTERVAL - ? YEAR)'
                ;
        $arrWhereVal = array(
            (int) self::TARGET_YEAR
        );
    }

    /**
     * 一連のポイントリセット処理
     *
     * @return boolean
     */
    public function resetPointAction()
    {
        $objQuery =& SC_Query_Ex::getSingletonInstance();
        $objQuery->begin();

        // ① ポイントリセットのログ記録: ポイント付与を先に処理すると不都合な為、先にログ記録
        $success = $this->addPointLog();

        // ② ポイントリセット
        if ($success) {
            $success = $this->resetCustomerPoint();
        }

        if ($success) {
            $objQuery->commit();
            return true;
        }
        else {
            $objQuery->rollback();
            return false;
        }
    }

    /**
     * ポイントログ記録
     *
     * @return boolean
     */
    public function addPointLog()
    {
        $where = '';
        $arrWhereVal = array();
        $this->getWhere($where, $arrWhereVal);

        $sql = <<< __EOS__
INSERT INTO dtb_point_log
(add_point, point, customer_id, note)
SELECT
    - dtb_customer.point,
    0,
    dtb_customer.customer_id,
    '{$this->LOG_FILE}'
FROM
    dtb_customer
WHERE
    {$where}
ORDER BY dtb_customer.customer_id;
__EOS__;

        $ret = $this->query($sql, $arrWhereVal);
        if ($ret === false) {
            return false;
        }
        else {
            return true;
        }
    }

    /**
     * ポイントリセット
     *
     * @return boolean
     */
    public function resetCustomerPoint()
    {
        $where = '';
        $arrWhereVal = array();
        $this->getWhere($where, $arrWhereVal);

        $sql = <<< __EOS__
UPDATE dtb_customer
SET point = 0,
    update_date = CURRENT_TIMESTAMP
WHERE
    {$where}
__EOS__;

        $ret = $this->query($sql, $arrWhereVal);
        if ($ret === false) {
            return false;
        }
        else {
            return true;
        }
    }
}