<?php
/*
 * This file is part of EC-CUBE
 *
 * Copyright(c) 2000-2014 LOCKON CO.,LTD. All Rights Reserved.
 *
 * http://www.lockon.co.jp/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

/**
 * 月額会員へのポイント付与バッチクラス
 *
 * @package Batch
 * @author IDS Corporation
 * @version $Id$
 */
class SC_Batch_AddPointMonthly extends SC_Batch_Base
{
    public $LOG_FILE = 'batch_point_monthly';
    static $point = null;
    static $price = null;
    static $priceIncTax = null;

    /**
     * バッチ処理を実行する.
     */
    public function execute()
    {

        $startTime = date('Y-m-d H:i:s');
        $this->setLogMsg("月額ポイント付与処理を開始します。start: {$startTime}");

        $point = $this->getPoint();
        if (!$point) {
            $this->setLogMsg("付与ポイントがありません。処理を終了します。");
            $this->writeLogMsg();
            return;
        }

        // 対象レコードを取得
    	$cnt = $this->getTargetCount();
        if (!$cnt) {
            $this->setLogMsg('処理対象が無い為処理を完了します。');
            $this->writeLogMsg();
            return;
        }
        $this->setLogMsg("{$cnt} レコードに対して処理を実行します。");

        // ポイント付与実行
        $success = $this->addPointAction();

        $endTime = date('Y-m-d H:i:s');
        if ($success) {
            $this->setLogMsg("【正常終了】月額ポイント付与処理を完了します。end: {$endTime}");
        }
        else {
            $subject = '月額ポイント付与バッチ処理失敗';
            $msg = array(
                "【処理開始時刻】 " . $startTime,
                "【処理終了時刻】 " . $endTime,
                "月額ポイント付与バッチ処理に失敗した為、ポイント付与前の状態に戻しました。",
                "現在、本処理でのポイントは付与されていない状態です。",
                "",
                "エラーログ等を確認の上、対応をお願いいたします。",
            );

            $this->sendMail($subject, $msg);
            $this->setLogMsg("【処理失敗】{$subject}");
        }

        $this->writeLogMsg();

        return;
    }

    /**
     * 対象となるレコード数を取得
     */
    public function getTargetCount()
    {
        $objQuery =& SC_Query_Ex::getSingletonInstance();
        $table = 'dtb_customer';
        $where = '';
        $arrWhereVal = array();
        $this->getWhere($where, $arrWhereVal);

        return $objQuery->count($table, $where, $arrWhereVal);
    }

    /**
     * 処理対象絞り込み用の WHERE 句を作成
     * @param string $where
     * @param array  $arrWhereVal
     */
    public function getWhere(&$where, &$arrWhereVal)
    {
        $where = 'dtb_customer.del_flg = 0'
                . ' AND dtb_customer.status = 2'
                . ' AND dtb_customer.entry_date_start IS NOT NULL'
                . " AND dtb_customer.entry_date_start < DATE_FORMAT(CURDATE() , '%Y-%m-01 00:00:00')"
                . ' AND dtb_customer.entry_date_end IS NULL'
                ;
        $arrWhereVal = array(
        );
    }

    /**
     * 一連のポイント付与処理
     *
     * @return boolean
     */
    public function addPointAction()
    {
        $objQuery =& SC_Query_Ex::getSingletonInstance();
        $objQuery->begin();

        // ① 追加ポイントログ記録: ポイント付与を先に処理すると不都合な為、先にログ記録
        $success = $this->addPointLog();

        // ② ポイント付与
        if ($success) {
            $success = $this->addCustomerPoint();
        }

        if ($success) {
            $objQuery->commit();
            return true;
        }
        else {
            $objQuery->rollback();
            return false;
        }
    }

    /**
     * ポイントログ記録
     *
     * @return boolean
     */
    public function addPointLog()
    {
        $where = '';
        $arrWhereVal = array();
        $this->getWhere($where, $arrWhereVal);
        $point = (int) $this->getPoint();

        $sql = <<< __EOS__
INSERT INTO dtb_point_log
(add_point, point, customer_id, note)
SELECT
    {$point},
    dtb_customer.point + {$point},
    dtb_customer.customer_id,
    '{$this->LOG_FILE}'
FROM
    dtb_customer
WHERE
    {$where}
ORDER BY dtb_customer.customer_id;
__EOS__;

        $ret = $this->query($sql, $arrWhereVal);
        if ($ret === false) {
            return false;
        }
        else {
            return true;
        }
    }

    /**
     * ポイント付与を実行
     *
     * @return boolean
     */
    public function addCustomerPoint()
    {
        $where = '';
        $arrWhereVal = array();
        $this->getWhere($where, $arrWhereVal);
        $point = (int) $this->getPoint();

        $sql = <<< __EOS__
UPDATE dtb_customer
SET point = point + {$point},
    update_date = CURRENT_TIMESTAMP
__EOS__;

        if (BATCH_POINT_UPDATE_BUY) {
            $priceIncTax = $this->getPrice();
            $sql .= ", buy_total = buy_total + {$priceIncTax}"
                    . ", last_buy_date = CURRENT_TIMESTAMP";
        }

        // where 句
        $sql .= ' WHERE ' . $where;

        $ret = $this->query($sql, $arrWhereVal);
        if ($ret === false) {
            return false;
        }
        else {
            return true;
        }
    }

    /**
     * 月額のポイントを返却
     */
    public function getPoint()
    {
        if (is_null($this->point)) {
            $this->setPricePoint();
        }
        return $this->point;
    }

    /**
     * 月額のポイントを返却
     */
    public function getPrice()
    {
        if (is_null($this->priceIncTax)) {
            $this->setPricePoint();
        }
        return $this->priceIncTax;
    }

    public function setPricePoint()
    {
        $objProduct = new SC_Product_Ex();
        $ret = $objProduct->getDetail(PAID_PRODUCT_ID);
        $pointRate = $ret['point_rate'];
        $price = (int) $ret['price02_min'];
        $this->priceIncTax = (int) $ret['price02_min_inctax'];

        if (!USE_POINT) {
            $this->point = 0;
            return;
        }

        // 店舗基本情報からポイント付与率を取得
        if (!$pointRate) {
            $conf = SC_Helper_DB_Ex::sfGetBasisData();
            $pointRate = $conf['point_rate'];
        }

        if (!$pointRate) {
            $this->point = 0;
        }
        else {
            $this->point = intval($price * ($pointRate / 100));
        }
    }
}