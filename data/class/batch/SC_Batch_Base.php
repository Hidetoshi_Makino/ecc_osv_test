<?php
/*
 * This file is part of EC-CUBE
 *
 * Copyright(c) 2000-2014 LOCKON CO.,LTD. All Rights Reserved.
 *
 * http://www.lockon.co.jp/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

require_once CLASS_EX_REALDIR . 'util_extends/GC_Utils_Ex.php';
require_once CLASS_EX_REALDIR . 'util_extends/SC_Utils_Ex.php';

/**
 * 拡張実装バッチ処理の共通クラス
 *
 * @package Batch
 * @author IDS Corporation
 * @version $Id$
 */
class SC_Batch_Base extends SC_Batch_Ex
{
    public $LOG_FILE = 'batch';
    static $logMsg = array();
    static $logFiles = array(
        'batch_point_order' => '受注ポイント付与',
        'batch_point_monthly' => '月額ポイント付与',
        'batch_reset_point' => 'ポイント失効処理',
    );

    /**
     * ログメッセージをセット
     * @param strng $msg
     */
    public function setLogMsg($msg)
    {
        $this->logMsg[] = $msg;
    }

    /**
     * セットされたログメッセージを返却
     *
     * @return array
     */
    public function getLogMsg()
    {
        return $this->logMsg;
    }

    /**
     * ログメッセージをリセットする
     */
    public function resetLogMsg()
    {
        $this->logMsg = array();
    }

    /**
     * ログファイルへの書き出し
     *
     * @param string|array $_msg
     */
    public function writeLogMsg()
    {
        $path = DATA_REALDIR . 'logs/' . $this->LOG_FILE . '.log';

        $_msg = $this->getLogMsg();
        if (!$_msg) {
            return;
        }
        $msg = "";
        foreach ($_msg as $key => $val) {
            $msg .= $msg ? "\n\t" : "";
            $msg .= $key . ': ' . $val;
        }

        // 呼び出し元
        $info = debug_backtrace();
        $callPath = $info[0]['file'];

        $logMsg = date('Y/m/d H:i:s') . " [{$callPath}] {$msg}\n";
        error_log($logMsg, 3, $path);

        // ログテーション
        GC_Utils_Ex::gfLogRotation(MAX_LOG_QUANTITY, MAX_LOG_SIZE, $path);
    }

    /**
     * 対象とするログリストを返却する
     *
     * @return array
     */
    public function getLogList()
    {
        return self::$logFiles;
    }

    /**
     * メール送信処理
     *
     * @param string        $subject
     * @param string|array  $msg
     */
    public function sendMail($subject, $msg)
    {
        // 店舗情報を取得
        $arrInfo = SC_Helper_DB_Ex::sfGetBasisData();

        $mailBody = '';
        if (is_array($msg)) {
            foreach ($msg as $val) {
                $mailBody .= $val . "\n";
            }
        }
        else {
            $mailBody = $msg . "\n";
        }

        $objSendMail = new SC_SendMail_Ex();
        // setItem($to, $subject, $body, $from, $from_name, $reply_to='', $return_path='', $errors_to='', $bcc='', $cc ='')
        $to = $arrInfo['email03'];
        $from = $reply = $arrInfo['email04'];
        $fromName = $arrInfo['shop_name'];
        $error = $return = $arrInfo['email04'];

        $objSendMail->setItem($to, $subject, $mailBody, $from, $fromName, $reply, $return, $error);
        $objSendMail->sendMail();
    }


    /**
     * クエリを実行する
     *
     * @param  string $sql
     * @param  array $arrWhereVal
     * @return boolean
     */
    public function query($sql, $arrWhereVal)
    {

        $objQuery =& SC_Query_Ex::getSingletonInstance();
        $objQuery->force_run = true;
        $result = $objQuery->query($sql, $arrWhereVal, false, null, MDB2_PREPARE_MANIP);

        if (is_int($result) && $result) {
            return true;
        }
        else {
            return false;
        }
    }

}
